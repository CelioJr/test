﻿using System;

namespace SEMP.Model.VO.Materiais
{
	public class ConsultaRomaneio
	{
		//EntMov
		public int NumRomaneio { get; set; }
		public string CodOrigem { get; set; }
		public string CodFab { get; set; }
		public string CodTipo { get; set; }
		public string NomUsuario { get; set; }
		public string NomMaquina { get; set; }
		public string CodStatus { get; set; }
		public System.DateTime DatGerac { get; set; }
		public string DatGeracString { get; set; }
		public Nullable<int> QtdImpressa { get; set; }
		public Nullable<System.DateTime> DtUltimaImp { get; set; }
		public string UltImpUser { get; set; }

		//EntMovItem
		public string CodItem { get; set; }
		public Nullable<decimal> QtdItem { get; set; }		
		public string CodModelo { get; set; }
		public string CodDefeito { get; set; }
		public string CodCausa { get; set; }
		public string OrigemDefeito { get; set; }
		public string DetalheDefeito { get; set; }
		public string NomResp { get; set; }
		public string flgDevolvido { get; set; }
		public string DesDevMotivo { get; set; }
		public string NumGrim { get; set; }
		public string FlgCritico { get; set; }
		public string FlgNecReposicao { get; set; }

		//EntMovAprov		
		public Nullable<System.DateTime> DatAprov { get; set; }
		public String DatAprovString { get; set; }
		public string UsuAprov { get; set; }
		public string MaqAprov { get; set; }
		public string CodStatusAprov { get; set; }

		//EntMovRec		
		public string CodItemRec { get; set; }
		public Nullable<decimal> QtdRec { get; set; }
		public Nullable<System.DateTime> DatReceb { get; set; }
		public string DatRecebString { get; set; }
		public string NomUsuarioRec { get; set; }
		public string NomMaquinaRec { get; set; }
		public Nullable<int> NumControle { get; set; }
		public string CodModeloRec { get; set; }
		public string CodDefeitoRec { get; set; }
		public string CodStatusRec { get; set; }
		public string DocInt { get; set; }
		public Nullable<long> QtdLaudo { get; set; }
		public string Ciente { get; set; }
		public string JustCiente { get; set; }
		public Nullable<decimal> QtdPaga { get; set; }
	}
}
