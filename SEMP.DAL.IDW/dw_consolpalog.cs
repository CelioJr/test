namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolpalog
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_consolpalog()
        {
            dw_consolciplog = new HashSet<dw_consolciplog>();
            dw_consolciplog1 = new HashSet<dw_consolciplog>();
            dw_consolpaoco = new HashSet<dw_consolpaoco>();
            dw_consolpt = new HashSet<dw_consolpt>();
            dw_rt = new HashSet<dw_rt>();
        }

        [Key]
        public long id_consolpalog { get; set; }

        public DateTime? dthr_iparada { get; set; }

        public int? ms_dthriparada { get; set; }

        public DateTime? dthr_fparada { get; set; }

        public int? ms_dthrfparada { get; set; }

        public DateTime? dthr_fparada_ab { get; set; }

        public int? ms_dthrfparada_ab { get; set; }

        public long id_pt { get; set; }

        public long id_tparada { get; set; }

        public decimal? seg_auto_tempoparada { get; set; }

        public decimal? seg_auto_tempoparada_ab { get; set; }

        public long? id_tacao { get; set; }

        public long? id_tjust { get; set; }

        public long? id_tcausa { get; set; }

        public decimal? seg_Auto_Tempoparada_Cp { get; set; }

        public decimal? seg_Auto_Tempoparada_sp { get; set; }

        public decimal? seg_manu_Tempoparada_Cp { get; set; }

        public decimal? seg_manu_Tempoparada_sp { get; set; }

        public short? is_varritmo { get; set; }

        public decimal? seg_auto_cta { get; set; }

        public decimal? seg_manu_cta { get; set; }

        public long? id_cp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolciplog> dw_consolciplog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolciplog> dw_consolciplog1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolpaoco> dw_consolpaoco { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolpt> dw_consolpt { get; set; }

        public virtual pp_cp pp_cp { get; set; }

        public virtual dw_t_parada dw_t_parada { get; set; }

        public virtual om_pt om_pt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rt> dw_rt { get; set; }
    }
}
