using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
	public partial class LSAATDTO
	{
		public string CodFab { get; set; }
		public string NumAT { get; set; }
		public System.DateTime DatReferencia { get; set; }
		public string Sequencia { get; set; }
		public string CodModelo { get; set; }
		public Nullable<int> QtdLoteAT { get; set; }
		public System.DateTime DatEmissao { get; set; }
		public System.DateTime DatAlimentacao { get; set; }
		public string NumCGC { get; set; }
		public string CodProcesso { get; set; }
		public string CodStatus { get; set; }
		public Nullable<decimal> QtdProduzida { get; set; }
		public string TipoAT { get; set; }
		public string Observacao { get; set; }
		public Nullable<System.DateTime> DatInicioProd { get; set; }
		public string NumFAT { get; set; }
		public Nullable<System.DateTime> DatLiberacao { get; set; }
		public Nullable<System.DateTime> DatAceite { get; set; }
		public Nullable<System.DateTime> DatFechamento { get; set; }
		public Nullable<System.DateTime> DatProgInicio { get; set; }
		public Nullable<System.DateTime> DatProgFinal { get; set; }
		public string NomAceite { get; set; }

		[Display(Name = "Diferenša")]
		public decimal Diferenca
		{
			get
			{
				var prod = QtdProduzida ?? 0;
				var lote = QtdLoteAT ?? 0;
				return prod - lote;
			}
		}
	}
}
