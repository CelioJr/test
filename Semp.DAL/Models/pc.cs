namespace SEMP.DAL.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class pc
    {
        public decimal id { get; set; }
        public string numeroDeSerie { get; set; }
        public string caminho { get; set; }
        public DateTime? dataReplicacao { get; set; }
    }
}
