﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
	public class tbl_CBCadSubDefeitoMap : EntityTypeConfiguration<tbl_CBCadSubDefeito>
	{
		public tbl_CBCadSubDefeitoMap()
		{
			// Primary Key
			this.HasKey(t => new { t.CodDefeito, t.CodSubDefeito });

			// Properties
			this.Property(t => t.CodDefeito)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(4)
                .IsUnicode(false);

			this.Property(t => t.CodSubDefeito)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.DesDefeito)
				.HasMaxLength(60)
                .IsUnicode(false);

			this.Property(t => t.DesVerificacao)
				.HasMaxLength(100)
                .IsUnicode(false);

			this.Property(t => t.flgAtivo)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			// Table & Column Mappings
			this.ToTable("tbl_CBCadSubDefeito");
			this.Property(t => t.CodDefeito).HasColumnName("CodDefeito");
			this.Property(t => t.CodSubDefeito).HasColumnName("CodSubDefeito");
			this.Property(t => t.DesDefeito).HasColumnName("DesDefeito");
			this.Property(t => t.DesVerificacao).HasColumnName("DesVerificacao");
			this.Property(t => t.flgAtivo).HasColumnName("flgAtivo");
		}
	}
}
