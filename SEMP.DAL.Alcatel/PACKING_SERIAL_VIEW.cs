namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.PACKING_SERIAL_VIEW")]
    public partial class PACKING_SERIAL_VIEW
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_SERIAL { get; set; }

        public DateTime? DATE_COLETIVA { get; set; }

        public DateTime? DATE_FT { get; set; }

        public DateTime? DATE_IMEI { get; set; }

        public DateTime? DATE_PALLET { get; set; }

        public DateTime? DT_CREATION { get; set; }

        [StringLength(20)]
        public string HOSTNAME { get; set; }

        [StringLength(15)]
        public string IMEI_1 { get; set; }

        [StringLength(15)]
        public string IMEI_2 { get; set; }

        [StringLength(15)]
        public string IMEI_3 { get; set; }

        [StringLength(15)]
        public string IMEI_4 { get; set; }

        public int? LINE_COLETIVA { get; set; }

        public int? LINE_FT { get; set; }

        public int? LINE_IMEI { get; set; }

        public int? LINE_INDIVIDUAL { get; set; }

        public int? LINE_PALLET { get; set; }

        [StringLength(15)]
        public string PACKING { get; set; }

        [StringLength(15)]
        public string SERIAL { get; set; }
    }
}
