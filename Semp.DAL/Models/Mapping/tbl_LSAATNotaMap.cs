using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_LSAATNotaMap : EntityTypeConfiguration<tbl_LSAATNota>
    {
        public tbl_LSAATNotaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.NumAT, t.CodItem, t.CodItemAlt, t.Sequencia });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NumAT)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodItemAlt)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.Sequencia)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodNumTec)
                .IsFixedLength()
                .HasMaxLength(10)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_LSAATNota");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.NumAT).HasColumnName("NumAT");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.CodItemAlt).HasColumnName("CodItemAlt");
            this.Property(t => t.Sequencia).HasColumnName("Sequencia");
            this.Property(t => t.QtdNota).HasColumnName("QtdNota");
            this.Property(t => t.PrecoUnitario).HasColumnName("PrecoUnitario");
            this.Property(t => t.CodNumTec).HasColumnName("CodNumTec");
        }
    }
}
