using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using SEMP.Model.DTO;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBTempoModeloMap : EntityTypeConfiguration<CBTempoModeloDTO>
    {
        public tbl_CBTempoModeloMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Id });

            this.Property(t => t.Id)
                .IsRequired();

            // Properties
            this.Property(t => t.CodIdentificador)
                .IsRequired()
                .HasMaxLength(6);

            this.Property(t => t.flgTipoProduto)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.CodTempoMod)
                .IsRequired()
                .HasMaxLength(40);

            this.Property(t => t.flgAtivo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("tbl_CBTempoModelo");
            this.Property(t => t.Id).HasColumnName("ID");
            this.Property(t => t.CodIdentificador).HasColumnName("CodIdentificador");
            this.Property(t => t.DatVigenciaIni).HasColumnName("DatVigenciaIni");
            this.Property(t => t.DatVigenciaFim).HasColumnName("DatVigenciaFim");
            this.Property(t => t.flgTipoProduto).HasColumnName("flgTipoProduto");
            this.Property(t => t.CodTempoMod).HasColumnName("CodTempoMod");
            this.Property(t => t.ValTempoPadrao).HasColumnName("ValTempoPadrao");
            this.Property(t => t.ValTempoMontagem).HasColumnName("ValTempoMontagem");
            this.Property(t => t.flgAtivo).HasColumnName("flgAtivo");
            this.Property(t => t.ValPrepPlaca).HasColumnName("ValPrepPlaca");
            this.Property(t => t.ValWakeUp).HasColumnName("ValWakeUp");
            this.Property(t => t.ValRunIn).HasColumnName("ValRunIn");
            this.Property(t => t.ValTesteFinal).HasColumnName("ValTesteFinal");
            this.Property(t => t.ValDownload).HasColumnName("ValDownload");
            this.Property(t => t.ValPrepKits).HasColumnName("ValPrepKits");
            this.Property(t => t.ValEmbalagem).HasColumnName("ValEmbalagem");
            this.Property(t => t.ValSerigrafiaGab).HasColumnName("ValSerigrafiaGab");
            this.Property(t => t.ValTampografiaFront).HasColumnName("ValTampografiaFront");
            this.Property(t => t.ValTampografiaLCD).HasColumnName("ValTampografiaLCD");
            this.Property(t => t.ValTampografiaCover).HasColumnName("ValTampografiaCover");
            this.Property(t => t.ValTampografiaBackCover).HasColumnName("ValTampografiaBackCover");
            this.Property(t => t.ValMontagemGabinete).HasColumnName("ValMontagemGabinete");
            this.Property(t => t.ValMontagemFrontal).HasColumnName("ValMontagemFrontal");
            this.Property(t => t.ValMontagemFonte).HasColumnName("ValMontagemFonte");
            this.Property(t => t.ValMontagemLCD).HasColumnName("ValMontagemLCD");
            this.Property(t => t.ValPreTeste).HasColumnName("ValPreTeste");
            this.Property(t => t.ValCalibGSensor).HasColumnName("ValCalibGSensor");
        }
    }
}
