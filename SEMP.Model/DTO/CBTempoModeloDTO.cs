using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEMP.Model.DTO
{    
    [Table("tbl_CBTempoModelo")]
    public partial class CBTempoModeloDTO
    {
        [Key]        
        public Nullable<int> Id { get; set; }

        [Required(ErrorMessage = "Digite o Identificador")]
        [Display(Name = "Identificador")]
        public string CodIdentificador { get; set; }
                
        [Required(ErrorMessage = "Digite a data inicial de vig�ncia")]
        [Display(Name = "Vig�ncia Inicial")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime DatVigenciaIni { get; set; }

        [Required(ErrorMessage = "Digite a data final de vig�ncia")]
        [Display(Name = "Vig�ncia Final")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime DatVigenciaFim { get; set; }

        [Required(ErrorMessage = "Selecione o Tipo do Produto. N: Notebook, TAB: Tablet")]
        [Display(Name = "Tipo Produto")]        
        public string flgTipoProduto { get; set; }

        
        //private string myVar;
        //[Required(ErrorMessage = "Selecione o Tipo do Produto. N: Notebook, TAB: Tablet")]
        //[Display(Name = "Tipo Produto")]
        //public string flgTipoProduto
        //{
        //    get { return myVar.ToString().Trim(); }
        //    set { myVar = value; }
        //}
        

        [Required(ErrorMessage = "Digite o c�digo do modelo")]
        [Display(Name = "Descri��o")]
        public string CodTempoMod { get; set; }

        [Display(Name = "Valor Tempo Padr�o")]
        [DisplayFormat(DataFormatString = "{0:00.00}", ApplyFormatInEditMode = true, NullDisplayText = "0.00m")]
        public Nullable<decimal> ValTempoPadrao { get; set; }

        [Required(ErrorMessage = "Digite o tempo de montagem")]
        [DisplayFormat(DataFormatString = "{0:00.00}", ApplyFormatInEditMode = true, NullDisplayText = "0.00m")]
        public decimal ValTempoMontagem { get; set; }

        [Required(ErrorMessage = "Marque o item como ativo ou inativo")]
        [Display(Name = "Ativo/Inativo")]
        public string flgAtivo { get; set; }

        [DisplayFormat(DataFormatString = "{0:00.00}", ApplyFormatInEditMode = true, NullDisplayText = "0.00m")]
        [Display(Name = "Prepara��o de placas")]
        public Nullable<decimal> ValPrepPlaca { get; set; }

        [Display(Name = "Wake-Up")]
        [DisplayFormat(DataFormatString = "{0:00.00}", ApplyFormatInEditMode = true, NullDisplayText = "0.00m")]
        public Nullable<decimal> ValWakeUp { get; set; }

        [Display(Name = "Run-In")]
        [DisplayFormat(DataFormatString = "{0:00.00}", ApplyFormatInEditMode = true, NullDisplayText = "0.00m")]
        public Nullable<decimal> ValRunIn { get; set; }

        [Display(Name = "Teste Final")]
        [DisplayFormat(DataFormatString = "{0:00.00}", ApplyFormatInEditMode = true, NullDisplayText = "0.00m")]
        public Nullable<decimal> ValTesteFinal { get; set; }
        
        [Display(Name = "Download")]
        [DisplayFormat(DataFormatString = "{0:00.00}", ApplyFormatInEditMode = true, NullDisplayText = "0.00m")]
        public Nullable<decimal> ValDownload { get; set; }
        
        [Display(Name = "Prepara��o de Kits")]
        [DisplayFormat(DataFormatString = "{0:00.00}", ApplyFormatInEditMode = true, NullDisplayText = "0.00m")]
        public Nullable<decimal> ValPrepKits { get; set; }

        [Display(Name = "Embalagem")]
        [DisplayFormat(DataFormatString = "{0:00.00}", ApplyFormatInEditMode = true, NullDisplayText = "0.00m")]
        public Nullable<decimal> ValEmbalagem { get; set; }
        
        [Display(Name="Serigrafia do Gabinete")]
        [DisplayFormat(DataFormatString = "{0:00.00}", ApplyFormatInEditMode = true, NullDisplayText = "0.00m")]
        public Nullable<decimal> ValSerigrafiaGab { get; set; }

        [Display(Name = "Tampografia Frontal/T. Plastica")]
        [DisplayFormat(DataFormatString = "{0:00.00}", ApplyFormatInEditMode = true, NullDisplayText = "0.00m")]
        public Nullable<decimal> ValTampografiaFront { get; set; }

        [Display(Name = "Tampografia do LCD/Touch")]
        [DisplayFormat(DataFormatString = "{0:00.00}", ApplyFormatInEditMode = true, NullDisplayText = "0.00m")]
        public Nullable<decimal> ValTampografiaLCD { get; set; }

        [Display(Name = "Tampografia do Cover C")]
        [DisplayFormat(DataFormatString = "{0:00.00}", ApplyFormatInEditMode = true, NullDisplayText = "0.00m")]
        public Nullable<decimal> ValTampografiaCover { get; set; }

        [Display(Name = "Tampografia do Back Cover")]
        [DisplayFormat(DataFormatString = "{0:00.00}", ApplyFormatInEditMode = true, NullDisplayText = "0.00m")]
        public Nullable<decimal> ValTampografiaBackCover { get; set; }

        [Display(Name = "Montagem do Gabinete")]
        [DisplayFormat(DataFormatString = "{0:00.00}", ApplyFormatInEditMode = true, NullDisplayText = "0.00m")]
        public Nullable<decimal> ValMontagemGabinete { get; set; }

        [Display(Name = "Montagem do Frontal")]
        [DisplayFormat(DataFormatString = "{0:00.00}", ApplyFormatInEditMode = true, NullDisplayText = "0.00m")]
        public Nullable<decimal> ValMontagemFrontal { get; set; }

        [Display(Name = "Montagem da Fonte")]
        [DisplayFormat(DataFormatString = "{0:00.00}", ApplyFormatInEditMode = true, NullDisplayText = "0.00m")]
        public Nullable<decimal> ValMontagemFonte { get; set; }
        
        [Display(Name = "Montagem do LCD")]
        [DisplayFormat(DataFormatString = "{0:00.00}", ApplyFormatInEditMode = true, NullDisplayText = "0.00m")]
        public Nullable<decimal> ValMontagemLCD { get; set; }

        [Display(Name = "Pr�-Teste")]
        [DisplayFormat(DataFormatString = "{0:00.00}", ApplyFormatInEditMode = true, NullDisplayText = "0.00m")]
        public Nullable<decimal> ValPreTeste { get; set; }

        [Display(Name = "Calibra��o G-Sensor")]
        [DisplayFormat(DataFormatString = "{0:00.00}", ApplyFormatInEditMode = true, NullDisplayText = "0.00m")]
        public Nullable<decimal> ValCalibGSensor { get; set; }

        [NotMapped]
        [Display(Name = "Total Tampografia")]
        [DisplayFormat(DataFormatString = "{0:00.00}", NullDisplayText = "0.00m")]
        public Nullable<decimal> ValorTotalTampografica 
        { 
            get{
                return (ValSerigrafiaGab + ValTampografiaFront + ValTampografiaLCD + ValTampografiaCover + ValTampografiaBackCover);
               }
            set { }
        }
        
        [NotMapped]
        [Display(Name = "Total Gabinete")]
        [DisplayFormat(DataFormatString = "{0:00.00}", NullDisplayText = "0.00m")]
        public Nullable<decimal> ValorTotalGabinete 
        { 
            get{
                return (ValMontagemGabinete + ValMontagemFrontal + ValMontagemFonte + ValMontagemLCD);
               }
            set{} 
        }

        [NotMapped]
        [Display(Name = "TP Montagem(Front-end)")]
        [DisplayFormat(DataFormatString = "{0:00.00}", NullDisplayText = "0.00m")]
        public Nullable<decimal> ValorTotalTempoMontagem { get { return ValTempoMontagem; } set { } }

        [NotMapped]
        [Display(Name = "TP Outros (Back-end)")]
        [DisplayFormat(DataFormatString = "{0:00.00}", NullDisplayText = "0.00m")]
        public Nullable<decimal> ValorTotalTempoOutros 
        { 
            get{
                return ValPrepPlaca + ValPreTeste + ValWakeUp + ValRunIn + ValCalibGSensor + ValTesteFinal + ValDownload + ValPrepKits + ValEmbalagem; 
               } 
            set{} 
        }

        [NotMapped]
        [Display(Name = "Tempo Padr�o (Front + Back)")]
        [DisplayFormat(DataFormatString = "{0:00.00}", NullDisplayText = "0.00m")]
        public Nullable<decimal> ValorTotalTempoPadrao 
        { 
            get {
                return ValorTotalTempoOutros + ValorTotalTempoMontagem;
            }
            set { } 
        }

        [NotMapped]
        [Display(Name = "Tempo Total")]
        [DisplayFormat(DataFormatString = "{0:00.00}", NullDisplayText = "0.00m")]
        public Nullable<decimal> ValorTotalTempoTotal 
        { 
            get{
                return ValorTotalTempoPadrao + ValorTotalGabinete + ValorTotalTampografica;
            }
            set { } 
        }
        
        [NotMapped]
        [Display(Name = "Produto")]
        public string DescricaoProduto
        {
            get
            {                
                return flgTipoProduto.Trim() == "D" ? "Desktop" :
                       flgTipoProduto.Trim() == "N" ? "Notebook" :
                       flgTipoProduto.Trim() == "TAB" ? "Tablet" :
                       flgTipoProduto.Trim() == "GAB" ? "Gabinete" :
                       flgTipoProduto.Trim() == "TAM" ? "Tampografia" :
                       flgTipoProduto.Trim() == "M" ? "Media PC" : flgTipoProduto;
            }
            
        }
    }
}
