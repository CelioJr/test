namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_ft_sub
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_ft_sub()
        {
            dw_ft_subparam = new HashSet<dw_ft_subparam>();
            dw_passtfse = new HashSet<dw_passtfse>();
            dw_testesub = new HashSet<dw_testesub>();
        }

        [Key]
        public long id_ft_sub { get; set; }

        [StringLength(100)]
        public string ds_sub { get; set; }

        public long id_ft_etapa { get; set; }

        public short? tp_ft_sub { get; set; }

        public short? tp_status { get; set; }

        public int? ms { get; set; }

        public int? x41 { get; set; }

        public int? x42 { get; set; }

        public decimal? y81 { get; set; }

        public decimal? y82 { get; set; }

        public decimal? y83 { get; set; }

        public decimal? y84 { get; set; }

        public long? id_ft_param { get; set; }

        public short? tp_ft_param { get; set; }

        public decimal? vl_ft_param { get; set; }

        public short? st_ft_param { get; set; }

        public long? id_produto { get; set; }

        public int? x45 { get; set; }

        public int? x46 { get; set; }

        public int? x47 { get; set; }

        public int? x48 { get; set; }

        public int? x49 { get; set; }

        public virtual dw_ft_etapa dw_ft_etapa { get; set; }

        public virtual dw_ft_param dw_ft_param { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_ft_subparam> dw_ft_subparam { get; set; }

        public virtual om_produto om_produto { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_passtfse> dw_passtfse { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_testesub> dw_testesub { get; set; }
    }
}
