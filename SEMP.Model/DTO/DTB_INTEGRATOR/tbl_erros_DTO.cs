namespace SEMP.Model.DTO
{
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;

	public partial class tbl_erros_DTO
	{
        public int ID { get; set; }

        [Required]
        [StringLength(5)]
        public string CODIGO { get; set; }

        [Required]
        [StringLength(150)]
        public string TEXTO { get; set; }
    }
}
