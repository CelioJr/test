using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_SISTEMA_CATEGORIA_DTO
    {
        public string SIS_CODIGO { get; set; }
        public int CAT_CODIGO { get; set; }
        public virtual FR_CATEGORIA_DTO FR_CATEGORIA { get; set; }
    }
}
