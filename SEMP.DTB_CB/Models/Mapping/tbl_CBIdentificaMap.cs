using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DTB_CB.Models.Mapping
{
    public class tbl_CBIdentificaMap : EntityTypeConfiguration<tbl_CBIdentifica>
    {
        public tbl_CBIdentificaMap()
        {
            // Primary Key
            this.HasKey(t => t.NumECB);

            // Properties
            this.Property(t => t.NumECB)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.CodPlacaIAC)
                .IsFixedLength()
                .HasMaxLength(6);

            this.Property(t => t.CodPlacaIMC)
                .IsFixedLength()
                .HasMaxLength(6);

            this.Property(t => t.CodModelo)
                .IsFixedLength()
                .HasMaxLength(6);

            this.Property(t => t.CodLinhaIAC)
                .IsFixedLength()
                .HasMaxLength(8);

            this.Property(t => t.CodLinhaIMC)
                .IsFixedLength()
                .HasMaxLength(8);

            this.Property(t => t.CodLinhaFEC)
                .IsFixedLength()
                .HasMaxLength(8);

            this.Property(t => t.MacAddress)
                .IsFixedLength()
                .HasMaxLength(17);

            // Table & Column Mappings
            this.ToTable("tbl_CBIdentifica");
            this.Property(t => t.NumECB).HasColumnName("NumECB");
            this.Property(t => t.CodPlacaIAC).HasColumnName("CodPlacaIAC");
            this.Property(t => t.CodPlacaIMC).HasColumnName("CodPlacaIMC");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.DatIdentifica).HasColumnName("DatIdentifica");
            this.Property(t => t.DatIMC).HasColumnName("DatIMC");
            this.Property(t => t.DatModelo).HasColumnName("DatModelo");
            this.Property(t => t.CodLinhaIAC).HasColumnName("CodLinhaIAC");
            this.Property(t => t.CodLinhaIMC).HasColumnName("CodLinhaIMC");
            this.Property(t => t.CodLinhaFEC).HasColumnName("CodLinhaFEC");
            this.Property(t => t.MacAddress).HasColumnName("MacAddress");
        }
    }
}
