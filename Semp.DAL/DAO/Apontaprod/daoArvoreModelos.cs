﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using SEMP.DAL.Models;
using SEMP.Model.DTO;

namespace SEMP.DAL.DAO.Rastreabilidade
{
	public class daoArvoreModelos
	{

		public static decimal ObterProgramaModelo(string CodModelo, DateTime DatProducao)
		{

			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{

				var modelo = (from prog in dtbSim.tbl_ProgDiaJit
							  where prog.CodModelo == CodModelo &&
									  prog.DatProducao.Year == DatProducao.Year &&
									  prog.DatProducao.Month == DatProducao.Month
							  group prog by prog.CodModelo into g
							  select new { QtdPrograma = g.Sum(x => x.QtdProducao ?? x.QtdProdPm) }).FirstOrDefault();

				return modelo.QtdPrograma ?? 0;
			}
		}

		public static ProdDiaDTO ObterProducaoModelo(string CodModelo, DateTime DatProducao)
		{

			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{

				var modelo = (from prog in dtbSim.tbl_ProdDia
							  where prog.CodModelo == CodModelo &&
									  prog.DatProducao.Year == DatProducao.Year &&
									  prog.DatProducao.Month == DatProducao.Month
							  group prog by prog.CodModelo into g
							  select new ProdDiaDTO
							  {
								  CodModelo = g.Key,
								  QtdProduzida = g.Sum(x => x.QtdProduzida),
								  QtdApontada = g.Sum(x => x.QtdApontada)
							  }).FirstOrDefault();

				return modelo;
			}
		}

		public static decimal ObterProgramaPlaca(string CodModelo, DateTime DatProducao)
		{

			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{

				var modelo = (from prog in dtbSim.tbl_ProgDiaPlaca
							  where prog.CodPlaca == CodModelo &&
									  prog.DatProducao.Year == DatProducao.Year &&
									  prog.DatProducao.Month == DatProducao.Month
							  group prog by prog.CodModelo into g
							  select new { QtdPrograma = g.Sum(x => x.QtdProdPM) }).FirstOrDefault();
				if (modelo != null) {
					return modelo.QtdPrograma;
				}
				else{
					return 0;
				}
			}
		}

		public static ProdDiaPlacaDTO ObterProducaoPlaca(string CodModelo, DateTime DatProducao)
		{

			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{

				var modelo = (from prog in dtbSim.tbl_ProdDiaPlaca
							  where prog.CodPlaca == CodModelo &&
									  prog.DatProducao.Year == DatProducao.Year &&
									  prog.DatProducao.Month == DatProducao.Month
							  group prog by prog.CodPlaca into g
							  select new ProdDiaPlacaDTO
							  {
								  CodPlaca = g.Key,
								  QtdProduzida = g.Sum(x => x.QtdProduzida ?? 0),
								  QtdApontada = g.Sum(x => x.QtdApontada)
							  }).FirstOrDefault();

				return modelo;
			}
		}
		
		public static List<PlanoItemDTO> ObterItensModelo(string CodModelo)
		{

			using (var dtbSim = new DTB_SIMContext())
			{

				var itens = (from i in dtbSim.tbl_PlanoItem
							 join modelo in dtbSim.viw_PlanoItemModelo on i.CodItem equals modelo.CodModelo
							 from pcp in dtbSim.tbl_ItemPCP.Where(p => p.CodItem == i.CodItem)
							 //from prog in dtbSim.tbl_ProgProcesso.Where(p => i.CodProcesso == p.CodProcesso).DefaultIfEmpty()
							 //from plano in dtbSim.tbl_PlanoProcesso.Where(p => i.CodProcesso == p.CodProcesso).DefaultIfEmpty()
							 where i.CodModelo == CodModelo
									&& pcp.MovEstoque == "S" 
									&& (pcp.CodProcesso.StartsWith("01") || pcp.CodProcesso.StartsWith("02"))
									//&& (new string[]{"M", "I", "T", "1", "2"}.Contains(prog.CodTipo) || plano.CodAP == "10")
							 select new PlanoItemDTO() {
								CodItem = i.CodItem,
								CodProcesso = pcp.CodProcesso //plano.CodAP == "10" ? plano.CodAP : (prog.CodTipo ?? plano.CodAP)
							 }).Distinct();


				return itens.ToList();
			}

		}

	}
}
