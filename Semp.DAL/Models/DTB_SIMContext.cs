using System;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Reflection;
using CodeFirstStoreFunctions;
using SEMP.DAL.Models.Mapping;
using SEMP.Model.DTO;
using SEMP.Model;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace SEMP.DAL.Models
{
	public partial class DTB_SIMContext : DbContext
	{
		static DTB_SIMContext()
		{
			Database.SetInitializer<DTB_SIMContext>(null);
		}

#if DEBUG
		public DTB_SIMContext()
			: base(SEMP.Model.Constantes.IsOldFab ? "Name=DTB_SIMContextDEBUG" : "Name=DTB_SEMPContextDEBUG")
		{
			Constantes.ConexaoString = this.Database.Connection.ConnectionString.Substring(0, this.Database.Connection.ConnectionString.IndexOf(";Persist"));
		}
#else
		public DTB_SIMContext()
			//: base("Name=DTB_SIMContext")
			: base(SEMP.Model.Constantes.IsOldFab ? "Name=DTB_SIMContext" : "Name=DTB_SEMPContext")
		{
			Constantes.ConexaoString = this.Database.Connection.ConnectionString.Substring(0, this.Database.Connection.ConnectionString.IndexOf(";Persist"));
		}
#endif

		public DbSet<tbl_AcessoMao> tbl_AcessoMao { get; set; }
		public DbSet<tbl_BarrasLinha> tbl_BarrasLinha { get; set; }
		public DbSet<tbl_BarrasProducao> tbl_BarrasProducao { get; set; }
		public DbSet<tbl_BarrasProducaoH> tbl_BarrasProducaoH { get; set; }
		public DbSet<tbl_BarrasProducaoItem> tbl_BarrasProducaoItem { get; set; }
		public DbSet<tbl_BarrasProducaoPlaca> tbl_BarrasProducaoPlaca { get; set; }
		public DbSet<tbl_BarrasProducaoPlacaH> tbl_BarrasProducaoPlacaH { get; set; }
		public DbSet<tbl_CapacLinha> tbl_CapacLinha { get; set; }
		public DbSet<tbl_CapacMdoLinha> tbl_CapacMdoLinha { get; set; }
		public DbSet<tbl_CnqCausa> tbl_CnqCausa { get; set; }
		public DbSet<tbl_CnqCustoRet> tbl_CnqCustoRet { get; set; }
		public DbSet<tbl_CnqDefeito> tbl_CnqDefeito { get; set; }
		public DbSet<tbl_CnqFamilia> tbl_CnqFamilia { get; set; }
		public DbSet<tbl_CNQMaoObra> tbl_CNQMaoObra { get; set; }
		public DbSet<tbl_CnqOrigem> tbl_CnqOrigem { get; set; }
		public DbSet<tbl_DCREstoque> tbl_DCREstoque { get; set; }
		public DbSet<tbl_Departamento> tbl_Departamento { get; set; }
		public DbSet<tbl_EntMov> tbl_EntMov { get; set; }
		public DbSet<tbl_EntMovAprov> tbl_EntMovAprov { get; set; }
		public DbSet<tbl_EntMovItem> tbl_EntMovItem { get; set; }
		public DbSet<tbl_EntMovRec> tbl_EntMovRec { get; set; }
		public DbSet<tbl_EPIATMovimentacao> tbl_EPIATMovimentacao { get; set; }
		public DbSet<tbl_Estoque> tbl_Estoque { get; set; }
		public DbSet<tbl_EstoquePCI> tbl_EstoquePCI { get; set; }
		public DbSet<tbl_Fabrica> tbl_Fabrica { get; set; }
		public DbSet<tbl_Familia> tbl_Familia { get; set; }
		public DbSet<tbl_GpaArea> tbl_GpaArea { get; set; }
		public DbSet<tbl_gpaboletos> tbl_gpaboletos { get; set; }
		public DbSet<tbl_GpaLocalArea> tbl_GpaLocalArea { get; set; }
		public DbSet<tbl_GpaMovimentacao> tbl_GpaMovimentacao { get; set; }
		public DbSet<tbl_GPANota> tbl_GPANota { get; set; }
		public DbSet<tbl_GPANotaItem> tbl_GPANotaItem { get; set; }
		public DbSet<TBL_GpaRecItem> TBL_GpaRecItem { get; set; }
		public DbSet<TBL_GpaRecLinha> TBL_GpaRecLinha { get; set; }
		public DbSet<TBL_GpaRecPallet> TBL_GpaRecPallet { get; set; }
		public DbSet<tbl_IAMCDiferenca> tbl_IAMCDiferenca { get; set; }
		public DbSet<tbl_IAMCFamilia> tbl_IAMCFamilia { get; set; }
		public DbSet<tbl_Item> tbl_Item { get; set; }
		public DbSet<tbl_ItemPCP> tbl_ItemPCP { get; set; }
		public DbSet<tbl_KcmNac> tbl_KcmNac { get; set; }
		public DbSet<tbl_Linha> tbl_Linha { get; set; }
		public DbSet<tbl_LinhaTurno> tbl_LinhaTurno { get; set; }
		public DbSet<tbl_Local> tbl_Local { get; set; }
		public DbSet<tbl_LSAAP> tbl_LSAAP { get; set; }
		public DbSet<tbl_LSAAPItem> tbl_LSAAPItem { get; set; }
		public DbSet<tbl_LSAAPItemPosicao> tbl_LSAAPItemPosicao { get; set; }
		public DbSet<tbl_LSAAPModelo> tbl_LSAAPModelo { get; set; }
		public DbSet<tbl_LSAAT> tbl_LSAAT { get; set; }
		public DbSet<tbl_LSAATItem> tbl_LSAATItem { get; set; }
		public DbSet<tbl_LSAATNota> tbl_LSAATNota { get; set; }
		public DbSet<tbl_LSAATNotaRodape> tbl_LSAATNotaRodape { get; set; }
		public DbSet<tbl_MAPScrapBaixa> tbl_MAPScrapBaixa { get; set; }
		public DbSet<tbl_MensagemPainel> tbl_MensagemPainel { get; set; }
		public DbSet<tbl_Movimentacao> tbl_Movimentacao { get; set; }
		public DbSet<tbl_Parada> tbl_Parada { get; set; }
		public DbSet<tbl_Plano> tbl_Plano { get; set; }
		public DbSet<tbl_PlanoAlt> tbl_PlanoAlt { get; set; }
		public DbSet<tbl_PlanoEtapa> tbl_PlanoEtapa { get; set; }
		public DbSet<tbl_PlanoEtapaMaq> tbl_PlanoEtapaMaq { get; set; }
		public DbSet<tbl_PlanoIAC> tbl_PlanoIAC { get; set; }
		public DbSet<tbl_PlanoIACItem> tbl_PlanoIACItem { get; set; }
		public DbSet<tbl_PlanoItem> tbl_PlanoItem { get; set; }
		public DbSet<tbl_PlanoProcesso> tbl_PlanoProcesso { get; set; }
		public DbSet<tbl_ProdAmplitude> tbl_ProdAmplitude { get; set; }
		public DbSet<tbl_ProdCin> tbl_ProdCin { get; set; }
		public DbSet<tbl_ProdDia> tbl_ProdDia { get; set; }
		public DbSet<tbl_ProdDiaPlaca> tbl_ProdDiaPlaca { get; set; }
		public DbSet<tbl_ProdModelo> tbl_ProdModelo { get; set; }
		public DbSet<tbl_ProdParada> tbl_ProdParada { get; set; }
		public DbSet<tbl_ProdParadaDep> tbl_ProdParadaDep { get; set; }
		public DbSet<tbl_ProdParadaDepItem> tbl_ProdParadaDepItem { get; set; }
		public DbSet<tbl_ProdParadaObs> tbl_ProdParadaObs { get; set; }
		public DbSet<tbl_ProdReferencia> tbl_ProdReferencia { get; set; }
		public DbSet<tbl_ProgDiaJit> tbl_ProgDiaJit { get; set; }
		public DbSet<tbl_ProgDiaJitColeta> tbl_ProgDiaJitColeta { get; set; }
		public DbSet<tbl_ProgDiaPlaca> tbl_ProgDiaPlaca { get; set; }
		public DbSet<tbl_ProgMesPlaca> tbl_ProgMesPlaca { get; set; }
		public DbSet<tbl_ProgMesResumo> tbl_ProgMesResumo { get; set; }
		public DbSet<tbl_ProgMesVPE> tbl_ProgMesVPE { get; set; }
		public DbSet<tbl_ProgProcesso> tbl_ProgProcesso { get; set; }
		public DbSet<tbl_RIFuncionario> tbl_RIFuncionario { get; set; }
		public DbSet<tbl_RIJornada> tbl_RIJornada { get; set; }
		public DbSet<tbl_RITurno> tbl_RITurno { get; set; }
		public DbSet<tbl_RMA> tbl_RMA { get; set; }
		public DbSet<tbl_RPA> tbl_RPA { get; set; }
		public DbSet<tbl_rpaitem> tbl_rpaitem { get; set; }
		public DbSet<tbl_RPAMotivo> tbl_RPAMotivo { get; set; }
		public DbSet<tbl_RPE> tbl_RPE { get; set; }
		public DbSet<tbl_Supervisor> tbl_Supervisor { get; set; }
		public DbSet<tbl_TempoPadrao> tbl_TempoPadrao { get; set; }
		public DbSet<tbl_Usuario> tbl_Usuario { get; set; }
		public DbSet<tbl_Validacao> tbl_Validacao { get; set; }
		public DbSet<tbl_WIPProducao> tbl_WIPProducao { get; set; }
		public DbSet<viw_BarrasProducao> viw_BarrasProducao { get; set; }
		public DbSet<viw_BarrasProducaoPlaca> viw_BarrasProducaoPlaca { get; set; }
		public DbSet<viw_BarrasSAPContingencia> viw_BarrasSAPContingencia { get; set; }
		public DbSet<viw_PlanoItemModelo> viw_PlanoItemModelo { get; set; }
		public DbSet<tbl_Retrabalho> tbl_Retrabalho { get; set; }
		public DbSet<viw_RelOrdemServicoSTS> viw_RelOrdemServicoSTS { get; set; }

		//public DbSet<MAPScrapBaixaDTO> SPC_MAPGeraSCRAPs { get; set; }


		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Configurations.Add(new tbl_AcessoMaoMap());
			modelBuilder.Configurations.Add(new tbl_BarrasLinhaMap());
			modelBuilder.Configurations.Add(new tbl_BarrasProducaoMap());
			modelBuilder.Configurations.Add(new tbl_BarrasProducaoHMap());
			modelBuilder.Configurations.Add(new tbl_BarrasProducaoItemMap());
			modelBuilder.Configurations.Add(new tbl_BarrasProducaoPlacaMap());
			modelBuilder.Configurations.Add(new tbl_BarrasProducaoPlacaHMap());
			modelBuilder.Configurations.Add(new tbl_CapacLinhaMap());
			modelBuilder.Configurations.Add(new tbl_CapacMdoLinhaMap());
			modelBuilder.Configurations.Add(new tbl_CnqCausaMap());
			modelBuilder.Configurations.Add(new tbl_CnqCustoRetMap());
			modelBuilder.Configurations.Add(new tbl_CnqDefeitoMap());
			modelBuilder.Configurations.Add(new tbl_CnqFamiliaMap());
			modelBuilder.Configurations.Add(new tbl_CNQMaoObraMap());
			modelBuilder.Configurations.Add(new tbl_CnqOrigemMap());
			modelBuilder.Configurations.Add(new tbl_DCREstoqueMap());
			modelBuilder.Configurations.Add(new tbl_DepartamentoMap());
			modelBuilder.Configurations.Add(new tbl_EntMovMap());
			modelBuilder.Configurations.Add(new tbl_EntMovAprovMap());
			modelBuilder.Configurations.Add(new tbl_EntMovItemMap());
			modelBuilder.Configurations.Add(new tbl_EntMovRecMap());
			modelBuilder.Configurations.Add(new tbl_EPIATMovimentacaoMap());
			modelBuilder.Configurations.Add(new tbl_EstoqueMap());
			modelBuilder.Configurations.Add(new tbl_EstoquePCIMap());
			modelBuilder.Configurations.Add(new tbl_FabricaMap());
			modelBuilder.Configurations.Add(new tbl_FamiliaMap());
			modelBuilder.Configurations.Add(new tbl_GpaAreaMap());
			modelBuilder.Configurations.Add(new tbl_gpaboletosMap());
			modelBuilder.Configurations.Add(new tbl_GpaLocalAreaMap());
			modelBuilder.Configurations.Add(new tbl_GpaMovimentacaoMap());
			modelBuilder.Configurations.Add(new tbl_GPANotaMap());
			modelBuilder.Configurations.Add(new tbl_GPANotaItemMap());
			modelBuilder.Configurations.Add(new TBL_GpaRecItemMap());
			modelBuilder.Configurations.Add(new TBL_GpaRecLinhaMap());
			modelBuilder.Configurations.Add(new TBL_GpaRecPalletMap());
			modelBuilder.Configurations.Add(new tbl_IAMCDiferencaMap());
			modelBuilder.Configurations.Add(new tbl_IAMCFamiliaMap());
			modelBuilder.Configurations.Add(new tbl_ItemMap());
			modelBuilder.Configurations.Add(new tbl_ItemPCPMap());
			modelBuilder.Configurations.Add(new tbl_KcmNacMap());
			modelBuilder.Configurations.Add(new tbl_LinhaMap());
			modelBuilder.Configurations.Add(new tbl_LinhaTurnoMap());
			modelBuilder.Configurations.Add(new tbl_LocalMap());
			modelBuilder.Configurations.Add(new tbl_LSAAPMap());
			modelBuilder.Configurations.Add(new tbl_LSAAPItemMap());
			modelBuilder.Configurations.Add(new tbl_LSAAPItemPosicaoMap());
			modelBuilder.Configurations.Add(new tbl_LSAAPModeloMap());
			modelBuilder.Configurations.Add(new tbl_LSAATMap());
			modelBuilder.Configurations.Add(new tbl_LSAATItemMap());
			modelBuilder.Configurations.Add(new tbl_LSAATNotaMap());
			modelBuilder.Configurations.Add(new tbl_LSAATNotaRodapeMap());
			modelBuilder.Configurations.Add(new tbl_MAPScrapBaixaMap());
			modelBuilder.Configurations.Add(new tbl_MensagemPainelMap());
			modelBuilder.Configurations.Add(new tbl_MovimentacaoMap());
			modelBuilder.Configurations.Add(new tbl_ParadaMap());
			modelBuilder.Configurations.Add(new tbl_PlanoMap());
			modelBuilder.Configurations.Add(new tbl_PlanoAltMap());
			modelBuilder.Configurations.Add(new tbl_PlanoEtapaMap());
			modelBuilder.Configurations.Add(new tbl_PlanoEtapaMaqMap());
			modelBuilder.Configurations.Add(new tbl_PlanoIACMap());
			modelBuilder.Configurations.Add(new tbl_PlanoIACItemMap());
			modelBuilder.Configurations.Add(new tbl_PlanoItemMap());
			modelBuilder.Configurations.Add(new tbl_PlanoProcessoMap());
			modelBuilder.Configurations.Add(new tbl_ProdAmplitudeMap());
			modelBuilder.Configurations.Add(new tbl_ProdCinMap());
			modelBuilder.Configurations.Add(new tbl_ProdDiaMap());
			modelBuilder.Configurations.Add(new tbl_ProdDiaPlacaMap());
			modelBuilder.Configurations.Add(new tbl_ProdModeloMap());
			modelBuilder.Configurations.Add(new tbl_ProdParadaMap());
			modelBuilder.Configurations.Add(new tbl_ProdParadaDepMap());
			modelBuilder.Configurations.Add(new tbl_ProdParadaDepItemMap());
			modelBuilder.Configurations.Add(new tbl_ProdParadaObsMap());
			modelBuilder.Configurations.Add(new tbl_ProdReferenciaMap());
			modelBuilder.Configurations.Add(new tbl_ProgDiaJitMap());
			modelBuilder.Configurations.Add(new tbl_ProgDiaJitColetaMap());
			modelBuilder.Configurations.Add(new tbl_ProgDiaPlacaMap());
			modelBuilder.Configurations.Add(new tbl_ProgMesPlacaMap());
			modelBuilder.Configurations.Add(new tbl_ProgMesResumoMap());
			modelBuilder.Configurations.Add(new tbl_ProgMesVPEMap());
			modelBuilder.Configurations.Add(new tbl_ProgProcessoMap());
			modelBuilder.Configurations.Add(new tbl_RIFuncionarioMap());
			modelBuilder.Configurations.Add(new tbl_RIJornadaMap());
			modelBuilder.Configurations.Add(new tbl_RITurnoMap());
			modelBuilder.Configurations.Add(new tbl_RMAMap());
			modelBuilder.Configurations.Add(new tbl_RPAMap());
			modelBuilder.Configurations.Add(new tbl_rpaitemMap());
			modelBuilder.Configurations.Add(new tbl_RPAMotivoMap());
			modelBuilder.Configurations.Add(new tbl_RPEMap());
			modelBuilder.Configurations.Add(new tbl_SupervisorMap());
			modelBuilder.Configurations.Add(new tbl_TempoPadraoMap());
			modelBuilder.Configurations.Add(new tbl_UsuarioMap());
			modelBuilder.Configurations.Add(new tbl_ValidacaoMap());
			modelBuilder.Configurations.Add(new tbl_WIPProducaoMap());
			modelBuilder.Configurations.Add(new viw_BarrasProducaoMap());
			modelBuilder.Configurations.Add(new viw_BarrasProducaoPlacaMap());
			modelBuilder.Configurations.Add(new viw_BarrasSAPContingenciaMap());
			modelBuilder.Configurations.Add(new viw_PlanoItemModeloMap());
			modelBuilder.Configurations.Add(new tbl_RetrabalhoMap());
			modelBuilder.Configurations.Add(new viw_RelOrdemServicoSTSMap());

			modelBuilder.Conventions.Add(new FunctionsConvention<DTB_SIMContext>("dbo"));

			modelBuilder.Entity<tbl_TempoPadrao>().Property(x => x.TempoPadrao).HasPrecision(12, 4);
		}

		/*public ObjectResult<MAPScrapBaixaDTO> SPC_MAPGeraSCRAP(DateTime datInicio, DateTime datFim)
		{
			try
			{
				var dataIni = new ObjectParameter("pDatRefInicio", datInicio);
				var dataFim = new ObjectParameter("pDatRefFinal", datFim);

				return ((IObjectContextAdapter)this).ObjectContext.
							ExecuteFunction<MAPScrapBaixaDTO>("SPC_MAPGeraSCRAP", dataIni, dataFim);
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}*/

		public ObjectResult<int> spc_BarrasQtdPontos(string codModelo, DateTime datCorte)
		{
			try
			{
				var pCodModelo = new ObjectParameter("Modelo", codModelo);
				var pDatCorte = new ObjectParameter("DatCorte", datCorte);

				return ((IObjectContextAdapter)this).ObjectContext.
							ExecuteFunction<int>("spc_BarrasQtdPontos", pCodModelo, pDatCorte);
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}

	}
}
