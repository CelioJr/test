﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.Models
{
	public partial class tbl_CBTrocaItem
	{
		public int CodRetrabalho { get; set; }
		public string CodItemAnterior { get; set; }
		public string CodItemNovo { get; set; }
		public Nullable<int> TipoAmarra { get; set; }
		public virtual tbl_CBRetrabalho tbl_CBRetrabalho { get; set; }
	}
}
