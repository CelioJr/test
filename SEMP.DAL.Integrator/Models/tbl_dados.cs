namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_dados
    {
        public int ID { get; set; }

        public int ID_TAREFA { get; set; }

        public int ID_ERRO { get; set; }

        public int ID_TIPO { get; set; }

        [Column(TypeName = "xml")]
        [Required]
        public string CARGA_XML { get; set; }

        public DateTime DATAEXEC { get; set; }

        public bool STATUS { get; set; }

        public virtual tbl_TAREFA tbl_TAREFA { get; set; }

        public virtual tbl_erros tbl_erros { get; set; }

        public virtual tbl_tipo tbl_tipo { get; set; }
    }
}
