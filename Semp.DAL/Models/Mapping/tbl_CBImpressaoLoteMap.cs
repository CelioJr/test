using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBImpressaoLoteMap : EntityTypeConfiguration<tbl_CBImpressaoLote>
    {
        public tbl_CBImpressaoLoteMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.NumLote)
                .IsRequired()
                .HasMaxLength(18)
                .IsUnicode(false);

            this.Property(t => t.CodDrt)
                .IsRequired()
                .HasMaxLength(8)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_CBImpressaoLote");
            this.Property(t => t.NumLote).HasColumnName("NumLote");
            this.Property(t => t.fase).HasColumnName("fase");
            this.Property(t => t.CodDrt).HasColumnName("CodDrt");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.DataImpressao).HasColumnName("DataImpressao");
            this.Property(t => t.id).HasColumnName("id");
        }
    }
}
