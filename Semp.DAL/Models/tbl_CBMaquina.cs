using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBMaquina
    {
        public int CodLinha { get; set; }
        public string CodMaquina { get; set; }
        public string DesMaquina { get; set; }
        //public virtual tbl_CBLinha tbl_CBLinha { get; set; }
    }
}
