namespace SEMP.Model.DTO
{
	using System.ComponentModel.DataAnnotations;

	public partial class ZCSD_INTERFACE_CAB_ORDEM_DTO
	{
        public int id { get; set; }

        [Required]
        [StringLength(16)]
        public string PACKNO { get; set; }

        [Required]
        [StringLength(10)]
        public string EXTROW { get; set; }

        [StringLength(12)]
        public string NUMEROPEDIDO { get; set; }

        [StringLength(22)]
        public string NUMEROPEDIDOSEMP { get; set; }

        [StringLength(12)]
        public string DATAPEDIDO { get; set; }

        [StringLength(6)]
        public string TIPOORDEM { get; set; }

        [StringLength(6)]
        public string MOTIVOORDEM { get; set; }

        [StringLength(6)]
        public string VENDEDOR { get; set; }

        [StringLength(8)]
        public string GERENTE { get; set; }

        [StringLength(12)]
        public string DIRETOR { get; set; }

        [StringLength(8)]
        public string CONDICAOPAGAMENTO { get; set; }

        [StringLength(3)]
        public string FORMAPAGAMENTO { get; set; }

        [StringLength(6)]
        public string PEDIDOBLOQUEADO { get; set; }

        [StringLength(14)]
        public string CODIGOCLIENTE { get; set; }

        [StringLength(20)]
        public string CNPJCLIENTE { get; set; }

        [StringLength(15)]
        public string CPFCLIENTE { get; set; }

        [StringLength(3)]
        public string CONSUMIDORFINAL { get; set; }

        [StringLength(10)]
        public string DATAENTREGA1 { get; set; }

        [StringLength(10)]
        public string DATAENTREGA2 { get; set; }

        [StringLength(8000)]
        public string TEXTOAPROVACAO { get; set; }

        [StringLength(8000)]
        public string TEXTOCREDITO { get; set; }

        [StringLength(8000)]
        public string TEXTOLOGISTICO { get; set; }

        [StringLength(6)]
        public string MODALIDADEFRETE { get; set; }

        [StringLength(20)]
        public string REFERENCIANF { get; set; }

        [StringLength(24)]
        public string REFERENCIAINVOICE { get; set; }
    }
}
