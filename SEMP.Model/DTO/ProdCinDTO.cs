using System;

namespace SEMP.Model.DTO
{
    public class ProdCinDTO
    {
        public System.DateTime DatBase { get; set; }
        public string CodCinescopio { get; set; }
        public Nullable<decimal> QtdPlanejada { get; set; }
        public Nullable<decimal> QtdProduzida { get; set; }
        public System.Guid rowguid { get; set; }
    }
}
