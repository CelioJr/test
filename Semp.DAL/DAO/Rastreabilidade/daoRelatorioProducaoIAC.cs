﻿using SEMP.DAL.Models;
using SEMP.Model.DTO;
using SEMP.Model.VO.CNQ;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.DAO.Rastreabilidade
{
    public class daoRelatorioProducaoIAC
    {
        public static List<IndicadorPostos> ObterIndicadoresQuantidadeProduzidaPostosModelo(DateTime DatInicio, DateTime DatFim, int CodLinha, int Turno, string modelo)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                List<IndicadorPostos> listaIndicadorPosto = new List<IndicadorPostos>();
                dtbCb.viw_CBPassagemTurno
                    .Where(a => a.CodLinha == CodLinha &&
                                a.DatReferencia == DatInicio &&
                                a.DatReferencia <= DatFim &&
                                a.Turno == Turno &&
                                a.NumECB.Substring(0, 6) == modelo)
                    .GroupBy(b => b.NumPosto)
                    .Select(x => new { NumPosto = x.Key, Count = x.Count() })
                    .OrderBy(z => z.NumPosto)
                    .ToList().ForEach(f => listaIndicadorPosto.Add(
                        new IndicadorPostos { NumPosto = f.NumPosto, Quantidade = f.Count}
                        ));

                return listaIndicadorPosto;
            }
        }

        public static List<CBPassagemDTO> ObterIndicadoresQuantidadeProduzidaPorModelo(DateTime datInicio, DateTime datFim, int codLinha, int turno, string modelo)
        {
            //flavio
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                List<CBPassagemDTO> listaIndicador = new List<CBPassagemDTO>();
                dtbCb.viw_CBPassagemTurno
                    .Where(a => a.CodLinha == codLinha &&
                                a.DatReferencia == datInicio &&
                                a.DatReferencia <= datFim &&
                                a.Turno == turno &&
                                a.NumECB.Substring(0, 6) == modelo)
                    .OrderBy(z => z.DatEvento)
                    .ToList().ForEach(f => listaIndicador.Add(
                        new CBPassagemDTO() { NumPosto = f.NumPosto, NumECB = f.NumECB, CodLinha = f.CodLinha, DatEvento = f.DatEvento}
                        ));

                return listaIndicador;
            }            
        }
         
        public static List<IndicadorPostos> ObterIndicadoresQuantidadeDefeitosModelo(DateTime DatInicio, DateTime DatFim, int CodLinha, int Turno, string modelo)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                List<IndicadorPostos> listaIndicadorPosto = new List<IndicadorPostos>();

                dtbCb.viw_CBDefeitoTurno
                    .Where(a => a.CodLinha == CodLinha &&
                                a.DatReferencia >= DatInicio &&
                                a.DatReferencia <= DatFim &&
                                a.Turno == Turno &&
                                String.IsNullOrEmpty(a.NumSeriePai) &&
                                a.FlgRevisado != 2 && a.FlgRevisado != 4 &&
                                a.NumECB.Substring(0, 6) == modelo
                                )
                    .GroupBy(b => b.NumPosto)
                    .Select(x => new { NumPosto = x.Key, Count = x.Count() })
                    .OrderBy(z => z.NumPosto)
                    .ToList().ForEach(f => listaIndicadorPosto.Add(
                        new IndicadorPostos { NumPosto = f.NumPosto, Quantidade = f.Count }
                        ));

                return listaIndicadorPosto;
            }
        }

		public static List<CBDefeitoDTO> ObterDefeitosLinha(DateTime DatInicio, DateTime DatFim, int CodLinha)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{

				var dados = dtbCb.tbl_CBDefeito
					.Where(a => a.CodLinha == CodLinha &&
								a.DatEvento >= DatInicio &&
								a.DatEvento <= DatFim &&
								a.FlgRevisado != 2 && a.FlgRevisado != 4
								)
					.Select(x => new CBDefeitoDTO()
					{
						NumECB = x.NumECB,
						CodCausa = x.CodCausa,
						CodDefeito = x.CodDefeito,
						CodDefeitoTest = x.CodDefeitoTest,
						CodDRT = x.CodDRT,
						CodDRTRevisor = x.CodDRTRevisor,
						CodDRTTest = x.CodDRTTest,
						CodItem = x.CodItem,
						CodLinha = x.CodLinha,
						CodLinhaRevisado = x.CodLinhaRevisado,
						CodOrigem = x.CodOrigem,
						CodRma = x.CodRma,
						CodSubDefeito = x.CodSubDefeito,
						DatEntRevisao = x.DatEntRevisao,
						DatEvento = x.DatEvento,
						DatRevisado = x.DatRevisado,
						DefIncorreto = x.DefIncorreto,
						DesAcao = x.DesAcao,
						FlgRevisado = x.FlgRevisado,
						FlgTipo = x.FlgTipo,
						NomUsuario = x.NomUsuario,
						NomUsuarioTest = x.NomUsuarioTest,
						NumIP = x.NumIP,
						NumPosto = x.NumPosto,
						NumPostoRevisado = x.NumPostoRevisado,
						NumSeriePai = x.NumSeriePai,
						ObsDefeito = x.ObsDefeito,
						Posicao = x.Posicao,
						Romaneio = x.Romaneio
					}).ToList();
					

				return dados;
			}
		}

		/// <summary>
		/// alterado
		/// </summary>
		/// <param name="DatInicio"></param>
		/// <param name="DatFim"></param>
		/// <param name="CodLinha"></param>
		/// <param name="Turno"></param>
		/// <param name="modelo"></param>
		/// <returns></returns>
		public static List<InspecaoIACRelatorio> ObterIndicadoresQuantidadeDefeitosModelo2(DateTime DatInicio, DateTime DatFim, int CodLinha, int Turno, string modelo)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                //flavio
                List<InspecaoIACRelatorio> listaIndicadorPosto = new List<InspecaoIACRelatorio>();

                dtbCb.viw_CBDefeitoInspecaoIAC
                    .Where(a => a.CodLinha == CodLinha &&
                                a.DatReferencia >= DatInicio &&
                                a.DatReferencia <= DatFim &&
                                a.Turno == Turno &&                                                         
                                a.Modelo == modelo
                                )
                    .GroupBy(b => new {Modelo = b.Modelo, Posicao = b.Posicao, CodDefeito = b.CodDefeito, Data = b.Data, Hora = b.hora })
                    .Select(x => new { Modelo = x.Key.Modelo,  Posicao = x.Key.Posicao, CodDefeito = x.Key.CodDefeito, Data = x.Key.Data, Hora = x.Key.Hora, Count = x.Count() })
                    .OrderBy(z => new {z.Modelo, z.Data, z.Hora })
                    .ToList().ForEach(f => listaIndicadorPosto.Add(
                        new InspecaoIACRelatorio
                        {
                            Modelo = f.Modelo,
                            PosicaoMecanica = f.Posicao, 
                            CodigoDefeito = f.CodDefeito, 
                            Data = f.Data,
                            Hora = f.Hora,
                            Quantidade =  f.Count                            
                        }
                        ));

                return listaIndicadorPosto;
            }
        }

      


        public static int ObterQuantidadePontosPorModelo(string codModelo)
        {
            var quantidadePontos = daoResumoIAC.ObterQuantidadePontos(codModelo);

            return quantidadePontos;
        }

        public static decimal GetQuantidadeProduzidaPostoEHoraModelo(
            DateTime DataInicio,
            DateTime DataFim,
            TimeSpan HoraInicio,
            TimeSpan HoraFim,
            int NumPosto,
            int Turno,
            int CodLinha,
            string modelo)
        {

            CBHorarioDTO turnoHorario = daoIndicadores.GetTurno(Turno);
            string stDataInicio = DataInicio.ToShortDateString() + " " + HoraInicio.ToString();
            string stDataFim = DataFim.ToShortDateString() + " " + HoraFim.ToString();

            if (TimeSpan.Parse(turnoHorario.HoraInicio) > TimeSpan.Parse(turnoHorario.HoraFim) && HoraInicio.Hours >= 0 && HoraFim.Hours <= TimeSpan.Parse(turnoHorario.HoraFim).Hours)
            {
                stDataInicio = DataInicio.AddDays(1).ToShortDateString() + " " + HoraInicio.ToString();
                stDataFim = DataFim.AddDays(1).ToShortDateString() + " " + HoraFim.ToString();
            }

            DateTime DataHoraInicio = DateTime.Parse(stDataInicio);
            DateTime DataHoraFim = DateTime.Parse(stDataFim);
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var query = dtbCb.viw_CBPassagemTurno.Where(a => a.NumPosto == NumPosto &&
                    a.Turno == Turno &&
                    a.CodLinha == CodLinha &&
                    a.DatReferencia >= DataInicio &&
                    a.DatReferencia <= DataFim &&
                    a.DatEvento >= DataHoraInicio && a.DatEvento <= DataHoraFim &&
                    a.NumECB.Substring(0, 6) == modelo
                    );

                var resultado = query.ToList();

                return resultado.Count();
            }
        }

        public static decimal GetQuantidadeDefeitosPostoEHoraModelo(
            DateTime DataInicio,
            DateTime DataFim,
            TimeSpan HoraInicio,
            TimeSpan HoraFim,
            int NumPosto,
            int Turno,
            int CodLinha,
            string modelo)
        {
            CBHorarioDTO turnoHorario = daoIndicadores.GetTurno(Turno);
            string stDataInicio = DataInicio.ToShortDateString() + " " + HoraInicio.ToString();
            string stDataFim = DataFim.ToShortDateString() + " " + HoraFim.ToString();

            if (TimeSpan.Parse(turnoHorario.HoraInicio) > TimeSpan.Parse(turnoHorario.HoraFim) && HoraInicio.Hours >= 0 && HoraFim.Hours <= TimeSpan.Parse(turnoHorario.HoraFim).Hours)
            {
                stDataInicio = DataInicio.AddDays(1).ToShortDateString() + " " + HoraInicio.ToString();
                stDataFim = DataFim.AddDays(1).ToShortDateString() + " " + HoraFim.ToString();
            }

            DateTime DataHoraInicio = DateTime.Parse(stDataInicio);
            DateTime DataHoraFim = DateTime.Parse(stDataFim);
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var query = dtbCb.viw_CBDefeitoTurno.Where(a => a.NumPosto == NumPosto &&
                    a.Turno == Turno &&
                    a.CodLinha == CodLinha &&
                    a.DatReferencia >= DataInicio &&
                    a.DatReferencia <= DataFim &&
                    a.DatEvento >= DataHoraInicio && a.DatEvento <= DataHoraFim &&
                    String.IsNullOrEmpty(a.NumSeriePai) &&
                    a.FlgRevisado != 2 &&
                    a.NumECB.Substring(0, 6) == modelo
                    ).ToList();

                return query.Count();
            }
        }

        public static List<MaioresDefeitos> GetDefeitosLinhaModelo(DateTime dataInicial, DateTime dataFinal, int CodLinha, string CodModelo)
        {
            using (DTB_CBContext contexto = new DTB_CBContext())
            {
                List<MaioresDefeitos> listaMaiores = (from a in contexto.tbl_CBDefeito
                                                      join b in contexto.tbl_CBLinha on a.CodLinha equals b.CodLinha
                                                      join d in contexto.tbl_CBCadCausa on a.CodCausa equals d.CodCausa
                                                      where b.Setor == "IAC" &&
                                                            a.FlgRevisado == 1 &&
                                                            a.DatEvento >= dataInicial &&
                                                            a.DatEvento <= dataFinal &&
                                                            b.CodLinha == CodLinha &&
                                                            a.Posicao != null &&
                                                            a.Posicao != "" &&
                                                            a.NumECB.Substring(0, 6) == CodModelo
                                                      select new MaioresDefeitos
                                                      {
                                                          CodCausa = d.CodCausa,
                                                          DesCausa = d.DesCausa,
                                                          Posicao = a.Posicao,
                                                          Familia = b.Familia,
                                                          Setor = b.Setor,
                                                          DatEvento = a.DatEvento,
                                                          CodDefeito = a.CodDefeito
                                                      }).ToList();

                return listaMaiores;
            }
        }

       
    }
}
