namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_PERMISSAO
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PER_CODIGO { get; set; }

        public int GRP_CODIGO { get; set; }

        [Required]
        [StringLength(3)]
        public string SIS_CODIGO { get; set; }

        public int? REL_CODIGO { get; set; }

        public int? FRM_CODIGO { get; set; }

        public int? COM_CODIGO { get; set; }

        public int? MNU_CODIGO { get; set; }

        [Required]
        [StringLength(1)]
        public string PER_ADICIONAR { get; set; }

        [Required]
        [StringLength(1)]
        public string PER_EXCLUIR { get; set; }

        [Required]
        [StringLength(1)]
        public string PER_EDITAR { get; set; }

        [Required]
        [StringLength(1)]
        public string PER_VISUALIZAR { get; set; }

        [Required]
        [StringLength(1)]
        public string PER_HABILITADO { get; set; }

        public virtual FR_FORMULARIO FR_FORMULARIO { get; set; }

        public virtual FR_GRUPO FR_GRUPO { get; set; }

        public virtual FR_RELATORIO FR_RELATORIO { get; set; }
    }
}
