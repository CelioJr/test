﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.DTO
{
   public class ProducaoHoraPostoDTO
    {
        public int dia { get; set; }
        public int mes { get; set; }
        public int ano { get; set; }
        public int hora { get; set; }
        public int linha { get; set; }
        public int posto { get; set; }
        public String desPosto { get; set; }
        public int qtdProd { get; set; }
        
    }
}
