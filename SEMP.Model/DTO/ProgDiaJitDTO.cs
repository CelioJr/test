using System;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
	public class ProgDiaJitDTO
	{
		[Display(Name = "F�b.")]
		public string CodFab { get; set; }
		[Display(Name = "Modelo")]
		public string CodModelo { get; set; }
		[Display(Name = "Data")]
		public System.DateTime DatProducao { get; set; }
		[Display(Name = "Linha")]
		public string CodLinha { get; set; }
		[Display(Name = "Tipo Produ��o")]
		public string FlagTP { get; set; }
		[Display(Name = "Qtd. Produzido")]
		public decimal? QtdProducao { get; set; }
		[Display(Name = "Qtd. Programa")]
		public decimal? QtdProdPm { get; set; }
		[Display(Name = "Usu�rio")]
		public string Usuario { get; set; }
		public System.Guid rowguid { get; set; }
		[Display(Name = "Conj. Cin")]
		public string CodConjCin { get; set; }
		[Display(Name = "Conj. Fly")]
		public string CodConjFly { get; set; }
		[Display(Name = "Data Estudo")]
		public DateTime? DatEstudo { get; set; }
		[Display(Name = "Cobertura")]
		public string flgCobertura { get; set; }
		[Display(Name = "Observa��o")]
		public string Observacao { get; set; }
		[Display(Name = "Turno")]
		public int NumTurno { get; set; }
		[Display(Name = "Descri��o")]
		public string DesModelo { get; set; }

		[Display(Name = "Fam�lia")]
		public string Familia { get; set; }
		[Display(Name = "Descri��o")]
		public string DesLinha { get; set; }
		[Display(Name = "Capac.")]
		public decimal? QtdCapac { get; set; }

		[Display(Name = "PP M�s")]
		[DisplayFormat(DataFormatString = "")]
		public decimal? PlanoMes { get; set; }

		public string CorPrograma { get; set; }
	}
}
