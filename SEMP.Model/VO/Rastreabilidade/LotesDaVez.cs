﻿namespace SEMP.Model.VO.Rastreabilidade
{
	public class LotesDaVez
	{
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public int? CodLinha { get; set; }
		public string DesLinha { get; set; }
		public string NumLote { get; set; }

	}
}
