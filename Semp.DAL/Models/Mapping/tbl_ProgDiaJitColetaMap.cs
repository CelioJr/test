using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_ProgDiaJitColetaMap : EntityTypeConfiguration<tbl_ProgDiaJitColeta>
    {
        public tbl_ProgDiaJitColetaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.CodModelo, t.DatProducao, t.CodLinha, t.FlagTP, t.rowguid });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6);

            this.Property(t => t.CodLinha)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8);

            this.Property(t => t.FlagTP)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.Usuario)
                .IsFixedLength()
                .HasMaxLength(25);

            this.Property(t => t.CodConjCin)
                .IsFixedLength()
                .HasMaxLength(6);

            this.Property(t => t.CodConjFly)
                .IsFixedLength()
                .HasMaxLength(6);

            this.Property(t => t.FlgCobertura)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.Observacao)
                .HasMaxLength(800);

            // Table & Column Mappings
            this.ToTable("tbl_ProgDiaJitColeta");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.DatProducao).HasColumnName("DatProducao");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.FlagTP).HasColumnName("FlagTP");
            this.Property(t => t.QtdProducao).HasColumnName("QtdProducao");
            this.Property(t => t.QtdProdPm).HasColumnName("QtdProdPm");
            this.Property(t => t.Usuario).HasColumnName("Usuario");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
            this.Property(t => t.CodConjCin).HasColumnName("CodConjCin");
            this.Property(t => t.CodConjFly).HasColumnName("CodConjFly");
            this.Property(t => t.DatEstudo).HasColumnName("DatEstudo");
            this.Property(t => t.FlgCobertura).HasColumnName("FlgCobertura");
            this.Property(t => t.Observacao).HasColumnName("Observacao");
        }
    }
}
