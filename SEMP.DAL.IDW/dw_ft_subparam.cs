namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_ft_subparam
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_ft_subparam()
        {
            dw_testesubetapa = new HashSet<dw_testesubetapa>();
        }

        [Key]
        public long id_subparam { get; set; }

        public long id_ft_sub { get; set; }

        public long id_ft_param { get; set; }

        public virtual dw_ft_param dw_ft_param { get; set; }

        public virtual dw_ft_sub dw_ft_sub { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_testesubetapa> dw_testesubetapa { get; set; }
    }
}
