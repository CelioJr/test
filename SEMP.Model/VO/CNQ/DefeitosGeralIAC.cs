﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.CNQ
{
    public class DefeitosGeralIAC
    {
        public string Data { get; set; }
        public string Fase { get; set; }
        public string Familia { get; set; }
        public int QuantidadeDefeito { get; set; }
        public int QuantidadeProduzida { get; set; }
        public Double Calculo { get; set; }
    }
}
