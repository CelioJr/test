namespace SEMP.Model.DTO
{
    public class ProdParadaObsDTO
    {
        public string CodFab { get; set; }
        public System.DateTime DatParada { get; set; }
        public string FlgTipo { get; set; }
        public string DesObs { get; set; }
        public System.Guid rowguid { get; set; }
    }
}
