using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_PlanoIACMap : EntityTypeConfiguration<tbl_PlanoIAC>
    {
        public tbl_PlanoIACMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodModelo, t.NumRevisao, t.CodProcesso, t.rowguid, t.FlgLeftRight });

            // Properties
            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumRevisao)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodProcesso)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.CodStatus)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.NomAprovacao)
                .IsFixedLength()
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.NomEmissor)
                .IsFixedLength()
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.NomDocLDC)
                .HasMaxLength(42)
                .IsUnicode(false);

            this.Property(t => t.Amplitude)
                .HasMaxLength(50)
                .IsUnicode(false);

            this.Property(t => t.FlgLeftRight)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("tbl_PlanoIAC");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.NumRevisao).HasColumnName("NumRevisao");
            this.Property(t => t.CodProcesso).HasColumnName("CodProcesso");
            this.Property(t => t.CodStatus).HasColumnName("CodStatus");
            this.Property(t => t.DatStatus).HasColumnName("DatStatus");
            this.Property(t => t.DatAtivo).HasColumnName("DatAtivo");
            this.Property(t => t.DatInativo).HasColumnName("DatInativo");
            this.Property(t => t.DatUltAlt).HasColumnName("DatUltAlt");
            this.Property(t => t.NomAprovacao).HasColumnName("NomAprovacao");
            this.Property(t => t.NomEmissor).HasColumnName("NomEmissor");
            this.Property(t => t.NomDocLDC).HasColumnName("NomDocLDC");
            this.Property(t => t.Amplitude).HasColumnName("Amplitude");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
            this.Property(t => t.FlgLeftRight).HasColumnName("FlgLeftRight");
        }
    }
}
