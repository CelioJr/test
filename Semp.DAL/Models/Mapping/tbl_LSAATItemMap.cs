using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_LSAATItemMap : EntityTypeConfiguration<tbl_LSAATItem>
    {
        public tbl_LSAATItemMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.NumAT, t.CodItem, t.CodItemAlt, t.CodIdentifica, t.CodModelo, t.QtdFrequencia, t.QtdNecessaria });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NumAT)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodItemAlt)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodIdentifica)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodAplAlt)
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.QtdFrequencia)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.QtdNecessaria)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodNumTec)
                .IsFixedLength()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.Observacao)
                .HasMaxLength(50)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_LSAATItem");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.NumAT).HasColumnName("NumAT");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.CodItemAlt).HasColumnName("CodItemAlt");
            this.Property(t => t.CodIdentifica).HasColumnName("CodIdentifica");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.CodAplAlt).HasColumnName("CodAplAlt");
            this.Property(t => t.QtdFrequencia).HasColumnName("QtdFrequencia");
            this.Property(t => t.QtdNecessaria).HasColumnName("QtdNecessaria");
            this.Property(t => t.QtdNota).HasColumnName("QtdNota");
            this.Property(t => t.QtdPaga).HasColumnName("QtdPaga");
            this.Property(t => t.QtdEstNormal).HasColumnName("QtdEstNormal");
            this.Property(t => t.QtdEstHannan).HasColumnName("QtdEstHannan");
            this.Property(t => t.QtdPadrao).HasColumnName("QtdPadrao");
            this.Property(t => t.QtdSaldoExterno).HasColumnName("QtdSaldoExterno");
            this.Property(t => t.QtdRetorno).HasColumnName("QtdRetorno");
            this.Property(t => t.CodNumTec).HasColumnName("CodNumTec");
            this.Property(t => t.PrecoUnitario).HasColumnName("PrecoUnitario");
            this.Property(t => t.Observacao).HasColumnName("Observacao");
            this.Property(t => t.QtdLibMaior).HasColumnName("QtdLibMaior");
        }
    }
}
