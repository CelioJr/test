using System;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
    public class LSAAPDTO
    {
		[Display(Name="F�b.")]
        public string CodFab { get; set; }
		[Display(Name = "AP")]
        public string NumAP { get; set; }
        public System.DateTime DatEmissao { get; set; }
        public System.DateTime DatAP { get; set; }
        public System.DateTime DatReferencia { get; set; }
        public string CodProcesso { get; set; }
        public string CodModelo { get; set; }
        public string CodCinescopio { get; set; }
		[Display(Name = "Lote")]
        public Nullable<int> QtdLoteAP { get; set; }
		[Display(Name = "Apontado")]
        public Nullable<int> QtdProduzida { get; set; }
        public string Sequencia { get; set; }
        public string CodRevisao { get; set; }
        public string CodRadial { get; set; }
        public string CodRevRadial { get; set; }
        public string CodAxial { get; set; }
        public string CodRevAxial { get; set; }
        public string CodLocal { get; set; }
		[Display(Name = "Status")]
        public string CodStatus { get; set; }
        public string CodLinha { get; set; }
		[Display(Name = "Observa��o")]
        public string Observacao { get; set; }
        public string TipAP { get; set; }
        public string CodLocalAP { get; set; }
        public Nullable<System.DateTime> DatLiberacao { get; set; }
        public Nullable<System.DateTime> DatAceite { get; set; }
        public Nullable<System.DateTime> DatFechamento { get; set; }
        public Nullable<System.DateTime> DatProgInicio { get; set; }
        public Nullable<System.DateTime> DatProgFinal { get; set; }
        public string NomAceite { get; set; }
        public Nullable<System.DateTime> DatInicioProd { get; set; }
        public string FlgDivida { get; set; }
        public Nullable<int> QtdEmbalado { get; set; }
        public string NumSerieINI { get; set; }
        public string NumSerieFIN { get; set; }
        public string CodSO { get; set; }
        public string FlgToshiba { get; set; }
        public string FlgTipoVenda { get; set; }
		public string NumAPDestino { get; set; }
		public string NumOP { get; set; }

		[Display(Name = "Diferen�a")]
		public int Diferenca { get { return (QtdProduzida ?? 0) - (QtdLoteAP ?? 0); } }

		public string DesModelo { get; set; }
		public string DesLinha { get; set; }
		public string DesProcesso { get; set; }
		public string DesLocal { get; set; }
		public string DesModIMC { get; set; }

	}
}
