namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_REPAIR")]
    public partial class ALC_REPAIR
    {
        [Key]
        public int ID_REPAIR { get; set; }

        [StringLength(255)]
        public string COMPONENT { get; set; }

        public DateTime? DT_ENTRY { get; set; }

        public DateTime? DT_REPAIR { get; set; }

        [StringLength(255)]
        public string OBSERVATION { get; set; }

        [StringLength(10)]
        public string STEP { get; set; }

        public int? ID_ACTION { get; set; }

        [Required]
        [StringLength(10)]
        public string ID_DEFECT { get; set; }

        public int COD_LINE { get; set; }

        public int ID_POST { get; set; }

        [StringLength(255)]
        public string ID_REASON { get; set; }

        public int ID_SERIAL { get; set; }

        public int COD_SKU { get; set; }

        public int? TECHNICAL { get; set; }

        [StringLength(5)]
        public string STATUS_ENG { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? DURATION_TIME { get; set; }

        public int? ENTRY_USER { get; set; }

        public int? ID_ORIGIN { get; set; }

        public virtual ALC_DEFECT ALC_DEFECT { get; set; }

        public virtual ALC_POST_USER ALC_POST_USER { get; set; }

        public virtual ALC_REPAIR_ORIGIN ALC_REPAIR_ORIGIN { get; set; }

        public virtual ALC_SYS_USER ALC_SYS_USER { get; set; }

        public virtual ALC_SYS_USER ALC_SYS_USER1 { get; set; }

        public virtual ALC_REPAIR_ACTION ALC_REPAIR_ACTION { get; set; }

        public virtual ALC_REPAIR_REASON ALC_REPAIR_REASON { get; set; }
    }
}
