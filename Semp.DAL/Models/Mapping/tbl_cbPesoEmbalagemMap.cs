﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
	public class tbl_CBPesoEmbalagemMap : EntityTypeConfiguration<tbl_CBPesoEmbalagem>
	{
		public tbl_CBPesoEmbalagemMap ()
		{
			// Primary Key
			this.HasKey(t => t.Modelo);

			// Properties
			this.Property(t => t.Modelo)
				.IsRequired()
				.HasMaxLength(6);

			this.Property(t => t.PesoMin)
				.HasMaxLength(6);

			this.Property(t => t.PesoMax)
				.HasMaxLength(6);

			// Table & Column Mappings
			this.ToTable("tbl_CBPesoEmbalagem");
			this.Property(t => t.Modelo).HasColumnName("Modelo");
			this.Property(t => t.PesoMin).HasColumnName("PesoMin");
			this.Property(t => t.PesoMax).HasColumnName("PesoMax");
		}
	}
}
