using System;
using System.Collections.Generic;

namespace SEMP.DTB_CB.Models
{
    /// <summary>
    /// Classe responsável por representar a tabela tbl_CBAmarra do banco de dados.
    /// </summary>
    public partial class tbl_CBAmarra
    {
        public string NumSerie { get; set; }
        public string NumECB { get; set; }
        public string CodModelo { get; set; }
        public Nullable<int> FlgTipo { get; set; }
        public Nullable<System.DateTime> DatAmarra { get; set; }

        //public Nullable<int> CodLinha { get; set; }
        //public Nullable<int> NumPosto { get; set; }
        //public string CodFab { get; set; }
        //public string NumAP { get; set; }
        //public string CodItem { get; set; }
    }
}
