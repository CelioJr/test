namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_SHIFT_PROD")]
    public partial class ALC_SHIFT_PROD
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ALC_SHIFT_PROD()
        {
            ALC_INTERVAL_PROD = new HashSet<ALC_INTERVAL_PROD>();
        }

        [Key]
        public int idShift { get; set; }

        [Required]
        [StringLength(255)]
        public string description { get; set; }

        public bool enable { get; set; }

        public int? HOUR_FINAL { get; set; }

        public int? HOUR_INICIAL { get; set; }

        public int? MIN_FINAL { get; set; }

        public int? MIN_INI { get; set; }

        public int id_shift { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_INTERVAL_PROD> ALC_INTERVAL_PROD { get; set; }
    }
}
