using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_ProgDiaJitMap : EntityTypeConfiguration<tbl_ProgDiaJit>
    {
        public tbl_ProgDiaJitMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.CodModelo, t.DatProducao, t.CodLinha, t.FlagTP, t.rowguid, t.NumTurno });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodLinha)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.FlagTP)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.Usuario)
                .IsFixedLength()
                .HasMaxLength(25)
                .IsUnicode(false);

            this.Property(t => t.CodConjCin)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodConjFly)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.flgCobertura)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.Observacao)
                .HasMaxLength(800)
                .IsUnicode(false);

            this.Property(t => t.NumTurno)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("tbl_ProgDiaJit");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.DatProducao).HasColumnName("DatProducao");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.FlagTP).HasColumnName("FlagTP");
            this.Property(t => t.QtdProducao).HasColumnName("QtdProducao");
            this.Property(t => t.QtdProdPm).HasColumnName("QtdProdPm");
            this.Property(t => t.Usuario).HasColumnName("Usuario");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
            this.Property(t => t.CodConjCin).HasColumnName("CodConjCin");
            this.Property(t => t.CodConjFly).HasColumnName("CodConjFly");
            this.Property(t => t.DatEstudo).HasColumnName("DatEstudo");
            this.Property(t => t.flgCobertura).HasColumnName("flgCobertura");
            this.Property(t => t.Observacao).HasColumnName("Observacao");
            this.Property(t => t.NumTurno).HasColumnName("NumTurno");
        }
    }
}
