﻿using System;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
	public class ProducaoLinhaMinuto
	{

		public string CodLinha { get; set; }
		public string DesLinha { get; set; }
		public DateTime DatReferencia { get; set; }
		public int Hora { get; set; }
		public int Minuto { get; set; }
		public int QtdLeitura { get; set; }
		public decimal? QtdMeta { get; set; }
		public string CodModelo { get; set; }

		public decimal MetaMin
		{
			get
			{
				if (QtdMeta > 0) {

					var prodMin = (QtdMeta??0) / 518;

					return prodMin;
				}

				return 0;
			}
		}

	}
}
