namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_est
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_est()
        {
            dw_estlocal = new HashSet<dw_estlocal>();
            dw_estpro = new HashSet<dw_estpro>();
            dw_nserie = new HashSet<dw_nserie>();
            dw_passagem = new HashSet<dw_passagem>();
            dw_rotapasso = new HashSet<dw_rotapasso>();
            dw_rotapasso1 = new HashSet<dw_rotapasso>();
            om_cfg = new HashSet<om_cfg>();
            om_cfg1 = new HashSet<om_cfg>();
            om_cfg2 = new HashSet<om_cfg>();
            om_cfg3 = new HashSet<om_cfg>();
            om_cfg4 = new HashSet<om_cfg>();
            om_cfg5 = new HashSet<om_cfg>();
            om_obj = new HashSet<om_obj>();
            om_produto = new HashSet<om_produto>();
            pp_cp = new HashSet<pp_cp>();
        }

        [Key]
        public long id_est { get; set; }

        [StringLength(30)]
        public string cd_est { get; set; }

        public int? revisao { get; set; }

        [StringLength(40)]
        public string ds_est { get; set; }

        public DateTime? dt_revisao { get; set; }

        public short? st_ativo { get; set; }

        public DateTime? dt_stativo { get; set; }

        public long id_usrrevisao { get; set; }

        public long id_usrstativo { get; set; }

        [StringLength(30)]
        public string depara { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_estlocal> dw_estlocal { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_estpro> dw_estpro { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_nserie> dw_nserie { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_passagem> dw_passagem { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rotapasso> dw_rotapasso { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rotapasso> dw_rotapasso1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg4 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg5 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_obj> om_obj { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_produto> om_produto { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cp> pp_cp { get; set; }
    }
}
