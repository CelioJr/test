using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DTB_CB.Models.Mapping
{
    public class tbl_CBRastLocalMap : EntityTypeConfiguration<tbl_CBRastLocal>
    {
        public tbl_CBRastLocalMap()
        {
            // Primary Key
            this.HasKey(t => new { t.NumSerieRastLocal, t.NumSerieProd });

            // Properties
            this.Property(t => t.NumSerieRastLocal)
                .IsRequired()
                .HasMaxLength(15);

            this.Property(t => t.NumSerieProd)
                .IsRequired()
                .HasMaxLength(25);

            this.Property(t => t.CodDRT)
                .IsFixedLength()
                .HasMaxLength(8);

            // Table & Column Mappings
            this.ToTable("tbl_CBRastLocal");
            this.Property(t => t.NumSerieRastLocal).HasColumnName("NumSerieRastLocal");
            this.Property(t => t.NumSerieProd).HasColumnName("NumSerieProd");
            this.Property(t => t.DatRegistro).HasColumnName("DatRegistro");
            this.Property(t => t.CodDRT).HasColumnName("CodDRT");
        }
    }
}
