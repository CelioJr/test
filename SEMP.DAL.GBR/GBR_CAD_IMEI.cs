namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_CAD_IMEI
    {
        [Key]
        public int COD_IMEI { get; set; }

        public int? COD_SMT { get; set; }

        [StringLength(16)]
        public string IMEI { get; set; }

        public int? ORDEM { get; set; }

        public bool? FLAG_IMEI { get; set; }

        public DateTime? DATA { get; set; }
    }
}
