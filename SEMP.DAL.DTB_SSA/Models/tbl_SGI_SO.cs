using System;
using System.Collections.Generic;

namespace SEMP.DAL.DTB_SSA.Models
{
    public partial class tbl_SGI_SO
    {
        public tbl_SGI_SO()
        {
        }

        public string codSO { get; set; }
        public string nomSO { get; set; }
        public string TipoConector { get; set; }
    }
}
