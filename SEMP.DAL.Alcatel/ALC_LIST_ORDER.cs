namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_LIST_ORDER")]
    public partial class ALC_LIST_ORDER
    {
        [Key]
        [StringLength(15)]
        public string imei { get; set; }

        [StringLength(30)]
        public string codorder { get; set; }

        [StringLength(30)]
        public string fullsku { get; set; }

        [StringLength(15)]
        public string lot { get; set; }

        [StringLength(15)]
        public string packing { get; set; }

        [StringLength(15)]
        public string pallet { get; set; }

        [StringLength(255)]
        public string shipping { get; set; }
    }
}
