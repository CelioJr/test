using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_PROD_PLAN_DTO
    {
        public decimal id_plan { get; set; }
        public string full_sku { get; set; }
        public Nullable<System.DateTime> ini_date { get; set; }
        public Nullable<int> made { get; set; }
        public Nullable<int> planned { get; set; }
        public int line { get; set; }
        public string ppb { get; set; }
        public int sku { get; set; }
        public Nullable<int> target { get; set; }
        public Nullable<double> curve { get; set; }
        public Nullable<int> ttl_defects { get; set; }
        public Nullable<int> capacity { get; set; }
        public virtual ALC_PPB_DTO ALC_PPB { get; set; }
        public virtual ALC_TARGET_HOUR_DTO ALC_TARGET_HOUR { get; set; }
    }
}
