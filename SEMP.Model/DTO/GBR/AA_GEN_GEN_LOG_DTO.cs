using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class AA_GEN_GEN_LOG_DTO
    {
        public int GENERATOR { get; set; }
        public Nullable<int> SESSION_ID { get; set; }
    }
}
