using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBLinhaModelo
    {
        public int CodLinha { get; set; }
        public string CodIdentificador { get; set; }
        public System.DateTime DatIniProd { get; set; }
        public System.DateTime DatFimProd { get; set; }
        public Nullable<int> CodOpeCriacao { get; set; }
        public Nullable<System.DateTime> DatExclusao { get; set; }
        public Nullable<int> CodOpeExclusao { get; set; }
        public Nullable<int> TempoParada { get; set; }
        public virtual tbl_CBLinhaMontagem tbl_CBLinhaMontagem { get; set; }
    }
}
