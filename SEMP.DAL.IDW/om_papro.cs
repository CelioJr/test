namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_papro
    {
        [Key]
        public long id_papro { get; set; }

        public long? id_pt { get; set; }

        public long? id_pa { get; set; }

        public long? id_produto { get; set; }

        public long? id_mapapa { get; set; }

        public decimal? qt_atual { get; set; }

        public virtual om_mapapa om_mapapa { get; set; }

        public virtual om_pa om_pa { get; set; }

        public virtual om_produto om_produto { get; set; }

        public virtual om_pt om_pt { get; set; }
    }
}
