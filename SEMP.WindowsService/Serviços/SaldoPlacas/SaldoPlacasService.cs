﻿using log4net;
using MATERIAIS.BO.Estoque;
using System;
using System.Linq;

namespace SEMP.WindowsService
{
	public class SaldoPlacasService
	{

		private static ILog _logger = LogManager.GetLogger("SaldoPlacas");

		public Resposta RunJob()
		{
			Resposta retorno = new Resposta { Codigo = 0, Msg = "" };

			FazerImportacao();

			return new Resposta();
		}

		private void FazerImportacao()
		{
			try
			{
				SetStatus("Iniciando importação Saldo de Placas");

				var dados = SAPConnector.SAPFunctions.ObterEstoquePCI();

				if (dados != null)
				{
					SetStatus($"{dados.Count()} registros encontrados, gravando...");
					Estoque.GravarEstoquePCI(dados);

					SetStatus("Importação finalizada.");
				}
				else
				{
					SetStatus("Não foram encontrados dados a importar ou aconteceu erro na consulta.");
				}


			}
			catch (Exception ex)
			{
				_logger.Error("Erro ao executar a importação das Ordens de Produção.", ex);
			}
		}

		public void SetStatus(string msg)
		{
			_logger.Info(msg);
		}

	}
}
