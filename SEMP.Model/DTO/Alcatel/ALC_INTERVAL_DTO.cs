using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_INTERVAL_DTO
    {
        public int ID_INTERVAL { get; set; }
        public Nullable<int> INTERVAL_HOUR { get; set; }
        public Nullable<int> QTY_MINUTES { get; set; }
        public Nullable<int> COD_LINHA { get; set; }
    }
}
