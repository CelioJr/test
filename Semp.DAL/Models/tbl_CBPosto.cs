using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
   /// <summary>
	/// Classe responsável por espelhar a tabela tbl_CBPosto do banco de dados.
   /// </summary>
	public partial class tbl_CBPosto
	{
		public int CodLinha { get; set; }
		public int NumPosto { get; set; }
		public int CodTipoPosto { get; set; }
		public string DesPosto { get; set; }
		public string NumIP { get; set; }
		public string CodDRT { get; set; }
		public string FlgTipoTerminal { get; set; }
		public string FlgAtivo { get; set; }
		public int NumSeq { get; set; }
		public Nullable<bool> flgObrigatorio { get; set; }
		public Nullable<bool> flgDRTObrig { get; set; }
		public string TipoAmarra { get; set; }
		public string flgPedeAP { get; set; }
		public string flgImprimeEtq { get; set; }
		public Nullable<int> TempoLeitura { get; set; }
		public Nullable<int> TipoEtiqueta { get; set; }
		public Nullable<decimal> TempoCiclo { get; set; }
		public string FlgGargalo { get; set; }
        
        public string NumIPBalanca { get; set; }

        public string PostoBloqueado { get; set; }

        public Nullable<int> TempoBloqueio { get; set; }

        public Nullable<bool> flgValidaOrigem { get; set; }

        public Nullable<bool> MontarLote { get; set; }
		public Nullable<bool> flgBaixaEstoque { get; set; }
    }
}
