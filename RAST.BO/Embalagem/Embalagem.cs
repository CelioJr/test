﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Linq;
using System.Collections.Generic;
using SEMP.Model;
using SEMP.DAL;
using RAST.BO.Rastreabilidade;
using SEMP.DAL.DAO.Embalagem;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.VO;
using SEMP.Model.DTO;
using SEMP.Model.VO.Rastreabilidade;
using SEMP.Model.VO.Embalagem;
using System.Diagnostics;

namespace RAST.BO.Embalagem
{

	/// <summary>
	/// Classe responsável por realizar operações do posto de embalagem
	/// </summary>'
	public class Embalagem : IEmbalagem
	{
		public static IEmbalagem Recuperar_Posto_Embalagem(CBPostoDTO posto)
		{
			if ((posto.CodLinha.Equals(43) || posto.CodLinha.Equals(44)) && (posto.CodTipoPosto.Equals(Constantes.POSTO_AMARRA_LOTE_TABLET) || posto.CodTipoPosto.Equals(Constantes.POSTO_AMARRA_LOTE_TABLET_IMPRESSAO_ETIQUETA_PESO) || posto.CodTipoPosto.Equals(Constantes.POSTO_AMARRA_LOTE_TABLET_IMPRESSAO_ETIQUETA))) //Tablet
				return new EmbalagemTablet();

			if (posto.CodLinha.Equals(42) && posto.CodTipoPosto.Equals(Constantes.POSTO_AMARRA_LOTE_NOTEBOOK)) //Notebook                        
				return new EmbalagemNotebook();

			if (posto.CodTipoPosto.Equals(Constantes.POSTO_AMARRA_LOTE_ENTREPOSTO_IAC))
			{
				return new EmbalagemEntrepostoIAC();
			}


			return new Embalagem();
		}

		/// <summary>
		/// Método responsável por embalar os produtos, inserindo os dados na tbl_cbembalada
		/// Quanto houverem itens para serem amarrados ao produtos, estes itens são inseridos na tbl_cbamarra
		/// Todos os produtos a serem embalados também são inseridos na tbl_cbpassagem, para identificar que passou no posto de embalagem
		/// </summary>
		/// <param name="embalagem"></param>
		/// <param name="listaAmarra"></param>
		/// <param name="passagem"></param>
		/// <returns></returns>
		public virtual Mensagem Embalar_Produto(CBEmbaladaDTO embalagem, List<CBAmarraDTO> listaAmarra, Passagem passagem, CBPostoDTO posto, bool gravarBancoSemp = true)
		{
			Stopwatch sw = new Stopwatch();
			string tempoExecucaoMetodo = string.Empty;
			string TextoTempo = string.Empty;
			TimeSpan ts;

			Mensagem retorno = new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "LIME", DesMensagem = "LEITURA OK. LEIA PRÓXIMO." };

			try
			{
				#region Embalar Produto
					sw.Start();
					
					var resultProcedure = DaoEmbalagem.Procedure_Embalar_Amarrar_Produto(embalagem, listaAmarra);
					sw.Stop();
					ts = sw.Elapsed;

					tempoExecucaoMetodo = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
															   ts.Hours, ts.Minutes, ts.Seconds,
															   ts.Milliseconds / 10);
					TextoTempo += "\n Tempo Embalagem Produto: " + tempoExecucaoMetodo;

					if (resultProcedure == null || resultProcedure.CodSucesso == 1)
					{
						retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
						retorno.DesMensagem = "Erro ao executar procedure embalagem";
						retorno.MensagemAuxiliar = TextoTempo;
						return retorno;
					}
				#endregion

				embalagem.NumLote = resultProcedure.numeroLote;

				#region Impressão de etiquetas
				if (resultProcedure.qtd_atual_cc >= resultProcedure.qtd_padrao_cc)
					{
						if (posto.flgImprimeEtq == "S")
						{
							sw.Restart();
							ImprimirCaixaColetiva(resultProcedure.numeroCaixaColetiva, embalagem.Linha, posto);
							sw.Stop();
							ts = sw.Elapsed;
							tempoExecucaoMetodo = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);

							TextoTempo += "\n Tempo Impressão: " + tempoExecucaoMetodo;
						}
					}
				#endregion
				
				
				#region Procedure SEMP
				if (gravarBancoSemp && embalagem.Linha.Familia != "DAT")
				{
					sw.Restart();
					switch (embalagem.Linha.Setor)
					{
						case "IMC":
						case "IAC":
                        case "LCM":
							//se for IAC ou IMC
							retorno = SU_GravaSTA(embalagem);
							break;

						case "FEC":
							switch (embalagem.Linha.Familia)
							{
								case "AUD":
								case "LCD":
								case "DVD":
									//se for Audio, LCD ou DVD
									retorno = SU_GravaFECSTA(embalagem, listaAmarra);
									break;

								case "INF":
									if (!DaoEmbalagem.Atualizar_tabela_dtbSSA_Tablet(embalagem))
									{
										retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
										retorno.DesMensagem = "Erro ao atualizar Banco SSA";
									}
									break;
								default:
									break;
							}
							break;

						default:
							break;
					}
					sw.Stop();
					ts = sw.Elapsed;
					tempoExecucaoMetodo = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
														  ts.Hours, ts.Minutes, ts.Seconds,
														  ts.Milliseconds / 10);

					TextoTempo += "\n Tempo Procedure SEMP: " + tempoExecucaoMetodo;

					if (retorno.CodMensagem != Constantes.RAST_OK)
					{
						DesfazerOperacoes(embalagem, listaAmarra);
						Email.SU_EnviaEmail(retorno.DesMensagem, retorno.StackErro);
						retorno.MensagemAuxiliar = TextoTempo;
						return retorno;
					}
				}
				#endregion            
			}
			catch (Exception ex)
			{
				if (retorno.CodMensagem == Constantes.RAST_IMPRESSAO_NAO_REALIZADA)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.RAST_IMPRESSAO_NAO_REALIZADA);
				}
				else
				{
					DesfazerOperacoes(embalagem, listaAmarra);
					retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				}

				retorno.DesMensagem = ex.Message;
			}

			retorno.MensagemAuxiliar = TextoTempo;
			
			return retorno;
		}

		public static bool Inserir_Embalada(CBEmbaladaDTO embalada, bool salvar)
		{
			return DaoEmbalagem.Inserir_Embalada(embalada, salvar);
		
		}

		/// <summary>
		/// Chama o método que desfaz as operações no banco dtb_cb, caso ocorra erro durante a embalagem
		/// </summary>
		/// <param name="embalagem"></param>
		/// <param name="listaAmarra"></param>
		protected void DesfazerOperacoes (CBEmbaladaDTO embalagem, List<CBAmarraDTO> listaAmarra)
		{
			DaoEmbalagem.DesfazerOperacoes(embalagem, listaAmarra);
		}

		public string RecuperarLoteAberto(string codFab, string numap, int codLinha)
		{
			return DaoEmbalagem.RecuperarNumeroLoteAberto(codFab, numap, codLinha);
		}

		public String RecuperarLoteAberto(string numAp, string codModelo, int codLinha, string codFab)
		{
			return DaoEmbalagem.RecuperarNumeroLoteAberto(numAp, codModelo, codLinha, codFab);
		}

		public string RecuperarLoteAberto(string codModelo, int codLinha, string codFab)
		{
			return DaoEmbalagem.RecuperarNumeroLoteAberto(codModelo, codLinha, codFab);
		}

		public String RecuperarNumeroCaixaColetivaAberta(string numAp, int codLinha, string codFab)
		{
			return DaoEmbalagem.RecuperarNumeroCaixaColetivaAberta(numAp, codLinha, codFab);
		}

		public String RecuperarNumeroCaixaColetivaAberta(int codLinha)
		{
			return DaoEmbalagem.RecuperarNumeroCaixaColetivaAberta(codLinha);
		}

		public String RecuperarNumeroCaixaColetivaAberta(int codLinha, string codModelo)
		{
			return DaoEmbalagem.RecuperarNumeroCaixaColetivaAberta(codLinha, codModelo);
		}

		public String RecuperarNumeroCaixaColetivaAberta(string numAp,string modelo, int codLinha, string codFab)
		{
			return DaoEmbalagem.RecuperarNumeroCaixaColetivaAberta(numAp, modelo,codLinha, codFab);
		}
 
		/// <summary>
		/// Método que realiza as operações de inclusão na tabela tbl_barrasproducaoplaca do dtb_sim, afim de contar placas embaladas
		/// </summary>
		/// <param name="embalagem"></param>
		/// <returns></returns>
		public static Mensagem SU_GravaSTA (CBEmbaladaDTO embalagem)
		{
			//grava no DTB_SIM.BarrasProducaoPlaca
			return DaoEmbalagem.SU_GravaSTA(embalagem.CodFab,
													embalagem.Linha.CodLinhaT1,
													DateTime.Now.ToShortDateString(),
													embalagem.CodOperador,
													embalagem.CodModelo,
													embalagem.NumECB,
													embalagem.NumAP,
													embalagem.Turno);            
		}

		/// <summary>
		/// Método que faz o controle de inclusão dos dados na tabela tbl_barrasproducao do banco dtb_sim. Usado em casos de FECHAMENTO das linhas de AUDIO, LCD ou DVD
		/// </summary>
		/// <param name="embalagem"></param>
		/// <param name="listaAmarra"></param>
		/// <returns></returns>
		private static Mensagem SU_GravaFECSTA (CBEmbaladaDTO embalagem, List<CBAmarraDTO> listaAmarra)
		{
			return DaoEmbalagem.SU_GravaFECSTA(embalagem, listaAmarra);          
		}
	   
		/// <summary>
		/// Método que faz a validação dos itens lidos no posto. Este método deve ser implementado para as classes que herdarem esta classe
		/// </summary>
		/// <param name="tipoAmarra"></param>
		/// <param name="leitura"></param>
		/// <param name="numAp"></param>
		/// <param name="numSerie"></param>
		/// <param name="numKitProvisorio"></param>
		/// <returns></returns>
		public virtual Mensagem ValidarLeitura(int tipoAmarra, string leitura, string numAp, string numSerie, string numKitProvisorio,CBLinhaDTO linha)
		{
			var retorno = new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "Lime", DesMensagem = "LEITURA OK. LEIA PRÓXIMO." };

			if (linha.Setor.Equals("FEC"))
			{
				if ((linha.Familia.Equals("AUD") || linha.Familia.Equals("LCD") || linha.Familia.Equals("DVD")))
				{
					var retornoValidacao = Amarracao.ValidarItem(leitura);
					retorno.CodMensagem = retornoValidacao.CodMensagem;
					retorno.DesMensagem = retornoValidacao.DesMensagem;
					retorno.DesCor = retornoValidacao.DesCor;
				}
			}

			return retorno;
		}

		/// <summary>
		/// Método que valida se o produto atual pode ser embalado.
		///     1 - Verifica se passou por todas as fases anteriores
		///     2 - Verifica se possui defeito em aberto
		///     3 - Verifiva spc_BarrasValidaPCISerie
		/// </summary>
		/// <param name="passagem"></param>
		/// <param name="codDrt"></param>
		/// <param name="numAp"></param>
		/// <param name="leitura"></param>
		/// <param name="modelo"></param>
		/// <param name="codFab"></param>
		/// <param name="codigoCinescopio"></param>
		/// <param name="linha"></param>
		/// <returns></returns>
		public virtual Mensagem ValidarProduto (Passagem passagem, String codDrt, string numAp, string leitura, string modelo, string codFab, string codigoCinescopio, CBLinhaDTO linha)
		{           
			//verifica passagem do item atual pelos postos anteriores
			Mensagem retorno = passagem.ValidaPassagem(leitura, codDrt, passagem.posto.CodLinha, passagem.posto.NumPosto, numAp);
			if (retorno.CodMensagem != Constantes.RAST_OK)
				return retorno;

			//if (linha.Setor.Equals("IMC") || linha.Setor.Equals("IAC"))  //comente para linhas IAC e IMC
			//{ 
			//    //verifica se o item já foi lido no dtb_sim
			//    var codModelo = (linha.Setor.Equals("IAC") ? daoGeral.ObterCodModeloIAC(modelo) : modelo);
			//    retorno = Validar_PciSerie(codModelo, leitura, linha.CodLinhaT1, codFab, numAp);
			//}
			if (linha.Setor.Equals("IMC") || linha.Setor.Equals("IAC"))  //comente para linhas IAC e IMC
			{
				//verifica se o item já foi lido no dtb_sim
				//var codModelo = (linha.Setor.Equals("IAC") ? daoGeral.ObterCodModeloIAC(modelo) : modelo);
				retorno = Validar_PciSerie(modelo, leitura, linha.CodLinhaT1, codFab, numAp);
			}
			
			else if(linha.Setor.Equals("FEC"))
			{
				//somente para as linhas de fechamento de audio, LCD e TV
				//Validar se o painel está associado ao produto
				if (linha.Familia.Equals("LCD"))
				{
					var dadosAmarra = DaoEmbalagem.RecuperarDadosAmarracaoProduto(leitura);
					if (dadosAmarra == null || dadosAmarra.Count<= 0)
					{
						retorno = daoMensagem.ObterMensagem(Constantes.RAST_REGISTRO_NAO_ENCONTRADO);
						retorno.DesMensagem = "AMARRAÇÃO DO PRODUTO NÃO ENCONTRADA";
						return retorno;
					}

				    #region validando dados do painel
                    var dadosPainel = dadosAmarra.FirstOrDefault(x => x.FlgTipo == Constantes.AMARRA_PAINEL || x.FlgTipo == Constantes.AMARRA_PAINEL_LCD || x.FlgTipo == Constantes.AMARRA_PAINEL_LCD_LCM);

                    if (dadosPainel == null)
                    {
                        retorno = daoMensagem.ObterMensagem(Constantes.RAST_REGISTRO_NAO_ENCONTRADO);
                        retorno.DesMensagem = "ITEM NÃO POSSUI PAINEL ASSOCIADO";
                        return retorno;
                    }

				    if (string.IsNullOrEmpty(dadosPainel.NumAP) || string.IsNullOrEmpty(dadosPainel.CodItem))
				    {
				        retorno = daoMensagem.ObterMensagem(Constantes.RAST_REGISTRONAOENCONTRADO);
				        retorno.DesMensagem = "ITEM NÃO POSSUI PAINEL ASSOCIADO";
				        return retorno;
				    }

                    if (!string.IsNullOrEmpty(codigoCinescopio))
                    {
                        var numeroNE_i = Convert.ToInt32(dadosPainel.CodItem);
                        var codCin_i = Convert.ToInt32(codigoCinescopio);

                        if (numeroNE_i.ToString("000000") != codCin_i.ToString("000000"))
                        {
                            retorno = daoMensagem.ObterMensagem(Constantes.RAST_REGISTRONAOENCONTRADO);
                            retorno.DesMensagem =
                                string.Format("PAINEL DO ITEM AMARRADO EM AP DIFERENTE DA ATUAL. AP ATUAL: {0}, AP AMARRAÇÃO:{1}",
                                    numAp, dadosPainel.NumAP);
                        }
                    }
                    else {
                        retorno = daoMensagem.ObterMensagem(Constantes.RAST_REGISTRONAOENCONTRADO);
                        retorno.DesMensagem = "CINESCÓPIO NÃO ENCONTRADO. FAVOR COMUNICAR ENGENHARIA/DTI";
                        return retorno;
                    }

				    #endregion					
				}
			}
			return retorno;
		}

		/// <summary>
		/// Método responsável por abrir caixa coletiva para o produto atual
		/// </summary>
		/// <param name="idLinha"></param>
		/// <param name="serial"></param>
		/// <param name="codModelo"></param>
		/// <returns></returns>
		public String AbrirCaixaColetiva(int idLinha, string codModelo, string codFab, string numAp)
		{
			return DaoEmbalagem.AbrirNovaCaixaColetiva(idLinha, codModelo, codFab, numAp);
		}		

		/// <summary>
		/// método responsável por fechar a caixa coletiva atual
		/// </summary>
		/// <param name="numLote"></param>
		/// <param name="codDrt"></param>
		/// <param name="justificativa"></param>
		/// <returns></returns>
		public Mensagem FecharCaixaColetiva(string numLote, string codDrt, string justificativa)
		{
			var retorno = Defeito.ObterMensagem(Constantes.RAST_OK);
			var resultado = DaoEmbalagem.FecharCaixaColetiva(numLote, codDrt, justificativa);

			if (resultado)
				return retorno;
			
			retorno = Defeito.ObterMensagem(Constantes.RAST_LOTE_NAOFINALIZADO);

			return retorno;
		}

		/// <summary>
		/// método responsável por fechar o lote atual
		/// </summary>
		/// <param name="numLote"></param>
		/// <param name="codDrt"></param>
		/// <param name="justificativa"></param>
		/// <returns></returns>
		public static Mensagem FecharLote (string numLote, string codDrt, string justificativa)
		{
			var retorno = new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "LIME", DesMensagem = "LEITURA OK. LEIA PRÓXIMO." }; // Defeito.ObterMensagem(Constantes.RAST_OK);
			var resultado = DaoEmbalagem.FecharLote(numLote, codDrt, justificativa);

			if (resultado) return retorno;

			retorno = Defeito.ObterMensagem(Constantes.RAST_LOTE_NAOFINALIZADO);

			return retorno;
		} 

		/// <summary>
		/// Recupera todos os produtos associados ao lote aberto para a AP/ Linha
		/// </summary>
		/// <param name="numAp"></param>
		/// <param name="codModelo"></param>
		/// <param name="codLinha"></param>
		/// <param name="codFab"></param>
		/// <returns></returns>
		public List<CBEmbaladaDTO> ProdutosDeLoteEmAberto(string numAp, string codModelo, int codLinha, string codFab)
		{
			return DaoEmbalagem.ProdutosDeLoteEmAberto(numAp, codModelo, codLinha, codFab);            
		}

		/// <summary>
		/// Recupera os produtos que estão na caixa coletiva aberta para a linha
		/// </summary>
		/// <param name="numAp"></param>
		/// <param name="codModelo"></param>
		/// <param name="codLinha"></param>
		/// <param name="codFab"></param>
		/// <returns></returns>
		public List<CBEmbaladaDTO> RecuperarItensEmbaladosDaCaixaColetivaAberta (string numAp, string codModelo, int codLinha, string codFab)
		{
			return DaoEmbalagem.RecuperarItensEmbaladosDaCaixaColetivaAberta(numAp, codModelo, codLinha, codFab);
		}

		public List<CBEmbaladaDTO> RecuperarItensEmbaladadosDaCaixaColetiva (string numCC)
		{
			return DaoEmbalagem.RecuperarItensEmbaladadosDaCaixaColetiva(numCC);
		}

		/// <summary>
		/// Valida a existeência de determinado número de série no posto de embalagem
		/// </summary>
		/// <param name="serial"></param>
		/// <returns></returns>
		public static bool VerificaSeNumeroSerieExisteNaEmbalagem(string serial)
		{
			return DaoEmbalagem.VerificaSeNumeroSerieExisteNaEmbalagem(serial);
		}

		/// <summary>
		/// Método implementado pela classe filha
		/// </summary>
		/// <param name="numAp"></param>
		/// <param name="leitura"></param>
		/// <param name="etiqueta"></param>
		/// <param name="objImpressao"></param>
		/// <param name="listaEtqImpressao"></param>
		/// <returns></returns>
		public virtual Mensagem ImprimirEtiquetasIndividuais (string numAp, string leitura, string codModelo, CBPostoDTO posto, double PesoCaixa)
		{
			return new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "LIME", DesMensagem = "LEITURA OK. LEIA PRÓXIMO." };
		}

		/// <summary>
		/// Método implementado pela classe filha
		/// </summary>
		/// <param name="numAp"></param>
		/// <param name="etiqueta"></param>
		/// <param name="seriais"></param>
		/// <param name="qtdLote"></param>
		/// <param name="objImpressao"></param>
		/// <returns></returns>        
		public virtual Mensagem ImprimirCaixaColetiva (string numeroLote, CBLinhaDTO linha, CBPostoDTO posto)
		{
			var retorno = new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "LIME", DesMensagem = "LEITURA OK. LEIA PRÓXIMO." };
	   
			try
			{ 
				if ((linha.Setor.Equals("IMC") && linha.Familia != "DAT")|| linha.Setor.Equals("IAC"))
				{
					int qtdeImprimir = new Embalagem().QuantidadeItensEmbaladosCaixaColetiva(numeroLote);
					var lote = Embalagem.RecuperarDadosLote(numeroLote);
					string data1 = (lote.DatFechamento == null ? DateTime.Now.ToString() : lote.DatFechamento.ToString());
					DateTime dataImprimir = Convert.ToDateTime(data1);
					string modelo = lote.CodModelo;
					int qtdLotePadrao = Geral.ObterQuantidadeLotePorLinha(linha.CodLinha);

					var EtiquetasPorPosto = Embalagem.RecuperarEtiquetasPorPosto(posto.CodLinha, posto.NumPosto, true);  

					string descModelo = Geral.ObterDescricaoItem(modelo);
				   
					var dados = new Hashtable
					{
						{"origem", linha.Setor},
						{"desOrigem", linha.DesLinha},
						{"desModelo", descModelo},
						{"modelo", modelo},
						{"codFab", lote.CodFab},
						{"numAP", lote.NumAP},
						{"numLote", numeroLote},
						{"qtdeLote", qtdeImprimir},
						{"tamLote", qtdLotePadrao},
						{"data", dataImprimir.ToString("yyyy/MM/dd")},
						{"hora", dataImprimir.ToShortTimeString()}

					};
					foreach (var etiqueta in EtiquetasPorPosto)
					{
						GerenciarImpressao.ImprimirEtiqueta(dados, etiqueta);
					}
				}
				else if (linha.Setor.Equals("FEC") && linha.Familia.Equals("DVD"))
				{
					var EtiquetasPorPosto = Embalagem.RecuperarEtiquetasPorPosto(posto.CodLinha, posto.NumPosto, true);

					//existem etiquetas para serem impressas
					if (EtiquetasPorPosto != null)
					{
						var etiqueta = EtiquetasPorPosto.FirstOrDefault(x => x.NomeEtiqueta == "Etq_DVD_Fechamento");

						var itensLote = RecuperarItensEmbaladadosDaCaixaColetiva(numeroLote);
						var descModelo = "MOD.:  " + Geral.ObterDescricaoItem(itensLote[0].CodModelo);

						var dados = new Hashtable();
						int cont = 0;

						for (var i = 0; i <= 3; i++)
						{
							dados.Add("descricao" + (i + 1), "");
							dados.Add("lbSN" + (i + 1), "");
							dados.Add("numeroSerie" + (i + 1), "");
						}
						  
						foreach(var item in itensLote)
						{
							cont++;
							dados["descricao" + cont] = descModelo;
							dados["lbSN" + cont] = "N. SERIE: ";
							dados["numeroSerie" + cont] = item.NumECB;                             
						}
						GerenciarImpressao.ImprimirEtiqueta(dados, etiqueta);
					}
				}
                else if (linha.Familia.Equals("DAT"))
                {
                    var EtiquetasPorPosto = Embalagem.RecuperarEtiquetasPorPosto(posto.CodLinha, posto.NumPosto, true);

                    //existem etiquetas para serem impressas
                    if (EtiquetasPorPosto != null)
                    {
                        var etiqueta = EtiquetasPorPosto.FirstOrDefault(x => x.NomeEtiqueta == "EtqInspecaoPlacas2-beta4");

                        //string numeroLote = Embalagem.RecuperarCCProdutoEmbalado(numECB);

                        var itensLote = new Embalagem().RecuperarItensEmbaladadosDaCaixaColetiva(numeroLote);

                        var tabelaImpressao = new Hashtable();

                        tabelaImpressao.Add("csDataAprovProd", DateTime.Today.ToShortDateString());
                        tabelaImpressao.Add("csDataAprovCQ", DateTime.Today.ToShortDateString());
                        tabelaImpressao.Add("Date0", DateTime.Today.ToShortDateString());

                        tabelaImpressao.Add("csRespCQ", "");
                        tabelaImpressao.Add("csNumLote", numeroLote);
                        tabelaImpressao.Add("csQtdSerial", itensLote.Count());

                        tabelaImpressao.Add("csCItm", itensLote[0].CodModelo);
                        tabelaImpressao.Add("csDescItem", Geral.ObterDescricaoItem(itensLote[0].CodModelo));

                        tabelaImpressao.Add("csBarCode", numeroLote);

                        var csNumSerie = "";
                        foreach (var item in itensLote)
                        {
                            csNumSerie += item.NumECB.Substring(6, 6) + "  ";
                        }

                        tabelaImpressao.Add("csNumSerie", csNumSerie);
                        GerenciarImpressao.ImprimirEtiqueta(tabelaImpressao, etiqueta);
                    }
                
                }
            
			}
			catch (Exception ex)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.RAST_IMPRESSAO_NAO_REALIZADA);
				retorno.DesMensagem = ex.Message;                 
			}

			return retorno;
		}

		public static string RecuperarCCProdutoEmbalado (string numSerie)
		{
			return DaoEmbalagem.RecuperarCCProdutoEmbalado(numSerie);
		}

		/// <summary>
		/// Recupera os dados usados para impressão de etiquetas de produtos STA
		/// </summary>
		/// <param name="codModelo"></param>
		/// <returns></returns>
		public static viw_STI_ModeloEtiquetaDTO RecuperarDadosEtiquetaDto(string codModelo)
		{
			return DaoEmbalagem.Recuperar_Dados_Modelo_Etiqueta(codModelo);
		}

		/// <summary>
		/// Atualiza o modo retrabalho do posto de embalagem de produtos STA
		/// </summary>
		/// <param name="codLinha"></param>
		/// <param name="numPosto"></param>
		/// <param name="codDrt"></param>
		public static bool Atualizar_Modo_Retrabalho (int codLinha, int numPosto, int codDrt)
		{
			return DaoEmbalagem.Atualizar_Modo_Retrabalho(codLinha, numPosto, codDrt);
		}

		/// <summary>
		/// Recupera o status do retrabalho para determinada linha e posto
		/// </summary>
		/// <param name="codLinha"></param>
		/// <param name="numPosto"></param>
		/// <returns></returns>
		public static bool Recuperar_Status_Retrabalho(int codLinha, int numPosto)
		{
			return DaoEmbalagem.Recuperar_Status_Retrabalho(codLinha, numPosto);
		}

		/// <summary>
		/// faz a chamada da procedure spc_BarrasValidaPCISerie para validação de produto
		/// </summary>
		/// <param name="codModelo"></param>
		/// <param name="barcode"></param>
		/// <param name="codLinha"></param>
		/// <param name="codFab"></param>
		/// <param name="ap"></param>
		/// <returns></returns>
		private Mensagem Validar_PciSerie(string codModelo, string barcode, string codLinha, string codFab, string ap)
		{
			string validaBarcodeAp = DaoEmbalagem.SU_ValidaBarcodeAp(codModelo, barcode, codLinha, codFab, ap);
			Mensagem retorno = null;

			/* 0-> OK | 1-> Já lido | 2-> Erro | 3-> Troca de cinescpio | 4-> AP finalizada */
			switch (validaBarcodeAp)
			{
				case ("0"):
					retorno = Defeito.ObterMensagem(Constantes.RAST_OK);
					break;

				case ("1"):
					retorno = Defeito.ObterMensagem(Constantes.RAST_LEITURA_REPETIDA);
					break;
					
				case ("2"):
					retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
					break;

				case ("3"):
					retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
					break;

				case ("4"):
					retorno = Defeito.ObterMensagem(Constantes.RAST_AP_FINALIZADA);
					break;

				default:
					retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
					retorno.StackErro = validaBarcodeAp;
					break;
			}    
	   
			return retorno;
		}

		/// <summary>
		/// Recupera a quantidade de itens associados a determinado lote
		/// </summary>
		/// <param name="numLote"></param>
		/// <returns></returns>
		public int QtdeCaixasColetivasNoLote(string numLote)
		{
			return DaoEmbalagem.QtdeCaixasColetivasNoLote(numLote);
		}

		/// <summary>
		/// Recupera a quantidade de itens embalados em determinada caixa coletiva
		/// </summary>
		/// <param name="numCC"></param>
		/// <returns></returns>
		public int QuantidadeItensEmbaladosCaixaColetiva (string numCC)
		{
			return DaoEmbalagem.QuantidadeItensEmbaladosCaixaColetiva(numCC);
		}
		
		public bool AlterarTamanhoCaixaColetiva(int codLinha, int tamanho)
		{
			return DaoEmbalagem.AlterarTamanhoCaixaColetiva(codLinha, tamanho);            
		}

		public bool AlterarTamanhoLote(int codLinha, int tamanho)
		{
			return DaoEmbalagem.AlterarTamanhoLote(codLinha, tamanho);
		}

		public static List<ClassesAuxiliaresEmbalagem.DadosHoraHora> carregaHoraHoraFEC (string linha)
		{
			List<ClassesAuxiliaresEmbalagem.DadosHoraHora> dadosHoraHora = DaoEmbalagem.SU_GetHoraHoraFEC(DateTime.Now, linha);

			return dadosHoraHora;
		}

		public static List<ClassesAuxiliaresEmbalagem.DadosHoraHora> carregaHoraHoraPlaca (string linha)
		{
			List<ClassesAuxiliaresEmbalagem.DadosHoraHora> dadosHoraHora = DaoEmbalagem.SU_GetPlacaHoraHora(DateTime.Now, linha);

			return dadosHoraHora;
		}

		public static ClassesAuxiliaresEmbalagem.ResultadoEmbalagemProduto ExecProcedure_EmbalarProduto (CBEmbaladaDTO embalada)
		{
			var dadosEmbalada = DaoEmbalagem.Exec_Procedure_EmbalarProduto(embalada);

			return dadosEmbalada;
		}

		public static CBLoteDTO RecuperarDadosLote (string numLote)
		{
			return DaoEmbalagem.RecuperarDadosLote(numLote);
		}

		public static CBLoteConfigDTO RecuperarDadosCC(int codLinha)
		{
			return DaoEmbalagem.RecuperarDadosCC(codLinha);
		}

		public String ObterTotalProduzidoCaixaColetiva (string codFabrica, string numAp, int codLinha)
		{
			// recuperar o número da caixa coletiva aberta na linha e AP
			string numCC = RecuperarNumeroCaixaColetivaAberta(numAp, codLinha, codFabrica);
			int qtdeEmb = 0;

			// se não encontrou caixa coletiva aberta na AP, verifica se há aberta em outra AP  (do mesmo modelo)
			if (String.IsNullOrEmpty(numCC))
			{
				// buscando informações da AP
				var ap = Geral.ObterAP(codFabrica, numAp);

				if (ap != null) { 

					// localiza caixa coletiva aberta
					numCC = RecuperarNumeroCaixaColetivaAberta(codLinha, ap.CodModelo);

					// se encontrar, altera a AP se for do mesmo modelo
					if (!String.IsNullOrEmpty(numCC)) { 
						// altera o código do modelo no registro do lote
						DaoEmbalagem.AlterarAPLoteAberto(numCC, codFabrica, numAp);
					}

				}
				else
				{
					// localiza caixa coletiva aberta
					numCC = RecuperarNumeroCaixaColetivaAberta(codLinha);

					var mesmoModelo = DaoEmbalagem.VerificarModeloAPAbertaNovaAP(numCC, codFabrica, numAp);

					// se encontrar, altera a AP se for do mesmo modelo
					if (!String.IsNullOrEmpty(numCC) && mesmoModelo)
					{
						// altera o código do modelo no registro do lote
						DaoEmbalagem.AlterarAPLoteAberto(numCC, codFabrica, numAp);
					}
				}

			}

			// se encontrar uma caixa coletiva aberta, pega a quantidade
			if (!string.IsNullOrEmpty(numCC))
				qtdeEmb = DaoEmbalagem.QuantidadeItensEmbaladosCaixaColetiva(numCC);

			return qtdeEmb.ToString();

		}

		public string ObterTotalProduzidoCaixaColetiva(string codFabrica, string numAp, int codLinha, string codModelo)
		{
			throw new NotImplementedException();
		}

	   
		//public static CBLoteDTO RecuperarDadosCaixaColetiva (string numAP, string codModelo, int codlinha, string codFabrica)
		//{
		//    return DaoEmbalagem.RecuperarDadosCaixaColetiva(numAP, codModelo, codlinha, codFabrica);
		//}

		public static CBPesoEmbalagemDTO RecuperarPesoEmbalagem (string codModelo)
		{
			return DaoEmbalagem.RecuperarPesoEmbalagem(codModelo);
		}

		public static List<CBEtiquetaDTO> RecuperarEtiquetasPorPosto (int codLinha, int numPosto, bool ativo)
		{
			return DaoEmbalagem.RecuperarEtiquetasPorPosto(codLinha, numPosto, ativo);
		}

		public static bool AtivarImpressaoDeEtiqueta (int codLinha, int numPosto, string nomeEtiqueta)
		{
			return DaoEmbalagem.AtivarImpressaoDeEtiqueta(codLinha, numPosto, nomeEtiqueta);
		}

		public static string ObterModeloFase (string codModelo, string fase)
		{
			return daoPainelProducao.ObterModeloFase(codModelo, fase);
		}

		private static bool AtualizarStatusBloqueioPosto(int codLinha, int codPosto, string NovoStatus)
		{
			return DaoEmbalagem.BloquearPosto(codLinha, codPosto, NovoStatus);
		}

		public static bool BloquearPostoSeLeituraRepetida(Passagem passagem, string numEcb)
		{
			CBPassagemDTO passagemDto = new CBPassagemDTO()
			{
				CodLinha = passagem.posto.CodLinha ,
				NumPosto = passagem.posto.NumPosto,
				NumECB = numEcb            
			};

			DateTime datLeituraPassagem = Convert.ToDateTime(DaoEmbalagem.RecuperarHoraPassagem(passagemDto));

			var tempoMinutos = (DateTime.Now - datLeituraPassagem).TotalMinutes;

			if (passagem.posto.TempoBloqueio <= 0 || !(tempoMinutos >= passagem.posto.TempoBloqueio)) return false;
		 
			AtualizarStatusBloqueioPosto(passagem.posto.CodLinha, passagem.posto.NumPosto, "S");
			return true;
		}

		public static bool DesbloquearPosto(Passagem passagem)
		{
			return AtualizarStatusBloqueioPosto(passagem.posto.CodLinha, passagem.posto.NumPosto, "N");
		}

		public static bool ApontarProducao(string codFab, string numAP, string codLocalDeb, int qtdProduzida, string codTraInsumo, DateTime datTra, string codTraModelo,
											string codModelo, string codLocalCre, string reprocessa, string baixaCM, string rotina, string usuario,
											string codCin, DateTime datLancamento, int turno)
		{

			var dados = Embalagem.ApontarProducao(codFab, numAP, codLocalDeb, qtdProduzida, codTraInsumo, datTra, codTraModelo,
											codModelo, codLocalCre, reprocessa, baixaCM, rotina, usuario,
											codCin, datLancamento, turno);

			return dados;

		}
	}
}
