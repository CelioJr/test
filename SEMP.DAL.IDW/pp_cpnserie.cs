namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_cpnserie
    {
        [Key]
        public long id_cpserie { get; set; }

        public long? id_cp { get; set; }

        public long? id_nserie { get; set; }

        public virtual dw_nserie dw_nserie { get; set; }

        public virtual pp_cp pp_cp { get; set; }
    }
}
