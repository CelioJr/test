using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_IDIOMA_DTO
    {
        public FR_IDIOMA_DTO()
        {
            this.FR_TRADUCAO_IDIOMA = new List<FR_TRADUCAO_IDIOMA_DTO>();
        }

        public int IDI_CODIGO { get; set; }
        public string IDI_NOME { get; set; }
        public string IDI_SIGLA { get; set; }
        public int IDI_ORDEM { get; set; }
        public virtual ICollection<FR_TRADUCAO_IDIOMA_DTO> FR_TRADUCAO_IDIOMA { get; set; }
    }
}
