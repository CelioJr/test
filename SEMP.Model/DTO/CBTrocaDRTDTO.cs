using System;

namespace SEMP.Model.DTO
{
    public class CBTrocaDRTDTO
    {
        public int CodLinha { get; set; }
        public Nullable<int> NumPosto { get; set; }
        public string CodDRT { get; set; }
        public Nullable<System.DateTime> DatTroca { get; set; }
        public string CodDRTAnt { get; set; }
    }
}
