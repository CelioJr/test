namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolreoco
    {
        [Key]
        public long id_consolreoco { get; set; }

        public long? id_consolrelog { get; set; }

        public long? id_consolre { get; set; }
    }
}
