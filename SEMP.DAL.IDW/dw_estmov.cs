namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_estmov
    {
        [Key]
        public long id_estmov { get; set; }

        public DateTime? dthr_mov { get; set; }

        public short? tp_mov { get; set; }

        public long id_estpro { get; set; }

        public short? tp_origem { get; set; }

        public short? is_efetivado { get; set; }

        public decimal? qt_ajuste { get; set; }

        public decimal? qt_entrada_ant { get; set; }

        public decimal? qt_saida_ant { get; set; }

        public decimal? qt_reservada_ant { get; set; }

        public long id_usr { get; set; }

        public DateTime? dthr_cadastro { get; set; }

        public decimal? qt_ajuste_ant { get; set; }

        public long? id_turno { get; set; }

        public long? id_pt { get; set; }

        public long? id_gt { get; set; }

        [StringLength(40)]
        public string lancamento { get; set; }

        public int? ano { get; set; }

        public int? mes { get; set; }

        public decimal? qt_total_ant { get; set; }

        public decimal? qt_total { get; set; }

        public long? id_estlocalpro { get; set; }

        public virtual dw_estlocalpro dw_estlocalpro { get; set; }

        public virtual dw_estpro dw_estpro { get; set; }

        public virtual om_gt om_gt { get; set; }

        public virtual om_pt om_pt { get; set; }

        public virtual dw_turno dw_turno { get; set; }

        public virtual om_usr om_usr { get; set; }
    }
}
