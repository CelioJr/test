using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GpaMovimentacaoDTO
    {
        public string CodItem { get; set; }
        public string DocInterno1 { get; set; }
        public string DocInterno2 { get; set; }
        public string CodArea { get; set; }
        public string CodLocal { get; set; }
        public System.DateTime DataInicial { get; set; }
        public string TipDocto1 { get; set; }
        public string DocInterno3 { get; set; }
        public System.DateTime DataMovimentacao { get; set; }
        public Nullable<System.DateTime> DataFinal { get; set; }
        public string CodDRT { get; set; }
        public string Operacao { get; set; }
        public string CodBloqueio { get; set; }
    }
}
