﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using RAST.BO.Rastreabilidade;
using RAST.BO.Relatorios;
using SEMP.Model.DTO;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using SEMP.Models;

namespace SEMP.Areas.Coleta.Controllers
{
	public class AcompanhamentoController : Controller
	{
		//
		// GET: /Coleta/Acompanhamento/

		public ActionResult Index()
		{
			return View();
		}

		public ActionResult Lista(String datInicio, String datFim, String codLinha, String Codigo,
			String NumSerie, String Tipo, String fase, string flgContingencia)
		{
			Response.AddHeader("content-disposition", "attachment; filename=Acompanhamento.xls");
			Response.ContentType = "application/ms-excel";

			List<LeiturasMF> leituras = new List<LeiturasMF>();
			try
			{
				var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
				var dFim = dIni;

				if (!String.IsNullOrEmpty(datInicio))
				{
					dIni = DateTime.Parse(datInicio);
				}
				if (!String.IsNullOrEmpty(datFim))
				{
					dFim = DateTime.Parse(datFim);
				}

				if (String.IsNullOrEmpty(codLinha))
				{
					return View(new List<LeiturasMF>());
				}

				if (!String.IsNullOrEmpty(fase) && (fase.Equals("IMC") || fase.Equals("IAC")) || fase.Equals("LCM"))
				{
					leituras = Acompanhamento.ObterLeiturasIMC(dIni, dFim, codLinha, Codigo, NumSerie);
				}
				else if (fase.Equals("DAT"))
				{
					leituras = Acompanhamento.ObterLeiturasDAT(dIni, dFim, codLinha, Codigo, NumSerie);
				}
				else
				{
					leituras = Acompanhamento.ObterLeituras(dIni, dFim, codLinha, Codigo, NumSerie, Tipo);

					if (!String.IsNullOrEmpty(flgContingencia)){

						leituras = leituras.Where(x => x.CodTestador.Trim() != "SAP1").ToList();

					}

				}

			}
			catch (Exception ex)
			{
				Log.AppLog().Error(ex, $"\r\n{ex.StackTrace}\r\n{ex.TargetSite.Name}");
				//Log.AppLog().ErrorException($"\r\n{ex.StackTrace}\r\n{ex.TargetSite.Name}", ex);
				ViewBag.Erro = "Erro ao consultar";
			}

			return View(leituras);
		}

		public ActionResult GerarExcel(String datInicio, String datFim, String codLinha, String Codigo,
			String NumSerie, String Tipo, String fase)
		{
			Response.AddHeader("content-disposition", "attachment; filename=RelatorioTecnico.xls");
			Response.ContentType = "application/ms-excel";

			List<LeiturasMF> leituras = new List<LeiturasMF>();
			try
			{
				var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
				var dFim = dIni;

				if (!String.IsNullOrEmpty(datInicio))
				{
					dIni = DateTime.Parse(datInicio);
				}
				if (!String.IsNullOrEmpty(datFim))
				{
					dFim = DateTime.Parse(datFim);
				}

				if (String.IsNullOrEmpty(codLinha))
				{
					return View(new List<LeiturasMF>());
				}

				if (!String.IsNullOrEmpty(fase) && (fase.Equals("IMC") || fase.Equals("IAC")) || fase.Equals("LCM"))
				{
					leituras = Acompanhamento.ObterLeiturasIMC(dIni, dFim, codLinha, Codigo, NumSerie);
				}
				else if (fase.Equals("DAT"))
				{
					leituras = Acompanhamento.ObterLeiturasDAT(dIni, dFim, codLinha, Codigo, NumSerie);
				}
				else
				{
					leituras = Acompanhamento.ObterLeituras(dIni, dFim, codLinha, Codigo, NumSerie, Tipo);
				}
			}
			catch (Exception ex)
			{
				//Log.AppLog().ErrorException(string.Format("\r\n{0}\r\n{1}", ex.StackTrace, ex.TargetSite.Name), ex);
				Log.AppLog().Error(ex, "\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name);
				ViewBag.Erro = "Erro ao consultar";
			}

			return View(leituras);
		}

		public ActionResult GerarExcelSAP(String datInicio, String datFim)
		{
			Response.AddHeader("content-disposition", "attachment; filename=SeriesSAP.xls");
			Response.ContentType = "application/ms-excel";

			List<viw_BarrasSAPContingenciaDTO> leituras = new List<viw_BarrasSAPContingenciaDTO>();
			try
			{
				var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
				var dFim = dIni;

				if (!String.IsNullOrEmpty(datInicio))
				{
					dIni = DateTime.Parse(datInicio);
				}
				if (!String.IsNullOrEmpty(datFim))
				{
					dFim = DateTime.Parse(datFim);
				}

				leituras = Acompanhamento.ObterLeiturasSAP(dIni, dFim);
			}
			catch (Exception ex)
			{
				//Log.AppLog().ErrorException(string.Format("\r\n{0}\r\n{1}", ex.StackTrace, ex.TargetSite.Name), ex);
				Log.AppLog().Error(ex, "\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name);
				ViewBag.Erro = "Erro ao consultar";
			}

			return View(leituras);
		}

		public ActionResult ListaAP(String codFab, String numAP, String datInicio, String datFim, String codLinha)
		{
			List<LeiturasMF> leituras = new List<LeiturasMF>();
			try
			{
				DateTime? dIni = null;
				DateTime? dFim = null;

				if (!String.IsNullOrEmpty(datInicio))
				{
					dIni = DateTime.Parse(datInicio);
				}
				if (!String.IsNullOrEmpty(datFim))
				{
					dFim = DateTime.Parse(datFim);
				}

				if (String.IsNullOrEmpty(numAP) || String.IsNullOrEmpty(codFab))
				{
					return View(new List<LeiturasMF>());
				}
				
				leituras = Acompanhamento.ObterLeituras(codFab, numAP, dIni, dFim, codLinha);

				var ap = Geral.ObterAP(codFab, numAP);
				ViewBag.Fase = ap.CodProcesso.StartsWith("03") ? "FEC" : "PCI";

				
			}
			catch (Exception ex)
			{
				Log.AppLog().Error(ex, "\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name);
				//Log.AppLog().ErrorException(string.Format("\r\n{0}\r\n{1}", ex.StackTrace, ex.TargetSite.Name), ex);
				ViewBag.Erro = "Erro ao consultar";
			}

			return View(leituras);
		}
	}
}
