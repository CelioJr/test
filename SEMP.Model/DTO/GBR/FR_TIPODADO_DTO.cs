using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_TIPODADO_DTO
    {
        public int TPD_CODIGO { get; set; }
        public string TPD_DESCRICAO { get; set; }
        public string TPD_MASCARAFORMATACAO { get; set; }
        public string TPD_MASCARAEDICAO { get; set; }
    }
}
