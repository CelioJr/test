using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBPadraoIndicadorMap : EntityTypeConfiguration<tbl_CBPadraoIndicador>
    {
        public tbl_CBPadraoIndicadorMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Indicador, t.Fabrica, t.CodLinha });

            // Properties
            this.Property(t => t.Indicador)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Fabrica)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("tbl_CBPadraoIndicador");
            this.Property(t => t.Indicador).HasColumnName("Indicador");
            this.Property(t => t.Fabrica).HasColumnName("Fabrica");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.Valor).HasColumnName("Valor");
        }
    }
}
