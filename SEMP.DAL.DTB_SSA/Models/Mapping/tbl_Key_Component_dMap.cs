using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.DTB_SSA.Models.Mapping
{
    public class tbl_Key_Component_dMap : EntityTypeConfiguration<tbl_Key_Component_d>
    {
        public tbl_Key_Component_dMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Model_Number, t.Serial_Number, t.MFG_Part_Number, t.MFG_Serial_Number });

            // Properties
            this.Property(t => t.Model_Number)
                .IsRequired()
                .HasMaxLength(15);

            this.Property(t => t.Serial_Number)
                .IsRequired()
                .HasMaxLength(15);

            this.Property(t => t.MFG_Part_Number)
                .IsRequired()
                .HasMaxLength(13);

            this.Property(t => t.MFG_Serial_Number)
                .IsRequired()
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("tbl_Key_Component_d");
            this.Property(t => t.Model_Number).HasColumnName("Model_Number");
            this.Property(t => t.Serial_Number).HasColumnName("Serial_Number");
            this.Property(t => t.MFG_Part_Number).HasColumnName("MFG_Part_Number");
            this.Property(t => t.MFG_Serial_Number).HasColumnName("MFG_Serial_Number");
            this.Property(t => t.Data_Registro).HasColumnName("Data_Registro");
            this.Property(t => t.cod_operador).HasColumnName("cod_operador");
            this.Property(t => t.Data_Desativacao).HasColumnName("Data_Desativacao");
            this.Property(t => t.cod_OpeDesat).HasColumnName("cod_OpeDesat");
            this.Property(t => t.dispositivo_id).HasColumnName("dispositivo_id");

            // Relationships
            this.HasRequired(t => t.tbl_ItemEstoque)
                .WithMany(t => t.tbl_Key_Component_d)
                .HasForeignKey(d => d.MFG_Part_Number);
            this.HasRequired(t => t.tbl_MFG_Build_d)
                .WithMany(t => t.tbl_Key_Component_d)
                .HasForeignKey(d => new { d.Model_Number, d.Serial_Number });

        }
    }
}
