﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Materiais
{
	public class ResumoMesReposicao
	{
		public int? QtdTotalRomaneio { get; set; }
		public int? QtdTotalItens { get; set; }
		public int? QtdAprovada { get; set; }
		public int? QtdRecebido { get; set; }
		public int? QtdPaga { get; set; }
	}
}
