using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBTATempoItemMap : EntityTypeConfiguration<tbl_CBTATempoItem>
    {
        public tbl_CBTATempoItemMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodModelo, t.NumECB, t.Fase, t.SubFase, t.CodLinha });

            // Properties
            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumECB)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.Fase)
                .IsRequired()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.SubFase)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Observacao)
                .HasMaxLength(300)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_CBTATempoItem");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.NumECB).HasColumnName("NumECB");
            this.Property(t => t.Fase).HasColumnName("Fase");
            this.Property(t => t.SubFase).HasColumnName("SubFase");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.DatEntrada).HasColumnName("DatEntrada");
            this.Property(t => t.DatSaida).HasColumnName("DatSaida");
            this.Property(t => t.TempoAtravessa).HasColumnName("TempoAtravessa");
            this.Property(t => t.Observacao).HasColumnName("Observacao");
            this.Property(t => t.QtdDefeito).HasColumnName("QtdDefeito");
            this.Property(t => t.TempoCorrido).HasColumnName("TempoCorrido");
            this.Property(t => t.TempoTecnico).HasColumnName("TempoTecnico");
        }
    }
}
