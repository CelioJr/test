namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_LIST_PALLET
    {
        [Key]
        public int COD_LIST_PALLET { get; set; }

        [StringLength(10)]
        public string PACK_STRING { get; set; }

        [StringLength(10)]
        public string PALLET_STRING { get; set; }

        public int? COD_OP { get; set; }

        public int? COD_SKU { get; set; }

        public int? COD_MODELO { get; set; }

        public int? COD_USUARIO { get; set; }

        public bool? FECHADO { get; set; }

        public DateTime? DATA { get; set; }

        public bool? ETIQUETA_VALIDADA { get; set; }

        public DateTime? ETIQUETA_DATA { get; set; }

        public bool? CQ_VALIDADA { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PESO { get; set; }

        public bool? REMESSA_IMPRESSA { get; set; }

        public int? COD_REMESSA { get; set; }
    }
}
