using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_BarrasProducaoItem
    {
        public string CodModelo { get; set; }
        public string NumSerieModelo { get; set; }
        public Nullable<System.DateTime> DatLeitura { get; set; }
        public string CodItem { get; set; }
        public string NumSerieItem { get; set; }
        public string CodTipoItem { get; set; }
    }
}
