namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_ft_etapa
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_ft_etapa()
        {
            dw_expcvs = new HashSet<dw_expcvs>();
            dw_ft_sub = new HashSet<dw_ft_sub>();
            dw_passtf = new HashSet<dw_passtf>();
        }

        [Key]
        public long id_ft_etapa { get; set; }

        [StringLength(100)]
        public string ds_etapa { get; set; }

        [StringLength(100)]
        public string ds_mensagemok { get; set; }

        [StringLength(100)]
        public string ds_mensagemnok { get; set; }

        [StringLength(30)]
        public string cd_etapa { get; set; }

        public int? revisao { get; set; }

        public short? st_ativo { get; set; }

        public DateTime? dt_revisao { get; set; }

        public DateTime? dt_stativo { get; set; }

        public long id_usrrevisao { get; set; }

        public long id_usrstativo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_expcvs> dw_expcvs { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_ft_sub> dw_ft_sub { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_passtf> dw_passtf { get; set; }
    }
}
