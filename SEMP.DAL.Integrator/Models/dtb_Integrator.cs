namespace SEMP.DAL.Integrator
{
	using System;
	using System.Data.Entity;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	public partial class dtb_Integrator : DbContext
	{
#if DEBUG
		public dtb_Integrator()
			: base("name=dtb_IntegratorDEBUG")
		{
		}
#else
		public dtb_Integrator()
			: base("name=dtb_Integrator")
		{
		}
#endif

		public virtual DbSet<ORDER_CONDITIONS_IN> ORDER_CONDITIONS_IN { get; set; }
		public virtual DbSet<ORDER_HEADER_IN> ORDER_HEADER_IN { get; set; }
		public virtual DbSet<ORDER_ITEMS_IN> ORDER_ITEMS_IN { get; set; }
		public virtual DbSet<ORDER_PARTNERS> ORDER_PARTNERS { get; set; }
		public virtual DbSet<ORDER_SCHEDULES_IN> ORDER_SCHEDULES_IN { get; set; }
		public virtual DbSet<ORDER_TEXT> ORDER_TEXT { get; set; }
		public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
		public virtual DbSet<tbl_acao> tbl_acao { get; set; }
		public virtual DbSet<tbl_criticidade> tbl_criticidade { get; set; }
		public virtual DbSet<tbl_dados> tbl_dados { get; set; }
		public virtual DbSet<tbl_erros> tbl_erros { get; set; }
		public virtual DbSet<tbl_historico> tbl_historico { get; set; }
		public virtual DbSet<tbl_logs> tbl_logs { get; set; }
		public virtual DbSet<tbl_LogsServico> tbl_LogsServico { get; set; }
		public virtual DbSet<tbl_pai> tbl_pai { get; set; }
		public virtual DbSet<tbl_ProcessaApontFinal> tbl_ProcessaApontFinal { get; set; }
		public virtual DbSet<tbl_ProcessaOrdens> tbl_ProcessaOrdens { get; set; }
		public virtual DbSet<tbl_retorno> tbl_retorno { get; set; }
		public virtual DbSet<tbl_setup> tbl_setup { get; set; }
		public virtual DbSet<tbl_status> tbl_status { get; set; }
		public virtual DbSet<tbl_TAREFA> tbl_TAREFA { get; set; }
		public virtual DbSet<tbl_tipo> tbl_tipo { get; set; }
		public virtual DbSet<tbl_ZApontamento> tbl_ZApontamento { get; set; }
		public virtual DbSet<tmp_APsMarco> tmp_APsMarco { get; set; }
		public virtual DbSet<ZCSD_INTERFACE_CAB_NF> ZCSD_INTERFACE_CAB_NF { get; set; }
		public virtual DbSet<ZCSD_INTERFACE_CAB_ORDEM> ZCSD_INTERFACE_CAB_ORDEM { get; set; }
		public virtual DbSet<ZCSD_INTERFACE_DIV_REMESSA> ZCSD_INTERFACE_DIV_REMESSA { get; set; }
		public virtual DbSet<ZCSD_INTERFACE_ITEM_NF> ZCSD_INTERFACE_ITEM_NF { get; set; }
		public virtual DbSet<ZCSD_INTERFACE_ITEM_ORDEM> ZCSD_INTERFACE_ITEM_ORDEM { get; set; }
		public virtual DbSet<ZCSD_INTERFACE_REM_EMBARQUE> ZCSD_INTERFACE_REM_EMBARQUE { get; set; }
		public virtual DbSet<ZSPP_AP> ZSPP_AP { get; set; }
		public virtual DbSet<ZSPP_APONT_FINAL> ZSPP_APONT_FINAL { get; set; }
		public virtual DbSet<ZSPP_APONTAMENTO_PCI> ZSPP_APONTAMENTO_PCI { get; set; }
		public virtual DbSet<ZSPP_APONTAMENTO_PCI_LOG> ZSPP_APONTAMENTO_PCI_LOG { get; set; }
		public virtual DbSet<ZSPP_APONTAMENTO_SISAP> ZSPP_APONTAMENTO_SISAP { get; set; }
		public virtual DbSet<ZSPP_LISTA_TECNICA> ZSPP_LISTA_TECNICA { get; set; }
		public virtual DbSet<ZSPP_NUMERACAO> ZSPP_NUMERACAO { get; set; }
		public virtual DbSet<ZSPP_OP_PCI_LOG> ZSPP_OP_PCI_LOG { get; set; }
		public virtual DbSet<ZSPP_ORDEM_PRODUCAO> ZSPP_ORDEM_PRODUCAO { get; set; }
		public virtual DbSet<ZSPP_SISTEMA_ORIGEM> ZSPP_SISTEMA_ORIGEM { get; set; }
		public virtual DbSet<ZSSD_GET_DADOS_CLIENTES> ZSSD_GET_DADOS_CLIENTES { get; set; }
		public virtual DbSet<ZSSD_GET_DADOS_LIST_TEC> ZSSD_GET_DADOS_LIST_TEC { get; set; }
		public virtual DbSet<ZSSD_GET_PARCEIRO> ZSSD_GET_PARCEIRO { get; set; }
		public virtual DbSet<tbl_status_backup> tbl_status_backup { get; set; }
		public virtual DbSet<tmp_Apontamento_PCI> tmp_Apontamento_PCI { get; set; }
		public virtual DbSet<ZSPP_APONTAMENTO_PCI_APAGADOS> ZSPP_APONTAMENTO_PCI_APAGADOS { get; set; }
		public virtual DbSet<ZSSD_GET_CREATE_ORDER> ZSSD_GET_CREATE_ORDER { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<ORDER_CONDITIONS_IN>()
				.Property(e => e.ITM_NUMBER)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_CONDITIONS_IN>()
				.Property(e => e.COND_TYPE)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_CONDITIONS_IN>()
				.Property(e => e.COND_VALUE)
				.HasPrecision(24, 9);

			modelBuilder.Entity<ORDER_CONDITIONS_IN>()
				.Property(e => e.KOEIN)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_HEADER_IN>()
				.Property(e => e.PACKNO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_HEADER_IN>()
				.Property(e => e.DOC_TYPE)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_HEADER_IN>()
				.Property(e => e.SALES_ORG)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_HEADER_IN>()
				.Property(e => e.DISTR_CHAN)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_HEADER_IN>()
				.Property(e => e.DIVISION)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_HEADER_IN>()
				.Property(e => e.SALES_OFF)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_HEADER_IN>()
				.Property(e => e.SALES_GRP)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_HEADER_IN>()
				.Property(e => e.PURCH_NO_C)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_HEADER_IN>()
				.Property(e => e.BSTNK)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_HEADER_IN>()
				.Property(e => e.ORD_REASON)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_HEADER_IN>()
				.Property(e => e.PMNTTRMS)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_HEADER_IN>()
				.Property(e => e.PYMT_METH)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_HEADER_IN>()
				.Property(e => e.DLV_BLOCK)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_HEADER_IN>()
				.Property(e => e.S_PROC_IND)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_HEADER_IN>()
				.Property(e => e.REF_DOC_L)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_HEADER_IN>()
				.Property(e => e.PURCH_DATE)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_HEADER_IN>()
				.Property(e => e.ORDCOMB_IN)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_HEADER_IN>()
				.Property(e => e.INCOTERMS1)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_HEADER_IN>()
				.HasMany(e => e.ORDER_ITEMS_IN)
				.WithRequired(e => e.ORDER_HEADER_IN)
				.HasForeignKey(e => e.ORDER_HEADER_IN_ID)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ORDER_HEADER_IN>()
				.HasMany(e => e.ORDER_PARTNERS)
				.WithRequired(e => e.ORDER_HEADER_IN)
				.HasForeignKey(e => e.ORDER_HEADER_IN_ID)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ORDER_HEADER_IN>()
				.HasMany(e => e.ORDER_TEXT)
				.WithRequired(e => e.ORDER_HEADER_IN)
				.HasForeignKey(e => e.ORDER_HEADER_IN_ID)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ORDER_ITEMS_IN>()
				.Property(e => e.ITM_NUMBER)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_ITEMS_IN>()
				.Property(e => e.ITEM_CATEG)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_ITEMS_IN>()
				.Property(e => e.MATERIAL)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_ITEMS_IN>()
				.Property(e => e.TARGET_QTY)
				.HasPrecision(15, 3);

			modelBuilder.Entity<ORDER_ITEMS_IN>()
				.Property(e => e.PLANT)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_ITEMS_IN>()
				.Property(e => e.STORE_LOC)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_ITEMS_IN>()
				.Property(e => e.ORDERID)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_ITEMS_IN>()
				.Property(e => e.PURCH_DATE)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_ITEMS_IN>()
				.Property(e => e.PO_DAT_S)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_ITEMS_IN>()
				.HasMany(e => e.ORDER_CONDITIONS_IN)
				.WithRequired(e => e.ORDER_ITEMS_IN)
				.HasForeignKey(e => e.ORDER_ITEMS_IN_ID)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ORDER_ITEMS_IN>()
				.HasMany(e => e.ORDER_SCHEDULES_IN)
				.WithRequired(e => e.ORDER_ITEMS_IN)
				.HasForeignKey(e => e.ORDER_ITEMS_IN_ID)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ORDER_PARTNERS>()
				.Property(e => e.PARTN_ROLE)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_PARTNERS>()
				.Property(e => e.PARTN_NUMB)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_PARTNERS>()
				.Property(e => e.NAME)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_PARTNERS>()
				.Property(e => e.STREET)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_PARTNERS>()
				.Property(e => e.HOUSE_NUM1)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_PARTNERS>()
				.Property(e => e.DISTRICT)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_PARTNERS>()
				.Property(e => e.HOUSE_NUM2)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_PARTNERS>()
				.Property(e => e.POSTL_CODE)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_PARTNERS>()
				.Property(e => e.CITY)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_PARTNERS>()
				.Property(e => e.REGION)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_PARTNERS>()
				.Property(e => e.COUNTRY)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_PARTNERS>()
				.Property(e => e.TRANSPZONE)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_PARTNERS>()
				.Property(e => e.TELEPHONE)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_PARTNERS>()
				.Property(e => e.TELEPHONE2)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_PARTNERS>()
				.Property(e => e.SMTP_ADDR)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_PARTNERS>()
				.Property(e => e.STCD2)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_PARTNERS>()
				.Property(e => e.STCD1)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_PARTNERS>()
				.Property(e => e.STCD3)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_PARTNERS>()
				.Property(e => e.NATPERS)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_SCHEDULES_IN>()
				.Property(e => e.REQ_QTY)
				.HasPrecision(15, 3);

			modelBuilder.Entity<ORDER_SCHEDULES_IN>()
				.Property(e => e.REQ_DATE)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_TEXT>()
				.Property(e => e.TEXT_ID)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ORDER_TEXT>()
				.Property(e => e.TEXT_LINE)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_acao>()
				.Property(e => e.ACAO)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_acao>()
				.HasMany(e => e.tbl_historico)
				.WithRequired(e => e.tbl_acao)
				.HasForeignKey(e => e.ID_ACAO)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<tbl_criticidade>()
				.Property(e => e.NOME)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_criticidade>()
				.HasMany(e => e.tbl_TAREFA)
				.WithRequired(e => e.tbl_criticidade)
				.HasForeignKey(e => e.ID_CRITICIDADE)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<tbl_erros>()
				.Property(e => e.CODIGO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<tbl_erros>()
				.Property(e => e.TEXTO)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_erros>()
				.HasMany(e => e.tbl_dados)
				.WithRequired(e => e.tbl_erros)
				.HasForeignKey(e => e.ID_ERRO)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<tbl_erros>()
				.HasMany(e => e.tbl_logs)
				.WithRequired(e => e.tbl_erros)
				.HasForeignKey(e => e.ID_ERRO)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<tbl_historico>()
				.Property(e => e.USUARIO)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_historico>()
				.Property(e => e.OBS)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_logs>()
				.Property(e => e.MSG)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_logs>()
				.Property(e => e.TIPO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<tbl_LogsServico>()
				.Property(e => e.NomPrograma)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_LogsServico>()
				.Property(e => e.Mensagem)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_pai>()
				.Property(e => e.CODAPP)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_pai>()
				.Property(e => e.NAME)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_retorno>()
				.Property(e => e.PACKNO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<tbl_retorno>()
				.Property(e => e.MSG)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_setup>()
				.Property(e => e.NOME)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_setup>()
				.Property(e => e.VALOR)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_status>()
				.Property(e => e.codapp)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_status>()
				.Property(e => e.name)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_TAREFA>()
				.Property(e => e.NOME)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_TAREFA>()
				.Property(e => e.DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_TAREFA>()
				.Property(e => e.ACTION)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_TAREFA>()
				.Property(e => e.COMANDO)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_TAREFA>()
				.Property(e => e.EMAILRESPONSAVEIS)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_TAREFA>()
				.HasMany(e => e.tbl_dados)
				.WithRequired(e => e.tbl_TAREFA)
				.HasForeignKey(e => e.ID_TAREFA)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<tbl_TAREFA>()
				.HasMany(e => e.tbl_historico)
				.WithRequired(e => e.tbl_TAREFA)
				.HasForeignKey(e => e.ID_TAREFA)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<tbl_TAREFA>()
				.HasMany(e => e.tbl_logs)
				.WithRequired(e => e.tbl_TAREFA)
				.HasForeignKey(e => e.ID_TAREFA)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<tbl_tipo>()
				.Property(e => e.NOME)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_tipo>()
				.HasMany(e => e.tbl_dados)
				.WithRequired(e => e.tbl_tipo)
				.HasForeignKey(e => e.ID_TIPO)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<tbl_tipo>()
				.HasMany(e => e.tbl_TAREFA)
				.WithRequired(e => e.tbl_tipo)
				.HasForeignKey(e => e.ID_TIPO)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<tbl_ZApontamento>()
				.Property(e => e.NumOrdem)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_ZApontamento>()
				.Property(e => e.CodHash)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<tmp_APsMarco>()
				.Property(e => e.NumOP)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_NF>()
				.Property(e => e.PACKNO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_NF>()
				.Property(e => e.EXTROW)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_NF>()
				.Property(e => e.DOCUMENTOSAP)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_NF>()
				.Property(e => e.NUMERONF)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_NF>()
				.Property(e => e.SERIENF)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_NF>()
				.Property(e => e.DATAEMISSAO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_NF>()
				.Property(e => e.DATALANCAMENTO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_NF>()
				.Property(e => e.EMPRESA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_NF>()
				.Property(e => e.LOCALNEGOCIO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_NF>()
				.Property(e => e.TIPONF)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_NF>()
				.Property(e => e.ENTRADASAIDA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_NF>()
				.Property(e => e.CLIENTE)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_NF>()
				.Property(e => e.CANCELADA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_NF>()
				.Property(e => e.DATACANCELAMENTO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_NF>()
				.Property(e => e.INCOTERMS)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.PACKNO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.EXTROW)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.NUMEROPEDIDO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.NUMEROPEDIDOSEMP)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.DATAPEDIDO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.TIPOORDEM)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.MOTIVOORDEM)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.VENDEDOR)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.GERENTE)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.DIRETOR)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.CONDICAOPAGAMENTO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.FORMAPAGAMENTO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.PEDIDOBLOQUEADO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.CODIGOCLIENTE)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.CNPJCLIENTE)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.CPFCLIENTE)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.CONSUMIDORFINAL)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.DATAENTREGA1)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.DATAENTREGA2)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.TEXTOAPROVACAO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.TEXTOCREDITO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.TEXTOLOGISTICO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.MODALIDADEFRETE)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.REFERENCIANF)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_CAB_ORDEM>()
				.Property(e => e.REFERENCIAINVOICE)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_DIV_REMESSA>()
				.Property(e => e.PACKNO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_DIV_REMESSA>()
				.Property(e => e.EXTROW)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_DIV_REMESSA>()
				.Property(e => e.NUMEROPEDIDO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_DIV_REMESSA>()
				.Property(e => e.NUMEROITEM)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_DIV_REMESSA>()
				.Property(e => e.DATAREMESSA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_DIV_REMESSA>()
				.Property(e => e.QTDORDEM)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_DIV_REMESSA>()
				.Property(e => e.QTDCONFIRMADA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.PACKNO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.EXTROW)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.DOCUMENTOSAP)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.ITEMDOCUMENTO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.MATERIAL)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.CENTRO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.CFOP)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.NCM)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.ORIGEM)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.CST)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.INVOICE)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.ITEMINVOICE)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.QUANTIDADE)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.VALORUNITARIO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.QUANTIDADETRIB)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.ALIQUOTAICMS)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.BASEICMS)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.BASEEXCLUIDA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.OUTRASBASES)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.VALORICMS)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.ALIQUOTAICMSST)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.BASEICMSST)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.VALORICMSST)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.ALIQUOTAIPI)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.BASEIPI)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.BASEEXCLUIDA1)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.OUTRASBASES1)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.VALORIPI)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.ALIQUOTAPIS)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.BASEPIS)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.VALORPIS)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.ALIQUOTACOFINS)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.BASECOFINS)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.VALORCOFINS)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.ALIQUOTAFCP)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.BASEFCP)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.VALORFCP)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.ALIQUOTADIFALORIG)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.BASEDIFALORIG)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.VALORDIFALORIG)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.ALIQUOTADIFALDEST)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.BASEDIFALDEST)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.VALORDIFALDEST)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.NRREMESSA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.ITEMREMESSA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.ORDEMVENDAS)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_NF>()
				.Property(e => e.ITEMORDEMVENDA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.PACKNO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.EXTROW)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.NUMEROPEDIDO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.NUMEROITEM)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.NEMATERIAL)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.CATEGORIAITEM)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.FABRICA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.DEPOSITO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.QUANTIDADE)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.PRECOUNITARIO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.ORDEMCOMPRA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.MOTIVORECUSA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.IMPOSTOSICMS)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.IMPOSTOSICMSST)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.IMPOSTOSIPI)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.IMPOSTOSPIS)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.IMPOSTOSCOFINS)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.IMPOSTOSFCP)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.IMPOSTOSDIFAL)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.VALORFRETE)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.JUROS)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.VERBA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.PRECOLIQUIDO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.PRECOREFERENCIA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.REFERENCIAINVOICE)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_ITEM_ORDEM>()
				.Property(e => e.REFERENCIAITEM)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.PACKNO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.EXTROW)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.NRREMESSA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.ITEMREMESSA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.DATAREMESSA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.MATERIAL)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.LOCALEXPEDICAO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.QTDREMESSA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.QTDCONFIRMADA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.TRANSPORTADORA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.DOCVENDA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.ITEMDOCVENDA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.LOCALORGTRANSP)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.TRANSPORTADORA1)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.DATAEMBARQUE)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.NUMERO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.SERIENF)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.NUMERONF)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.CONHECIMENTO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.NUMEROFATURA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.CODIGOOCORRENCIA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.DATAEMBARQUE1)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.OBJETORASTREIO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.FABRICA)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.ITINERARIO)
				.IsUnicode(false);

			modelBuilder.Entity<ZCSD_INTERFACE_REM_EMBARQUE>()
				.Property(e => e.INICIOTRANSPORTE)
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_AP>()
				.Property(e => e.PACKNO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_AP>()
				.Property(e => e.EXTROW)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_AP>()
				.Property(e => e.WERKS)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_AP>()
				.Property(e => e.AUFNR)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_AP>()
				.Property(e => e.TYPE)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_AP>()
				.Property(e => e.MATNR)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_AP>()
				.Property(e => e.ARBPL)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_AP>()
				.Property(e => e.OQTY)
				.HasPrecision(13, 3);

			modelBuilder.Entity<ZSPP_AP>()
				.Property(e => e.IAUFNR)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_AP>()
				.Property(e => e.IWERKS)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_AP>()
				.Property(e => e.IMATNR)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_AP>()
				.Property(e => e.BDMNG)
				.HasPrecision(13, 3);

			modelBuilder.Entity<ZSPP_AP>()
				.Property(e => e.MEINS)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_AP>()
				.Property(e => e.SORTF)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_AP>()
				.Property(e => e.MATKL)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_AP>()
				.Property(e => e.BAUGR)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_AP>()
				.Property(e => e.WEMNG)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_AP>()
				.Property(e => e.SYSTEM_STATUS)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_AP>()
				.Property(e => e.POSTP)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_AP>()
				.Property(e => e.DUMPS)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.PACKNO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.EXTROW)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.WERKS)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.AUFNR)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.MATNR1)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.EAN13)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.SERIAL1)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.CREDATE1)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.MATNR2)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.SERIAL2)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.CREDATE2)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.CRETIME2)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.CRETIME1)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.ARBPL)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.CODLINHA)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.CODPOSTO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.DRT)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.TPAMAR)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.GESME)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.STATCONF)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.DTCONF)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.MBLNR)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.STATVD)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.STBCKFLUSH)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.DTBCKFLUSH)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.USERNAME)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.SISAP)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONT_FINAL>()
				.Property(e => e.AUART)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONTAMENTO_PCI>()
				.Property(e => e.PACKNO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONTAMENTO_PCI>()
				.Property(e => e.EXTROW)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONTAMENTO_PCI>()
				.Property(e => e.AUFNR)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONTAMENTO_PCI>()
				.Property(e => e.YIELD)
				.HasPrecision(13, 3);

			modelBuilder.Entity<ZSPP_APONTAMENTO_PCI>()
				.Property(e => e.MEINS)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONTAMENTO_PCI_LOG>()
				.Property(e => e.PACKNO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONTAMENTO_PCI_LOG>()
				.Property(e => e.INTTROW)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONTAMENTO_SISAP>()
				.Property(e => e.PACKNO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_LISTA_TECNICA>()
				.Property(e => e.PACKNO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_LISTA_TECNICA>()
				.Property(e => e.EXTROW)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_LISTA_TECNICA>()
				.Property(e => e.AUFNR)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_LISTA_TECNICA>()
				.Property(e => e.WERKS)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_LISTA_TECNICA>()
				.Property(e => e.MATNR)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_LISTA_TECNICA>()
				.Property(e => e.BDMNG)
				.HasPrecision(13, 3);

			modelBuilder.Entity<ZSPP_LISTA_TECNICA>()
				.Property(e => e.MEINS)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_LISTA_TECNICA>()
				.Property(e => e.SORTF)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_LISTA_TECNICA>()
				.Property(e => e.MATKL)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_NUMERACAO>()
				.Property(e => e.Codigo)
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_OP_PCI_LOG>()
				.Property(e => e.EXTROW)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_OP_PCI_LOG>()
				.Property(e => e.AUFNR)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_OP_PCI_LOG>()
				.Property(e => e.YIELD)
				.HasPrecision(13, 3);

			modelBuilder.Entity<ZSPP_OP_PCI_LOG>()
				.Property(e => e.MEINS)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_OP_PCI_LOG>()
				.Property(e => e.MBLNR)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_OP_PCI_LOG>()
				.Property(e => e.USERNAME)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_OP_PCI_LOG>()
				.Property(e => e.TYPE)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_OP_PCI_LOG>()
				.Property(e => e.MATNR)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_OP_PCI_LOG>()
				.Property(e => e.MESSAGE)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_OP_PCI_LOG>()
				.HasMany(e => e.ZSPP_APONTAMENTO_PCI_LOG)
				.WithOptional(e => e.ZSPP_OP_PCI_LOG)
				.HasForeignKey(e => e.INTTROW);

			modelBuilder.Entity<ZSPP_ORDEM_PRODUCAO>()
				.Property(e => e.PACKNO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_ORDEM_PRODUCAO>()
				.Property(e => e.EXTROW)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_ORDEM_PRODUCAO>()
				.Property(e => e.WERKS)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_ORDEM_PRODUCAO>()
				.Property(e => e.AUFNR)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_ORDEM_PRODUCAO>()
				.Property(e => e.TYPE)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_ORDEM_PRODUCAO>()
				.Property(e => e.MATNR)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_ORDEM_PRODUCAO>()
				.Property(e => e.ARBPL)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_ORDEM_PRODUCAO>()
				.Property(e => e.OQTY)
				.HasPrecision(13, 3);

			modelBuilder.Entity<ZSPP_ORDEM_PRODUCAO>()
				.Property(e => e.ITEM)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_ORDEM_PRODUCAO>()
				.HasMany(e => e.ZSPP_LISTA_TECNICA)
				.WithRequired(e => e.ZSPP_ORDEM_PRODUCAO)
				.HasForeignKey(e => new { e.PACKNO, e.EXTROW, e.AUFNR })
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ZSPP_SISTEMA_ORIGEM>()
				.Property(e => e.PACKNO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_SISTEMA_ORIGEM>()
				.Property(e => e.INTTROW)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.PACKNO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.EXTROW)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.ORGANIZACAOVENDAS)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.CANALDISTRIBUICAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.SETORATIVIDADE)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.CODIGOSAP)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.CNPJ)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.CPF)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.INSCRICAOESTADUAL)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.NOME)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.RUA)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.NUMERO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.COMPLEMENTO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.CIDADE)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.BAIRRO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.CEP)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.UF)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.PAIS)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.RELEVANTEST)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.BLOQUEADO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.CODIGOSUFRAMA)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.PESSOAFISICA)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.TELEFONE1)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.TELEFONE2)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.TIPODECLARACAOEMPRESA)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.REGIMEPISCOFINS)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.VENDEDOR)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.GERENTE)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.GRUPOCONTASCLIENTE)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.EMAIL)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.HIERARQUIACLIENTE)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.ICMSTAXPAY)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.ZAHLS)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.BUKRS)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_CLIENTES>()
				.Property(e => e.OBS_INTEGRATOR)
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_LIST_TEC>()
				.Property(e => e.PACKNO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_LIST_TEC>()
				.Property(e => e.EXTROW)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_LIST_TEC>()
				.Property(e => e.FABRICA)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_LIST_TEC>()
				.Property(e => e.APARELHO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_LIST_TEC>()
				.Property(e => e.COMPONENTE)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_LIST_TEC>()
				.Property(e => e.MARCAALTERNATIVO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_LIST_TEC>()
				.Property(e => e.GRUPOALTERNATIVO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_LIST_TEC>()
				.Property(e => e.MODELO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_LIST_TEC>()
				.Property(e => e.DATAINICIO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_LIST_TEC>()
				.Property(e => e.POSICAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_LIST_TEC>()
				.Property(e => e.QUANTIDADE)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_LIST_TEC>()
				.Property(e => e.FINALIDADE)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_LIST_TEC>()
				.Property(e => e.MAST_ANDAT)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_LIST_TEC>()
				.Property(e => e.MAST_ANNAM)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_LIST_TEC>()
				.Property(e => e.MAST_AEDAT)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_LIST_TEC>()
				.Property(e => e.MAST_AENAM)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_LIST_TEC>()
				.Property(e => e.STKO_STLST)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_LIST_TEC>()
				.Property(e => e.STKO_DATUV)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_LIST_TEC>()
				.Property(e => e.STPO_ANDAT)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_LIST_TEC>()
				.Property(e => e.STPO_ANNAM)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_LIST_TEC>()
				.Property(e => e.STPO_AEDAT)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_LIST_TEC>()
				.Property(e => e.STPO_AENAM)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_DADOS_LIST_TEC>()
				.Property(e => e.STPO_DATUV)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_PARCEIRO>()
				.Property(e => e.PACKNO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_PARCEIRO>()
				.Property(e => e.EXTROW)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_PARCEIRO>()
				.Property(e => e.CLIENTE)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_PARCEIRO>()
				.Property(e => e.ORGANIZACAOVENDAS)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_PARCEIRO>()
				.Property(e => e.CANALDISTRIBUICAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_PARCEIRO>()
				.Property(e => e.SETORATIVIDADE)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_PARCEIRO>()
				.Property(e => e.FUNCAOPARCEIRO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_PARCEIRO>()
				.Property(e => e.CLIENTEPARCEIRO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_PARCEIRO>()
				.Property(e => e.FORNECEDOR)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<tbl_status_backup>()
				.Property(e => e.codapp)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_status_backup>()
				.Property(e => e.name)
				.IsUnicode(false);

			modelBuilder.Entity<tmp_Apontamento_PCI>()
				.Property(e => e.PACKNO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<tmp_Apontamento_PCI>()
				.Property(e => e.EXTROW)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<tmp_Apontamento_PCI>()
				.Property(e => e.AUFNR)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<tmp_Apontamento_PCI>()
				.Property(e => e.YIELD)
				.HasPrecision(13, 3);

			modelBuilder.Entity<tmp_Apontamento_PCI>()
				.Property(e => e.MEINS)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONTAMENTO_PCI_APAGADOS>()
				.Property(e => e.PACKNO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONTAMENTO_PCI_APAGADOS>()
				.Property(e => e.EXTROW)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONTAMENTO_PCI_APAGADOS>()
				.Property(e => e.AUFNR)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONTAMENTO_PCI_APAGADOS>()
				.Property(e => e.YIELD)
				.HasPrecision(13, 3);

			modelBuilder.Entity<ZSPP_APONTAMENTO_PCI_APAGADOS>()
				.Property(e => e.MEINS)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ZSPP_APONTAMENTO_PCI_APAGADOS>()
				.Property(e => e.pacote)
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_CREATE_ORDER>()
				.Property(e => e.PACKNO)
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_CREATE_ORDER>()
				.Property(e => e.EXTROW)
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_CREATE_ORDER>()
				.Property(e => e.TYPE)
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_CREATE_ORDER>()
				.Property(e => e.ID)
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_CREATE_ORDER>()
				.Property(e => e.NUMBER)
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_CREATE_ORDER>()
				.Property(e => e.MESSAGE)
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_CREATE_ORDER>()
				.Property(e => e.LOG_NO)
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_CREATE_ORDER>()
				.Property(e => e.LOG_MSG_NO)
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_CREATE_ORDER>()
				.Property(e => e.MESSAGE_V1)
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_CREATE_ORDER>()
				.Property(e => e.MESSAGE_V2)
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_CREATE_ORDER>()
				.Property(e => e.MESSAGE_V3)
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_CREATE_ORDER>()
				.Property(e => e.MESSAGE_V4)
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_CREATE_ORDER>()
				.Property(e => e.PARAMETER)
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_CREATE_ORDER>()
				.Property(e => e.ROW)
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_CREATE_ORDER>()
				.Property(e => e.FIELD)
				.IsUnicode(false);

			modelBuilder.Entity<ZSSD_GET_CREATE_ORDER>()
				.Property(e => e.SYSTEM)
				.IsUnicode(false);
		}
	}
}
