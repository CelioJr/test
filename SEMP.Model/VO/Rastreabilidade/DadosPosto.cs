﻿using System;

namespace SEMP.Model.VO.Rastreabilidade
{
	[Serializable]
	public class DadosPosto
	{
		public int CodLinha { get; set; }
		public int NumPosto { get; set; }
		public int CodTipoPosto { get; set; }
		public string DesPosto { get; set; }
        public string numIP { get; set; }
		public string flgAtivo { get; set; }
		public int NumSeq { get; set; }
		public string flgObrigatorio { get; set; }
		public string flgDRTObrig { get; set; }
		public string TipoAmarra { get; set; }
		public string flgPedeAP { get; set; }
		public string flgImprimeEtq { get; set; }
		public string LinhaCliente { get; set; }
		public int Turno { get; set; }
		public int QtdPrograma { get; set; }
		public string DesAcao { get; set; }
        public string NumIPBalanca { get; set; }
        public string PostoBloqueado { get; set; }
        public int? TempoBloqueio { get; set; }
        public bool flgValidaOrigem { get; set; }
		public string DesLinha { get; set; }
		public String Setor { get; set; }

		public DadosPosto()
		{
			CodLinha = -1;
			NumPosto = -1;
			CodTipoPosto = -1;
			DesPosto = "";
			flgAtivo = "";
			NumSeq = -1;
            numIP = "";
			flgObrigatorio = "";
			flgDRTObrig = "";
			TipoAmarra = "";
			flgPedeAP = "N";
			flgImprimeEtq = "N";
			LinhaCliente = "";
			Turno = 1;
			QtdPrograma = 0;
			DesAcao = "";
            NumIPBalanca = "";
		    PostoBloqueado = "";
		    TempoBloqueio = 0;
            flgValidaOrigem = false;
			DesLinha = "";
			Setor = "";
		}
	}
}
