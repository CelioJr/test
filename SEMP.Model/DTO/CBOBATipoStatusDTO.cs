﻿namespace SEMP.Model.DTO
{
	public class CBOBATipoStatusDTO
	{
		public string CodStatus { get; set; }
		public string DesStatus { get; set; }
		public string DesCor { get; set; }
	}
}
