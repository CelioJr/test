﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.Rastreabilidade;
using SEMP.Model.VO.Rastreabilidade;
using SEMP.Model.DTO;
using SEMP.Model.VO;
using SEMP.Model;
using System.Reflection;
using System.Text.RegularExpressions;

namespace SEMP.Areas.Coleta.Controllers
{
	/// <summary>
	/// Classe que controla as Action da funcionalidade Configuração
	/// </summary>
	public class ConfiguracaoController : Controller
	{				
		/// <summary>
		/// Método Index controla a pagina inicial do controller Configuracao.
		/// </summary>
		/// <returns>Retorna a View Index.cshtml</returns>
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult Parametros() {

			var enviaApontamento = Geral.ObterAcessosBarApontamento();

			ViewBag.EnviaApontamento = enviaApontamento;

			return View();
		}

		#region Etiquetas
		public ActionResult Etiquetas(string codLinha)
		{

			List<CBEtiquetaDTO> lista = Configuracao.ObterEtiquetas(codLinha);
			
		
			return View(lista);
		}


		public ActionResult EtiquetasPartial(string codLinha)
		{

			List<CBEtiquetaDTO> lista = Configuracao.ObterEtiquetas(codLinha);


			return PartialView(lista);
		}
		
		#endregion

		/// <summary>
		/// Método que controla a página que exibe todas as linhas da produção
		/// </summary>
		/// <returns>Retorna a View Linha.cshtml</returns>
		public ActionResult Linha(string flgStatus, string fase)
		{
			Configuracao config = new Configuracao();
			
			var dados = config.ObterLinhas();

			ViewBag.Linhas = dados;

			if (String.IsNullOrEmpty(flgStatus) || flgStatus == "1")
			{
				dados = dados.Where(x => x.FlgAtivo == "1").ToList();
			}
			else if (String.IsNullOrEmpty(flgStatus) || flgStatus == "0")
			{
				dados = dados.Where(x => x.FlgAtivo == "0").ToList();
			}

			if (!string.IsNullOrEmpty(fase))
			{
				dados = dados.Where(x => x.Setor == fase).ToList();
			}

			return View(dados.OrderBy(x => x.Setor).ThenBy(y => y.CodLinhaT1 != null ? y.CodLinhaT1.Substring(3) : y.DesLinha));
		}

		/// <summary>
		/// Método que controla a página de edição de linha
		/// </summary>
		/// <param name="id">Parâmetro com o código da linha que será editada</param>
		/// <returns>Retorna a View EditLinhas.cshtml</returns>
		public ActionResult EditLinhas(int id)
		{			
			Configuracao config = new Configuracao();

			CBLinhaDTO linha = config.ObterLinha(id);

			if (linha == null)
			{
				return HttpNotFound();
			}
			ViewBag.CodLinha = new SelectList("CodLinha", linha.CodLinha);

			var configuracao = new Configuracao();
			var fases = configuracao.ObterFases();
			ViewBag.fases = fases;

			return View(linha);
		}

		public ActionResult ViewLinha(int id)
		{
			Configuracao config = new Configuracao();

			CBLinhaDTO linha = config.ObterLinha(id);

			if (linha == null)
			{
				return HttpNotFound();
			}
			ViewBag.CodLinha = new SelectList("CodLinha", linha.CodLinha);

			var configuracao = new Configuracao();
			var fases = configuracao.ObterFases();
			ViewBag.fases = fases;

			var horarios = Configuracao.ObterHorarioIntervalo(id);
			var postos = configuracao.ObterPostos(id);

			ViewBag.Postos = postos;
			ViewBag.Horarios = horarios;

			return View(linha);
		}

		public ActionResult Postos(int id )
		{
			Configuracao config = new Configuracao();

            
            ViewBag.CodLinha = id;
            ViewBag.DesLinha = Configuracao.ObterDesLinha(id);
           

			return View(config.ObterPostos(id).ToList());
		}

        public ActionResult PostosTablet(int codLinha, int tipoPosto)
        {

            int NumPosto = Configuracao.ObterTipoPostoTablet(codLinha, tipoPosto);

            
            return Json(NumPosto, JsonRequestBehavior.AllowGet);
        }

		public ActionResult Horario()
		{
			Configuracao config = new Configuracao();

			return View(config.ObterHorarios().ToList());
		}


        public ActionResult PostoResumo(int CodLinha, int NumPosto)
        {
            Configuracao config = new Configuracao();

            ViewBag.DesPosto = Configuracao.ObterDesPosto(NumPosto);
            ViewBag.DesLinha = Configuracao.ObterDesLinha(CodLinha);


            CBPostoDTO posto = config.ObterPosto(CodLinha, NumPosto);


            if (posto == null)
            {
                return HttpNotFound();
            }

            return View(posto);

        }

		public ActionResult EditPosto(int CodLinha, int NumPosto)
		{		
			Configuracao config = new Configuracao();

			CBPostoDTO posto = config.ObterPosto(CodLinha, NumPosto);
			List<CBTipoPostoDTO> CodTipoPosto = config.ObterTiposPosto();
			List<CBTipoAmarraDTO> TipoAmarra = config.ObterTiposAmarra();

			if (posto == null)
			{
				return HttpNotFound();
			}
			
			ViewBag.CodLinha = new SelectList("CodLinha", posto.CodLinha);
			ViewBag.CodTipoPosto = new SelectList(CodTipoPosto.ToList(), "CodTipoPosto", "DesTipoPosto", posto.CodTipoPosto);		
			return View(posto);
		}

		public ActionResult EditHorario(int id)
		{
			Configuracao config = new Configuracao();

			CBHorarioDTO horario = config.ObterHorario(id);

			if (horario == null)
			{
				return HttpNotFound();
			}
			ViewBag.CodLinha = new SelectList("idHorario", horario.idHorario);
			return View(horario);
		}

		/// <summary>
		/// Método que faz a chamada para adicionar nova linha de produção
		/// </summary>
		/// <returns></returns>
		public ActionResult AddLinha()
		{
			var configuracao = new Configuracao();
			var fases = configuracao.ObterFases();
			ViewBag.fases = fases;

			return View();
		}

		/// <summary>
		/// Método que faz a chamada para adicionar nova linha de produção
		/// </summary>
		/// <returns></returns>
		public ActionResult AddHorario()
		{
			return View();
		}

		/// <summary>
		/// Método que faz chamada para página de adição de posto
		/// </summary>
		/// <returns></returns>
		/*public ActionResult AddPosto()
		{
			Configuracao config = new Configuracao();
			
			List<CBLinhaDTO> linha = config.ObterLinhas();
			List<CBTipoPostoDTO> CodTipoPosto = config.ObterTiposPosto();

			ViewBag.CodLinha = new SelectList(linha.ToList(), "CodLinha", "DesLinha", linha.FirstOrDefault());
			ViewBag.CodTipoPosto = new SelectList(CodTipoPosto.ToList(), "CodTipoPosto", "DesTipoPosto", CodTipoPosto.FirstOrDefault());

			return View();
		}*/

		public ActionResult AddPosto(int? id)
		{
			Configuracao config = new Configuracao();

			List<CBTipoPostoDTO> CodTipoPosto = config.ObterTiposPosto();

			var postos = config.ObterPostos(id ?? 0);
			var ultPosto = postos.Count() > 0 ? postos.Max(x => x.NumPosto) : 0;
			

			ViewBag.CodLinha = id;
			ViewBag.ultPosto = ultPosto;
			ViewBag.CodTipoPosto = new SelectList(CodTipoPosto.ToList(), "CodTipoPosto", "DesTipoPosto", CodTipoPosto.FirstOrDefault());

			return View(new CBPostoDTO());
		}

		/// <summary>
		/// Método que faz chamada para a página de Configuração de Máscara
		/// </summary>
		/// <returns></returns>
		public ActionResult Mascara()
		{
			Configuracao config = new Configuracao();

			return View(config.ObterPerfilMascara().ToList());
		}

		public ActionResult AddMascara()
		{			

			return View();
		}

		/// <summary>
		/// Método para exibir as linhas de produção
		/// </summary>
		/// <param name="sidx"></param>
		/// <param name="sord"></param>
		/// <param name="page"></param>
		/// <param name="rows"></param>
		/// <returns></returns>
		public ActionResult ObterLinhas(string sidx, string sord, int page, int rows)
		{
			Configuracao config = new Configuracao();
			var context = config.ObterLinhas();


			int pageIndex = Convert.ToInt32(page) - 1;
			int pageSize = rows;
			int totalRecords = context.Count();
			int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

			var authors = context.OrderBy(a => sord).Skip(pageIndex * pageSize).Take(pageSize).ToList();

			var jsonData = new
			{
				total = totalPages,
				page,
				records = totalRecords,
				rows = (from n in authors
						select new
						{
							i = n.CodLinha,
							cell = new string[] { n.CodLinha.ToString(), n.DesLinha, n.Setor, n.Familia, n.CodLinhaT1 }
						}).ToArray()
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		public JsonResult EditarLinha(string DesLinha, string DesObs, string Setor, string Familia, string CodLinhaT1)
		{

			String retorno = "";

			return Json(retorno, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult EditLinhas(CBLinhaDTO tbl_cblinha)
		{			
			Configuracao config = new Configuracao();

			Boolean gravar = config.EditarLinha(tbl_cblinha.CodLinha, tbl_cblinha.DesLinha, tbl_cblinha.DesObs, tbl_cblinha.Setor, tbl_cblinha.Familia, tbl_cblinha.CodLinhaT1,
								tbl_cblinha.CodLinhaT2, tbl_cblinha.CodLinhaT3, tbl_cblinha.FlgAtivo, tbl_cblinha.CodLinhaOrigem, tbl_cblinha.SetorOrigem);

			if(gravar)
				ViewBag.JS = "alert('Linha editada com sucesso.'); location.href ='/Coleta/Configuracao/Linha/'";
			else
				ViewBag.JS = "Não foi possível editar a linha. Tente novamente.";

			return View(tbl_cblinha);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult EditPosto(CBPostoDTO tbl_cbposto)
		{
			Configuracao config = new Configuracao();
						
			Boolean gravar = config.EditarPosto(tbl_cbposto.CodLinha, tbl_cbposto.NumPosto, tbl_cbposto.CodTipoPosto, tbl_cbposto.DesPosto, 
												tbl_cbposto.FlgAtivo, tbl_cbposto.NumSeq, tbl_cbposto.flgObrigatorio.HasValue && (bool)tbl_cbposto.flgObrigatorio, tbl_cbposto.flgPedeAP,
												tbl_cbposto.flgImprimeEtq, tbl_cbposto.TempoLeitura ?? 0, tbl_cbposto.TipoEtiqueta ?? 0,
                                                tbl_cbposto.TempoCiclo ?? 0, tbl_cbposto.FlgGargalo, tbl_cbposto.flgValidaOrigem.HasValue && (bool)tbl_cbposto.flgValidaOrigem, tbl_cbposto.MontarLote.HasValue && (bool)tbl_cbposto.MontarLote,
												tbl_cbposto.FlgTipoTerminal);
												

			if (gravar)
				ViewBag.JS = "alert('Posto editado com sucesso.'); location.href ='/Coleta/Configuracao/Postos/" + tbl_cbposto.CodLinha + "'";
			else
				ViewBag.JS = "alert('Não foi possível editar o posto. Tente novamente.');";

			List<CBTipoPostoDTO> CodTipoPosto = config.ObterTiposPosto();
			ViewBag.CodLinha = new SelectList("CodLinha", tbl_cbposto.CodLinha);
			ViewBag.CodTipoPosto = new SelectList(CodTipoPosto.ToList(), "CodTipoPosto", "DesTipoPosto", tbl_cbposto.CodTipoPosto);


			return View(tbl_cbposto);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult AddLinha(CBLinhaDTO tbl_cblinha)
		{
			Configuracao config = new Configuracao();

			Mensagem retorno = config.InsereLinha(tbl_cblinha.CodLinha, tbl_cblinha.DesLinha, tbl_cblinha.DesObs, tbl_cblinha.Setor,
												tbl_cblinha.Familia, tbl_cblinha.CodLinhaT1);


			if (retorno.CodMensagem.Equals(Constantes.RAST_OK))
				ViewBag.JS = "alert('Linha criada com sucesso.'); location.href ='/Coleta/Configuracao/Linha/'";
			else
				ViewBag.JS = "alert('Não foi possível criar a nova linha: " + retorno.DesMensagem + "');";


			return View(tbl_cblinha);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult AddPosto(CBPostoDTO tbl_cbposto)
		{
			Configuracao config = new Configuracao();

			Boolean gravar = config.InserePosto(tbl_cbposto.CodLinha, tbl_cbposto.NumPosto, tbl_cbposto.CodTipoPosto, tbl_cbposto.DesPosto,
												tbl_cbposto.FlgAtivo, tbl_cbposto.NumSeq, tbl_cbposto.flgObrigatorio.HasValue && (bool)tbl_cbposto.flgObrigatorio, tbl_cbposto.flgPedeAP,
												tbl_cbposto.flgImprimeEtq, tbl_cbposto.TempoLeitura ?? 0, tbl_cbposto.TipoEtiqueta ?? 0,
												tbl_cbposto.TempoCiclo ?? 0, tbl_cbposto.FlgGargalo);


			if (gravar)
				ViewBag.JS = "alert('Posto criado com sucesso.'); location.href ='/Coleta/Configuracao/Postos/" + tbl_cbposto.CodLinha + "'";
			else
				ViewBag.JS = "alert('Não foi possíve criar o posto');";

			List<CBTipoPostoDTO> CodTipoPosto = config.ObterTiposPosto();
			//ViewBag.CodLinha = new SelectList("CodLinha", tbl_cbposto.CodLinha);
			ViewBag.CodLinha = tbl_cbposto.CodLinha;
			ViewBag.ultPosto = tbl_cbposto.NumPosto--;
			ViewBag.CodTipoPosto = new SelectList(CodTipoPosto.ToList(), "CodTipoPosto", "DesTipoPosto", tbl_cbposto.CodTipoPosto);
			return View(tbl_cbposto);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult AddHorario(CBHorarioDTO tbl_cbhorario)
		{
			Configuracao config = new Configuracao();

			Mensagem retorno = new Mensagem();

			retorno = config.InsereHorario((int)tbl_cbhorario.Turno, tbl_cbhorario.HoraInicio.ToString(), tbl_cbhorario.HoraFim.ToString());


			if (retorno.CodMensagem.Equals(Constantes.RAST_OK))
				ViewBag.JS = "alert('Horário criado com sucesso.'); location.href ='/Coleta/Configuracao/Horario/'";
			else
				ViewBag.JS = "alert('Não foi possíve criar o horário');";

			return View(tbl_cbhorario);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult EditHorario(CBHorarioDTO tbl_cbhorario)
		{
			Configuracao config = new Configuracao();

			Mensagem gravar = config.EditarHorario(tbl_cbhorario.idHorario, (int)tbl_cbhorario.Turno, tbl_cbhorario.HoraInicio.ToString(), tbl_cbhorario.HoraFim.ToString());


			if (gravar.CodMensagem.Equals(Constantes.RAST_OK))
				ViewBag.JS = "alert('Horário editado com sucesso.'); location.href ='/Coleta/Configuracao/Horario/'";
			else
				ViewBag.JS = "alert('Não foi possível editar o horário. Tente novamente.');";

			return View(tbl_cbhorario);
		}


		public ActionResult MapaLinhas() {

			var linhas = Geral.ObterLinhasCB();

			return View(linhas);
		}

		public JsonResult ObterTiposAmarra(int CodLinha, int NumPosto)
		{
			Configuracao config = new Configuracao();

			List<CBPerfilAmarraPostoDTO> perfil = config.ObterTipoAmarra(CodLinha, NumPosto);
			List<CBTipoAmarraDTO> tipo = config.ObterTiposAmarra();
			CBTipoAmarraDTO tipoAmarra = new CBTipoAmarraDTO();

			tipo = tipo.Where(x => !perfil.Select(p => p.CodTipoAmarra).Contains(x.CodTipoAmarra)).ToList();

			return Json(tipo, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterFases()
		{
			Configuracao config = new Configuracao();

			var perfil = config.ObterFases();

			return Json(perfil, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterTipoAmarra(int CodLinha, int NumPosto)
		{
			Configuracao config = new Configuracao();

			List<CBPerfilAmarraPostoDTO> perfil = config.ObterTipoAmarra(CodLinha, NumPosto);
			List<CBTipoAmarraDTO> tipo = new List<CBTipoAmarraDTO>();
			CBTipoAmarraDTO tipoAmarra = new CBTipoAmarraDTO();
			

			foreach (CBPerfilAmarraPostoDTO element in perfil)
			{
				tipoAmarra = new CBTipoAmarraDTO();
				tipoAmarra = config.ObterTipoAmarra(element.CodTipoAmarra);
				tipo.Add(tipoAmarra);
			}

			return Json(tipo, JsonRequestBehavior.AllowGet);
		}

		public JsonResult InserirTipoAmarra(int CodLinha, int NumPosto, string CodTipoPosto)
		{
			Mensagem retorno = new Mensagem();

			Configuracao config = new Configuracao();

			retorno = config.RemoverPerfilAmarraPosto(CodLinha, NumPosto);

			var array = CodTipoPosto.Split('|');

			for (int i = 0; i < array.Length; i++)
			{
				if (!string.IsNullOrWhiteSpace(array[i]))
				{
					retorno = config.InserirPerfilAmarraPosto(CodLinha, NumPosto, int.Parse(array[i]), (i+1));	
				}
			}

			return Json(retorno, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterHorarios()
		{
			Configuracao config = new Configuracao();

			List<CBHorarioDTO> horario = config.ObterHorarios();


			return Json(horario.ToList(), JsonRequestBehavior.AllowGet);
		}

		public JsonResult InserirLoteLinha(int CodLinha, int TamLote, int TamMagazine, string CodDRT)
		{
			Mensagem retorno = new Mensagem();

			Configuracao config = new Configuracao();

			retorno = config.InserirLoteConfig(CodLinha, TamLote, TamMagazine, CodDRT);

			return Json(retorno, JsonRequestBehavior.AllowGet);	
		}

		public JsonResult InserirHorarioLinha(int CodLinha, string IdHorario, string Setor, string CodLinhaT1)
		{
			Mensagem retorno = new Mensagem();

			Configuracao config = new Configuracao();

			var array = IdHorario.Split('|');

			for (int i = 0; i < array.Length; i++)
			{
				if (!string.IsNullOrWhiteSpace(array[i]))
				{
					retorno = config.InsereHorarioLinha(int.Parse(array[i]), CodLinha, Setor, CodLinhaT1);
				}
			}

			return Json(retorno, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterTamLote(int CodLinha)
		{
			Configuracao config = new Configuracao();

			int tamLote = config.ObterTamanhoLote(CodLinha);

			return Json(tamLote, JsonRequestBehavior.AllowGet);
		}

        public JsonResult ObterLoteConfig(int CodLinha)
        {
            Configuracao config = new Configuracao();

            var loteConfig = config.ObterLoteConfig(CodLinha);

            return Json(loteConfig, JsonRequestBehavior.AllowGet);
        }

		public JsonResult ObterHorariosLinha(int CodLinha)
		{
			Configuracao config = new Configuracao();

			List<CBHorarioDTO> horario = config.ObterHorarioLinha(CodLinha);

			return Json(horario.ToList(), JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterHorariosDisponiveisLinha(int CodLinha)
		{
			Configuracao config = new Configuracao();

			List<CBHorarioDTO> horario = config.ObterHorarios();
			List<CBHorarioDTO> horarioLinha = config.ObterHorarioLinha(CodLinha);

			horario = horario.Where(x => !horarioLinha.Select(p => p.idHorario).Contains(x.idHorario)).ToList();

			return Json(horario.ToList(), JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterDesItem(string CodItem)
		{			
			string desItem = Geral.ObterDescricaoItem(CodItem);			

			if(String.IsNullOrEmpty(desItem))
			{
				desItem = "Modelo não encontrado";
			}			
			
			return Json(desItem, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ValidarMascaraNE(string CodItem)
		{
			Configuracao config = new Configuracao();
			
			string mascara = config.ObterMascara(CodItem);

			return Json(mascara, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GerarMascaraPadrao(string amostras)
		{
			if(amostras == null)
				return Json(amostras, JsonRequestBehavior.AllowGet);

			string items = amostras.Substring(10);

			String[] arrAmostras = items.Split('|');						

			Configuracao config = new Configuracao();

			string mascara = config.ObterMascaraPadrao(arrAmostras);

			return Json(mascara, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GerarMascara(string amostras)
		{
			if (amostras == null)
				return Json(amostras, JsonRequestBehavior.AllowGet);

			string items = amostras.Substring(10);

			String[] arrAmostras = items.Split('|');

			Configuracao config = new Configuracao();

			string mascara = config.ObterExpressaoRegular(arrAmostras);

			return Json(mascara, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ValidarMascara(string amostras, string mascara)
		{
			if (amostras == null)
				return Json(amostras, JsonRequestBehavior.AllowGet);

			Configuracao config = new Configuracao();

			Boolean valida = config.ValidaMascara(amostras, mascara);
			string result = "";

			if(valida == true)
			{
				//ViewBag.JS = "alert('Máscara válida.');";
				result = "Máscara válida.";
			}
			else
			{
				//ViewBag.JS = "alert('Máscara NÂO válida para todas as amostras coletadas.');";
				result = "Máscara NÂO válida para todas as amostras coletadas.";
			}

			return Json(result, JsonRequestBehavior.AllowGet);
		}

		public JsonResult SalvarMascara(string CodItem, string Mascara)
		{
			if (CodItem == null || Mascara == null)
				return Json(null, JsonRequestBehavior.AllowGet);

			Configuracao config = new Configuracao();

			Mensagem gravar = config.InserirMascara(CodItem, Mascara);
			string result;

			if (gravar.CodMensagem.Equals(Constantes.RAST_OK))
			{
				//ViewBag.JS = "alert('Item salvo.');location.href ='/Coleta/Configuracao/Mascara/'";
				result = "Item salvo com sucesso.";
			}
			else
			{
				//ViewBag.JS = "alert('Problemas ao inserir a máscara.');";
				result = "Problemas ao inserir a máscara.";
			}


			return Json(result, JsonRequestBehavior.AllowGet);
		}


        /// <summary>
        /// Método que controla a página que exibe todas as Máquinas da IAC
        /// </summary>
        /// <returns>Retorna a View Linha.cshtml</returns>
        public ActionResult Maquina()
        {
            Configuracao config = new Configuracao();

            return View(config.ObterMaquinas().OrderBy(x => x.CodLinha));
        }

        /// <summary>
        /// Método que faz a chamada para adicionar nova Maquina da IAC
        /// </summary>
        /// <returns></returns>
        public ActionResult AddMaquina()
        {
            return View();
        }


        public JsonResult InserirMaquina(int CodLinha, string CodMaquina, string DesMaquina)
		{
			Mensagem retorno = new Mensagem();

			Configuracao config = new Configuracao();

            retorno = config.InsereMaquina(CodLinha, CodMaquina, DesMaquina);

			return Json(retorno, JsonRequestBehavior.AllowGet);	
		}

        public JsonResult EditarMaquina(int CodLinha, string CodMaquina, string DesMaquina)
        {
            Mensagem retorno = new Mensagem();

            Configuracao config = new Configuracao();

            retorno = config.EditarMaquina(CodLinha, CodMaquina, DesMaquina);

            return Json(retorno, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Método que controla a página de edição de maquina
        /// </summary>
        /// <param name="id">Parâmetro com o código da Linha que será editada</param>
        /// <param name="codMaquina">Parâmetro com o código da maquina que será editada</param>
        /// <returns>Retorna a View EditMaquinas.cshtml</returns>
        public ActionResult EditMaquinas(int id, string codMaquina)
        {
            Configuracao config = new Configuracao();

            CBMaquinaDTO maquina = config.ObterMaquina(id, codMaquina);

            if (maquina == null)
            {
                return HttpNotFound();
            }
            ViewBag.CodLinha = new SelectList("CodLinha", maquina.CodLinha);
            ViewBag.CodMaquina = new SelectList("CodMaquina", maquina.CodMaquina);
            return View(maquina);
        }

		public ActionResult ListaHorarios(int codLinha){

			var dados = Configuracao.ObterHorarioIntervalo(codLinha);

			return View(dados);
		}

		public JsonResult ListaHorariosJSON(int codLinha)
		{

			var dados = Configuracao.ObterHorarioIntervalo(codLinha);

			return Json(dados, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ApagarIntervalo(int codLinha, string tipoIntervalo){

			try
			{
				var gravar = Configuracao.RemoverIntervalo(codLinha, tipoIntervalo);

				return Json(gravar, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json("true|" + ex.Message, JsonRequestBehavior.AllowGet);
			}
			
		}

		public JsonResult AlterarIntervalo(int codLinha, int turno, string tipoIntervalo, string horaInicio, string horaFim, int tempoIntervalo)
		{

			try
			{
				var gravar = Configuracao.GravaIntervalo(codLinha, turno, tipoIntervalo, horaInicio, horaFim, tempoIntervalo);

				return Json(gravar, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json("true|" + ex.Message, JsonRequestBehavior.AllowGet);
			}

		}

		public JsonResult AlterarLinhaOrigem(int codLinha, string setor, int codLinhaOrigem)
		{

			try
			{
				var gravar = Configuracao.AlterarLinhaOrigem(codLinha, setor, codLinhaOrigem);

				return Json(gravar, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json("true|" + ex.Message, JsonRequestBehavior.AllowGet);
			}

		}

		public JsonResult AlterarValidarOrigem(int codLinha, int numPosto, bool flgValidarOrigem)
		{

			try
			{
				var gravar = Configuracao.AlterarValidarOrigem(codLinha, numPosto, flgValidarOrigem);

				return Json(gravar, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json("true|" + ex.Message, JsonRequestBehavior.AllowGet);
			}

		}


		[HttpPost]
		public JsonResult AtivarEnvioSAP() {

			Geral.SetarAcessosBarApontamento("True");

			return Json(true, JsonRequestBehavior.AllowGet);
		
		}

		[HttpPost]
		public JsonResult DesativarEnvioSAP()
		{

			Geral.SetarAcessosBarApontamento("False");

			return Json(true, JsonRequestBehavior.AllowGet);

		}

	}
}
