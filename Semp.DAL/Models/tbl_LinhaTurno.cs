using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_LinhaTurno
    {
        public string CodLinhaOri { get; set; }
        public string CodLinhaDest { get; set; }
        public int Turno { get; set; }
    }
}
