﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.Models;
using SEMP.Model.DTO;

namespace SEMP.DAL.DAO.PM
{
	public class daoCadLinha
	{
		/// <summary>
		/// Rotina para buscar no banco de dados os registros cadastrados
		/// </summary>
		/// <returns></returns>
		public static List<LinhaDTO> ObterLinhas()
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from c in dtbSim.tbl_Linha
								 select new LinhaDTO()
								 {
									CodLinha = c.CodLinha,
									DesDep = c.DesDep,
									DesLinha = c.DesLinha,
									FlgTipo = c.FlgTipo,
									FlgAtivo = c.FlgAtivo,
									Usuario = c.Usuario,
									rowguid = c.rowguid
								 });

				return resultado.ToList();
			}
		}

		/// <summary>
		/// Rotina para buscar no banco de dados um determinado registro
		/// </summary>
		/// <param name="codLinha"></param>
		/// <returns></returns>
		public static LinhaDTO ObterLinha(string codLinha)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from c in dtbSim.tbl_Linha
								where c.CodLinha.Equals(codLinha)
								 select new LinhaDTO()
								 {
									CodLinha = c.CodLinha,
									DesDep = c.DesDep.Trim(),
									DesLinha = c.DesLinha.Trim(),
									FlgTipo = c.FlgTipo,
									FlgAtivo = c.FlgAtivo,
									Usuario = c.Usuario,
									rowguid = c.rowguid
								 }).FirstOrDefault();

				return resultado;

			}
		}

		/// <summary>
		/// Rotina de manuntenção de dados, para inclusão e alteração
		/// </summary>
		/// <param name="linha"></param>
		public static void GravarLinha(LinhaDTO linha)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var existe =
					dtbSim.tbl_Linha.FirstOrDefault(
						x => x.CodLinha.Equals(linha.CodLinha));

				if (existe != null)
				{
					existe.DesDep = linha.DesDep;
					existe.DesLinha = linha.DesLinha;
					existe.Usuario = linha.Usuario;
					existe.FlgTipo = linha.FlgTipo;
					existe.FlgAtivo = linha.FlgAtivo;
					dtbSim.SaveChanges();
				}
				else
				{
					var nlinha = new tbl_Linha()
					{
						CodLinha = linha.CodLinha,
						DesDep = linha.DesDep,
						DesLinha = linha.DesLinha,
						Usuario = linha.Usuario,
						FlgTipo = linha.FlgTipo,
						FlgAtivo = linha.FlgAtivo,
                        rowguid = Guid.NewGuid()
					};

					dtbSim.tbl_Linha.Add(nlinha);
					dtbSim.SaveChanges();
				}
			}
		}

		/// <summary>
		/// Rotina para excluir um registro do banco de dados
		/// </summary>
		/// <param name="codLinha"></param>
		public static void RemoverLinha(string codLinha)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var existe =
					dtbSim.tbl_Linha.FirstOrDefault(
						x => x.CodLinha.Equals(codLinha));

				if (existe == null) return;

				dtbSim.tbl_Linha.Remove(existe);
				dtbSim.SaveChanges();
			}
		}
	}
}
