using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_UsuarioMap : EntityTypeConfiguration<tbl_Usuario>
    {
        public tbl_UsuarioMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.NomUsuario, t.FlgAtivo });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NomUsuario)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(30)
                .IsUnicode(false);

            this.Property(t => t.NomDepto)
                .IsFixedLength()
                .HasMaxLength(30)
                .IsUnicode(false);

            this.Property(t => t.FullName)
                .HasMaxLength(60)
                .IsUnicode(false);

            this.Property(t => t.FlgAtivo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.Email)
                .HasMaxLength(100)
                .IsUnicode(false);

            this.Property(t => t.NomDeptoAD)
                .HasMaxLength(30)
                .IsUnicode(false);

            this.Property(t => t.Cpf)
                .HasMaxLength(11)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_Usuario");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.NomUsuario).HasColumnName("NomUsuario");
            this.Property(t => t.NomDepto).HasColumnName("NomDepto");
            this.Property(t => t.FullName).HasColumnName("FullName");
            this.Property(t => t.FlgAtivo).HasColumnName("FlgAtivo");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.NomDeptoAD).HasColumnName("NomDeptoAD");
            this.Property(t => t.Cpf).HasColumnName("Cpf");
        }
    }
}
