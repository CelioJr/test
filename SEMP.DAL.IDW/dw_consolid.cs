namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolid
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_consolid()
        {
            dw_consol = new HashSet<dw_consol>();
            dw_consolciplog = new HashSet<dw_consolciplog>();
            dw_consolciplog1 = new HashSet<dw_consolciplog>();
            dw_consolpt = new HashSet<dw_consolpt>();
            dw_consolpt1 = new HashSet<dw_consolpt>();
            dw_consolpt2 = new HashSet<dw_consolpt>();
            dw_consolpt3 = new HashSet<dw_consolpt>();
            dw_passagem = new HashSet<dw_passagem>();
            dw_prorea = new HashSet<dw_prorea>();
        }

        [Key]
        public long id_consolid { get; set; }

        public short? tp_id { get; set; }

        public DateTime? dt_referencia { get; set; }

        public decimal? ano { get; set; }

        public decimal? mes { get; set; }

        public DateTime? dthr_ihora { get; set; }

        public DateTime? dthr_iturno { get; set; }

        public DateTime? dthr_fturno { get; set; }

        public DateTime? dthr_fhora { get; set; }

        public DateTime? dthr_cadastro { get; set; }

        public long id_pepro { get; set; }

        public long id_cal { get; set; }

        public long id_turno { get; set; }

        public long id_pt { get; set; }

        public long id_folha { get; set; }

        public long? id_rt { get; set; }

        public long? id_cp { get; set; }

        [StringLength(100)]
        public string ds_especializaApon { get; set; }

        public short? st_ativo { get; set; }

        public short? is_alertasEnviados { get; set; }

        public long? id_gt { get; set; }

        public DateTime? dthr_iconsol { get; set; }

        public DateTime? dthr_fconsol { get; set; }

        public virtual dw_cal dw_cal { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consol> dw_consol { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolciplog> dw_consolciplog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolciplog> dw_consolciplog1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolpt> dw_consolpt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolpt> dw_consolpt1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolpt> dw_consolpt2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolpt> dw_consolpt3 { get; set; }

        public virtual pp_cp pp_cp { get; set; }

        public virtual dw_folha dw_folha { get; set; }

        public virtual om_gt om_gt { get; set; }

        public virtual dw_pepro dw_pepro { get; set; }

        public virtual om_pt om_pt { get; set; }

        public virtual dw_rt dw_rt { get; set; }

        public virtual dw_turno dw_turno { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_passagem> dw_passagem { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_prorea> dw_prorea { get; set; }
    }
}
