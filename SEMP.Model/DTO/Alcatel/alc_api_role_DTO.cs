using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class alc_api_role_DTO
    {
        public alc_api_role_DTO()
        {
            this.alc_api_user = new List<alc_api_user_DTO>();
            this.alc_api_user1 = new List<alc_api_user_DTO>();
        }

        public long id { get; set; }
        public string description { get; set; }
        public virtual ICollection<alc_api_user_DTO> alc_api_user { get; set; }
        public virtual ICollection<alc_api_user_DTO> alc_api_user1 { get; set; }
    }
}
