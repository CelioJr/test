namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_CAD_MODELO
    {
        [Key]
        public int COD_MODELO { get; set; }

        [StringLength(10)]
        public string SHORTCODE { get; set; }

        [StringLength(20)]
        public string MODELO { get; set; }

        [StringLength(13)]
        public string ANATEL { get; set; }

        [StringLength(13)]
        public string NOME_MODELO { get; set; }

        [StringLength(2)]
        public string PCBA { get; set; }

        [StringLength(1)]
        public string ORIGEM { get; set; }

        [StringLength(7)]
        public string BLUETOOTH_QD_ID { get; set; }

        public DateTime? DATA { get; set; }

        public int? POS_IMEI1 { get; set; }

        public int? POS_IMEI2 { get; set; }

        public int? POS_SKU { get; set; }

        [StringLength(12)]
        public string PI_MAIN { get; set; }

        [StringLength(12)]
        public string PI_SUB1 { get; set; }

        [StringLength(12)]
        public string PI_SUB2 { get; set; }

        public int? DONOTLINE { get; set; }

        public bool? ATIVO { get; set; }

        public bool? IMPRESSAO_SMT_LENTA { get; set; }

        public bool? ATIVAR_TAC { get; set; }

        public int? NO_DIGITOS_IMEI { get; set; }
    }
}
