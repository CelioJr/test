namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ORDER_HEADER_IN
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ORDER_HEADER_IN()
        {
            ORDER_ITEMS_IN = new HashSet<ORDER_ITEMS_IN>();
            ORDER_PARTNERS = new HashSet<ORDER_PARTNERS>();
            ORDER_TEXT = new HashSet<ORDER_TEXT>();
        }

        public Guid ID { get; set; }

        [StringLength(16)]
        public string PACKNO { get; set; }

        [StringLength(4)]
        public string DOC_TYPE { get; set; }

        [StringLength(4)]
        public string SALES_ORG { get; set; }

        [StringLength(2)]
        public string DISTR_CHAN { get; set; }

        [StringLength(2)]
        public string DIVISION { get; set; }

        [StringLength(4)]
        public string SALES_OFF { get; set; }

        [StringLength(3)]
        public string SALES_GRP { get; set; }

        [StringLength(35)]
        public string PURCH_NO_C { get; set; }

        [StringLength(20)]
        public string BSTNK { get; set; }

        [StringLength(3)]
        public string ORD_REASON { get; set; }

        [StringLength(4)]
        public string PMNTTRMS { get; set; }

        [StringLength(1)]
        public string PYMT_METH { get; set; }

        [StringLength(2)]
        public string DLV_BLOCK { get; set; }

        [StringLength(4)]
        public string S_PROC_IND { get; set; }

        [StringLength(16)]
        public string REF_DOC_L { get; set; }

        [StringLength(10)]
        public string PURCH_DATE { get; set; }

        [StringLength(1)]
        public string ORDCOMB_IN { get; set; }

        [StringLength(3)]
        public string INCOTERMS1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDER_ITEMS_IN> ORDER_ITEMS_IN { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDER_PARTNERS> ORDER_PARTNERS { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDER_TEXT> ORDER_TEXT { get; set; }
    }
}
