﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.Models;
using SEMP.Model.DTO;

namespace SEMP.DAL.DAO.Rastreabilidade
{
	public class daoWIP
	{

		public static List<WIPProducaoDTO> ObterDadosWIP()
		{
			
			using(DTB_SIMContext dtbSim = new DTB_SIMContext()){
				
				var wip = (from w in dtbSim.tbl_WIPProducao
							join i in dtbSim.tbl_Item on w.CodItem equals i.CodItem
							select new WIPProducaoDTO() {
								CodItem = w.CodItem,
								CodLocal = w.CodLocal,
								DesItem = i.DesItem,
								Familia = w.Familia,
								QtdEstoque = w.QtdEstoque,
								Valor = w.Valor
							}).ToList();

				return wip;
			}
		}

		public static List<WIPProducaoDTO> ObterDadosWIPItem(string local, string familia)
		{

			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{

				var wip = (from w in dtbSim.tbl_WIPProducao
						   join i in dtbSim.tbl_Item on w.CodItem equals i.CodItem
							where w.CodLocal == local && w.Familia == familia
							select new WIPProducaoDTO() {
								CodItem = w.CodItem,
								CodLocal = w.CodLocal,
								DesItem = i.DesItem,
								Familia = w.Familia,
								QtdEstoque = w.QtdEstoque,
								Valor = w.Valor
							}).ToList();

				return wip;
			}
		}

		public static void GerarDadosWIP(){

			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{

				dtbSim.Database.CommandTimeout = 180;

				dtbSim.Database.SqlQuery<BarrasPainelDTO>("EXEC SPC_Painel_WIP_1 ").FirstOrDefault();

			}
		}

		public static List<string> ObterFamiliasWIP() {

			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{

				var wip = (from w in dtbSim.tbl_WIPProducao
						   select w.Familia).Distinct().ToList();

				return wip;
			}

		}

	}
}
