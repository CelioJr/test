namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_cmcom
    {
        [Key]
        public long id_cmcom { get; set; }

        public long id_cm { get; set; }

        public long id_produtofinal { get; set; }

        public long? id_produtoEntra { get; set; }

        public long? id_produtoSai { get; set; }

        public DateTime? dthr_vigor { get; set; }

        public short? is_consumirMP { get; set; }

        public decimal? qt_sai { get; set; }

        public decimal? qt_entra { get; set; }

        [StringLength(256)]
        public string obs { get; set; }

        [StringLength(100)]
        public string posicao { get; set; }

        public long? id_produtosemiacabado { get; set; }

        public DateTime? dthr_sai { get; set; }

        public long? id_produto { get; set; }

        public virtual om_produto om_produto { get; set; }

        public virtual om_produto om_produto1 { get; set; }

        public virtual om_produto om_produto2 { get; set; }

        public virtual om_produto om_produto3 { get; set; }

        public virtual om_produto om_produto4 { get; set; }

        public virtual pp_cm pp_cm { get; set; }
    }
}
