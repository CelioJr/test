﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade
{
	public class VelocidadeLinha
	{
		public int CodLinha { get; set; }
		public int QtdProduzido { get; set; }
		public decimal Velocidade { get; set; }

	}
}
