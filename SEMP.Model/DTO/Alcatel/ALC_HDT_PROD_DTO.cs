using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_HDT_PROD_DTO
    {
        public string HDT_NAME { get; set; }
        public string BEGIN_RANGE { get; set; }
        public string CURRENT_RANGE { get; set; }
        public string END_RANGE { get; set; }
    }
}
