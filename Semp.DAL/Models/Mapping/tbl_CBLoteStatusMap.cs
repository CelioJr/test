﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBLoteStatusMap : EntityTypeConfiguration<tbl_CBLoteStatus>
    {

        public tbl_CBLoteStatusMap()
        {
        
        
            this.HasKey(t => new { t.CodStatus});

            this.Property(t => t.CodStatus)
                .IsRequired();

            this.Property(t => t.Descricao)
                .HasMaxLength(50)
                .IsUnicode(false);

            
            this.ToTable("tbl_CBLoteStatus");
            this.Property(t => t.CodStatus).HasColumnName("CodStatus");
            this.Property(t => t.Descricao).HasColumnName("Descricao");
            this.Property(t => t.flgAtivo).HasColumnName("flgAtivo");

        }
    }
}
