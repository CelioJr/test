namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_cpfaltamp
    {
        [Key]
        public long id_cpfaltamp { get; set; }

        public long? id_cp { get; set; }

        public long? id_produto { get; set; }

        public decimal? qtde { get; set; }

        public short? st_mp { get; set; }

        public virtual om_produto om_produto { get; set; }

        public virtual pp_cp pp_cp { get; set; }
    }
}
