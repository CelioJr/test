﻿using SEMP.DAL.Models;
using SEMP.Model.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.DAO.Materiais
{
	public class daoAP
	{
		#region  Capa
		public static LSAAPDTO ObterAP(string codFab, string numAP)
		{

			using (var contexto = new DTB_SIMContext())
			{
				var dados = (from x in contexto.tbl_LSAAP
							 join item in contexto.tbl_Item on x.CodModelo equals item.CodItem
							 from proc in contexto.tbl_PlanoProcesso.Where(p => x.CodProcesso == p.CodProcesso).DefaultIfEmpty()
							 from linha in contexto.tbl_Linha.Where(l => x.CodLinha == l.CodLinha).DefaultIfEmpty()
							 from local in contexto.tbl_Local.Where(lo => lo.CodLocal == x.CodLocal).DefaultIfEmpty()
							 from modIMC in contexto.tbl_Item.Where(m => x.CodRadial == m.CodItem).DefaultIfEmpty()
							 where x.CodFab == codFab && x.NumAP == numAP
							 select new LSAAPDTO()
							 {
								 CodFab = x.CodFab,
								 NumAP = x.NumAP,
								 CodModelo = x.CodModelo,
								 QtdLoteAP = x.QtdLoteAP,
								 QtdEmbalado = x.QtdEmbalado,
								 QtdProduzida = x.QtdProduzida,
								 CodStatus = x.CodStatus,
								 CodLinha = x.CodLinha,
								 DatReferencia = x.DatReferencia,
								 CodLocal = x.CodLocal,
								 CodLocalAP = x.CodLocalAP,
								 TipAP = x.TipAP,
								 DatAP = x.DatAP,
								 CodProcesso = x.CodProcesso,
								 CodAxial = x.CodAxial,
								 CodCinescopio = x.CodCinescopio,
								 CodRadial = x.CodRadial,
								 CodRevAxial = x.CodRevAxial,
								 CodRevisao = x.CodRevisao,
								 CodRevRadial = x.CodRevRadial,
								 CodSO = x.CodSO,
								 DatAceite = x.DatAceite,
								 DatEmissao = x.DatEmissao,
								 DatFechamento = x.DatFechamento,
								 DatInicioProd = x.DatInicioProd,
								 DatLiberacao = x.DatLiberacao,
								 DatProgFinal = x.DatProgFinal,
								 DatProgInicio = x.DatProgInicio,
								 FlgDivida = x.FlgDivida,
								 FlgTipoVenda = x.FlgTipoVenda,
								 FlgToshiba = x.FlgToshiba,
								 NomAceite = x.NomAceite,
								 NumSerieFIN = x.NumSerieFIN,
								 NumSerieINI = x.NumSerieINI,
								 Observacao = x.Observacao,
								 Sequencia = x.Sequencia,
								 DesModelo = item.DesItem,
								 DesProcesso = proc.DesProcesso,
								 DesLinha = linha.DesLinha,
								 DesLocal = local.DesLocal,
								 DesModIMC = modIMC.DesItem,
								 NumAPDestino = x.NumAPDestino,
								 NumOP = x.NumOP
							 })
							.FirstOrDefault();

				return dados;

			}

		}

		public static LSAAPDTO ObterAP(string numOP)
		{

			using (var contexto = new DTB_SIMContext())
			{
				var dados = (from x in contexto.tbl_LSAAP
							 join item in contexto.tbl_Item on x.CodModelo equals item.CodItem
							 from proc in contexto.tbl_PlanoProcesso.Where(p => x.CodProcesso == p.CodProcesso).DefaultIfEmpty()
							 from linha in contexto.tbl_Linha.Where(l => x.CodLinha == l.CodLinha).DefaultIfEmpty()
							 from local in contexto.tbl_Local.Where(lo => lo.CodLocal == x.CodLocal).DefaultIfEmpty()
							 from modIMC in contexto.tbl_Item.Where(m => x.CodRadial == m.CodItem).DefaultIfEmpty()
							 where x.NumOP == numOP
							 select new LSAAPDTO()
							 {
								 CodFab = x.CodFab,
								 NumAP = x.NumAP,
								 CodModelo = x.CodModelo,
								 QtdLoteAP = x.QtdLoteAP,
								 QtdEmbalado = x.QtdEmbalado,
								 QtdProduzida = x.QtdProduzida,
								 CodStatus = x.CodStatus,
								 CodLinha = x.CodLinha,
								 DatReferencia = x.DatReferencia,
								 CodLocal = x.CodLocal,
								 CodLocalAP = x.CodLocalAP,
								 TipAP = x.TipAP,
								 DatAP = x.DatAP,
								 CodProcesso = x.CodProcesso,
								 CodAxial = x.CodAxial,
								 CodCinescopio = x.CodCinescopio,
								 CodRadial = x.CodRadial,
								 CodRevAxial = x.CodRevAxial,
								 CodRevisao = x.CodRevisao,
								 CodRevRadial = x.CodRevRadial,
								 CodSO = x.CodSO,
								 DatAceite = x.DatAceite,
								 DatEmissao = x.DatEmissao,
								 DatFechamento = x.DatFechamento,
								 DatInicioProd = x.DatInicioProd,
								 DatLiberacao = x.DatLiberacao,
								 DatProgFinal = x.DatProgFinal,
								 DatProgInicio = x.DatProgInicio,
								 FlgDivida = x.FlgDivida,
								 FlgTipoVenda = x.FlgTipoVenda,
								 FlgToshiba = x.FlgToshiba,
								 NomAceite = x.NomAceite,
								 NumSerieFIN = x.NumSerieFIN,
								 NumSerieINI = x.NumSerieINI,
								 Observacao = x.Observacao,
								 Sequencia = x.Sequencia,
								 DesModelo = item.DesItem,
								 DesProcesso = proc.DesProcesso,
								 DesLinha = linha.DesLinha,
								 DesLocal = local.DesLocal,
								 DesModIMC = modIMC.DesItem,
								 NumAPDestino = x.NumAPDestino,
								 NumOP = x.NumOP
							 })
							.FirstOrDefault();

				return dados;

			}

		}

		public static List<LSAAPDTO> ObterAPs(List<string> numOP)
		{

			using (var contexto = new DTB_SIMContext())
			{
				var dados = (from x in contexto.tbl_LSAAP
							 join item in contexto.tbl_Item on x.CodModelo equals item.CodItem
							 from proc in contexto.tbl_PlanoProcesso.Where(p => x.CodProcesso == p.CodProcesso).DefaultIfEmpty()
							 from linha in contexto.tbl_Linha.Where(l => x.CodLinha == l.CodLinha).DefaultIfEmpty()
							 from local in contexto.tbl_Local.Where(lo => lo.CodLocal == x.CodLocal).DefaultIfEmpty()
							 from modIMC in contexto.tbl_Item.Where(m => x.CodRadial == m.CodItem).DefaultIfEmpty()
							 where numOP.Contains(x.NumOP)
							 select new LSAAPDTO()
							 {
								 CodFab = x.CodFab,
								 NumAP = x.NumAP,
								 CodModelo = x.CodModelo,
								 QtdLoteAP = x.QtdLoteAP,
								 QtdEmbalado = x.QtdEmbalado,
								 QtdProduzida = x.QtdProduzida,
								 CodStatus = x.CodStatus,
								 CodLinha = x.CodLinha,
								 DatReferencia = x.DatReferencia,
								 CodLocal = x.CodLocal,
								 CodLocalAP = x.CodLocalAP,
								 TipAP = x.TipAP,
								 DatAP = x.DatAP,
								 CodProcesso = x.CodProcesso,
								 CodAxial = x.CodAxial,
								 CodCinescopio = x.CodCinescopio,
								 CodRadial = x.CodRadial,
								 CodRevAxial = x.CodRevAxial,
								 CodRevisao = x.CodRevisao,
								 CodRevRadial = x.CodRevRadial,
								 CodSO = x.CodSO,
								 DatAceite = x.DatAceite,
								 DatEmissao = x.DatEmissao,
								 DatFechamento = x.DatFechamento,
								 DatInicioProd = x.DatInicioProd,
								 DatLiberacao = x.DatLiberacao,
								 DatProgFinal = x.DatProgFinal,
								 DatProgInicio = x.DatProgInicio,
								 FlgDivida = x.FlgDivida,
								 FlgTipoVenda = x.FlgTipoVenda,
								 FlgToshiba = x.FlgToshiba,
								 NomAceite = x.NomAceite,
								 NumSerieFIN = x.NumSerieFIN,
								 NumSerieINI = x.NumSerieINI,
								 Observacao = x.Observacao,
								 Sequencia = x.Sequencia,
								 DesModelo = item.DesItem,
								 DesProcesso = proc.DesProcesso,
								 DesLinha = linha.DesLinha,
								 DesLocal = local.DesLocal,
								 DesModIMC = modIMC.DesItem,
								 NumAPDestino = x.NumAPDestino,
								 NumOP = x.NumOP
							 });

				return dados.ToList();

			}

		}

		public static List<LSAAPDTO> ObterAPs(string codModelo, DateTime datReferencia)
		{

			using (var contexto = new DTB_SIMContext())
			{
				var dados = (from x in contexto.tbl_LSAAP
							 join item in contexto.tbl_Item on x.CodModelo equals item.CodItem
							 where x.DatReferencia == datReferencia && x.CodModelo == codModelo
							 orderby new { x.CodProcesso, x.CodModelo, x.NumAP }
							 select new LSAAPDTO()
							 {
								 CodFab = x.CodFab,
								 NumAP = x.NumAP,
								 CodModelo = x.CodModelo,
								 QtdLoteAP = x.QtdLoteAP,
								 QtdEmbalado = x.QtdEmbalado,
								 QtdProduzida = x.QtdProduzida,
								 CodStatus = x.CodStatus,
								 CodLinha = x.CodLinha,
								 DatReferencia = x.DatReferencia,
								 CodLocal = x.CodLocal,
								 CodLocalAP = x.CodLocalAP,
								 TipAP = x.TipAP,
								 DatAP = x.DatAP,
								 CodProcesso = x.CodProcesso,
								 CodAxial = x.CodAxial,
								 CodCinescopio = x.CodCinescopio,
								 CodRadial = x.CodRadial,
								 CodRevAxial = x.CodRevAxial,
								 CodRevisao = x.CodRevisao,
								 CodRevRadial = x.CodRevRadial,
								 CodSO = x.CodSO,
								 DatAceite = x.DatAceite,
								 DatEmissao = x.DatEmissao,
								 DatFechamento = x.DatFechamento,
								 DatInicioProd = x.DatInicioProd,
								 DatLiberacao = x.DatLiberacao,
								 DatProgFinal = x.DatProgFinal,
								 DatProgInicio = x.DatProgInicio,
								 FlgDivida = x.FlgDivida,
								 FlgTipoVenda = x.FlgTipoVenda,
								 FlgToshiba = x.FlgToshiba,
								 NomAceite = x.NomAceite,
								 NumSerieFIN = x.NumSerieFIN,
								 NumSerieINI = x.NumSerieINI,
								 Observacao = x.Observacao,
								 Sequencia = x.Sequencia,
								 DesModelo = item.DesItem,
								 NumAPDestino = x.NumAPDestino,
								 NumOP = x.NumOP
							 })
							.ToList();

				return dados;

			}

		}

		public static List<LSAAPDTO> ObterAPs(DateTime datReferencia)
		{

			using (var contexto = new DTB_SIMContext())
			{
				var dados = (from x in contexto.tbl_LSAAP
							 join item in contexto.tbl_Item on x.CodModelo equals item.CodItem
							 where x.DatReferencia == datReferencia
							 orderby new { x.CodProcesso, x.CodModelo, x.NumAP }
							 select new LSAAPDTO()
							 {
								 CodFab = x.CodFab,
								 NumAP = x.NumAP,
								 CodModelo = x.CodModelo,
								 QtdLoteAP = x.QtdLoteAP,
								 QtdEmbalado = x.QtdEmbalado,
								 QtdProduzida = x.QtdProduzida,
								 CodStatus = x.CodStatus,
								 CodLinha = x.CodLinha,
								 DatReferencia = x.DatReferencia,
								 CodLocal = x.CodLocal,
								 CodLocalAP = x.CodLocalAP,
								 TipAP = x.TipAP,
								 DatAP = x.DatAP,
								 CodProcesso = x.CodProcesso,
								 CodAxial = x.CodAxial,
								 CodCinescopio = x.CodCinescopio,
								 CodRadial = x.CodRadial,
								 CodRevAxial = x.CodRevAxial,
								 CodRevisao = x.CodRevisao,
								 CodRevRadial = x.CodRevRadial,
								 CodSO = x.CodSO,
								 DatAceite = x.DatAceite,
								 DatEmissao = x.DatEmissao,
								 DatFechamento = x.DatFechamento,
								 DatInicioProd = x.DatInicioProd,
								 DatLiberacao = x.DatLiberacao,
								 DatProgFinal = x.DatProgFinal,
								 DatProgInicio = x.DatProgInicio,
								 FlgDivida = x.FlgDivida,
								 FlgTipoVenda = x.FlgTipoVenda,
								 FlgToshiba = x.FlgToshiba,
								 NomAceite = x.NomAceite,
								 NumSerieFIN = x.NumSerieFIN,
								 NumSerieINI = x.NumSerieINI,
								 Observacao = x.Observacao,
								 Sequencia = x.Sequencia,
								 DesModelo = item.DesItem,
								 NumAPDestino = x.NumAPDestino,
								 NumOP = x.NumOP
							 })
							.ToList();

				return dados;

			}

		}

		public static List<LSAAPDTO> ObterAPs(DateTime datReferencia, string codFase)
		{

			using (var contexto = new DTB_SIMContext())
			{
				var dados = contexto.tbl_LSAAP
							.Where(x => x.DatReferencia == datReferencia && x.CodProcesso.StartsWith(codFase))
							.Select(x => new LSAAPDTO()
							{
								CodFab = x.CodFab,
								NumAP = x.NumAP,
								CodModelo = x.CodModelo,
								QtdLoteAP = x.QtdLoteAP,
								QtdEmbalado = x.QtdEmbalado,
								QtdProduzida = x.QtdProduzida,
								CodStatus = x.CodStatus,
								CodLinha = x.CodLinha,
								DatReferencia = x.DatReferencia,
								CodLocal = x.CodLocal,
								CodLocalAP = x.CodLocalAP,
								TipAP = x.TipAP,
								NumAPDestino = x.NumAPDestino,
								NumOP = x.NumOP
							})
							.ToList();

				return dados;

			}

		}

		public static List<LSAAPDTO> ObterAPsMes(String codModelo, DateTime datReferencia)
		{

			using (DTB_SIMContext dtbSIM = new DTB_SIMContext())
			{

				var aps = dtbSIM.tbl_LSAAP.Where(x => x.DatReferencia == datReferencia && x.CodModelo == codModelo && x.CodStatus == "03")
						.Select(y => new LSAAPDTO()
						{
							CodFab = y.CodFab,
							NumAP = y.NumAP,
							QtdLoteAP = y.QtdLoteAP,
							QtdProduzida = y.QtdProduzida,
							CodStatus = y.CodStatus,
							QtdEmbalado = y.QtdEmbalado,
							Observacao = y.Observacao,
							TipAP = y.TipAP,
							NumAPDestino = y.NumAPDestino,
							NumOP = y.NumOP
						}).ToList();

				return aps;

			}

		}

		public static bool AlterarModeloIMC_AP(string codFab, string numAP, string codModelo)
		{
			using (var contexto = new DTB_SIMContext())
			{

				var ap = contexto.tbl_LSAAP.FirstOrDefault(x => x.CodFab == codFab && x.NumAP == numAP);

				if (ap != null)
				{
					ap.CodRadial = codModelo;

					contexto.SaveChanges();

					return true;
				}
				else
				{
					return false;
				}

			}

		}

		public static bool AlterarLinha_AP(string codFab, string numAP, string codLinha)
		{
			using (var contexto = new DTB_SIMContext())
			{

				var ap = contexto.tbl_LSAAP.FirstOrDefault(x => x.CodFab == codFab && x.NumAP == numAP);

				if (ap != null)
				{
					ap.CodLinha = codLinha;

					contexto.SaveChanges();

					return true;
				}
				else
				{
					return false;
				}

			}

		}

		public static bool AlterarAP(LSAAPDTO ap)
		{
			using (var contexto = new DTB_SIMContext())
			{
				try
				{
					var registro = contexto.tbl_LSAAP.FirstOrDefault(x => x.NumAP == ap.NumAP && x.CodFab == ap.CodFab);

					AutoMapper.Mapper.Map(ap, registro);

					contexto.SaveChanges();

				}
				catch
				{
					return false;
				}
				return true;
			}
		}

		public static bool ApagarAP(string codFab, string numAP)
		{

			using (var contexto = new DTB_SIMContext())
			{
				try
				{
					var apItemPosicao = contexto.tbl_LSAAPItemPosicao.Where(x => x.CodFab == codFab && x.NumAP == numAP).ToList();
					contexto.tbl_LSAAPItemPosicao.RemoveRange(apItemPosicao);

					contexto.SaveChanges();

					var apItem = contexto.tbl_LSAAPItem.Where(x => x.CodFab == codFab && x.NumAP == numAP).ToList();
					contexto.tbl_LSAAPItem.RemoveRange(apItem);

					contexto.SaveChanges();

					var apModelo = contexto.tbl_LSAAPModelo.FirstOrDefault(x => x.CodFab == codFab && x.NumAP == numAP);
					contexto.tbl_LSAAPModelo.Remove(apModelo);

					contexto.SaveChanges();

					var ap = contexto.tbl_LSAAP.FirstOrDefault(x => x.CodFab == codFab && x.NumAP == numAP);
					contexto.tbl_LSAAP.Remove(ap);

					contexto.SaveChanges();

					return true;
				}
				catch
				{
					return false;
				}
			}

		}

		#endregion

		#region Modelos
		public static List<LSAAPModeloDTO> ObterAPModelos(string codFab, string numAP)
		{

			using (var contexto = new DTB_SIMContext())
			{
				var dados = (from ap in contexto.tbl_LSAAPModelo
							 join modelo in contexto.tbl_Item on ap.CodModelo equals modelo.CodItem
							 where ap.CodFab == codFab && ap.NumAP == numAP
							 select new LSAAPModeloDTO()
							 {
								 CodFab = ap.CodFab,
								 NumAP = ap.NumAP,
								 CodModelo = ap.CodModelo,
								 CodProcesso = ap.CodProcesso,
								 CodLinha = ap.CodLinha,
								 CodLocalCRE = ap.CodLocalCRE,
								 CodLocalDEB = ap.CodLocalDEB,
								 CodModeloApont = ap.CodModeloApont,
								 FlgApontar = ap.FlgApontar,
								 Nivel = ap.Nivel,
								 Ordem = ap.Ordem,
								 QtdApontado = ap.QtdApontado,
								 QtdEmbalado = ap.QtdEmbalado,
								 QtdItens = ap.QtdItens,
								 QtdNecessidade = ap.QtdNecessidade,
								 QtdRejeitado = ap.QtdRejeitado,
								 QtdSaldoAnt = ap.QtdSaldoAnt,

								 DesModelo = modelo.DesItem
							 })
							.ToList();

				return dados;

			}

		}

		public static LSAAPModeloDTO ObterAPModelo(string codFab, string numAP, string codModelo)
		{

			using (var contexto = new DTB_SIMContext())
			{
				var dados = (from ap in contexto.tbl_LSAAPModelo
							 join modelo in contexto.tbl_Item on ap.CodModelo equals modelo.CodItem
							 where ap.CodFab == codFab && ap.NumAP == numAP && ap.CodModelo == codModelo
							 select new LSAAPModeloDTO()
							 {
								 CodFab = ap.CodFab,
								 NumAP = ap.NumAP,
								 CodModelo = ap.CodModelo,
								 CodProcesso = ap.CodProcesso,
								 CodLinha = ap.CodLinha,
								 CodLocalCRE = ap.CodLocalCRE,
								 CodLocalDEB = ap.CodLocalDEB,
								 CodModeloApont = ap.CodModeloApont,
								 FlgApontar = ap.FlgApontar,
								 Nivel = ap.Nivel,
								 Ordem = ap.Ordem,
								 QtdApontado = ap.QtdApontado,
								 QtdEmbalado = ap.QtdEmbalado,
								 QtdItens = ap.QtdItens,
								 QtdNecessidade = ap.QtdNecessidade,
								 QtdRejeitado = ap.QtdRejeitado,
								 QtdSaldoAnt = ap.QtdSaldoAnt,

								 DesModelo = modelo.DesItem
							 })
							.FirstOrDefault();

				return dados;

			}

		}
		#endregion

		#region Item
		public static List<LSAAPItemDTO> ObterAPItens(string codFab, string numAP)
		{

			using (var contexto = new DTB_SIMContext())
			{
				var dados = (from ap in contexto.tbl_LSAAPItem
							 join item in contexto.tbl_Item on ap.CodItem equals item.CodItem
							 join alt in contexto.tbl_Item on ap.CodItemAlt equals alt.CodItem
							 join modelo in contexto.tbl_Item on ap.CodModelo equals modelo.CodItem
							 where ap.CodFab == codFab && ap.NumAP == numAP
							 select new LSAAPItemDTO()
							 {
								 CodFab = ap.CodFab,
								 NumAP = ap.NumAP,
								 CodModelo = ap.CodModelo,
								 CodItem = ap.CodItem,
								 CodItemAlt = ap.CodItemAlt,
								 CodProcesso = ap.CodProcesso,
								 QtdFrequencia = ap.QtdFrequencia,
								 QtdNecessaria = ap.QtdNecessaria,
								 QtdProduzida = ap.QtdProduzida,
								 CodIdentificaItem = ap.CodIdentificaItem,
								 CodAplAlt = ap.CodAplAlt,
								 FlgApontado = ap.FlgApontado,
								 FlgItemOK = ap.FlgItemOK,
								 NumNa = ap.NumNa,
								 OrdConsumo = ap.OrdConsumo,
								 QtdDevolucao = ap.QtdDevolucao,
								 QtdEmpenho = ap.QtdEmpenho,
								 QtdEmpenho2 = ap.QtdEmpenho2,
								 CodIdentificaRej = ap.CodIdentificaRej,
								 CodProcesso2 = ap.CodProcesso2,
								 QtdPaga = ap.QtdPaga,
								 QtdRecebida = ap.QtdRecebida,
								 QtdRejeito = ap.QtdRejeito,
								 QtdSaldoProd = ap.QtdSaldoProd,
								 DesModelo = modelo.DesItem,
								 DesItem = item.DesItem,
								 DesAlt = alt.DesItem
							 })
							.ToList();

				return dados;

			}

		}

		public static List<LSAAPItemDTO> ObterAPItens(string codFab, string numAP, string codModelo)
		{

			using (var contexto = new DTB_SIMContext())
			{
				var dados = (from ap in contexto.tbl_LSAAPItem
							 join item in contexto.tbl_Item on ap.CodItem equals item.CodItem
							 join alt in contexto.tbl_Item on ap.CodItemAlt equals alt.CodItem
							 join modelo in contexto.tbl_Item on ap.CodModelo equals modelo.CodItem
							 where ap.CodFab == codFab && ap.NumAP == numAP && ap.CodModelo == codModelo
							 select new LSAAPItemDTO()
							 {
								 CodFab = ap.CodFab,
								 NumAP = ap.NumAP,
								 CodModelo = ap.CodModelo,
								 CodItem = ap.CodItem,
								 CodItemAlt = ap.CodItemAlt,
								 CodProcesso = ap.CodProcesso,
								 QtdFrequencia = ap.QtdFrequencia,
								 QtdNecessaria = ap.QtdNecessaria,
								 QtdProduzida = ap.QtdProduzida,
								 CodIdentificaItem = ap.CodIdentificaItem,
								 CodAplAlt = ap.CodAplAlt,
								 FlgApontado = ap.FlgApontado,
								 FlgItemOK = ap.FlgItemOK,
								 NumNa = ap.NumNa,
								 OrdConsumo = ap.OrdConsumo,
								 QtdDevolucao = ap.QtdDevolucao,
								 QtdEmpenho = ap.QtdEmpenho,
								 QtdEmpenho2 = ap.QtdEmpenho2,
								 CodIdentificaRej = ap.CodIdentificaRej,
								 CodProcesso2 = ap.CodProcesso2,
								 QtdPaga = ap.QtdPaga,
								 QtdRecebida = ap.QtdRecebida,
								 QtdRejeito = ap.QtdRejeito,
								 QtdSaldoProd = ap.QtdSaldoProd,
								 DesModelo = modelo.DesItem,
								 DesItem = item.DesItem,
								 DesAlt = alt.DesItem
							 })
							.ToList();

				return dados;

			}

		}

		public static List<LSAAPItemDTO> ObterAPItens(string numOP)
		{

			using (var contexto = new DTB_SIMContext())
			{
				var dados = (from ap in contexto.tbl_LSAAPItem
							 join item in contexto.tbl_Item on ap.CodItem equals item.CodItem
							 join alt in contexto.tbl_Item on ap.CodItemAlt equals alt.CodItem
							 join modelo in contexto.tbl_Item on ap.CodModelo equals modelo.CodItem
							 where ap.NumOP == numOP
							 select new LSAAPItemDTO()
							 {
								 CodFab = ap.CodFab,
								 NumAP = ap.NumAP,
								 CodModelo = ap.CodModelo,
								 CodItem = ap.CodItem,
								 CodItemAlt = ap.CodItemAlt,
								 CodProcesso = ap.CodProcesso,
								 QtdFrequencia = ap.QtdFrequencia,
								 QtdNecessaria = ap.QtdNecessaria,
								 QtdProduzida = ap.QtdProduzida,
								 CodIdentificaItem = ap.CodIdentificaItem,
								 CodAplAlt = ap.CodAplAlt,
								 FlgApontado = ap.FlgApontado,
								 FlgItemOK = ap.FlgItemOK,
								 NumNa = ap.NumNa,
								 OrdConsumo = ap.OrdConsumo,
								 QtdDevolucao = ap.QtdDevolucao,
								 QtdEmpenho = ap.QtdEmpenho,
								 QtdEmpenho2 = ap.QtdEmpenho2,
								 CodIdentificaRej = ap.CodIdentificaRej,
								 CodProcesso2 = ap.CodProcesso2,
								 QtdPaga = ap.QtdPaga,
								 QtdRecebida = ap.QtdRecebida,
								 QtdRejeito = ap.QtdRejeito,
								 QtdSaldoProd = ap.QtdSaldoProd,
								 DesModelo = modelo.DesItem,
								 DesItem = item.DesItem,
								 DesAlt = alt.DesItem
							 })
							.ToList();

				return dados;

			}

		}

		public static LSAAPItemDTO ObterAPItem(string codFab, string numAP, string codModelo, string codItem, string codItemAlt)
		{

			using (var contexto = new DTB_SIMContext())
			{
				var dados = (from ap in contexto.tbl_LSAAPItem
							 join item in contexto.tbl_Item on ap.CodItem equals item.CodItem
							 join alt in contexto.tbl_Item on ap.CodItemAlt equals alt.CodItem
							 join modelo in contexto.tbl_Item on ap.CodModelo equals modelo.CodItem
							 where ap.CodFab == codFab && ap.NumAP == numAP &&
									ap.CodModelo == codModelo && ap.CodItem == codItem && ap.CodItemAlt == codItemAlt
							 select new LSAAPItemDTO()
							 {
								 CodFab = ap.CodFab,
								 NumAP = ap.NumAP,
								 CodModelo = ap.CodModelo,
								 CodItem = ap.CodItem,
								 CodItemAlt = ap.CodItemAlt,
								 CodProcesso = ap.CodProcesso,
								 QtdFrequencia = ap.QtdFrequencia,
								 QtdNecessaria = ap.QtdNecessaria,
								 QtdProduzida = ap.QtdProduzida,
								 CodIdentificaItem = ap.CodIdentificaItem,
								 CodAplAlt = ap.CodAplAlt,
								 FlgApontado = ap.FlgApontado,
								 FlgItemOK = ap.FlgItemOK,
								 NumNa = ap.NumNa,
								 OrdConsumo = ap.OrdConsumo,
								 QtdDevolucao = ap.QtdDevolucao,
								 QtdEmpenho = ap.QtdEmpenho,
								 QtdEmpenho2 = ap.QtdEmpenho2,
								 CodIdentificaRej = ap.CodIdentificaRej,
								 CodProcesso2 = ap.CodProcesso2,
								 QtdPaga = ap.QtdPaga,
								 QtdRecebida = ap.QtdRecebida,
								 QtdRejeito = ap.QtdRejeito,
								 QtdSaldoProd = ap.QtdSaldoProd,
								 DesModelo = modelo.DesItem,
								 DesItem = item.DesItem,
								 DesAlt = alt.DesItem
							 })
							.FirstOrDefault();

				return dados;

			}

		}

		public static bool AlteraIdentificacaoItem(string codFab, string numAP, string codModelo, string codItem, string codIdentifica)
		{
			using (var contexto = new DTB_SIMContext())
			{

				var ap = contexto.tbl_LSAAPItem.FirstOrDefault(x => x.CodFab == codFab && x.NumAP == numAP && x.CodModelo == codModelo && x.CodItemAlt == codItem);

				if (ap != null)
				{
					ap.CodIdentificaItem = codIdentifica;

					contexto.SaveChanges();

					return true;
				}
				else
				{
					return false;
				}

			}

		}

        public static bool AlterarItemAP(LSAAPItemDTO ap)
        {
            using (var contexto = new DTB_SIMContext())
            {
                try
                {
                    var registro = contexto.tbl_LSAAPItem.FirstOrDefault(x => x.NumAP == ap.NumAP && x.CodFab == ap.CodFab && x.CodItemAlt == ap.CodItemAlt);

					if (registro != null)
					{
						AutoMapper.Mapper.Map(ap, registro);
					}


                    contexto.SaveChanges();

                }
                catch
                {
                    return false;
                }
                return true;
            }
        }

		public static bool ApagarAPItem(string codFab, string numAP, string codItem)
		{

			using (var contexto = new DTB_SIMContext())
			{
				try
				{
					var apItemPosicao = contexto.tbl_LSAAPItemPosicao.FirstOrDefault(x => x.CodFab == codFab && x.NumAP == numAP && x.CodItem == codItem);
					contexto.tbl_LSAAPItemPosicao.Remove(apItemPosicao);

					var apItem = contexto.tbl_LSAAPItem.FirstOrDefault(x => x.CodFab == codFab && x.NumAP == numAP && x.CodItemAlt == codItem);
					contexto.tbl_LSAAPItem.Remove(apItem);

					contexto.SaveChanges();

					return true;
				}
				catch
				{
					return false;
				}
			}

		}

		#endregion

		#region Item Posição
		public static List<LSAAPItemPosicaoDTO> ObterAPItensPosicoes(string codFab, string numAP)
		{

			using (var contexto = new DTB_SIMContext())
			{
				var dados = (from ap in contexto.tbl_LSAAPItemPosicao
							 join item in contexto.tbl_Item on ap.CodItem equals item.CodItem
							 join modelo in contexto.tbl_Item on ap.CodModelo equals modelo.CodItem
							 where ap.CodFab == codFab && ap.NumAP == numAP
							 select new LSAAPItemPosicaoDTO()
							 {
								 CodFab = ap.CodFab,
								 NumAP = ap.NumAP,
								 CodModelo = ap.CodModelo,
								 CodItem = ap.CodItem,
								 NumPosicao = ap.NumPosicao,
								 NumOP = ap.NumOP,
								 QtdItem = ap.QtdItem,
								 DesModelo = modelo.DesItem,
								 DesItem = item.DesItem
							 })
							.ToList();

				return dados;

			}

		}

		public static List<LSAAPItemPosicaoDTO> ObterAPItensPosicoes(string codFab, string numAP, string codModelo)
		{

			using (var contexto = new DTB_SIMContext())
			{
				var dados = (from ap in contexto.tbl_LSAAPItemPosicao
							 join item in contexto.tbl_Item on ap.CodItem equals item.CodItem
							 join modelo in contexto.tbl_Item on ap.CodModelo equals modelo.CodItem
							 where ap.CodFab == codFab && ap.NumAP == numAP && ap.CodModelo == codModelo
							 select new LSAAPItemPosicaoDTO()
							 {
								 CodFab = ap.CodFab,
								 NumAP = ap.NumAP,
								 CodModelo = ap.CodModelo,
								 CodItem = ap.CodItem,
								 NumPosicao = ap.NumPosicao,
								 NumOP = ap.NumOP,
								 QtdItem = ap.QtdItem,
								 DesModelo = modelo.DesItem,
								 DesItem = item.DesItem
							 })
							.ToList();

				return dados;

			}

		}

		public static List<LSAAPItemPosicaoDTO> ObterAPItensPosicoes(string numOP)
		{

			using (var contexto = new DTB_SIMContext())
			{
				var dados = (from ap in contexto.tbl_LSAAPItemPosicao
							 join item in contexto.tbl_Item on ap.CodItem equals item.CodItem
							 join modelo in contexto.tbl_Item on ap.CodModelo equals modelo.CodItem
							 where ap.NumOP == numOP
							 select new LSAAPItemPosicaoDTO()
							 {
								 CodFab = ap.CodFab,
								 NumAP = ap.NumAP,
								 CodModelo = ap.CodModelo,
								 CodItem = ap.CodItem,
								 NumPosicao = ap.NumPosicao,
								 NumOP = ap.NumOP,
								 QtdItem = ap.QtdItem,
								 DesModelo = modelo.DesItem,
								 DesItem = item.DesItem
							 })
							.ToList();

				return dados;

			}

		}

		public static LSAAPItemPosicaoDTO ObterAPItemPosicao(string codFab, string numAP, string codModelo, string codItem, string numPosicao)
		{

			using (var contexto = new DTB_SIMContext())
			{
				var dados = (from ap in contexto.tbl_LSAAPItemPosicao
							 join item in contexto.tbl_Item on ap.CodItem equals item.CodItem
							 join modelo in contexto.tbl_Item on ap.CodModelo equals modelo.CodItem
							 where ap.CodFab == codFab && ap.NumAP == numAP &&
									ap.CodModelo == codModelo && ap.CodItem == codItem && ap.NumPosicao == numPosicao
							 select new LSAAPItemPosicaoDTO()
							 {
								 CodFab = ap.CodFab,
								 NumAP = ap.NumAP,
								 CodModelo = ap.CodModelo,
								 CodItem = ap.CodItem,
								 NumPosicao = ap.NumPosicao,
								 NumOP = ap.NumOP,
								 QtdItem = ap.QtdItem,
								 DesModelo = modelo.DesItem,
								 DesItem = item.DesItem
							 })
							.FirstOrDefault();

				return dados;

			}

		}

		#endregion

	}
}
