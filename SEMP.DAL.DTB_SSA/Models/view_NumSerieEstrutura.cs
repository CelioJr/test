namespace SEMP.DAL.DTB_SSA.Models
{
   
    public partial class view_NumSerieEstrutura
    {
        public string DesSerie { get; set; }
        public int CodOF { get; set; }
        public string NE { get; set; }
        public decimal QtdFrequencia { get; set; }
        public string DesItemEstoque { get; set; }
        public int? Dispositivo_ID { get; set; }
        public string DesDispositivo { get; set; }
        public string hcl_tg { get; set; }
        public string hcl_f { get; set; }
        public string hcl_p { get; set; }
        public string flgVirtual { get; set; }
        public string flgGEDB { get; set; }
        public string flgColetaSerial { get; set; }
    }
}
