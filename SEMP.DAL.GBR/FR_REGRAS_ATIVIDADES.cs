namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_REGRAS_ATIVIDADES
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ATV_COD { get; set; }

        [StringLength(50)]
        public string ATV_NOME { get; set; }

        [StringLength(50)]
        public string ATV_NOME_REAL { get; set; }

        [StringLength(500)]
        public string ATV_DESCRICAO { get; set; }

        [StringLength(500)]
        public string ATV_PARAMS { get; set; }

        [StringLength(50)]
        public string ATV_RETORNO { get; set; }

        [StringLength(10)]
        public string ATV_COMPATIBILIDADE { get; set; }
    }
}
