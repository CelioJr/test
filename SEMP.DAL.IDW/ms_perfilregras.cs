namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ms_perfilregras
    {
        [Key]
        public long id_perfilregras { get; set; }

        public long? id_perfilandon { get; set; }

        public int? porta { get; set; }

        public int? prioridade { get; set; }

        public decimal? seg_tempoauto { get; set; }

        public decimal? seg_tempobaixa { get; set; }

        public short? is_piscante { get; set; }

        public short? tp_saida { get; set; }

        public short? tp_motivo { get; set; }

        public short? sinal_motivo { get; set; }

        [StringLength(60)]
        public string vl_motivo { get; set; }

        public decimal? seg_tolerancia { get; set; }

        [StringLength(255)]
        public string url_conexaoUP { get; set; }

        public virtual ms_perfilandon ms_perfilandon { get; set; }
    }
}
