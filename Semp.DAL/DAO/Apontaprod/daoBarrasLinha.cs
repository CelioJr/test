﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.Models;
using SEMP.Model.DTO;

namespace SEMP.DAL.DAO.Rastreabilidade
{
	public class daoBarrasLinha
	{
        //static daoBarrasLinha()
        //{
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<tbl_BarrasLinha, BarrasLinhaDTO>();
        //        cfg.CreateMap<BarrasLinhaDTO, tbl_BarrasLinha>();
        //    });
        //}

        public static List<BarrasLinhaDTO> ObterLinhas()
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var linhas = dtbSim.tbl_BarrasLinha.OrderBy(x => x.Setor).ThenBy(x => x.CodLinha).ToList();

				var linhasDTO = new List<BarrasLinhaDTO>();

				AutoMapper.Mapper.Map(linhas, linhasDTO);

				return linhasDTO;
			}

		}

		public static BarrasLinhaDTO ObterLinha(string codLinha)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var linhas = dtbSim.tbl_BarrasLinha.FirstOrDefault(x => x.CodLinha == codLinha);
				
				var linhasDTO = new BarrasLinhaDTO();

				AutoMapper.Mapper.Map(linhas, linhasDTO);

				return linhasDTO;
			}

		}

		public static void AtualizaLinha(BarrasLinhaDTO linha)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var linhaAtual = dtbSim.tbl_BarrasLinha.FirstOrDefault(x => x.CodLinha == linha.CodLinha);
				if (linhaAtual != null)
				{
					AutoMapper.Mapper.Map(linha, linhaAtual);

					dtbSim.SaveChanges();
				}
			}

		}
	}
}
