namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_ft_param
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_ft_param()
        {
            dw_consol_param = new HashSet<dw_consol_param>();
            dw_consolmedparamlog = new HashSet<dw_consolmedparamlog>();
            dw_consolmedparamlog1 = new HashSet<dw_consolmedparamlog>();
            dw_consolmedparamlog2 = new HashSet<dw_consolmedparamlog>();
            dw_folhamedtemhor = new HashSet<dw_folhamedtemhor>();
            dw_ft_sub = new HashSet<dw_ft_sub>();
            dw_ft_subparam = new HashSet<dw_ft_subparam>();
            om_cfg = new HashSet<om_cfg>();
            om_cfg1 = new HashSet<om_cfg>();
            om_cfg2 = new HashSet<om_cfg>();
            om_cfg3 = new HashSet<om_cfg>();
            om_cfg4 = new HashSet<om_cfg>();
            om_cfg5 = new HashSet<om_cfg>();
            om_cfg6 = new HashSet<om_cfg>();
        }

        [Key]
        public long id_ft_param { get; set; }

        [StringLength(40)]
        public string ds_parametro { get; set; }

        public short? is_minimo { get; set; }

        public short? is_meta { get; set; }

        public short? is_maximo { get; set; }

        public short? is_combo { get; set; }

        public short? st_valor1 { get; set; }

        [StringLength(40)]
        public string ds_valor1 { get; set; }

        public short? st_valor2 { get; set; }

        [StringLength(40)]
        public string ds_valor2 { get; set; }

        public short? st_valor3 { get; set; }

        [StringLength(40)]
        public string ds_valor3 { get; set; }

        public short? st_valor4 { get; set; }

        [StringLength(40)]
        public string ds_valor4 { get; set; }

        public long? id_ftgrupo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consol_param> dw_consol_param { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolmedparamlog> dw_consolmedparamlog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolmedparamlog> dw_consolmedparamlog1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolmedparamlog> dw_consolmedparamlog2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhamedtemhor> dw_folhamedtemhor { get; set; }

        public virtual dw_ft_grupo dw_ft_grupo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_ft_sub> dw_ft_sub { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_ft_subparam> dw_ft_subparam { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg4 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg5 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg6 { get; set; }
    }
}
