namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_testesub
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_testesub()
        {
            dw_testesubetapa = new HashSet<dw_testesubetapa>();
        }

        [Key]
        public long id_testesub { get; set; }

        public long id_folhateste { get; set; }

        public long id_ft_sub { get; set; }

        public int? ordem { get; set; }

        public decimal? qt_med_seg { get; set; }

        public decimal? qt_med_seg_pos_falha { get; set; }

        public int? ms_cap_med_pos_falha { get; set; }

        public short? is_salvar { get; set; }

        public int? ordemEtapa { get; set; }

        public int? acenderLampada { get; set; }

        public virtual dw_folhateste dw_folhateste { get; set; }

        public virtual dw_ft_sub dw_ft_sub { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_testesubetapa> dw_testesubetapa { get; set; }
    }
}
