﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.Models
{
    public class tbl_CBLoteStatus
    {
        public int CodStatus { get; set; }
        public string Descricao { get; set; }

        public bool flgAtivo { get; set; }

    
    }
}
