using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_ItemPCP
    {
        public string CodItem { get; set; }
        public string CodComp { get; set; }
        public string CodProg { get; set; }
        public string CodAgrup { get; set; }
        public string CodTransporte { get; set; }
        public string CodClasse { get; set; }
        public string TipConsumo { get; set; }
        public Nullable<decimal> PerPerdas { get; set; }
        public string FlgAleFixo { get; set; }
        public Nullable<decimal> LeadMont { get; set; }
        public Nullable<decimal> PontoPed { get; set; }
        public string CodRastr { get; set; }
        public string Usuario { get; set; }
        public System.Guid rowguid { get; set; }
        public string CodProgSeg { get; set; }
        public string FlgFabEspecial { get; set; }
        public string CodProcesso { get; set; }
        public string MovEstoque { get; set; }
        public string FlgBaixaDiaria { get; set; }
    }
}
