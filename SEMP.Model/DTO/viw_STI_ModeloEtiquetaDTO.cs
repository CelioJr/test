﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.DTO
{
    [Serializable]
    public class viw_STI_ModeloEtiquetaDTO
    {
        public string CodItem { get; set; }
        public string CodEstrutura { get; set; }
        public string DesModelo { get; set; }
        public string DesResumida { get; set; }
        public string DesDetalhe { get; set; }
        public string CodEAN { get; set; }
        public string CodEANCxColetiva { get; set; }
        public Nullable<decimal> QtdEmbUni { get; set; }
    }
}
