﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBEtiquetaMap : EntityTypeConfiguration<tbl_CBEtiqueta>
    {
        public tbl_CBEtiquetaMap ()
		{
            // Primary Key
            this.HasKey(t => new { t.CodLinha, t.NumPosto, t.NomeEtiqueta });

            // Properties
            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumPosto)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NomeEtiqueta)
                .HasMaxLength(50)
                .IsUnicode(false);

            this.Property(t => t.DescricaoEtiqueta)
                .HasMaxLength(100)
                .IsUnicode(false);

            this.Property(t => t.CaminhoEtiqueta)
                .HasMaxLength(100)
                .IsUnicode(false);

            this.Property(t => t.NumIP_Impressora)
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.Porta_Impressora)
                .HasMaxLength(15)
                .IsUnicode(false);
 
            this.Property(t => t.NomeImpressora)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_CBEtiqueta");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.NumPosto).HasColumnName("NumPosto");
            this.Property(t => t.NomeEtiqueta).HasColumnName("NomeEtiqueta");
            this.Property(t => t.DescricaoEtiqueta).HasColumnName("DescricaoEtiqueta");
            this.Property(t => t.CaminhoEtiqueta).HasColumnName("CaminhoEtiqueta");
            this.Property(t => t.NumIP_Impressora).HasColumnName("NumIP_Impressora");
            this.Property(t => t.Porta_Impressora).HasColumnName("Porta_Impressora");
            this.Property(t => t.flgAtivo).HasColumnName("flgAtivo");
            this.Property(t => t.NomeImpressora).HasColumnName("NomeImpressora");


		}
    }
}
