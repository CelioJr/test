namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_REGRAS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FR_REGRAS()
        {
            FR_DOC_REGRAS_IMAGEM = new HashSet<FR_DOC_REGRAS_IMAGEM>();
            FR_TAREFA = new HashSet<FR_TAREFA>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int REG_COD { get; set; }

        [StringLength(255)]
        public string REG_NOME { get; set; }

        public string REG_DESCRICAO { get; set; }

        public string REG_PARAMS { get; set; }

        public string REG_VARIAVEIS { get; set; }

        public string REG_PARAMS_OUT { get; set; }

        public string REG_INTERFACE { get; set; }

        public string REG_SCRIPT { get; set; }

        public DateTime? REG_DATA { get; set; }

        public DateTime? REG_HORA { get; set; }

        [StringLength(1)]
        public string REG_COMPILADA { get; set; }

        public int? REG_DESTINO { get; set; }

        [StringLength(50)]
        public string REG_HASH { get; set; }

        public int? CAT_COD { get; set; }

        public int? USR_CODIGO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_REGRAS_IMAGEM> FR_DOC_REGRAS_IMAGEM { get; set; }

        public virtual FR_REGRAS_CATEGORIAS FR_REGRAS_CATEGORIAS { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_TAREFA> FR_TAREFA { get; set; }
    }
}
