namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_ZApontamento
    {
        public int ID { get; set; }

        [Required]
        [StringLength(12)]
        public string NumOrdem { get; set; }

        public int QtdProduzido { get; set; }

        [StringLength(32)]
        public string CodHash { get; set; }

        public DateTime DatCriacao { get; set; }

        public int CodStatus { get; set; }

        public DateTime? DatProcessamento { get; set; }
    }
}
