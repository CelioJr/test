using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
	public class tbl_IAMCFamiliaMap : EntityTypeConfiguration<tbl_IAMCFamilia>
	{
		public tbl_IAMCFamiliaMap()
		{
			// Primary Key
			this.HasKey(t => t.CodFam);

			// Properties
			this.Property(t => t.NomFam)
				.HasMaxLength(10)
                .IsUnicode(false);

			this.Property(t => t.CodFam)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(6)
                .IsUnicode(false);

			// Table & Column Mappings
			this.ToTable("tbl_IAMCFamilia");
			this.Property(t => t.NomFam).HasColumnName("NomFam");
			this.Property(t => t.CodFam).HasColumnName("CodFam");
			this.Property(t => t.NumOrdem).HasColumnName("NumOrdem");
		}
	}
}
