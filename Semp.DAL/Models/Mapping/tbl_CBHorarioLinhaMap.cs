using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBHorarioLinhaMap : EntityTypeConfiguration<tbl_CBHorarioLinha>
    {
        public tbl_CBHorarioLinhaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.idHorario, t.Turno, t.CodLinha, t.Fabrica, t.DatInicio });

            // Properties
            this.Property(t => t.idHorario)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Turno)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Fabrica)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.CodLinhaCliente)
                .HasMaxLength(10)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_CBHorarioLinha");
            this.Property(t => t.idHorario).HasColumnName("idHorario");
            this.Property(t => t.Turno).HasColumnName("Turno");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.Fabrica).HasColumnName("Fabrica");
            this.Property(t => t.DatInicio).HasColumnName("DatInicio");
            this.Property(t => t.DatFim).HasColumnName("DatFim");
            this.Property(t => t.CodLinhaCliente).HasColumnName("CodLinhaCliente");
        }
    }
}
