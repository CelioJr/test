﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using SEMP.Model.DTO;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.DAL.Models;
using SEMP.Model.VO;
using SEMP.Model.VO.Rastreabilidade;
using SEMP.BO;
using System.Globalization;
using System.Text.RegularExpressions;
using SEMP.Model;

namespace RAST.BO.Rastreabilidade
{

	public class Configuracao
	{

		#region Consultas
		/// <summary>
		/// Listar todas as linhas disponíveis na tbl_CBLinha
		/// </summary>
		/// <returns>Retorna uma lista de CBLinhaDTO</returns>
		public List<CBLinhaDTO> ObterLinhas()
		{
			return daoConfiguracao.ObterLinhas();
		}

		/// <summary>
		/// Obter informação sobre uma determinada linha
		/// </summary>
		/// <param name="CodLinha"></param>
		/// <returns>Retorna um objeto do tipo CBLinhaDTO com informações de uma determinada linha </returns>
		public CBLinhaDTO ObterLinha(int CodLinha)
		{
			return daoConfiguracao.ObterLinha(CodLinha);
		}

		/// <summary>
		/// Obter informação sobre uma determinada maquina
		/// </summary>
		/// <param name="CodLinha"></param>
		/// <returns>Retorna um objeto do tipo CBMaquinaDTO com informações de uma determinada maquina </returns>
		public CBMaquinaDTO ObterMaquina(int CodLinha, string CodMaquina)
		{
			return daoConfiguracao.ObterMaquina(CodLinha, CodMaquina);
		}

		/// <summary>
		/// Listar todos os postos de uma determinada linha
		/// </summary>
		/// <param name="CodLinha"></param>
		/// <returns></returns>
		public List<CBPostoDTO> ObterPostos(int CodLinha)
		{
			return daoConfiguracao.ObterPostos(CodLinha);
		}

		/// <summary>
		/// Obter informação sobre um determinado posto de uma linha
		/// </summary>
		/// <param name="CodLinha"></param>
		/// <param name="NumPosto"></param>
		/// <returns></returns>
		public CBPostoDTO ObterPosto(int CodLinha, int NumPosto)
		{
			return daoConfiguracao.ObterPosto(CodLinha, NumPosto);
		}

		/// <summary>
		/// Listar todos os tipos de postos existentes
		/// </summary>
		/// <returns></returns>
		public List<CBTipoPostoDTO> ObterTiposPosto()
		{
			return daoConfiguracao.ObterTiposPosto();
		}

		/// <summary>
		/// Listar todos os horários disponíveis
		/// </summary>
		/// <returns></returns>
		public List<CBHorarioDTO> ObterHorarios()
		{
			return daoConfiguracao.ObterHorarios();
		}

		/// <summary>
		/// Obter horário a partir de um ID
		/// </summary>
		/// <param name="idHorario"></param>
		/// <returns></returns>
		public CBHorarioDTO ObterHorario(int idHorario)
		{
			return daoConfiguracao.ObterHorario(idHorario);
		}

		/// <summary>
		/// Listar todos os tipos de amarração disponíveis
		/// </summary>
		/// <returns></returns>
		public List<CBTipoAmarraDTO> ObterTiposAmarra()
		{
			return daoConfiguracao.ObterTiposAmarra();
		}

		/// <summary>
		/// Obter o codigo do posto especifico TABLET 
		/// </summary>
		/// <param name="codLinha"></param>
		/// <param name="tipoPosto"></param>
		/// <returns></returns>

		public static int ObterTipoPostoTablet(int codLinha, int tipoPosto)
		{
			return daoConfiguracao.ObterTipoPostoTablet(codLinha, tipoPosto);

		}

		/// <summary>
		/// Obter os tipos de amarração vinculados a um determinado posto de uma linha
		/// </summary>
		/// <param name="CodLinha"></param>
		/// <param name="NumPosto"></param>
		/// <returns></returns>
		public List<CBPerfilAmarraPostoDTO> ObterTipoAmarra(int CodLinha, int NumPosto)
		{
			return daoConfiguracao.ObterTipoAmarra(CodLinha, NumPosto);
		}

		/// <summary>
		/// Obter objeto do tipo de amarração a partir do ID
		/// </summary>
		/// <param name="CodTipoAmarra"></param>
		/// <returns></returns>
		public CBTipoAmarraDTO ObterTipoAmarra(int CodTipoAmarra)
		{
			return daoConfiguracao.ObterTipoAmarra(CodTipoAmarra);
		}

		/// <summary>
		/// Obter descrição de linha a partir de código ID
		/// </summary>
		/// <param name="codLinha"></param>
		/// <returns></returns>
		public static string ObterDesLinha(int codLinha)
		{
			return daoConfiguracao.ObterDesLinha(codLinha);
		}

		/// <summary>
		/// Obter descrição de posto a partir de código INT codTipoPosto
		/// </summary>
		/// <param name="codTipoPosto"></param>
		/// <returns></returns>
		public static string ObterDesPosto(int codTipoPosto)
		{
			return daoConfiguracao.ObterDesPosto(codTipoPosto);
		}

		/// <summary>
		/// Listar todas as máscaras cadastradas
		/// </summary>
		/// <returns></returns>
		public List<CBPerfilMascaraDTO> ObterPerfilMascara()
		{
			return daoConfiguracao.ObterPerfilMascara();
		}

		/// <summary>
		/// Obter o tamanho do lote a partir do código da linha
		/// </summary>
		/// <param name="IdLinha"></param>
		/// <returns></returns>
		public int ObterTamanhoLote(int IdLinha)
		{
			return daoConfiguracao.ObterTamanhoLote(IdLinha);
		}

		/// <summary>
		/// Obter a configuração lote a partir do código da linha
		/// </summary>
		/// <param name="IdLinha"></param>
		/// <returns></returns>
		public CBLoteConfigDTO ObterLoteConfig(int IdLinha)
		{
			return daoConfiguracao.ObterLoteConfig(IdLinha);
		}

		/// <summary>
		/// Obter os horários configurados para uma determinada linha
		/// </summary>
		/// <param name="IdHorario"></param>
		/// <returns></returns>
		public List<CBHorarioDTO> ObterHorarioLinha(int IdHorario)
		{
			return daoConfiguracao.ObterHorarioLinha(IdHorario);
		}

		/// <summary>
		/// Obter a máscara de um item a partir do NE
		/// </summary>
		/// <param name="CodItem"></param>
		/// <returns></returns>
		public String ObterMascara(string CodItem)
		{
			return daoConfiguracao.ObterMascara(CodItem);
		}

		/// <summary>
		/// Rotina para buscar os horários de intervalo de uma linha
		/// </summary>
		/// <param name="codLinha"></param>
		/// <returns></returns>
		public static List<CBHorarioIntervaloDTO> ObterHorarioIntervalo(int codLinha)
		{
			return daoConfiguracao.ObterHorarioIntervalo(codLinha);
		}


		public static ItemDTO ObterDescricaoNE(String bar)
		{
			return daoConfiguracao.ObterDescricaoNE(bar);

		}

		public static List<CBEtiquetaDTO> ObterEtiquetas(string linha)
		{
			return daoConfiguracao.ObterEtiquetas(linha);
		}

		/// <summary>
		/// Listar todas as Máquinas disponíveis na tbl_CBMaquina
		/// </summary>
		/// <returns>Retorna uma Maquina de CBMaquinaDTO</returns>
		public List<CBMaquinaDTO> ObterMaquinas()
		{
			return daoConfiguracao.ObterMaquinas();
		}


		/// <summary>
		/// Obter setores cadastrados na tabela de linha
		/// </summary>
		/// <returns></returns>
		public List<string> ObterFases()
		{
			return daoConfiguracao.ObterFases();
		}
		#endregion

		#region Validação
		/// <summary>
		/// Verifica se existe uma linha com um dado código
		/// </summary>
		/// <returns></returns>
		public Boolean VerificaLinha(int CodLinha)
		{
			return daoConfiguracao.VerificaLinha(CodLinha);
		}

		/// <summary>
		/// Verifica se existe uma Maquina cadastrada na linha
		/// </summary>
		/// <returns></returns>
		public Boolean VerificaMaquina(tbl_CBMaquina Maquina)
		{
			return daoConfiguracao.VerificaMaquina(Maquina);
		}

		/// <summary>
		/// Verifica se existe uma linha com um dado código do cliente (SEMP)
		/// </summary>
		/// <returns></returns>
		public Boolean VerificaLinhaCliente(string CodLinha)
		{
			return daoConfiguracao.VerificaLinhaCliente(CodLinha);
		}

		/// <summary>
		/// Verifica se existe uma linha com um dado código do cliente (SEMP) já cadastrado para ser usado pelo Rastreabilidade
		/// </summary>
		/// <returns></returns>
		public Boolean VerificaLinhaClienteSIM(string CodLinha)
		{
			return daoConfiguracao.VerificaLinhaClienteSIM(CodLinha);
		}

		/// <summary>
		/// Verifica se já existe um número de posto para a linha
		/// </summary>
		/// <param name="CodLinha"></param>
		/// <param name="NumPosto"></param>
		/// <returns></returns>
		public Boolean VerificaPosto(int CodLinha, int NumPosto)
		{
			return daoConfiguracao.VerificaPosto(CodLinha, NumPosto);
		}

		/// <summary>
		/// Verifica se já existe um horário cadastrado com os valores passados
		/// </summary>
		/// <param name="HorarioInicio"></param>
		/// <param name="HorarioFim"></param>
		/// <returns></returns>
		public static Boolean VerificaHorario(string HorarioInicio, string HorarioFim)
		{
			return daoConfiguracao.VerificaHorario(HorarioInicio, HorarioFim);
		}

		/// <summary>
		/// Método para verificar se já está cadastrada a máscara para um item
		/// </summary>
		/// <param name="CodItem"></param>
		/// <returns></returns>
		public Boolean VerificaMascara(string CodItem)
		{
			return daoConfiguracao.VerificaMascara(CodItem);
		}

		#endregion

		#region Gravação

		/// <summary>
		/// Método para validar e inserir nova linha na base de dados
		/// </summary>
		/// <param name="CodLinha"></param>
		/// <param name="DesLinha"></param>
		/// <param name="Setor"></param>
		/// <param name="Familia"></param>
		/// <param name="CodLinhaT1"></param>
		/// <returns></returns>
		public Mensagem InsereLinha(int CodLinha, string DesLinha, string DesObs, string Setor, string Familia, string CodLinhaT1)
		{
			Mensagem retorno = new Mensagem();

			try
			{
				//Verifica se o Códido de Linha já existe na tbl_CBLinha
				if (VerificaLinha(CodLinha) == true)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.RAST_REGISTRO_EXISTENTE);
					return retorno;
				}

				//Verifica se o Códido da Linha do Cliente (SEMP) já existe na tbl_CBLinha
				if (CodLinhaT1 != null && VerificaLinhaCliente(CodLinhaT1) == true)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.RAST_REGISTRO_EXISTENTE);
					return retorno;
				}

				//Verifica se o Códido da Linha do Cliente (SEMP) já foi cadastrado no banco da SEMP e o mesmo pode ser usado no rastreabilidade
				if (CodLinhaT1 != null && VerificaLinhaClienteSIM(CodLinhaT1) == false)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.RAST_REGISTRONAOENCONTRADO);
					return retorno;
				}

				tbl_CBLinha linha = new tbl_CBLinha();

				linha.CodLinha = CodLinha;
				linha.DesLinha = DesLinha;
				linha.DesObs = DesObs;
				linha.Setor = Setor;
				linha.Familia = Familia;
				linha.CodLinhaT1 = CodLinhaT1;

				Boolean gravar = daoConfiguracao.InsereLinha(linha);

				if (gravar)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);
				}
				else
				{
					retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
					return retorno;
				}

			}
			catch (Exception ex)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				retorno.StackErro = ex.InnerException.Message;
			}


			return retorno;
		}

		/// <summary>
		/// Método para validar e insere um novo posto
		/// </summary>
		/// <param name="CodLinha"></param>
		/// <param name="NumPosto"></param>
		/// <param name="CodTipoPosto"></param>
		/// <param name="DesPosto"></param>
		/// <param name="NumSeq"></param>
		/// <param name="flgObrigatorio"></param>
		/// <param name="flgPedeAP"></param>
		/// <param name="flgImprimeEtq"></param>
		/// <param name="tempoLeitura"></param>
		/// <param name="tipoEtiqueta"></param>
		/// <returns></returns>
		public Boolean InserePosto(int CodLinha, int NumPosto, int CodTipoPosto, string DesPosto, string flgAtivo, int NumSeq, bool flgObrigatorio, string flgPedeAP, string flgImprimeEtq, int TempoLeitura, int TipoEtiqueta, decimal? TempoCiclo, string FlgGargalo)
		{
			//Mensagem retorno = new Mensagem();

			if (VerificaPosto(CodLinha, NumPosto) == true)
			{
				//retorno = daoMensagem.ObterMensagem(Constantes.RAST_REGISTRO_EXISTENTE);
				return false;
			}

			try
			{
				tbl_CBPosto posto = new tbl_CBPosto();

				posto.CodLinha = CodLinha;
				posto.NumPosto = NumPosto;
				posto.CodTipoPosto = CodTipoPosto;
				posto.DesPosto = DesPosto;
				posto.FlgAtivo = flgAtivo;
				posto.NumSeq = NumSeq;
				posto.flgObrigatorio = flgObrigatorio;
				posto.flgPedeAP = flgPedeAP;
				posto.flgImprimeEtq = flgImprimeEtq;
				posto.TempoLeitura = TempoLeitura;
				posto.TipoEtiqueta = TipoEtiqueta;
				posto.TempoCiclo = TempoCiclo;
				posto.FlgGargalo = FlgGargalo;

				Boolean gravar = daoConfiguracao.InserePosto(posto);

				if (gravar)
				{
					//retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);
					return true;
				}
				else
				{
					//retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
					return false;
				}

			}
			catch (Exception)
			{
				return false;
			}

		}

		/// <summary>
		/// Método para inserir horário de trabalho de um turno
		/// </summary>
		/// <param name="Turno"></param>
		/// <param name="HoraInicio"></param>
		/// <param name="HoraFim"></param>
		/// <param name="flgViraDia"></param>
		/// <returns></returns>
		public Mensagem InsereHorario(int Turno, string HoraInicio, string HoraFim)
		{

			Mensagem retorno = new Mensagem();

			if (VerificaHorario(HoraInicio, HoraFim) == true)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.RAST_REGISTRO_EXISTENTE);
				return retorno;
			}

			try
			{
				tbl_CBHorario horario = new tbl_CBHorario();
				horario.Turno = Turno;
				horario.HoraInicio = HoraInicio;
				horario.HoraFim = HoraFim;

				CultureInfo provider = CultureInfo.InvariantCulture;
				string format = "HH:mm";
				DateTime dtHoraInicio = DateTime.ParseExact(HoraInicio, format, provider);
				CultureInfo provider2 = CultureInfo.InvariantCulture;
				string format2 = "HH:mm";
				DateTime dtHoraFim = DateTime.ParseExact(HoraFim, format2, provider2);
				int result = DateTime.Compare(dtHoraInicio, dtHoraFim);

				/*
				 * É verificado o horário para saber se passa de meia-noite.
				 * Caso o horário fim seja menor que o horário de início, isso significa que passa da meia-noite o turno.
				*/
				if (result < 0)
					horario.flgViraDia = "N";
				else if (result > 0)
					horario.flgViraDia = "S";


				Boolean gravar = daoConfiguracao.InsereHorario(horario);

				if (gravar)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);
				}
				else
				{
					retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
					return retorno;
				}

			}
			catch (Exception ex)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				retorno.StackErro = ex.InnerException.Message;
			}

			return retorno;
		}

		/// <summary>
		/// Método para inserir e configurar o horário de funcionamento de uma linha
		/// </summary>
		/// <param name="IdHorario"></param>
		/// <param name="CodLinha"></param>
		/// <param name="Fabrica"></param>
		/// <param name="CodLinhaCliente"></param>
		/// <returns></returns>
		public Mensagem InsereHorarioLinha(int IdHorario, int CodLinha, string Fabrica, string CodLinhaCliente)
		{
			Mensagem retorno = new Mensagem();

			try
			{
				int? turno = daoConfiguracao.ObterTurno(IdHorario);

				tbl_CBHorarioLinha horario = new tbl_CBHorarioLinha();
				horario.idHorario = IdHorario;
				horario.Turno = (int)turno;
				horario.CodLinha = CodLinha;
				horario.Fabrica = Fabrica;
				horario.DatInicio = DateTime.Today;
				horario.CodLinhaCliente = CodLinhaCliente;

				Boolean gravar = false;

				/*
				 * Caso exista um horário configurado para uma linha, serão apagados todos os registros relacionados
				 * para que seja possível inserir os novos horários.
				 */
				if (daoConfiguracao.VerificaHorarioLinha(IdHorario, CodLinha, (int)turno))
				{
					CBHorarioLinhaDTO cbHorarioLinhaDTO = daoConfiguracao.ObterDatInicioHorarioLinha(CodLinha);
					horario.DatInicio = cbHorarioLinhaDTO.DatInicio;

					gravar = daoConfiguracao.RemoverHorarioLinha(horario);

					if (gravar)
						gravar = daoConfiguracao.InsereHorarioLinha(horario);
				}
				else
					gravar = daoConfiguracao.InsereHorarioLinha(horario);

				if (gravar)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);
				}
				else
				{
					retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
					return retorno;
				}

			}
			catch (Exception ex)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				retorno.StackErro = ex.InnerException.Message;
			}

			return retorno;
		}

		/// <summary>
		/// Método para validar e inserir nova Maquina na base de dados
		/// </summary>
		/// <param name="CodLinha"></param>
		/// <param name="CodMaquina"></param>
		/// <param name="DesMaquina"></param>
		/// <returns></returns>
		public Mensagem InsereMaquina(int CodLinha, string CodMaquina, string DesMaquina)
		{
			Mensagem retorno = new Mensagem();

			try
			{

				tbl_CBMaquina maquina = new tbl_CBMaquina();

				maquina.CodLinha = CodLinha;
				maquina.CodMaquina = CodMaquina;
				maquina.DesMaquina = DesMaquina;

				//Verifica se o Maquina já foi cadastrada na linha
				if (VerificaMaquina(maquina) == true)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.RAST_REGISTRO_EXISTENTE);
					return retorno;
				}

				Boolean gravar = daoConfiguracao.InsereMaquina(maquina);

				if (gravar)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);
				}
				else
				{
					retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
					return retorno;
				}
			}
			catch (Exception ex)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				retorno.StackErro = ex.InnerException.Message;
			}
			return retorno;
		}

		/// <summary>
		/// Método para validar e editar Maquina na base de dados
		/// </summary>
		/// <param name="CodLinha"></param>
		/// <param name="CodMaquina"></param>
		/// <param name="DesMaquina"></param>
		/// <returns></returns>
		public Mensagem EditarMaquina(int CodLinha, string CodMaquina, string DesMaquina)
		{
			Mensagem retorno = new Mensagem();

			try
			{

				tbl_CBMaquina maquina = new tbl_CBMaquina();

				maquina.CodLinha = CodLinha;
				maquina.CodMaquina = CodMaquina;
				maquina.DesMaquina = DesMaquina;

				//Verifica se o Maquina já foi cadastrada na linha
				if (VerificaMaquina(maquina) == false)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.RAST_REGISTRO_EXISTENTE);
					return retorno;
				}

				Boolean gravar = daoConfiguracao.EditarMaquina(maquina);

				if (gravar)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);
				}
				else
				{
					retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
					return retorno;
				}
			}
			catch (Exception ex)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				retorno.StackErro = ex.InnerException.Message;
			}
			return retorno;
		}

		/// <summary>
		/// Método para editar informações de uma linha na tabela tbl_CBLinha
		/// </summary>
		/// <param name="CodLinha"></param>
		/// <param name="DesLinha"></param>
		/// <param name="DesObs"></param>
		/// <param name="Setor"></param>
		/// <param name="Familia"></param>
		/// <param name="CodLinhaT1"></param>
		/// <returns></returns>
		public Boolean EditarLinha(int CodLinha, string DesLinha, string DesObs, string Setor, string Familia, string CodLinhaT1, string CodLinhaT2, string CodLinhaT3, string FlgAtivo,
									int codLinhaOrigem, string setorOrigem)
		{

			try
			{
				tbl_CBLinha linha = new tbl_CBLinha()
				{
					CodLinha = CodLinha,
					DesLinha = DesLinha,
					DesObs = DesObs,
					Setor = Setor,
					Familia = Familia,
					CodLinhaT1 = CodLinhaT1,
					flgAtivo = FlgAtivo,
					CodLinhaT2 = CodLinhaT2,
					CodLinhaT3 = CodLinhaT3,
					CodLinhaOrigem = codLinhaOrigem,
					SetorOrigem = setorOrigem
				};

				Boolean gravar = daoConfiguracao.EditarLinha(linha);

				if (gravar)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch (Exception)
			{
				return false;
			}


		}

		/// <summary>
		/// Método para editar as informações de um posto na tabela tbl_CBPosto
		/// </summary>
		/// <param name="CodLinha"></param>
		/// <param name="NumPosto"></param>
		/// <param name="CodTipoPosto"></param>
		/// <param name="DesPosto"></param>
		/// <param name="flgAtivo"></param>
		/// <param name="NumSeq"></param>
		/// <param name="flgObrigatorio"></param>
		/// <param name="flgPedeAP"></param>
		/// <param name="flgImprimeEtiqueta"></param>
		/// <param name="TempoLeitura"></param>
		/// <param name="TipoEtiqueta"></param>
		/// <returns></returns>
		public Boolean EditarPosto(int CodLinha, int NumPosto, int CodTipoPosto, string DesPosto, string flgAtivo, int NumSeq, bool flgObrigatorio, string flgPedeAP, string flgImprimeEtiqueta, int TempoLeitura, int TipoEtiqueta, decimal? TempoCiclo, string FlgGargalo, bool flgValidaOrigem, bool MontarLote, string FlgTipoTerminal)
		{

			try
			{
				tbl_CBPosto posto = new tbl_CBPosto();
				posto.CodLinha = CodLinha;
				posto.NumPosto = NumPosto;
				posto.CodTipoPosto = CodTipoPosto;
				posto.DesPosto = DesPosto;
				posto.FlgAtivo = flgAtivo;
				posto.NumSeq = NumSeq;
				posto.flgObrigatorio = flgObrigatorio;
				posto.flgPedeAP = flgPedeAP;
				posto.flgImprimeEtq = flgImprimeEtiqueta;
				posto.TempoLeitura = TempoLeitura == 0 ? (int?)null : TempoLeitura;
				posto.TipoEtiqueta = TipoEtiqueta == 0 ? (int?)null : TipoEtiqueta;
				posto.TempoCiclo = TempoCiclo;
				posto.FlgGargalo = FlgGargalo;
				posto.flgValidaOrigem = flgValidaOrigem;
				posto.MontarLote = MontarLote;
				posto.FlgTipoTerminal = FlgTipoTerminal;

				Boolean gravar = daoConfiguracao.EditarPosto(posto);

				return gravar;

			}
			catch (Exception)
			{
				return false;
			}


		}

		public static Boolean EditarPosto(CBPostoDTO posto)
		{

			try
			{
				Boolean gravar = daoConfiguracao.EditarPosto(posto);

				return gravar;

			}
			catch (Exception)
			{
				return false;
			}


		}
		/// <summary>
		/// Método para inserir horário de trabalho de um turno
		/// </summary>
		/// <param name="Turno"></param>
		/// <param name="HoraInicio"></param>
		/// <param name="HoraFim"></param>
		/// <param name="flgViraDia"></param>
		/// <returns></returns>
		public Mensagem EditarHorario(int idHorario, int Turno, string HoraInicio, string HoraFim)
		{

			Mensagem retorno = new Mensagem();

			if (VerificaHorario(HoraInicio, HoraFim) == true)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.RAST_REGISTRO_EXISTENTE);
				return retorno;
			}

			try
			{
				tbl_CBHorario horario = new tbl_CBHorario();
				horario.idHorario = idHorario;
				horario.Turno = Turno;
				horario.HoraInicio = HoraInicio;
				horario.HoraFim = HoraFim;

				CultureInfo provider = CultureInfo.InvariantCulture;
				string format = "HH:mm";
				DateTime dtHoraInicio = DateTime.ParseExact(HoraInicio, format, provider);
				CultureInfo provider2 = CultureInfo.InvariantCulture;
				string format2 = "HH:mm";
				DateTime dtHoraFim = DateTime.ParseExact(HoraFim, format2, provider2);
				int result = DateTime.Compare(dtHoraInicio, dtHoraFim);

				/*
				 * É verificado o horário para saber se passa de meia-noite.
				 * Caso o horário fim seja menor que o horário de início, isso significa que passa da meia-noite o turno.
				*/
				if (result < 0)
					horario.flgViraDia = "N";
				else if (result > 0)
					horario.flgViraDia = "S";


				Boolean gravar = daoConfiguracao.EditarHorario(horario);

				if (gravar)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);
				}
				else
				{
					retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
					return retorno;
				}

			}
			catch (Exception ex)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				retorno.StackErro = ex.InnerException.Message;
			}

			return retorno;
		}

		/// <summary>
		/// Método para remover o perfil de amarração de um posto. Ou seja, todos os tipos de amarrações associados ao posto
		/// de uma linha serão removidos.
		/// </summary>
		/// <param name="CodLinha"></param>
		/// <param name="NumPosto"></param>
		/// <returns></returns>
		public Mensagem RemoverPerfilAmarraPosto(int CodLinha, int NumPosto)
		{
			Mensagem retorno = new Mensagem();

			try
			{

				tbl_CBPerfilAmarraPosto perfil = new tbl_CBPerfilAmarraPosto();
				perfil.CodLinha = CodLinha;
				perfil.NumPosto = NumPosto;

				Boolean gravar = daoConfiguracao.RemoverPerfilAmarraPosto(perfil);

				if (gravar)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);
				}
				else
				{
					retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
					return retorno;
				}

			}
			catch (Exception ex)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				retorno.StackErro = ex.InnerException.Message;
			}
			return retorno;
		}

		/// <summary>
		/// Método para inserir os tipos de amarrações que poderão ser realizados em um posto
		/// </summary>
		/// <param name="CodLinha"></param>
		/// <param name="NumPosto"></param>
		/// <param name="CodTipoPosto"></param>
		/// <returns></returns>
		public Mensagem InserirPerfilAmarraPosto(int CodLinha, int NumPosto, int CodTipoPosto, int sequencia)
		{
			Mensagem retorno = new Mensagem();

			try
			{

				tbl_CBPerfilAmarraPosto perfil = new tbl_CBPerfilAmarraPosto();
				perfil.CodLinha = CodLinha;
				perfil.NumPosto = NumPosto;
				perfil.CodTipoAmarra = CodTipoPosto;
				perfil.Sequencia = sequencia;

				Boolean gravar = daoConfiguracao.InserePerfilAmarraPosto(perfil);

				if (gravar)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);
				}
				else
				{
					retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
					return retorno;
				}

			}
			catch (Exception ex)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				retorno.StackErro = ex.InnerException.Message;
			}
			return retorno;
		}

		/// <summary>
		/// Método para inserir ou editar o tamanho do lote de uma linha
		/// </summary>
		/// <param name="CodLinha"></param>
		/// <param name="TamLote"></param>
		/// <param name="CodDRT"></param>
		/// <returns></returns>
		public Mensagem InserirLoteConfig(int CodLinha, int TamLote, int TamMagazine, string CodDRT)
		{
			Mensagem retorno = new Mensagem();

			try
			{

				tbl_CBLoteConfig config = new tbl_CBLoteConfig();
				config.idLinha = CodLinha;
				config.Qtde = TamLote;
				config.CodDRT = CodDRT;
				config.QtdeCC = TamMagazine;

				Boolean gravar = false;

				/*
				 * Verifica se existe um registro de tamanho de lote para uma determinada linha.
				 * Caso exista, é chamado o método apenas para editar o tamanho do lote existente.
				 */
				if (daoConfiguracao.VerificaLoteConfig(CodLinha) == false)
					gravar = daoConfiguracao.InsereLoteConfig(config);
				else
					gravar = daoConfiguracao.EditarLoteConfig(config);

				if (gravar)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);
				}
				else
				{
					retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
					return retorno;
				}

			}
			catch (Exception ex)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				retorno.StackErro = ex.InnerException.Message;
			}
			return retorno;
		}

		/// <summary>
		/// Método para editar o tamanho do lote de uma linha
		/// </summary>
		/// <param name="CodLinha"></param>
		/// <param name="TamLote"></param>
		/// <returns></returns>
		public Mensagem EditarLoteConfig(int CodLinha, int TamLote, int TamMagazine)
		{
			Mensagem retorno = new Mensagem();

			try
			{

				tbl_CBLoteConfig config = new tbl_CBLoteConfig();
				config.idLinha = CodLinha;
				config.Qtde = TamLote;
				config.QtdeCC = TamMagazine;


				Boolean gravar = daoConfiguracao.InsereLoteConfig(config);

				if (gravar)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);
				}
				else
				{
					retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
					return retorno;
				}

			}
			catch (Exception ex)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				retorno.StackErro = ex.InnerException.Message;
			}
			return retorno;
		}

		/// <summary>
		/// Método para inserir ou editar uma máscara cadastrada para um NE de um item
		/// </summary>
		/// <param name="CodItem"></param>
		/// <param name="Mascara"></param>
		/// <returns></returns>
		public Mensagem InserirMascara(string CodItem, string Mascara)
		{
			Mensagem retorno = new Mensagem();

			try
			{

				tbl_CBPerfilMascara item = new tbl_CBPerfilMascara();
				item.CodItem = CodItem;
				item.Mascara = Mascara;

				Boolean gravar = false;

				/*
				 * Verifica se existe uma máscara cadastrada para um item.
				 * Caso já exista, é apenas alterada a máscara já existente pela nova.
				 */
				if (daoConfiguracao.VerificaMascara(CodItem) == false)
					gravar = daoConfiguracao.InsereMascaraEspecifica(item);
				else
					gravar = daoConfiguracao.EditarMascara(item);

				if (gravar)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);
				}
				else
				{
					retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
					return retorno;
				}

			}
			catch (Exception ex)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				retorno.StackErro = ex.InnerException.Message;
			}
			return retorno;
		}


		public Boolean AtivaDesativaPosto(int codLinha, int numPosto, bool flgStatus)
		{
			return daoConfiguracao.AtivaDesativaPosto(codLinha, numPosto, flgStatus);

		}

		public static Boolean GravaIntervalo(int codLinha, int turno, string tipoIntervalo, string horaInicio, string horaFim, int tempoIntervalo)
		{
			return daoConfiguracao.GravaIntervalo(codLinha, turno, tipoIntervalo, horaInicio, horaFim, tempoIntervalo);
		}

		public static Boolean RemoverIntervalo(int codLinha, string tipoIntervalo) {
			return daoConfiguracao.RemoverIntervalo(codLinha, tipoIntervalo);
		}

		public static Boolean AlterarLinhaOrigem(int codLinha, string setor, int codLinhaOrigem) {
			return daoConfiguracao.AlterarLinhaOrigem(codLinha, setor, codLinhaOrigem);
		}

		public static Boolean AlterarValidarOrigem(int codLinha, int numPosto, bool flgValidarOrigem){
			return daoConfiguracao.AlterarValidarOrigem(codLinha, numPosto, flgValidarOrigem);
		}

		#endregion

		#region Máscara

		/*
		 * Área com métodos para geração das máscaras.
		 */

		/// <summary>
		/// Obter máscara padrão. A máscara padrão é representada apenas pela quantidade de caracteres da string.
		/// Ao final, é inserido um código 12345 para que a máscara também permita a leitura da etiqueta padrão de painel (para aquelas painéis que apresentam falha na eitqueta).
		/// </summary>
		/// <param name="arrAmostras">Independente da quantidade de amostras, sempre será utilizada a última amostra.</param>
		/// <returns></returns>
		public String ObterMascaraPadrao(String[] arrAmostras)
		{
			int tam = 0;
			string mask;


			for (int i = 0; i < arrAmostras.Length; i++)
			{
				tam = arrAmostras[i].Length;
			}

			mask = @"^\w{" + tam + "}$|^12345$";

			return mask;
		}

		/// <summary>
		/// Método que obtém a melhor expressão regular com base nas amostras passadas como paramêtro.
		/// </summary>
		/// <param name="arrAmostras">Array com as amostras que servirão como base para a geração da expressão regular</param>
		/// <returns></returns>
		public String ObterExpressaoRegular(String[] arrAmostras)
		{
			List<Mascara.Amostra> lstAmostras = new List<Mascara.Amostra>();

			for (int i = 0; i < arrAmostras.Length; i++)
			{

				var amostra = new Mascara.Amostra() { CodItem = "", Leitura = arrAmostras[i], lstExpressaoRegular = new List<Mascara.ExpressaoRegular>() };

				AddExpressao(ref amostra, CriarExpressao(arrAmostras[i], Mascara.enumNivel.Forte), Mascara.enumNivel.Forte);

				AddExpressao(ref amostra, CriarExpressao(arrAmostras[i], Mascara.enumNivel.Generico), Mascara.enumNivel.Generico);

				lstAmostras.Add(amostra);
			}



			String padraoFinal = String.Empty;

			List<Mascara.ExpressaoRegular> lstExpressoesValidas = ValidaTodasExpressoes(ref lstAmostras);

			//obter a expressao que teve o maior numero de amostras validas
			var aux = (from e in lstExpressoesValidas
					   let max = lstExpressoesValidas.Max(ret => ret.QtdAmostrasAfetadas)
					   where e.QtdAmostrasAfetadas == max
					   select e);

			if (aux.Count() > 0)
			{
				//Pegando a ultima expressão. A menos generica possível
				var expressao = aux.Last();

				//contatena as expressões encontradas formando uma só
				padraoFinal = padraoFinal.Equals(String.Empty) ?
								"^((" + expressao.Pattern.Remove(0, 1).Remove(expressao.Pattern.Remove(0, 1).Length - 1, 1) + ")"
								: padraoFinal + "|(" + expressao.Pattern.Remove(0, 1).Remove(expressao.Pattern.Remove(0, 1).Length - 1, 1) + ")";

				//Se a quantidade de amostras afetadas dessa expressão for menor do que a quantidade de amostras deve-se encontrar as amostras que não foram incluidas para pegar
				//as expressões que atende as mesmas
				if (expressao.QtdAmostrasAfetadas < lstAmostras.Count())
				{
					//Obtendo as amostras que não são validas para a expressão

					var lstamostrasInvalidas = lstAmostras.Where(a => !Regex.IsMatch(a.Leitura, expressao.Pattern));

					if (lstamostrasInvalidas.Count() > 0)
					{
						List<Mascara.ExpressaoRegular> lstExpressoesInvalidas = new List<Mascara.ExpressaoRegular>();

						foreach (var aInvalida in lstamostrasInvalidas)
						{
							//Obter a expressão mais genérica
							Mascara.ExpressaoRegular expGe = aInvalida.lstExpressaoRegular.Where(e => e.Nivel == Mascara.enumNivel.Generico).FirstOrDefault();

							if (expGe == null) //não existe expressões genericas então busca as fortes
								expGe = aInvalida.lstExpressaoRegular.Where(e => e.Nivel == Mascara.enumNivel.Forte).FirstOrDefault();

							if (lstExpressoesInvalidas.Where(e => e.Pattern.Equals(expGe.Pattern)).Count() == 0)
							{
								lstExpressoesInvalidas.Add(expGe);

								//Adicionando a expressão
								padraoFinal = padraoFinal.Equals(String.Empty) ?
												"^(" + expGe.Pattern.Remove(0, 1).Remove(expGe.Pattern.Remove(0, 1).Length - 1, 1) + ")"
												: padraoFinal + "|(" + expGe.Pattern.Remove(0, 1).Remove(expGe.Pattern.Remove(0, 1).Length - 1, 1) + ")";
							}

							//verificar se essa expressão atende as outras amostras
							if (lstamostrasInvalidas.Where(a => Regex.IsMatch(a.Leitura, expGe.Pattern)).Count() == lstamostrasInvalidas.Count())
							{
								//parando o loop
								break;
							}
							else
							{
								//retira a amostra da lista
								lstamostrasInvalidas = lstamostrasInvalidas.Where(e => !e.Leitura.Equals(aInvalida.Leitura));
							}
						}
					}

				}

			}
			else
			{
				var lstamostrasInvalidas = lstAmostras.AsEnumerable();

				if (lstamostrasInvalidas.Count() > 0)
				{
					List<Mascara.ExpressaoRegular> lstExpressoesInvalidas = new List<Mascara.ExpressaoRegular>();

					foreach (var aInvalida in lstamostrasInvalidas)
					{
						//Obter a expressão mais genérica
						Mascara.ExpressaoRegular expGe = aInvalida.lstExpressaoRegular.Where(e => e.Nivel == Mascara.enumNivel.Forte).First();

						if (lstExpressoesInvalidas.Where(e => e.Pattern.Equals(expGe.Pattern)).Count() == 0)
						{
							lstExpressoesInvalidas.Add(expGe);

							//Adicionando a expressão
							padraoFinal = padraoFinal.Equals(String.Empty) ?
											"^((" + expGe.Pattern.Remove(0, 1).Remove(expGe.Pattern.Remove(0, 1).Length - 1, 1) + ")"
											: padraoFinal + "|(" + expGe.Pattern.Remove(0, 1).Remove(expGe.Pattern.Remove(0, 1).Length - 1, 1) + ")";
						}

						//verificar se essa expressão atende as outras amostras
						if (lstamostrasInvalidas.Where(a => Regex.IsMatch(a.Leitura, expGe.Pattern)).Count() == lstamostrasInvalidas.Count())
						{
							//parando o loop
							break;
						}
						else
						{
							//retira a amostra da lista
							lstamostrasInvalidas = lstamostrasInvalidas.Where(e => !e.Leitura.Equals(aInvalida.Leitura));
						}
					}
				}
			}

			if (!padraoFinal.Equals(String.Empty))
				padraoFinal = padraoFinal + ")$";

			return padraoFinal + "|^12345$";
		}

		/// <summary>
		/// Método que gera as expressões regulares para todas as amostras passadas como parâmetro
		/// </summary>
		/// <param name="Leitura">Amostra</param>
		/// <param name="eNivel">Nível da expressão</param>
		/// <returns>Lista de todas as expressões regulares encontradas</returns>
		private List<String> CriarExpressao(String Leitura, Mascara.enumNivel eNivel)
		{
			//String AuxpadraoInteiro = @"(?<int>[0-9]*)";

			//String AuxpadraoLetras = @"(?<letra>[a-zA-Z]*)";

			//String padraoEspecial = @"(?<especial>[-+!|*$#@&:;?<>]*)";

			//String padraoEspacoEmBranco = @"(?<branco>\s*)";

			String padrao;

			if (eNivel == Mascara.enumNivel.Forte)
			{
				padrao = @"((?<int>[0-9]*)(?<letra>[a-zA-Z]*)(?<especial>[-_\+!|\*\$#@&:\{\};\?\<\>]*)(?<branco>\s*))"; //Padrão mais forte
																														//padrao = @"((?<int>[0-9]*)(?<letra>[a-zA-Z]*)(?<especial>[-_\+!|\*\$#@&:\{\};\?\<\>]*)(?<branco>\s*)(?<LetraNumero>\w*))";
			}
			else
			{
				padrao = @"((?<int>\d*)(?<LetraNumero>\w*)(?<especial>[-_\+!|\*\$#@&:\{\};\?\<\>]*)(?<branco>\s*))"; //Padrão mais genérico
			}

			String padraoFinal = String.Empty;

			List<String> lstPadrao = new List<String>();

			Regex rgx = new Regex(padrao);

			//Gerando padrões fortes
			for (int x = 0; x <= Leitura.Length - 1; x++)
			{
				padraoFinal = String.Empty;

				foreach (Match m in rgx.Matches(Leitura, x))
				{

					if (!String.IsNullOrEmpty(m.Value))
					{

						for (int i = 0; i < m.Groups.Count; i++)
						{
							//if (eNivel == enumNivel.Forte)
							//{
							if (rgx.GetGroupNames()[i].Equals("int") && !String.IsNullOrEmpty(m.Groups[i].Value))
							{
								padraoFinal = padraoFinal + @"\d{" + m.Groups["int"].Length.ToString() + "}";
							}

							if (rgx.GetGroupNames()[i].Equals("letra") && !String.IsNullOrEmpty(m.Groups[i].Value))
							{
								padraoFinal = padraoFinal + @"[a-zA-Z]{" + m.Groups["letra"].Length.ToString() + "}";
							}
							//}
							//else
							//{
							if (rgx.GetGroupNames()[i].Equals("LetraNumero") && !String.IsNullOrEmpty(m.Groups[i].Value))
							{
								padraoFinal = padraoFinal + @"\w{" + m.Groups["LetraNumero"].Length.ToString() + "}";
							}

							if (rgx.GetGroupNames()[i].Equals("LetraNumero2") && !String.IsNullOrEmpty(m.Groups[i].Value))
							{
								padraoFinal = padraoFinal + @"[0-9a-zA-Z]{" + m.Groups["LetraNumero2"].Length.ToString() + "}";
							}
							//}

							if (rgx.GetGroupNames()[i].Equals("especial") && !String.IsNullOrEmpty(m.Groups[i].Value))
							{
								padraoFinal = padraoFinal + @"[" + m.Groups["especial"].Value + "]{" + m.Groups["especial"].Length.ToString() + "}";
							}

							if (rgx.GetGroupNames()[i].Equals("branco") && !String.IsNullOrEmpty(m.Groups[i].Value))
							{
								padraoFinal = padraoFinal + @"\s{" + m.Groups["branco"].Length.ToString() + "}";
							}
						}
					}
				}

				if (!String.IsNullOrEmpty(padraoFinal))
				{
					padraoFinal = @"^" + Leitura.Substring(0, x) + padraoFinal + "$";

					lstPadrao.Add(padraoFinal);
				}
			}
			return lstPadrao;
		}

		/// <summary>
		/// Método que adiciona na amostra passada como parâmetro todas as expressões encontradas.
		/// </summary>
		/// <param name="amostraAtual">End de memória da amostra</param>
		/// <param name="lstpadraoFinal">lista com todas as expressões criadas</param>
		/// <param name="nivel">Nível das expressões</param>
		private void AddExpressao(ref Mascara.Amostra amostraAtual, List<String> lstpadraoFinal, Mascara.enumNivel nivel)
		{
			foreach (String padraoFinal in lstpadraoFinal)
			{
				if (amostraAtual.lstExpressaoRegular.Where(e => e.Pattern.Equals(padraoFinal)).Count() == 0) //verificando se a expressão já foi adicionada na amostra
				{
					amostraAtual.lstExpressaoRegular.Add(new Mascara.ExpressaoRegular() { Pattern = padraoFinal, Valida = false, Nivel = nivel }); //Adicionando
				}
			}
		}

		/// <summary>
		/// Método que valida todas as expressões criadas em todas as amostras 
		/// </summary>
		/// <param name="lstAmostras">End de memória das amostras</param>
		/// <returns>Lista das expressões válidas</returns>
		private List<Mascara.ExpressaoRegular> ValidaTodasExpressoes(ref List<Mascara.Amostra> lstAmostras)
		{
			List<Mascara.ExpressaoRegular> lstExpressoesEncontradas = new List<Mascara.ExpressaoRegular>();

			foreach (Mascara.Amostra amostra in lstAmostras) //pegando cada amostra
			{
				foreach (var exp in amostra.lstExpressaoRegular) //pegando cada expressão regular encontrada para a amostra corrente
				{
					//Obtendo as amostras que são válidas para a expressão regular corrente
					var lstAmostrasValidas = lstAmostras.Where(a => !a.Leitura.Equals(amostra.Leitura) && Regex.IsMatch(a.Leitura, exp.Pattern));

					if (lstAmostrasValidas.Count() > 0)
					{
						exp.Valida = true;

						if (lstExpressoesEncontradas.Where(e => e.Pattern.Equals(exp.Pattern)).Count() == 0) //Verificando se a expressão já foi adicionada na lista de expressões válidas
						{
							lstExpressoesEncontradas.Add(new Mascara.ExpressaoRegular() { Pattern = exp.Pattern, Valida = true, QtdAmostrasAfetadas = lstAmostrasValidas.Count() + 1, Nivel = exp.Nivel });
						}

					}

					//Obtendo todas as amostras que são validas com a expressão encontrada              
					foreach (Mascara.Amostra amostraValida in lstAmostrasValidas)
					{
						//verificando se o padrão já foi inserido
						if (amostraValida.lstExpressaoRegular.Where(e => e.Pattern.Equals(exp.Pattern)).Count() == 0)
						{
							//Não encontrou o padrão. Deve-se adicionar
							amostraValida.lstExpressaoRegular.Add(exp);
						}
						else
						{
							amostraValida.lstExpressaoRegular.Where(e => e.Pattern.Equals(exp.Pattern)).FirstOrDefault().Valida = true;
						}

					}

				}
			}

			return lstExpressoesEncontradas;
		}

		/// <summary>
		/// Método para validar se a máscara gerada é compatível com todas as amostras coletadas
		/// </summary>
		/// <param name="amostras">Parâmetro com todas as amostras coletadas seperadas por '|' </param>
		/// <param name="mascara">Parâmetro com a máscara já gerada</param>
		/// <returns></returns>
		public Boolean ValidaMascara(string amostras, string mascara)
		{
			string items = amostras.Substring(10);

			String[] arrAmostras = items.Split('|');

			List<Mascara.Amostra> amostrasMascara = new List<Mascara.Amostra>();
			Mascara.Amostra amostraMascara = new Mascara.Amostra();

			for (int i = 0; i < arrAmostras.Length; i++)
			{
				amostraMascara = new Mascara.Amostra();
				amostraMascara.Leitura = arrAmostras[i];
				amostrasMascara.Add(amostraMascara);
			}

			if (arrAmostras.Length > 0)
			{
				amostrasMascara.Where(x => new Regex(mascara).IsMatch(x.Leitura)).ToList().ForEach(x => x.Valida = true);
				foreach (Mascara.Amostra element in amostrasMascara)
				{
					if (element.Valida == false)
						return false;
				}
			}

			return true;
		}

		#endregion

		#region Tipo Amarração
		public Mensagem GravarTipoAmarra(CBTipoAmarraDTO dado)
		{
			Mensagem retorno = new Mensagem();

			try
			{

				Boolean gravar = daoConfiguracao.GravarTipoAmarra(dado);

				if (gravar)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);
				}
				else
				{
					retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
					return retorno;
				}

			}
			catch (Exception ex)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				retorno.StackErro = ex.InnerException.Message;
			}


			return retorno;
		}

		public Mensagem RemoverTipoAmarra(int codTipoAmarra)
		{
			Mensagem retorno = new Mensagem();

			try
			{

				Boolean gravar = daoConfiguracao.RemoverTipoAmarra(codTipoAmarra);

				if (gravar)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);
				}
				else
				{
					retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
					return retorno;
				}

			}
			catch (Exception ex)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				retorno.StackErro = ex.InnerException.Message;
			}


			return retorno;
		}
		#endregion

	}
}
