using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_INVENTORY_DTO
    {
        public int ID_INVENTORY { get; set; }
        public Nullable<System.DateTime> DT_INVENTORY { get; set; }
        public string PACKAGE_CODE { get; set; }
        public byte[] ppb { get; set; }
        public Nullable<int> SERIAL_CODE { get; set; }
        public Nullable<int> SKU_CODE { get; set; }
        public Nullable<int> USER_INVENTORY { get; set; }
        public virtual ALC_SYS_USER_DTO ALC_SYS_USER { get; set; }
    }
}
