using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBPlanoAcaoAcompanhamentoMap : EntityTypeConfiguration<tbl_CBPlanoAcaoAcompanhamento>
    {
        public tbl_CBPlanoAcaoAcompanhamentoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodPlano, t.CodPlanoCausa, t.CodAcompanha });

            // Properties
            this.Property(t => t.CodPlano)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodPlanoCausa)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodAcompanha)
               .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.DesAcompanha)
                .HasMaxLength(400);

            this.Property(t => t.NomAcompanhante)
                .HasMaxLength(50);

            this.Property(t => t.FlgStatus)
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("tbl_CBPlanoAcaoAcompanhamento");
            this.Property(t => t.CodPlano).HasColumnName("CodPlano");
            this.Property(t => t.CodPlanoCausa).HasColumnName("CodPlanoCausa");
            this.Property(t => t.CodAcompanha).HasColumnName("CodAcompanha");
            this.Property(t => t.DesAcompanha).HasColumnName("DesAcompanha");
            this.Property(t => t.DatAcompanha).HasColumnName("DatAcompanha");
            this.Property(t => t.NomAcompanhante).HasColumnName("NomAcompanhante");
            this.Property(t => t.FlgStatus).HasColumnName("FlgStatus");
            this.Property(t => t.DatInicio).HasColumnName("DatInicio");
            this.Property(t => t.DatFim).HasColumnName("DatFim");
            this.Property(t => t.DatConclusao).HasColumnName("DatConclusao");
			this.Property(t => t.Observacao).HasColumnName("Observacao");

        }
    }
}
