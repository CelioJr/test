namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_alimrea
    {
        public long id_alim { get; set; }

        [Key]
        public long id_alimrea { get; set; }

        public DateTime? dthr_leitura { get; set; }

        public short? st_leitura { get; set; }

        [StringLength(60)]
        public string cd_lido { get; set; }

        public long id_mapapa { get; set; }

        public short? tp_leitura { get; set; }

        public long id_usr { get; set; }

        public decimal? qt_alimentada { get; set; }

        [StringLength(60)]
        public string cb_rap { get; set; }

        public virtual om_alim om_alim { get; set; }

        public virtual om_mapapa om_mapapa { get; set; }

        public virtual om_usr om_usr { get; set; }
    }
}
