using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public class CBMaquinaDTO
    {
        public int CodLinha { get; set; }
        public string CodMaquina { get; set; }
        public string DesMaquina { get; set; }
    }
}
