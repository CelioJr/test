namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_PPB")]
    public partial class ALC_PPB
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ALC_PPB()
        {
            ALC_DOWNTIME = new HashSet<ALC_DOWNTIME>();
            ALC_ORDER = new HashSet<ALC_ORDER>();
            ALC_PROD_PLAN = new HashSet<ALC_PROD_PLAN>();
        }

        [Key]
        [StringLength(5)]
        public string ppb { get; set; }

        public bool? BATTERY { get; set; }

        public bool? CHARGER { get; set; }

        public bool? MEMORY { get; set; }

        public bool? SD_CARD { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_DOWNTIME> ALC_DOWNTIME { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_ORDER> ALC_ORDER { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_PROD_PLAN> ALC_PROD_PLAN { get; set; }
    }
}
