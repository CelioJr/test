﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace RAST.BO.Embalagem
{
    public class BalancaToledo
    {
        private static NetworkStream networkStream;
        private static BinaryWriter binaryWriter;
        private static BinaryReader binaryReader;
        private static TcpClient tcpClient = new TcpClient();
        Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                
        public static string RecuperarPesoToledo (string IP_Servidor)
        {             
            try
            {
                bool conectada = ConectarBalanca(IP_Servidor);
                if (!conectada)
                    return "Erro";                
            }
            catch
            {
                return "Erro";
            }
            
            networkStream = tcpClient.GetStream();
            binaryWriter = new BinaryWriter(networkStream);
            binaryReader = new BinaryReader(networkStream);

            String message;
            try
            {
                do
                {
                    message = "";

                    try
                    {
                        binaryWriter.Write("peso");
                        message = binaryReader.ReadString();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                } while (message == "");
            }
            catch
            {
                return "Erro";
            }
             
            return message;
        }

        public static  bool ConectarBalanca (string IP_Servidor)
        {
            try
            {
                if (!tcpClient.Connected)
                {
                    if (tcpClient.Client.Connected)
                        tcpClient.Client.Disconnect(true);    

                    tcpClient.Connect(IP_Servidor, 8888);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }            
        }

        public void DesconectarBalanda (string IP_Servidor)
        {            
            try
            {
                tcpClient.Close();
            }
            catch (Exception)
            {                
                throw;
            }
        }

        public bool VerificarServicoDisponivel (string IP_Servidor)
        {
            TcpClient tcpTeste = new TcpClient();

            try
            {

            if (!tcpTeste.Connected)
                tcpTeste.Connect(IP_Servidor, 8888);

                tcpTeste.Close();

                return true;
            }
            catch (Exception)
            {
                return false;                
            }
        } 
    }
}
