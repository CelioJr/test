﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SEMP.Model.VO.Embalagem
{
	public class ClassesAuxiliaresEmbalagem
	{
		public class DadosHoraHora
		{
			public string CodModelo { get; set; }
			public string Linha { get; set; }
			public string DesModelo { get; set; }
			public decimal QtdProdPM { get; set; }
			public int QtdProducao { get; set; }

			public int Hora6 { get; set; }
			public int Hora7 { get; set; }
			public int Hora8 { get; set; }
			public int Hora9 { get; set; }
			public int Hora10 { get; set; }
			public int Hora11 { get; set; }
			public int Hora12 { get; set; }
			public int Hora13 { get; set; }
			public int Hora14 { get; set; }
			public int Hora15 { get; set; }
			public int Hora16 { get; set; }
			public int Hora17 { get; set; }
			public int Hora17_2 { get; set; }
			public int Hora18 { get; set; }
			public int Hora19 { get; set; }
			public int Hora20 { get; set; }
			public int Hora21 { get; set; }
			public int Hora22 { get; set; }
			public int Hora23 { get; set; }
			public int Hora0 { get; set; }
			public int Hora1 { get; set; }
			public int Hora2 { get; set; }
			public int Hora3 { get; set; }
			public int Hora4 { get; set; }
			public int Hora5 { get; set; }

		}

		public class ResultadoEmbalagemProduto
		{
			public string numeroCaixaColetiva { get; set; }
			public string numeroLote { get; set; }
			public int qtd_padrao_cc { get; set; }
			public int qtd_atual_cc { get; set; }
			public int qtd_padrao_lote { get; set; }
			public int qtd_atual_lote { get; set; }
		}


		public class ResultProcEmbAmarraProd
		{
			public string numeroCaixaColetiva { get; set; }
			public string numeroLote { get; set; }
			public int qtd_padrao_cc { get; set; }
			public int qtd_atual_cc { get; set; }
			public int qtd_padrao_lote { get; set; }
			public int qtd_atual_lote { get; set; }
			public int CodSucesso { get; set; }
			public string MsgRetorno { get; set; }
		}


		public class ResultProcGetApsModelo
		{
			public string desitem { get; set; }
			public string codFab { get; set; }
			public string numAP { get; set; }
			public string CodProcesso { get; set; }
			public string CodModelo { get; set; }
			public int QtdLoteAP { get; set; }
			public int QtdProduzida { get; set; }
			public DateTime? DatInicioProd { get; set; }
			public string NumAPDestino { get; set; }

		}


	    public class LeiturasDuplicadas
	    {
            public string numEcb { get; set; }

            public DateTime datLeitura { get; set; }

	    }


		public class RetornoApontamento
		{
			public int TmpFlag { get; set; }
			public string TmpCodModelo { get; set; }
			public string TmpCodItemOri { get; set; }
			public string TmpCodItemAlt { get; set; }
			public string TmpDesItem { get; set; }
			public Decimal? TmpQtdNecessaria { get; set; }
			public Decimal? TmpQtdFrequencia { get; set; }
			public Decimal? TmpQtdProduzida { get; set; }
			public Decimal? TmpQtdRecebida { get; set; }
			public string TmpMensagem { get; set; }
			public Decimal? TmpQtdEstoque { get; set; }
			public string TmpCodFam { get; set; }
			public string TmpCodIdentificaItem { get; set; }
			public string TmpOrdem { get; set; }
			public Decimal? TmpQtdBaixa { get; set; }
		}
	}
}
