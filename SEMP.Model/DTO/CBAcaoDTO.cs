﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.DTO
{
    public class CBAcaoDTO
    {
        [Display(Name = "Codigo")]
        public int CodAcao { get; set; }

        [Display(Name = "Descrição")]
        public string Descricao { get; set; }

        [Display(Name = "Tipo")]
        public string Tipo { get; set; }

        [Display(Name = "Ativo")]
        public Nullable<bool> FlgAtivo { get; set; }
    }
}
