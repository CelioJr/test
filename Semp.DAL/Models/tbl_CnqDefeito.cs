using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CnqDefeito
    {
        public string CodFab { get; set; }
        public string CodFam { get; set; }
        public string CodSubFam { get; set; }
        public string CodDefeito { get; set; }
        public string DesDefeito { get; set; }
        public Nullable<short> flgIMC { get; set; }
        public Nullable<short> flgMONTAGEM { get; set; }
        public Nullable<short> flgIAC { get; set; }
        public string flgAtivo { get; set; }
    }
}
