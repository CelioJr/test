namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_rotapasso_pt
    {
        [Key]
        public long id_rotapasso_pt { get; set; }

        public long id_rotapasso { get; set; }

        public long id_pt { get; set; }

        public int? ordem { get; set; }

        public short? is_mostrargt { get; set; }

        public virtual dw_rotapasso dw_rotapasso { get; set; }

        public virtual om_pt om_pt { get; set; }
    }
}
