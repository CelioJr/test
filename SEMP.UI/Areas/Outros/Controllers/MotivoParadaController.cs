﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using RAST.BO.PM;
using SEMP.Model.DTO;

namespace SEMP.Areas.Outros.Controllers
{
	public class MotivoParadaController : Controller
	{
		#region Listagem
		//
		// GET: /Outros/MotivoParada/
		public ActionResult Index()
		{
			return View();
		}

		//
		// GET: /Outros/MotivoParada/Details/5
		public ActionResult Lista(string codFab, string tipoParada, string desParada)
		{
			Response.AddHeader("content-disposition", "attachment; filename=MotivoParada.xls");
			Response.ContentType = "application/ms-excel";

			var dados = ParadaDeLinha.ObterMotivos(codFab, tipoParada);

			if (!String.IsNullOrEmpty(desParada))
			{
				dados = dados.Where(x => x.DesParada.Contains(desParada)).ToList();

			}

			return View(dados);
		}
		#endregion

		#region Create
		//
		// GET: /Outros/MotivoParada/Create
		public ActionResult Create()
		{
			return View();
		}

		//
		// POST: /Outros/MotivoParada/Create
		[HttpPost]
		public ActionResult Create(ParadaDTO parada)
		{
			try
			{
				ParadaDeLinha.GravarMotivo(parada);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json("Erro ao gravar. " + ex.Message, JsonRequestBehavior.AllowGet);
			}
		}
		#endregion

		#region Edit
		//
		// GET: /Outros/MotivoParada/Edit/5
		public ActionResult Edit(string codParada, string codFab)
		{
			var motivo = ParadaDeLinha.ObterMotivo(codParada, codFab);

			return View(motivo);
		}

		//
		// POST: /Outros/MotivoParada/Edit/5
		[HttpPost]
		public ActionResult Edit(ParadaDTO parada)
		{
			try
			{
				ParadaDeLinha.GravarMotivo(parada);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}
		#endregion

		#region Delete
		//
		// GET: /Outros/MotivoParada/Delete/5
		public ActionResult Delete(string codParada, string codFab)
		{
			var motivo = ParadaDeLinha.ObterMotivo(codParada, codFab);

			return View(motivo);
		}

		//
		// POST: /Outros/MotivoParada/Delete/5
		[HttpPost]
		public ActionResult Delete(ParadaDTO parada)
		{
			try
			{
				ParadaDeLinha.RemoverMotivo(parada.CodParada, parada.CodFab);

				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}
		}
		#endregion
	}
}
