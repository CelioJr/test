using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_FULL_SKU_DTO
    {
        public string FULL_SKU { get; set; }
        public Nullable<bool> AUXILIARY_1 { get; set; }
        public Nullable<bool> AUXILIARY_2 { get; set; }
        public Nullable<bool> BATTERY { get; set; }
        public Nullable<bool> CHARGER { get; set; }
        public Nullable<bool> MAIN_BOARD { get; set; }
        public Nullable<bool> MEMORY { get; set; }
        public Nullable<bool> SD_CARD { get; set; }
        public Nullable<bool> SUB_BOARD { get; set; }
        public Nullable<int> BOARD_QTY { get; set; }
        public Nullable<bool> HAS_SD_CARD { get; set; }
        public Nullable<bool> HAS_DTV { get; set; }
    }
}
