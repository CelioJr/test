using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_SYS_DTO
    {
        public int COD_SYS { get; set; }
        public Nullable<int> SESSAO { get; set; }
        public Nullable<System.DateTime> ALIVE { get; set; }
        public string PATH_IMPORTATION_BIN { get; set; }
        public string BACKUP_IMPORTATION_BIN { get; set; }
        public string PATH_IMEIS_VENDIDOS { get; set; }
        public string PATH_IMEIS_VENDIDOS_BACKUP { get; set; }
    }
}
