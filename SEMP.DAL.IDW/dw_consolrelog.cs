namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolrelog
    {
        [Key]
        public long id_consolrelog { get; set; }

        public DateTime? dthr_refugo { get; set; }

        public int? ms_dthrrefugo { get; set; }

        public long? pcs_auto_producaorefugada { get; set; }

        public long? pcs_manu_producaorefugada { get; set; }

        public DateTime? dthr_cancelado { get; set; }

        public short? is_cancelado { get; set; }

        public long? id_pt { get; set; }

        public long? id_trefugo { get; set; }

        public long? id_produto { get; set; }

        public long? id_tcausa { get; set; }

        public long? id_tacao { get; set; }

        public short? is_lancadotm { get; set; }
    }
}
