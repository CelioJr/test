﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.DTO;

namespace RAST.BO.ApontaProd
{
	public class BarrasLinhaBO
	{

		public static List<BarrasLinhaDTO> ObterLinhas()
		{
			return daoBarrasLinha.ObterLinhas();

		}

		public static BarrasLinhaDTO ObterLinha(string codLinha)
		{
			return daoBarrasLinha.ObterLinha(codLinha);
		}

		public static void AtualizaLinha(BarrasLinhaDTO linha)
		{
			daoBarrasLinha.AtualizaLinha(linha);
		}

	}
}
