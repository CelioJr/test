﻿using System;
using System.Collections.Generic;
using System.Linq;
using SEMP.Model.DTO;
using SEMP.DAL.Models;
using System.Data.SqlClient;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using SEMP.DAL.Common;
using System.Data;
using SEMP.DAL.Alcatel;

namespace SEMP.DAL.DAO.Rastreabilidade
{
	public class daoPainelProducao
	{

		public static List<BarrasPainelDTO> ObterPainelProducao()
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{

				var resumo = dtbSim.Database.SqlQuery<BarrasPainelDTO>(
					"EXEC spc_BarrasPainel "
					).ToList();

				return resumo;

			}
		}

		public static List<BarrasPainelDTO> ObterPainelProducao(string codLinha)
		{
			using (var dtbSim = new DTB_SIMContext())
			{

				var resumo = dtbSim.Database.SqlQuery<BarrasPainelDTO>(
					"EXEC spc_BarrasPainel @CodLinha",
					new SqlParameter("CodLinha", codLinha)
					).ToList();

				return resumo;

			}
		}

		public static List<BarrasPainel_STIDTO> ObterPainelProducaoSTI()
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var resumo = dtbSim.Database.SqlQuery<BarrasPainel_STIDTO>(
					"EXEC spc_BarrasPainel_STI "
					).ToList();

				return resumo;

			}
		}

		public static List<BarrasPainelDTO> ObterPainelProducaoALCATEL()
		{
			using (var contexto = new ALCATELContext())
			{
				try
				{
					var resumo = contexto.Database.SqlQuery<BarrasPainelDTO>(
						"EXEC spc_BarrasPainel "
						).ToList();

					return resumo;
				}
				catch (Exception)
				{
					return null;
				}
				

			}
		}

		public static List<BarrasPainelDTO> ObterPainelProducaoIMC()
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{

				var resumo = dtbSim.Database.SqlQuery<BarrasPainelDTO>(
					"EXEC spc_BarrasPainel_PCI @CodLinha, @DatReferencia, @Fase",
					new SqlParameter("CodLinha", DBNull.Value),
					new SqlParameter("DatReferencia", DBNull.Value),
					new SqlParameter("Fase", "IMC")
					).ToList();

				return resumo;

			}
		}

		public static List<BarrasPainelDTO> ObterPainelProducaoIMC(string codLinha)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{

				var resumo = dtbSim.Database.SqlQuery<BarrasPainelDTO>(
									"EXEC spc_BarrasPainel_PCI @CodLinha",
									new SqlParameter("CodLinha", codLinha)
									).ToList();
				return resumo;

			}
		}

		public static List<ResumoProducao> ObterResumoIMC(string codLinha)
		{
			List<ResumoProducao> resumos = new List<ResumoProducao>();
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				SqlConnection con = new SqlConnection(dtbSim.Database.Connection.ConnectionString);
				 try
				{
					SqlParameter[] parameters =
					 {    
						new SqlParameter("CodLinha", codLinha)
					 };
					
					con.Open();
					string cmd = "EXEC spc_BarrasPainel_PCI @CodLinha";
					SqlCommand oCom = new SqlCommand(cmd, con);
					oCom.Parameters.Clear();

					foreach (SqlParameter param in parameters)
						oCom.Parameters.Add(param);

					DataSet oDs = new DataSet();

					using (SqlDataAdapter oDa = new SqlDataAdapter())
					{
						oDa.SelectCommand = oCom;
						oDa.Fill(oDs);
					}
					if (oDs != null)
					{
						foreach (DataTable table in oDs.Tables)
						{
							if (table.Columns.Count == 9)
							{
								foreach (DataRow row in table.Rows)
								{
									ResumoProducao resumo = new ResumoProducao();
									resumo.CodModelo = row["CodModelo"].ToString();
									resumo.DesModelo = row["DesModelo"].ToString();
									resumo.QtdPlano = (string.IsNullOrEmpty(row["Plano"].ToString()) ? 0 : int.Parse(row["Plano"].ToString()));
									resumo.QtdRealizado = (string.IsNullOrEmpty(row["Embalado"].ToString()) ? 0 : int.Parse(row["Embalado"].ToString()));
									resumo.CodFab = row["CodFab"].ToString();
									resumo.NumAP = row["NumAP"].ToString();
									resumos.Add(resumo);
								}
							}
						}
					}

					return resumos;
				}
				 catch (Exception ex)
				 {
					 throw new Exception(ex.Message, ex.InnerException);
				 }
				 finally
				 {
					 con.Close();
				 }
			}
		}

		public static List<ResumoProducao> ObterResumoIAC(string codLinha)
		{
			List<ResumoProducao> resumos = new List<ResumoProducao>();
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				SqlConnection con = new SqlConnection(dtbSim.Database.Connection.ConnectionString);
				try
				{
					SqlParameter[] parameters =
					 {
						new SqlParameter("CodLinha", codLinha)
					 };

					con.Open();
					string cmd = "EXEC spc_BarrasPainel_PCI @CodLinha";
					SqlCommand oCom = new SqlCommand(cmd, con);
					oCom.Parameters.Clear();

					foreach (SqlParameter param in parameters)
						oCom.Parameters.Add(param);

					DataSet oDs = new DataSet();

					using (SqlDataAdapter oDa = new SqlDataAdapter())
					{
						oDa.SelectCommand = oCom;
						oDa.Fill(oDs);
					}
					if (oDs != null)
					{
						foreach (DataTable table in oDs.Tables)
						{
							if (table.Columns.Count == 9)
							{
								foreach (DataRow row in table.Rows)
								{
									ResumoProducao resumo = new ResumoProducao();
									resumo.CodModelo = row["CodModelo"].ToString();
									resumo.DesModelo = row["DesModelo"].ToString();
									resumo.QtdPlano = (string.IsNullOrEmpty(row["Plano"].ToString()) ? 0 : int.Parse(row["Plano"].ToString()));
									resumo.QtdRealizado = (string.IsNullOrEmpty(row["Embalado"].ToString()) ? 0 : int.Parse(row["Embalado"].ToString()));

									resumo.CodFab = row["CodFab"].ToString();
									resumo.NumAP = row["NumAP"].ToString();
									resumos.Add(resumo);
								}
							}
						}
					}

					return resumos;
				}
				catch (Exception ex)
				{
					throw new Exception(ex.Message, ex.InnerException);
				}
				finally
				{
					con.Close();
				}
			}
		}

		public static List<ResumoProducao> ObterResumoMF(string codLinha)
		{
			List<ResumoProducao> resumos = new List<ResumoProducao>();
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				SqlConnection con = new SqlConnection(dtbSim.Database.Connection.ConnectionString);
				try
				{
					SqlParameter[] parameters =
					 {    
						new SqlParameter("CodLinha", codLinha)
					 };

					con.Open();
					string cmd = "EXEC spc_BarrasPainel @CodLinha";
					SqlCommand oCom = new SqlCommand(cmd, con);
					oCom.Parameters.Clear();

					foreach (SqlParameter param in parameters)
						oCom.Parameters.Add(param);

					DataSet oDs = new DataSet();

					using (SqlDataAdapter oDa = new SqlDataAdapter())
					{
						oDa.SelectCommand = oCom;
						oDa.Fill(oDs);
					}
					if (oDs != null)
					{
						foreach (DataTable table in oDs.Tables)
						{
							if (table.Columns.Count == 6)
							{
								foreach (DataRow row in table.Rows)
								{
									ResumoProducao resumo = new ResumoProducao();
									resumo.CodModelo = row["CodModelo"].ToString();
									resumo.DesModelo = row["DesModelo"].ToString();
									resumo.QtdPlano = (string.IsNullOrEmpty(row["Plano"].ToString()) ? 0 : int.Parse(row["Plano"].ToString()));
									resumo.QtdRealizado = (string.IsNullOrEmpty(row["Embalado"].ToString()) ? 0 : int.Parse(row["Embalado"].ToString()));
									resumos.Add(resumo);
								}
							}
						}
					}

					return resumos;
				}
				catch (Exception ex)
				{
					throw new Exception(ex.Message, ex.InnerException);
				}
				finally
				{
					con.Close();
				}
			}
		}

		public static List<BarrasPainelDTO> ObterPainelProducaoIAC()
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{

				var resumo = dtbSim.Database.SqlQuery<BarrasPainelDTO>(
					"EXEC spc_BarrasPainel_PCI @CodLinha, @DatReferencia, @Fase",
					new SqlParameter("CodLinha", DBNull.Value),
					new SqlParameter("DatReferencia", DBNull.Value),
					new SqlParameter("Fase", "IAC")
					).ToList();

				return resumo;

			}
		}

		public static List<BarrasPainelDTO> ObterPainelProducaoIAC(string codLinha)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{

				var resumo = dtbSim.Database.SqlQuery<BarrasPainelDTO>(
					"EXEC spc_BarrasPainel_PCI @CodLinha, @DatReferencia, @Fase",
					new SqlParameter("CodLinha", codLinha),
					new SqlParameter("DatReferencia", DBNull.Value),
					new SqlParameter("Fase", "IAC")
					).ToList();

				return resumo;

			}
		}

		public static List<BarrasPainelDTO> ObterPainelProducaoIACOriginal(int codLinha)
		{
			using (var dtbCB = new DTB_CBContext())
			using (var dtbSim = new DTB_SIMContext())
			{
				DateTime datIni = new DateTime();
				DateTime datFim = new DateTime();

				DateTime Hora = daoUtil.GetDate();
				TimeSpan HoraAtual = new TimeSpan(Hora.Hour, Hora.Minute, Hora.Second);

				var linha = daoConfiguracao.ObterLinha(codLinha);

				if (linha == null) return new List<BarrasPainelDTO>();

				datIni = daoGeral.ObterDataInicio(linha.CodLinha, HoraAtual);

				if ((Hora.Hour >= 0) && (Hora.Hour < 7))
					datIni = datIni.AddDays(-1);

				datFim = daoGeral.ObterDataFim(linha.CodLinha, HoraAtual);

				if ((datFim.Hour >= 0) && (datFim.Hour < 7))
					datFim = datFim.AddDays(1);

				var datHoje = DateTime.Parse(DateTime.Now.ToShortDateString());

				var ultnumserieQ = (from p in dtbCB.tbl_CBPassagem
									join posto in dtbCB.tbl_CBPosto on p.CodLinha equals posto.CodLinha
									where p.NumPosto == posto.NumPosto && posto.FlgTipoTerminal == "P" && p.CodLinha == linha.CodLinha
									orderby p.DatEvento descending
									select p.NumECB.Substring(0, 6)
					);

				var ultnumserie = ultnumserieQ.FirstOrDefault();

				var produzidoQ = (from p in dtbCB.tbl_CBPassagem
								  join posto in dtbCB.tbl_CBPosto on new { p.CodLinha, p.NumPosto } equals new { posto.CodLinha, posto.NumPosto }
								  join item in dtbCB.tbl_Item on p.NumECB.Substring(0, 6) equals item.CodItem
								  where p.CodLinha == linha.CodLinha && p.DatEvento >= datIni && p.DatEvento <= datFim && (posto.FlgTipoTerminal == "P")
								  && p.NumECB.StartsWith(ultnumserie)
								  group p by new { item.CodItem, item.DesItem } into grupo
								  select new ResumoProducao()
								  {
									  CodModelo = grupo.Key.CodItem,
									  DesModelo = grupo.Key.DesItem,
									  QtdRealizado = grupo.Count(),
									  DesLinha = linha.DesLinha
								  });
				var produzido = produzidoQ.ToList();

				var programa = (from jit in dtbSim.tbl_ProgDiaPlaca
								join plano in dtbSim.tbl_PlanoItem on jit.CodPlaca equals plano.CodItem

								from plano2 in ( from pi in dtbSim.tbl_PlanoItem
													join i in dtbSim.tbl_ItemPCP on pi.CodModelo equals i.CodItem
													//where i.CodProcesso.StartsWith("02")
												 select new { pi.CodModelo, pi.CodItem, i.CodProcesso }
													).Where(x => x.CodItem == plano.CodModelo).DefaultIfEmpty()
								//from pcp in dtbSim.tbl_ItemPCP.Where(x => plano2.CodModelo == x.CodItem && x.CodProcesso.StartsWith("02")).DefaultIfEmpty()

								from plano3 in ( from pi in dtbSim.tbl_PlanoItem
												 join i in dtbSim.tbl_ItemPCP on pi.CodModelo equals i.CodItem
												 //where i.CodProcesso.StartsWith("02")
												 select new { pi.CodModelo, pi.CodItem, i.CodProcesso }
													).Where(x => plano2.CodModelo == x.CodItem).DefaultIfEmpty()
								//from pcp2 in dtbSim.tbl_ItemPCP.Where(x => plano3.CodModelo == x.CodItem && x.CodProcesso.StartsWith("02")).DefaultIfEmpty()

								from plano4 in ( from pi in dtbSim.tbl_PlanoItem
												 join i in dtbSim.tbl_ItemPCP on pi.CodModelo equals i.CodItem
												 //where i.CodProcesso.StartsWith("02")
												 select new { pi.CodModelo, pi.CodItem, i.CodProcesso }
													).Where(x => plano3.CodModelo == x.CodItem).DefaultIfEmpty()

								where jit.DatProducao == datHoje && (jit.CodProcesso.StartsWith("01")) && jit.CodLinha == linha.CodLinhaT3
								select new
								{
									Turno = jit.NumTurno,
									CodLinha = jit.CodLinha,
									CodModelo = plano4 != null && plano4.CodProcesso.StartsWith("02") ? plano4.CodModelo : 
												plano3 != null && plano3.CodProcesso.StartsWith("02") ? plano3.CodModelo : 
												plano2 != null && plano2.CodProcesso.StartsWith("02") ? plano2.CodModelo : 
												plano.CodModelo,
									CodPlaca = jit.CodPlaca,
									QtdPlano = (int)jit.QtdProdPM
								}).ToList();
				//var programas = programa.ToList();

				produzido.ForEach(prod =>
				{
					var plano = programa.FirstOrDefault(prog => prog.CodModelo == prod.CodModelo);
					if (plano != null)
					{
						prod.QtdPlano = plano.QtdPlano;
						prod.CodModelo = plano.CodPlaca;
						prod.CodLinha = plano.CodLinha;
					}
				});

				var horasTrab = 8.63;

				var obterHorasTrab = ObterHorasTrab(linha.CodLinha);

				if (obterHorasTrab == null) return new List<BarrasPainelDTO>();

				var resumo = produzido.Select(x => new BarrasPainelDTO()
				{
					CodLinha = x.CodLinha,
					CodModelo = x.CodModelo,
					DesModelo = x.DesModelo,
					Linha = x.DesLinha,
					Producao = x.QtdPlano,
					Embalados = x.QtdRealizado,
					DifModelo = x.QtdRealizado - x.QtdPlano,
					AparHora = (decimal)(Math.Round(x.QtdPlano / horasTrab, 2)),
					AparMin = (decimal)(Math.Round((x.QtdPlano / horasTrab) / 60, 2)),
					MinutosDia = obterHorasTrab.MinutosDia,
					QtdMinReal = obterHorasTrab.QtdMinReal,
					StatusLinha = obterHorasTrab.Intervalo,
					ProdEsperada = (int)((decimal)(Math.Round((x.QtdPlano / horasTrab) / 60, 2)) * obterHorasTrab.MinutosDia),
					Diferenca = x.QtdRealizado - (int)((decimal)(Math.Round((x.QtdPlano / horasTrab) / 60, 2)) * obterHorasTrab.MinutosDia)
				}).ToList();

				return resumo;

			}
		}

		public static List<BarrasPainelDTO> ObterPainelProducaoIAC(int codLinha)
		{
			using (var dtbCB = new DTB_CBContext())
			using (var dtbSim = new DTB_SIMContext())
			{
				DateTime datIni = new DateTime();
				DateTime datFim = new DateTime();

				DateTime Hora = daoUtil.GetDate();
				TimeSpan HoraAtual = new TimeSpan(Hora.Hour, Hora.Minute, Hora.Second);

				var linha = daoConfiguracao.ObterLinha(codLinha);

				if (linha == null) return new List<BarrasPainelDTO>();

				datIni = daoGeral.ObterDataInicio(linha.CodLinha, HoraAtual);

				if ((Hora.Hour >= 0) && (Hora.Hour < 7))
					datIni = datIni.AddDays(-1);

				datFim = daoGeral.ObterDataFim(linha.CodLinha, HoraAtual);

				if ((datFim.Hour >= 0) && (datFim.Hour < 7))
					datFim = datFim.AddDays(1);

				var datHoje = DateTime.Parse(DateTime.Now.ToShortDateString());

				var ultLeitura = (from p in dtbCB.tbl_CBEmbalada
								  where p.CodLinha == codLinha && p.NumAP != ""
								  orderby p.DatLeitura descending
								  select p).FirstOrDefault();

				var ultnumserie = ultLeitura != null ? ultLeitura.NumECB.Substring(0, 6) : "";

				var produzidoQ = (from p in dtbCB.tbl_CBEmbalada
								  join item in dtbCB.tbl_Item on p.CodModelo equals item.CodItem
								  where p.CodLinha == codLinha && p.DatLeitura >= datIni && p.DatLeitura <= datFim && p.NumECB.StartsWith(ultnumserie)
								  group p by new { item.CodItem, item.DesItem } into grupo
								  select new ResumoProducao()
								  {
									  CodModelo = grupo.Key.CodItem,
									  DesModelo = grupo.Key.DesItem,
									  QtdRealizado = grupo.Count(),
									  DesLinha = linha.DesLinha
								  });
				var produzido = produzidoQ.ToList();

				var programa = (from jit in dtbSim.tbl_ProgDiaPlaca
								where jit.DatProducao == datHoje && (jit.CodProcesso.StartsWith("01")) && jit.CodLinha == linha.CodLinhaT3
								select new
								{
									Turno = jit.NumTurno,
									CodLinha = jit.CodLinha,
									CodModelo = jit.CodPlaca,
									QtdPlano = (int)jit.QtdProdPM
								}).ToList();
				//var programas = programa.ToList();

				produzido.ForEach(prod =>
				{
					var plano = programa.FirstOrDefault(prog => prog.CodModelo == prod.CodModelo);
					if (plano != null)
					{
						prod.QtdPlano = plano.QtdPlano;
						prod.CodModelo = plano.CodModelo;
						prod.CodLinha = plano.CodLinha;
					}
				});

				var horasTrab = 8.63;

				var obterHorasTrab = ObterHorasTrab(linha.CodLinha);

				if (obterHorasTrab == null) return new List<BarrasPainelDTO>();

				var resumo = produzido.Select(x => new BarrasPainelDTO()
				{
					CodLinha = x.CodLinha,
					CodModelo = x.CodModelo,
					DesModelo = x.DesModelo,
					Linha = x.DesLinha,
					Producao = x.QtdPlano,
					Embalados = x.QtdRealizado,
					DifModelo = x.QtdRealizado - x.QtdPlano,
					AparHora = (decimal)(Math.Round(x.QtdPlano / horasTrab, 2)),
					AparMin = (decimal)(Math.Round((x.QtdPlano / horasTrab) / 60, 2)),
					MinutosDia = obterHorasTrab.MinutosDia,
					QtdMinReal = obterHorasTrab.QtdMinReal,
					StatusLinha = obterHorasTrab.Intervalo,
					ProdEsperada = (int)((decimal)(Math.Round((x.QtdPlano / horasTrab) / 60, 2)) * obterHorasTrab.MinutosDia),
					Diferenca = x.QtdRealizado - (int)((decimal)(Math.Round((x.QtdPlano / horasTrab) / 60, 2)) * obterHorasTrab.MinutosDia),
					NumAP = ultLeitura != null ? ultLeitura.NumAP : ""
				}).ToList();

				return resumo;

			}
		}

		public static List<MensagemPainelDTO> ObterMensagens()
		{

			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var mensagens = dtbSim.tbl_MensagemPainel.Where(x => String.IsNullOrEmpty(x.Destino) && x.flgAtivo == "s").ToList();

				return mensagens.Select(x => new MensagemPainelDTO()
				{
					CodMensagem = x.CodMensagem,
					Data = x.Data,
					Destino = x.Destino,
					flgAtivo = x.flgAtivo,
					Mensagem = x.Mensagem,
					nomusuario = x.nomusuario
				}).ToList();
			}
		}

		public static List<BarrasPainelLinhaMSG> ObterMensagensLinha(string codLinha)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				try
				{
					var resumo = dtbSim.Database.SqlQuery<BarrasPainelLinhaMSG>(
						"EXEC spc_BarrasPainelLinha_MSG @CodLinha",
						new SqlParameter("CodLinha", codLinha)
						).ToList();

					return resumo;
				}
				catch (Exception)
				{
					return new List<BarrasPainelLinhaMSG>();
				}
			}
		}

		public static CBObterHorasTrab ObterHorasTrab(int codLinha)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				try
				{
					var obterHorasTrab = dtbCB.Database.SqlQuery<CBObterHorasTrab>(
						"EXEC spc_CBObterHorasTrab @CodLinha",
						new SqlParameter("CodLinha", codLinha)
						).FirstOrDefault();
					return obterHorasTrab;
				}
				catch (Exception)
				{
					return null;
				}
			}
		}

		public static string ObterModeloFase(string codModelo, string fase)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				var modelo = dtbCB.Database.SqlQuery<string>(
									"EXEC spc_CBModeloFase @CodModelo, @Fase",
									new SqlParameter("CodModelo", codModelo),
									new SqlParameter("Fase", fase)
									).FirstOrDefault();
				return modelo;
			}
		}

		#region Manutenção mensagem painel

		public static List<MensagemPainelDTO> ObterMensagemPainel()
		{

			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var mensagens = dtbSim.tbl_MensagemPainel.ToList();

				return mensagens.Select(x => new MensagemPainelDTO()
				{
					CodMensagem = x.CodMensagem,
					Data = x.Data,
					Destino = x.Destino,
					flgAtivo = x.flgAtivo,
					Mensagem = x.Mensagem,
					nomusuario = x.nomusuario
				}).ToList();
			}
		}

		/// <summary>
		/// Rotina para buscar no banco de dados um determinado registro
		/// </summary>
		/// <param name="codMensagem"></param>
		/// <returns></returns>
		public static MensagemPainelDTO ObterMensagemPainel(long codMensagem)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from c in dtbSim.tbl_MensagemPainel
								 where c.CodMensagem.Equals(codMensagem)
								 select new MensagemPainelDTO()
								 {
									 CodMensagem = c.CodMensagem,
									 Data = c.Data,
									 Destino = c.Destino.Trim(),
									 flgAtivo = c.flgAtivo,
									 Mensagem = c.Mensagem,
									 nomusuario = c.nomusuario
								 }).FirstOrDefault();

				return resultado;

			}
		}

		/// <summary>
		/// Rotina de manuntenção de dados, para inclusão e alteração
		/// </summary>
		/// <param name="mensagem"></param>
		public static void GravarMensagemPainel(MensagemPainelDTO mensagem)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var existe =
					dtbSim.tbl_MensagemPainel.FirstOrDefault(
						x => x.CodMensagem.Equals(mensagem.CodMensagem));

				if (existe != null)
				{
					existe.Data = mensagem.Data;
					existe.Destino = mensagem.Destino;
					existe.flgAtivo = mensagem.flgAtivo;
					existe.Mensagem = mensagem.Mensagem;
					existe.nomusuario = mensagem.nomusuario;
					dtbSim.SaveChanges();
				}
				else
				{
					var nMensagem = new tbl_MensagemPainel()
					{
						Data = mensagem.Data,
						Destino = mensagem.Destino,
						flgAtivo = mensagem.flgAtivo,
						Mensagem = mensagem.Mensagem,
						nomusuario = mensagem.nomusuario
					};

					dtbSim.tbl_MensagemPainel.Add(nMensagem);
					dtbSim.SaveChanges();
				}
			}
		}

		/// <summary>
		/// Rotina para excluir um registro do banco de dados
		/// </summary>
		/// <param name="codLinha"></param>
		public static void RemoverMensagemPainel(long codMensagem)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var existe =
					dtbSim.tbl_MensagemPainel.FirstOrDefault(
						x => x.CodMensagem.Equals(codMensagem));

				if (existe == null) return;

				dtbSim.tbl_MensagemPainel.Remove(existe);
				dtbSim.SaveChanges();
			}
		}


		#endregion
		
	}
}
