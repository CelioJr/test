using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBRastLocal
    {
        public string NumSerieRastLocal { get; set; }
        public string NumSerieProd { get; set; }
        public Nullable<System.DateTime> DatRegistro { get; set; }
        public string CodDRT { get; set; }
    }
}
