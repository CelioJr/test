using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_PARAMETRO_DTO
    {
        public int FRM_CODIGO { get; set; }
        public int COM_CODIGO { get; set; }
        public int ACO_CODIGO { get; set; }
        public string ACC_MOMENTO { get; set; }
        public string PAR_NOME { get; set; }
        public string PAR_VALOR { get; set; }
        public virtual FR_ACAOCOMPONENTE_DTO FR_ACAOCOMPONENTE { get; set; }
    }
}
