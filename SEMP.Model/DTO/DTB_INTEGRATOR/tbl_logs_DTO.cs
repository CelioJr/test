namespace SEMP.Model.DTO
{
	using System;
	using System.ComponentModel.DataAnnotations;

	public partial class tbl_logs_DTO
	{
        public int ID { get; set; }

        public DateTime DATAEXEC { get; set; }

        public int ID_TAREFA { get; set; }

        public int ID_ERRO { get; set; }

        [StringLength(300)]
        public string MSG { get; set; }

        [StringLength(2)]
        public string TIPO { get; set; }

    }
}
