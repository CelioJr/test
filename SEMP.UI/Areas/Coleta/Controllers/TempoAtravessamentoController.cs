﻿using RAST.BO.Rastreabilidade;
using SEMP.Model.VO.Rastreabilidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEMP.Areas.Coleta.Controllers
{
	public class TempoAtravessamentoController : Controller
	{
		// GET: Coleta/TempoAtravessamento
		public ActionResult Index()
		{
			return View();
		}

		#region Telas
		public ActionResult Lista(string datInicio, string datFim, string codModelo)
		{
			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}

			try
			{
				dFim = dFim.AddDays(1).AddMinutes(-1);

				var dado = TempoAtravessamentoBO.ObterAtravessamento(dIni, dFim, codModelo);

				return View(dado);
			}
			catch (Exception ex)
			{
				ViewBag.Erro = "Erro ao consultar: " + ex.Message;
				return View(new List<TempoAtravessamento>());
			}
		}

		public ActionResult ListaResumo(string datInicio, string datFim, string codModelo, string numSerie)
		{
			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}

			try
			{
				dFim = dFim.AddDays(1).AddMinutes(-1);

				var dado = String.IsNullOrEmpty(numSerie) ? TempoAtravessamentoBO.ObterAtravessamentoResumo(dIni, dFim, codModelo) : TempoAtravessamentoBO.ObterAtravessamentoSerie(numSerie);

				return View(dado);
			}
			catch (Exception ex)
			{
				ViewBag.Erro = "Erro ao consultar: " + ex.Message;
				return View(new List<TempoAtravessamentoResumo>());
			}
		}

		public ActionResult AnaliseModelo(string codModelo, string datInicio, string datFim, string flgExcel, string numSerie) {
			if (!String.IsNullOrEmpty(flgExcel) && flgExcel == "S")
			{
				Response.AddHeader("content-disposition", "attachment; filename=TempoAtravessamento.xls");
				Response.ContentType = "application/ms-excel";
			}
			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}
            dFim = dFim.AddDays(1).AddMinutes(-1);

			try
			{

				//var dado = TempoAtravessamentoBO.ObterAtravessamentoResumo(dIni, dFim, codModelo);
				//var resumo = TempoAtravessamentoBO.ObterAtravessamento(dIni, dFim, codModelo);

				var dado2 = TempoAtravessamentoBO.ObterResumoItem(dIni, dFim, codModelo);
				var metas = TempoAtravessamentoBO.ObterMetas(codModelo);

				ViewBag.metas = metas;

				return View(dado2);
			}
			catch (Exception ex)
			{
				ViewBag.Erro = "Erro ao consultar: " + ex.Message;
				return View(new List<SEMP.Model.DTO.CBTAItemDTO>());
			}

		}

        /// <summary>
        /// Listagem com o detalhamento das leituras por modelo selecionado
        /// </summary>
        /// <param name="datInicio"></param>
        /// <param name="datFim"></param>
        /// <param name="codModelo"></param>
        /// <param name="flgOrdem"></param>
        /// <returns></returns>
		public ActionResult ListaModelo(string datInicio, string datFim, string codModelo, string flgOrdem, string numSerie)
		{
			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}

			if (String.IsNullOrEmpty(codModelo)) {
				ViewBag.Erro = "Informe o Modelo";
				return View(new List<TempoAtravessamentoModelo>());
			}

			try
			{

				if (dIni == dFim){
					dFim = dIni.AddDays(1).AddMinutes(-1);
				}

				var dado = String.IsNullOrEmpty(numSerie) ? TempoAtravessamentoBO.ObterAtravessamentoModelo(dIni, dFim, codModelo) : TempoAtravessamentoBO.ObterAtravessamentoSerieDados(numSerie);

				if (!String.IsNullOrEmpty(flgOrdem)) {
					if(flgOrdem == "1") {
						dado = dado.OrderByDescending(x => x.TempoAtravessamento + x.TempoEspera).ToList();
					}
					else if (flgOrdem == "2")
					{
						dado = dado.OrderByDescending(x => x.TempoAtravessamento).ToList();
					}
					else if (flgOrdem == "3")
					{
						dado = dado.OrderByDescending(x => x.TempoEspera).ToList();
					}
					else if (flgOrdem == "4")
					{
						dado = dado.OrderByDescending(x => x.TempoCorrido).ToList();
					}
					if (flgOrdem == "5")
					{
						dado = dado.OrderByDescending(x => x.TempoIAC).ToList();
					}
					else if (flgOrdem == "6")
					{
						dado = dado.OrderByDescending(x => x.TempoIMC).ToList();
					}
					else if (flgOrdem == "7")
					{
						dado = dado.OrderByDescending(x => x.TempoMF).ToList();
					}
				}

				return View(dado);
			}
			catch (Exception ex)
			{
				ViewBag.Erro = "Erro ao consultar: " + ex.Message;
				return View(new List<TempoAtravessamentoModelo>());
			}
		}

        /// <summary>
        /// Rotina que retorna uma listagem com os números de série de uma determinada classe do histogrma, conforme o período de tempo selecionado
        /// </summary>
        /// <param name="datInicio">Data inicial a ser analisada</param>
        /// <param name="datFim">Data final a ser analisada</param>
        /// <param name="codModelo">Modlo</param>
        /// <param name="menor">Menor tempo de atravessamento</param>
        /// <param name="maior">Maior tempo de atravessamento</param>
        /// <param name="flgTipoGrafico">Tipo de gráfico</param>
        /// <returns></returns>
		public ActionResult ListaClasse(string datInicio, string datFim, string codModelo, int menor, int maior, string flgTipoGrafico = "P")
		{
			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}

			if (String.IsNullOrEmpty(codModelo))
			{
				ViewBag.Erro = "Informe o Modelo";
				return View(new List<TempoAtravessamentoModelo>());
			}

			try
			{

				var dado = TempoAtravessamentoBO.ObterResumoItem(dIni, dFim, codModelo, menor, maior, flgTipoGrafico);

				return View(dado);
			}
			catch (Exception ex)
			{
				ViewBag.Erro = "Erro ao consultar: " + ex.Message;
				return View(new List<TempoAtravessamentoModelo>());
			}
		}

		public ActionResult ListaResumoAP(string datInicio, string datFim, string flgDia, string flgDefeitos, string flgFaltaLeitura, string codFab, string numAP)
		{
			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
				dIni = dIni.AddDays(-dIni.Day).AddDays(1);
			}

			dFim = dIni.AddMonths(1).AddDays(-1);

			try
			{

				var dado = TempoAtravessamentoBO.ObterAtravessamentoResumoAP(flgDia, flgDefeitos, flgFaltaLeitura, codFab, numAP);

				return View(dado);
			}
			catch (Exception ex)
			{
				ViewBag.Erro = "Erro ao consultar: " + ex.Message;
				return View(new List<TempoAtravessamentoResumo>());
			}
		}

		#endregion

		#region Gráficos
		public ActionResult Grafico(string datInicio, string datFim, string codModelo, int? qtdAparelhos)
		{

			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}
			var dFim = dIni.AddMonths(1).AddDays(-1);

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}

			var grafico = TempoAtravessamentoBO.ObterAtravessamento(dIni, dFim, codModelo);

			ViewBag.QtdTotal = grafico.Count();

			if (qtdAparelhos != null && qtdAparelhos > 0){
				grafico = grafico.Take(qtdAparelhos.Value).ToList();
			}
			return View(grafico);
		}

		public ActionResult GraficoModelos(string datInicio, string datFim)
		{

			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}
			var dFim = dIni.AddMonths(1).AddDays(-1);

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}

			var grafico = TempoAtravessamentoBO.ObterResumoModelos(dIni, dFim);

			return View(grafico);
		}

		public ActionResult GraficoModelo(string datInicio, string datFim, string codModelo, string MargemDesvio)
		{

			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}

			if (String.IsNullOrEmpty(codModelo))
			{
				ViewBag.Erro = "Informe o Modelo";
				return View(new List<TempoAtravessamentoModelo>());
			}

			try
			{

				var dado = TempoAtravessamentoBO.ObterAtravessamentoModelo(dIni, dFim, codModelo);


				return View(dado);
			}
			catch (Exception ex)
			{
				ViewBag.Erro = "Erro ao consultar: " + ex.Message;
				return View(new List<TempoAtravessamentoModelo>());
			}
		}

		public ActionResult GraficoEvolucao(string datInicio, string datFim, string codModelo)
		{

			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}

			if (String.IsNullOrEmpty(codModelo))
			{
				ViewBag.Erro = "Informe o Modelo";
				return View(new List<TempoAtravessamentoModelo>());
			}

			try
			{

				var dado = TempoAtravessamentoBO.ObterMediaMensal(dIni, dFim, codModelo);

				return View(dado);
			}
			catch (Exception ex)
			{
				ViewBag.Erro = "Erro ao consultar: " + ex.Message;
				return View(new List<TempoAtravessamentoModelo>());
			}
		}

		public ActionResult GraficoABC(string datInicio, string datFim, string codModelo, string MargemDesvio)
		{

			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}

			if (String.IsNullOrEmpty(codModelo))
			{
				ViewBag.Erro = "Informe o Modelo";
				return View(new List<TempoAtravessamentoModelo>());
			}

			try
			{

				var dado = TempoAtravessamentoBO.ObterAtravessamentoModelo(dIni, dFim, codModelo);

				return View(dado);
			}
			catch (Exception ex)
			{
				ViewBag.Erro = "Erro ao consultar: " + ex.Message;
				return View(new List<TempoAtravessamentoModelo>());
			}
		}

		public ActionResult GraficoHistograma(string datInicio, string datFim, string codModelo, string flgTipoGrafico = "P", string flgFase = "")
		{

			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
				dFim = dFim.AddDays(1).AddMinutes(-1);
			}

			var dados = TempoAtravessamentoBO.ObterHistogramaAtravessa(dIni, dFim, codModelo, flgTipoGrafico, flgFase);

			return View(dados);

		}
		#endregion

	}

}