﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SEMP.UIColeta.Models
{
	public class Menu
	{
		public int Codigo { get; set; }
		public string Titulo { get; set; }
		public string Endereco { get; set; }
		public string Legenda { get; set; }
		public string CodPermissao { get; set; }
		public List<Menu> Items { get; set; }
	}
}