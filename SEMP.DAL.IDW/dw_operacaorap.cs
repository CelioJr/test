namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_operacaorap
    {
        [Key]
        public long id_operacaorap { get; set; }

        public long? id_operacao { get; set; }

        public long? id_rap { get; set; }

        public virtual dw_operacao dw_operacao { get; set; }
    }
}
