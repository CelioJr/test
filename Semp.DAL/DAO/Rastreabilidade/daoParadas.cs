﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.Models;
using SEMP.Model.DTO;

namespace SEMP.DAL.DAO.Rastreabilidade
{
	public class daoParadas
	{
        //static daoParadas()
        //{
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<tbl_ProdParada, ProdParadaDTO>();
        //        cfg.CreateMap<tbl_ProdParadaDep, ProdParadaDepDTO>();
        //    });
        //}

        public static TimeSpan ToTimeSpan(String hora)
		{
			//DateTime t = DateTime.ParseExact(hora, "hh:mm", CultureInfo.InvariantCulture);
			if (!hora.Trim().Equals(String.Empty))
			{
				TimeSpan thora = TimeSpan.Parse(hora);//t.TimeOfDay;

				return thora;
			}
			else
			{
				return new TimeSpan();
			}
		}

		public static List<ProdParadaDTO> ObterParadas(DateTime DatInicio, DateTime DatFim, int Turno, string Linha)
		{
			TimeSpan HoraInicio;
			TimeSpan HoraFim;

			if (Turno == 1)
			{
				HoraInicio = new TimeSpan(07, 0, 0);
				HoraFim = new TimeSpan(17, 7, 59);
			}
			else
			{
				HoraInicio = new TimeSpan(17, 8, 0);
				HoraFim = new TimeSpan(02, 20, 0);
			}

			using (var dtbSim = new DTB_SIMContext())
			{

				var paradas = (from p in dtbSim.tbl_ProdParada
							   join pp in dtbSim.tbl_Parada on new {p.CodParada, p.CodFab} equals new {pp.CodParada, pp.CodFab}
							   where p.DatParada >= DatInicio && p.DatParada <= DatFim && p.CodLinha.Trim() != String.Empty
										&& p.CodLinha == Linha
							   select new
							   {
								   CodFab = p.CodFab,
								   CodItem = p.CodItem,
								   CodLinha = p.CodLinha,
								   CodModelo = p.CodModelo,
								   CodParada = p.CodParada,
								   CodTurno = p.CodTurno,
								   DatParada = p.DatParada,
								   DesObs = pp.DesParada  + " - Obs:" + p.DesObs.Trim(),
								   HorAlmoco = p.HorAlmoco,
								   HorFim = p.HorFim,
								   HorInicio = p.HorInicio,
								   NumDrt = p.NumDrt,
								   NumFuncionarios = p.NumFuncionarios,
								   NumParada = p.NumParada,
								   QtdFalta = p.QtdFalta,
								   QtdItem = p.QtdItem,
								   QtdNaoProduzida = p.QtdNaoProduzida,
								   rowguid = p.rowguid,
								   StatusEmail = p.StatusEmail,
								   StatusPerda = p.StatusPerda
							   }).ToList();

				//List<tbl_ProdParada> parada = paradas.Where(x => (ToTimeSpan(x.HorInicio) >= HoraInicio && ToTimeSpan(x.HorInicio) <= HoraFim)).Select(p => new tbl_ProdParada

				List<tbl_ProdParada> parada = paradas.Where(x =>
					HoraInicio < HoraFim ?
						(ToTimeSpan(x.HorInicio) >= HoraInicio && ToTimeSpan(x.HorInicio) <= HoraFim)
					:
						(ToTimeSpan(x.HorInicio) >= HoraInicio && ToTimeSpan(x.HorFim) <= HoraFim)
				).Select(p => new tbl_ProdParada
				{
					CodFab = p.CodFab,
					CodItem = p.CodItem,
					CodLinha = p.CodLinha,
					CodModelo = p.CodModelo,
					CodParada = p.CodParada,
					CodTurno = p.CodTurno,
					DatParada = p.DatParada,
					DesObs = p.DesObs,
					HorAlmoco = p.HorAlmoco,
					HorFim = p.HorFim,
					HorInicio = p.HorInicio,
					NumDrt = p.NumDrt,
					NumFuncionarios = p.NumFuncionarios,
					NumParada = p.NumParada,
					QtdFalta = p.QtdFalta,
					QtdItem = p.QtdItem,
					QtdNaoProduzida = p.QtdNaoProduzida,
					rowguid = p.rowguid,
					StatusEmail = p.StatusEmail,
					StatusPerda = p.StatusPerda
				}).ToList();

				List<ProdParadaDTO> paradasDTO = new List<ProdParadaDTO>();

				AutoMapper.Mapper.Map(parada, paradasDTO);

				return paradasDTO;
			}

		}

		public static List<ProdParadaDepDTO> ObterParadaDep(string NumParada)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var paradas = (from p in dtbSim.tbl_ProdParadaDep
							   where p.NumParada == NumParada
							   select p).ToList();
				List<ProdParadaDepDTO> paradasDTO = new List<ProdParadaDepDTO>();

				AutoMapper.Mapper.Map(paradas, paradasDTO);

				return paradasDTO;
			}
		}

	}
}

