using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_EVENTOS_DTO
    {
        public int COD_EVENTO { get; set; }
        public string EVENTO { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
        public string USUARIO { get; set; }
        public string OPERACAO { get; set; }
        public string DADOS_ANTERIORES { get; set; }
        public string DADOS_ATUAIS { get; set; }
        public string PACKSTRING { get; set; }
        public string PALLET { get; set; }
        public string SERIAL { get; set; }
        public string IMEI { get; set; }
        public Nullable<int> TIPO { get; set; }
    }
}
