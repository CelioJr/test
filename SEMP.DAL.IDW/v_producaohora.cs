﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEMP.DAL.IDW
{
	public partial class v_producaohora
	{
		[Key]
		[Column(Order = 1)]
		public Nullable<DateTime> dthr_ihora { get; set; }
		[Key]
		[Column(Order = 2)]
		public Nullable<DateTime> dthr_fhora { get; set; }
		[Key]
		[Column(Order = 3)]
		public string cd_pt { get; set; }
		[Key]
		[Column(Order = 4)]
		public string cd_cp { get; set; }
		public string cd_folha { get; set; }
		public Nullable<long> producaobruta { get; set; }

	}
}
