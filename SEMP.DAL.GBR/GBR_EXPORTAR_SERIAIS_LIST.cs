namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_EXPORTAR_SERIAIS_LIST
    {
        [Key]
        public int GBR_EXP_ID { get; set; }

        [Required]
        [StringLength(20)]
        public string SERIAL { get; set; }

        public DateTime? DATA { get; set; }

        [StringLength(20)]
        public string USUARIO { get; set; }

        [StringLength(20)]
        public string SKU { get; set; }

        [StringLength(20)]
        public string CODETIQUETA { get; set; }

        public int? COD_SKU { get; set; }
    }
}
