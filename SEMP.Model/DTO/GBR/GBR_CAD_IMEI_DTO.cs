using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_CAD_IMEI_DTO
    {
        public int COD_IMEI { get; set; }
        public Nullable<int> COD_SMT { get; set; }
        public string IMEI { get; set; }
        public Nullable<int> ORDEM { get; set; }
        public Nullable<bool> FLAG_IMEI { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
    }
}
