﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using RAST.BO.Rastreabilidade;
using SEMP.Model.DTO;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using SEMP.DAL.DAO.Rastreabilidade;

namespace RAST.BO.Relatorios
{
	public class AuditorSerie
	{

		#region Busca as faltas
		public static List<PassagemSerie> ObterSeriesFalta(DateTime DatInicio, DateTime DatFim, int CodLinha)
		{
			List<PassagemSerie> passagem = new List<PassagemSerie>();

			var modelos = daoAuditorSerie.ObterModelos(DatInicio, DatFim, CodLinha);

			foreach (var modelo in modelos)
			{
				var passagens = ObterSeriesFalta(DatInicio, DatFim, CodLinha, modelo);
				passagem.AddRange(passagens);
			}

			return passagem;

		}

		public static List<PassagemSerie> ObterSeriesFalta(DateTime DatInicio, DateTime DatFim, int CodLinha, String CodModelo)
		{
			//Busca as passagens por período/linha/modelo
			var passagem = daoAuditorSerie.ObterPassagem(DatInicio, DatFim, CodLinha, CodModelo);

			//Busca os postos obrigatórios
			var postos = daoAuditorSerie.ObterPostosObrigatorios(CodLinha);

			// Calcula a quantidade de postos padrão da linha
			int totalpostos = postos.GroupBy(g => g.NumSeq).Count();

			passagem = passagem.Where(x => (postos.Any(p => p.NumPosto == x.NumPosto))).ToList();

			// Agrupa as passagens pelo número de série e seleciona aqueles que tem menos leitura que o total de postos padrão
			var passagempostos = passagem.GroupBy(g => g.NumSerie).Select(i => new { NumSerie = i.Key, Qtde = i.Count() }).Where(x => x.Qtde < totalpostos).ToList();

			// Seleciona das passagens aqueles que foram identificados com falta de leitura
			passagem = passagem.Join(passagempostos,
				p => p.NumSerie,
				c => c.NumSerie,
				(p, c) => p).ToList();

			// Dados do modelo (Descrição e código com dígito verificador
			string desModelo = daoGeral.ObterDescricaoItem(CodModelo);
			string codigomodelo = "";
			if (passagem.Any())
			{
				codigomodelo = passagem.First().NumSerie.Substring(0, 10);
			}
			else
			{
				codigomodelo = CodModelo;
			}

			// Rotina portada para uma Stored Procedure para melhorar desempenho
			List<string> naolidos = daoAuditorSerie.ObterNaoLidos(DatInicio, DatFim, CodLinha, CodModelo);

			// Se encontrou dados, insere nos dados da passagem
			if (naolidos != null)
			{

				foreach (string serie in naolidos)
				{
					var naoecontrado = new PassagemSerie();

					naoecontrado.CodModelo = CodModelo;
					naoecontrado.NumSerie = codigomodelo + String.Format(serie, "000000");
					naoecontrado.CodLinha = CodLinha;
					naoecontrado.DesModelo = desModelo;
					naoecontrado.DatLeitura = "";

					passagem.Add(naoecontrado);
				}
			}

			// Agrupa os números de série para encontrar as leituras no sistema GPA
			var passagens = (from p in passagem
							 group p by new { p.NumSerie, p.CodLinha, p.CodModelo, p.DesModelo } into x
							 select new PassagemSerie()
							 {
								 CodLinha = x.Key.CodLinha,
								 CodModelo = x.Key.CodModelo,
								 DesModelo = x.Key.DesModelo,
								 NumSerie = x.Key.NumSerie
							 }).ToList();

			// Varre as séries que encontrou e cria o registro de passagem pelo GPA, com código de posto 99 e dos demais postos que estão faltando
			foreach (var passagemSeries in passagens)
			{
				var postosSerie = passagem.Where(x => x.NumSerie == passagemSeries.NumSerie).Select(x => x.NumPosto).ToList();
				var postosFaltantes = postos.Where(x => !postosSerie.Contains(x.NumPosto)).ToList();

				// inserir na passagem os postos que estão faltando, deixando a data em branco
				postosFaltantes.ForEach(x => passagem.Add(new PassagemSerie()
				{
					CodLinha = x.CodLinha,
					NumPosto = x.NumPosto,
					CodModelo = passagemSeries.CodModelo,
					DesModelo = passagemSeries.DesModelo,
					NumSerie = passagemSeries.NumSerie,
					DatLeitura = ""
				}));

				// Incluir passagem no posto técnico
				passagem.Add(ObterPassagemTecnico(passagemSeries));
				// Incluir passagem no GPA
				passagem.Add(ObterPassagemExped(passagemSeries));
				// Incluir qtde desassociações
				passagem.Add(ObterPassagemDesassociados(passagemSeries));

			}

			return passagem.OrderBy(x => x.NumSerie).ToList();

		}

		public static List<PassagemSerie> ObterSeriesFalta(int CodLinha)
		{
			List<PassagemSerie> passagem = new List<PassagemSerie>();

			var modelos = daoAuditorSerie.ObterModelos(CodLinha);

			foreach (var modelo in modelos)
			{
				var passagens = ObterSeriesFalta(CodLinha, modelo);
				passagem.AddRange(passagens);
			}

			return passagem;

		}

		public static List<PassagemSerie> ObterSeriesFalta(int CodLinha, String CodModelo)
		{
			var passagem = daoAuditorSerie.ObterPassagem(CodLinha, CodModelo);
			var postos = daoAuditorSerie.ObterPostosObrigatorios(CodLinha);

			var postoslinha = daoGeral.ObterPostoPorLinha(CodLinha).FirstOrDefault(x => x.CodTipoPosto == 4 || x.CodTipoPosto == 10 || x.CodTipoPosto == 9);

			var repetidos = (from p2 in passagem
							 where postoslinha != null && p2.NumPosto == postoslinha.NumPosto
							 select p2.NumSerie).ToList();

			passagem = (from p in passagem
						where !repetidos.Contains(p.NumSerie)
						select p).ToList();

			int totalpostos = postos.GroupBy(g => g.NumSeq).Count();

			var passagempostos = passagem.GroupBy(g => g.NumSerie).Select(i => new { NumSerie = i.Key, Qtde = i.Count() }).Where(x => x.Qtde < totalpostos).ToList();

			passagem = passagem.Join(passagempostos,
				p => p.NumSerie,
				c => c.NumSerie,
				(p, c) => p).ToList();

			return passagem.OrderBy(x => x.NumSerie).ToList();

		}
		#endregion

		#region Obter passagem das séries
		public static List<PassagemSerie> ObterPassagemSerie(String NumECB)
		{
			var passagem = daoAuditorSerie.ObterPassagem(NumECB);

			string desModelo = daoGeral.ObterDescricaoItem(passagem.FirstOrDefault().CodModelo);
			
			//Busca os postos obrigatórios
			var postos = daoAuditorSerie.ObterPostosObrigatorios(passagem.FirstOrDefault().CodLinha);
			var postosSerie = passagem.Select(x => x.NumPosto).ToList();

			var postosFaltantes = postos.Where(x => !postosSerie.Contains(x.NumPosto)).ToList();

			// inserir na passagem os postos que estão faltando, deixando a data em branco
			postosFaltantes.ForEach(x => passagem.Add(new PassagemSerie()
			{
				CodLinha = x.CodLinha,
				NumPosto = x.NumPosto,
				CodModelo = NumECB.Substring(0, 6),
				DesModelo = desModelo,
				NumSerie = NumECB,
				DatLeitura = ""
			}));

			// Incluir passagem no posto técnico
			passagem.Add(ObterPassagemTecnico(passagem.FirstOrDefault()));
			// Incluir passagem no GPA
			passagem.Add(ObterPassagemExped(passagem.FirstOrDefault()));
			// Incluir qtde desassociações
			passagem.Add(ObterPassagemDesassociados(passagem.FirstOrDefault()));

			return passagem;

		}

		public static List<PassagemSerie> ObterPassagemLinha(int codLinha, DateTime datIni, DateTime datFim)
		{
			var passagem = daoAuditorSerie.ObterPassagem(datIni, datFim, codLinha);

			//Busca os postos obrigatórios
			var postos = daoAuditorSerie.ObterPostosObrigatorios(passagem.FirstOrDefault().CodLinha);
			var postosSerie = passagem.Select(x => x.NumPosto).ToList();

			var postosFaltantes = postos.Where(x => !postosSerie.Contains(x.NumPosto)).ToList();

			// inserir na passagem os postos que estão faltando, deixando a data em branco
			postosFaltantes.ForEach(x => passagem.Add(new PassagemSerie()
			{
				CodLinha = x.CodLinha,
				NumPosto = x.NumPosto,
				CodModelo = "",
				DesModelo = "",
				NumSerie = "",
				DatLeitura = ""
			}));

			var outros = new List<PassagemSerie>();

			foreach (var serie in passagem)
			{
				outros.Add(serie);
				// Incluir passagem no posto técnico
				outros.Add(ObterPassagemTecnico(serie));
				// Incluir passagem no GPA
				outros.Add(ObterPassagemExped(serie));
				// Incluir qtde desassociações
				outros.Add(ObterPassagemDesassociados(serie));
			}

			return outros;

		}
		#endregion

		#region Passagens extras
		public static PassagemSerie ObterPassagemTecnico(PassagemSerie serie)
		{
			var passagem = new PassagemSerie()
			{
				CodLinha = serie.CodLinha,
				NumPosto = 98,
				CodModelo = serie.CodModelo,
				DesModelo = serie.DesModelo,
				NumSerie = serie.NumSerie,
				DatLeitura = ""
			};

			// incluir pelo menos um registro do posto técnico
			var defeitos = daoDefeitos.ObterDefeitosSerie(serie.NumSerie).OrderByDescending(x => x.DatEvento).FirstOrDefault();
			if (defeitos != null)
			{
				passagem.DatLeitura = defeitos.DatEvento.ToString();
			}

			return passagem;
		}

		public static PassagemSerie ObterPassagemExped(PassagemSerie serie)
		{
			// posto GPA (Expedição)
			PassagemSerie exped = new PassagemSerie()
			{
				CodLinha = serie.CodLinha,
				CodModelo = serie.CodModelo,
				DesModelo = serie.DesModelo,
				NumPosto = 99,
				NumSerie = serie.NumSerie,
				DatLeitura = ""
			};

			var gpa = ObterDataGPA(serie.CodModelo, serie.SoSerie);

			if (gpa != null)
			{
				exped.DatLeitura = gpa.Value.ToString();
			}

			return exped;

		}

		public static PassagemSerie ObterPassagemDesassociados(PassagemSerie serie)
		{

			var passagem = new PassagemSerie()
			{
				CodLinha = serie.CodLinha,
				NumPosto = 100,
				CodModelo = serie.CodModelo,
				DesModelo = serie.DesModelo,
				NumSerie = serie.NumSerie
			};

			// Qtde Desassociado
			var qtdDes = AuditorSerie.ObterDefeitosDesassociacao(serie.NumSerie);
			passagem.DatLeitura = qtdDes.ToString();

			return passagem;
		}

		#endregion

		#region Funções acessórias
		public static List<CBPostoDTO> ObterPostosObrigatorios(int? CodLinha)
		{
			if (CodLinha != null)
			{
				var postos = daoAuditorSerie.ObterPostosObrigatorios(CodLinha.Value);

				postos.Add(new CBPostoDTO() { CodLinha = CodLinha.Value, NumPosto = 98, DesPosto = "Técnico", NumSeq = 98 });
				postos.Add(new CBPostoDTO() { CodLinha = CodLinha.Value, NumPosto = 99, DesPosto = "Exped.", NumSeq = 99 });

				return postos;
			}
			else
			{
				return null;
			}

		}

		public static List<CBPostoDTO> ObterPostosPassagem(string NumSerie)
		{

			return daoAuditorSerie.ObterPostosPassagem(NumSerie);

		}

		public static int ObterLinhaPorSerie(string NumSerie)
		{

			return daoAuditorSerie.ObterLinhaPorSerie(NumSerie);

		}

		public static int ObterDefeitosDesassociacao(String NumSerie)
		{
			return daoAuditorSerie.ObterDefeitosDesassociacao(NumSerie);

		}

		public static GpaRecItemDTO ObterLeituraGPA(String CodModelo, String NumSerie)
		{
			return daoAuditorSerie.ObterLeituraGPA(CodModelo, NumSerie);
		}

		public static DateTime? ObterDataGPA(String CodModelo, String NumSerie)
		{
			var gpa = daoAuditorSerie.ObterLeituraGPA(CodModelo, NumSerie);

			if (gpa.CodItem != null)
			{
				return gpa.DataLeitura;
			}
			else
			{
				return null;
			}

		}

		public static List<String> ObterModelos(DateTime DatInicio, DateTime DatFim, int CodLinha)
		{
			var modelos = daoAuditorSerie.ObterModelos(DatInicio, DatFim, CodLinha);

			return modelos;
		}

		#endregion

		#region Novos métodos

		public static List<ResumoSeries> ObterResumoNaoLidas(String CodModelo, String SerieIni, String SerieFim, DateTime DatIni, DateTime DatFim)
		{
			return daoAuditorSerie.ObterResumoNaoLidas(CodModelo, SerieIni, SerieFim, DatIni, DatFim);
		}

		public static List<SeriesAusentes> ObterSeriesNaoLidas(String CodModelo, String SerieIni, String SerieFim, DateTime DatIni, DateTime DatFim)
		{
			return daoAuditorSerie.ObterSeriesNaoLidas(CodModelo, SerieIni, SerieFim, DatIni, DatFim);
		}

		#endregion

		#region Consultas Gerais

		public static List<String> ObterPassagemSerie(DateTime DatInicio, DateTime DatFim, int CodLinha)
		{
			return daoAuditorSerie.ObterPassagemSerie(DatInicio, DatFim, CodLinha);
		
		}

		public static List<CBPassagemDTO> ObterPassagemLinha(DateTime DatInicio, DateTime DatFim, int CodLinha)
		{
			return daoAuditorSerie.ObterPassagemLinha(DatInicio, DatFim, CodLinha);

		}
		#endregion

		#region Outras validações

		public static List<CBEmbaladaDTO> ObterSeriesNaoAmarradas(DateTime startDate, int? codLinha, string codModelo) {

			return daoAuditorSerie.ObterSeriesNaoAmarradas(startDate, codLinha, codModelo);
		}

		public static List<CBPassagemDTO> ObterSeriesNaoEmbaladas(DateTime startDate, int? codLinha) {

			return daoAuditorSerie.ObterSeriesNaoEmbaladas(startDate, codLinha);
		}

		public static List<CBEmbaladaDTO> ObterEmbaladosIACIMC(DateTime startDate, int? codLinha, string codModelo){
			return daoAuditorSerie.ObterEmbaladosIACIMC(startDate, codLinha, codModelo);

		}
		#endregion

	}
}
