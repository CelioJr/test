namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_REGRAS_FUNCOES
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FUN_COD { get; set; }

        [Required]
        [StringLength(50)]
        public string FUN_NOME { get; set; }

        [Required]
        public string FUN_DESCRICAO { get; set; }

        [Required]
        [StringLength(50)]
        public string FUN_NOME_REAL { get; set; }

        public string FUN_PARAMS { get; set; }

        [StringLength(30)]
        public string FUN_RETORNO { get; set; }

        public int FUN_TIPO { get; set; }

        [StringLength(10)]
        public string FUN_COMPATIBILIDADE { get; set; }

        [StringLength(2000)]
        public string FUN_RESUMO { get; set; }

        public string FUN_CONTEUDO_SERVIDOR { get; set; }

        public string FUN_CONTEUDO_CLIENTE { get; set; }

        public string FUN_CONTEUDO_BANCO { get; set; }

        [StringLength(10)]
        public string FUN_VERSAO { get; set; }
    }
}
