using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
	public class tbl_CBPostoMap : EntityTypeConfiguration<tbl_CBPosto>
	{
		public tbl_CBPostoMap()
		{
			// Primary Key
			this.HasKey(t => new { t.CodLinha, t.NumPosto });

			// Properties
			this.Property(t => t.CodLinha)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.NumPosto)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.DesPosto)
				.HasMaxLength(50)
                .IsUnicode(false);

			this.Property(t => t.NumIP)
				.HasMaxLength(15)
                .IsUnicode(false);

			this.Property(t => t.CodDRT)
				.IsFixedLength()
				.HasMaxLength(8)
                .IsUnicode(false);

			this.Property(t => t.FlgTipoTerminal)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			this.Property(t => t.FlgAtivo)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			this.Property(t => t.TipoAmarra)
				.IsFixedLength()
				.HasMaxLength(2)
                .IsUnicode(false);

			this.Property(t => t.flgPedeAP)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			this.Property(t => t.flgImprimeEtq)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			this.Property(t => t.FlgGargalo)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.PostoBloqueado)
                .IsFixedLength()
                .HasMaxLength(100)
                .IsUnicode(false);

			// Table & Column Mappings
			this.ToTable("tbl_CBPosto");
			this.Property(t => t.CodLinha).HasColumnName("CodLinha");
			this.Property(t => t.NumPosto).HasColumnName("NumPosto");
			this.Property(t => t.CodTipoPosto).HasColumnName("CodTipoPosto");
			this.Property(t => t.DesPosto).HasColumnName("DesPosto");
			this.Property(t => t.NumIP).HasColumnName("NumIP");
			this.Property(t => t.CodDRT).HasColumnName("CodDRT");
			this.Property(t => t.FlgTipoTerminal).HasColumnName("FlgTipoTerminal");
			this.Property(t => t.FlgAtivo).HasColumnName("FlgAtivo");
			this.Property(t => t.NumSeq).HasColumnName("NumSeq");
			this.Property(t => t.flgObrigatorio).HasColumnName("flgObrigatorio");
			this.Property(t => t.flgDRTObrig).HasColumnName("flgDRTObrig");
			this.Property(t => t.TipoAmarra).HasColumnName("TipoAmarra");
			this.Property(t => t.flgPedeAP).HasColumnName("flgPedeAP");
			this.Property(t => t.flgImprimeEtq).HasColumnName("flgImprimeEtq");
			this.Property(t => t.TempoLeitura).HasColumnName("TempoLeitura");
			this.Property(t => t.TipoEtiqueta).HasColumnName("TipoEtiqueta");
			this.Property(t => t.TempoCiclo).HasColumnName("TempoCiclo");
			this.Property(t => t.FlgGargalo).HasColumnName("FlgGargalo");
            this.Property(t => t.PostoBloqueado).HasColumnName("PostoBloqueado");
            this.Property(t => t.TempoBloqueio).HasColumnName("TempoBloqueio");
            this.Property(t => t.flgValidaOrigem).HasColumnName("flgValidaOrigem");
            this.Property(t => t.MontarLote).HasColumnName("MontarLote");
			this.Property(t => t.flgBaixaEstoque).HasColumnName("flgBaixaEstoque");
		}
	}
}
