using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class TBL_GpaRecLinha
    {
        public long IdRegistro { get; set; }
        public string CodLinha { get; set; }
        public int QtdPorLinha { get; set; }
        public string CodItem { get; set; }
        public Nullable<int> QtdLida { get; set; }
        public Nullable<int> UltimoCodPalletLido { get; set; }
        public string Status { get; set; }
    }
}
