﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.Models
{
	public class viw_RelOrdemServicoSTS
	{
		public string NumOS { get; set; }
		public DateTime DatOS { get; set; }
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public string CodFam { get; set; }
		public string NomFam { get; set; }
		public string CodEstado { get; set; }
		public string NomEstado { get; set; }
		public string NumSerie { get; set; }
		public string NumNotaFiscal { get; set; }
		public string CodItem { get; set; }
		public string DesItem { get; set; }
		public string CodDefExec { get; set; }
		public string DesDefExec { get; set; }
		public string CodDefPeca { get; set; }
		public string DesDefPeca { get; set; }
		public string Posicao { get; set; }
		public decimal? Quantidade { get; set; }
		public string DefReclamado { get; set; }
		public string DefConstatado { get; set; }
		public string CodTecnico { get; set; }
		public string NomTecnico { get; set; }
		public string FlgStatus { get; set; }
		public string NomStatus { get; set; }
	}
}
