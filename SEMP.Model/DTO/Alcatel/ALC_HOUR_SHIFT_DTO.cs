using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_HOUR_SHIFT_DTO
    {
        public int ID_TARGET { get; set; }
        public Nullable<int> HOUR { get; set; }
        public Nullable<int> SEQ { get; set; }
        public Nullable<int> TARGET { get; set; }
        public virtual ALC_TARGET_HOUR_DTO ALC_TARGET_HOUR { get; set; }
    }
}
