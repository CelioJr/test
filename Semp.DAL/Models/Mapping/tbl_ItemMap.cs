using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_ItemMap : EntityTypeConfiguration<tbl_Item>
    {
        public tbl_ItemMap()
        {
            // Primary Key
            this.HasKey(t => t.CodItem);

            // Properties
            this.Property(t => t.CodItem)
                .IsRequired()
                .HasMaxLength(15)
                .IsUnicode(false);

            this.Property(t => t.DesItem)
                .IsRequired()
                .HasMaxLength(40)
                .IsUnicode(false);

            this.Property(t => t.CodDesenho)
                .HasMaxLength(40)
                .IsUnicode(false);

            this.Property(t => t.Origem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.CodUnid)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(4)
                .IsUnicode(false);

            this.Property(t => t.CodFam)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodNBM)
                .IsFixedLength()
                .HasMaxLength(12)
                .IsUnicode(false);

            this.Property(t => t.CodTEC)
                .IsFixedLength()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.Formatacao)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.FlgMecElet)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.Usuario)
                .IsFixedLength()
                .HasMaxLength(25)
                .IsUnicode(false);

            this.Property(t => t.CodEmbalagem)
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.PartNumberTos)
                .IsFixedLength()
                .HasMaxLength(18)
                .IsUnicode(false);

            this.Property(t => t.FlgEmbPadrao)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.CodTECSeq)
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.FlgMSD)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.NumNivelMSD)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.FlgVencto)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.FlgAtivoItem)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.CodTipoReceita)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodPisCofinsReceita)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.DesResumida)
                .HasMaxLength(16)
                .IsUnicode(false);

            this.Property(t => t.FlgComercializado)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.FlgItemVirtual)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.CodClasPPB)
                .HasMaxLength(3)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_Item");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.DesItem).HasColumnName("DesItem");
            this.Property(t => t.CodDesenho).HasColumnName("CodDesenho");
            this.Property(t => t.Origem).HasColumnName("Origem");
            this.Property(t => t.CodUnid).HasColumnName("CodUnid");
            this.Property(t => t.CodFam).HasColumnName("CodFam");
            this.Property(t => t.CodNBM).HasColumnName("CodNBM");
            this.Property(t => t.CodTEC).HasColumnName("CodTEC");
            this.Property(t => t.PerIPI).HasColumnName("PerIPI");
            this.Property(t => t.Formatacao).HasColumnName("Formatacao");
            this.Property(t => t.FlgMecElet).HasColumnName("FlgMecElet");
            this.Property(t => t.Usuario).HasColumnName("Usuario");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
            this.Property(t => t.CodEmbalagem).HasColumnName("CodEmbalagem");
            this.Property(t => t.PartNumberTos).HasColumnName("PartNumberTos");
            this.Property(t => t.FlgEmbPadrao).HasColumnName("FlgEmbPadrao");
            this.Property(t => t.CodTECSeq).HasColumnName("CodTECSeq");
            this.Property(t => t.FlgMSD).HasColumnName("FlgMSD");
            this.Property(t => t.NumNivelMSD).HasColumnName("NumNivelMSD");
            this.Property(t => t.FlgVencto).HasColumnName("FlgVencto");
            this.Property(t => t.FlgAtivoItem).HasColumnName("FlgAtivoItem");
            this.Property(t => t.CodTipoReceita).HasColumnName("CodTipoReceita");
            this.Property(t => t.CodPisCofinsReceita).HasColumnName("CodPisCofinsReceita");
            this.Property(t => t.DesResumida).HasColumnName("DesResumida");
            this.Property(t => t.FlgComercializado).HasColumnName("FlgComercializado");
            this.Property(t => t.FlgItemVirtual).HasColumnName("FlgItemVirtual");
            this.Property(t => t.CodClasPPB).HasColumnName("CodClasPPB");
            this.Property(t => t.PesoLiqNac).HasColumnName("PesoLiqNac");
        }
    }
}
