namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolpa_param
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_consolpa_param()
        {
            dw_consol_parammed = new HashSet<dw_consol_parammed>();
        }

        [Key]
        public long id_consolpa_param { get; set; }

        public int? vl_minimo { get; set; }

        public decimal? vl_maximo { get; set; }

        public decimal? vl_somado { get; set; }

        public long? id_consolpa { get; set; }

        public long? id_ft_param { get; set; }

        public decimal? qt_medicoes { get; set; }

        public decimal? vl_medio { get; set; }

        public decimal? vl_monetario { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consol_parammed> dw_consol_parammed { get; set; }
    }
}
