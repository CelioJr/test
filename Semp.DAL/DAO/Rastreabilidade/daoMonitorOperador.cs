﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.Models;
using SEMP.Model.DTO;

namespace SEMP.DAL.DAO.Rastreabilidade
{
	public class daoMonitorOperador
	{

		public static List<MonitorOperadorDTO> ListaQtdLiberada(DateTime dataIni, DateTime dataFim, int linha, int posto)
		{

		   using (var dtbCB = new DTB_CBContext())            
		   using (var dtbSim = new DTB_SIMContext())
			{
				var lista = (from a in dtbCB.tbl_CBPassagem
					join b in dtbCB.tbl_CBPosto on a.CodLinha equals b.CodLinha
					join c in dtbCB.tbl_CBLinha on a.CodLinha equals c.CodLinha
				   
					 where
						a.CodLinha == linha 
						&& a.DatEvento >= dataIni 
						&& a.DatEvento <= dataFim 
						&& b.NumPosto == a.NumPosto
						group a by new {
									a.CodDRT,
									c.DesLinha,
									b.DesPosto,
									a.NumPosto,
									a.CodLinha 
								} into g
								select new
								{
									g.Key.CodDRT,                                                                        
									g.Key.DesLinha,
									g.Key.DesPosto,
									g.Key.NumPosto,
									g.Key.CodLinha,
									qtd = g.Count()
								}).OrderBy(o => o.qtd).ToList();

				var	listafunc = dtbSim.tbl_RIFuncionario.Select(x => new {x.NomFuncionario, x.CodDRT}).ToList();
				
				
				var listaOperador = new List<MonitorOperadorDTO>();

				lista.ForEach(ab =>
				{
					var tblRiFuncionario = listafunc.FirstOrDefault(y => y.CodDRT == ab.CodDRT);
				  
					listaOperador.Add(new MonitorOperadorDTO
					{
						DRT = int.Parse(ab.CodDRT),
						Nome = tblRiFuncionario != null ? tblRiFuncionario.NomFuncionario: "",
						DesLinha = ab.DesLinha,
						DesPosto = ab.DesPosto,
						NumPosto = ab.NumPosto,			           
						QtdLiberada = ab.qtd
					});
				});

				return listaOperador.OrderBy(x => x.Nome).ToList();
			}
			
		}

	}
}