namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ALC_REPAIR_BACK
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_REPAIR { get; set; }

        [StringLength(255)]
        public string COMPONENT { get; set; }

        public DateTime? DT_ENTRY { get; set; }

        public DateTime? DT_REPAIR { get; set; }

        [StringLength(255)]
        public string OBSERVATION { get; set; }

        [StringLength(10)]
        public string STEP { get; set; }

        public int? ID_ACTION { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        public string ID_DEFECT { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int COD_LINE { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_POST { get; set; }

        [StringLength(255)]
        public string ID_REASON { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_SERIAL { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int COD_SKU { get; set; }

        public int? TECHNICAL { get; set; }

        [StringLength(5)]
        public string STATUS_ENG { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? DURATION_TIME { get; set; }

        public int? ENTRY_USER { get; set; }

        public int? ID_ORIGIN { get; set; }
    }
}
