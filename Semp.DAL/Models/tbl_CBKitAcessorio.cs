namespace SEMP.DAL.Models
{
    public partial class tbl_CBKitAcessorio
    {
        public int CodKit { get; set; }
        public System.DateTime DatImpressao { get; set; }
        public int CodOperador { get; set; }
        public int CodOF { get; set; }
        public string NumSerie { get; set; }
        public string SerialAdaptador { get; set; }
        public string NumLicencaNero { get; set; }
    }
}
