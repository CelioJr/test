using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_PlanoItemMap : EntityTypeConfiguration<tbl_PlanoItem>
    {
        public tbl_PlanoItemMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodModelo, t.CodItem, t.NumPosicao, t.DatInicio, t.rowguid });

            // Properties
            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumPosicao)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.CodProcesso)
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.CodPreForma)
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.NumNAEnt)
                .IsFixedLength()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.NumNASai)
                .IsFixedLength()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.Quadrante)
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.FlgAtivo)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.CodBreakdown)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NomUsuario)
                .IsFixedLength()
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.CodBlocoCirc)
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_PlanoItem");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.NumPosicao).HasColumnName("NumPosicao");
            this.Property(t => t.QtdItem).HasColumnName("QtdItem");
            this.Property(t => t.CodProcesso).HasColumnName("CodProcesso");
            this.Property(t => t.CodPreForma).HasColumnName("CodPreForma");
            this.Property(t => t.DatInicio).HasColumnName("DatInicio");
            this.Property(t => t.DatFim).HasColumnName("DatFim");
            this.Property(t => t.NumNAEnt).HasColumnName("NumNAEnt");
            this.Property(t => t.NumNASai).HasColumnName("NumNASai");
            this.Property(t => t.Quadrante).HasColumnName("Quadrante");
            this.Property(t => t.FlgAtivo).HasColumnName("FlgAtivo");
            this.Property(t => t.CodBreakdown).HasColumnName("CodBreakdown");
            this.Property(t => t.NomUsuario).HasColumnName("NomUsuario");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
            this.Property(t => t.CodBlocoCirc).HasColumnName("CodBlocoCirc");
        }
    }
}
