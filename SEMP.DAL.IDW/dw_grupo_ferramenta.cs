namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_grupo_ferramenta
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_grupo_ferramenta()
        {
            dw_rap_grupo = new HashSet<dw_rap_grupo>();
        }

        [Key]
        public long id_grupo_ferramenta { get; set; }

        [StringLength(60)]
        public string cd_grupo_ferramenta { get; set; }

        [StringLength(100)]
        public string ds_grupo_ferramenta { get; set; }

        public int? revisao { get; set; }

        [Column(TypeName = "date")]
        public DateTime? dt_revisao { get; set; }

        public short? st_ativo { get; set; }

        [Column(TypeName = "date")]
        public DateTime? dt_stativo { get; set; }

        public long id_usrrevisao { get; set; }

        public long id_usrstativo { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rap_grupo> dw_rap_grupo { get; set; }
    }
}
