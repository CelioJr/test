﻿using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.DAL.Models;

using SEMP.Model.DTO;
using SEMP.Model.VO.CNQ;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEMP.DAL.DAO.CNQ
{
    public class daoQualidade
    {

        //static daoQualidade()
        //{
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<tbl_CBDefeito, CBDefeitoDTO>();

        //    });
        //}


        public static DefeitosGeralIAC GetIndices(string data, string fase, string familia, string tipo,bool geral = false)
        {
            using (DTB_CBContext contexto = new DTB_CBContext())
            {
                DefeitosGeralIAC obj = new DefeitosGeralIAC();

                var dataExplode = data.Split('|');

                DateTime dataInicial = DateTime.Parse(dataExplode[0]);
                DateTime dataFinal = DateTime.Parse(dataExplode[1]);

                var query = new List<tbl_CBDefeito>();

                if (!geral)
                {
                    query = (from a in contexto.tbl_CBDefeito
                                 join b in contexto.tbl_CBLinha on a.CodLinha equals b.CodLinha
                                 where b.Setor == fase && b.Familia == familia && a.FlgRevisado == 1 && a.FlgTipo == tipo
                                     && a.DatEvento >= dataInicial && a.DatEvento <= dataFinal
                                 select a).ToList();
                }
                else
                {
                    query = (from a in contexto.tbl_CBDefeito
                                 join b in contexto.tbl_CBLinha on a.CodLinha equals b.CodLinha
                                 where b.Setor == fase && b.Familia == familia && a.FlgRevisado == 1 && a.FlgTipo != null && a.FlgTipo != ""
                                     && a.DatEvento >= dataInicial && a.DatEvento <= dataFinal
                                 select a).ToList();
                }

                List<ResumoProducao> produzido = new List<ResumoProducao>();

                if (fase == "FEC")
                {
                    produzido = daoResumoProducao.ObterResumoMF(dataInicial, dataFinal).Where(a => a.Familia.Contains(familia)).ToList();
                }
                else if (fase == "IMC")
                {
                    produzido = daoResumoProducao.ObterResumoIMC(dataInicial, dataFinal).Where(a => a.DesLinha != null).ToList().Where(a => a.DesLinha.Contains(familia)).ToList();
                }
                else if (fase == "IAC")
                {
                    produzido = daoResumoProducao.ObterResumoIAC(dataInicial, dataFinal).Where(a => a.Familia.Contains(familia)).ToList();
                }
                
                int quantDefeito = query.Count;
                int quantProduzida = produzido.Sum(a => a.QtdRealizado);
                double calc = quantProduzida > 0 ? (double)quantDefeito / (double)quantProduzida : 0;

                obj.QuantidadeDefeito = quantDefeito;
                obj.QuantidadeProduzida = quantProduzida;
                obj.Calculo = calc * 100;
                obj.Fase = fase;
                obj.Familia = familia;
                obj.Data = dataInicial.ToString();

                return obj;
            }
        }

        public static DefeitosGeralIAC GetDefeitosPorFase(string data, string fase, string familia)
        {
            using (DTB_CBContext contexto = new DTB_CBContext())
            {
                var dataExplode = data.Split('|');

                DateTime dataInicial = DateTime.Parse(dataExplode[0]);
                DateTime dataFinal = DateTime.Parse(dataExplode[1]);

                DefeitosGeralIAC obj = new DefeitosGeralIAC();

                var query = (from a in contexto.tbl_CBDefeito
                             join b in contexto.tbl_CBLinha on a.CodLinha equals b.CodLinha
                             where b.Setor == fase && b.Familia == familia && a.FlgRevisado == 1 && a.CodOrigem == "12"
                                 && a.DatEvento >= dataInicial && a.DatEvento <= dataFinal
                             select a).ToList();

                List<ResumoProducao> produzido = new List<ResumoProducao>();

                if (fase == "FEC")
                {
                    produzido = daoResumoProducao.ObterResumoMF(dataInicial, dataFinal).Where(a => a.Familia.Contains(familia)).ToList(); 
                }
                else if(fase == "IMC")
	            {
                    produzido = daoResumoProducao.ObterResumoIMC(dataInicial, dataFinal).Where(a => a.DesLinha != null).ToList().Where(a => a.DesLinha.Contains(familia)).ToList();
	            }
                else if (fase == "IAC")
                {
                    produzido = daoResumoProducao.ObterResumoIAC(dataInicial, dataFinal).Where(a => a.Familia.Contains(familia)).ToList();
                }

                int quantDefeito = query.Count;
                int quantProduzida = produzido.Sum(a => a.QtdRealizado);
                double calc = quantProduzida > 0 ? (double)quantDefeito / (double)quantProduzida : 0;

                obj.QuantidadeDefeito = quantDefeito;
                obj.QuantidadeProduzida = quantProduzida;
                obj.Calculo = calc * 100;
                obj.Fase = fase;
                obj.Familia = familia;
                obj.Data = dataInicial.ToString();

                return obj;
            }
        }

        public static List<MaioresDefeitos> GetMaioresDefeitos(DateTime dataInicial, DateTime dataFinal, string fase, string familia)
        {
            using (DTB_CBContext contexto = new DTB_CBContext())
            {
                List<MaioresDefeitos> listaMaiores = (
                    from a in contexto.tbl_CBDefeito
                    join b in contexto.tbl_CBLinha on a.CodLinha equals b.CodLinha
                    join c in contexto.tbl_CBCadCausa on a.CodCausa equals c.CodCausa
                    where b.Setor == fase && b.Familia == familia && a.FlgRevisado == 1 && a.CodOrigem == "12"
                        && a.DatEvento >= dataInicial && a.DatEvento <= dataFinal && a.Posicao != null && a.Posicao != ""
                    select new MaioresDefeitos { 
                        CodCausa = c.CodCausa,
                        DesCausa = c.DesCausa,
                        Posicao = a.Posicao,
                        Familia = b.Familia,
                        Setor = b.Setor,
                        DatEvento = a.DatEvento
                    }).ToList();

                return listaMaiores;
            }
        }

        public static DefeitosGeralIAC GetDefeitoGeralIAC(string data)
        {
            using (DTB_CBContext contexto = new DTB_CBContext())
            {
                var dataExplode = data.Split('|');

                DateTime dataInicial = DateTime.Parse(dataExplode[0]);
                DateTime dataFinal = DateTime.Parse(dataExplode[1]);

                DefeitosGeralIAC obj = new DefeitosGeralIAC();

                var query = (from a in contexto.tbl_CBDefeito
                             join b in contexto.tbl_CBLinha on a.CodLinha equals b.CodLinha
                             join c in contexto.tbl_CBPosto  on b.CodLinha equals c.CodLinha
                             where 
                                b.Setor == "IAC" && 
                                a.FlgRevisado == 1 && 
                                //a.CodOrigem == "12" && 
                                (c.CodTipoPosto == 16 || c.CodTipoPosto == 28) &&
                                a.DatEvento >= dataInicial && 
                                a.DatEvento <= dataFinal
                             select a.NumECB).Count();

                //List<ResumoProducao> produzido = new List<ResumoProducao>();
                //produzido = daoResumoProducao.ObterResumoIAC(dataInicial, dataFinal).ToList();
                //List<SEMP.Model.DTO.v_producaoturnoDTO> producaoIDW = SEMP.DAL.DAO.Rastreabilidade.daoIDW.ObterProducaoIDW(dataInicial, dataFinal);

                int produzido = GetQuantidadeProduzida(dataInicial, dataFinal, "IAC");
                
                int quantDefeito = query;
                int quantProduzida = produzido;
                double calc = quantProduzida > 0 ? (double)quantDefeito / (double)quantProduzida : 0;

                obj.QuantidadeDefeito = quantDefeito;
                obj.QuantidadeProduzida = quantProduzida;
                obj.Calculo = calc * 100;
                obj.Fase = "IAC";
                obj.Data = dataInicial.ToString();

                return obj;
            }
        }

        public static int GetQuantidadeProduzida(DateTime dataInicial, DateTime dataFinal, string fase)
        {
            using (DTB_CBContext contexto = new DTB_CBContext())
            {
                
                int query = (from a in contexto.tbl_CBPassagem
                             join b in contexto.tbl_CBLinha on a.CodLinha equals b.CodLinha
                             where b.Setor == fase
                                 && a.DatEvento >= dataInicial && a.DatEvento <= dataFinal
                             select a.NumECB).Distinct().Count();

                return query;
            }
        }

        public static int GetQuantidadeProduzidaIAC(DateTime dataInicial, DateTime dataFinal, string tipo)
        {
            using (DTB_CBContext contexto = new DTB_CBContext())
            {
                int codTipoPosto = 0;

                if (tipo == "ICT")
                {
                    codTipoPosto = 28;
                }
                else if (tipo == "INSPECAO")
                {
                    codTipoPosto = 16;
                }
                else
                {
                    codTipoPosto = 16;
                }

                int query = (from a in contexto.tbl_CBPassagem
                             join b in contexto.tbl_CBLinha on a.CodLinha equals b.CodLinha
                             join c in contexto.tbl_CBPosto on b.CodLinha equals c.CodLinha
                             where b.Setor == "IAC" && 
                                   a.DatEvento >= dataInicial && 
                                   a.DatEvento <= dataFinal &&
                                   c.CodTipoPosto == codTipoPosto
                             select a.NumECB).Distinct().Count();

                return query;
            }
        }

        public static int GetQuantidadeDefeitoIAC(DateTime dataInicial, DateTime dataFinal, string tipo)
        {
            using (DTB_CBContext contexto = new DTB_CBContext())
            {
                int codTipoPosto = 0;

                if (tipo == "ICT")
                {
                    codTipoPosto = 28;
                }
                else if (tipo == "INSPECAO")
                {
                    codTipoPosto = 16;
                }
                else
                {
                    codTipoPosto = 16;
                }

                int query = (from a in contexto.tbl_CBDefeito
                             join b in contexto.tbl_CBLinha on a.CodLinha equals b.CodLinha
                             join c in contexto.tbl_CBPosto on b.CodLinha equals c.CodLinha
                             where b.Setor == "IAC" &&
                                   a.DatEvento >= dataInicial &&
                                   a.DatEvento <= dataFinal &&
                                   c.CodTipoPosto == codTipoPosto
                             select a.NumECB).Count();

                return query;
            }
        }

        public static int GetTotalProduzido(DateTime dataInicial, DateTime dataFinal, string fase, string familia)
        {
            List<ResumoProducao> produzido = new List<ResumoProducao>();

            if (fase == "FEC")
            {
                produzido = daoResumoProducao.ObterResumoMF(dataInicial, dataFinal).Where(a => a.Familia.Contains(familia)).ToList();
            }
            else if (fase == "IMC")
            {
                produzido = daoResumoProducao.ObterResumoIMC(dataInicial, dataFinal).Where(a => a.DesLinha != null).ToList().Where(a => a.DesLinha.Contains(familia)).ToList();
            }
            else if (fase == "IAC")
            {
                produzido = daoResumoProducao.ObterResumoIAC(dataInicial, dataFinal).Where(a => a.Familia.Contains(familia)).ToList();
            }

            int quantProduzida = produzido.Sum(a => a.QtdRealizado);

            return quantProduzida;
        }

        public static List<CBDefeitoDTO> GetDefeitosDTO(DateTime dataInicial, DateTime dataFinal, string fase)
        {
            using (DTB_CBContext contexto = new DTB_CBContext())
            {
                var query = (from a in contexto.tbl_CBDefeito
                             join b in contexto.tbl_CBLinha on a.CodLinha equals b.CodLinha
                             where b.Setor == fase && a.FlgRevisado == 1 && a.CodOrigem == "12"
                                 && a.DatEvento >= dataInicial && a.DatEvento <= dataFinal
                             select a).ToList();

                List<CBDefeitoDTO> listaDTO = new List<CBDefeitoDTO>();

                AutoMapper.Mapper.Map(query, listaDTO);

                return listaDTO;
            }
        }

        public static List<MaioresDefeitos> GetMaioresDefeitosIACINSPECAO(DateTime dataInicial, DateTime dataFinal, string tipo)
        {
            using (DTB_CBContext contexto = new DTB_CBContext())
            {
                int codTipoPosto = 0;

                if (tipo == "ICT")
                {
                    codTipoPosto = 28;
                }
                else if (tipo == "INSPECAO")
                {
                    codTipoPosto = 16;  
                }
                else
                {
                    codTipoPosto = 16;
                }

                List<MaioresDefeitos> listaMaiores = (from a in contexto.tbl_CBDefeito
                             join b in contexto.tbl_CBLinha on a.CodLinha equals b.CodLinha
                             join c in contexto.tbl_CBPosto on b.CodLinha equals c.CodLinha
                             join d in contexto.tbl_CBCadCausa on a.CodCausa equals d.CodCausa
                             where b.Setor == "IAC" &&
                                   a.FlgRevisado == 1 &&
                                   a.DatEvento >= dataInicial &&
                                   a.DatEvento <= dataFinal &&
                                   c.CodTipoPosto == codTipoPosto && 
                                   a.Posicao != null &&
                                   a.Posicao != ""
                             select new MaioresDefeitos
                             {
                                 CodCausa = d.CodCausa,
                                 DesCausa = d.DesCausa,
                                 Posicao = a.Posicao,
                                 Familia = b.Familia,
                                 Setor = b.Setor,
                                 DatEvento = a.DatEvento
                             }).ToList();

                return listaMaiores;
            }
        }

        public static int GetQuantidadeProduzidaIACPorLinha(DateTime dataInicial, DateTime dataFinal, int CodLinha)
        {
            using (DTB_CBContext contexto = new DTB_CBContext())
            {
                
                int query = (from a in contexto.tbl_CBPassagem
                             join b in contexto.tbl_CBLinha on a.CodLinha equals b.CodLinha
                             where b.Setor == "IAC" &&
                                   a.DatEvento >= dataInicial &&
                                   a.DatEvento <= dataFinal &&
                                   b.CodLinha == CodLinha
                             select a.NumECB).Distinct().Count();

                return query;
            }
        }

        public static decimal GetQuantidadeProduzidaPorLinhaEHora(
            DateTime DataInicio,
            DateTime DataFim,
            TimeSpan HoraInicio,
            TimeSpan HoraFim,
            int Turno,
            int CodLinha)
        {

            CBHorarioDTO turnoHorario = daoIndicadores.GetTurno(Turno);
            string stDataInicio = DataInicio.ToShortDateString() + " " + HoraInicio.ToString();
            string stDataFim = DataFim.ToShortDateString() + " " + HoraFim.ToString();

            if (TimeSpan.Parse(turnoHorario.HoraInicio) > TimeSpan.Parse(turnoHorario.HoraFim) && HoraInicio.Hours >= 0 && HoraFim.Hours <= TimeSpan.Parse(turnoHorario.HoraFim).Hours)
            {
                stDataInicio = DataInicio.AddDays(1).ToShortDateString() + " " + HoraInicio.ToString();
                stDataFim = DataFim.AddDays(1).ToShortDateString() + " " + HoraFim.ToString();
            }

            DateTime DataHoraInicio = DateTime.Parse(stDataInicio);
            DateTime DataHoraFim = DateTime.Parse(stDataFim);
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var query = dtbCb.viw_CBPassagemTurno.Where(a => 
                    a.Turno == Turno &&
                    a.CodLinha == CodLinha &&
                    a.DatReferencia >= DataInicio &&
                    a.DatReferencia <= DataFim &&
                    a.DatEvento >= DataHoraInicio && a.DatEvento <= DataHoraFim
                    );

                var resultado = query.ToList();

                return resultado.Count();
            }
        }

        public static int GetQuantidadeDefeitoIACPorLinha(DateTime dataInicial, DateTime dataFinal, int CodLinha)
        {
            using (DTB_CBContext contexto = new DTB_CBContext())
            {
                int query = (from a in contexto.tbl_CBDefeito
                             join b in contexto.tbl_CBLinha on a.CodLinha equals b.CodLinha
                             where b.Setor == "IAC" &&
                                   a.DatEvento >= dataInicial &&
                                   a.DatEvento <= dataFinal &&
                                   b.CodLinha == CodLinha
                             select a.NumECB).Count();

                return query;
            }
        }

		public static int GetQuantidadeDefeitoIACPorLinha(DateTime dataInicial, DateTime dataFinal, int CodLinha, string CodModelo)
		{
			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				int query = (from a in contexto.tbl_CBDefeito
							 join b in contexto.tbl_CBLinha on a.CodLinha equals b.CodLinha
							 where b.Setor == "IAC" &&
								   a.DatEvento >= dataInicial &&
								   a.DatEvento <= dataFinal &&
								   a.NumECB.Substring(0, 6) == CodModelo &&
								   b.CodLinha == CodLinha &&
								   a.FlgRevisado < 2
							 select a.NumECB).Count();

				return query;
			}
		}

		public static int GetQuantidadeDefeitoIACPorLinhaReal(DateTime dataInicial, DateTime dataFinal, int CodLinha, string CodModelo)
		{
			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				var query = (from a in contexto.tbl_CBDefeito
							 join b in contexto.tbl_CBLinha on a.CodLinha equals b.CodLinha
							 where b.Setor == "IAC" &&
								   a.DatEvento >= dataInicial &&
								   a.DatEvento <= dataFinal &&
								   a.NumECB.Substring(0, 6) == CodModelo &&
								   b.CodLinha == CodLinha &&
								   a.FlgRevisado < 2 &&
								   !(a.CodCausa.StartsWith("WX") || a.DesAcao == "2")
							 select a.NumECB);

				return query.Count();
			}
		}

		public static int GetQuantidadeDefeitoIACPorLinhaFalsaFalha(DateTime dataInicial, DateTime dataFinal, int CodLinha, string CodModelo)
		{
			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				int query = (from a in contexto.tbl_CBDefeito
							 join b in contexto.tbl_CBLinha on a.CodLinha equals b.CodLinha
							 where b.Setor == "IAC" &&
								   a.DatEvento >= dataInicial &&
								   a.DatEvento <= dataFinal &&
								   a.NumECB.Substring(0, 6) == CodModelo &&
								   b.CodLinha == CodLinha &&
								   a.FlgRevisado < 2 &&
								   (a.CodCausa.StartsWith("WX") || a.DesAcao == "2")
							 select a.NumECB).Count();

				return query;
			}
		}

		public static List<MaioresDefeitos> GetMaioresDefeitosIACPorLinha(DateTime dataInicial, DateTime dataFinal, int CodLinha)
        {
            using (DTB_CBContext contexto = new DTB_CBContext())
            {
                List<MaioresDefeitos> listaMaiores = (from a in contexto.tbl_CBDefeito
                                                      join b in contexto.tbl_CBLinha on a.CodLinha equals b.CodLinha
                                                      join d in contexto.tbl_CBCadCausa on a.CodCausa equals d.CodCausa
                                                      //join e in contexto.tbl_CBCadDefeito on a.CodDefeito equals e.CodDefeito
                                                      where b.Setor == "IAC" &&
                                                            a.FlgRevisado == 1 &&
                                                            a.DatEvento >= dataInicial &&
                                                            a.DatEvento <= dataFinal &&
                                                            b.CodLinha == CodLinha &&
                                                            a.Posicao != null &&
                                                            a.Posicao != ""
                                                      select new MaioresDefeitos
                                                      {
                                                          CodDefeito = a.CodDefeito,
                                                          CodCausa = d.CodCausa,
                                                          DesCausa = d.DesCausa,
                                                          Posicao = a.Posicao,
                                                          Familia = b.Familia,
                                                          Setor = b.Setor,
                                                          DatEvento = a.DatEvento,
                                                          CodLinha = CodLinha,
                                                          NumPosto = a.NumPosto//,DesDefeito = e.DesDefeito
                                                      }).ToList();

                return listaMaiores;
            }
        }

        public static int GetQuantidadeDePontos(DateTime dataInicial, DateTime dataFinal, int CodLinha)
        {
            using (DTB_CBContext contexto = new DTB_CBContext())
            {

                var query = (from a in contexto.tbl_CBPassagem
                             join b in contexto.tbl_CBLinha on a.CodLinha equals b.CodLinha
                             where b.Setor == "IAC" &&
                                   a.DatEvento >= dataInicial &&
                                   a.DatEvento <= dataFinal &&
                                   b.CodLinha == CodLinha
                             select new {
                                 Modelo = a.NumECB.Substring(0, 6)
                             }).Distinct().ToList();

                int quantidade = 0;

                foreach (var item in query)
                {
                    var modelo = daoTecnico.ObterCodModeloIAC(item.Modelo);
                    quantidade += daoResumoIAC.ObterQuantidadePontos(modelo);
                }

                return quantidade;
            }
        }

        public static int GetQuantidadeDePontosPorLinhaEHora(DateTime DataInicio,
            DateTime DataFim,
            TimeSpan HoraInicio,
            TimeSpan HoraFim,
            int Turno,
            int CodLinha)
        {
            using (DTB_CBContext contexto = new DTB_CBContext())
            {

                CBHorarioDTO turnoHorario = daoIndicadores.GetTurno(Turno);
                string stDataInicio = DataInicio.ToShortDateString() + " " + HoraInicio.ToString();
                string stDataFim = DataFim.ToShortDateString() + " " + HoraFim.ToString();

                if (TimeSpan.Parse(turnoHorario.HoraInicio) > TimeSpan.Parse(turnoHorario.HoraFim) && HoraInicio.Hours >= 0 && HoraFim.Hours <= TimeSpan.Parse(turnoHorario.HoraFim).Hours)
                {
                    stDataInicio = DataInicio.AddDays(1).ToShortDateString() + " " + HoraInicio.ToString();
                    stDataFim = DataFim.AddDays(1).ToShortDateString() + " " + HoraFim.ToString();
                }

                DateTime DataHoraInicio = DateTime.Parse(stDataInicio);
                DateTime DataHoraFim = DateTime.Parse(stDataFim);

                var query = contexto.viw_CBPassagemTurno.Where(a =>
                    a.Turno == Turno &&
                    a.CodLinha == CodLinha &&
                    a.DatReferencia >= DataInicio &&
                    a.DatReferencia <= DataFim &&
                    a.DatEvento >= DataHoraInicio && a.DatEvento <= DataHoraFim
                    ).Select(s => new { Modelo = s.NumECB.Substring(0, 6) }).Distinct().ToList();

                int quantidade = 0;

                foreach (var item in query)
                {
                    var modelo = daoTecnico.ObterCodModeloIAC(item.Modelo);
                    quantidade += daoResumoIAC.ObterQuantidadePontos(modelo);
                }

                return quantidade;
            }
        }

        public static int GetQuantidadeDePontosGeral(DateTime dataInicial, DateTime dataFinal, string tipo)
        {
            using (DTB_CBContext contexto = new DTB_CBContext())
            {

                int codTipoPosto = 0;

                if (tipo == "ICT")
                {
                    codTipoPosto = 28;
                }
                else if (tipo == "INSPECAO")
                {
                    codTipoPosto = 16;
                }
                else
                {
                    codTipoPosto = 16;
                }

                var query = (from a in contexto.tbl_CBPassagem
                             join b in contexto.tbl_CBLinha on a.CodLinha equals b.CodLinha
                             join c in contexto.tbl_CBPosto on a.CodLinha equals c.CodLinha
                             where b.Setor == "IAC" &&
                                   a.DatEvento >= dataInicial &&
                                   a.DatEvento <= dataFinal &&
                                   c.CodTipoPosto == codTipoPosto
                             select new
                             {
                                 Modelo = a.NumECB.Substring(0, 6)
                             }).Distinct().ToList();

                int quantidade = 0;

                foreach (var item in query)
                {
                    var modelo = daoTecnico.ObterCodModeloIAC(item.Modelo);
                    var quantidadePontos = daoResumoIAC.ObterQuantidadePontos(modelo);
                    quantidade += quantidadePontos;
                }

                return quantidade;
            }
        }

        public static List<RelatorioInspecaoIACModeloLinha> GetModelosLinhasIAC(DateTime dataInicial, DateTime dataFinal, int CodLinha)
        {
            using (DTB_CBContext contexto = new DTB_CBContext())
            {
                List<RelatorioInspecaoIACModeloLinha> lista = new List<RelatorioInspecaoIACModeloLinha>();
                var query = (from a in contexto.tbl_CBPassagem
                             join b in contexto.tbl_CBLinha on a.CodLinha equals b.CodLinha
                             where b.Setor == "IAC" &&
                                   a.DatEvento >= dataInicial &&
                                   a.DatEvento <= dataFinal &&
                                   b.CodLinha == CodLinha
                             select new
                             {
                                 Modelo = a.NumECB.Substring(0, 6)
                             });
                
                var listagem = query.Distinct().ToList();

                int quantidadePontos = 0;

                foreach (var item in listagem)
                {
                    var modelo = daoTecnico.ObterCodModeloIAC(item.Modelo);
                    quantidadePontos = daoResumoIAC.ObterQuantidadePontos(modelo);
                    var obj = new RelatorioInspecaoIACModeloLinha();
                    obj.Modelo = item.Modelo;
                    obj.NE = modelo;
                    obj.DescModelo = daoGeral.ObterDescricaoItem(item.Modelo);
                    obj.DescNE = daoGeral.ObterDescricaoItem(modelo);
                    obj.QuantidadePonto = quantidadePontos;
                    obj.QuantidadeInspecionada = query.Where(a => a.Modelo == item.Modelo).Count();
                    obj.QuantidadeDefeito = (from a in contexto.tbl_CBDefeito
                                             join b in contexto.tbl_CBLinha on a.CodLinha equals b.CodLinha
                                             where b.Setor == "IAC" &&
                                                   a.DatEvento >= dataInicial &&
                                                   a.DatEvento <= dataFinal &&
                                                   b.CodLinha == CodLinha &&
                                                   a.NumECB.Substring(0, 6) == item.Modelo
                                             select new
                                             {
                                                 Modelo = a.NumECB
                                             }).Count();

                    var calculo = obj.QuantidadeDefeito > 0 && quantidadePontos > 0 ? (double)obj.QuantidadeDefeito / (double)(obj.QuantidadeInspecionada * quantidadePontos) : 0;

                    var ppm = Math.Round((double)calculo * 1000000, 1);

                    obj.PPM = (decimal)ppm;

                    lista.Add(obj);
                }

                return lista;
            }
        }

    }
}
