namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_TRADUCAO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FR_TRADUCAO()
        {
            FR_TRADUCAO_IDIOMA = new HashSet<FR_TRADUCAO_IDIOMA>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TRA_CODIGO { get; set; }

        [StringLength(300)]
        public string TRA_ITEM { get; set; }

        [Required]
        [StringLength(2000)]
        public string TRA_TEXTO { get; set; }

        [Required]
        [StringLength(1)]
        public string TRA_TIPO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_TRADUCAO_IDIOMA> FR_TRADUCAO_IDIOMA { get; set; }
    }
}
