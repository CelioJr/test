﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.DTO
{
    public class MonitorOperadorDTO
    {
        public int DRT { get; set; }
        public String Nome { get; set; }
        public int tipoPosto { get; set; }
        public String DesPosto { get; set; }
        public int NumPosto { get; set; }
        public String DesLinha{ get; set; }
        public int QtdLiberada { get; set; }
    }
}
