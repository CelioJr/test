using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.DTB_SSA.Models.Mapping
{
    public class tbl_KitAcessorioMap : EntityTypeConfiguration<tbl_KitAcessorio>
    {
        public tbl_KitAcessorioMap()
        {
            // Primary Key
            this.HasKey(t => t.CodKit);

            // Properties
            this.Property(t => t.NumSerie)
                .HasMaxLength(10);

            this.Property(t => t.SerialAdaptador)
                .HasMaxLength(30);

            this.Property(t => t.NumLicencaNero)
                .HasMaxLength(39);

            // Table & Column Mappings
            this.ToTable("tbl_KitAcessorio");
            this.Property(t => t.CodKit).HasColumnName("CodKit");
            this.Property(t => t.DatImpressao).HasColumnName("DatImpressao");
            this.Property(t => t.CodOperador).HasColumnName("CodOperador");
            this.Property(t => t.CodOF).HasColumnName("CodOF");
            this.Property(t => t.NumSerie).HasColumnName("NumSerie");
            this.Property(t => t.SerialAdaptador).HasColumnName("SerialAdaptador");
            this.Property(t => t.NumLicencaNero).HasColumnName("NumLicencaNero");

        }
    }
}
