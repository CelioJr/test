using System;

namespace SEMP.Model.DTO
{
    public class ProgDiaJitColetaDTO
    {
        public string CodFab { get; set; }
        public string CodModelo { get; set; }
        public System.DateTime DatProducao { get; set; }
        public string CodLinha { get; set; }
        public string FlagTP { get; set; }
        public Nullable<decimal> QtdProducao { get; set; }
        public Nullable<decimal> QtdProdPm { get; set; }
        public string Usuario { get; set; }
        public System.Guid rowguid { get; set; }
        public string CodConjCin { get; set; }
        public string CodConjFly { get; set; }
        public Nullable<System.DateTime> DatEstudo { get; set; }
        public string FlgCobertura { get; set; }
        public string Observacao { get; set; }
    }
}
