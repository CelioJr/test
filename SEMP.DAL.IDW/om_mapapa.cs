namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_mapapa
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_mapapa()
        {
            om_alimrea = new HashSet<om_alimrea>();
            om_papro = new HashSet<om_papro>();
        }

        [Key]
        public long id_mapapa { get; set; }

        public long id_mapa { get; set; }

        public long id_produto { get; set; }

        public long id_pa { get; set; }

        public decimal? qt_usada { get; set; }

        public decimal? seg_frequenciaRealimentacao { get; set; }

        public short? is_ciclounico { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_alimrea> om_alimrea { get; set; }

        public virtual om_mapa om_mapa { get; set; }

        public virtual om_pa om_pa { get; set; }

        public virtual om_produto om_produto { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_papro> om_papro { get; set; }
    }
}
