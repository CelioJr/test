﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.SAPConnector.VO
{
	public class BAPI_ORDER_COMPONENT
	{
		[Display(Description = "Nº reserva / necessidades dependentes")]
		public int RESERVATION_NUMBER { get; set; }
		[Display(Description = "Nº item da reserva / das necessidades dependentes")]
		public int RESERVATION_ITEM { get; set; }
		[Display(Description = "Tipo de registro")]
		public String RESERVATION_TYPE { get; set; }
		[Display(Description = "Item foi eliminado")]
		public String DELETION_INDICATOR { get; set; }
		[Display(Description = "Número do material (18 caracteres)")]
		public String MATERIAL { get; set; }
		[Display(Description = "Centro")]
		public String PROD_PLANT { get; set; }
		[Display(Description = "Depósito")]
		public String STORAGE_LOCATION { get; set; }
		[Display(Description = "Área de abastecimento da produção")]
		public String SUPPLY_AREA { get; set; }
		[Display(Description = "Número do lote")]
		public String BATCH { get; set; }
		[Display(Description = "Código de estoque especial")]
		public String SPECIAL_STOCK { get; set; }
		[Display(Description = "Data necessidade do componente")]
		public DateTime REQ_DATE { get; set; }
		[Display(Description = "Quantidade necessária")]
		public Decimal REQ_QUAN { get; set; }
		[Display(Description = "Unidade de medida básica")]
		public String BASE_UOM { get; set; }
		[Display(Description = "Cód.ISO unid.medida")]
		public String BASE_UOM_ISO { get; set; }
		[Display(Description = "Qtd.retirada")]
		public Decimal WITHDRAWN_QUANTITY { get; set; }
		[Display(Description = "Quantidade na unidade de medida do registro")]
		public Decimal ENTRY_QUANTITY { get; set; }
		[Display(Description = "Unidade de medida do registro")]
		public String ENTRY_UOM { get; set; }
		[Display(Description = "Cód.ISO unid.medida")]
		public String ENTRY_UOM_ISO { get; set; }
		[Display(Description = "Nº ordem")]
		public String ORDER_NUMBER { get; set; }
		[Display(Description = "Tipo de movimento (administração de estoques)")]
		public String MOVEMENT_TYPE { get; set; }
		[Display(Description = "Ctg.item (lista técnica)")]
		public String ITEM_CATEGORY { get; set; }
		[Display(Description = "Nº item da lista técnica")]
		public String ITEM_NUMBER { get; set; }
		[Display(Description = "Seq.")]
		public String SEQUENCE { get; set; }
		[Display(Description = "Nº operação")]
		public String OPERATION { get; set; }
		[Display(Description = "Código: baixa por explosão")]
		public String BACKFLUSH { get; set; }
		[Display(Description = "Avaliação estoque especial")]
		public String VALUATION_SPEC_STOCK { get; set; }
		[Display(Description = "Texto de status editado")]
		public String SYSTEM_STATUS { get; set; }
		[Display(Description = "Texto breve de material")]
		public String MATERIAL_DESCRIPTION { get; set; }
		[Display(Description = "Qtd.confirmada")]
		public Decimal COMMITED_QUANTITY { get; set; }
		[Display(Description = "Quantidade em falta")]
		public Decimal SHORTAGE { get; set; }
		[Display(Description = "Nº requisição de compra")]
		public String PURCHASE_REQ_NO { get; set; }
		[Display(Description = "Nº do item da requisição de compra")]
		public int PURCHASE_REQ_ITEM { get; set; }
		[Display(Description = "Nº de material longo para o campo MATERIAL")]
		public String MATERIAL_EXTERNAL { get; set; }
		[Display(Description = "GUID externo para campo MATERIAL")]
		public String MATERIAL_GUID { get; set; }
		[Display(Description = "Nº de versão para campo MATERIAL")]
		public String MATERIAL_VERSION { get; set; }
		[Display(Description = "Segmento de necessidade")]
		public String REQ_SEGMENT { get; set; }
		[Display(Description = "Segmento de estoque")]
		public String STOCK_SEGMENT { get; set; }
		[Display(Description = "Nº do material")]
		public String MATERIAL_LONG { get; set; }
	}
}
