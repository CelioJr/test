using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_PlanoProcessoMap : EntityTypeConfiguration<tbl_PlanoProcesso>
    {
        public tbl_PlanoProcessoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodProcesso, t.rowguid });

            // Properties
            this.Property(t => t.CodProcesso)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.DesProcesso)
                .IsFixedLength()
                .HasMaxLength(25)
                .IsUnicode(false);

            this.Property(t => t.CodAntigo)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.CodAP)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_PlanoProcesso");
            this.Property(t => t.CodProcesso).HasColumnName("CodProcesso");
            this.Property(t => t.DesProcesso).HasColumnName("DesProcesso");
            this.Property(t => t.FlgProcesso).HasColumnName("FlgProcesso");
            this.Property(t => t.FlgEmpenho).HasColumnName("FlgEmpenho");
            this.Property(t => t.FlgLSA).HasColumnName("FlgLSA");
            this.Property(t => t.CodAntigo).HasColumnName("CodAntigo");
            this.Property(t => t.CodAP).HasColumnName("CodAP");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
        }
    }
}
