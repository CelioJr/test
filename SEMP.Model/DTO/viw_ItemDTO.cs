using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class viw_ItemDTO
    {
        public string CodItem { get; set; }
        public string DesItem { get; set; }
		public string Origem { get; set; }
		public string CodFam { get; set; }
	}
}
