﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.CNQ
{
    public class MaioresDefeitos
    {
        public string CodCausa { get; set; }
        public string DesCausa { get; set; }
        public string Posicao { get; set; }
        public string Setor { get; set; }
        public string Familia { get; set; }
        public DateTime DatEvento { get; set; }

        //AUX
        public string Categoria { get; set; }
        public string CodDefeito { get; set; }
        public string DesDefeito { get; set; }
        public string Data { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public int QuantidadeDefeito { get; set; }
        public int QuantidadeProduzida { get; set; }
        public double Calculo { get; set; }
        public string Hora { get; set; }
        public TimeSpan HoraInicio { get; set; }
        public TimeSpan HoraFim { get; set; }
        public int CodLinha { get; set; }
        public int NumPosto { get; set; }
    }
}
