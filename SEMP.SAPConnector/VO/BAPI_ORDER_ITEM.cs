﻿using System;

namespace SEMP.SAPConnector.VO
{
	public class BAPI_ORDER_ITEM
	{
		public String ORDER_NUMBER { get; set; }
		public String ORDER_ITEM_NUMBER { get; set; }
		public String SALES_ORDER { get; set; }
		public String SALES_ORDER_ITEM { get; set; }
		public Decimal? SCRAP { get; set; }
		public Decimal? QUANTITY { get; set; }
		public Decimal? DELIVERED_QUANTITY { get; set; }
		public String BASE_UNIT { get; set; }
		public String BASE_UNIT_ISO { get; set; }
		public String MATERIAL { get; set; }
		public DateTime ACTUAL_DELIVERY_DATE { get; set; }
		public DateTime PLANNED_DELIVERY_DATE { get; set; }
		public String PLAN_PLANT { get; set; }
		public String STORAGE_LOCATION { get; set; }
		public String DELIVERY_COMPL { get; set; }
		public String PRODUCTION_VERSION { get; set; }
		public String PROD_PLANT { get; set; }
		public String ORDER_TYPE { get; set; }
		public DateTime FINISH_DATE { get; set; }
		public DateTime PRODUCTION_FINISH_DATE { get; set; }
		public String BATCH { get; set; }
		public String DELETION_FLAG { get; set; }
		public String MRP_AREA { get; set; }
		public String MATERIAL_TEXT { get; set; }
		public String MATERIAL_EXTERNAL { get; set; }
		public String MATERIAL_GUID { get; set; }
		public String MATERIAL_VERSION { get; set; }
		public String STOCK_SEGMENT { get; set; }
		public String MATERIAL_LONG { get; set; }
	}
}
