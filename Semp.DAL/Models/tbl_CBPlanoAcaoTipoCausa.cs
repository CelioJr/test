using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEMP.DAL.Models
{
	public partial class tbl_CBPlanoAcaoTipoCausa
	{
		public int CodTipoCausa { get; set; }
		public string DesTipoCausa { get; set; }
	}
}
