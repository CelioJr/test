namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_DOC_MATERIAL_REFERENCIA
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DOC_CODIGO { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(500)]
        public string REF_BIBLIOGRAFIA { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string REF_TIPO { get; set; }

        public virtual FR_DOC_PRINCIPAL FR_DOC_PRINCIPAL { get; set; }
    }
}
