namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_CQTF_SEQ
    {
        [Key]
        public int COD_CQTF { get; set; }

        public DateTime? DATA { get; set; }
    }
}
