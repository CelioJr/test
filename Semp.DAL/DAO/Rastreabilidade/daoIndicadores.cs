﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.Models;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using System.Data.Entity.Core.Objects;
using SEMP.Model.DTO;

namespace SEMP.DAL.DAO.Rastreabilidade
{
    public class daoIndicadores
    {
        //static daoIndicadores()
        //{
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<tbl_CBLinha, CBLinhaDTO>();
        //        cfg.CreateMap<tbl_CBPosto, CBPostoDTO>();
        //        cfg.CreateMap<tbl_CBHorario, CBHorarioDTO>();
        //        cfg.CreateMap<viw_CBDefeitoTurno, viw_CBDefeitoTurnoDTO>();
        //    });
        //}

        #region Indicador Global

        public static decimal ObterTotalIAC(DateTime DatInicio, DateTime DatFim)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                //var qtde = (from t in dtbCb.viw_CBPassagemTurno
                //			join l in dtbCb.tbl_CBLinha on t.CodLinha equals l.CodLinha
                //			join p in dtbCb.tbl_CBPosto on new { t.CodLinha, t.NumPosto } equals new { p.CodLinha, p.NumPosto }
                //			where t.DatReferencia >= DatInicio && t.DatReferencia <= DatFim && l.Setor == "IAC" &&
                //					((t.NumPosto == 3 && t.CodLinha == 34) || p.CodTipoPosto == 10)
                //			select new { NumECB = t.NumECB }).Count();
                var qtde = (from t in dtbCb.tbl_CBPassagem
                            join l in dtbCb.tbl_CBLinha on t.CodLinha equals l.CodLinha
                            join p in dtbCb.tbl_CBPosto on new { t.CodLinha, t.NumPosto } equals new { p.CodLinha, p.NumPosto }
                            join tp in dtbCb.tbl_CBTipoPosto on p.CodTipoPosto equals tp.CodTipoPosto
                            where t.DatEvento >= DatInicio && t.DatEvento <= DatFim && l.Setor == "IAC" &&
                                    tp.FlgEntradaSaida == "S"
                            select new { NumECB = t.NumECB }).Count();

                return qtde;
            }
        }

        public static decimal ObterTotalEmbalado(String Fabrica, DateTime DatInicio, DateTime DatFim)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var qtde = (from t in dtbCb.tbl_CBEmbalada
                            join l in dtbCb.tbl_CBLinha on t.CodLinha equals l.CodLinha
                            where t.DatReferencia >= DatInicio && t.DatReferencia <= DatFim && l.Setor == Fabrica
                            select new { NumECB = t.NumECB }).Count();

                return qtde;
            }
        }

        public static decimal ObterPlano(String Fabrica, DateTime DatInicio, DateTime DatFim)
        {
            decimal qtde = 0;

            using (DTB_SIMContext dtbSim = new DTB_SIMContext())
            {

                if (Fabrica == "FEC")
                {
                    qtde = (from t in dtbSim.tbl_ProgDiaJit
                            where t.DatProducao >= DatInicio && t.DatProducao <= DatFim
                            select t).Sum(x => x.QtdProdPm) ?? 0;
                }
                else if (Fabrica == "IMC")
                {
                    var dado = (from t in dtbSim.tbl_ProgDiaPlaca
                                where t.CodProcesso == "02-00" && t.CodLinha != "DAT" && t.CodLinha != "111.03-0" && t.CodLinha != "111.04-0" && t.CodLinha != "111.24-0"
                                        && t.DatProducao >= DatInicio && t.DatProducao <= DatFim
                                group t by t into grupo
                                select new { QtdProdPM = grupo.Sum(x => x.QtdProdPM) }).FirstOrDefault();
                    qtde = dado != null ? dado.QtdProdPM : 0;
                }
                else
                {

                    qtde = dtbSim.tbl_ProgDiaPlaca.Any(
                        t => t.CodProcesso.StartsWith("01")
                        && t.CodLinha == "SMD-09"
                        && t.DatProducao >= DatInicio
                        && t.DatProducao <= DatFim) == true ?
                    dtbSim.tbl_ProgDiaPlaca.Where(t => t.CodProcesso.StartsWith("01") && t.CodLinha == "SMD-09" && t.DatProducao >= DatInicio && t.DatProducao <= DatFim).Sum(s => s.QtdProdPM) : 0;
                }

                return qtde;
            }
        }

        public static decimal ObterPadraoIndicador(int Indicador, String Fabrica)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var lista = (from t in dtbCb.tbl_CBPadraoIndicador
                             where t.Indicador == Indicador && t.Fabrica == Fabrica
                             select t).ToList();

                decimal qtde = (lista.Count() > 0) ? lista.Sum(x => x.Valor).Value / lista.Count() : 0;

                return qtde;
            }
        }

        public static decimal ObterTotalDefeito(String Fabrica, DateTime DatInicio, DateTime DatFim)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var qtde = (from t in dtbCb.viw_CBDefeitoTurno
                            join l in dtbCb.tbl_CBLinha on t.CodLinha equals l.CodLinha
                            where t.DatReferencia >= DatInicio
                                    && t.DatReferencia <= DatFim
                                    && l.Setor == Fabrica
                                    && t.FlgRevisado != 2
                                    && String.IsNullOrEmpty(t.NumSeriePai)
                            select new { NumECB = t.NumECB }).Count();

                return qtde;
            }
        }

        #endregion

        #region Indicador por linha

        public static decimal ObterPadraoIndicador(int Indicador, String Fabrica, int CodLinha)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var dados = (from t in dtbCb.tbl_CBPadraoIndicador
                             where t.Indicador == Indicador && t.Fabrica == Fabrica && t.CodLinha == CodLinha
                             select t).FirstOrDefault();
                if (dados != null)
                {
                    var qtde = dados.Valor;

                    return qtde.Value;
                }
                else
                {
                    return 0;
                }
            }
        }

        public static decimal ObterTotalEmbalado(DateTime DatInicio, DateTime DatFim, int CodLinha)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var qtde = (from t in dtbCb.tbl_CBEmbalada
                            where t.DatReferencia >= DatInicio && t.DatReferencia <= DatFim && t.CodLinha == CodLinha
                            select new { NumECB = t.NumECB }).Count();

                return qtde;
            }
        }

        public static decimal ObterPlano(String Fabrica, DateTime DatInicio, DateTime DatFim, string CodLinha)
        {
            decimal qtde = 0;
            using (DTB_SIMContext dtbSim = new DTB_SIMContext())
            {
                if (Fabrica == "FEC")
                {
                    var result = (from t in dtbSim.tbl_ProgDiaJit
                                  where t.CodLinha == CodLinha
                                  select new { QtdProg = t.QtdProdPm, DatProducao = t.DatProducao }).ToList();

                    var result2 = (from t in result where t.DatProducao >= DatInicio && t.DatProducao <= DatFim select t);
                    qtde = result2.Sum(x => x.QtdProg).Value;
                }
                else
                {
                    String CodProcesso = (Fabrica == "IMC" ? "02" : "01");

                    var result = (from t in dtbSim.tbl_ProgDiaPlaca
                                  where t.CodProcesso.Substring(0, 2) == CodProcesso && t.CodLinha == CodLinha
                                  select new { QtdProg = t.QtdProdPM, DatProducao = t.DatProducao }).ToList();

                    var result2 = (from t in result where t.DatProducao >= DatInicio && t.DatProducao <= DatFim select t);

                    qtde = result2.Sum(x => x.QtdProg);
                }
                return qtde;
            }
        }

        public static decimal ObterTotalDefeito(DateTime DatInicio, DateTime DatFim, int CodLinha)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var qtde = (from t in dtbCb.viw_CBDefeitoTurno
                            where t.DatReferencia >= DatInicio
                                    && t.DatReferencia <= DatFim
                                    && t.CodLinha == CodLinha
                                    && t.FlgRevisado != 2
                                    && String.IsNullOrEmpty(t.NumSeriePai)
                            select new { NumECB = t.NumECB }).Count();

                return qtde;
            }
        }

        public static decimal ObterTotalDefeitoReparadosPorFabrica(DateTime DatInicio, DateTime DatFim, string Setor, string Familia)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {

                var linhas = !string.IsNullOrWhiteSpace(Familia) && Setor == "FEC" ?
                    dtbCb.tbl_CBLinha.Where(t => t.Setor == Setor && t.Familia == Familia && t.CodLinhaT1 != null).Select(s => s.CodLinha)
                        :
                    dtbCb.tbl_CBLinha.Where(t => t.Setor == Setor && t.CodLinhaT1 != null).Select(s => s.CodLinha);

                var qtde = (from t in dtbCb.viw_CBDefeitoTurno
                            where
                            t.DatReferencia >= DatInicio
                            && t.DatReferencia <= DatFim
                            && t.FlgRevisado == 1
                            && String.IsNullOrEmpty(t.NumSeriePai)
                            && linhas.Contains(t.CodLinha)
                            select new { t.NumECB }).Count();

                return qtde;
            }
        }

        public static decimal ObterTotalDefeitoReparadosPorLinha(DateTime DatInicio, DateTime DatFim, int CodLinha)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var qtde = (from t in dtbCb.viw_CBDefeitoTurno
                            where
                            t.DatReferencia >= DatInicio
                            && t.DatReferencia <= DatFim
                            && t.FlgRevisado == 1
                            && String.IsNullOrEmpty(t.NumSeriePai)
                            && t.CodLinha == CodLinha
                            select new { NumECB = t.NumECB }).Count();

                return qtde;
            }
        }

        public static decimal ObterTotalDefeitoReparadosPorPosto(DateTime DatInicio, DateTime DatFim, int CodLinha, int NumPosto)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var qtde = (from t in dtbCb.viw_CBDefeitoTurno
                            where
                            t.DatReferencia >= DatInicio
                            && t.DatReferencia <= DatFim
                            && t.FlgRevisado == 1
                            && String.IsNullOrEmpty(t.NumSeriePai)
                            && t.CodLinha == CodLinha
                            && t.NumPosto == NumPosto
                            select new { NumECB = t.NumECB }).Count();

                return qtde;
            }
        }

        public static List<CBLinhaDTO> GetLinhas(string Familia, string Fabrica)
        {
            string fam = string.IsNullOrWhiteSpace(Familia) == true ? "LCD" : Familia;

            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {

                var linhas = Fabrica == "FEC" ? (from l in dtbCb.tbl_CBLinha
                                                 where
                                                    l.Setor == Fabrica
                                                 && l.Familia == fam
                                                 && l.CodLinhaT1 != null
                                                 select l).OrderBy(x => x.DesLinha).ToList()
                              :
                              (from l in dtbCb.tbl_CBLinha
                               where
                                  l.Setor == Fabrica
                                  && l.CodLinhaT1 != null
                               select l).OrderBy(x => x.DesLinha).ToList();

                List<CBLinhaDTO> cBLinhaDTO = new List<CBLinhaDTO>();

                AutoMapper.Mapper.Map(linhas, cBLinhaDTO);

                return cBLinhaDTO;
            }
        }

        #endregion

        #region Indicador por Postos

        public static List<CBPostoDTO> GetPostos(int CodLinha)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var postos = dtbCb.tbl_CBPosto.Where(a => a.CodLinha == CodLinha).OrderBy(o => o.NumPosto).ToList();

                List<CBPostoDTO> postosDTO = new List<CBPostoDTO>();

                AutoMapper.Mapper.Map(postos, postosDTO);

                return postosDTO;
            }
        }

        public static List<IndicadorPostos> ObterIndicadoresQuantidadeProduzidaPostos(DateTime DatInicio, DateTime DatFim, int CodLinha, int Turno)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                List<IndicadorPostos> listaIndicadorPosto = new List<IndicadorPostos>();
                dtbCb.viw_CBPassagemTurno
                    .Where(a => a.CodLinha == CodLinha &&
                                a.DatReferencia == DatInicio &&
                                a.DatReferencia <= DatFim &&
                                a.Turno == Turno)
                    .GroupBy(b => b.NumPosto)
                    .Select(x => new { NumPosto = x.Key, Count = x.Count() })
                    .OrderBy(z => z.NumPosto)
                    .ToList().ForEach(f => listaIndicadorPosto.Add(
                        new IndicadorPostos { NumPosto = f.NumPosto, Quantidade = f.Count }
                        ));

                return listaIndicadorPosto;
            }
        }

        public static List<IndicadorPostos> ObterIndicadoresQuantidadeDefeitos(DateTime DatInicio, DateTime DatFim, int CodLinha, int Turno)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                List<IndicadorPostos> listaIndicadorPosto = new List<IndicadorPostos>();

                dtbCb.viw_CBDefeitoTurno
                    .Where(a => a.CodLinha == CodLinha &&
                                a.DatReferencia >= DatInicio &&
                                a.DatReferencia <= DatFim &&
                                a.Turno == Turno &&
                                String.IsNullOrEmpty(a.NumSeriePai) &&
                                a.FlgRevisado != 2
                                )
                    .GroupBy(b => b.NumPosto)
                    .Select(x => new { NumPosto = x.Key, Count = x.Count() })
                    .OrderBy(z => z.NumPosto)
                    .ToList().ForEach(f => listaIndicadorPosto.Add(
                        new IndicadorPostos { NumPosto = f.NumPosto, Quantidade = f.Count }
                        ));

                return listaIndicadorPosto;
            }
        }

        public static CBLinhaDTO GetLinhaByCodigo(int CodLinha)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var linha = dtbCb.tbl_CBLinha.FirstOrDefault(a => a.CodLinha == CodLinha);

                CBLinhaDTO linhaDTO = new CBLinhaDTO();

                AutoMapper.Mapper.Map(linha, linhaDTO);

                return linhaDTO;
            }
        }

        #endregion

        #region Indicador por Hora do Posto

        public static CBHorarioDTO GetTurno(int Turno)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var horario = dtbCb.tbl_CBHorario.FirstOrDefault(a => a.Turno == Turno);

                CBHorarioDTO horarioDTO = new CBHorarioDTO();
                AutoMapper.Mapper.Map(horario, horarioDTO);

                return horarioDTO;
            }
        }

        public static decimal GetQuantidadeProduzidaPostoEHora(
            DateTime DataInicio,
            DateTime DataFim,
            TimeSpan HoraInicio,
            TimeSpan HoraFim,
            int NumPosto,
            int Turno,
            int CodLinha)
        {

            CBHorarioDTO turnoHorario = GetTurno(Turno);
            string stDataInicio = DataInicio.ToShortDateString() + " " + HoraInicio.ToString();
            string stDataFim = DataFim.ToShortDateString() + " " + HoraFim.ToString();

            if (TimeSpan.Parse(turnoHorario.HoraInicio) > TimeSpan.Parse(turnoHorario.HoraFim) && HoraInicio.Hours >= 0 && HoraFim.Hours <= TimeSpan.Parse(turnoHorario.HoraFim).Hours)
            {
                stDataInicio = DataInicio.AddDays(1).ToShortDateString() + " " + HoraInicio.ToString();
                stDataFim = DataFim.AddDays(1).ToShortDateString() + " " + HoraFim.ToString();
            }

            DateTime DataHoraInicio = DateTime.Parse(stDataInicio);
            DateTime DataHoraFim = DateTime.Parse(stDataFim);
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var query = dtbCb.viw_CBPassagemTurno.Where(a => a.NumPosto == NumPosto &&
                    a.Turno == Turno &&
                    a.CodLinha == CodLinha &&
                    a.DatReferencia >= DataInicio &&
                    a.DatReferencia <= DataFim &&
                    a.DatEvento >= DataHoraInicio && a.DatEvento <= DataHoraFim
                    );

                var resultado = query.ToList();

                return resultado.Count();
            }
        }

        public static decimal GetQuantidadeDefeitosPostoEHora(
            DateTime DataInicio,
            DateTime DataFim,
            TimeSpan HoraInicio,
            TimeSpan HoraFim,
            int NumPosto,
            int Turno,
            int CodLinha)
        {
            CBHorarioDTO turnoHorario = GetTurno(Turno);
            string stDataInicio = DataInicio.ToShortDateString() + " " + HoraInicio.ToString();
            string stDataFim = DataFim.ToShortDateString() + " " + HoraFim.ToString();

            if (TimeSpan.Parse(turnoHorario.HoraInicio) > TimeSpan.Parse(turnoHorario.HoraFim) && HoraInicio.Hours >= 0 && HoraFim.Hours <= TimeSpan.Parse(turnoHorario.HoraFim).Hours)
            {
                stDataInicio = DataInicio.AddDays(1).ToShortDateString() + " " + HoraInicio.ToString();
                stDataFim = DataFim.AddDays(1).ToShortDateString() + " " + HoraFim.ToString();
            }

            DateTime DataHoraInicio = DateTime.Parse(stDataInicio);
            DateTime DataHoraFim = DateTime.Parse(stDataFim);
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var query = dtbCb.viw_CBDefeitoTurno.Where(a => a.NumPosto == NumPosto &&
                    a.Turno == Turno &&
                    a.CodLinha == CodLinha &&
                    a.DatReferencia >= DataInicio &&
                    a.DatReferencia <= DataFim &&
                    a.DatEvento >= DataHoraInicio && a.DatEvento <= DataHoraFim &&
                    String.IsNullOrEmpty(a.NumSeriePai) &&
                    a.FlgRevisado != 2
                    ).ToList();

                return query.Count();
            }
        }

        #endregion

        public static List<viw_CBDefeitoTurnoDTO> GetDefeitos(DateTime DatInicio, DateTime DatFim, string Fabrica, string Familia, int? CodLinha, int? NumPosto)
        {

            try
            {
                using (DTB_CBContext dtbCb = new DTB_CBContext())
                {
                    List<viw_CBDefeitoTurnoDTO> listaIndicadorPosto = new List<viw_CBDefeitoTurnoDTO>();
                    List<tbl_CBLinha> cbLinha = !string.IsNullOrWhiteSpace(Familia) && Fabrica == "FEC" ?
                        dtbCb.tbl_CBLinha.Where(t => t.Setor == Fabrica && t.Familia == Familia && t.CodLinhaT1 != null).ToList()
                        :
                        dtbCb.tbl_CBLinha.Where(t => t.Setor == Fabrica && t.CodLinhaT1 != null).ToList();

                    var linhas = CodLinha == null ? cbLinha.Select(a => a.CodLinha) : cbLinha.Where(a => a.CodLinha.Equals(CodLinha.Value)).Select(a => a.CodLinha);


                    var consultaQuery = NumPosto != null ? dtbCb.viw_CBDefeitoTurno
                        .Where(a => a.DatReferencia >= DatInicio &&
                                    a.DatReferencia <= DatFim &&
                                    a.FlgRevisado != 2 &&
                                    String.IsNullOrEmpty(a.NumSeriePai) &&
                                    linhas.Contains(a.CodLinha) &&
                                    a.NumPosto == NumPosto.Value
                                    )
                                    :
                                    dtbCb.viw_CBDefeitoTurno
                        .Where(a => a.DatReferencia >= DatInicio &&
                                    a.DatReferencia <= DatFim &&
                                    a.FlgRevisado != 2 &&
                                    String.IsNullOrEmpty(a.NumSeriePai) &&
                                    linhas.Contains(a.CodLinha)
                                    );

                    var consulta = consultaQuery.ToList();

                    AutoMapper.Mapper.Map(consulta, listaIndicadorPosto);

                    return listaIndicadorPosto;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
