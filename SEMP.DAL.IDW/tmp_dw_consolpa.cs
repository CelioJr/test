namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tmp_dw_consolpa
    {
        public decimal? seg_auto_tempoparada { get; set; }

        [Key]
        [Column(Order = 0)]
        public long id_consolpa { get; set; }

        public short? is_paradapesa { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id_consol { get; set; }

        public decimal? seg_manu_tempoparada { get; set; }

        public decimal? qt_auto_ocorrencias { get; set; }

        public decimal? qt_manu_ocorrencias { get; set; }

        public decimal? seg_auto_tempoparada_ab { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id_tparada { get; set; }
    }
}
