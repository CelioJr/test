﻿namespace SEMP.Model.DTO
{
	public class LSAAPItemPosicaoDTO
	{
		public string CodFab { get; set; }
		public string NumAP { get; set; }
		public string NumOP { get; set; }
		public string CodModelo { get; set; }
		public string CodItem { get; set; }
		public string NumPosicao { get; set; }
		public decimal? QtdItem { get; set; }

		public string DesModelo { get; set; }
		public string DesItem { get; set; }
	}

}
