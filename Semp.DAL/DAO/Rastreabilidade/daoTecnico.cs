﻿using SEMP.DAL.Common;
using SEMP.DAL.DTB_SSA.Models;
using SEMP.DAL.Models;
using SEMP.Model.DTO;
using SEMP.Model.VO.Rastreabilidade;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;

namespace SEMP.DAL.DAO.Rastreabilidade
{
    public class daoTecnico
	{
        //static daoTecnico()
        //{
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<tbl_CBCadOrigem, CnqOrigemDTO>();
        //        cfg.CreateMap<tbl_CBCadCausa, CnqCausaDTO>();
        //        cfg.CreateMap<tbl_CBCadDefeito, CBCadDefeitoDTO>();
        //        cfg.CreateMap<tbl_CBDefeito, CBDefeitoDTO>();
        //        cfg.CreateMap<CBDefeitoDTO, tbl_CBDefeito>();
        //    });
        //}

        public static List<CnqOrigemDTO> ListaOrigem()
		{
			using (DTB_CBContext contextoSIM = new DTB_CBContext())
			{
				List<tbl_CBCadOrigem> lista = contextoSIM.tbl_CBCadOrigem.Where(a => a.FlgAtivo == true).OrderBy(a => a.DesOrigem).ToList();
				List<CnqOrigemDTO> listaDTO = new List<CnqOrigemDTO>();

				AutoMapper.Mapper.Map(lista, listaDTO);

				return listaDTO;
			}
		}

		public static List<CnqCausaDTO> ListaCausa()
		{
			using (DTB_CBContext contextoSIM = new DTB_CBContext())
			{
				List<tbl_CBCadCausa> lista = contextoSIM.tbl_CBCadCausa.Where(a => a.flgAtivo == "S").OrderBy(a => a.DesCausa).ToList();
				List<CnqCausaDTO> listaDTO = new List<CnqCausaDTO>();
                
				AutoMapper.Mapper.Map(lista, listaDTO);

				return listaDTO;
			}
		}

        public static List<CBCadDefeitoDTO> CodListDefeitos()
        {
            using (DTB_CBContext contextoCB = new DTB_CBContext())
            {
                List<tbl_CBCadDefeito> lista = contextoCB.tbl_CBCadDefeito.Where(a => a.flgAtivo == "1").OrderBy(a => a.CodDefeito).ToList();
                List<CBCadDefeitoDTO> listaDTO = new List<CBCadDefeitoDTO>();

                AutoMapper.Mapper.Map(lista, listaDTO);

                return listaDTO;
            }

        }

		public static string ValidarInserirDefeito(string serial)
		{
			using (DTB_CBContext contextoCB = new DTB_CBContext())
			{
				bool retorno = contextoCB.tbl_CBDefeito.Any(a => a.NumECB == serial && a.FlgRevisado == 0);
				if (retorno)
					return "Já Lido nesta etapa.";
				else
					return "";
			}
		}

		public static bool VerificaSeExisteDefeitoAberto(string serial)
		{
			using (DTB_CBContext contextoCB = new DTB_CBContext())
			{
				bool retorno = contextoCB.tbl_CBDefeito.Any(a => a.NumECB == serial && a.FlgRevisado == 0 && a.FlgRevisado != 2);

				return retorno;
			}
		}

		public static bool VerificaSeDefeitoFechado(string serial)
		{
			using (DTB_CBContext contextoCB = new DTB_CBContext())
			{
				bool retorno = contextoCB.tbl_CBDefeito.Any(a => a.NumECB == serial && (a.FlgRevisado == 1 || a.FlgRevisado == 3) && a.FlgRevisado != 2);

				return retorno;
			}
		}

		public static CBDefeitoDTO GetDefeito(string serial)
		{
			using (DTB_SIMContext contextoSIM = new DTB_SIMContext())
			using (DTB_CBContext contextoCB = new DTB_CBContext())
			{
				tbl_CBDefeito defeito = contextoCB.tbl_CBDefeito.Where(a => a.NumECB == serial && a.FlgRevisado == 0).FirstOrDefault();

				CBDefeitoDTO defeitoDTO = new CBDefeitoDTO();

				AutoMapper.Mapper.Map(defeito, defeitoDTO);

				defeitoDTO.DescricaoModelo = daoGeral.ObterDescricaoItem(defeitoDTO.NumECB.Substring(0, 6));
				defeitoDTO.DescricaoDefeito = daoDefeitos.ObterDesDefeito(defeitoDTO.CodDefeito);
				defeitoDTO.DescricaoPostoApontouDefeito = daoGeral.ObterPosto(defeitoDTO.CodLinha, defeitoDTO.NumPosto).DesPosto;

				if (!string.IsNullOrWhiteSpace(defeito.CodItem))
				{
					defeitoDTO.DescricaoItemPosicao = contextoSIM.tbl_Item.FirstOrDefault(a => a.CodItem == defeito.CodItem).DesItem;
				}
				else
				{
					defeitoDTO.DescricaoItemPosicao = "";
				}

				return defeitoDTO;
			}
		}

		public static CBDefeitoDTO GetDefeito(string serial, int flgRevisado)
		{
			using (DTB_SIMContext contextoSIM = new DTB_SIMContext())
			using (DTB_CBContext contextoCB = new DTB_CBContext())
			{
				tbl_CBDefeito defeito = null;

				if (flgRevisado == 1)
				{
					defeito = contextoCB.tbl_CBDefeito.Where(a => a.NumECB == serial
					 && (a.FlgRevisado == flgRevisado || a.FlgRevisado == 3)).FirstOrDefault();
				}
				else
				{
					defeito = contextoCB.tbl_CBDefeito.Where(a => a.NumECB == serial
					 && a.FlgRevisado == flgRevisado).FirstOrDefault();
				}

				CBDefeitoDTO defeitoDTO = new CBDefeitoDTO();

				AutoMapper.Mapper.Map(defeito, defeitoDTO);

				defeitoDTO.DescricaoModelo = daoGeral.ObterDescricaoItem(defeitoDTO.NumECB.Substring(0, 6));
				defeitoDTO.DescricaoDefeito = daoDefeitos.ObterDesDefeito(defeitoDTO.CodDefeito);
				defeitoDTO.DescricaoPostoApontouDefeito = daoGeral.ObterPosto(defeitoDTO.CodLinha, defeitoDTO.NumPosto).DesPosto;

				if (!string.IsNullOrWhiteSpace(defeito.CodItem))
				{
					defeitoDTO.DescricaoItemPosicao = contextoSIM.tbl_Item.FirstOrDefault(a => a.CodItem == defeito.CodItem).DesItem;
				}
				else
				{
					defeitoDTO.DescricaoItemPosicao = "";
				}

				return defeitoDTO;
			}
		}

		public static void SalvarDefeito(CBDefeitoDTO defeitoDTO)
		{
			try
			{
				using (DTB_CBContext dtb_CB = new DTB_CBContext())
				{
					tbl_CBDefeito defeito = dtb_CB.tbl_CBDefeito.Where(a => a.NumECB == defeitoDTO.NumECB && a.FlgRevisado == 0).FirstOrDefault();

					defeitoDTO.DatRevisado = daoUtil.GetDate();

					AutoMapper.Mapper.Map(defeitoDTO, defeito);

					dtb_CB.SaveChanges();
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Erro ao salvar defeito: " + ex.Message);
			}
		}

        public static void SalvarDefeitoCodDefeitoExistente(CBDefeitoDTO defeitoDTO, int flgRevisado)
        {
            try
            {
                using (DTB_CBContext dtb_CB = new DTB_CBContext())
                {
                    tbl_CBDefeito defeito = dtb_CB.tbl_CBDefeito.Where(a => a.NumECB == defeitoDTO.NumECB && a.CodDefeito == defeitoDTO.CodDefeito && a.FlgRevisado == flgRevisado).FirstOrDefault();

                    defeitoDTO.DatRevisado = daoUtil.GetDate();

                    AutoMapper.Mapper.Map(defeitoDTO, defeito);

                    dtb_CB.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao salvar defeito: " + ex.Message);
            }
        }

		public static void SalvarDefeito(CBDefeitoDTO defeitoDTO, int flgRevisado)
		{
			try
			{
				using (DTB_CBContext dtb_CB = new DTB_CBContext())
				{
					tbl_CBDefeito defeito = dtb_CB.tbl_CBDefeito.Where(a => a.NumECB == defeitoDTO.NumECB && a.FlgRevisado == flgRevisado).FirstOrDefault();

					defeitoDTO.DatRevisado = daoUtil.GetDate();

					AutoMapper.Mapper.Map(defeitoDTO, defeito);

					dtb_CB.SaveChanges();
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Erro ao salvar defeito: " + ex.Message);
			}
		}

		public static void AdicionarDefeito(CBDefeitoDTO defeitoDTO, bool alterarData = true)
		{
			try
			{
				using (DTB_CBContext dtb_CB = new DTB_CBContext())
				{
					tbl_CBDefeito defeito = new tbl_CBDefeito();//dtb_CB.tbl_CBDefeito.Where(a => a.NumECB == defeitoDTO.NumECB && a.FlgRevisado == 1).FirstOrDefault();

					if (alterarData)
					{
						defeitoDTO.DatEvento = daoUtil.GetDate();
					}
					defeitoDTO.DatRevisado = daoUtil.GetDate();

					AutoMapper.Mapper.Map(defeitoDTO, defeito);

					dtb_CB.tbl_CBDefeito.Add(defeito);

					dtb_CB.SaveChanges();
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Erro ao salvar defeito: " + ex.Message);
			}
		}

		public static void ExcluirDefeito(CBDefeitoDTO defeitoDTO)
		{
			try
			{
				using (DTB_CBContext dtb_CB = new DTB_CBContext())
				{
					tbl_CBDefeito defeito = dtb_CB.tbl_CBDefeito.Where(a => a.NumECB == defeitoDTO.NumECB && (a.FlgRevisado == 1 || a.FlgRevisado == 3))
						.FirstOrDefault();

					dtb_CB.tbl_CBDefeito.Remove(defeito);
					dtb_CB.SaveChanges();
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Erro ao salvar defeito: " + ex.Message);
			}
		}

		public static int TransferirDefeito(CBDefeitoDTO defeitoDTO)
		{
			try
			{
				using (DTB_CBContext dtb_CB = new DTB_CBContext())
				{
					tbl_CBDefeito defeito = new tbl_CBDefeito();

					if (defeitoDTO.FlgRevisado == 1)
						defeitoDTO.DatRevisado = daoUtil.GetDate();

					AutoMapper.Mapper.Map(defeitoDTO, defeito);

					dtb_CB.tbl_CBDefeito.Add(defeito);

					int retorno = dtb_CB.SaveChanges();

					return retorno;
				}
			}
			catch
			{
				throw new Exception("Erro ao transferir Defeito.");
			}

		}

		public static bool TranferirPassagem(int CodLinha, string NumSerie)
		{
			try
			{
				using (DTB_CBContext dtbCB = new DTB_CBContext())
				{
					var resultado = dtbCB.Database.SqlQuery<spc_CBFecTransfPassagem>(
							"EXEC spc_CBFecTransfPassagem @codLinha, @numECB",
							new SqlParameter("codLinha", CodLinha),
							new SqlParameter("numECB", NumSerie)
							);

					dtbCB.SaveChanges();

					var retorno = resultado.FirstOrDefault().Result;

					if (retorno.Equals("0"))
						return true;
					else
						return false;
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Erro ao transferir a passsagem: " + ex.Message);
			}
		}

		public static bool TranferirPassagemItem(int codLinha, int numPosto, string numSerie)
		{
			try
			{
				using (DTB_CBContext dtbCB = new DTB_CBContext())
				{
					var resultado = dtbCB.Database.SqlQuery<spc_CBFecTransfPassagem>(
							"EXEC spc_CBFecTransfPassagemItem @codLinha, @numPosto, @numECB",
							new SqlParameter("codLinha", codLinha),
							new SqlParameter("numPosto", numPosto),
							new SqlParameter("numECB", numSerie)
							);

					dtbCB.SaveChanges();

					var retorno = resultado.FirstOrDefault().Result;

					if (retorno.Equals("0"))
						return true;
					else
						return false;
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Erro ao transferir a passsagem: " + ex.Message);
			}
		}

		public static bool TranferirAmarracao(int CodLinha, string NumSerie)
		{
			try
			{
				using (DTB_CBContext dtbCB = new DTB_CBContext())
				{
					var resultado = dtbCB.Database.SqlQuery<spc_CBFecTransfAmarra>(
							"EXEC spc_CBFecTransfAmarra @CodLinha,@NumSerie",
							new SqlParameter("CodLinha", CodLinha),
							new SqlParameter("NumSerie", NumSerie)
							);

					dtbCB.SaveChanges();

					var retorno = resultado.FirstOrDefault().Result;

					if (retorno.Equals("0"))
						return true;
					else
						return false;
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Erro ao transferir a Amarração: " + ex.Message);
			}
		}

		public static bool TranferirAmarracaoItem(string numSerie, string numECB)
		{
			try
			{
				using (DTB_CBContext dtbCB = new DTB_CBContext())
				{
					var resultado = dtbCB.Database.SqlQuery<spc_CBFecTransfAmarra>(
							"EXEC spc_CBFecTransfAmarraItem @NumSerie, @NumECB",
							new SqlParameter("NumSerie", numSerie),
							new SqlParameter("NumECB", numECB)
							);

					dtbCB.SaveChanges();

					var retorno = resultado.FirstOrDefault().Result;

					if (retorno.Equals("0"))
						return true;
					else
						return false;
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Erro ao transferir a Amarração: " + ex.Message);
			}
		}

		public static int GetTotalDefeitoResolvido(int codLinha, int numPosto, int turno)
		{
			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				DateTime DatInicio = new DateTime();
				DateTime DatFim = new DateTime();
				DateTime Data = daoUtil.GetDate();
				DateTime Hora = daoUtil.GetDate();

				TimeSpan HoraAtual = new TimeSpan(Hora.Hour, Hora.Minute, Hora.Second);

				DatInicio = daoGeral.ObterDataInicio(codLinha, HoraAtual);

				if ((Hora.Hour >= 0) && (Hora.Hour < 7))
					DatInicio = DatInicio.AddDays(-1);

				DatFim = daoGeral.ObterDataFim(codLinha, HoraAtual);

				if ((DatFim.Hour >= 0) && (DatFim.Hour < 7))
					DatFim = DatFim.AddDays(1);

				var query = contexto.viw_CBDefeitoTurno.Where(a => (a.CodLinha == codLinha || a.CodLinhaRevisado == codLinha)
					&& a.NumPostoRevisado == numPosto
					&& (a.FlgRevisado.Value == 1 || a.FlgRevisado.Value == 3)
					&& a.Turno == turno
					&& a.DatRevisado >= DatInicio && a.DatRevisado <= DatFim);

				var cont = query.ToList();

				return cont.Count();
			}
		}

		public static List<TecnicoPosicao> GetPosicao(string NumSerie, int CodLinha)
		{
			try
			{
				using (DtbSsaContext contextoSSA = new DtbSsaContext())
				using (DTB_CBContext contextoCB = new DTB_CBContext())
				using (DTB_SIMContext contextoSIM = new DTB_SIMContext())
				{

					var InforSetor = contextoCB.tbl_CBLinha.FirstOrDefault(a => a.CodLinha == CodLinha);

					string regexProduto = @"^\d{6}[A-Za-z]{1}\d{1}[A-Za-z]{2}\d{6}$";
					string regexPlaca = @"^\w{12}$";
					string NE = "";

					var regex = new Regex(regexProduto);
					bool retornoRegexProduto = regex.IsMatch(NumSerie);

					regex = new Regex(regexPlaca);

					bool retornoRegexPlaca = regex.IsMatch(NumSerie);

					if (retornoRegexProduto == true || retornoRegexPlaca == true)
					{
						NE = NumSerie.Substring(0, 6);
					}
					else
					{
						if (NumSerie.Count() == 9)
						{
							var historico = contextoSSA.TblHistorico.Where(a => a.desSerie == NumSerie).FirstOrDefault();
							if (historico != null)
							{
								var tblOF = contextoSSA.Tbl_Of.Where(a => a.codOF == historico.codOF).FirstOrDefault();
								NE = tblOF.codModelo;
							}
						}
						else if (NumSerie.Count() != 12 || NumSerie.Count() != 16 || NumSerie.Count() != 13)
						{
							var defeito = contextoCB.tbl_CBDefeito.Where(a => a.NumECB == NumSerie).FirstOrDefault();
							if (defeito != null)
							{
								NE = defeito.NumSeriePai.Substring(0, 6);
							}
						}
					}

					List<TecnicoPosicao> listaPosicao = new List<TecnicoPosicao>();

					if (InforSetor.Setor.Equals("FEC") && retornoRegexProduto == true)
					{
						if (!string.IsNullOrWhiteSpace(NE))
						{
							listaPosicao = (
							from a in contextoSIM.tbl_PlanoItem
							join b in contextoSIM.tbl_Item on a.CodItem equals b.CodItem
							where a.CodModelo.Contains(NE)
							select new TecnicoPosicao
							{
								Posicao = a.NumPosicao,
								Descricao = b.DesItem,
								CodItem = b.CodItem
							}).ToList();

							// adiciona os itens que fazem parte de conjuntos de painel
							listaPosicao.AddRange((
								from a in contextoSIM.tbl_PlanoItem
								join a2 in contextoSIM.tbl_PlanoItem on a.CodItem equals a2.CodModelo
								join b in contextoSIM.tbl_Item on a2.CodItem equals b.CodItem
								where a.CodModelo == NE && b.CodFam.Contains("173")
								select new TecnicoPosicao
								{
									Posicao = a2.NumPosicao,
									Descricao = b.DesItem,
									CodItem = b.CodItem
								}).ToList()
							);

							// adiciona os itens que fazem parte das placas
							listaPosicao.AddRange((
								from a in contextoSIM.tbl_PlanoItem
								join a2 in contextoSIM.tbl_PlanoItem on a.CodItem equals a2.CodModelo
								join b in contextoSIM.tbl_Item on a2.CodItem equals b.CodItem
								where a.CodModelo == NE && b.CodFam.Contains("031")
								select new TecnicoPosicao
								{
									Posicao = a2.NumPosicao,
									Descricao = b.DesItem,
									CodItem = b.CodItem
								}).ToList()
							);

							// inclui as peças alternativas do modelo
							var listaPosicaoAlt = (from a in contextoSIM.tbl_PlanoAlt
												   join b in contextoSIM.tbl_Item on a.CodItemAlt equals b.CodItem
												   // join original in contextoSIM.tbl_PlanoItem on new { a.CodModelo, a.CodItem } equals new { original.CodModelo, original.CodItem }
												   where a.CodModelo == NE
												   select new TecnicoPosicao
												   {
													   Posicao = a.NumPosicao,
													   Descricao = b.DesItem.Trim() + "(ALT)",
													   CodItem = b.CodItem
												   }).ToList();

							listaPosicao.AddRange(listaPosicaoAlt);
						}
					}
					else
					{
						var DatCorte = DateTime.Now.Date;
						var resultado = contextoCB.Database.SqlQuery<spc_CBPosicoes>(
							"EXEC spc_CBPosicoes @Modelo, @DatCorte",
							new SqlParameter("Modelo", NE),
							new SqlParameter("DatCorte", DatCorte)
							);

						var listaResultadoSP = resultado.ToList();

						listaResultadoSP.ForEach(a => listaPosicao.Add(new TecnicoPosicao
						{
							Posicao = a.NumPosicao,
							Descricao = a.DesItem,
							CodItem = a.CodItem
						}));

					}

					//if (!InforSetor.Setor.Equals("FEC"))
					//{
					//    var lista2 = (
					//    from a in contextoSIM.tbl_PlanoItem
					//    join b in contextoSIM.tbl_Item on a.CodItem equals b.CodItem
					//    where (
					//        from c in contextoSIM.tbl_PlanoItem
					//        where c.CodModelo.Contains(NE)
					//        select c.CodItem).ToList().Contains(a.CodModelo)
					//    select new TecnicoPosicao
					//    {
					//        Posicao = a.NumPosicao,
					//        Descricao = b.DesItem,
					//        CodItem = b.CodItem
					//    }).ToList();

					//    listaPosicao.AddRange(lista2);
					//}

					//if (retornoRegexPlaca && !string.IsNullOrWhiteSpace(NE))
					//{
					//    var NEIAC = ObterCodModeloIAC(NE);
					//    var queryNEIAC = (
					//    from a in contextoSIM.tbl_PlanoItem
					//    join b in contextoSIM.tbl_Item on a.CodItem equals b.CodItem
					//    where a.CodModelo.Contains(NEIAC)
					//    select new TecnicoPosicao
					//    {
					//        Posicao = a.NumPosicao,
					//        Descricao = b.DesItem,
					//        CodItem = b.CodItem
					//    }).ToList();

					//    listaPosicao.AddRange(queryNEIAC);
					//}

					return listaPosicao;
				}
			}
			catch (Exception)
			{

				throw;
			}
		}

		public static String ObterCodModeloIAC(string CodModelo)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var modelo = (from p in dtbSim.tbl_PlanoItem
							  join i in dtbSim.tbl_ItemPCP on p.CodItem equals i.CodItem
							  where p.CodModelo == CodModelo && i.CodProcesso.StartsWith("01") && i.MovEstoque == "S"
							  select i.CodItem);

				return modelo.FirstOrDefault();
			}
		}

		public static List<TecnicoPosicao> GetPosicaoPlaca(string NumSerie, int CodLinha)
		{
			using (DTB_SIMContext contextoSIM = new DTB_SIMContext())
			using (DTB_CBContext contextoCB = new DTB_CBContext())
			{
				var amarra = (from a in contextoCB.tbl_CBAmarra
							  join tipo in contextoCB.tbl_CBTipoAmarra on a.FlgTipo equals tipo.CodTipoAmarra
							  where a.NumSerie == NumSerie && tipo.FlgPlacaPainel == 1
							  select a).FirstOrDefault();

				var listaPosicao = (
						from a in contextoSIM.tbl_PlanoItem
						join b in contextoSIM.tbl_Item on a.CodItem equals b.CodItem
						where a.CodModelo == amarra.CodModelo && a.CodItem == amarra.CodItem
						select new TecnicoPosicao
						{
							Posicao = a.NumPosicao,
							Descricao = b.DesItem,
							CodItem = b.CodItem
						}).ToList();
				if (!listaPosicao.Any())
				{
					listaPosicao = (
						from a in contextoSIM.tbl_PlanoItem
						join a2 in contextoSIM.tbl_PlanoItem on a.CodItem equals a2.CodModelo
						join b in contextoSIM.tbl_Item on a2.CodItem equals b.CodItem
						where a.CodModelo == amarra.CodModelo && a2.CodItem == amarra.CodItem
						select new TecnicoPosicao
						{
							Posicao = a2.NumPosicao,
							Descricao = b.DesItem,
							CodItem = b.CodItem
						}).ToList();
				}

			
				var listaPosicaoAlt = (from a in contextoSIM.tbl_PlanoAlt
									  join b in contextoSIM.tbl_Item on a.CodItem equals b.CodItem
									  where a.CodModelo == amarra.CodModelo && a.CodItem == amarra.CodItem
									  select new TecnicoPosicao
									  {
										  Posicao = a.NumPosicao,
										  Descricao = b.DesItem,
										  CodItem = b.CodItem
									  }).ToList();

				listaPosicao.AddRange(listaPosicaoAlt);

				return listaPosicao;
			}
		}

		public static List<TecnicoPosicao> GetPosicaoPainel(string NumSerie)
		{
			using (DTB_SIMContext contextoSIM = new DTB_SIMContext())
			using (DTB_CBContext contextoCB = new DTB_CBContext())
			{
				var amarra = (from a in contextoCB.tbl_CBAmarra
							  join tipo in contextoCB.tbl_CBTipoAmarra on a.FlgTipo equals tipo.CodTipoAmarra
							  where a.NumSerie == NumSerie && tipo.FlgPlacaPainel == 2
							  select a).FirstOrDefault();

				var listaPosicao = (
						from a in contextoSIM.tbl_PlanoItem
						join b in contextoSIM.tbl_Item on a.CodItem equals b.CodItem
						where a.CodModelo == amarra.CodModelo && a.CodItem == amarra.CodItem
						select new TecnicoPosicao
						{
							Posicao = a.NumPosicao,
							Descricao = b.DesItem,
							CodItem = b.CodItem
						}).ToList();
				if (!listaPosicao.Any())
				{
					listaPosicao = (
						from a in contextoSIM.tbl_PlanoItem
						join a2 in contextoSIM.tbl_PlanoItem on a.CodItem equals a2.CodModelo
						join b in contextoSIM.tbl_Item on a2.CodItem equals b.CodItem
						where a.CodModelo == amarra.CodModelo && a2.CodItem == amarra.CodItem
						select new TecnicoPosicao
						{
							Posicao = a2.NumPosicao,
							Descricao = b.DesItem,
							CodItem = b.CodItem
						}).ToList();
				}

				return listaPosicao;
			}
		}

        public static CBDefeitoDTO GetDefeitoByCodDefeitoExistente(string serial, string codDefeito)
        {
            using (DTB_CBContext contextoCB = new DTB_CBContext())
            {
                tbl_CBDefeito defeito = contextoCB.tbl_CBDefeito.Where(a => a.NumECB == serial && a.CodDefeito == codDefeito && a.FlgRevisado == 2).FirstOrDefault();
                
                CBDefeitoDTO defeitoDTO = new CBDefeitoDTO();

                AutoMapper.Mapper.Map(defeito, defeitoDTO);

                return defeitoDTO;
            }
        }
	}

	public class spc_CBFecTransfPassagem
	{
		public string Result { get; set; }
	}

	public class spc_CBFecTransfAmarra
	{
		public string Result { get; set; }
	}

	public class spc_CBPosicoes
	{
		public string CodItem { get; set; }
		public string NumPosicao { get; set; }
		public string DesItem { get; set; }
	}
}
