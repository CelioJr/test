namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_HDT_PROD")]
    public partial class ALC_HDT_PROD
    {
        [Key]
        [StringLength(255)]
        public string HDT_NAME { get; set; }

        [StringLength(255)]
        public string BEGIN_RANGE { get; set; }

        [StringLength(255)]
        public string CURRENT_RANGE { get; set; }

        [StringLength(255)]
        public string END_RANGE { get; set; }
    }
}
