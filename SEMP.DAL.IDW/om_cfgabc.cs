namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_cfgabc
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_cfgabc()
        {
            om_cfg = new HashSet<om_cfg>();
            om_cfgabclim = new HashSet<om_cfgabclim>();
        }

        [Key]
        public long id_cfgabc { get; set; }

        [StringLength(60)]
        public string cd_cfgabc { get; set; }

        public int? revisao { get; set; }

        [StringLength(100)]
        public string ds_cfgabc { get; set; }

        [Column(TypeName = "date")]
        public DateTime? dt_revisao { get; set; }

        [Column(TypeName = "date")]
        public DateTime? dt_stativo { get; set; }

        public short? st_ativo { get; set; }

        public long? id_usrstativo { get; set; }

        public long? id_usrrevisao { get; set; }

        [StringLength(256)]
        public string obs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfgabclim> om_cfgabclim { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }
    }
}
