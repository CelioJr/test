namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_ROMANEIO_LIST
    {
        [Key]
        public int COD_LIST { get; set; }

        [StringLength(15)]
        public string DOC { get; set; }

        public int? SERIE { get; set; }

        [StringLength(20)]
        public string COD { get; set; }

        public int? QTD { get; set; }

        public DateTime? DATA { get; set; }
    }
}
