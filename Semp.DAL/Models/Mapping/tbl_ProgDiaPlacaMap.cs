using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_ProgDiaPlacaMap : EntityTypeConfiguration<tbl_ProgDiaPlaca>
    {
        public tbl_ProgDiaPlacaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.CodPlaca, t.DatProducao, t.CodLinha, t.CodProcesso, t.QtdProdPM, t.rowguid, t.NumTurno });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodPlaca)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodLinha)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.CodProcesso)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.QtdProdPM)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodAparelho)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.flgCobertura)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.CodConjCin)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodModelo)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.Observacao)
                .HasMaxLength(800)
                .IsUnicode(false);

            this.Property(t => t.FlagTP)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.NumTurno)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("tbl_ProgDiaPlaca");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.CodPlaca).HasColumnName("CodPlaca");
            this.Property(t => t.DatProducao).HasColumnName("DatProducao");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.CodProcesso).HasColumnName("CodProcesso");
            this.Property(t => t.QtdProdPM).HasColumnName("QtdProdPM");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
            this.Property(t => t.CodAparelho).HasColumnName("CodAparelho");
            this.Property(t => t.flgCobertura).HasColumnName("flgCobertura");
            this.Property(t => t.DatEstudo).HasColumnName("DatEstudo");
            this.Property(t => t.DatReferencia).HasColumnName("DatReferencia");
            this.Property(t => t.CodConjCin).HasColumnName("CodConjCin");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.QtdProducao).HasColumnName("QtdProducao");
            this.Property(t => t.Observacao).HasColumnName("Observacao");
            this.Property(t => t.FlagTP).HasColumnName("FlagTP");
            this.Property(t => t.NumTurno).HasColumnName("NumTurno");
        }
    }
}
