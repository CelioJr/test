namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_DOC_RELATORIO
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DOC_CODIGO { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int REL_CODIGO { get; set; }

        [StringLength(500)]
        public string REL_DESCRICAO { get; set; }

        [StringLength(500)]
        public string REL_OBJETIVO { get; set; }

        public byte[] REL_IMAGEM { get; set; }

        public virtual FR_DOC_PRINCIPAL FR_DOC_PRINCIPAL { get; set; }

        public virtual FR_RELATORIO FR_RELATORIO { get; set; }
    }
}
