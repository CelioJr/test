﻿using Newtonsoft.Json;
using SEMP.DAL.Common;
using SEMP.DAL.DTB_SSA.Models;
using SEMP.DAL.Models;
using SEMP.Model;
using SEMP.Model.DTO;
using SEMP.Model.VO;
using SEMP.Model.VO.Rastreabilidade;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace SEMP.DAL.DAO.Rastreabilidade
{
	/// <summary>
	/// Esta classe é responsável por controlar todas as transções com o banco de dados.
	/// </summary>
	public class daoAmarracao
	{
		//static daoAmarracao() {
		//    AutoMapper.Mapper.Initialize(cfg =>
		//    {
		//        cfg.CreateMap<viw_CBPerfilAmarra, viw_CBPerfilAmarraDTO>();
		//        cfg.CreateMap<tbl_CBAmarra, CBAmarraDTO>();
		//    });

		//}

		/// <summary>
		/// Este método é responsável por selecionar informações referentes ao histórico do posto de amarração.
		/// </summary>
		/// <param name="codLinha"></param>
		/// <param name="numPosto"></param>
		/// <returns></returns>
		public static List<CBAmarraDTO> HistoricoAmarracaoPostoSimples(int codLinha, int numPosto)
		{
			using (var contexto = new DTB_CBContext())
			{
				contexto.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
				//var posto = daoGeral.ObterPosto(codLinha, numPosto);

				var historico = (from a in contexto.tbl_CBAmarra
									 //join b in contexto.tbl_CBTipoAmarra on a.FlgTipo equals b.CodTipoAmarra
								 where a.CodLinha == codLinha && a.NumPosto == numPosto
								 orderby a.DatAmarra descending
								 select new CBAmarraDTO
								 {
									 NumSerie = a.NumSerie,
									 NumPosto = a.NumPosto,
									 NumECB = a.NumECB,
									 NumAP = a.NumAP,
									 FlgTipo = a.FlgTipo,
									 //DesTipoAmarra = b.DesTipoAmarra,
									 DatAmarra = a.DatAmarra,
									 CodModelo = a.CodModelo,
									 CodLinha = a.CodLinha,
									 CodItem = a.CodItem,
									 CodFab = a.CodFab
								 }).Take(15).OrderByDescending(o => o.DatAmarra);

				return historico.ToList();
			}
		}

		/// <summary>
		///  Este método é responsável por selecionar informações referentes ao histórico do posto de passagem.
		/// </summary>
		/// <param name="codLinha"></param>
		/// <param name="numPosto"></param>
		/// <returns></returns>
		public static List<CBAmarraDTO> HistoricoPostoPassagem(int codLinha, int numPosto)
		{
			using (var contexto = new DTB_CBContext())
			{
				contexto.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
				//var posto = daoGeral.ObterPosto(codLinha, numPosto);

				var historico = (from a in contexto.tbl_CBPassagem
								 where a.CodLinha == codLinha && a.NumPosto == numPosto
								 orderby a.DatEvento descending
								 select new CBAmarraDTO
								 {
									 NumSerie = a.NumECB,
									 NumPosto = a.NumPosto,
									 NumECB = a.NumECB,
									 DatAmarra = a.DatEvento
								 }).Take(15).OrderByDescending(o => o.DatAmarra);

				return historico.ToList();
			}
		}

		/// <summary>
		///  Este método é responsável por selecionar informações referentes ao histórico do posto de amarração de lotes.
		/// </summary>
		/// <param name="codLinha"></param>
		/// <param name="numPosto"></param>
		/// <returns></returns>
		public static List<CBAmarraDTO> HistoricoAmarracaoPostoLote(int codLinha, int numPosto)
		{
			using (var contexto = new DTB_CBContext())
			{
				//var posto = daoGeral.ObterPosto(codLinha, numPosto);
				contexto.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

				var listaEmbalada = (from a in contexto.tbl_CBEmbalada
									 where a.CodLinha == codLinha && a.NumPosto == numPosto
									 orderby a.DatLeitura descending
									 select new { NumECB = a.NumECB, DataLeitura = a.DatLeitura, numLote = a.NumLote, numCC = a.NumCC }).Take(10).ToList();

				var listaNumECB = listaEmbalada.Select(x => x.NumECB).ToArray();

				var historico = (from a in contexto.tbl_CBAmarra
								 join b in contexto.tbl_CBTipoAmarra on a.FlgTipo equals b.CodTipoAmarra
								 let numSerie = contexto.tbl_CBEmbalada.Where(z => z.CodLinha == codLinha && z.NumPosto == numPosto).OrderByDescending(x => x.DatLeitura).Select(x => x.NumECB).Take(10).ToList()
								 where a.CodLinha == codLinha && a.NumPosto == numPosto && numSerie.Contains(a.NumSerie)
								 orderby a.DatAmarra descending
								 select new CBAmarraDTO
								 {
									 NumSerie = a.NumSerie,
									 NumPosto = a.NumPosto,
									 NumECB = a.NumECB,
									 NumAP = a.NumAP,
									 FlgTipo = a.FlgTipo,
									 DesTipoAmarra = b.DesTipoAmarra,
									 DatAmarra = a.DatAmarra,
									 CodModelo = a.CodModelo,
									 CodLinha = a.CodLinha,
									 CodItem = a.CodItem,
									 CodFab = a.CodFab,
								 }).ToList();

				foreach (var hist in historico)
				{
					var temp = listaEmbalada.FirstOrDefault(x => x.NumECB == hist.NumSerie);

					if (temp != null)
					{
						hist.numCC = temp.numCC;
						hist.numLote = temp.numLote;

					}
				}

				var listaHistoricoAux = historico.Select(x => x.NumSerie).ToArray();

				var x1 = listaEmbalada.Where(z => !listaHistoricoAux.Contains(z.NumECB)).Select(a => new CBAmarraDTO
				{
					NumSerie = a.NumECB,
					NumPosto = 0,
					NumECB = "",
					NumAP = "",
					FlgTipo = 0,
					DesTipoAmarra = "",
					DatAmarra = a.DataLeitura,
					CodModelo = "",
					CodLinha = 0,
					CodItem = "",
					CodFab = "",
					numCC = a.numCC,
					numLote = a.numLote
				}).ToList();

				historico = historico.Union(x1).ToList();

				return historico;
			}
		}

		public static List<CBAmarraDTO> HistoricoAmarracaoPostoLoteAtual(int codLinha, int numPosto, string codModelo)
		{
			using (var contexto = new DTB_CBContext())
			{
				contexto.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

				var resultado = contexto.Database.SqlQuery<CBAmarraDTO>(
							"EXEC spc_HistoricoAmarracaoLoteAtual @CodLinha, @NumPosto, @CodModelo",
							new SqlParameter("CodLinha", codLinha),
							new SqlParameter("NumPosto", numPosto),
							new SqlParameter("CodModelo", codModelo)
							);

				var historico = resultado.ToList();


				return historico;
			}
		}

		/// <summary>
		///  Este método é responsável por selecionar qual o tipo de posto e o que será lido no posto.
		/// </summary>
		/// <param name="codLinha"></param>
		/// <param name="numPosto"></param>
		/// <param name="codModelo"></param>
		/// <param name="codFab"></param>
		/// <param name="numAP"></param>
		/// <returns></returns>
		/// 
		/// 
		public static List<viw_CBPerfilAmarraDTO> GetPerfilPosto(int codLinha, int numPosto, string codModelo, string codFab, string numAP)
		{
			try
			{
				var dtbSSA = new DtbSsaContext();

				using (var contextoSIM = new DTB_SIMContext())
				using (var contexto = new DTB_CBContext())
				{
					var perfil = new List<viw_CBPerfilAmarra>();
					var itemAmarra = new List<ItemDTO>();
					var familias = GetFamiliaLeitura(codLinha, numPosto);
					var codProcessos = GetCodProcessoLeitura(codLinha, numPosto);
					var posto = daoGeral.ObterPosto(codLinha, numPosto);
					var linhaDTO = daoGeral.ObterLinha(codLinha);

					var q = (from a in contextoSIM.tbl_LSAAPItem
							 join b in contextoSIM.tbl_Item on a.CodItemAlt equals b.CodItem
							 from d in contextoSIM.tbl_Familia.Where(y => b.CodFam == y.CodFam)
							 from c in contextoSIM.tbl_ItemPCP.Where(x => a.CodItemAlt == x.CodItem && codProcessos.Contains(x.CodProcesso)).DefaultIfEmpty()
							 where
							 (familias.Contains(d.CodFam) || c.CodProcesso != null) &&
							 d.FlgRastreavel == 1 &&
							 //(c.MovEstoque??"S") == "S" &&
							 a.CodIdentificaItem != "10" &&
							 a.CodFab == codFab &&
							 a.NumAP == numAP
							 select new { CodItem = a.CodItemAlt, DesItem = b.DesItem, CodFam = c.CodProcesso ?? b.CodFam, Frequencia = a.QtdFrequencia });
					//select new { CodItem = a.CodItemAlt, DesItem = b.DesItem, CodFam = (c.CodProcesso) ? c.CodProcesso :  b.CodFam, Frequencia = a.QtdFrequencia });
					var listaLSAAP = q.ToList();

					listaLSAAP.ForEach(f => itemAmarra.Add(new ItemDTO
					{
						CodFam = f.CodFam,
						DesItem = f.DesItem,
						CodItem = f.CodItem,
						//CodProcesso = f.CodProcesso
					}));


					//recupera o dispositivo_id para cada item que deve ser lido na tela.
					foreach (var item in itemAmarra)
					{
						item.dispositivo_id = (from a in dtbSSA.TblItemEstoque
											   where a.NE == item.CodItem
											   select a.dispositivo_id).FirstOrDefault();
					}

					//if (listaLSAAP.Count == 0)
					//{
					//    throw new Exception("Não foi encontrado nenhum item rastreável para ser lido.");                        
					//}

					var perfilAmarra = (from a in contexto.tbl_CBPerfilAmarraPosto
										join b in contexto.tbl_CBTipoAmarra on a.CodTipoAmarra equals b.CodTipoAmarra
										where a.CodLinha == codLinha && a.NumPosto == numPosto
										select new
										{
											CodTipoAmarra = a.CodTipoAmarra,
											b.DesTipoAmarra,
											b.MascaraPadrao,
											b.CodFam,
											b.FlgMascaraPadrao,
											a.Sequencia,
											b.FlgPlacaPainel
										}).OrderBy(x => x.Sequencia).ToList();

					//var lista = contexto.viw_CBPerfilAmarra.Where(a => ak.CodLinha == codLinha && a.NumPosto == numPosto && a.CodModelo == codModelo).ToList();
					var lista = new List<viw_CBPerfilAmarra>();

					foreach (var item in perfilAmarra)
					{

						List<ItemDTO> item1;

						switch (item.CodTipoAmarra)
						{
							case Constantes.AMARRA_PLACA_MAE_TABLET:
								item1 = itemAmarra.Where(i => i.CodFam.Substring(0, 3) == item.CodFam.Substring(0, 3) && i.dispositivo_id == 53).ToList();
								break;
							case Constantes.AMARRA_PLACA_AUX_TABLET:
								item1 = itemAmarra.Where(i => i.CodFam.Substring(0, 3) == item.CodFam.Substring(0, 3) && i.dispositivo_id == 751).ToList();
								break;
							default:
								item1 = itemAmarra.Where(i => i.CodFam == item.CodFam).ToList();
								break;
						}

						if (item1 != null)
						{

							item1.ForEach(f =>
							lista.Add(new viw_CBPerfilAmarra
							{
								CodLinha = codLinha,
								NumPosto = numPosto,
								CodModelo = codModelo,
								CodTipoAmarra = item.CodTipoAmarra,
								CodItem = f.CodItem,
								DesItem = f.DesItem,
								Mascara = item.FlgMascaraPadrao == "S" ? item.MascaraPadrao.Replace("<NE>", f.CodItem) : GetMascara(f.CodItem).Replace("<NE>", f.CodItem),
								DesTipoAmarra = item.DesTipoAmarra,
								Sequencia = item.Sequencia ?? 0,
								flgPlacaPainel = item.FlgPlacaPainel
							}));
						}
					}

					var listaFrequencia = listaLSAAP.Where(a => a.Frequencia > 1).ToList();

					foreach (var item in listaFrequencia)
					{
						var duplicar = lista.Find(a => a.CodItem == item.CodItem);

						for (var i = 1; i < item.Frequencia; i++)
						{
							lista.Add(new viw_CBPerfilAmarra
							{
								CodLinha = duplicar.CodLinha,
								NumPosto = duplicar.NumPosto,
								CodModelo = duplicar.CodModelo,
								CodTipoAmarra = duplicar.CodTipoAmarra,
								CodItem = duplicar.CodItem,
								DesItem = duplicar.DesItem,
								Mascara = duplicar.Mascara,
								DesTipoAmarra = duplicar.DesTipoAmarra,
								Sequencia = duplicar.Sequencia,
								flgPlacaPainel = duplicar.flgPlacaPainel
							});
						}
					}

					//List<string> listaFamiliaInformatica = new List<string> { "680", "627", "634", "618","608","622","606","607"};

					if (linhaDTO.CodLinha == 42 || linhaDTO.CodLinha == 43 || linhaDTO.CodLinha == 44)
					{ //linhas de informatica
						perfil.Add(new viw_CBPerfilAmarra
						{
							CodLinha = codLinha,
							NumPosto = numPosto,
							CodModelo = codModelo,
							CodTipoAmarra = Constantes.AMARRA_PRODUTO,
							CodItem = codModelo,
							DesItem = daoGeral.ObterDescricaoItem(codModelo),
							Mascara = "^(\\d{9})$",
							DesTipoAmarra = "Produto",
							Sequencia = SEMP.Model.Constantes.SEQ_PRODUTO,
							flgPlacaPainel = 0
						});
					}
					else
					{
						if (linhaDTO.Setor.Equals("LCM"))
						{
							perfil.Add(new viw_CBPerfilAmarra
							{
								CodLinha = codLinha,
								NumPosto = numPosto,
								CodModelo = codModelo,
								CodTipoAmarra = Constantes.AMARRA_PRODUTO,
								CodItem = codModelo,
								DesItem = daoGeral.ObterDescricaoItem(codModelo),
								Mascara = "^" + codModelo + "[a-nA-N]{1}[0-9]{1}[a-fA-F]{2}\\d{6}$",
								DesTipoAmarra = "Produto",
								Sequencia = SEMP.Model.Constantes.SEQ_PRODUTO,
								flgPlacaPainel = 0
							});

						}
						else
						{
							perfil.Add(new viw_CBPerfilAmarra
							{
								CodLinha = codLinha,
								NumPosto = numPosto,
								CodModelo = codModelo,
								CodTipoAmarra = Constantes.AMARRA_PRODUTO,
								CodItem = codModelo,
								DesItem = daoGeral.ObterDescricaoItem(codModelo),
								Mascara = linhaDTO.Setor.Equals("FEC") ? "^" + codModelo + "[a-nA-N]{1}[0-9]{1}[a-fA-F]{2}\\d{6}$" : "^" + codModelo + "(\\w{6})$",
								DesTipoAmarra = "Produto",
								Sequencia = SEMP.Model.Constantes.SEQ_PRODUTO,
								flgPlacaPainel = 0
							});
						}


					}

					var CodEAN = "";

					if (contextoSIM.tbl_ProdModelo.Any(a => a.CodModelo == codModelo))
					{
						CodEAN = contextoSIM.tbl_ProdModelo.FirstOrDefault(a => a.CodModelo == codModelo).CodEAN;
					}

					var lerCaixa = contexto.tbl_CBPosto.FirstOrDefault(a => a.CodLinha == codLinha && a.NumPosto == numPosto).TipoAmarra;

					if (linhaDTO.Setor.Equals("FEC") && (linhaDTO.Familia.Equals("AUD") || linhaDTO.Familia.Equals("LCD") || linhaDTO.Familia.Equals("DVD")))
					{   //somente para os postos de embalagem da linha de fechamento e família LCD, DVD e TV
						//nestes casos sempre tem caixa, independente do campo da tabela cbposto

						if (!string.IsNullOrWhiteSpace(CodEAN) && (posto.CodTipoPosto.Equals(9)) || (posto.CodTipoPosto.Equals(6) && posto.TipoAmarra == "CA"))
						{
							perfil.Add(
									new viw_CBPerfilAmarra
									{
										CodLinha = codLinha,
										NumPosto = numPosto,
										CodModelo = codModelo,
										CodTipoAmarra = Constantes.AMARRA_EAN,
										CodItem = CodEAN,
										DesItem = daoGeral.ObterDescricaoItem(codModelo),
										Mascara = "^\\d{8}" + CodEAN + "\\d{1}$",
										DesTipoAmarra = "EAN13",
										Sequencia = SEMP.Model.Constantes.SEQ_CAIXA,
										flgPlacaPainel = 0
									});
						}
					}
					else
					{   //A leitura da caixa pode ser habilitada no banco de dados inserindo "CA" no campo TipoAmarra 
						//caso possua EAN, e seja posto tipo 9 e tenha que ler caixa, habilita a leitura
						if (!string.IsNullOrWhiteSpace(CodEAN) && (posto.CodTipoPosto.Equals(6)) && (lerCaixa != null))
							if (lerCaixa.Equals("CA"))
							{
								perfil.Add(
										new viw_CBPerfilAmarra
										{
											CodLinha = codLinha,
											NumPosto = numPosto,
											CodModelo = codModelo,
											CodTipoAmarra = Constantes.AMARRA_EAN,
											CodItem = CodEAN,
											DesItem = daoGeral.ObterDescricaoItem(codModelo),
											Mascara = "^\\d{8}" + CodEAN + "\\d{1}$",
											DesTipoAmarra = "EAN13",
											Sequencia = SEMP.Model.Constantes.SEQ_CAIXA,
											flgPlacaPainel = 0
										});
							}
					}

					perfil = perfil.Union(lista).ToList();

					//var produto = perfil.FirstOrDefault(x => x.CodTipoAmarra == Constantes.AMARRA_PRODUTO);
					//produto.Sequencia = perfil.Count();

					//perfil = ordenaPerfil(perfil);
					if (posto.FlgTipoTerminal == "S")
					{
						perfil = perfil.OrderByDescending(x => x.Sequencia).ToList();
					}
					else
					{
						perfil = perfil.OrderBy(x => x.Sequencia).ToList();
					}

					var viw_DTO = new List<viw_CBPerfilAmarraDTO>();

					AutoMapper.Mapper.Map(perfil, viw_DTO);

					if (posto.CodTipoPosto == 23)
					{
						viw_DTO = DispositivosIntegradoAPlacaMae(viw_DTO);
					}

					foreach (var item in viw_DTO)
					{
						item.numAP = numAP;
					}


					return viw_DTO;
				}

			}
			catch (Exception ex)
			{
				throw new Exception("Erro ao carregar Itens: " + ex.Message);
			}
		}

		/// <summary>
		/// Este método é responsável por selecionar a familia dos produtos que estão sendo lidos.
		/// </summary>
		/// <param name="codLinha"></param>
		/// <param name="numPosto"></param>
		/// <returns></returns>
		public static List<string> GetFamiliaLeitura(int codLinha, int numPosto)
		{
			using (var contexto = new DTB_CBContext())
			{
				var lista = (from a in contexto.tbl_CBPerfilAmarraPosto
							 join b in contexto.tbl_CBTipoAmarra on a.CodTipoAmarra equals b.CodTipoAmarra
							 where a.CodLinha == codLinha && a.NumPosto == numPosto
							 select b.CodFam).Distinct().Select(a => a);

				return lista.ToList();
			}
		}

		/// <summary>
		/// Este método é responsável por selecionar o código de processo dos produtos que estão sendo lidos.
		/// </summary>
		/// <param name="codLinha"></param>
		/// <param name="numPosto"></param>
		/// <returns></returns>
		public static List<string> GetCodProcessoLeitura(int codLinha, int numPosto)
		{
			using (var contexto = new DTB_CBContext())
			{
				var lista = (from a in contexto.tbl_CBPerfilAmarraPosto
							 join b in contexto.tbl_CBTipoAmarra on a.CodTipoAmarra equals b.CodTipoAmarra
							 where a.CodLinha == codLinha && a.NumPosto == numPosto
							 select b.CodProcesso).Distinct().Select(a => a);

				return lista.ToList();
			}
		}

		/// <summary>
		/// Este método seleciona uma mascará cadastrada para o respectivo item recebido como parametro.
		/// </summary>
		/// <param name="codItem"></param>
		/// <returns></returns>
		public static string GetMascara(string codItem)
		{
			using (var contexto = new DTB_CBContext())
			{
				var mascara = contexto.tbl_CBPerfilMascara.FirstOrDefault(a => a.CodItem == codItem);
				return mascara != null ? mascara.Mascara : "SEM MASCARA";
			}
		}

		/// <summary>
		/// Este metodo é responsável por verificar se o número de série já está salvo na tabela de amarração.
		/// </summary>
		/// <param name="NumECB"></param>
		/// <returns></returns>
		public static bool VerificaSeItemFoiAmarrado(string NumECB)
		{
			using (var contexto = new DTB_CBContext())
			{
				contexto.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
				var retorno = contexto.tbl_CBAmarra.Any(a => a.NumECB == NumECB);

				return retorno;
			}
		}

		public static bool VerificaSeItemTemDefeito(string NumECB)
		{
			using (var contexto = new DTB_CBContext())
			{
				contexto.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
				var retorno = contexto.tbl_CBDefeito.Any(a => a.NumECB == NumECB && (a.FlgRevisado == 0 || a.FlgRevisado == 3));

				return retorno;
			}
		}


		public static Mensagem ValidaLeituraLinhasAnteriores(DadosPosto posto, string numECB, string codDRT)
		{
			Mensagem retorno = new Mensagem() { CodMensagem = "R0001", DesCor = "lime", DesMensagem = "LEITURA OK. LEIA PRÓXIMO." };

			using (var contexto = new DTB_CBContext())
			{
				try
				{

					var linhaAtual = (from linha in contexto.tbl_CBLinha where linha.CodLinha == posto.CodLinha select linha).FirstOrDefault();

					//var setorOrigem = (from linha in contexto.tbl_CBLinha where linha.CodLinha == posto.CodLinha select linha.SetorOrigem).FirstOrDefault();

					if (string.IsNullOrEmpty(linhaAtual.SetorOrigem))
					{
						retorno = daoMensagem.ObterMensagem(Constantes.RAST_REGISTRO_NAO_ENCONTRADO);
						retorno.DesMensagem = "Setor de origem não configurado. Favor configurar o setor de origem da placa/painel";
					}
					else
					{
						// verifica se a linha de origem está configurada para continuar
						if (linhaAtual.CodLinhaOrigem != 0)
						{
							// encontra a linha anterior da pci
							var linhaAnterior = (from l in contexto.tbl_CBLinha
												 join p in contexto.tbl_CBPosto on l.CodLinha equals p.CodLinha
												 from o in contexto.tbl_CBPassagem.Where(x => p.CodLinha == x.CodLinha && p.NumPosto == x.NumPosto)
												 where l.Setor == linhaAtual.SetorOrigem && l.CodLinha == linhaAtual.CodLinhaOrigem && p.flgObrigatorio == true && o.NumECB.Equals(numECB)
												 orderby p.NumPosto descending
												 select p).FirstOrDefault();

							// se encontra a passagem na linha anterior, executa a procedure de validação de passagem
							if (linhaAnterior != null)
							{
								var numposto = daoGeral.ObterUltimoPostoObrigatorio(linhaAnterior.CodLinha);

								retorno = daoPassagem.ValidarErros(numECB, codDRT, linhaAnterior.CodLinha, numposto, "");
							}
							else
							{
								var bDefeito = false;
								if (linhaAtual.Setor == "IMC")
								{
									var defeito = daoDefeitos.ObterDefeitoPCI(numECB);
									if (defeito.Any())
										if (defeito.FirstOrDefault(x => x.CodLinha != linhaAtual.CodLinha && x.FlgRevisado == 1) != null)
										{
											bDefeito = true;
										}

								}
								if (bDefeito == false)
								{
									retorno = daoMensagem.ObterMensagem(Constantes.RAST_REGISTRO_NAO_ENCONTRADO);
									retorno.DesMensagem = "Item lido não passou em todos os postos obrigatórios da linha anterior (SETOR: " + linhaAtual.SetorOrigem + "). Favor verificar.";
								}
								else
								{
									retorno.CodMensagem = Constantes.RAST_LEITURA_REPETIDA;
								}
							}

						}
						else
						{
							var linhaAnterior = (from e in contexto.tbl_CBEmbalada
												 join l in contexto.tbl_CBLinha on e.CodLinha equals l.CodLinha
												 where e.NumECB == numECB && l.Setor == linhaAtual.SetorOrigem
												 select e).FirstOrDefault();

							if (linhaAnterior != null)
							{
								retorno = daoPassagem.ValidarErros(numECB, codDRT, linhaAnterior.CodLinha, linhaAnterior.NumPosto.Value, "");
							}
							else
							{
								retorno = daoMensagem.ObterMensagem(Constantes.RAST_REGISTRO_NAO_ENCONTRADO);
								retorno.DesMensagem = "Item lido não passou em todos os postos obrigatórios da linha anterior (SETOR: " + linhaAtual.SetorOrigem + "). Favor verificar.";
							}

						}


					}
				}
				catch (Exception)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				}
			}

			return retorno;
		}


		public static CBMensagensDTO ValidarItem(string NumECB)
		{
			using (var contexto = new DTB_CBContext())
			{

				var resultado = contexto.Database.SqlQuery<spc_CBFecValidaItemAmarracao>(
							"EXEC spc_CBFecValidaItemAmarracao @NumECB",
							new SqlParameter("NumECB", NumECB)
							);

				var validaItem = resultado.FirstOrDefault();

				var retorno = new CBMensagensDTO();

				if (validaItem.CodErro == 1)
				{
					retorno.erro = true;
					retorno.CodMensagem = "R0002";
					retorno.DesCor = "red";
					retorno.DesMensagem = validaItem.Erro.ToUpper();
				}
				else if (validaItem.CodErro == 2)
				{
					retorno.erro = true;
					retorno.CodMensagem = "R0003";
					retorno.DesCor = "yellow";
					retorno.DesMensagem = validaItem.Erro.ToUpper();
				}
				else if (validaItem.CodErro == 0)
				{
					retorno.erro = false;
					retorno.CodMensagem = "R0001";
					retorno.DesCor = "lime";
					retorno.DesMensagem = "LEITURA OK. LEIA PRÓXIMO.";
				}
				else
				{
					retorno.erro = true;
					retorno.CodMensagem = "E0002";
					retorno.DesCor = "red";
					retorno.DesMensagem = "OCORREU UM ERRO NÃO IDENTIFICADO. ENTRE EM CONTATO COM O ADMINISTRADOR DO SISTEMA.";
				}

				return retorno;
			}
		}

		/// <summary>
		/// Este método é responsável por inserir os dados da amarração no banco de dados.
		/// </summary>
		/// <param name="listaAmarra"></param>
		/// <param name="CodDRT"></param>
		/// <param name="posto"></param>
		/// <returns></returns>
		public static bool InserirAmarracao(List<CBAmarraDTO> listaAmarra, string CodDRT, DadosPosto posto)
		{
			try
			{
				//using (DtbSsaContext contextoSSA = new DtbSsaContext())
				using (var contexto = new DTB_CBContext())
				{
					contexto.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

					tbl_CBPassagem passagem = null;
					tbl_CBIdentifica identifica = null;
					tbl_CBAmarra amarra = null;
					var produto = listaAmarra.FirstOrDefault();

					passagem = new tbl_CBPassagem();
					passagem.CodLinha = produto.CodLinha.Value;
					passagem.NumPosto = produto.NumPosto.Value;
					passagem.NumECB = produto.NumSerie;
					passagem.CodDRT = CodDRT;

					var dataAtual = daoUtil.GetDate();

					passagem.DatEvento = dataAtual;//daoUtil.GetDate();

					if (contexto.tbl_CBIdentifica.Any(x => x.NumECB == passagem.NumECB) == false)
					{
						identifica = new tbl_CBIdentifica();
						identifica.NumECB = produto.NumSerie;
						identifica.CodModelo = produto.NumSerie.Substring(0, 6);
						var fabrica = (from i in contexto.tbl_CBLinha
									   where i.CodLinha == produto.CodLinha.Value
									   select i.Setor).FirstOrDefault().ToUpper().Trim();

						switch (fabrica)
						{
							case ("IAC"):
								identifica.DatIdentifica = dataAtual;//daoUtil.GetDate();
								identifica.CodLinhaIAC = produto.CodLinha.Value.ToString();
								identifica.CodPlacaIAC = daoGeral.ObterCodModeloIAC(identifica.CodModelo);
								break;

							case ("IMC"):
								identifica.DatIMC = dataAtual;//daoUtil.GetDate();
								identifica.CodLinhaIMC = produto.CodLinha.Value.ToString();
								identifica.CodPlacaIAC = daoGeral.ObterCodModeloIAC(identifica.CodModelo);
								identifica.CodPlacaIMC = identifica.CodModelo;
								break;

							case ("FEC"):
								identifica.DatModelo = dataAtual;//daoUtil.GetDate();
								identifica.CodLinhaFEC = produto.CodLinha.Value.ToString();
								identifica.CodPlacaIMC = daoGeral.ObterCodModeloIMC(identifica.CodModelo);
								identifica.CodModelo = identifica.CodModelo;
								identifica.CodPlacaIAC = daoGeral.ObterCodModeloIAC(identifica.CodModelo);
								break;
						}

						contexto.tbl_CBIdentifica.Add(identifica);

						contexto.tbl_CBPassagem.Add(passagem);
					}
					else
					{
						contexto.tbl_CBPassagem.Add(passagem);
					}

					foreach (var item in listaAmarra)
					{
						amarra = new tbl_CBAmarra();
						amarra.NumSerie = item.NumSerie;
						amarra.NumECB = item.NumECB;
						amarra.CodModelo = item.CodModelo;
						amarra.FlgTipo = item.FlgTipo;
						amarra.DatAmarra = dataAtual;//daoUtil.GetDate();
						amarra.CodLinha = item.CodLinha;
						amarra.NumPosto = item.NumPosto;
						amarra.CodFab = item.CodFab;
						amarra.NumAP = item.NumAP;
						amarra.CodItem = item.CodItem;

						// se não é um dispositivo integrado add na Amarracao
						//if (!item.IsDispositivoIntegrado && contexto.tbl_CBAmarra.Any(j => j.NumSerie == item.NumSerie
						//    && j.NumECB == item.NumECB && j.CodLinha == item.CodLinha && j.NumPosto == item.NumPosto) == false)
						//{
						contexto.tbl_CBAmarra.Add(amarra);
						//}
					}
					contexto.SaveChanges();

					return true;
				}
			}
			catch
			{
				return false;
			}
		}

		public static bool InserirAmarracaoProcedure(List<CBAmarraDTO> listaAmarra, string CodDRT)
		{
			try
			{
				using (var contexto = new DTB_CBContext())
				{
					string jsonString = JsonConvert.SerializeObject(listaAmarra.Select(s => new
					{
						NumSerie = s.NumSerie,
						NumECB = s.NumECB,
						CodModelo = s.CodModelo,
						CodItem = s.CodItem,
						CodLinha = s.CodLinha,
						CodFab = s.CodFab,
						NumAP = s.NumAP,
						NumPosto = s.NumPosto,
						FlgTipo = s.FlgTipo
					}).ToArray());

					var resultado = contexto.Database.SqlQuery<spc_CBFecAmarraItens>(
							"EXEC spc_CBFecAmarraItens @CodDRT, @JSON",
							new SqlParameter("CodDRT", CodDRT),
							new SqlParameter("JSON", jsonString)
							);

					contexto.SaveChanges();

					var retorno = resultado.FirstOrDefault();

					if (retorno.MSG == "sucesso")
					{
						return true;
					}
					else
					{
						return false;
					}

				}
			}
			catch
			{
				return false;
			}
		}

		public static bool InserirAmarracaoInformatica(List<CBAmarraDTO> listaAmarra, string CodDRT)
		{
			try
			{
				using (var contexto = new DTB_CBContext())
				{
					tbl_CBPassagem passagem = null;
					tbl_CBIdentifica identifica = null;
					tbl_CBAmarra amarra = null;

					var produto = listaAmarra.FirstOrDefault();

					passagem = new tbl_CBPassagem
					{
						CodLinha = produto.CodLinha.Value,
						NumPosto = produto.NumPosto.Value,
						NumECB = produto.NumSerie,
						CodDRT = CodDRT,
						DatEvento = daoUtil.GetDate()
					};

					if (daoPassagem.VerificaIdentifica(passagem.NumECB) == false)
					{
						identifica = new tbl_CBIdentifica
						{
							NumECB = produto.NumSerie,
							CodModelo = produto.NumSerie.Substring(0, 6)
						};

						var fabrica = daoGeral.ObterFabricaPorLinha(produto.CodLinha.Value).ToUpper().Trim();

						switch (fabrica)
						{
							case ("IAC"):
								identifica.DatIdentifica = daoUtil.GetDate();
								identifica.CodLinhaIAC = produto.CodLinha.Value.ToString();
								identifica.CodPlacaIAC = daoGeral.ObterCodModeloIAC(identifica.CodModelo);
								break;

							case ("IMC"):
								identifica.DatIMC = daoUtil.GetDate();
								identifica.CodLinhaIMC = produto.CodLinha.Value.ToString();
								identifica.CodPlacaIAC = daoGeral.ObterCodModeloIAC(identifica.CodModelo);
								identifica.CodPlacaIMC = identifica.CodModelo;
								break;

							case ("FEC"):
								identifica.DatModelo = daoUtil.GetDate();
								identifica.CodLinhaFEC = produto.CodLinha.Value.ToString();
								identifica.CodPlacaIMC = daoGeral.ObterCodModeloIMC(identifica.CodModelo);
								identifica.CodModelo = identifica.CodModelo;
								identifica.CodPlacaIAC = daoGeral.ObterCodModeloIAC(identifica.CodModelo);
								break;
						}

						contexto.tbl_CBIdentifica.Add(identifica);

						contexto.tbl_CBPassagem.Add(passagem);
					}
					else
					{
						contexto.tbl_CBPassagem.Add(passagem);
					}

					foreach (var item in listaAmarra)
					{
						amarra = new tbl_CBAmarra
						{
							NumSerie = item.NumSerie,
							NumECB = item.NumECB,
							CodModelo = item.CodModelo,
							FlgTipo = item.FlgTipo,
							DatAmarra = daoUtil.GetDate(),
							CodLinha = item.CodLinha,
							NumPosto = item.NumPosto,
							CodFab = item.CodFab,
							NumAP = item.NumAP,
							CodItem = item.CodItem
						};

						// se não é um dispositivo integrado add na Amarracao
						if (!item.IsDispositivoIntegrado && contexto.tbl_CBAmarra.Any(j => j.NumSerie == item.NumSerie
							&& j.NumECB == item.NumECB && j.CodLinha == item.CodLinha && j.NumPosto == item.NumPosto) == false)
						{
							contexto.tbl_CBAmarra.Add(amarra);
						}

						InserirNaBaseDoGePro(amarra, int.Parse(CodDRT), item.DipositivoID);
					}

					contexto.SaveChanges();

					return true;
				}
			}
			catch
			{
				return false;
			}
		}

		/// <summary>
		/// Este método é responsável por inserir no banco de dados do GePro os dados referentes a amarração do tablet/Notebook.
		/// </summary>
		/// <param name="amarra"></param>
		/// <param name="CodDRT"></param>
		/// <param name="IdDispositivo"></param>
		public static void InserirNaBaseDoGePro(tbl_CBAmarra amarra, int CodDRT, int IdDispositivo)
		{
			try
			{

				using (var contextoSSA = new DtbSsaContext())
				using (var contextoSIM = new DTB_SIMContext())
				{
					var ap = contextoSIM.tbl_LSAAP.FirstOrDefault(a => a.NumAP == amarra.NumAP && a.CodFab == amarra.CodFab);

					if (ap == null) return;

					var dispositivoID = IdDispositivo == 0 ? contextoSSA.TblItemEstoque.Where(a => a.NE == amarra.CodItem).FirstOrDefault().dispositivo_id : IdDispositivo;

					if (ap.FlgToshiba.Equals("S"))
					{
						var modelNumber = contextoSIM.tbl_Item.Where(a => a.CodItem == amarra.CodItem).First().CodDesenho;
						var serialNumber = contextoSSA.TblHistoricoToshiba.Where(a => a.NumSerieSTI == amarra.NumSerie).FirstOrDefault().NumSerieToshiba;

						// se for produto TOSHIBA insere na tabela [tbl_Key_Component_d]
						var componenteD = new tbl_Key_Component_d
						{
							Model_Number = modelNumber,
							Serial_Number = serialNumber,
							MFG_Part_Number = amarra.CodModelo,
							MFG_Serial_Number = amarra.NumECB,
							Data_Registro = DateTime.Now,
							cod_operador = CodDRT,
							Data_Desativacao = null,
							cod_OpeDesat = null,
							dispositivo_id = dispositivoID
						};

						contextoSSA.TblKeyComponentD.Add(componenteD);
					}
					else
					{
						// se for produto SEMP insere na tabela [tbl_Key_Component_STI]
						var componenteSTI = new tbl_Key_Component_STI();

						if (contextoSSA.TblKeyComponentSti.Any(a => a.numSerie == amarra.NumSerie &&
																	a.NE == amarra.CodItem && a.serialDispositivo == amarra.NumECB))
						{
							componenteSTI = contextoSSA.TblKeyComponentSti.FirstOrDefault(a => a.numSerie == amarra.NumSerie &&
																							   a.NE == amarra.CodItem && a.serialDispositivo == amarra.NumECB);

							componenteSTI.numSerie = amarra.NumSerie;
							componenteSTI.NE = amarra.CodItem;
							componenteSTI.serialDispositivo = amarra.NumECB;
							componenteSTI.dataRegistro = DateTime.Now;
							componenteSTI.codOperador = CodDRT;
							componenteSTI.dataDesativacao = null;
							componenteSTI.codOpeDesat = null;
							componenteSTI.dispositivo_Id = dispositivoID;
						}
						else
						{
							componenteSTI.numSerie = amarra.NumSerie;
							componenteSTI.NE = amarra.CodItem;
							componenteSTI.serialDispositivo = amarra.NumECB;
							componenteSTI.dataRegistro = DateTime.Now;
							componenteSTI.codOperador = CodDRT;
							componenteSTI.dataDesativacao = null;
							componenteSTI.codOpeDesat = null;
							componenteSTI.dispositivo_Id = dispositivoID;

							contextoSSA.TblKeyComponentSti.Add(componenteSTI);
						}
					}

					contextoSSA.SaveChanges();
				}

			}
			catch (Exception ex)
			{
				throw new Exception("Erro ao inserir o objeto: " + amarra.NumECB + "\n ERRO:" + ex.Message);
			}
		}

		/// <summary>
		/// Este método é responsável por verificar se o produto existe na tabela TblHistorico 
		/// </summary>
		/// <param name="NumSerie"></param>
		/// <param name="NumAP"></param>
		/// <returns></returns>
		public static bool VerificaSeProdutoExisteInformatica(string NumSerie, string NumAP)
		{
			using (var contextoSSA = new DtbSsaContext())
			{
				var codOF = int.Parse(NumAP);
				var retorno = contextoSSA.TblHistorico.Any(a => a.desSerie == NumSerie && a.codOF == codOF);
				return retorno;
			}
		}

		/// <summary>
		/// Este método é responsável por verificar se a placa mãe existe na tabela tbl_CBEmbalada
		/// </summary>
		/// <param name="NumSerie"></param>
		/// <returns></returns>
		public static bool VerificaSePlacaMaeExisteInformatica(string NumSerie)
		{
			try
			{
				//using (SEMP.DTB_CB.Models.DTB_CBContext contexto = new SEMP.DTB_CB.Models.DTB_CBContext())
				using (var contexto = new DTB_CBContext())
				{
					//bool retorno = contexto.tbl_CBEmbalada.Any(a => a.NumECB == NumSerie);
					var result = contexto.Database.SqlQuery<int>(
						"SELECT [dbo].[fun_ExistePlaca] (@serial, @codPlaca)",
						new SqlParameter("serial", NumSerie),
						new SqlParameter("codPlaca", NumSerie.Substring(0, 6))
					).FirstOrDefault();

					return result == 1 ? true : false;
				}
			}
			catch
			{
				return false;
			}

		}

		/// <summary>
		/// Este método é responsável por verificar se a placa mãe tem algum dispositivo associado a ela.
		/// </summary>
		/// <param name="viewDTO"></param>
		/// <returns></returns>
		public static List<viw_CBPerfilAmarraDTO> DispositivosIntegradoAPlacaMae(List<viw_CBPerfilAmarraDTO> viewDTO)
		{
			using (var contextoSSA = new DtbSsaContext())
			{
				var listaRetorno = new List<viw_CBPerfilAmarraDTO>();

				var descricao = string.Empty;
				foreach (var perfil in viewDTO)
				{
					var dispositivoIntegrado = contextoSSA.tbl_DispositivoIntegrado.Where(a => a.NE == perfil.CodItem).ToList();

					foreach (var item in dispositivoIntegrado)
					{
						for (var i = 0; i < item.frequencia; i++)
						{
							if (item.frequencia > 1)
							{
								descricao = contextoSSA.tbl_Dispositivo.Where(a => a.dispositivo_id == item.dispositivo_id).FirstOrDefault().dispositivo;
								descricao = descricao + " " + (i + 1);
							}
							else
							{
								descricao = contextoSSA.tbl_Dispositivo.Where(a => a.dispositivo_id == item.dispositivo_id).FirstOrDefault().dispositivo;
							}


							listaRetorno.Add(new viw_CBPerfilAmarraDTO
							{
								CodLinha = perfil.CodLinha,
								CodModelo = perfil.CodModelo,
								CodTipoAmarra = 0,
								CodItem = item.NE,
								DesItem = descricao,
								Mascara = contextoSSA.tbl_Item_Padrao_Serial.Where(a => a.NE == "582046" && a.dispositivo_id == item.dispositivo_id).FirstOrDefault().ExpressaoRegular,
								DesTipoAmarra = descricao,
								NumPosto = perfil.NumPosto,
								DesModelo = perfil.DesModelo,
								IsDispositivoIntegrado = true,
								DipositivoID = item.dispositivo_id
							});
						}

					}
				}

				viewDTO = viewDTO.Union(listaRetorno).ToList();

				return viewDTO;
			}
		}


		public static bool ValidarSeProdutoInformaticaPassouSeparacao(string numSerie)
		{
			using (var contextoSSA = new DtbSsaContext())
			{
				var retorno = contextoSSA.TblHistorico.Any(a => a.desSerie == numSerie && a.dat2 != null);
				return retorno;
			}
		}

		public static List<viw_CBPerfilAmarra> ordenaPerfil(List<viw_CBPerfilAmarra> perfil)
		{
			foreach (var item in perfil)
			{

				if (item.DesTipoAmarra.Equals("Produto"))
				{
					item.Sequencia = SEMP.Model.Constantes.SEQ_PRODUTO;
				}
				else if (item.DesTipoAmarra.Equals("Placa"))
				{
					item.Sequencia = SEMP.Model.Constantes.SEQ_PLACA;
				}
				else if (item.DesTipoAmarra.Equals("Kit Acessório"))
				{
					item.Sequencia = SEMP.Model.Constantes.SEQ_KIT_ACESSORIO;
				}
				else if (item.DesTipoAmarra.Equals("Painel LCD"))
				{
					item.Sequencia = SEMP.Model.Constantes.SEQ_PAINEL;
				}
				else if (item.DesTipoAmarra.Equals("Suporte"))
				{
					item.Sequencia = SEMP.Model.Constantes.SEQ_SUPORTE;
				}
				else if (item.DesTipoAmarra.Equals("Pedestal"))
				{
					item.Sequencia = SEMP.Model.Constantes.SEQ_PEDESTAL;
				}
				else if (item.DesTipoAmarra.Equals("Base"))
				{
					item.Sequencia = SEMP.Model.Constantes.SEQ_BASE;
				}
				else if (item.DesTipoAmarra.Equals("EAN13"))
				{
					item.Sequencia = SEMP.Model.Constantes.SEQ_CAIXA;
				}
				else if (item.DesTipoAmarra.Equals("Fonte"))
				{
					item.Sequencia = SEMP.Model.Constantes.SEQ_FONTE;
				}
				else
				{ item.Sequencia = 1; }
			}

			perfil = perfil.OrderBy(x => x.Sequencia).ToList();

			return perfil;
		}

		public static List<CBAmarraDTO> RetornaItensAmarrados(int codLinha, int numPosto, string numSerie)
		{
			using (var contexto = new DTB_CBContext())
			{
				contexto.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
				var itens = new List<tbl_CBAmarra>();

				itens = contexto.tbl_CBAmarra.Where(a => a.CodLinha == codLinha && a.NumPosto == numPosto && a.NumSerie == numSerie).ToList();

				var itenDTO = new List<CBAmarraDTO>();

				AutoMapper.Mapper.Map(itens, itenDTO);

				return itenDTO;
			}
		}
	}

	public class spc_CBFecValidaItemAmarracao
	{
		public int CodErro { get; set; }
		public string Erro { get; set; }
	}

	public class spc_CBFecAmarraItens
	{
		public int RESULT { get; set; }
		public string MSG { get; set; }
	}
}
