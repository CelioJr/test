using System;

namespace SEMP.Model.DTO
{
    public class viw_CBPassagemTurnoDTO
    {
        public string NumECB { get; set; }
        public int CodLinha { get; set; }
        public int NumPosto { get; set; }
        public System.DateTime DatEvento { get; set; }
        public string CodDRT { get; set; }
        public Nullable<System.DateTime> DatReferencia { get; set; }
        public Nullable<int> Turno { get; set; }
    }
}
