using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DOC_CASO_USO_GENERALIZACAO_DTO
    {
        public int DOC_CODIGO { get; set; }
        public int USO_CODIGO_HERDADOR { get; set; }
        public int USO_CODIGO_HERDADO { get; set; }
        public virtual FR_DOC_CASO_USO_DTO FR_DOC_CASO_USO { get; set; }
        public virtual FR_DOC_CASO_USO_DTO FR_DOC_CASO_USO1 { get; set; }
    }
}
