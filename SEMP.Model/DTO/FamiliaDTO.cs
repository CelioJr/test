﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.DTO
{
	public partial class FamiliaDTO
	{
		public string CodFam { get; set; }
		public string NomFam { get; set; }
		public string EspTecnica { get; set; }
		public string FlgInspecao { get; set; }
		public string Usuario { get; set; }
		public System.Guid rowguid { get; set; }
		public Nullable<short> FlgAtivo { get; set; }
		public string FlgKitAcessorio { get; set; }
		public string CodFamToshiba { get; set; }
		public Nullable<int> FlgRastreavel { get; set; }
	}
}
