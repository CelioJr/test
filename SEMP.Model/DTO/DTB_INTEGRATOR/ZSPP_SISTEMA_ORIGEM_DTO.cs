namespace SEMP.Model.DTO
{
	using System.ComponentModel.DataAnnotations;

	public partial class ZSPP_SISTEMA_ORIGEM_DTO
	{
        [Key]
        [StringLength(16)]
        public string PACKNO { get; set; }

        [StringLength(10)]
        public string INTTROW { get; set; }
    }
}
