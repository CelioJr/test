using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class TBL_GpaRecPallet
    {
        public long IdRegistro { get; set; }
        public string CodItem { get; set; }
        public System.DateTime DataCriacao { get; set; }
        public int QtdPorPallet { get; set; }
        public int QtdCarregamento { get; set; }
        public string Status { get; set; }
    }
}
