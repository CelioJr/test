namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_proreaativobs
    {
        [Key]
        public long id_proreaativobs { get; set; }

        [StringLength(256)]
        public string ds_obs { get; set; }

        [Column(TypeName = "image")]
        public byte[] foto { get; set; }

        [Column(TypeName = "image")]
        public byte[] audio { get; set; }

        public DateTime? dthr_obs { get; set; }

        public short? is_foto { get; set; }

        public short? is_texto { get; set; }

        public short? is_audio { get; set; }

        public long id_proreaativ { get; set; }

        public virtual dw_proreaativ dw_proreaativ { get; set; }
    }
}
