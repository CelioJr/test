using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBCadOrigemMap : EntityTypeConfiguration<tbl_CBCadOrigem>
    {
        public tbl_CBCadOrigemMap()
        {
            // Primary Key
            this.HasKey(t => t.CodOrigem);

            // Properties
            this.Property(t => t.CodOrigem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.DesOrigem)
                .HasMaxLength(30)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_CBCadOrigem");
            this.Property(t => t.CodOrigem).HasColumnName("CodOrigem");
            this.Property(t => t.DesOrigem).HasColumnName("DesOrigem");
            this.Property(t => t.FlgAtivo).HasColumnName("FlgAtivo");
        }
    }
}
