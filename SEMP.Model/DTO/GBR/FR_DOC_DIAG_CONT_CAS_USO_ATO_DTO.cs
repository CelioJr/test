using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DOC_DIAG_CONT_CAS_USO_ATO_DTO
    {
        public int CAS_USO_ATO_CODIGO { get; set; }
        public int CON_CODIGO { get; set; }
        public virtual FR_DOC_DIAGRAMA_CONTEXTO_DTO FR_DOC_DIAGRAMA_CONTEXTO { get; set; }
    }
}
