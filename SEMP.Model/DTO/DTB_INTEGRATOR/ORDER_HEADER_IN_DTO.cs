namespace SEMP.Model.DTO
{
	using System;
	using System.ComponentModel.DataAnnotations;

	public partial class ORDER_HEADER_IN_DTO
	{
        public Guid ID { get; set; }

        [StringLength(16)]
        public string PACKNO { get; set; }

        [StringLength(4)]
        public string DOC_TYPE { get; set; }

        [StringLength(4)]
        public string SALES_ORG { get; set; }

        [StringLength(2)]
        public string DISTR_CHAN { get; set; }

        [StringLength(2)]
        public string DIVISION { get; set; }

        [StringLength(4)]
        public string SALES_OFF { get; set; }

        [StringLength(3)]
        public string SALES_GRP { get; set; }

        [StringLength(35)]
        public string PURCH_NO_C { get; set; }

        [StringLength(20)]
        public string BSTNK { get; set; }

        [StringLength(3)]
        public string ORD_REASON { get; set; }

        [StringLength(4)]
        public string PMNTTRMS { get; set; }

        [StringLength(1)]
        public string PYMT_METH { get; set; }

        [StringLength(2)]
        public string DLV_BLOCK { get; set; }

        [StringLength(4)]
        public string S_PROC_IND { get; set; }

        [StringLength(16)]
        public string REF_DOC_L { get; set; }

        [StringLength(10)]
        public string PURCH_DATE { get; set; }

        [StringLength(1)]
        public string ORDCOMB_IN { get; set; }

        [StringLength(3)]
        public string INCOTERMS1 { get; set; }
    }
}
