using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.DTB_SSA.Models.Mapping
{
    public class view_NumSerieEstruturaMap : EntityTypeConfiguration<view_NumSerieEstrutura>
    {
        public view_NumSerieEstruturaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.DesSerie, t.CodOF, t.NE, t.QtdFrequencia, t.flgVirtual });

            // Properties
            this.Property(t => t.DesSerie)
                .IsRequired()
                .HasMaxLength(13);

            this.Property(t => t.CodOF)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NE)
                .IsRequired()
                .HasMaxLength(13);

            this.Property(t => t.QtdFrequencia)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DesItemEstoque)
                .HasMaxLength(90);

            this.Property(t => t.DesDispositivo)
                .HasMaxLength(50);

            this.Property(t => t.hcl_tg)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.hcl_f)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.hcl_p)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.flgVirtual)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.flgGEDB)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.flgColetaSerial)
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("view_NumSerieEstrutura");
            this.Property(t => t.DesSerie).HasColumnName("DesSerie");
            this.Property(t => t.CodOF).HasColumnName("CodOF");
            this.Property(t => t.NE).HasColumnName("NE");
            this.Property(t => t.QtdFrequencia).HasColumnName("QtdFrequencia");
            this.Property(t => t.DesItemEstoque).HasColumnName("DesItemEstoque");
            this.Property(t => t.Dispositivo_ID).HasColumnName("Dispositivo_ID");
            this.Property(t => t.DesDispositivo).HasColumnName("DesDispositivo");
            this.Property(t => t.hcl_tg).HasColumnName("hcl_tg");
            this.Property(t => t.hcl_f).HasColumnName("hcl_f");
            this.Property(t => t.hcl_p).HasColumnName("hcl_p");
            this.Property(t => t.flgVirtual).HasColumnName("flgVirtual");
            this.Property(t => t.flgGEDB).HasColumnName("flgGEDB");
            this.Property(t => t.flgColetaSerial).HasColumnName("flgColetaSerial");
        }
    }
}
