﻿using System;
using System.ComponentModel.DataAnnotations;


namespace SEMP.DAL.Models
{
	public class tbl_AcessoMao
	{

		[Key]
		public string Codigo { get; set; }
		public string Acesso { get; set; }
		public string CodigoPai { get; set; }
		public string Sistema { get; set; }
		public Nullable<System.Guid> rowguid { get; set; }

	}
}
