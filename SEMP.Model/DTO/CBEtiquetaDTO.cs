﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SEMP.Model.DTO
{
    public class CBEtiquetaDTO
    {
        public int CodLinha { get; set; }
        public int NumPosto { get; set; }
        public string NomeEtiqueta { get; set; }
        public string DescricaoEtiqueta { get; set; }
        public string CaminhoEtiqueta { get; set; }
        public string NumIP_Impressora { get; set; }
        public string Porta_Impressora { get; set; }
        public Nullable<bool> flgAtivo { get; set; }
        public string NomeImpressora { get; set; }
    }
}
