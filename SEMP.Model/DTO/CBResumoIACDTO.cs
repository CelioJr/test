﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
	public partial class CBResumoIACDTO : IEquatable<CBResumoIACDTO>
	{
		[Key]
		[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
		public System.DateTime Data { get; set; }
		[Key]
		public int Turno { get; set; }
		[Key]
		public string Linha { get; set; }
		[Key]
		[Display(Name = "NE")]
		public string CodModelo { get; set; }

		[Display(Name = "Modelo")]
		public string DesModelo { get; set; }

		[Display(Name = "Plano")]
		public Nullable<int> QtdPlano { get; set; }

		[Display(Name = "Real")]
		public Nullable<int> QtdReal { get; set; }

		public Nullable<int> Dif { get { return QtdReal - QtdPlano; } }

		[Display(Name = "Efic. %")]
		public Nullable<decimal> Eficiencia
		{
			get
			{
				if (QtdPlano > 0) {
					decimal eficiencia = ((decimal)QtdReal / (decimal)QtdPlano) * 100;
					return eficiencia;
				}
				else
					return 0;
			}
		}

		[Display(Name = "Defeitos")]
		public Nullable<int> QtdDefeitos { get; set; }


        [Display(Name = "Defeitos Real")]
        public Nullable<int> QtdDefeitosReal { get; set; }

		public Nullable<decimal> PPM
		{
			get
			{
				if (QtdDefeitos != null && QtdDefeitos > 0 &&
						QtdReal != null && QtdReal > 0 &&
						QtdPontos != null && QtdPontos > 0)
				{
					decimal ppm = (decimal)(QtdDefeitos / ((decimal)QtdReal * (decimal)QtdPontos) * (decimal)1000000);
					return ppm;
				}
				return 0;
			}
		}

        public Nullable<decimal> PPMReal
        {
            get
            {
                if (QtdDefeitosReal != null && QtdDefeitosReal > 0 &&
                        QtdReal != null && QtdReal > 0 &&
                        QtdPontos != null && QtdPontos > 0)
                {
                    decimal ppm = (decimal)(QtdDefeitosReal / ((decimal)QtdReal * (decimal)QtdPontos) * (decimal)1000000);
                    return ppm;
                }
                return 0;
            }
        }

		[Display(Name = "Ocorrência de Produtividade")]
		public string OcorrenciaParada { get; set; }
		public Nullable<int> Apontado { get; set; }

		[Display(Name = "Apontamento Sis. Est.")]
		public Nullable<int> ApontEstoque { get; set; }

		[Display(Name = "Ocorrência da Falta de Apontamento")]
		public string OcorrenciaFaltaApont { get; set; }

		[Display(Name = "No. de Pontos")]
		public Nullable<int> QtdPontos { get; set; }

        [Display(Name = "Qtd. Pontos")]
        public Nullable<int> TotalPontos 
        {
            get
            {
                if (Linha.StartsWith("SMD"))
                    return QtdReal * QtdPontos;
                else
                    return 0;
            }
        }

		[Display(Name = "Tempo Padrão")]
		public Nullable<decimal> TempoPadrao { get; set; }

		[Display(Name = "Ciclo Padrão")]
		public Nullable<int> CicloPadrao { get { return (int?)TempoPadrao * 60; } }

		[Display(Name = "Gargalo Real")]
		public Nullable<int> GargaloReal { get; set; }

		[Display(Name = "Meta por Hora")]
		public Nullable<int> MetaHora { get { return 3600 / GargaloReal; } }

		[Display(Name = "CM-1 PICK UP RATE (%)")]
		public string CM_1_PICKUP { get; set; }

		[Display(Name = "CM-1 MOUNT RATE (%)")]
		public string CM_1_MOUNT { get; set; }

		[Display(Name = "CM-2 PICK UP RATE (%)")]
		public string CM_2_PICKUP { get; set; }

		[Display(Name = "CM-2 MOUNT RATE (%)")]
		public string CM_2_MOUNT { get; set; }

		[Display(Name = "% Ciclo")]
		public Nullable<decimal> PercCiclo { get { return GargaloReal == 0 ? 0 : (CicloPadrao / GargaloReal) * 100; } }

		[Display(Name = "Tempo Disp.")]
		public Nullable<int> TempoDisp { get; set; }

		[Display(Name = "Tempo Trab.")]
		public Nullable<int> TempoTrab { get { return (QtdReal * GargaloReal) / 60; } }

		[Display(Name = "Tempo Parado")]
		public Nullable<int> TempoParado { get { return TempoDisp == 0 ? 0 : TempoTrab - TempoDisp; } }

		[Display(Name = "Eficiência Realização")]
		public string EficienciaRealizacao { get; set; }

		[Display(Name = "Produtividade e OEE")]
		public string Produtividade { get; set; }

		[Key]
		[Display(Name = "Tipo")]
		public string FlagTP { get; set; }




		public bool Equals(CBResumoIACDTO outro)
		{

			if (Object.ReferenceEquals(outro, null)) return false;

			if (Object.ReferenceEquals(this, outro)) return true;

			return Linha.Equals(outro.Linha) && Data.Equals(outro.Data) && Turno.Equals(outro.Turno) && CodModelo.Equals(outro.CodModelo);

		}

		public override int GetHashCode()
		{

			int hashLinha = Linha == null ? 0 : Linha.GetHashCode();
			int hashData = Data == null ? 0 : Data.GetHashCode();
			int hashTurno = Turno.GetHashCode();
			int hashModelo = CodModelo == null ? 0 : CodModelo.GetHashCode();

			return hashData ^ hashLinha ^ hashModelo ^ hashTurno;
		}

	}
}
