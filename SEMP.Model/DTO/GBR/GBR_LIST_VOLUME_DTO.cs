using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_LIST_VOLUME_DTO
    {
        public int COD_VOLUME { get; set; }
        public string VOLUME_STRING { get; set; }
        public string PACK_STRING { get; set; }
        public Nullable<int> COD_USUARIO { get; set; }
        public Nullable<int> COD_SKU { get; set; }
        public string IP { get; set; }
        public Nullable<bool> FECHADO { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
    }
}
