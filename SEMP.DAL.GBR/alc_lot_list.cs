namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.alc_lot_list")]
    public partial class alc_lot_list
    {
        [Key]
        public int id_list { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? dt_pallet { get; set; }

        [StringLength(255)]
        public string packing { get; set; }

        public int? line { get; set; }

        [StringLength(25)]
        public string id_lote { get; set; }

        [StringLength(255)]
        public string order_id { get; set; }

        public int? resp_pallet { get; set; }

        public virtual GBR_CAD_LINHAS GBR_CAD_LINHAS { get; set; }
    }
}
