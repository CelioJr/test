using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.DTB_SSA.Models.Mapping
{
    public class tbl_Key_Component_STIMap : EntityTypeConfiguration<tbl_Key_Component_STI>
    {
        public tbl_Key_Component_STIMap()
        {
            // Primary Key
            this.HasKey(t => new { t.numSerie, t.NE, t.serialDispositivo });

            // Properties
            this.Property(t => t.numSerie)
                .IsRequired()
                .HasMaxLength(9);

            this.Property(t => t.NE)
                .IsRequired()
                .HasMaxLength(13);

            this.Property(t => t.serialDispositivo)
                .IsRequired()
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("tbl_Key_Component_STI");
            this.Property(t => t.numSerie).HasColumnName("numSerie");
            this.Property(t => t.NE).HasColumnName("NE");
            this.Property(t => t.serialDispositivo).HasColumnName("serialDispositivo");
            this.Property(t => t.dataRegistro).HasColumnName("dataRegistro");
            this.Property(t => t.codOperador).HasColumnName("codOperador");
            this.Property(t => t.dataDesativacao).HasColumnName("dataDesativacao");
            this.Property(t => t.codOpeDesat).HasColumnName("codOpeDesat");
            this.Property(t => t.dispositivo_Id).HasColumnName("dispositivo_Id");

            // Relationships
            this.HasRequired(t => t.tbl_ItemEstoque)
                .WithMany(t => t.tbl_Key_Component_STI)
                .HasForeignKey(d => d.NE);

        }
    }
}
