using System;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
	public class CBLinhaPostoPerfilDTO
	{

		[Display(Name = "Modelo")]
		public string CodModelo { get; set; }
		[Display(Name = "Descri��o")]
		public string DesModelo { get; set; }
		[Display(Name = "Linha")]
		public int CodLinha { get; set; }
		[Display(Name = "Descri��o")]
		public string DesLinha { get; set; }
		[Display(Name = "Posto")]
		public int NumPosto { get; set; }
		[Display(Name = "Descri��o")]
		public string DesPosto { get; set; }
		[Display(Name = "Ativo?")]
		public string FlgAtivo { get; set; }
		[Display(Name = "Seq.")]
		public int NumSeq { get; set; }
		[Display(Name = "Obrig.?")]
		public bool? flgObrigatorio { get; set; }
		[Display(Name = "Tempo Ciclo")]
		public decimal? TempoCiclo { get; set; }
		[Display(Name = "Aparelhos por Minuto")]
		public decimal? AparelhosMinuto { get; set; }
		[Display(Name = "Qtd. Jigs")]
		public int? QtdJigs { get; set; }
		[Display(Name = "Posto gargalo")]
		public string FlgGargalo { get; set; }
	}
}
