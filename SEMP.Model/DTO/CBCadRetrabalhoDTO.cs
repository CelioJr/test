﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
    public partial class CBCadRetrabalhoDTO
    {
        [Display(Name = "Código Retrabalho")]
        public string CodRetrab { get; set; }

        [Display(Name = "Data do Registro")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DataRetrab { get; set; }

        [Display(Name = "Código do Modelo")]
        public string CodModelo { get; set; }

        [Display(Name = "Justificativa")]
        public string MotRetrab { get; set; }

        [Display(Name = "Código do Solicitante")]
        public string CodSolicitante { get; set; }

        [Display(Name = "Solicitante Retrabalho")]
        public string SolicRetrab { get; set; }

        [Display(Name = "Status Retrabalho")]
        public string StatusRetrab { get; set; }

        [Display(Name = "Progresso Retrabalho")]
        public string ProgRetrab { get; set; }
    }
}