using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_GpaArea
    {
        public string CodArea { get; set; }
        public string DesArea { get; set; }
    }
}
