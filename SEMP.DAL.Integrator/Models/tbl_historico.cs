namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_historico
    {
        public int ID { get; set; }

        public int ID_TAREFA { get; set; }

        public int ID_ACAO { get; set; }

        [Required]
        [StringLength(100)]
        public string USUARIO { get; set; }

        public DateTime DATAACAO { get; set; }

        [Required]
        [StringLength(800)]
        public string OBS { get; set; }

        public virtual tbl_acao tbl_acao { get; set; }

        public virtual tbl_TAREFA tbl_TAREFA { get; set; }
    }
}
