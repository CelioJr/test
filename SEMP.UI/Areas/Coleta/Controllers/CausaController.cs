﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.Rastreabilidade;
using SEMP.Model.DTO;

namespace SEMP.Areas.Coleta.Controllers
{
	public class CausaController : Controller
	{
		//
		// GET: /Coleta/Causa/

		public ActionResult Index(string flgAtivo)
		{

			var causas = Defeito.ObterCausas();

			if (String.IsNullOrEmpty(flgAtivo))
			{
				causas = causas.Where(x => x.flgAtivo == "S").ToList();
			}
			else
			{
				if (flgAtivo.Equals("A"))
					causas = causas.Where(x => x.flgAtivo == "S").ToList();
				else if (flgAtivo.Equals("I"))
					causas = causas.Where(x => x.flgAtivo == "N").ToList();
			}

			return View(causas);
		}

		public ActionResult Print(string causas)
		{
			
			var causasP = Defeito.ObterCausas();

			if (!String.IsNullOrEmpty(causas))
			{
				var split = causas.Split('|').ToList();
				split.RemoveAll(x => x == "");

				causasP = causasP.Where(x => split.Contains(x.CodCausa)).ToList();

			}

			return View(causasP);

		}

		//
		// GET: /Coleta/Defeitos/Create

		public ActionResult Create()
		{
			return View();
		}

		//
		// POST: /Coleta/Defeitos/Create

		[HttpPost]
		public ActionResult Create(CBCadCausaDTO causa)
		{
			try
			{
				causa.flgAtivo = String.IsNullOrEmpty(causa.flgAtivo) ? "N" : "S";

				if (causa.flgMONTAGEM != null)
				{
					causa.flgMONTAGEM = 1;
				}
				else
				{
					causa.flgMONTAGEM = 0;
				}
				
				if (causa.flgIMC == null)
				{
					causa.flgIMC = 0;
				}
				else
				{
					causa.flgIMC = 1;
				}

				var ultimacausa = Defeito.ObterUltimaCausa();
				ultimacausa = ultimacausa + 1;

				causa.CodCausa = String.Format("{0}{1}", "W", ultimacausa.ToString("000"));

				Defeito.GravarCausa(causa);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}

		//
		// GET: /Coleta/Defeitos/Edit/5

		public ActionResult Edit(string codCausa)
		{
			var defeito = Defeito.ObterCausa(codCausa);

			return View(defeito);
		}

		//
		// POST: /Coleta/Defeitos/Edit/5

		[HttpPost]
		public ActionResult Edit(CBCadCausaDTO causa)
		{
			try
			{
				if (String.IsNullOrEmpty(causa.flgAtivo))
				{
					causa.flgAtivo = "N";
				}
				if (causa.flgMONTAGEM == null)
				{
					causa.flgMONTAGEM = 0;
				}
				if (causa.flgIMC == null)
				{
					causa.flgIMC = 0;
				}

				Defeito.GravarCausa(causa);

				return Json(true, JsonRequestBehavior.AllowGet); ;
			}
			catch
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}

		//
		// GET: /Coleta/Defeitos/Delete/5

		public ActionResult Delete(string codCausa)
		{

			var defeito = Defeito.ObterCausa(codCausa);

			return View(defeito);
		}

		//
		// POST: /Coleta/Defeitos/Delete/5

		[HttpPost]
		public ActionResult Delete(CBCadCausaDTO causa)
		{
			try
			{
				Defeito.RemoverCausa(causa);

				return RedirectToAction("Index");
			}
			catch
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}

		public string ObterDesCausa(string codCausa)
		{

			try
			{
				var causa = Defeito.ObterDesCausa(codCausa);

				return causa;
			}
			catch
			{
				return "";
			}

		}
	}
}
