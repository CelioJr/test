﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO.Ports;
using System.IO;

namespace SISAP.Configuracao
{
    /// <summary>
    /// Interaction logic for ConfigurarBalanca.xaml
    /// </summary>
    public partial class ConfigurarBalanca : Window
    {

        private Thread thread;
        SerialPort serialPort = new SerialPort();
        Passagem passagem = null;
        public ConfigurarBalanca(Passagem passagem)
        {
            this.passagem = passagem;
            InitializeComponent();
            //serialPort.DataReceived += serialPort_DataReceived;
            serialPort.DataReceived += new SerialDataReceivedEventHandler(serialPort_DataReceived);
            thread = new Thread(new ThreadStart(AsynchronousSocketListener.StartListening));
            thread.Start();
        }

        private void btnAtualizar_Click(object sender, RoutedEventArgs e)
        {
            listPortas.Items.Clear();
            txtPeso.Clear();
            string[] ports = SerialPort.GetPortNames();
            foreach (String porta in ports)
            {
                listPortas.Items.Add(porta);
            }
        }

        private void btnConectar_Click(object sender, RoutedEventArgs e)
        {
            if (!serialPort.IsOpen)
            {
                //txtPeso.Clear();
                passagem.SetConfigPorta();
                //serialPort.Close();
                Close();
                //new Passagem().Show();
                //MessageBox.Show("Porta Conectada", "Balança Toledo");
            }
            else
            {
                MessageBox.Show("Porta não Conectada", "Balança Toledo");
            }
        }

        private void listPortas_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (listPortas.SelectedItems.Count != 0 && !serialPort.IsOpen)
            {
                serialPort.PortName = listPortas.SelectedItem.ToString();

                //Salvar a porta
                SEMP.DESKTOP.Configuracao.Config.porta = listPortas.SelectedItem.ToString();

                Configuracao.ConfigurarPorta fConf = new Configuracao.ConfigurarPorta();
                fConf.setSerial(serialPort);
                fConf.ShowDialog();
            }
            else { MessageBox.Show("Nenhuma Porta Localizada"); }
        }

        public void lerSerial(object sender, EventArgs e)
        {
            List<string> listaDados = new List<string>();
            try
            {
                String message;
                message = serialPort.ReadLine();
                message = message.Trim().TrimEnd();

                string t = string.Empty;
                foreach (byte a in message.ToCharArray())
                {
                    if (Convert.ToChar(a).Equals(' ') || Char.IsNumber(Convert.ToChar(a)) || Char.IsSeparator(Convert.ToChar(a)) || Char.IsPunctuation(Convert.ToChar(a)))
                        t += Convert.ToChar(a);
                }

                string[] valores = t.Split(' ');
                foreach (var x in valores)
                {
                    if (!string.IsNullOrEmpty(x.Trim()))
                        listaDados.Add(x);
                }

                txtPeso.Text = (listaDados[2]);
                
                AsynchronousSocketListener.ValorOficial = listaDados[2];

            }
            catch (TimeoutException)
            {

            }

        }

        private void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                //this.Dispatcher.Invoke(new EventHandler(lerSerial));
                List<string> listaDados = new List<string>();
                try
                {
                    String message;
                    message = serialPort.ReadLine();
                    message = message.Trim().TrimEnd();

                    string t = string.Empty;
                    foreach (byte a in message.ToCharArray())
                    {
                        if (Convert.ToChar(a).Equals(' ') || Char.IsNumber(Convert.ToChar(a)) || Char.IsSeparator(Convert.ToChar(a)) || Char.IsPunctuation(Convert.ToChar(a)))
                            t += Convert.ToChar(a);
                    }

                    string[] valores = t.Split(' ');
                    foreach (var x in valores)
                    {
                        if (!string.IsNullOrEmpty(x.Trim()))
                            listaDados.Add(x);
                    }

                    txtPeso.Dispatcher.Invoke(new Action(() => { txtPeso.Text = listaDados[2]; }), null);

                    AsynchronousSocketListener.ValorOficial = listaDados[2];

                }
                catch (TimeoutException)
                {

                }
                
            }
            catch (System.TimeoutException a)
            {
                MessageBox.Show(a.Message.ToString());
            }
        }

        private static void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            string indata = sp.ReadExisting();
            Console.WriteLine("Data Received:");
            Console.Write(indata);
        }

        private void btnFecharPorta_Click(object sender, RoutedEventArgs e)
        {
            serialPort.Close();
        }

        private void Window_Closed_1(object sender, EventArgs e)
        {
            //serialPort.Close();
            Close();
        }
    }
}
