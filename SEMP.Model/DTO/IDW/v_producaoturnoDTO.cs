﻿using System;

namespace SEMP.Model.DTO
{
	public partial class v_producaoturnoDTO
	{
		public Nullable<DateTime> dt_referencia { get; set; }
		public string cd_turno { get; set; }
		public Nullable<DateTime> dthr_iturno { get; set; }
		public Nullable<DateTime> dthr_fturno { get; set; }
		public string cd_gt { get; set; }
		public string cd_pt { get; set; }
		public string cd_cp { get; set; }
		public string cd_folha { get; set; }
		public Nullable<long> producaobruta { get; set; }
		public Int16 is_aponGt { get; set; }
	}
}