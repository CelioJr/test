namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_DOC_DIAGRAMA_CONTEXTO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FR_DOC_DIAGRAMA_CONTEXTO()
        {
            FR_DOC_DIAG_CONT_CAS_USO_ATO = new HashSet<FR_DOC_DIAG_CONT_CAS_USO_ATO>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CON_CODIGO { get; set; }

        [StringLength(255)]
        public string CON_DESCRICAO { get; set; }

        public byte[] CON_IMAGEM { get; set; }

        public int? DOC_CODIGO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_DIAG_CONT_CAS_USO_ATO> FR_DOC_DIAG_CONT_CAS_USO_ATO { get; set; }

        public virtual FR_DOC_PRINCIPAL FR_DOC_PRINCIPAL { get; set; }
    }
}
