﻿namespace SEMP.Model.DTO
{
	public class CBQueimaTipoStatusDTO
	{
		public string CodStatus { get; set; }
		public string DesStatus { get; set; }
		public string DesCor { get; set; }
	}
}
