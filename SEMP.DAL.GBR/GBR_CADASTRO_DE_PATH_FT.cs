namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_CADASTRO_DE_PATH_FT
    {
        [Key]
        public int GBR_CAD_ID { get; set; }

        [Required]
        [StringLength(20)]
        public string POSTO { get; set; }

        [StringLength(65)]
        public string CAMINHO { get; set; }

        [StringLength(65)]
        public string CAMINHO_BACKUP { get; set; }

        public int? COD_SKU { get; set; }

        public bool? CHK_ATIVO { get; set; }

        public bool? IGNORAR { get; set; }

        public virtual GBR_CAD_SKU GBR_CAD_SKU { get; set; }
    }
}
