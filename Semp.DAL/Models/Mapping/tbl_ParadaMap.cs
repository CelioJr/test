using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_ParadaMap : EntityTypeConfiguration<tbl_Parada>
    {
        public tbl_ParadaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.CodParada, t.rowguid });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodParada)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(4)
                .IsUnicode(false);

            this.Property(t => t.DesParada)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(200)
                .IsUnicode(false);

            this.Property(t => t.TipParada)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_Parada");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.CodParada).HasColumnName("CodParada");
            this.Property(t => t.DesParada).HasColumnName("DesParada");
            this.Property(t => t.TipParada).HasColumnName("TipParada");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
        }
    }
}
