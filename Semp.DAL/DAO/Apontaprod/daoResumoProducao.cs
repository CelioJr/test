﻿using SEMP.DAL.IDW;
using SEMP.DAL.Models;
using SEMP.Model;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEMP.DAL.DAO.Rastreabilidade
{
	public class daoResumoProducao
	{
		#region Montagem Final

		public static List<ResumoProducao> ObterResumoMF(DateTime DatInicio, DateTime DatFim)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				//Console.Write(dtbSim.Database.Connection.ConnectionString);

				DateTime inicio = DatInicio.AddDays(-DatInicio.Day + 1);
				DateTime fim = inicio.AddMonths(1).AddDays(-1);

				var produzidoQ = (from p in dtbSim.tbl_ProdDia
								  join item in dtbSim.tbl_Item on p.CodModelo equals item.CodItem
								  join l in dtbSim.tbl_Linha on p.CodLinha equals l.CodLinha
								  join fam in dtbSim.tbl_IAMCFamilia on item.CodFam equals fam.CodFam
								  join vpe in dtbSim.tbl_ProgMesVPE on new { p.CodModelo, p.CodLinha, DatProducao = inicio } equals
									  new { vpe.CodModelo, vpe.CodLinha, vpe.DatProducao } into vpe2
								  from leftvpe in vpe2.DefaultIfEmpty()
								  where p.DatProducao >= DatInicio && p.DatProducao <= DatFim && (p.CodFab == Constantes.CodFab02 || p.CodFab == Constantes.CodFab20)
								  select new ResumoProducao()
								  {
									  DatProducao  = p.DatProducao,
									  Turno        = p.NumTurno,
									  CodLinha     = p.CodLinha,
									  DesLinha     = l.DesLinha,
									  CodModelo    = p.CodModelo,
									  DesModelo    = item.DesItem,
									  Cinescopio   = "",
									  QtdPlano     = 0,
									  QtdRealizado = p.QtdProduzida.HasValue ? (int)p.QtdProduzida : 0,
									  QtdApontado  = p.QtdApontada.HasValue ? (int)p.QtdApontada : 0,
									  QtdPlanoVPE  = leftvpe.QtdProdPm.HasValue ? (int)leftvpe.QtdProdPm : 0,
									  Familia      = fam.NomFam,
									  FlagTp       = p.FlagTP
								  });

				var produzido = produzidoQ.ToList();
				produzido = produzido.GroupBy(x => new { x.DatProducao, x.Turno, x.CodLinha, x.DesLinha, x.CodModelo, x.DesModelo, x.QtdPlano, x.Familia, x.QtdPlanoVPE, x.FlagTp })
							.Select(y => new ResumoProducao()
							{
								DatProducao = y.Key.DatProducao,
								Turno = y.Key.Turno,
								CodLinha = y.Key.CodLinha,
								DesLinha = y.Key.DesLinha,
								CodModelo = y.Key.CodModelo,
								DesModelo = y.Key.DesModelo,
								QtdPlano = y.Key.QtdPlano,
								Familia = y.Key.Familia,
								Cinescopio = "",
								QtdPlanoVPE = y.Key.QtdPlanoVPE,
								QtdRealizado = y.Sum(x => x.QtdRealizado),
								QtdApontado = y.Sum(x => x.QtdApontado),
								FlagTp = y.Key.FlagTp
							}).ToList();

				var programaQ = (from jit in dtbSim.tbl_ProgDiaJit
								 join item in dtbSim.tbl_Item on jit.CodModelo equals item.CodItem
								 join l in dtbSim.tbl_Linha on jit.CodLinha equals l.CodLinha
								 join fam in dtbSim.tbl_IAMCFamilia on item.CodFam equals fam.CodFam
								 join vpe in dtbSim.tbl_ProgMesVPE on new { jit.CodModelo, jit.CodLinha, DatProducao = inicio } equals
									  new { vpe.CodModelo, vpe.CodLinha, vpe.DatProducao } into vpe2
								 from leftvpe in vpe2.DefaultIfEmpty()
								 where jit.DatProducao >= DatInicio && jit.DatProducao <= DatFim
								 group jit by new { jit.CodLinha, jit.NumTurno, jit.DatProducao, jit.CodModelo, fam.NomFam, item.DesItem, leftvpe.QtdProdPm, l.DesLinha, jit.FlagTP } into g
								 select new ResumoProducao()
								 {
									 DatProducao = g.Key.DatProducao,
									 Turno = g.Key.NumTurno,
									 CodLinha = g.Key.CodLinha,
									 DesLinha = g.Key.DesLinha,
									 CodModelo = g.Key.CodModelo,
									 DesModelo = g.Key.DesItem,
									 Cinescopio = "",
									 QtdPlano = (int)g.Sum(x => x.QtdProdPm),
									 QtdRealizado = 0,
									 QtdApontado = 0,
									 QtdPlanoVPE = g.Key.QtdProdPm.HasValue ? (int)g.Key.QtdProdPm : 0,
									 Familia = g.Key.NomFam,
									 FlagTp = g.Key.FlagTP
								 });

				var programa = programaQ.ToList();

				var progMes = (from p in dtbSim.tbl_ProgDiaJit
							   where p.DatProducao >= inicio && p.DatProducao <= fim
							   group p by new { p.CodModelo }
								   into grupo
							   select new
							   {
								   grupo.Key.CodModelo,
								   QtdPrograma = grupo.Sum(x => x.QtdProducao ?? x.QtdProdPm)
							   }).ToList();

				var prodMes = (from p in dtbSim.tbl_ProdDia
							   where p.DatProducao >= inicio && p.DatProducao <= fim
							   group p by new { p.CodModelo }
								   into grupo
							   select new
							   {
								   grupo.Key.CodModelo,
								   QtdProduzido = grupo.Sum(x => x.QtdProduzida)
							   }).ToList();

				var gpa = (from g in dtbSim.TBL_GpaRecItem
						   join b in dtbSim.tbl_BarrasProducao on new { g.CodItem, g.NumSerie } equals new { CodItem = b.CodModelo, NumSerie = b.NumSerieModelo }
						   where g.DataReferencia >= DatInicio && g.DataReferencia <= DatFim
						   group g by new { g.CodItem, g.CodLinha, b.DatReferencia } into grupo
						   select new { CodItem = grupo.Key.CodItem, CodLinha = grupo.Key.CodLinha, QtdExped = grupo.Count(), grupo.Key.DatReferencia }).ToList();

				var gpa2 = (from g in dtbSim.tbl_GpaMovimentacao
							join b in dtbSim.tbl_BarrasProducao on new { g.CodItem, g.DocInterno2 } equals new { CodItem = b.CodModelo, DocInterno2 = b.NumSerieModelo }
							where g.DataMovimentacao >= DatInicio && g.DataMovimentacao <= DatFim && g.DataFinal != null && g.TipDocto1 == "INSP.OBA"
							group g by new { g.CodItem, b.CodLinha, b.DatReferencia } into grupo
							select new { CodItem = grupo.Key.CodItem, CodLinha = grupo.Key.CodLinha, QtdOBA = grupo.Count(), grupo.Key.DatReferencia }).ToList();

				foreach (var prod in produzido)
				{
					var pMes = progMes.FirstOrDefault(x => x.CodModelo == prod.CodModelo);
					var rMes = prodMes.FirstOrDefault(x => x.CodModelo == prod.CodModelo);
					var pDia = programa.FirstOrDefault(x => x.CodModelo == prod.CodModelo && x.CodLinha == prod.CodLinha && x.Turno == prod.Turno && x.DatProducao == prod.DatProducao && x.FlagTp == prod.FlagTp);
					var gItem = gpa.FirstOrDefault(x => x.CodItem == prod.CodModelo && x.CodLinha == prod.CodLinha && x.DatReferencia == prod.DatProducao);
					var gOba = gpa2.FirstOrDefault(x => x.CodItem == prod.CodModelo && x.CodLinha == prod.CodLinha && x.DatReferencia == prod.DatProducao);

					if (rMes != null) prod.QtdProduzidoMes = rMes.QtdProduzido;
					if (pMes != null) prod.QtdPlanoMes = (int?)pMes.QtdPrograma;
					if (pDia != null) prod.QtdPlano = pDia.QtdPlano;
					if (gItem != null) prod.QtdExped = gItem.QtdExped;
					if (gOba != null) prod.QtdOBA = gOba.QtdOBA;
				}

				var naoproduzidos = programa.Where(x => !(produzido.Where(p => p.CodModelo == x.CodModelo && p.DatProducao == x.DatProducao && p.CodLinha == x.CodLinha && p.Turno == x.Turno && p.FlagTp == x.FlagTp)).Any()).ToList();

				foreach (var naop in naoproduzidos)
				{
					var pMes = progMes.FirstOrDefault(x => x.CodModelo == naop.CodModelo);
					var rMes = prodMes.FirstOrDefault(x => x.CodModelo == naop.CodModelo);

					if (rMes != null) naop.QtdProduzidoMes = rMes.QtdProduzido;
					if (pMes != null) naop.QtdPlanoMes = (int?)pMes.QtdPrograma;
				}

				produzido.AddRange(naoproduzidos);

				return produzido.OrderBy(x => x.Turno).ThenBy(x => x.DatProducao).ThenBy(x => x.CodLinha).ThenBy(x => x.CodModelo).ToList();
			}
		}

		public static List<ResumoProducao> ObterResumoMF(DateTime DatInicio, DateTime DatFim, String CodModelo)
		{
			var programa = ObterResumoMF(DatInicio, DatFim).Where(x => x.CodModelo == CodModelo).ToList();
			return programa;
		}

		public static List<ResumoProducao> ObterResumoMF(DateTime DatInicio, DateTime DatFim, String CodModelo, String Familia)
		{
			var programa = ObterResumoMF(DatInicio, DatFim).Where(x => x.CodModelo == CodModelo && x.Familia == Familia).ToList();
			return programa;
		}

		public static List<ResumoProducao> ObterResumoMFFam(DateTime DatInicio, DateTime DatFim, String Familia)
		{
			var programa = ObterResumoMF(DatInicio, DatFim).Where(x => x.Familia == Familia).ToList();
			return programa;
		}

		public static List<ResumoProducao> ObterResumoModelo(DateTime DatInicio, DateTime DatFim)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				DateTime inicio = DatInicio.AddDays(-DatInicio.Day + 1);
				DateTime fim = inicio.AddMonths(1).AddDays(-1);

				var producaoQ = (from p in dtbSim.tbl_ProdDia
								 join item in dtbSim.tbl_Item on p.CodModelo equals item.CodItem
								 join fam in dtbSim.tbl_IAMCFamilia on item.CodFam equals fam.CodFam
								 join vpe in dtbSim.tbl_ProgMesVPE on new { p.CodModelo, p.CodLinha, DatProducao = inicio } equals
									  new { vpe.CodModelo, vpe.CodLinha, vpe.DatProducao } into vpe2
								 from leftvpe in vpe2.DefaultIfEmpty()
								 where p.DatProducao >= DatInicio && p.DatProducao <= DatFim
								 group p by new { p.CodModelo, fam.NomFam, item.DesItem, leftvpe.QtdProdPm } into g1
								 select new ResumoProducao()
								 {
									 Turno = 1,
									 CodLinha = "",
									 DesLinha = "",
									 CodModelo = g1.Key.CodModelo,
									 DesModelo = g1.Key.DesItem,
									 Cinescopio = "",
									 QtdPlano = 0,
									 QtdRealizado = g1.Sum(x => x.QtdProduzida).Value,
									 QtdApontado = g1.Sum(x => x.QtdApontada).Value,
									 QtdPlanoVPE = g1.Key.QtdProdPm ?? 0,
									 Familia = g1.Key.NomFam
								 });

				var producao = producaoQ.ToList();

				var programaQ = (from jit in dtbSim.tbl_ProgDiaJit
								 join item in dtbSim.tbl_Item on jit.CodModelo equals item.CodItem
								 join fam in dtbSim.tbl_IAMCFamilia on item.CodFam equals fam.CodFam
								 join vpe in dtbSim.tbl_ProgMesVPE on new { jit.CodModelo, jit.CodLinha, DatProducao = inicio } equals
									  new { vpe.CodModelo, vpe.CodLinha, vpe.DatProducao } into vpe2
								 from leftvpe in vpe2.DefaultIfEmpty()
								 where jit.DatProducao >= DatInicio && jit.DatProducao <= DatFim
								 group jit by new { jit.CodModelo, fam.NomFam, item.DesItem, leftvpe.QtdProdPm } into g1
								 select new ResumoProducao()
								 {
									 Turno = 1,
									 CodLinha = "",
									 DesLinha = "",
									 CodModelo = g1.Key.CodModelo,
									 DesModelo = g1.Key.DesItem,
									 Cinescopio = "",
									 QtdPlano = (int)g1.Sum(x => x.QtdProdPm),
									 QtdRealizado = 0,
									 QtdApontado = 0,
									 QtdPlanoVPE = g1.Key.QtdProdPm ?? 0,
									 Familia = g1.Key.NomFam
								 });

				var programa = programaQ.ToList();

				var progMes = (from p in dtbSim.tbl_ProgDiaJit
							   where p.DatProducao >= inicio && p.DatProducao <= fim
							   group p by new { p.CodModelo }
								   into grupo
							   select new
							   {
								   grupo.Key.CodModelo,
								   QtdPrograma = grupo.Sum(x => x.QtdProducao ?? x.QtdProdPm)
							   }).ToList();

				var prodMes = (from p in dtbSim.tbl_ProdDia
							   where p.DatProducao >= inicio && p.DatProducao <= fim
							   group p by new { p.CodModelo }
								   into grupo
							   select new
							   {
								   grupo.Key.CodModelo,
								   QtdProduzido = grupo.Sum(x => x.QtdProduzida)
							   }).ToList();

				foreach (var prod in producao)
				{
					var pMes = progMes.FirstOrDefault(x => x.CodModelo == prod.CodModelo);
					var rMes = prodMes.FirstOrDefault(x => x.CodModelo == prod.CodModelo);
					var pDia = programa.FirstOrDefault(x => x.CodModelo == prod.CodModelo);

					if (rMes != null) prod.QtdProduzidoMes = rMes.QtdProduzido;
					if (pMes != null) prod.QtdPlanoMes = (int?)pMes.QtdPrograma;
					if (pDia != null) prod.QtdPlano = pDia.QtdPlano;
				}

				var naoproduzidos = programa.Where(x => !(producao.Select(p => p.CodModelo)).Contains(x.CodModelo)).ToList();

				foreach (var naop in naoproduzidos)
				{
					var pMes = progMes.FirstOrDefault(x => x.CodModelo == naop.CodModelo);
					var rMes = prodMes.FirstOrDefault(x => x.CodModelo == naop.CodModelo);

					if (rMes != null) naop.QtdProduzidoMes = rMes.QtdProduzido;
					if (pMes != null) naop.QtdPlanoMes = (int?)pMes.QtdPrograma;
				}

				producao.AddRange(naoproduzidos);

				return producao;
			}
		}

		public static List<ResumoProducao> ObterResumoModelo(DateTime DatInicio, DateTime DatFim, String CodModelo)
		{
			var programa = ObterResumoModelo(DatInicio, DatFim).Where(x => x.CodModelo == CodModelo).ToList();
			return programa;
		}

		public static List<ResumoProducao> ObterResumoModelo(DateTime DatInicio, DateTime DatFim, String CodModelo, String Familia)
		{
			var programa = ObterResumoModelo(DatInicio, DatFim).Where(x => x.CodModelo == CodModelo && x.Familia == Familia).ToList();
			return programa;
		}

		public static List<ResumoProducao> ObterResumoModeloFam(DateTime DatInicio, DateTime DatFim, String Familia)
		{
			var programa = ObterResumoModelo(DatInicio, DatFim).Where(x => x.Familia == Familia).ToList();
			return programa;
		}

		#endregion Montagem Final

		#region IMC

		public static List<ResumoProducao> ObterResumoIMC(DateTime DatInicio, DateTime DatFim)
		{
			DateTime inicio = DatInicio.AddDays(-DatInicio.Day + 1);
			DateTime fim = inicio.AddMonths(1).AddDays(-1);

			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var programaQ = (from jit in dtbSim.tbl_ProgDiaPlaca
								 join item in dtbSim.tbl_Item on jit.CodPlaca equals item.CodItem
								 join p in dtbSim.tbl_ProdDiaPlaca on
									 new { jit.CodLinha, jit.CodPlaca, jit.DatProducao, jit.NumTurno, jit.FlagTP }
									 equals new { p.CodLinha, p.CodPlaca, p.DatProducao, p.NumTurno, p.FlagTP } into p2
								 join linha in dtbSim.tbl_Linha on jit.CodLinha equals linha.CodLinha into l2
								 from prod in p2.DefaultIfEmpty()
								 from leftlinha in l2.DefaultIfEmpty()
								 where
									 jit.DatProducao >= DatInicio && jit.DatProducao <= DatFim &&
									 (jit.CodProcesso.StartsWith("02") || jit.CodLinha.Contains("DAT"))
								 select new ResumoProducao()
								 {
									 DatProducao = jit.DatProducao,
									 Turno = jit.NumTurno,
									 CodLinha = jit.CodLinha,
									 DesLinha = leftlinha.DesLinha,
									 CodModelo = jit.CodPlaca,
									 DesModelo = item.DesItem,
									 Cinescopio = "",
									 QtdPlano = (int)(jit.QtdProdPM),
									 QtdRealizado = (int)(prod.QtdProduzida ?? 0),
									 QtdApontado = (int)(prod.QtdApontada ?? 0),
									 FlagTp = jit.FlagTP
								 }).ToList();

				var producaoQ = (from prod in dtbSim.tbl_ProdDiaPlaca
								 join item in dtbSim.tbl_Item on prod.CodPlaca equals item.CodItem
								 from jit in dtbSim.tbl_ProgDiaPlaca.Where(x =>
									 x.CodLinha == prod.CodLinha &&
									 x.CodPlaca == prod.CodPlaca &&
									 x.DatProducao == prod.DatProducao &&
									 x.NumTurno == prod.NumTurno &&
									 x.FlagTP == prod.FlagTP
									 ).DefaultIfEmpty()
								 from linha in dtbSim.tbl_Linha.Where(y => y.CodLinha == prod.CodLinha).DefaultIfEmpty()
								 where jit == null &&
									  prod.DatProducao >= DatInicio && prod.DatProducao <= DatFim &&
									  (prod.CodProcesso.StartsWith("02") || prod.CodLinha.Contains("DAT"))
								 select new ResumoProducao()
								 {
									 DatProducao = prod.DatProducao,
									 Turno = prod.NumTurno,
									 CodLinha = prod.CodLinha,
									 DesLinha = linha.DesLinha,
									 CodModelo = prod.CodPlaca,
									 DesModelo = item.DesItem,
									 Cinescopio = "",
									 QtdPlano = 0,
									 QtdRealizado = (int)prod.QtdProduzida,
									 QtdApontado = prod.QtdApontada.HasValue ? (int)prod.QtdApontada : 0,
									 FlagTp = prod.FlagTP
								 }).ToList();

				var programa = new List<ResumoProducao>();
				programa.AddRange(programaQ);
				programa.AddRange(producaoQ);
				programa = programa.OrderBy(x => x.Turno).ThenBy(y => y.CodLinha).ToList();

				var progMes = (from p in dtbSim.tbl_ProgDiaPlaca
							   where p.DatProducao >= inicio && p.DatProducao <= fim
							   group p by new { p.CodLinha, p.NumTurno, p.CodPlaca }
								   into grupo
							   select new
							   {
								   grupo.Key.CodLinha,
								   grupo.Key.NumTurno,
								   grupo.Key.CodPlaca,
								   QtdPrograma = grupo.Sum(x => x.QtdProdPM)
							   }).ToList();

				var prodMes = (from p in dtbSim.tbl_ProdDiaPlaca
							   where p.DatProducao >= inicio && p.DatProducao <= fim
							   group p by new { p.CodLinha, p.NumTurno, p.CodPlaca }
								   into grupo
							   select new
							   {
								   grupo.Key.CodLinha,
								   grupo.Key.NumTurno,
								   grupo.Key.CodPlaca,
								   QtdProduzido = grupo.Sum(x => x.QtdProduzida)
							   }).ToList();

				foreach (var prog in programa)
				{
					var pMes =
						progMes.FirstOrDefault(
							x => x.CodLinha == prog.CodLinha && x.CodPlaca == prog.CodModelo && x.NumTurno == prog.Turno);
					var rMes =
						prodMes.FirstOrDefault(
							x => x.CodLinha == prog.CodLinha && x.CodPlaca == prog.CodModelo && x.NumTurno == prog.Turno);

					if (rMes != null) prog.QtdProduzidoMes = (int)rMes.QtdProduzido;
					if (pMes != null) prog.QtdPlanoMes = (int?)pMes.QtdPrograma;
				}

				return programa;
			}
		}

		public static List<ResumoProducao> ObterResumoIMC(DateTime DatInicio, DateTime DatFim, String CodModelo)
		{
			var programa = ObterResumoIMC(DatInicio, DatFim).Where(x => x.CodModelo == CodModelo).ToList();
			return programa;
		}

		public static List<ResumoProducao> ObterResumoIMCModelo(DateTime DatInicio, DateTime DatFim, string CodModelo)
		{
			DateTime inicio = DatInicio.AddDays(-DatInicio.Day + 1);
			DateTime fim = inicio.AddMonths(1).AddDays(-1);

			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var producaoQ = (from p in dtbSim.tbl_ProdDiaPlaca
								 join item in dtbSim.tbl_Item on p.CodPlaca equals item.CodItem
								 where
									 p.DatProducao >= DatInicio && p.DatProducao <= DatFim &&
									 (p.CodProcesso.StartsWith("02") || p.CodLinha.Contains("DAT"))
								 group p by new { p.CodPlaca, p.NumTurno, item.DesItem } into g1
								 select new ResumoProducao()
								 {
									 Turno = g1.Key.NumTurno,
									 CodLinha = "",
									 DesLinha = "",
									 CodModelo = g1.Key.CodPlaca,
									 DesModelo = g1.Key.DesItem,
									 Cinescopio = "",
									 QtdPlano = 0,
									 QtdRealizado = (int)g1.Sum(x => x.QtdProduzida),
									 QtdApontado = (int)g1.Sum(x => x.QtdApontada),
									 QtdPlanoVPE = 0,
									 FlagTp = ""
								 });//.OrderBy(x => new { x.Turno, x.CodModelo });

				var producao = producaoQ.ToList();

				var programaQ = (from jit in dtbSim.tbl_ProgDiaPlaca
								 join item in dtbSim.tbl_Item on jit.CodModelo equals item.CodItem
								 where jit.DatProducao >= DatInicio && jit.DatProducao <= DatFim &&
									 (jit.CodProcesso.StartsWith("02") || jit.CodLinha.Contains("DAT"))
								 group jit by new { jit.CodPlaca, item.DesItem } into g1
								 select new ResumoProducao()
								 {
									 Turno = 1,
									 CodLinha = "",
									 DesLinha = "",
									 CodModelo = g1.Key.CodPlaca,
									 DesModelo = g1.Key.DesItem,
									 Cinescopio = "",
									 QtdPlano = (int)g1.Sum(x => x.QtdProdPM),
									 QtdRealizado = 0,
									 QtdApontado = 0,
									 QtdPlanoVPE = 0,
									 Familia = ""
								 });//.OrderBy(x => new { x.Turno, x.CodModelo });

				var programa = programaQ.ToList();

				var progMes = (from p in dtbSim.tbl_ProgDiaPlaca
							   where p.DatProducao >= inicio && p.DatProducao <= fim
							   group p by new { p.CodPlaca }
								   into grupo
							   select new
							   {
								   grupo.Key.CodPlaca,
								   QtdPrograma = grupo.Sum(x => x.QtdProdPM)
							   }).ToList();

				var prodMes = (from p in dtbSim.tbl_ProdDiaPlaca
							   where p.DatProducao >= inicio && p.DatProducao <= fim
							   group p by new { p.CodPlaca }
								   into grupo
							   select new
							   {
								   grupo.Key.CodPlaca,
								   QtdProduzido = grupo.Sum(x => x.QtdProduzida)
							   }).ToList();

				foreach (var prod in producao)
				{
					var pMes = progMes.FirstOrDefault(x => x.CodPlaca == prod.CodModelo);
					var rMes = prodMes.FirstOrDefault(x => x.CodPlaca == prod.CodModelo);
					var pDia = programa.FirstOrDefault(x => x.CodModelo == prod.CodModelo);

					if (rMes != null) prod.QtdProduzidoMes = (int)rMes.QtdProduzido;
					if (pMes != null) prod.QtdPlanoMes = (int?)pMes.QtdPrograma;
					if (pDia != null) prod.QtdPlano = pDia.QtdPlano;
				}

				var naoproduzidos = programa.Where(x => !(producao.Select(p => p.CodModelo)).Contains(x.CodModelo)).ToList();

				foreach (var naop in naoproduzidos)
				{
					var pMes = progMes.FirstOrDefault(x => x.CodPlaca == naop.CodModelo);
					var rMes = prodMes.FirstOrDefault(x => x.CodPlaca == naop.CodModelo);

					if (rMes != null) naop.QtdProduzidoMes = (int)rMes.QtdProduzido;
					if (pMes != null) naop.QtdPlanoMes = (int?)pMes.QtdPrograma;
				}

				producao.AddRange(naoproduzidos);

				return producao;
			}
		}

		#endregion IMC

		#region IAC

		public static List<ResumoProducao> ObterResumoIAC(DateTime DatInicio, DateTime DatFim)
		{
			DateTime inicio = DatInicio.AddDays(-DatInicio.Day + 1);
			DateTime fim = inicio.AddMonths(1).AddDays(-1);

			using (var dtbSim = new DTB_SIMContext())
			{
				var programaQ = (from jit in dtbSim.tbl_ProgDiaPlaca
								 join item in dtbSim.tbl_Item on jit.CodPlaca equals item.CodItem
								 join linha in dtbSim.tbl_BarrasLinha on jit.CodLinha equals linha.CodLinha
								 from prod in dtbSim.tbl_ProdDiaPlaca.Where(x => linha.CodLinhaRel == x.CodLinha &&
									jit.CodPlaca == x.CodPlaca && jit.DatProducao == x.DatProducao &&
									jit.NumTurno == x.NumTurno && jit.FlagTP == x.FlagTP).DefaultIfEmpty()
								 where
									 jit.DatProducao >= DatInicio && jit.DatProducao <= DatFim &&
									 (jit.CodProcesso.StartsWith("01"))
								 select new ResumoProducao()
								 {
									 DatProducao = jit.DatProducao,
									 Turno = jit.NumTurno,
									 CodLinha = jit.CodLinha,
									 DesLinha = linha.DesLinha,
									 CodModelo = jit.CodPlaca,
									 DesModelo = item.DesItem,
									 Cinescopio = "",
									 QtdPlano = (int)jit.QtdProdPM,
									 QtdRealizado = (int)(prod.QtdProduzida ?? 0),
									 QtdApontado = (int)(prod.QtdApontada ?? 0),
									 FlagTp = jit.FlagTP
								 }).OrderBy(x => new { x.Turno, x.CodLinha });

				var programa = programaQ.ToList();

				var producaoQ = (from prod in dtbSim.tbl_ProdDiaPlaca
								 join item in dtbSim.tbl_Item on prod.CodPlaca equals item.CodItem
								 from linha in dtbSim.tbl_BarrasLinha.Where(x => prod.CodLinha == x.CodLinha || prod.CodLinha == x.CodLinhaRel).DefaultIfEmpty()
								 from jit in dtbSim.tbl_ProgDiaPlaca.Where(x => linha.CodLinha == x.CodLinha &&
									prod.CodPlaca == x.CodPlaca && prod.DatProducao == x.DatProducao &&
									prod.NumTurno == x.NumTurno && prod.FlagTP == x.FlagTP).DefaultIfEmpty()
								 where
									 prod.DatProducao >= DatInicio && prod.DatProducao <= DatFim &&
									 (prod.CodProcesso.StartsWith("01")) && jit.CodPlaca == null
								 select new ResumoProducao()
								 {
									 DatProducao = prod.DatProducao,
									 Turno = prod.NumTurno,
									 CodLinha = linha.CodLinha,
									 DesLinha = linha.DesLinha,
									 CodModelo = prod.CodPlaca,
									 DesModelo = item.DesItem,
									 Cinescopio = "",
									 QtdPlano = 0,
									 QtdRealizado = (int)prod.QtdProduzida,
									 QtdApontado = (int)(prod.QtdApontada ?? 0),
									 FlagTp = prod.FlagTP
								 }).OrderBy(x => new { x.Turno, x.CodLinha });
				var producao = producaoQ.ToList();

				programa.AddRange(producao);

				var progMes = (from p in dtbSim.tbl_ProgDiaPlaca
							   where p.DatProducao >= inicio && p.DatProducao <= fim
							   group p by new { p.CodLinha, p.NumTurno, p.CodPlaca }
								   into grupo
							   select new
							   {
								   grupo.Key.CodLinha,
								   grupo.Key.NumTurno,
								   grupo.Key.CodPlaca,
								   QtdPrograma = grupo.Sum(x => x.QtdProdPM)
							   }).ToList();

				var prodMes = (from p in dtbSim.tbl_ProdDiaPlaca
							   where p.DatProducao >= inicio && p.DatProducao <= fim
							   group p by new { p.CodLinha, p.NumTurno, p.CodPlaca }
								   into grupo
							   select new
							   {
								   grupo.Key.CodLinha,
								   grupo.Key.NumTurno,
								   grupo.Key.CodPlaca,
								   QtdProduzido = grupo.Sum(x => x.QtdProduzida)
							   }).ToList();

				var idwContext = new IDWContext();
				idwContext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
				List<v_producaoturno> prodIDW = new List<v_producaoturno>();
				try
				{
					prodIDW = (from a in idwContext.v_producaoturno
							   where a.dt_referencia >= DatInicio && a.dt_referencia <= DatFim &&
									   a.producaobruta > 0 &&
									   //(a.cd_pt.Contains("-2") || a.cd_pt.Contains("DT")) &&
									   a.is_aponGt == 1 &&
									   (a.cd_turno == "1" || a.cd_turno == "2" || a.cd_turno == "3" || a.cd_turno == "0")
							   select a).ToList();
				}
				catch (Exception)
				{
				}

				foreach (var prog in programa)
				{
					var pMes =
						progMes.FirstOrDefault(
							x => x.CodLinha == prog.CodLinha && x.CodPlaca == prog.CodModelo && x.NumTurno == prog.Turno);
					var rMes =
						prodMes.FirstOrDefault(
							x => x.CodLinha == prog.CodLinha && x.CodPlaca == prog.CodModelo && x.NumTurno == prog.Turno);

					var idwDia = prodIDW.FirstOrDefault(x =>
														prog.DatProducao == x.dt_referencia &&
														prog.CodModelo == x.codModelo &&
														prog.Turno.ToString() == x.cd_turno &&
														prog.CodLinha.Trim() == x.cd_gt);

					if (rMes != null) prog.QtdProduzidoMes = (int)rMes.QtdProduzido;
					if (pMes != null) prog.QtdPlanoMes = (int?)pMes.QtdPrograma;
					if (idwDia != null) prog.QtdIDW = (int)(idwDia.producaobruta ?? 0);

					prodIDW.Remove(idwDia);
				}

				foreach (var item in prodIDW)
				{
					var linha = dtbSim.tbl_BarrasLinha.FirstOrDefault(x => x.CodLinha == item.cd_gt);
					var deslinha = linha != null ? linha.DesLinha : "";
					var desModelo = dtbSim.tbl_Item.FirstOrDefault(x => x.CodItem == item.codModelo);

					var desmodelo = desModelo != null ? desModelo.DesItem : "MODELO NÃO ENCONTRADO";

					var pMes =
						progMes.FirstOrDefault(
							x => x.CodLinha == item.cd_gt && x.CodPlaca == item.codModelo && x.NumTurno == int.Parse(item.cd_turno));
					var rMes =
						prodMes.FirstOrDefault(
							x => x.CodLinha == item.cd_gt && x.CodPlaca == item.codModelo && x.NumTurno == int.Parse(item.cd_turno));

					programa.Add(new ResumoProducao()
					{
						CodLinha = item.cd_gt,
						Turno = int.Parse(((item.cd_turno == "0") ? "9" : item.cd_turno)),
						CodModelo = item.codModelo,
						DatProducao = item.dt_referencia ?? DatInicio,
						QtdIDW = (int)(item.producaobruta ?? 0),
						DesLinha = deslinha,
						DesModelo = desmodelo
					});
				}

				return programa;
			}
		}

		public static List<ResumoProducao> ObterResumoIACModelo(DateTime DatInicio, DateTime DatFim, string CodModelo)
		{
			DateTime inicio = DatInicio.AddDays(-DatInicio.Day + 1);
			DateTime fim = inicio.AddMonths(1).AddDays(-1);

			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var producaoQ = (from p in dtbSim.tbl_ProdDiaPlaca
								 join item in dtbSim.tbl_Item on p.CodPlaca equals item.CodItem
								 where
									 p.DatProducao >= DatInicio && p.DatProducao <= DatFim &&
									 (p.CodProcesso.StartsWith("01"))
								 group p by new { p.CodPlaca, p.NumTurno, item.DesItem } into g1
								 select new ResumoProducao()
								 {
									 Turno = g1.Key.NumTurno,
									 CodLinha = "",
									 DesLinha = "",
									 CodModelo = g1.Key.CodPlaca,
									 DesModelo = g1.Key.DesItem,
									 Cinescopio = "",
									 QtdPlano = 0,
									 QtdRealizado = (int)g1.Sum(x => x.QtdProduzida),
									 QtdApontado = (int)g1.Sum(x => x.QtdApontada),
									 QtdPlanoVPE = 0,
									 FlagTp = ""
								 });//.OrderBy(x => new { x.Turno, x.CodModelo });

				var producao = producaoQ.ToList();

				var programaQ = (from jit in dtbSim.tbl_ProgDiaPlaca
								 join item in dtbSim.tbl_Item on jit.CodModelo equals item.CodItem
								 where jit.DatProducao >= DatInicio && jit.DatProducao <= DatFim &&
									 (jit.CodProcesso.StartsWith("01"))
								 group jit by new { jit.CodPlaca, item.DesItem } into g1
								 select new ResumoProducao()
								 {
									 Turno = 1,
									 CodLinha = "",
									 DesLinha = "",
									 CodModelo = g1.Key.CodPlaca,
									 DesModelo = g1.Key.DesItem,
									 Cinescopio = "",
									 QtdPlano = (int)g1.Sum(x => x.QtdProdPM),
									 QtdRealizado = 0,
									 QtdApontado = 0,
									 QtdPlanoVPE = 0,
									 Familia = ""
								 });//.OrderBy(x => new { x.Turno, x.CodModelo });

				var programa = programaQ.ToList();

				var progMes = (from p in dtbSim.tbl_ProgDiaPlaca
							   where p.DatProducao >= inicio && p.DatProducao <= fim
							   group p by new { p.CodPlaca }
								   into grupo
							   select new
							   {
								   grupo.Key.CodPlaca,
								   QtdPrograma = grupo.Sum(x => x.QtdProdPM)
							   }).ToList();

				var prodMes = (from p in dtbSim.tbl_ProdDiaPlaca
							   where p.DatProducao >= inicio && p.DatProducao <= fim
							   group p by new { p.CodPlaca }
								   into grupo
							   select new
							   {
								   grupo.Key.CodPlaca,
								   QtdProduzido = grupo.Sum(x => x.QtdProduzida)
							   }).ToList();

				foreach (var prod in producao)
				{
					var pMes = progMes.FirstOrDefault(x => x.CodPlaca == prod.CodModelo);
					var rMes = prodMes.FirstOrDefault(x => x.CodPlaca == prod.CodModelo);
					var pDia = programa.FirstOrDefault(x => x.CodModelo == prod.CodModelo);

					if (rMes != null) prod.QtdProduzidoMes = (int)rMes.QtdProduzido;
					if (pMes != null) prod.QtdPlanoMes = (int?)pMes.QtdPrograma;
					if (pDia != null) prod.QtdPlano = pDia.QtdPlano;
				}

				var naoproduzidos = programa.Where(x => !(producao.Select(p => p.CodModelo)).Contains(x.CodModelo)).ToList();

				foreach (var naop in naoproduzidos)
				{
					var pMes = progMes.FirstOrDefault(x => x.CodPlaca == naop.CodModelo);
					var rMes = prodMes.FirstOrDefault(x => x.CodPlaca == naop.CodModelo);

					if (rMes != null) naop.QtdProduzidoMes = (int)rMes.QtdProduzido;
					if (pMes != null) naop.QtdPlanoMes = (int?)pMes.QtdPrograma;
				}

				producao.AddRange(naoproduzidos);

				return producao;
			}
		}

		#endregion IAC
	}
}