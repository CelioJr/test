using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBTAItemMap : EntityTypeConfiguration<tbl_CBTAItem>
    {
        public tbl_CBTAItemMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodModelo, t.NumSerieModelo, t.CodModeloPCI, t.NumSeriePCI, t.Fase, t.SubFase, t.Ordem });

            // Properties
            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumSerieModelo)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.CodModeloPCI)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumSeriePCI)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.Fase)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);

            this.Property(t => t.SubFase)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.CodProcesso)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.Ordem)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("tbl_CBTAItem");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.NumSerieModelo).HasColumnName("NumSerieModelo");
            this.Property(t => t.CodModeloPCI).HasColumnName("CodModeloPCI");
            this.Property(t => t.NumSeriePCI).HasColumnName("NumSeriePCI");
            this.Property(t => t.Fase).HasColumnName("Fase");
            this.Property(t => t.SubFase).HasColumnName("SubFase");
            this.Property(t => t.CodProcesso).HasColumnName("CodProcesso");
            this.Property(t => t.Ordem).HasColumnName("Ordem");
            this.Property(t => t.DatEntrada).HasColumnName("DatEntrada");
            this.Property(t => t.DatSaida).HasColumnName("DatSaida");
            this.Property(t => t.TempoCorrido).HasColumnName("TempoCorrido");
            this.Property(t => t.TempoAtravessa).HasColumnName("TempoAtravessa");
            this.Property(t => t.QtdDefeito).HasColumnName("QtdDefeito");
            this.Property(t => t.TempoTecnico).HasColumnName("TempoTecnico");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
			this.Property(t => t.FlgTempoMedio).HasColumnName("FlgTempoMedio");
		}
    }
}
