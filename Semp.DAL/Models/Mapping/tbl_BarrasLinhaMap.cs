using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
	public class tbl_BarrasLinhaMap : EntityTypeConfiguration<tbl_BarrasLinha>
	{
		public tbl_BarrasLinhaMap()
		{
			// Primary Key
			this.HasKey(t => new { t.CodLinha, t.flgFabrica });

			// Properties
			this.Property(t => t.CodLinhaCB)
				.HasMaxLength(4)
                .IsUnicode(false);

			this.Property(t => t.CodLinha)
				.IsRequired()
				.HasMaxLength(8)
                .IsUnicode(false);

			this.Property(t => t.flgFabrica)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.DesLinha)
				.HasMaxLength(50)
                .IsUnicode(false);

			this.Property(t => t.WStation)
				.HasMaxLength(30)
                .IsUnicode(false);

			this.Property(t => t.CodLinhaRel)
				.HasMaxLength(10)
                .IsUnicode(false);

			this.Property(t => t.CodLinhaT1)
				.HasMaxLength(10)
                .IsUnicode(false);

			this.Property(t => t.CodLinhaT2)
				.HasMaxLength(10)
                .IsUnicode(false);

			this.Property(t => t.CodLinhaT3)
				.HasMaxLength(10)
                .IsUnicode(false);

			this.Property(t => t.Setor)
				.HasMaxLength(3)
                .IsUnicode(false);

			this.Property(t => t.Produto)
				.IsFixedLength()
				.HasMaxLength(3)
                .IsUnicode(false);

			this.Property(t => t.CodFam)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			this.Property(t => t.flgSeqLeit)
				.HasMaxLength(2)
                .IsUnicode(false);

			this.Property(t => t.HoraSaiIntervalo1)
				.IsFixedLength()
				.HasMaxLength(5)
                .IsUnicode(false);

			this.Property(t => t.HoraRetIntervalo1)
				.IsFixedLength()
				.HasMaxLength(5)
                .IsUnicode(false);

			this.Property(t => t.HoraSaiIntervalo2)
				.IsFixedLength()
				.HasMaxLength(5)
                .IsUnicode(false);

			this.Property(t => t.HoraRetIntervalo2)
				.IsFixedLength()
				.HasMaxLength(5)
                .IsUnicode(false);

			this.Property(t => t.HoraSaiAlmoco)
				.IsFixedLength()
				.HasMaxLength(5)
                .IsUnicode(false);

			this.Property(t => t.HoraRetAlmoco)
				.IsFixedLength()
				.HasMaxLength(5)
                .IsUnicode(false);

			this.Property(t => t.HoraSaiJanta)
				.IsFixedLength()
				.HasMaxLength(5)
                .IsUnicode(false);

			this.Property(t => t.HoraRetJanta)
				.IsFixedLength()
				.HasMaxLength(5)
                .IsUnicode(false);

			this.Property(t => t.HoraEntrada)
				.IsFixedLength()
				.HasMaxLength(5)
                .IsUnicode(false);

			this.Property(t => t.HoraSaida)
				.IsFixedLength()
				.HasMaxLength(5)
                .IsUnicode(false);

			this.Property(t => t.flgAtivo)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			this.Property(t => t.flgStatus)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			this.Property(t => t.Familia)
				.IsFixedLength()
				.HasMaxLength(6)
                .IsUnicode(false);

			this.Property(t => t.HoraEntradaT2)
				.IsFixedLength()
				.HasMaxLength(5)
                .IsUnicode(false);

			this.Property(t => t.HoraSaidaT2)
				.IsFixedLength()
				.HasMaxLength(5)
                .IsUnicode(false);

			this.Property(t => t.NomeEstacaoExp)
				.HasMaxLength(30)
                .IsUnicode(false);

			// Table & Column Mappings
			this.ToTable("tbl_BarrasLinha");
			this.Property(t => t.CodLinhaCB).HasColumnName("CodLinhaCB");
			this.Property(t => t.CodLinha).HasColumnName("CodLinha");
			this.Property(t => t.flgFabrica).HasColumnName("flgFabrica");
			this.Property(t => t.DesLinha).HasColumnName("DesLinha");            
			this.Property(t => t.WStation).HasColumnName("WStation");
			this.Property(t => t.CodLinhaRel).HasColumnName("CodLinhaRel");
			this.Property(t => t.CodLinhaT1).HasColumnName("CodLinhaT1");
			this.Property(t => t.CodLinhaT2).HasColumnName("CodLinhaT2");
			this.Property(t => t.CodLinhaT3).HasColumnName("CodLinhaT3");
			this.Property(t => t.Setor).HasColumnName("Setor");
			this.Property(t => t.Produto).HasColumnName("Produto");
			this.Property(t => t.CodFam).HasColumnName("CodFam");
			this.Property(t => t.flgSeqLeit).HasColumnName("flgSeqLeit");
			this.Property(t => t.HoraSaiIntervalo1).HasColumnName("HoraSaiIntervalo1");
			this.Property(t => t.HoraRetIntervalo1).HasColumnName("HoraRetIntervalo1");
			this.Property(t => t.HoraSaiIntervalo2).HasColumnName("HoraSaiIntervalo2");
			this.Property(t => t.HoraRetIntervalo2).HasColumnName("HoraRetIntervalo2");
			this.Property(t => t.HoraSaiAlmoco).HasColumnName("HoraSaiAlmoco");
			this.Property(t => t.HoraRetAlmoco).HasColumnName("HoraRetAlmoco");
			this.Property(t => t.HoraSaiJanta).HasColumnName("HoraSaiJanta");
			this.Property(t => t.HoraRetJanta).HasColumnName("HoraRetJanta");
			this.Property(t => t.HoraEntrada).HasColumnName("HoraEntrada");
			this.Property(t => t.HoraSaida).HasColumnName("HoraSaida");
			this.Property(t => t.flgAtivo).HasColumnName("flgAtivo");
			this.Property(t => t.flgStatus).HasColumnName("flgStatus");
			this.Property(t => t.Familia).HasColumnName("Familia");
			this.Property(t => t.HoraEntradaT2).HasColumnName("HoraEntradaT2");
			this.Property(t => t.HoraSaidaT2).HasColumnName("HoraSaidaT2");
			this.Property(t => t.NomeEstacaoExp).HasColumnName("NomeEstacaoExp");
		}
	}
}
