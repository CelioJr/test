using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_RPAMotivoMap : EntityTypeConfiguration<tbl_RPAMotivo>
    {
        public tbl_RPAMotivoMap()
        {
            // Primary Key
            this.HasKey(t => t.CodMotivo);

            // Properties
            this.Property(t => t.CodMotivo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.DesMotivo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(30)
                .IsUnicode(false);

            this.Property(t => t.CodLocalOri)
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.CodLocalDest)
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_RPAMotivo");
            this.Property(t => t.CodMotivo).HasColumnName("CodMotivo");
            this.Property(t => t.DesMotivo).HasColumnName("DesMotivo");
            this.Property(t => t.CodLocalOri).HasColumnName("CodLocalOri");
            this.Property(t => t.CodLocalDest).HasColumnName("CodLocalDest");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
        }
    }
}
