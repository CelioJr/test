using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_HISTORICO_SQL_DTO
    {
        public int SQL_CODIGO { get; set; }
        public string SQL_SCRIPT { get; set; }
        public System.DateTime SQL_DATA { get; set; }
        public string SIS_CODIGO { get; set; }
        public string SQL_TABELA { get; set; }
        public virtual FR_SISTEMA_DTO FR_SISTEMA { get; set; }
    }
}
