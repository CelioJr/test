namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_ft_grupo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_ft_grupo()
        {
            dw_ft_param = new HashSet<dw_ft_param>();
        }

        [Key]
        public long id_ftgrupo { get; set; }

        [StringLength(60)]
        public string cd_ftgrupo { get; set; }

        public int? revisao { get; set; }

        public short? st_ativo { get; set; }

        public DateTime? dt_stativo { get; set; }

        public DateTime? dt_revisao { get; set; }

        [StringLength(256)]
        public string ds_ftgrupo { get; set; }

        public long id_usrStativo { get; set; }

        public long id_usrRevisao { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_ft_param> dw_ft_param { get; set; }
    }
}
