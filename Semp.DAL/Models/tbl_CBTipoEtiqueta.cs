using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBTipoEtiqueta
    {
        public int TipoEtiqueta { get; set; }
        public string DesEtiqueta { get; set; }
    }
}
