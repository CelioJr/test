using System;

namespace SEMP.Model.DTO
{
	public class CBHorarioDTO
	{
		public int idHorario { get; set; }
		public Nullable<int> Turno { get; set; }
		public string HoraInicio { get; set; }
		public string HoraFim { get; set; }
		public string flgViraDia { get; set; }
		public double TempoDisp
		{
			get
			{
				double tempo = 0;
				DateTime Inicio = DateTime.Parse(HoraInicio);
				DateTime Fim = DateTime.Parse(HoraFim);

				if (flgViraDia == "S")
				{
					Fim = Fim.AddDays(1);
				}

				tempo = Fim.Subtract(Inicio).TotalMinutes;

				return tempo;
			}
		}
	}
}
