﻿using System.Drawing;
using RAST.BO.Rastreabilidade;
using SEMP.Model.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MATERIAIS.BO.Estoque;
using SEMP.Model;
using SEMP.Model.VO.Materiais;

namespace SEMP.Areas.Materiais.Controllers
{
	public class ReposicaoController : Controller
	{
		//
		// GET: /Materiais/Reposicao/
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult RelatorioReposicao(string DatInicio, string DatFim, string status)
		{
			List<RelReposicaoDTO> reposicoes = new List<RelReposicaoDTO>();
			List<TimeSpan?> somatorioTempo = new List<TimeSpan?>();			

			reposicoes = Reposicao.ObterReposicoes(DateTime.Parse(DatInicio), DateTime.Parse(DatFim), status);						

			ViewBag.Reposicoes = reposicoes;

			foreach (var item in reposicoes)
			{
				if (item.DatPagamento != null)
				{
					int dif = item.DatPagamento.Value.Day - item.Dia.Day;
					//double dif = (item.DatPagamento - item.Dia).Value.TotalDays;
					DateTime data = new DateTime();
					TimeSpan horaInicial = new TimeSpan(7, 0, 0);
					TimeSpan horaFinal = new TimeSpan(17, 8, 0);
					item.TempoReposicao = new TimeSpan();

					if (dif == 0)
					{
						item.TempoReposicao = TimeSpan.FromHours((item.DatPagamento - item.Dia).Value.TotalHours);
					}
					else
					{
						for (data = item.Dia; DateTime.Compare(data.Date, item.DatPagamento.Value) <= 0; data = (data.Day < DateTime.DaysInMonth(data.Year, data.Month)) ? new DateTime(data.Year,
						data.Month, data.Day + 1, item.DatPagamento.Value.Hour, item.DatPagamento.Value.Minute, item.DatPagamento.Value.Second) :
						new DateTime(item.DatPagamento.Value.Year,
						data.Month + 1, 1, item.DatPagamento.Value.Hour, item.DatPagamento.Value.Minute, item.DatPagamento.Value.Second))
						{
							if (DateTime.Compare(data, item.Dia) == 0)
							{
								item.TempoReposicao = item.TempoReposicao + (horaFinal - data.TimeOfDay);
							}
							else if (DateTime.Compare(data.Date, item.DatPagamento.Value.Date) == 0)
							{
								item.TempoReposicao = item.TempoReposicao + (data.TimeOfDay - horaInicial);
							}
							else
							{
								if (data.DayOfWeek != DayOfWeek.Saturday && data.DayOfWeek != DayOfWeek.Sunday)
								{
									item.TempoReposicao = item.TempoReposicao.Value.Add(new TimeSpan(8, 0, 0));	
								}								
							}
						}
					}
				}
			}

			foreach (var item in reposicoes)
			{
				if (item.DatAprovacao != null)
				{
					int dif = item.DatAprovacao.Value.Day - item.Dia.Day;					
					DateTime data = new DateTime();
					TimeSpan horaInicial = new TimeSpan(7, 0, 0);
					TimeSpan horaFinal = new TimeSpan(17, 8, 0);
					item.TempoAprovacao = new TimeSpan();

					if (dif == 0)
					{
						item.TempoAprovacao = TimeSpan.FromHours((item.DatAprovacao - item.Dia).Value.TotalHours);
					}
					else
					{
						for (data = item.Dia; DateTime.Compare(data.Date, item.DatAprovacao.Value) <= 0; data = (data.Day < DateTime.DaysInMonth(data.Year, data.Month)) ? new DateTime(data.Year,
						data.Month, data.Day + 1, item.DatAprovacao.Value.Hour, item.DatAprovacao.Value.Minute, item.DatAprovacao.Value.Second) :
						new DateTime(item.DatAprovacao.Value.Year,
						data.Month + 1, 1, item.DatAprovacao.Value.Hour, item.DatAprovacao.Value.Minute, item.DatAprovacao.Value.Second))
						{
							if (DateTime.Compare(data, item.Dia) == 0)
							{
								item.TempoAprovacao = item.TempoAprovacao + (horaFinal - data.TimeOfDay);
							}
							else if (DateTime.Compare(data.Date, item.DatAprovacao.Value.Date) == 0)
							{
								item.TempoAprovacao = item.TempoAprovacao + (data.TimeOfDay - horaInicial);
							}
							else
							{
								if (data.DayOfWeek != DayOfWeek.Saturday && data.DayOfWeek != DayOfWeek.Sunday)
								{
									item.TempoAprovacao = item.TempoAprovacao.Value.Add(new TimeSpan(8, 0, 0));
								}
							}
						}
					}
				}
			}

			foreach (var item in reposicoes)
			{
				if (item.DatRecebimento != null)
				{
					int dif = item.DatRecebimento.Value.Day - item.Dia.Day;
					DateTime data = new DateTime();
					TimeSpan horaInicial = new TimeSpan(7, 0, 0);
					TimeSpan horaFinal = new TimeSpan(17, 8, 0);
					item.TempoRecebimento = new TimeSpan();

					if (dif == 0)
					{
						item.TempoRecebimento = TimeSpan.FromHours((item.DatRecebimento - item.Dia).Value.TotalHours);
					}
					else
					{
						for (data = item.Dia; DateTime.Compare(data.Date, item.DatRecebimento.Value) <= 0; data = (data.Day < DateTime.DaysInMonth(data.Year, data.Month)) ? new DateTime(data.Year,
						data.Month, data.Day + 1, item.DatRecebimento.Value.Hour, item.DatRecebimento.Value.Minute, item.DatRecebimento.Value.Second) :
						new DateTime(item.DatRecebimento.Value.Year,
						data.Month + 1, 1, item.DatRecebimento.Value.Hour, item.DatRecebimento.Value.Minute, item.DatRecebimento.Value.Second))
						{
							if (DateTime.Compare(data, item.Dia) == 0)
							{
								item.TempoRecebimento = item.TempoRecebimento + (horaFinal - data.TimeOfDay);
							}
							else if (DateTime.Compare(data.Date, item.DatRecebimento.Value.Date) == 0)
							{
								item.TempoRecebimento = item.TempoRecebimento + (data.TimeOfDay - horaInicial);
							}
							else
							{
								if (data.DayOfWeek != DayOfWeek.Saturday && data.DayOfWeek != DayOfWeek.Sunday)
								{
									item.TempoRecebimento = item.TempoRecebimento.Value.Add(new TimeSpan(8, 0, 0));
								}
							}
						}
					}
				}
			}
			
			return PartialView();
		}

		public ActionResult Painel()
		{

			var dados = Reposicao.ObterDadosPainel();

			return View(dados);
		}

		public ActionResult Gestao()
		{

			//var dados = Reposicao.ObterDadosPainel();
			int fase = Constantes.FASE_MF;

			var resumo = Reposicao.ObterResumoMesResposicao(fase);

			ViewBag.Resumo = resumo;

			return View();
		}

		public ActionResult GestaoIMC()
		{
			int fase = Constantes.FASE_IMC;
			var resumo = Reposicao.ObterResumoMesResposicao(fase);

			ViewBag.Resumo = resumo;

			return View();
		}

		public JsonResult ObterDadosPainel()
		{
			var dados = Reposicao.ObterDadosPainel();

			return Json(dados, JsonRequestBehavior.AllowGet);

		}


		public JsonResult ObterDadosGestao(int fase)
		{

			var gestao = Reposicao.ObterDadosGestao(fase);

			var resumo = Reposicao.ObterResumoMesResposicao(fase);

			var dados = new
			{
				qualidade1 = gestao.FirstOrDefault(x => x.Categoria == PainelGestao.Tipo.Laudo) ?? new PainelGestao(),
				qualidade2 = gestao.FirstOrDefault(x => x.Categoria == PainelGestao.Tipo.Recebimento) ?? new PainelGestao(),
				supm = gestao.FirstOrDefault(x => x.Categoria == PainelGestao.Tipo.Pagamento) ?? new PainelGestao(),
				supm2 = gestao.FirstOrDefault(x => x.Categoria == PainelGestao.Tipo.RecebimentoProducao) ?? new PainelGestao(),
				producao = gestao.FirstOrDefault(x => x.Categoria == PainelGestao.Tipo.Aprovacao) ?? new PainelGestao(),
				qtdRomaneio = resumo.QtdTotalRomaneio,
				qtdItens = resumo.QtdTotalItens,
				qtdAprovada = resumo.QtdAprovada,
				qtdRecebida = resumo.QtdRecebido,
				qtdPaga = resumo.QtdPaga
			};			

			return Json(dados, JsonRequestBehavior.AllowGet);

		}

		public ActionResult ConsultaAPPagamento(string id)
		{
			var ap = Reposicao.ConsultaAPPagamento(id);

			ViewBag.AP = ap;

			return PartialView();
		}

		public ActionResult ConsultaPendencia(string id)
		{

			var reposicoes = Reposicao.ConsultaPendencia(id);			

			ViewBag.Reposicoes = reposicoes;

			return View();
		}

		public ActionResult ConsultaPendencia2(string id, int fase)
		{			
			var reposicoes = Reposicao.ConsultaPendencia2(id, fase);

			return View(reposicoes);
		}

		public ActionResult ConsultaRomaneio()
		{			
			return View();
		}

		public JsonResult ObterRomaneio(string NumRomaneio)
		{
			var romaneio = Reposicao.ObterRomaneio(NumRomaneio);

			return Json(romaneio, JsonRequestBehavior.AllowGet);
		}

	}

}