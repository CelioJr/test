namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_planptgt
    {
        [Key]
        public long id_planptgt { get; set; }

        public long id_plano { get; set; }

        public long? id_gt { get; set; }

        public long? id_pt { get; set; }

        public decimal? linha { get; set; }

        public decimal? coluna { get; set; }

        public short? is_coordenadaRelativa { get; set; }

        [StringLength(40)]
        public string aba { get; set; }

        public virtual om_gt om_gt { get; set; }

        public virtual om_pt om_pt { get; set; }

        public virtual pp_plano pp_plano { get; set; }
    }
}
