namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_CHECK_CQ_LIST
    {
        [Key]
        public int GBR_CHE_ID { get; set; }

        [StringLength(20)]
        public string PALLET { get; set; }

        [StringLength(20)]
        public string PACK { get; set; }

        [StringLength(20)]
        public string IMEI { get; set; }

        [StringLength(20)]
        public string USUARIO { get; set; }

        public DateTime? DATA { get; set; }

        public bool? FECHADOS { get; set; }
    }
}
