using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.DTB_SSA.Models.Mapping
{
    public class viw_STI_ModeloEtiquetaMap : EntityTypeConfiguration<viw_STI_ModeloEtiqueta>
    {
        public viw_STI_ModeloEtiquetaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodItem, t.DesModelo, t.DesResumida });

            // Properties
            this.Property(t => t.CodItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(11);

            this.Property(t => t.CodEstrutura)
                .HasMaxLength(12);

            this.Property(t => t.DesModelo)
                .IsRequired()
                .HasMaxLength(80);

            this.Property(t => t.DesResumida)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.DesDetalhe)
                .HasMaxLength(611);

            this.Property(t => t.CodEAN)
                .IsFixedLength()
                .HasMaxLength(13);

            this.Property(t => t.CodEANCxColetiva)
                .IsFixedLength()
                .HasMaxLength(13);

            // Table & Column Mappings
            this.ToTable("viw_STI_ModeloEtiqueta");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.CodEstrutura).HasColumnName("CodEstrutura");
            this.Property(t => t.DesModelo).HasColumnName("DesModelo");
            this.Property(t => t.DesResumida).HasColumnName("DesResumida");
            this.Property(t => t.DesDetalhe).HasColumnName("DesDetalhe");
            this.Property(t => t.CodEAN).HasColumnName("CodEAN");
            this.Property(t => t.CodEANCxColetiva).HasColumnName("CodEANCxColetiva");
            this.Property(t => t.QtdEmbUni).HasColumnName("QtdEmbUni");
        }
    }
}
