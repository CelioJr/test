﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.SAPConnector.VO
{
	public class BAPI_ORDER_OPERATION1
	{
		public int ROUTING_NO { get; set; }
		public int COUNTER { get; set; }
		public string SEQUENCE_NO { get; set; }
		public int CONF_NO { get; set; }
		public int CONF_CNT { get; set; }
		public string PURCHASE_REQ_NO { get; set; }
		public int PURCHASE_REQ_ITEM { get; set; }
		public string GROUP_COUNTER { get; set; }
		public string TASK_LIST_TYPE { get; set; }
		public string TASK_LIST_GROUP { get; set; }
		public string OPERATION_NUMBER { get; set; }
		public string OPR_CNTRL_KEY { get; set; }
		public string PROD_PLANT { get; set; }
		public string DESCRIPTION { get; set; }
		public string DESCRIPTION2 { get; set; }
		public string STANDARD_VALUE_KEY { get; set; }
		public string ACTIVITY_TYPE_1 { get; set; }
		public string ACTIVITY_TYPE_2 { get; set; }
		public string ACTIVITY_TYPE_3 { get; set; }
		public string ACTIVITY_TYPE_4 { get; set; }
		public string ACTIVITY_TYPE_5 { get; set; }
		public string ACTIVITY_TYPE_6 { get; set; }
		public string UNIT { get; set; }
		public string UNIT_ISO { get; set; }
		public Decimal QUANTITY { get; set; }
		public Decimal SCRAP { get; set; }
		public DateTime EARL_SCHED_START_DATE_EXEC { get; set; }
		public string EARL_SCHED_START_TIME_EXEC { get; set; }
		public DateTime EARL_SCHED_START_DATE_PROC { get; set; }
		public string EARL_SCHED_START_TIME_PROC { get; set; }
		public DateTime EARL_SCHED_START_DATE_TEARD { get; set; }
		public string EARL_SCHED_START_TIME_TEARD { get; set; }
		public DateTime EARL_SCHED_FIN_DATE_EXEC { get; set; }
		public string EARL_SCHED_FIN_TIME_EXEC { get; set; }
		public DateTime LATE_SCHED_START_DATE_EXEC { get; set; }
		public string LATE_SCHED_START_TIME_EXEC { get; set; }
		public DateTime LATE_SCHED_START_DATE_PROC { get; set; }
		public string LATE_SCHED_START_TIME_PROC { get; set; }
		public DateTime LATE_SCHED_START_DATE_TEARD { get; set; }
		public string LATE_SCHED_START_TIME_TEARD { get; set; }
		public DateTime LATE_SCHED_FIN_DATE_EXEC { get; set; }
		public string LATE_SCHED_FIN_TIME_EXEC { get; set; }
		public string WORK_CENTER { get; set; }
		public string WORK_CENTER_TEXT { get; set; }
		public string SYSTEM_STATUS { get; set; }
		public string SUBOPERATION { get; set; }


	}
}
