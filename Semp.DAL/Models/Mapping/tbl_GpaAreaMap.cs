using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_GpaAreaMap : EntityTypeConfiguration<tbl_GpaArea>
    {
        public tbl_GpaAreaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodArea, t.DesArea });

            // Properties
            this.Property(t => t.CodArea)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.DesArea)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_GpaArea");
            this.Property(t => t.CodArea).HasColumnName("CodArea");
            this.Property(t => t.DesArea).HasColumnName("DesArea");
        }
    }
}
