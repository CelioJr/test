namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_LOG_EVENT
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LOG_ID { get; set; }

        public DateTime? LOG_DATA { get; set; }

        [StringLength(8)]
        public string LOG_HORA { get; set; }

        public int? LOG_CODFORM { get; set; }

        [StringLength(100)]
        public string LOG_DESCFORM { get; set; }

        [StringLength(1)]
        public string LOG_OPERACAO { get; set; }

        [StringLength(30)]
        public string LOG_USUARIO { get; set; }

        [StringLength(3)]
        public string LOG_SISTEMA { get; set; }

        [StringLength(200)]
        public string LOG_CHAVE { get; set; }

        [StringLength(128)]
        public string LOG_CHAVECONT { get; set; }

        public string LOG_CONTEUDO { get; set; }

        [StringLength(20)]
        public string LOG_IP { get; set; }
    }
}
