﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.DTO
{
    public class SIMRelatorioRetrabalhoDTO
    {
        public string NS { get; set; }
        public string CodItem { get; set; }
        public string DesItem { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public System.DateTime? DatLeitura { get; set; }

    }
}
