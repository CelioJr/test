namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ZSPP_SISTEMA_ORIGEM
    {
        [Key]
        [StringLength(16)]
        public string PACKNO { get; set; }

        [StringLength(10)]
        public string INTTROW { get; set; }
    }
}
