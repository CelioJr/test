using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_CATEGORIA_DTO
    {
        public FR_CATEGORIA_DTO()
        {
            this.FR_COMPONENTE_CATEGORIA = new List<FR_COMPONENTE_CATEGORIA_DTO>();
            this.FR_RELATORIO_CATEGORIA = new List<FR_RELATORIO_CATEGORIA_DTO>();
            this.FR_SISTEMA_CATEGORIA = new List<FR_SISTEMA_CATEGORIA_DTO>();
            this.FR_FORMULARIO = new List<FR_FORMULARIO_DTO>();
        }

        public int CAT_CODIGO { get; set; }
        public string CAT_NOME { get; set; }
        public virtual ICollection<FR_COMPONENTE_CATEGORIA_DTO> FR_COMPONENTE_CATEGORIA { get; set; }
        public virtual ICollection<FR_RELATORIO_CATEGORIA_DTO> FR_RELATORIO_CATEGORIA { get; set; }
        public virtual ICollection<FR_SISTEMA_CATEGORIA_DTO> FR_SISTEMA_CATEGORIA { get; set; }
        public virtual ICollection<FR_FORMULARIO_DTO> FR_FORMULARIO { get; set; }
    }
}
