﻿using RAST.BO.Rastreabilidade;
using SEMP.Model.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEMP.Areas.Coleta.Controllers
{
	public class RetrabalhoController : Controller
	{
		//
		// GET: /Coleta/CRUDRetrabalho/
		public ActionResult Index()
		{
			return View(Retrabalho.ObterRetrabalhos());
		}

		//
		// GET: /Coleta/CRUDRetrabalho/Create

		public ActionResult Create()
		{
			ViewBag.CodRetrabalho = Retrabalho.UltimoCodRetrabalho() + 1;

			return View();
		}

		//
		// GET: /Coleta/CRUDRetrabalho/Edit/5

		public ActionResult Edit(string codRetrabalho)
		{
			//var retrabalho = Defeito.ObterCadDefeito(codRetrabalho);
			//return View(defeito);

			return View();
		}

		public ActionResult RetrabalhoInfo()
		{
            DateTime diaInicio = new DateTime(2015, 04, 17);
            DateTime diaFim = new DateTime(2015, 04, 28);

			var retrabalho = Retrabalho.ObterRetrabalhoInfoPorData(diaInicio, diaFim);				

			return View(retrabalho);
		}

        public ActionResult RetrabalhoInfoNovo()
        {
            return View();

        }

		[HttpPost]
		public ActionResult RelatorioRetrabalhoExterno(String DatInicio, String DatFim)
		{
			var DatIni = String.IsNullOrEmpty(DatInicio) ? DateTime.Now : DateTime.Parse(DatInicio);
			var DataFim = String.IsNullOrEmpty(DatFim) ? DateTime.Now : DateTime.Parse(DatFim);
			return PartialView(Retrabalho.ObterRetrabalhoSimPorData(DatIni, DataFim));

        }

        public ActionResult RelatorioRetrabalho()
        {
            DateTime diaInicio = new DateTime(2015, 04, 17);
            DateTime diaFim = new DateTime(2015, 04, 28);

            return View(Retrabalho.ListarRetrabalho(diaInicio, diaFim));
        }

        [HttpPost]
        public ActionResult RelatorioRetrabalhoAnalitico(string datInicio, string datFim, string codModelo)
        {
            var dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
            var dFim = dIni.AddDays(1).AddMinutes(-1);

            if (!String.IsNullOrEmpty(datInicio) && !String.IsNullOrEmpty(datFim))
            {
                dIni = DateTime.Parse(datInicio);
                dFim = DateTime.Parse(datFim);
            }

            List<CBRelatorioRetrabalhoDTO> ListaRetrabalho = new List<CBRelatorioRetrabalhoDTO>();

            ListaRetrabalho = Retrabalho.ListarRetrabalho(dIni, dFim);

            if (!String.IsNullOrEmpty(codModelo))
            {
                ListaRetrabalho = ListaRetrabalho.Where(x => x.NumECB.Substring(0, 6) == codModelo).ToList();
            }

            ViewBag.CodModelo = codModelo;

            return View(ListaRetrabalho);

        }

        [HttpPost]
        public ActionResult RelatorioRetrabalhoTabela(string datInicio, string datFim, string codModelo)
        {
            var dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
            var dFim = dIni.AddDays(1).AddMinutes(-1);

            if (!String.IsNullOrEmpty(datInicio) && !String.IsNullOrEmpty(datFim))
            {
                dIni = DateTime.Parse(datInicio);
                dFim = DateTime.Parse(datFim);
            }

            List<CBRelatorioRetrabalhoDTO> ListaRetrabalho = new List<CBRelatorioRetrabalhoDTO>();

            ListaRetrabalho = Retrabalho.ListarRetrabalho(dIni, dFim);

            if (!String.IsNullOrEmpty(codModelo))
            {
                ListaRetrabalho = ListaRetrabalho.Where(x => x.NumECB.Substring(0, 6) == codModelo).ToList();
            }

            ViewBag.CodModelo = codModelo;

            return PartialView(ListaRetrabalho);

        }

        [HttpPost]
        public ActionResult RelatorioRetrabalhoSintetico(string datInicio, string datFim, string codModelo)
        {
            var dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
            var dFim = dIni.AddDays(1).AddMinutes(-1);

            if (!String.IsNullOrEmpty(datInicio) && !String.IsNullOrEmpty(datFim))
           
            {
                dIni = DateTime.Parse(datInicio);
                dFim = DateTime.Parse(datFim);
            } 

            List<CBRelatorioRetrabalhoDTO> ListaRetrabalho = new List<CBRelatorioRetrabalhoDTO>();
            List<CBRelatorioRetrabalhoDTO> ListaRetrabalhoRetorno = new List<CBRelatorioRetrabalhoDTO>();
           
            ListaRetrabalho = Retrabalho.ListarRetrabalho(dIni, dFim);

            if (!String.IsNullOrEmpty(codModelo))
            {
                ListaRetrabalho = ListaRetrabalho.Where(x => x.NumECB.Substring(0, 6) == codModelo).ToList();
            }

            ListaRetrabalho.GroupBy(g => new { Data = g.DatEvento.ToShortDateString(), Modelo = g.Modelo, CodModelo = g.NumECB.Substring(0, 6) }).Select(s =>
                new { Data = s.Key.Data, Modelo = s.Key.Modelo, Qtde = s.Count(), CodModelo = s.Key.CodModelo }).OrderBy(x => x.Data).ToList().ForEach(f => 
                   
              ListaRetrabalhoRetorno.Add(new CBRelatorioRetrabalhoDTO {

                DatEvento = DateTime.Parse(f.Data),
                Modelo = f.Modelo,
                Total = f.Qtde,
                CodModelo = f.CodModelo

            }));


            return PartialView(ListaRetrabalhoRetorno);

        }

	}
}