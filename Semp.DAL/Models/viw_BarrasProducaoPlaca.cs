using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class viw_BarrasProducaoPlaca
    {
        public string CodFab { get; set; }
        public string CodLinha { get; set; }
        public System.DateTime DatReferencia { get; set; }
        public System.DateTime DatLeitura { get; set; }
        public string CodOperador { get; set; }
        public string CodApontador { get; set; }
        public string CodModelo { get; set; }
        public string NumSerieModelo { get; set; }
        public string NomEstacao { get; set; }
        public string NumAP { get; set; }
		public Nullable<int> NumTurno { get; set; }
		public DateTime DatProcessadoSAP { get; set; }
	}
}
