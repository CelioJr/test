using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class rpaitemDTO
    {
        public string NumRPA { get; set; }
        public string CodModelo { get; set; }
        public string NumSerie { get; set; }
        public Nullable<System.DateTime> DatMov { get; set; }
        public string NomUsuSaida { get; set; }
        public string NumRPE { get; set; }
        public Nullable<System.DateTime> DatRet { get; set; }
        public string NomUsuRec { get; set; }
    }
}
