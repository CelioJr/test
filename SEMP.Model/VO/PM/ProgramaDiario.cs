﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.PM
{
	public class ProgramaDiario
	{
		public string CodFam { get; set; }
		public string NomFam { get; set; }
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public string Observacao { get; set; }
		[DisplayFormat(DataFormatString = "{0:#,0}")]
		public decimal? QtdVPE { get; set; }
		[DisplayFormat(DataFormatString = "{0:#,0}")]
		public decimal? QtdPP { get; set; }
		[DisplayFormat(DataFormatString = "{0:#,0}")]
		public decimal? QtdProgDia { get; set; }
		[DisplayFormat(DataFormatString = "{0:#,0}")]
		public decimal? QtdProdDia { get; set; }
		[DisplayFormat(DataFormatString = "{0:#,0}")]
		public decimal? QtdDifDia {
			get { return (QtdProdDia ?? 0) - (QtdProgDia ?? 0); } }
		[DisplayFormat(DataFormatString = "{0:#,0}")]
		public decimal? QtdAcumulada { get; set; }
		[DisplayFormat(DataFormatString = "{0:#,0}")]
		public decimal? QtdDefeitos { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.00}")]
		public decimal? IndDefeitos {
			get
			{
				if (QtdDefeitos != null && QtdProdDia != null && QtdProdDia > 0)
				{
					return (QtdDefeitos/QtdProdDia) * 100;
				}
				return 0;
			} 
		}
		[DisplayFormat(DataFormatString = "{0:0.00}")]
		public decimal? IndEficiencia
		{
			get
			{
				if (QtdProgDia != null && QtdProdDia != null && QtdProgDia > 0)
				{
					return (QtdProdDia / QtdProgDia) * 100;
				}
				return 0;
			}
		}

		public int Ordem { get; set; }

		public int? CodModeloMob { get; set; }

	}
}