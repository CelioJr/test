using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_KcmNacMap : EntityTypeConfiguration<tbl_KcmNac>
    {
        public tbl_KcmNacMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.Data, t.CodItem });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_KcmNac");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.Data).HasColumnName("Data");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.QtdItem).HasColumnName("QtdItem");
            this.Property(t => t.CustoA).HasColumnName("CustoA");
            this.Property(t => t.CustoB).HasColumnName("CustoB");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
        }
    }
}
