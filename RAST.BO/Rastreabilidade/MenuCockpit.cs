﻿using RAST.BO.ApontaProd;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.VO.Rastreabilidade;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAST.BO.Rastreabilidade
{
	public class MenuCockpit
	{
		/// <summary>
		/// Para o gráfico de hora a hora
		/// </summary>
		/// <param name="fase"></param>
		/// <returns></returns>
		public static List<DefeitosHora> Lista(string fase)
		{
			try
			{


				var dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
				var dFim = dIni.AddDays(1).AddMinutes(-1);

				var defeitos = Defeito.ObterDefeitosRel(dIni, dFim, null, fase).Where(x => x.FlgRevisado == 1 && !x.CodCausa.StartsWith("WX")).ToList();

				var producaohora = ProducaoHoraAHoraBO.ObterProducaoLinhaMinutoFase(dIni, fase);
				if (producaohora != null)
				{
					producaohora = producaohora.GroupBy(x => new { x.Hora }).Select(x => new ProducaoLinhaMinuto() { Hora = x.Key.Hora, QtdLeitura = x.Sum(y => y.QtdLeitura) }).ToList();
				}

				var resumo = new List<DefeitosHora>();

				for (var i = 7; i <= DateTime.Now.Hour; i++)
				{
					resumo.Add(new DefeitosHora()
					{
						Hora = i,
						QtdProduzido = 0,
						QtdDefeitos = 0,
						QtdAcumulado = 0,
						QtdPontos = 1
					});
				}

				if (defeitos.Any())
				{
					var modelos = defeitos.GroupBy(g => g.CodModelo)
											.Select(x => new { x.Key, QtdPontos = daoGeral.ObterQuantidadePontos(x.Key) })
											.ToList();

					var resumoDefeitos = defeitos.GroupBy(x => x.DatEvento.Hour).Select(x => new
					{
						Hora = x.Key,
						QtdDefeitos = x.Count(),
						Cor = Color.Brown,
						QtdPontos = modelos.Sum(g => g.QtdPontos)
					}).OrderBy(x => x.Hora).ToList();


					foreach (var hora in resumo)
					{
						var horaAtual = resumoDefeitos.FirstOrDefault(y => y.Hora == hora.Hora);
						var anteriores = resumoDefeitos.Where(x => x.Hora <= hora.Hora).Sum(x => x.QtdDefeitos);

						var produzido = producaohora.FirstOrDefault(x => x.Hora == hora.Hora);
						var produzidoacum = producaohora.Where(x => x.Hora <= hora.Hora).Sum(x => x.QtdLeitura);

						hora.QtdDefeitos = horaAtual != null ? horaAtual.QtdDefeitos : 0;
						hora.QtdProduzido = produzido != null ? produzido.QtdLeitura : 0;

						hora.QtdProduzidoAcum = produzidoacum;

						hora.QtdAcumulado = anteriores;
					}

					return resumo.OrderBy(x => x.Hora).ToList();
				}

			}
			catch (Exception)
			{

				throw;
			}
			return new List<DefeitosHora>();
		}

		/// <summary>
		/// Para o índice de produção (agora em PPM)
		/// </summary>
		/// <returns></returns>
		public static List<DefeitosFabrica> ListaFabrica()
		{

			var dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			var dFim = dIni.AddDays(1).AddMinutes(-1);

			var defeitos = Defeito.ObterDefeitosRel(dIni, dFim).Where(x => x.FlgRevisado == 1 && !x.CodCausa.StartsWith("WX")).ToList();

			var modelos = defeitos.GroupBy(g => new { g.Setor, g.CodModelo })
									.Select(x => new { x.Key.CodModelo, x.Key.Setor, QtdDefeitos = x.Count(), QtdPontos = daoGeral.ObterQuantidadePontos(x.Key.CodModelo) })
									.ToList();

			var retorno = modelos.GroupBy(g => g.Setor).Select(x => new DefeitosFabrica()
			{
				Fabrica = x.Key,
				Data = dIni,
				QtdDefeito = x.Sum(d => d.QtdDefeitos),
				QtdPontos = x.Sum(y => y.QtdPontos),
				IndMeta = 0
			}).ToList();

			var prodIAC = Geral.ObterResumoEmbalagem(dIni, "IAC");
			var prodIMC = Geral.ObterResumoEmbalagem(dIni, "IMC");
			var prodFEC = Geral.ObterResumoEmbalagem(dIni, "FEC");

			retorno.ForEach(x =>
			{
				if (x.Fabrica == "IAC" && prodIAC.Any())
				{
					x.QtdProducao = prodIAC.Sum(q => q.QtdProduzida);
					x.PPMMeta = 65;
				}
				else if (x.Fabrica == "IMC" && prodIMC.Any())
				{
					x.QtdProducao = prodIMC.Sum(q => q.QtdProduzida);
					x.QtdPontos = 1;
					x.PPMMeta = 5000;
				}
				else if (x.Fabrica == "FEC" && prodFEC.Any())
				{
					x.QtdProducao = prodFEC.Sum(q => q.QtdProduzida);
					x.QtdPontos = 1;
					x.PPMMeta = 5000;
				}
			});

			return retorno.OrderBy(x => x.Ordem).ToList();


		}

		/// <summary>
		/// Para o top5
		/// </summary>
		/// <param name="fase"></param>
		/// <returns></returns>
		public static List<DefeitosFabrica> ListaTop5(string fase)
		{

			var dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			var dFim = dIni.AddDays(1).AddMinutes(-1);

			var defeitos = Defeito.ObterDefeitosRel(dIni, dFim, null, fase).Where(x => x.FlgRevisado == 1 && !x.CodCausa.StartsWith("WX")).ToList();

			var retorno = defeitos.GroupBy(g => g.DesDefeito).Select(x => new DefeitosFabrica()
			{
				Fabrica = x.Key,
				Data = dIni,
				QtdDefeito = x.Count()
			}).ToList().OrderByDescending(x => x.QtdDefeito).Take(5);

			return retorno.OrderBy(x => x.Ordem).ToList();


		}

	}
}
