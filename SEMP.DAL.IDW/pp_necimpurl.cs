namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_necimpurl
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pp_necimpurl()
        {
            pp_necimpurllog = new HashSet<pp_necimpurllog>();
        }

        [Key]
        public long id_necimpurl { get; set; }

        [StringLength(255)]
        public string url_fonte { get; set; }

        public long id_necimp { get; set; }

        [StringLength(30)]
        public string mascara { get; set; }

        [StringLength(30)]
        public string aba { get; set; }

        public virtual pp_necimp pp_necimp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_necimpurllog> pp_necimpurllog { get; set; }
    }
}
