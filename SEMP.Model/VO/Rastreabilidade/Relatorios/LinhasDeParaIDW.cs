﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
	public class LinhasDeParaIDW
	{
		public string CodLinhaDE { get; set; }
		public string CodLinhaPARA { get; set; }

	}
}
