
namespace SEMP.DAL.DTB_SSA.Models
{
    using System;

    public partial class tbl_Key_Component_d
    {
        public string Model_Number { get; set; }
        public string Serial_Number { get; set; }
        public string MFG_Part_Number { get; set; }
        public string MFG_Serial_Number { get; set; }
        public System.DateTime Data_Registro { get; set; }
        public int? cod_operador { get; set; }
        public DateTime? Data_Desativacao { get; set; }
        public int? cod_OpeDesat { get; set; }
        public int? dispositivo_id { get; set; }
    
        public virtual tbl_ItemEstoque tbl_ItemEstoque { get; set; }
        public virtual tbl_MFG_Build_d tbl_MFG_Build_d { get; set; }
    }
}
