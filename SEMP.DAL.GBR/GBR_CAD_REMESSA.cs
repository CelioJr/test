namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_CAD_REMESSA
    {
        [Key]
        public int COD_REMESSA { get; set; }

        [StringLength(60)]
        public string REMESSA { get; set; }

        public int? COD_SKU { get; set; }

        public DateTime? DATA_ABERTURA { get; set; }

        public DateTime? DATA_FECHAMENTO { get; set; }

        public bool? ATIVO { get; set; }

        public bool? FORCADO { get; set; }

        public int? NO_MAXIMO_SERIAL { get; set; }

        public bool? SERIAL_ENGINES { get; set; }

        [StringLength(5)]
        public string VARIANTE { get; set; }

        [StringLength(15)]
        public string REMESSA_GAM { get; set; }

        public int? DOCVENDA { get; set; }
    }
}
