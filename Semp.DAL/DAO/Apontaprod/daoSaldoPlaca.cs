﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.Model.DTO;
using SEMP.DAL.Models;
using System.Data.SqlClient;

namespace SEMP.DAL.DAO.Rastreabilidade
{
	public class daoSaldoPlaca
	{
		public static List<PCP201b_19DTO> ObterSaldoPlacas(DateTime Data, String Familia, String codModelo, String codPlaca, int numEstudo)
		{

			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{

				var resumo = dtbSim.Database.SqlQuery<PCP201b_19DTO>(
					"EXEC spc_PCP201b_19 @DatReferencia, @NumEstudo, @Familia, @Modelo, @Placa",
					new SqlParameter("DatReferencia", Data),
					new SqlParameter("NumEstudo", numEstudo),
					new SqlParameter("Familia", Familia),
					new SqlParameter("Modelo", codModelo),
					new SqlParameter("Placa", codPlaca)
					).ToList();

				return resumo;

			}
		}

		/*public static List<spc_SISAPSaldoPlacas> spc_SISAPSaldoPlacas(DateTime Data, String Familia, String codModelo, String codPlaca, int numEstudo)
		{

			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{

				var resumo = dtbSim.Database.SqlQuery<PCP201b_19DTO>(
					"EXEC spc_PCP201b_19 @DatReferencia, @NumEstudo, @Familia, @Modelo, @Placa",
					new SqlParameter("DatReferencia", Data),
					new SqlParameter("NumEstudo", numEstudo),
					new SqlParameter("Familia", Familia),
					new SqlParameter("Modelo", codModelo),
					new SqlParameter("Placa", codPlaca)
					).ToList();

				return resumo;

			}
		}*/

		public static List<LSAAPDTO> ObterAPsMes(String codModelo, DateTime datReferencia){
		
			using (DTB_SIMContext dtbSIM = new DTB_SIMContext()) {
				
				var aps = dtbSIM.tbl_LSAAP.Where(x => x.DatReferencia == datReferencia && x.CodModelo == codModelo && x.CodStatus == "03")
						.Select(y => new LSAAPDTO() {
							CodFab = y.CodFab,
							NumAP = y.NumAP,
							QtdLoteAP = y.QtdLoteAP,
							QtdProduzida = y.QtdProduzida,
							CodStatus = y.CodStatus,
							QtdEmbalado = y.QtdEmbalado,
							Observacao = y.Observacao,
							TipAP = y.TipAP
						}).ToList();

				return aps;

			}

		}

		public static List<LSAATDTO> ObterATsMes(String codModelo, DateTime datReferencia)
		{

			using (DTB_SIMContext dtbSIM = new DTB_SIMContext())
			{

				var aps = dtbSIM.tbl_LSAAT.Where(x => x.CodModelo == codModelo && x.QtdLoteAT != x.QtdProduzida /*&& x.CodStatus == "03"*/)
						.Select(y => new LSAATDTO()
						{
							CodFab = y.CodFab,
							NumAT = y.NumAT,
							QtdLoteAT = y.QtdLoteAT,
							QtdProduzida = y.QtdProduzida,
							CodStatus = y.CodStatus,
							Observacao = y.Observacao
						}).ToList();

				return aps;

			}

		}

	}
}
