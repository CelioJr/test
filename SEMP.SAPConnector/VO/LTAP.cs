﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SEMP.SAPConnector.VO
{
	public class LTAP
	{
		[Display(Description = "PosDepDestino")]
		public string NLPLA { get; set; }
		[Display(Description = "Material")]
		public string MATNR { get; set; }
		[Display(Description = "Unid.dep.origem")]
		public string VLENR { get; set; }
		[Display(Description = "Data confirm.")]
		public DateTime QDATU { get; set; }
		[Display(Description = "Hora confirm.")]
		public string QZEIT { get; set; }
		[Display(Description = "Centro")]
		public string WERKS { get; set; }
		[Display(Description = "Data EM")]
		public DateTime WDATU { get; set; }
		[Display(Description = "Qtd.real origem")]
		public double VISTM { get; set; }
		[Display(Description = "Tp.dep.origem")]
		public string VLTYP { get; set; }
		[Display(Description = "Usuário")]
		public string QNAME { get; set; }
		[Display(Description = "Descrição")]
		public string MAKTX { get; set; }

		public bool? Lido { get; set; }
		public DateTime? DatLido { get; set; }

	}
}
