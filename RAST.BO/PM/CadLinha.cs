﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.PM;
using SEMP.Model.DTO;

namespace RAST.BO.PM
{
	public class CadLinha
	{
		public static List<LinhaDTO> ObterLinhas()
		{
			return daoCadLinha.ObterLinhas();
		}

		public static LinhaDTO ObterLinha(string codLinha)
		{
			return daoCadLinha.ObterLinha(codLinha);
		}

		public static void GravarLinha(LinhaDTO linha)
		{
			daoCadLinha.GravarLinha(linha);
		}

		public static void RemoverLinha(string codLinha)
		{
			daoCadLinha.RemoverLinha(codLinha);
		}
	}
}
