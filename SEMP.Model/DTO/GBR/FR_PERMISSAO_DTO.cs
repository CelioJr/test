using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_PERMISSAO_DTO
    {
        public int PER_CODIGO { get; set; }
        public int GRP_CODIGO { get; set; }
        public string SIS_CODIGO { get; set; }
        public Nullable<int> REL_CODIGO { get; set; }
        public Nullable<int> FRM_CODIGO { get; set; }
        public Nullable<int> COM_CODIGO { get; set; }
        public Nullable<int> MNU_CODIGO { get; set; }
        public string PER_ADICIONAR { get; set; }
        public string PER_EXCLUIR { get; set; }
        public string PER_EDITAR { get; set; }
        public string PER_VISUALIZAR { get; set; }
        public string PER_HABILITADO { get; set; }
        public virtual FR_FORMULARIO_DTO FR_FORMULARIO { get; set; }
        public virtual FR_GRUPO_DTO FR_GRUPO { get; set; }
        public virtual FR_RELATORIO_DTO FR_RELATORIO { get; set; }
    }
}
