using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBCadDefeitoMap : EntityTypeConfiguration<tbl_CBCadDefeito>
    {
        public tbl_CBCadDefeitoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.CodDefeito });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodDefeito)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(4)
                .IsUnicode(false);

            this.Property(t => t.DesDefeito)
                .HasMaxLength(60)
                .IsUnicode(false);

            this.Property(t => t.DesVerificacao)
                .HasMaxLength(100)
                .IsUnicode(false);

            this.Property(t => t.flgAtivo)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_CBCadDefeito");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.CodDefeito).HasColumnName("CodDefeito");
            this.Property(t => t.DesDefeito).HasColumnName("DesDefeito");
            this.Property(t => t.DesVerificacao).HasColumnName("DesVerificacao");
            this.Property(t => t.flgIAC).HasColumnName("flgIAC");
            this.Property(t => t.flgIMC).HasColumnName("flgIMC");
            this.Property(t => t.flgFEC).HasColumnName("flgFEC");
            this.Property(t => t.flgLCM).HasColumnName("flgLCM");
            this.Property(t => t.flgAtivo).HasColumnName("flgAtivo");
        }
    }
}
