using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_BarrasProducaoPlacaMap : EntityTypeConfiguration<tbl_BarrasProducaoPlaca>
    {
        public tbl_BarrasProducaoPlacaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.CodLinha, t.DatReferencia, t.DatLeitura, t.CodOperador, t.CodApontador, t.CodModelo, t.NumSerieModelo });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.CodLinha)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8);

            this.Property(t => t.CodOperador)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8);

            this.Property(t => t.CodApontador)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8);

            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6);

            this.Property(t => t.NumSerieModelo)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.NomEstacao)
                .HasMaxLength(30);

            this.Property(t => t.NumAP)
                .IsFixedLength()
                .HasMaxLength(6);

            // Table & Column Mappings
            this.ToTable("tbl_BarrasProducaoPlaca");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.DatReferencia).HasColumnName("DatReferencia");
            this.Property(t => t.DatLeitura).HasColumnName("DatLeitura");
            this.Property(t => t.CodOperador).HasColumnName("CodOperador");
            this.Property(t => t.CodApontador).HasColumnName("CodApontador");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.NumSerieModelo).HasColumnName("NumSerieModelo");
            this.Property(t => t.NomEstacao).HasColumnName("NomEstacao");
            this.Property(t => t.NumAP).HasColumnName("NumAP");
			this.Property(t => t.NumTurno).HasColumnName("NumTurno");
        }
    }
}
