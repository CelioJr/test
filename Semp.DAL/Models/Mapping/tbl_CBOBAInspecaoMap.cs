using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBOBAInspecaoMap : EntityTypeConfiguration<tbl_CBOBAInspecao>
    {
		public tbl_CBOBAInspecaoMap()
		{
			// Primary Key
			this.HasKey(t => new { t.NumECB, t.CodLinha, t.NumPosto, t.CodFab, t.NumRPA });

			// Properties
			this.Property(t => t.NumECB)
				.IsRequired()
				.HasMaxLength(20)
                .IsUnicode(false);

			this.Property(t => t.CodLinha)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.NumPosto)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.CodFab)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(2)
                .IsUnicode(false);

			this.Property(t => t.NumRPA)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(6)
                .IsUnicode(false);

			this.Property(t => t.CodPallet)
				.HasMaxLength(6)
                .IsUnicode(false);

			this.Property(t => t.FlgStatus)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			this.Property(t => t.Observacao)
				.HasMaxLength(300)
                .IsUnicode(false);

			// Table & Column Mappings
			this.ToTable("tbl_CBOBAInspecao");
			this.Property(t => t.NumECB).HasColumnName("NumECB");
			this.Property(t => t.CodLinha).HasColumnName("CodLinha");
			this.Property(t => t.NumPosto).HasColumnName("NumPosto");
			this.Property(t => t.CodFab).HasColumnName("CodFab");
			this.Property(t => t.NumRPA).HasColumnName("NumRPA");
			this.Property(t => t.CodPallet).HasColumnName("CodPallet");
			this.Property(t => t.DatLeitura).HasColumnName("DatLeitura");
			this.Property(t => t.FlgStatus).HasColumnName("FlgStatus");
			this.Property(t => t.Observacao).HasColumnName("Observacao");
		}
	}
}
