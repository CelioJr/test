using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_REGRAS_BANCO_DTO
    {
        public int REG_COD { get; set; }
        public string BAN_SCRIPT { get; set; }
        public Nullable<System.DateTime> BAN_COMPILACAO { get; set; }
    }
}
