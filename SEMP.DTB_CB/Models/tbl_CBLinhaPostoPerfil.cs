using System;
using System.Collections.Generic;

namespace SEMP.DTB_CB.Models
{
    public partial class tbl_CBLinhaPostoPerfil
    {
        public string CodModelo { get; set; }
        public int CodLinha { get; set; }
        public int NumPosto { get; set; }
        public string FlgAtivo { get; set; }
        public int NumSeq { get; set; }
        public Nullable<bool> flgObrigatorio { get; set; }
        public virtual tbl_CBLinha tbl_CBLinha { get; set; }
    }
}
