namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_HOUR_PALLET")]
    public partial class ALC_HOUR_PALLET
    {
        [Key]
        [StringLength(50)]
        public string idHour { get; set; }

        public DateTime? dtInput { get; set; }

        public int? quantity { get; set; }

        public int? line_COD_LINHAS { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? idinput { get; set; }
    }
}
