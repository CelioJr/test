namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_contato
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_contato()
        {
            om_garantia = new HashSet<om_garantia>();
        }

        [Key]
        public long id_contato { get; set; }

        [StringLength(100)]
        public string nm_contato { get; set; }

        [StringLength(256)]
        public string obs { get; set; }

        [StringLength(20)]
        public string telefone1 { get; set; }

        [StringLength(20)]
        public string telefone2 { get; set; }

        [StringLength(20)]
        public string telefone3 { get; set; }

        [StringLength(256)]
        public string email1 { get; set; }

        [StringLength(256)]
        public string email2 { get; set; }

        [StringLength(256)]
        public string email3 { get; set; }

        public long id_cliente { get; set; }

        public virtual pp_cliente pp_cliente { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_garantia> om_garantia { get; set; }
    }
}
