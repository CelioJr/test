using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEMP.DAL.Models
{
	public partial class tbl_CBLinhaPostoPerfil
	{
		public string CodModelo { get; set; }
		public int CodLinha { get; set; }
		public int NumPosto { get; set; }
		public string FlgAtivo { get; set; }
		public int NumSeq { get; set; }
		public Nullable<bool> flgObrigatorio { get; set; }
		public Nullable<decimal> TempoCiclo { get; set; }
		[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public Nullable<decimal> AparelhosMinuto { get; set; }
		public Nullable<int> QtdJigs { get; set; }
		public string FlgGargalo { get; set; }
	}
}
