using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_ProgMesVPEMap : EntityTypeConfiguration<tbl_ProgMesVPE>
    {
        public tbl_ProgMesVPEMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.DatProducao, t.CodLinha, t.CodModelo });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodLinha)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.Usuario)
                .IsFixedLength()
                .HasMaxLength(25)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_ProgMesVPE");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.DatProducao).HasColumnName("DatProducao");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.QtdProdPm).HasColumnName("QtdProdPm");
            this.Property(t => t.Usuario).HasColumnName("Usuario");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
            this.Property(t => t.rowguid8).HasColumnName("rowguid8");
        }
    }
}
