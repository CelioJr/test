namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_PROPRIEDADE
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FRM_CODIGO { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int COM_CODIGO { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(100)]
        public string PRO_NOME { get; set; }

        [StringLength(8000)]
        public string PRO_VALOR { get; set; }

        public virtual FR_COMPONENTE FR_COMPONENTE { get; set; }
    }
}
