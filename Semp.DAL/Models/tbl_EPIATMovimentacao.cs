using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_EPIATMovimentacao
    {
        public string CodFab { get; set; }
        public string CodMovimentacao { get; set; }
        public string DocInterno1 { get; set; }
        public string DocInterno2 { get; set; }
        public string CodItem { get; set; }
        public string CodTra { get; set; }
        public string CodLocal { get; set; }
        public Nullable<System.DateTime> DatTra { get; set; }
        public string DocExterno { get; set; }
        public Nullable<decimal> QtdTra { get; set; }
        public Nullable<System.DateTime> DatDig { get; set; }
        public Nullable<decimal> QtdRecebida { get; set; }
        public Nullable<System.DateTime> DatRecebida { get; set; }
        public string NomUsuario { get; set; }
        public string NomEstacao { get; set; }
        public string CodGalpao { get; set; }
        public string CodRua { get; set; }
        public string CodBloco { get; set; }
        public string CodApto { get; set; }
        public string SubApto { get; set; }
        public string CodNivel { get; set; }
        public string SubNivel { get; set; }
        public string CodLocalAP { get; set; }
        public string CodModeloAP { get; set; }
        public string AgeExterno { get; set; }
        public Nullable<decimal> QtdLibRec { get; set; }
    }
}
