using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_ANYTHING_DTO
    {
        public int id_anything { get; set; }
        public string description { get; set; }
        public string key_value { get; set; }
        public Nullable<int> next_value { get; set; }
    }
}
