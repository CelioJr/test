namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_folhasetup
    {
        [Key]
        public long id_folhasetup { get; set; }

        public decimal? seg_setup { get; set; }

        public long id_folhasaindo { get; set; }

        public long id_folhaentrando { get; set; }

        public virtual dw_folha dw_folha { get; set; }

        public virtual dw_folha dw_folha1 { get; set; }
    }
}
