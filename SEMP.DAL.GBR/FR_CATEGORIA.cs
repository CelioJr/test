namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_CATEGORIA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FR_CATEGORIA()
        {
            FR_COMPONENTE_CATEGORIA = new HashSet<FR_COMPONENTE_CATEGORIA>();
            FR_RELATORIO_CATEGORIA = new HashSet<FR_RELATORIO_CATEGORIA>();
            FR_SISTEMA_CATEGORIA = new HashSet<FR_SISTEMA_CATEGORIA>();
            FR_FORMULARIO = new HashSet<FR_FORMULARIO>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CAT_CODIGO { get; set; }

        [Required]
        [StringLength(60)]
        public string CAT_NOME { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_COMPONENTE_CATEGORIA> FR_COMPONENTE_CATEGORIA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_RELATORIO_CATEGORIA> FR_RELATORIO_CATEGORIA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_SISTEMA_CATEGORIA> FR_SISTEMA_CATEGORIA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_FORMULARIO> FR_FORMULARIO { get; set; }
    }
}
