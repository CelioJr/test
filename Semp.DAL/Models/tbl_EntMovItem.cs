using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
	public partial class tbl_EntMovItem
	{
		public string CodItem { get; set; }
		public Nullable<decimal> QtdItem { get; set; }
		public int NumRomaneio { get; set; }
		public string CodModelo { get; set; }
		public string CodDefeito { get; set; }
		public string CodCausa { get; set; }
		public string OrigemDefeito { get; set; }
		public string DetalheDefeito { get; set; }
		public string NomResp { get; set; }
		public string flgDevolvido { get; set; }
		public string DesDevMotivo { get; set; }
		public string NumGrim { get; set; }
		public string FlgCritico { get; set; }
		public string FlgNecReposicao { get; set; }
	}
}
