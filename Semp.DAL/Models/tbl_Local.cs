﻿using System;

namespace SEMP.DAL.Models
{
	public class tbl_Local
	{
		public string CodLocal { get; set; }
		public string CodFab { get; set; }
		public string DesLocal { get; set; }
		public string FlgEndereco { get; set; }
		public string FlgAtivo { get; set; }
		public string FlgContabil { get; set; }
		public string FlgDisponivel { get; set; }
		public string FlgProdAcabado { get; set; }
		public Guid Rowguid { get; set; }

	}
}
