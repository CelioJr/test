namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.TEST_ALCATEL")]
    public partial class TEST_ALCATEL
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_TEST { get; set; }

        [StringLength(255)]
        public string TEST_NAME { get; set; }
    }
}
