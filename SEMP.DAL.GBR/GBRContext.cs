namespace SEMP.DAL.GBR
{
	using System.Data.Entity;

	public partial class GBRContext : DbContext
	{
		public GBRContext()
			: base("name=GBRContext")
		{
		}

		public virtual DbSet<alc_lot_list> alc_lot_list { get; set; }
		public virtual DbSet<FR_ACAO> FR_ACAO { get; set; }
		public virtual DbSet<FR_ACAOCOMPONENTE> FR_ACAOCOMPONENTE { get; set; }
		public virtual DbSet<FR_ACAOPARAMETRO> FR_ACAOPARAMETRO { get; set; }
		public virtual DbSet<FR_ACPTIPO> FR_ACPTIPO { get; set; }
		public virtual DbSet<FR_CAMPO> FR_CAMPO { get; set; }
		public virtual DbSet<FR_CATEGORIA> FR_CATEGORIA { get; set; }
		public virtual DbSet<FR_COMPILADOR> FR_COMPILADOR { get; set; }
		public virtual DbSet<FR_COMPILADOR_DATABASE> FR_COMPILADOR_DATABASE { get; set; }
		public virtual DbSet<FR_COMPONENTE> FR_COMPONENTE { get; set; }
		public virtual DbSet<FR_COMPONENTE_CATEGORIA> FR_COMPONENTE_CATEGORIA { get; set; }
		public virtual DbSet<FR_CONFIGURACAO> FR_CONFIGURACAO { get; set; }
		public virtual DbSet<FR_CONSULTA_AVANCADA> FR_CONSULTA_AVANCADA { get; set; }
		public virtual DbSet<FR_DATABASE> FR_DATABASE { get; set; }
		public virtual DbSet<FR_DEP_FORMULARIO_CAMPO> FR_DEP_FORMULARIO_CAMPO { get; set; }
		public virtual DbSet<FR_DEP_FORMULARIO_FORMULARIO> FR_DEP_FORMULARIO_FORMULARIO { get; set; }
		public virtual DbSet<FR_DEP_FORMULARIO_REGRA> FR_DEP_FORMULARIO_REGRA { get; set; }
		public virtual DbSet<FR_DEP_FORMULARIO_RELATORIO> FR_DEP_FORMULARIO_RELATORIO { get; set; }
		public virtual DbSet<FR_DEP_FORMULARIO_TABELA> FR_DEP_FORMULARIO_TABELA { get; set; }
		public virtual DbSet<FR_DEP_REGRA_CAMPO> FR_DEP_REGRA_CAMPO { get; set; }
		public virtual DbSet<FR_DEP_REGRA_COMPONENTE> FR_DEP_REGRA_COMPONENTE { get; set; }
		public virtual DbSet<FR_DEP_REGRA_FORMULARIO> FR_DEP_REGRA_FORMULARIO { get; set; }
		public virtual DbSet<FR_DEP_REGRA_FUNCAO> FR_DEP_REGRA_FUNCAO { get; set; }
		public virtual DbSet<FR_DEP_REGRA_REGRA> FR_DEP_REGRA_REGRA { get; set; }
		public virtual DbSet<FR_DEP_REGRA_RELATORIO> FR_DEP_REGRA_RELATORIO { get; set; }
		public virtual DbSet<FR_DEP_REGRA_TABELA> FR_DEP_REGRA_TABELA { get; set; }
		public virtual DbSet<FR_DIAGRAMA> FR_DIAGRAMA { get; set; }
		public virtual DbSet<FR_DOC_APROVACAO> FR_DOC_APROVACAO { get; set; }
		public virtual DbSet<FR_DOC_ATOR> FR_DOC_ATOR { get; set; }
		public virtual DbSet<FR_DOC_BENEFICIO> FR_DOC_BENEFICIO { get; set; }
		public virtual DbSet<FR_DOC_CASO_USO> FR_DOC_CASO_USO { get; set; }
		public virtual DbSet<FR_DOC_CASO_USO_ATOR> FR_DOC_CASO_USO_ATOR { get; set; }
		public virtual DbSet<FR_DOC_CASO_USO_EXTENSAO> FR_DOC_CASO_USO_EXTENSAO { get; set; }
		public virtual DbSet<FR_DOC_CASO_USO_GENERALIZACAO> FR_DOC_CASO_USO_GENERALIZACAO { get; set; }
		public virtual DbSet<FR_DOC_CASO_USO_INCLUSAO> FR_DOC_CASO_USO_INCLUSAO { get; set; }
		public virtual DbSet<FR_DOC_DIAG_CONT_CAS_USO_ATO> FR_DOC_DIAG_CONT_CAS_USO_ATO { get; set; }
		public virtual DbSet<FR_DOC_DIAGRAMA_CONTEXTO> FR_DOC_DIAGRAMA_CONTEXTO { get; set; }
		public virtual DbSet<FR_DOC_FORMULARIO> FR_DOC_FORMULARIO { get; set; }
		public virtual DbSet<FR_DOC_FORMULARIO_IMAGEM> FR_DOC_FORMULARIO_IMAGEM { get; set; }
		public virtual DbSet<FR_DOC_HIPOTESE_TRABALHO> FR_DOC_HIPOTESE_TRABALHO { get; set; }
		public virtual DbSet<FR_DOC_INTERFACE> FR_DOC_INTERFACE { get; set; }
		public virtual DbSet<FR_DOC_INTERPRETACAO_CAMPO> FR_DOC_INTERPRETACAO_CAMPO { get; set; }
		public virtual DbSet<FR_DOC_LIMITE> FR_DOC_LIMITE { get; set; }
		public virtual DbSet<FR_DOC_MATERIAL_REFERENCIA> FR_DOC_MATERIAL_REFERENCIA { get; set; }
		public virtual DbSet<FR_DOC_MODO_OPERACAO> FR_DOC_MODO_OPERACAO { get; set; }
		public virtual DbSet<FR_DOC_PRINCIPAL> FR_DOC_PRINCIPAL { get; set; }
		public virtual DbSet<FR_DOC_REGRAS_IMAGEM> FR_DOC_REGRAS_IMAGEM { get; set; }
		public virtual DbSet<FR_DOC_RELATORIO> FR_DOC_RELATORIO { get; set; }
		public virtual DbSet<FR_DOC_REQUISITO> FR_DOC_REQUISITO { get; set; }
		public virtual DbSet<FR_DOC_RESTRICAO> FR_DOC_RESTRICAO { get; set; }
		public virtual DbSet<FR_DOC_VERSAO> FR_DOC_VERSAO { get; set; }
		public virtual DbSet<FR_FONTEDADOS> FR_FONTEDADOS { get; set; }
		public virtual DbSet<FR_FORMULARIO> FR_FORMULARIO { get; set; }
		public virtual DbSet<FR_GRUPO> FR_GRUPO { get; set; }
		public virtual DbSet<FR_HISTORICO_SQL> FR_HISTORICO_SQL { get; set; }
		public virtual DbSet<FR_IDIOMA> FR_IDIOMA { get; set; }
		public virtual DbSet<FR_IMAGEM> FR_IMAGEM { get; set; }
		public virtual DbSet<FR_LOG_EVENT> FR_LOG_EVENT { get; set; }
		public virtual DbSet<FR_MENU> FR_MENU { get; set; }
		public virtual DbSet<FR_OPERADOR> FR_OPERADOR { get; set; }
		public virtual DbSet<FR_OPERANDO> FR_OPERANDO { get; set; }
		public virtual DbSet<FR_PARAMETRO> FR_PARAMETRO { get; set; }
		public virtual DbSet<FR_PERMISSAO> FR_PERMISSAO { get; set; }
		public virtual DbSet<FR_PROFILE_INSTANCE> FR_PROFILE_INSTANCE { get; set; }
		public virtual DbSet<FR_PROFILE_SUMMARY> FR_PROFILE_SUMMARY { get; set; }
		public virtual DbSet<FR_PROFILE_TREE> FR_PROFILE_TREE { get; set; }
		public virtual DbSet<FR_PROPRIEDADE> FR_PROPRIEDADE { get; set; }
		public virtual DbSet<FR_REGRAS> FR_REGRAS { get; set; }
		public virtual DbSet<FR_REGRAS_ATIVIDADES> FR_REGRAS_ATIVIDADES { get; set; }
		public virtual DbSet<FR_REGRAS_BANCO> FR_REGRAS_BANCO { get; set; }
		public virtual DbSet<FR_REGRAS_CATEGORIAS> FR_REGRAS_CATEGORIAS { get; set; }
		public virtual DbSet<FR_REGRAS_FUNCOES> FR_REGRAS_FUNCOES { get; set; }
		public virtual DbSet<FR_REGRAS_FUNCOES_TIPOS> FR_REGRAS_FUNCOES_TIPOS { get; set; }
		public virtual DbSet<FR_REGRAS_TIPOS> FR_REGRAS_TIPOS { get; set; }
		public virtual DbSet<FR_REGRAS_TRIGGERS> FR_REGRAS_TRIGGERS { get; set; }
		public virtual DbSet<FR_RELATORIO> FR_RELATORIO { get; set; }
		public virtual DbSet<FR_RELATORIO_CATEGORIA> FR_RELATORIO_CATEGORIA { get; set; }
		public virtual DbSet<FR_SCHEMA> FR_SCHEMA { get; set; }
		public virtual DbSet<FR_SESSAO> FR_SESSAO { get; set; }
		public virtual DbSet<FR_SISTEMA> FR_SISTEMA { get; set; }
		public virtual DbSet<FR_SISTEMA_CATEGORIA> FR_SISTEMA_CATEGORIA { get; set; }
		public virtual DbSet<FR_TABELA> FR_TABELA { get; set; }
		public virtual DbSet<FR_TAREFA> FR_TAREFA { get; set; }
		public virtual DbSet<FR_TAREFA_TEMPO> FR_TAREFA_TEMPO { get; set; }
		public virtual DbSet<FR_TIPO_EVENT> FR_TIPO_EVENT { get; set; }
		public virtual DbSet<FR_TIPODADO> FR_TIPODADO { get; set; }
		public virtual DbSet<FR_TRADUCAO> FR_TRADUCAO { get; set; }
		public virtual DbSet<FR_TRADUCAO_IDIOMA> FR_TRADUCAO_IDIOMA { get; set; }
		public virtual DbSet<FR_USUARIO> FR_USUARIO { get; set; }
		public virtual DbSet<FR_USUARIO_SISTEMA> FR_USUARIO_SISTEMA { get; set; }
		public virtual DbSet<FR_VERSAO> FR_VERSAO { get; set; }
		public virtual DbSet<GBR_BALANCA> GBR_BALANCA { get; set; }
		public virtual DbSet<GBR_CAD_DAY> GBR_CAD_DAY { get; set; }
		public virtual DbSet<GBR_CAD_ETIQUETA_STRING> GBR_CAD_ETIQUETA_STRING { get; set; }
		public virtual DbSet<GBR_CAD_IMEI> GBR_CAD_IMEI { get; set; }
		public virtual DbSet<GBR_CAD_IMPRESSORA> GBR_CAD_IMPRESSORA { get; set; }
		public virtual DbSet<GBR_CAD_LINEDO> GBR_CAD_LINEDO { get; set; }
		public virtual DbSet<GBR_CAD_LINHAS> GBR_CAD_LINHAS { get; set; }
		public virtual DbSet<GBR_CAD_MODELO> GBR_CAD_MODELO { get; set; }
		public virtual DbSet<GBR_CAD_MONTH> GBR_CAD_MONTH { get; set; }
		public virtual DbSet<GBR_CAD_OP> GBR_CAD_OP { get; set; }
		public virtual DbSet<GBR_CAD_POSTOS> GBR_CAD_POSTOS { get; set; }
		public virtual DbSet<GBR_CAD_REMESSA> GBR_CAD_REMESSA { get; set; }
		public virtual DbSet<GBR_CAD_SKU> GBR_CAD_SKU { get; set; }
		public virtual DbSet<GBR_CAD_SMT_SERIAL> GBR_CAD_SMT_SERIAL { get; set; }
		public virtual DbSet<GBR_CAD_YEAR> GBR_CAD_YEAR { get; set; }
		public virtual DbSet<GBR_CADASTRO_DE_PATH_FT> GBR_CADASTRO_DE_PATH_FT { get; set; }
		public virtual DbSet<GBR_CHECK_CQ_LIST> GBR_CHECK_CQ_LIST { get; set; }
		public virtual DbSet<GBR_CHK_SMT_SERIAL> GBR_CHK_SMT_SERIAL { get; set; }
		public virtual DbSet<GBR_CQTF_LIST> GBR_CQTF_LIST { get; set; }
		public virtual DbSet<GBR_CQTF_LIST_HIST> GBR_CQTF_LIST_HIST { get; set; }
		public virtual DbSet<GBR_CQTF_SEQ> GBR_CQTF_SEQ { get; set; }
		public virtual DbSet<GBR_EVENTOS> GBR_EVENTOS { get; set; }
		public virtual DbSet<GBR_EXPORTAR_SERIAIS_LIST> GBR_EXPORTAR_SERIAIS_LIST { get; set; }
		public virtual DbSet<GBR_FT_TEST> GBR_FT_TEST { get; set; }
		public virtual DbSet<GBR_IMEIS_VENDIDOS> GBR_IMEIS_VENDIDOS { get; set; }
		public virtual DbSet<GBR_IMPORT_ERROR> GBR_IMPORT_ERROR { get; set; }
		public virtual DbSet<GBR_LIST_PACK> GBR_LIST_PACK { get; set; }
		public virtual DbSet<GBR_LIST_PALLET> GBR_LIST_PALLET { get; set; }
		public virtual DbSet<GBR_LISTA_REMESSAS> GBR_LISTA_REMESSAS { get; set; }
		public virtual DbSet<GBR_LOG_DE_TROCA_DE_SERIAL_NO_> GBR_LOG_DE_TROCA_DE_SERIAL_NO_ { get; set; }
		public virtual DbSet<GBR_MOD_POSTOS> GBR_MOD_POSTOS { get; set; }
		public virtual DbSet<GBR_PACK> GBR_PACK { get; set; }
		public virtual DbSet<GBR_PALLET> GBR_PALLET { get; set; }
		public virtual DbSet<GBR_ROMANEIO> GBR_ROMANEIO { get; set; }
		public virtual DbSet<GBR_ROMANEIO_LIST> GBR_ROMANEIO_LIST { get; set; }
		public virtual DbSet<GBR_ROMANIEO_DEBUG> GBR_ROMANIEO_DEBUG { get; set; }
		public virtual DbSet<GBR_SLCB_LIST> GBR_SLCB_LIST { get; set; }
		public virtual DbSet<GBR_SUPERVISOR_PASS> GBR_SUPERVISOR_PASS { get; set; }
		public virtual DbSet<GBR_SYS> GBR_SYS { get; set; }
		public virtual DbSet<GBR_TACS> GBR_TACS { get; set; }
		public virtual DbSet<GBR_VOLUME> GBR_VOLUME { get; set; }
		public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
		public virtual DbSet<TEST_ALCATEL> TEST_ALCATEL { get; set; }
		public virtual DbSet<AA_GEN_GEN_LOG> AA_GEN_GEN_LOG { get; set; }
		public virtual DbSet<FR_PERMISSAO_MAKER> FR_PERMISSAO_MAKER { get; set; }
		public virtual DbSet<GBR_CAD_IMEI_BACK> GBR_CAD_IMEI_BACK { get; set; }
		public virtual DbSet<GBR_CAD_SMT_SERIAL_BACK> GBR_CAD_SMT_SERIAL_BACK { get; set; }
		public virtual DbSet<GBR_LIST_PACK_BACK> GBR_LIST_PACK_BACK { get; set; }
		public virtual DbSet<GBR_LIST_VOLUME> GBR_LIST_VOLUME { get; set; }
		public virtual DbSet<USER_HAS_ROLE> USER_HAS_ROLE { get; set; }
		public virtual DbSet<FR_DICIONARIO_FK_VI> FR_DICIONARIO_FK_VI { get; set; }
		public virtual DbSet<FR_DICIONARIO_VI> FR_DICIONARIO_VI { get; set; }
		public virtual DbSet<FR_DOC_FORMULARIO_COMANDO_VI> FR_DOC_FORMULARIO_COMANDO_VI { get; set; }
		public virtual DbSet<FR_SESSAO_VI> FR_SESSAO_VI { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<alc_lot_list>()
				.Property(e => e.packing)
				.IsUnicode(false);

			modelBuilder.Entity<alc_lot_list>()
				.Property(e => e.id_lote)
				.IsUnicode(false);

			modelBuilder.Entity<alc_lot_list>()
				.Property(e => e.order_id)
				.IsUnicode(false);

			modelBuilder.Entity<FR_ACAO>()
				.Property(e => e.ACO_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_ACAO>()
				.HasMany(e => e.FR_ACAOCOMPONENTE)
				.WithRequired(e => e.FR_ACAO)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_ACAOCOMPONENTE>()
				.Property(e => e.ACC_MOMENTO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_ACAOCOMPONENTE>()
				.Property(e => e.ACC_CONDICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_ACAOCOMPONENTE>()
				.HasMany(e => e.FR_PARAMETRO)
				.WithRequired(e => e.FR_ACAOCOMPONENTE)
				.HasForeignKey(e => new { e.FRM_CODIGO, e.COM_CODIGO, e.ACO_CODIGO, e.ACC_MOMENTO });

			modelBuilder.Entity<FR_ACAOPARAMETRO>()
				.Property(e => e.ACP_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_ACPTIPO>()
				.Property(e => e.ACT_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_ACPTIPO>()
				.HasMany(e => e.FR_ACAOPARAMETRO)
				.WithRequired(e => e.FR_ACPTIPO)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_CAMPO>()
				.Property(e => e.TAB_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_CAMPO>()
				.Property(e => e.CMP_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_CAMPO>()
				.Property(e => e.CMP_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_CAMPO>()
				.Property(e => e.CMP_VALORPADRAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_CATEGORIA>()
				.Property(e => e.CAT_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_CATEGORIA>()
				.HasMany(e => e.FR_COMPONENTE_CATEGORIA)
				.WithRequired(e => e.FR_CATEGORIA)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_CATEGORIA>()
				.HasMany(e => e.FR_RELATORIO_CATEGORIA)
				.WithRequired(e => e.FR_CATEGORIA)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_CATEGORIA>()
				.HasMany(e => e.FR_SISTEMA_CATEGORIA)
				.WithRequired(e => e.FR_CATEGORIA)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_COMPILADOR>()
				.Property(e => e.CPL_DESCRITOR)
				.IsUnicode(false);

			modelBuilder.Entity<FR_COMPILADOR>()
				.Property(e => e.CPL_ESPECIFICACAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_COMPILADOR>()
				.HasMany(e => e.FR_COMPILADOR_DATABASE)
				.WithRequired(e => e.FR_COMPILADOR)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_COMPILADOR_DATABASE>()
				.Property(e => e.CPL_DESCRITOR)
				.IsUnicode(false);

			modelBuilder.Entity<FR_COMPILADOR_DATABASE>()
				.Property(e => e.DBA_CODIGO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_COMPILADOR_DATABASE>()
				.Property(e => e.CDB_SINTAXE)
				.IsUnicode(false);

			modelBuilder.Entity<FR_COMPONENTE>()
				.Property(e => e.COM_TIPO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_COMPONENTE>()
				.HasMany(e => e.FR_ACAOCOMPONENTE)
				.WithRequired(e => e.FR_COMPONENTE)
				.HasForeignKey(e => new { e.FRM_CODIGO, e.COM_CODIGO });

			modelBuilder.Entity<FR_COMPONENTE>()
				.HasMany(e => e.FR_PROPRIEDADE)
				.WithRequired(e => e.FR_COMPONENTE)
				.HasForeignKey(e => new { e.FRM_CODIGO, e.COM_CODIGO });

			modelBuilder.Entity<FR_CONFIGURACAO>()
				.Property(e => e.CNF_MAKER_VERSION)
				.IsUnicode(false);

			modelBuilder.Entity<FR_CONSULTA_AVANCADA>()
				.Property(e => e.CAV_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_CONSULTA_AVANCADA>()
				.Property(e => e.CAV_TEXTO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DATABASE>()
				.Property(e => e.DBA_CODIGO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DATABASE>()
				.Property(e => e.DBA_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DATABASE>()
				.HasMany(e => e.FR_COMPILADOR_DATABASE)
				.WithRequired(e => e.FR_DATABASE)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_DEP_FORMULARIO_CAMPO>()
				.Property(e => e.TAB_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DEP_FORMULARIO_CAMPO>()
				.Property(e => e.CMP_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DEP_FORMULARIO_TABELA>()
				.Property(e => e.TAB_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DEP_REGRA_CAMPO>()
				.Property(e => e.TAB_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DEP_REGRA_CAMPO>()
				.Property(e => e.CMP_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DEP_REGRA_TABELA>()
				.Property(e => e.TAB_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DIAGRAMA>()
				.Property(e => e.DGR_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DIAGRAMA>()
				.Property(e => e.DGR_AUTOR)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DIAGRAMA>()
				.Property(e => e.DGR_VERSAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DIAGRAMA>()
				.Property(e => e.DGR_COMENTARIO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DIAGRAMA>()
				.Property(e => e.DGR_LAYOUT)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_APROVACAO>()
				.Property(e => e.APR_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_APROVACAO>()
				.Property(e => e.APR_CARGO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_APROVACAO>()
				.Property(e => e.APR_PARTICIPACAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_ATOR>()
				.Property(e => e.ATO_FREQUENCIA_USO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_ATOR>()
				.Property(e => e.ATO_NIVEL_INSTRUCAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_ATOR>()
				.Property(e => e.ATO_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_ATOR>()
				.Property(e => e.ATO_PROFICIENCIA_APLICACAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_ATOR>()
				.Property(e => e.ATO_PROFICIENCIA_INFORMATICA)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_ATOR>()
				.HasMany(e => e.FR_DOC_CASO_USO_ATOR)
				.WithRequired(e => e.FR_DOC_ATOR)
				.HasForeignKey(e => new { e.ATO_CODIGO_ATIVO, e.DOC_CODIGO })
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_DOC_ATOR>()
				.HasMany(e => e.FR_DOC_HIPOTESE_TRABALHO)
				.WithRequired(e => e.FR_DOC_ATOR)
				.HasForeignKey(e => new { e.ATO_CODIGO, e.DOC_CODIGO });

			modelBuilder.Entity<FR_DOC_BENEFICIO>()
				.Property(e => e.BEN_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_BENEFICIO>()
				.Property(e => e.BEN_VALOR)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_CASO_USO>()
				.Property(e => e.USO_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_CASO_USO>()
				.Property(e => e.USO_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_CASO_USO>()
				.Property(e => e.USO_REQUISITO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_CASO_USO>()
				.Property(e => e.USO_VALIDACAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_CASO_USO>()
				.Property(e => e.USO_CENARIO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_CASO_USO>()
				.HasMany(e => e.FR_DOC_CASO_USO_EXTENSAO)
				.WithRequired(e => e.FR_DOC_CASO_USO)
				.HasForeignKey(e => new { e.DOC_CODIGO, e.USO_CODIGO_EXTENDIDO })
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_DOC_CASO_USO>()
				.HasMany(e => e.FR_DOC_CASO_USO_EXTENSAO1)
				.WithRequired(e => e.FR_DOC_CASO_USO1)
				.HasForeignKey(e => new { e.DOC_CODIGO, e.USO_CODIGO_PRINCIPAL });

			modelBuilder.Entity<FR_DOC_CASO_USO>()
				.HasMany(e => e.FR_DOC_CASO_USO_GENERALIZACAO)
				.WithRequired(e => e.FR_DOC_CASO_USO)
				.HasForeignKey(e => new { e.DOC_CODIGO, e.USO_CODIGO_HERDADO })
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_DOC_CASO_USO>()
				.HasMany(e => e.FR_DOC_CASO_USO_GENERALIZACAO1)
				.WithRequired(e => e.FR_DOC_CASO_USO1)
				.HasForeignKey(e => new { e.DOC_CODIGO, e.USO_CODIGO_HERDADOR })
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_DOC_CASO_USO>()
				.HasMany(e => e.FR_DOC_CASO_USO_INCLUSAO)
				.WithRequired(e => e.FR_DOC_CASO_USO)
				.HasForeignKey(e => new { e.DOC_CODIGO, e.USO_CODIGO_PRINCIPAL });

			modelBuilder.Entity<FR_DOC_CASO_USO_EXTENSAO>()
				.Property(e => e.EXT_CONDICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_DIAGRAMA_CONTEXTO>()
				.Property(e => e.CON_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_FORMULARIO>()
				.Property(e => e.FRM_OBSERVACAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_FORMULARIO_IMAGEM>()
				.Property(e => e.FIM_ABA)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_FORMULARIO_IMAGEM>()
				.Property(e => e.FIM_ABA_NOME_ORIGINAL)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_FORMULARIO_IMAGEM>()
				.Property(e => e.FIM_VERSAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_HIPOTESE_TRABALHO>()
				.Property(e => e.TRA_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_INTERFACE>()
				.Property(e => e.INT_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_INTERFACE>()
				.Property(e => e.INT_TIPO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_INTERPRETACAO_CAMPO>()
				.Property(e => e.CAM_PADRAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_INTERPRETACAO_CAMPO>()
				.Property(e => e.CAM_TIPO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_LIMITE>()
				.Property(e => e.LIM_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_MATERIAL_REFERENCIA>()
				.Property(e => e.REF_BIBLIOGRAFIA)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_MATERIAL_REFERENCIA>()
				.Property(e => e.REF_TIPO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_MODO_OPERACAO>()
				.Property(e => e.OPE_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.Property(e => e.DOC_AUTORIA)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.Property(e => e.SIS_CODIGO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.Property(e => e.DOC_DATA)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.Property(e => e.DOC_EMPRESA)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.Property(e => e.DOC_LOCAL)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.Property(e => e.DOC_OBJETIVO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.Property(e => e.DOC_MISSAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.Property(e => e.DOC_COMPONENTES)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.Property(e => e.DOC_VISAO_GERAL)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.Property(e => e.DOC_CONVENCOES)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.Property(e => e.DOC_OBSERVACOES)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.Property(e => e.DOC_USUARIO_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.HasMany(e => e.FR_DOC_APROVACAO)
				.WithRequired(e => e.FR_DOC_PRINCIPAL)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.HasMany(e => e.FR_DOC_ATOR)
				.WithRequired(e => e.FR_DOC_PRINCIPAL)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.HasMany(e => e.FR_DOC_BENEFICIO)
				.WithRequired(e => e.FR_DOC_PRINCIPAL)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.HasMany(e => e.FR_DOC_CASO_USO)
				.WithRequired(e => e.FR_DOC_PRINCIPAL)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.HasMany(e => e.FR_DOC_FORMULARIO)
				.WithRequired(e => e.FR_DOC_PRINCIPAL)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.HasMany(e => e.FR_DOC_FORMULARIO_IMAGEM)
				.WithRequired(e => e.FR_DOC_PRINCIPAL)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.HasMany(e => e.FR_DOC_INTERFACE)
				.WithRequired(e => e.FR_DOC_PRINCIPAL)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.HasMany(e => e.FR_DOC_INTERPRETACAO_CAMPO)
				.WithRequired(e => e.FR_DOC_PRINCIPAL)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.HasMany(e => e.FR_DOC_LIMITE)
				.WithRequired(e => e.FR_DOC_PRINCIPAL)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.HasMany(e => e.FR_DOC_MATERIAL_REFERENCIA)
				.WithRequired(e => e.FR_DOC_PRINCIPAL)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.HasMany(e => e.FR_DOC_MODO_OPERACAO)
				.WithRequired(e => e.FR_DOC_PRINCIPAL)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.HasMany(e => e.FR_DOC_REGRAS_IMAGEM)
				.WithRequired(e => e.FR_DOC_PRINCIPAL)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.HasMany(e => e.FR_DOC_RELATORIO)
				.WithRequired(e => e.FR_DOC_PRINCIPAL)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.HasMany(e => e.FR_DOC_REQUISITO)
				.WithRequired(e => e.FR_DOC_PRINCIPAL)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.HasMany(e => e.FR_DOC_RESTRICAO)
				.WithRequired(e => e.FR_DOC_PRINCIPAL)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_DOC_PRINCIPAL>()
				.HasMany(e => e.FR_DOC_VERSAO)
				.WithRequired(e => e.FR_DOC_PRINCIPAL)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_DOC_REGRAS_IMAGEM>()
				.Property(e => e.REG_DEPENDENCIA)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_REGRAS_IMAGEM>()
				.Property(e => e.SIS_COD)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_REGRAS_IMAGEM>()
				.Property(e => e.REG_HASH)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_RELATORIO>()
				.Property(e => e.REL_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_RELATORIO>()
				.Property(e => e.REL_OBJETIVO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_REQUISITO>()
				.Property(e => e.REQ_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_REQUISITO>()
				.Property(e => e.REQ_TIPO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_RESTRICAO>()
				.Property(e => e.RES_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_RESTRICAO>()
				.Property(e => e.RES_TIPO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_VERSAO>()
				.Property(e => e.VER_DATA)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_VERSAO>()
				.Property(e => e.VER_REVISADA)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_VERSAO>()
				.Property(e => e.VER_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_FONTEDADOS>()
				.Property(e => e.FNT_CAMPOCHAVE)
				.IsUnicode(false);

			modelBuilder.Entity<FR_FONTEDADOS>()
				.Property(e => e.FNT_TABELA)
				.IsUnicode(false);

			modelBuilder.Entity<FR_FONTEDADOS>()
				.Property(e => e.FNT_SQLSELECT)
				.IsUnicode(false);

			modelBuilder.Entity<FR_FONTEDADOS>()
				.Property(e => e.FNT_SQLINSERT)
				.IsUnicode(false);

			modelBuilder.Entity<FR_FONTEDADOS>()
				.Property(e => e.FNT_SQLUPDATE)
				.IsUnicode(false);

			modelBuilder.Entity<FR_FONTEDADOS>()
				.Property(e => e.FNT_SQLDELETE)
				.IsUnicode(false);

			modelBuilder.Entity<FR_FONTEDADOS>()
				.Property(e => e.FNT_CAMPOINCREMENTO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_FONTEDADOS>()
				.Property(e => e.FNT_CAMPOGRADE)
				.IsUnicode(false);

			modelBuilder.Entity<FR_FONTEDADOS>()
				.Property(e => e.FNT_CAMPOPESQUISA)
				.IsUnicode(false);

			modelBuilder.Entity<FR_FONTEDADOS>()
				.Property(e => e.FNT_SQLDEFAULT)
				.IsUnicode(false);

			modelBuilder.Entity<FR_FONTEDADOS>()
				.Property(e => e.FNT_SQLMASCARA)
				.IsUnicode(false);

			modelBuilder.Entity<FR_FORMULARIO>()
				.Property(e => e.FRM_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_FORMULARIO>()
				.Property(e => e.FRM_TIPO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_FORMULARIO>()
				.Property(e => e.FRM_TIPO_CRIACAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_FORMULARIO>()
				.Property(e => e.FRM_GUID)
				.IsUnicode(false);

			modelBuilder.Entity<FR_FORMULARIO>()
				.Property(e => e.FRM_LOG)
				.IsUnicode(false);

			modelBuilder.Entity<FR_FORMULARIO>()
				.HasMany(e => e.FR_MENU)
				.WithOptional(e => e.FR_FORMULARIO)
				.WillCascadeOnDelete();

			modelBuilder.Entity<FR_FORMULARIO>()
				.HasMany(e => e.FR_PERMISSAO)
				.WithOptional(e => e.FR_FORMULARIO)
				.WillCascadeOnDelete();

			modelBuilder.Entity<FR_FORMULARIO>()
				.HasMany(e => e.FR_CATEGORIA)
				.WithMany(e => e.FR_FORMULARIO)
				.Map(m => m.ToTable("FR_FORMULARIO_CATEGORIA").MapLeftKey("FRM_CODIGO").MapRightKey("CAT_CODIGO"));

			modelBuilder.Entity<FR_FORMULARIO>()
				.HasMany(e => e.FR_SISTEMA)
				.WithMany(e => e.FR_FORMULARIO)
				.Map(m => m.ToTable("FR_FORMULARIO_SISTEMA").MapLeftKey("FRM_CODIGO").MapRightKey("SIS_CODIGO"));

			modelBuilder.Entity<FR_GRUPO>()
				.Property(e => e.SIS_CODIGO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_GRUPO>()
				.Property(e => e.GRP_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_GRUPO>()
				.Property(e => e.GRP_FILTRO_DICIONARIO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_GRUPO>()
				.HasMany(e => e.FR_PERMISSAO)
				.WithRequired(e => e.FR_GRUPO)
				.HasForeignKey(e => new { e.GRP_CODIGO, e.SIS_CODIGO });

			modelBuilder.Entity<FR_HISTORICO_SQL>()
				.Property(e => e.SQL_SCRIPT)
				.IsUnicode(false);

			modelBuilder.Entity<FR_HISTORICO_SQL>()
				.Property(e => e.SIS_CODIGO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_HISTORICO_SQL>()
				.Property(e => e.SQL_TABELA)
				.IsUnicode(false);

			modelBuilder.Entity<FR_IDIOMA>()
				.Property(e => e.IDI_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_IDIOMA>()
				.Property(e => e.IDI_SIGLA)
				.IsUnicode(false);

			modelBuilder.Entity<FR_IDIOMA>()
				.HasMany(e => e.FR_TRADUCAO_IDIOMA)
				.WithRequired(e => e.FR_IDIOMA)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_IMAGEM>()
				.Property(e => e.IMG_GUID)
				.IsUnicode(false);

			modelBuilder.Entity<FR_LOG_EVENT>()
				.Property(e => e.LOG_HORA)
				.IsUnicode(false);

			modelBuilder.Entity<FR_LOG_EVENT>()
				.Property(e => e.LOG_DESCFORM)
				.IsUnicode(false);

			modelBuilder.Entity<FR_LOG_EVENT>()
				.Property(e => e.LOG_OPERACAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_LOG_EVENT>()
				.Property(e => e.LOG_USUARIO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_LOG_EVENT>()
				.Property(e => e.LOG_SISTEMA)
				.IsUnicode(false);

			modelBuilder.Entity<FR_LOG_EVENT>()
				.Property(e => e.LOG_CHAVE)
				.IsUnicode(false);

			modelBuilder.Entity<FR_LOG_EVENT>()
				.Property(e => e.LOG_CHAVECONT)
				.IsUnicode(false);

			modelBuilder.Entity<FR_LOG_EVENT>()
				.Property(e => e.LOG_CONTEUDO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_LOG_EVENT>()
				.Property(e => e.LOG_IP)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<FR_MENU>()
				.Property(e => e.SIS_CODIGO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_MENU>()
				.Property(e => e.MNU_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_MENU>()
				.Property(e => e.MNU_TECLA)
				.IsUnicode(false);

			modelBuilder.Entity<FR_MENU>()
				.Property(e => e.MNU_SEPARADOR)
				.IsUnicode(false);

			modelBuilder.Entity<FR_MENU>()
				.Property(e => e.MNU_GUID)
				.IsUnicode(false);

			modelBuilder.Entity<FR_MENU>()
				.Property(e => e.MNU_TIPO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_OPERADOR>()
				.Property(e => e.OPDR_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_OPERADOR>()
				.Property(e => e.OPDR_TIPO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_OPERADOR>()
				.Property(e => e.OPDR_PARAMETROS)
				.IsUnicode(false);

			modelBuilder.Entity<FR_OPERANDO>()
				.Property(e => e.OPDO_EXPRESSAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PARAMETRO>()
				.Property(e => e.ACC_MOMENTO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PARAMETRO>()
				.Property(e => e.PAR_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PARAMETRO>()
				.Property(e => e.PAR_VALOR)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PERMISSAO>()
				.Property(e => e.SIS_CODIGO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PERMISSAO>()
				.Property(e => e.PER_ADICIONAR)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PERMISSAO>()
				.Property(e => e.PER_EXCLUIR)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PERMISSAO>()
				.Property(e => e.PER_EDITAR)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PERMISSAO>()
				.Property(e => e.PER_VISUALIZAR)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PERMISSAO>()
				.Property(e => e.PER_HABILITADO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PROFILE_INSTANCE>()
				.Property(e => e.PRI_CODIGO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PROFILE_INSTANCE>()
				.Property(e => e.PRI_DESCRIPTION)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PROFILE_INSTANCE>()
				.Property(e => e.SIS_CODIGO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PROFILE_SUMMARY>()
				.Property(e => e.PRS_CODIGO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PROFILE_SUMMARY>()
				.Property(e => e.PRI_CODIGO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PROFILE_SUMMARY>()
				.Property(e => e.PRS_IDENTIFIER)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PROFILE_SUMMARY>()
				.Property(e => e.PRS_PARENT_IDENTIFIER)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PROFILE_SUMMARY>()
				.Property(e => e.PRS_DESCRIPTION)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PROFILE_SUMMARY>()
				.Property(e => e.PRS_TYPE)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PROFILE_SUMMARY>()
				.Property(e => e.PRS_SHARED_TOTAL)
				.HasPrecision(9, 2);

			modelBuilder.Entity<FR_PROFILE_TREE>()
				.Property(e => e.PRT_CODIGO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PROFILE_TREE>()
				.Property(e => e.PRT_CODIGO_PARENT)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PROFILE_TREE>()
				.Property(e => e.PRI_CODIGO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PROFILE_TREE>()
				.Property(e => e.PRT_DESCRIPTION)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PROFILE_TREE>()
				.Property(e => e.PRT_IDENTIFIER)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PROFILE_TREE>()
				.Property(e => e.PRT_TYPE)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PROPRIEDADE>()
				.Property(e => e.PRO_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PROPRIEDADE>()
				.Property(e => e.PRO_VALOR)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS>()
				.Property(e => e.REG_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS>()
				.Property(e => e.REG_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS>()
				.Property(e => e.REG_PARAMS)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS>()
				.Property(e => e.REG_VARIAVEIS)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS>()
				.Property(e => e.REG_PARAMS_OUT)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS>()
				.Property(e => e.REG_INTERFACE)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS>()
				.Property(e => e.REG_SCRIPT)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS>()
				.Property(e => e.REG_COMPILADA)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS>()
				.Property(e => e.REG_HASH)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS>()
				.HasMany(e => e.FR_TAREFA)
				.WithRequired(e => e.FR_REGRAS)
				.HasForeignKey(e => e.REG_CODIGO);

			modelBuilder.Entity<FR_REGRAS_ATIVIDADES>()
				.Property(e => e.ATV_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_ATIVIDADES>()
				.Property(e => e.ATV_NOME_REAL)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_ATIVIDADES>()
				.Property(e => e.ATV_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_ATIVIDADES>()
				.Property(e => e.ATV_PARAMS)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_ATIVIDADES>()
				.Property(e => e.ATV_RETORNO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_ATIVIDADES>()
				.Property(e => e.ATV_COMPATIBILIDADE)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_BANCO>()
				.Property(e => e.BAN_SCRIPT)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_CATEGORIAS>()
				.Property(e => e.CAT_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_FUNCOES>()
				.Property(e => e.FUN_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_FUNCOES>()
				.Property(e => e.FUN_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_FUNCOES>()
				.Property(e => e.FUN_NOME_REAL)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_FUNCOES>()
				.Property(e => e.FUN_PARAMS)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_FUNCOES>()
				.Property(e => e.FUN_RETORNO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_FUNCOES>()
				.Property(e => e.FUN_COMPATIBILIDADE)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_FUNCOES>()
				.Property(e => e.FUN_RESUMO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_FUNCOES>()
				.Property(e => e.FUN_CONTEUDO_SERVIDOR)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_FUNCOES>()
				.Property(e => e.FUN_CONTEUDO_CLIENTE)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_FUNCOES>()
				.Property(e => e.FUN_CONTEUDO_BANCO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_FUNCOES>()
				.Property(e => e.FUN_VERSAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_FUNCOES_TIPOS>()
				.Property(e => e.FTP_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_TIPOS>()
				.Property(e => e.TIP_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_TIPOS>()
				.Property(e => e.TIP_EQUIVALENTE)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_TIPOS>()
				.Property(e => e.TIP_NOME_INTERNO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_TRIGGERS>()
				.Property(e => e.TAB_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_TRIGGERS>()
				.Property(e => e.TAB_PARAMS)
				.IsUnicode(false);

			modelBuilder.Entity<FR_REGRAS_TRIGGERS>()
				.Property(e => e.TAB_SCRIPT)
				.IsUnicode(false);

			modelBuilder.Entity<FR_RELATORIO>()
				.Property(e => e.SIS_CODIGO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_RELATORIO>()
				.Property(e => e.REL_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_RELATORIO>()
				.Property(e => e.REL_CONTEUDO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_RELATORIO>()
				.HasMany(e => e.FR_DOC_RELATORIO)
				.WithRequired(e => e.FR_RELATORIO)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_RELATORIO>()
				.HasMany(e => e.FR_PERMISSAO)
				.WithOptional(e => e.FR_RELATORIO)
				.WillCascadeOnDelete();

			modelBuilder.Entity<FR_SCHEMA>()
				.Property(e => e.SCH_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_SESSAO>()
				.Property(e => e.SES_USUARIO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_SESSAO>()
				.Property(e => e.SES_NOME_USUARIO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_SESSAO>()
				.Property(e => e.SES_NOME_MAQUINA)
				.IsUnicode(false);

			modelBuilder.Entity<FR_SESSAO>()
				.Property(e => e.SES_END_IP)
				.IsUnicode(false);

			modelBuilder.Entity<FR_SESSAO>()
				.Property(e => e.SIS_CODIGO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_SISTEMA>()
				.Property(e => e.SIS_CODIGO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_SISTEMA>()
				.Property(e => e.SIS_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_SISTEMA>()
				.Property(e => e.SIS_SQLDATALIMITE)
				.IsUnicode(false);

			modelBuilder.Entity<FR_SISTEMA>()
				.Property(e => e.SIS_SQLDADOSENTIDADE)
				.IsUnicode(false);

			modelBuilder.Entity<FR_SISTEMA>()
				.Property(e => e.SIS_SQLINFORMACOES)
				.IsUnicode(false);

			modelBuilder.Entity<FR_SISTEMA>()
				.Property(e => e.SIS_CHECK)
				.IsUnicode(false);

			modelBuilder.Entity<FR_SISTEMA>()
				.Property(e => e.SIS_INFORMACAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_SISTEMA>()
				.Property(e => e.SIS_RESUMO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_SISTEMA>()
				.HasMany(e => e.FR_MENU)
				.WithRequired(e => e.FR_SISTEMA)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_SISTEMA_CATEGORIA>()
				.Property(e => e.SIS_CODIGO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_TABELA>()
				.Property(e => e.TAB_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_TABELA>()
				.Property(e => e.TAB_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_TAREFA>()
				.Property(e => e.TRF_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_TAREFA>()
				.Property(e => e.SIS_CODIGO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_TAREFA>()
				.Property(e => e.TRF_ATIVA)
				.IsUnicode(false);

			modelBuilder.Entity<FR_TAREFA>()
				.Property(e => e.TRF_REGRA_PARAMETROS)
				.IsUnicode(false);

			modelBuilder.Entity<FR_TAREFA>()
				.Property(e => e.TRF_TIPO_AGENDAMENTO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_TAREFA_TEMPO>()
				.Property(e => e.TRT_TIPO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_TIPO_EVENT>()
				.Property(e => e.FTE_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_TIPO_EVENT>()
				.Property(e => e.FTE_SIGLA)
				.IsUnicode(false);

			modelBuilder.Entity<FR_TIPODADO>()
				.Property(e => e.TPD_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_TIPODADO>()
				.Property(e => e.TPD_MASCARAFORMATACAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_TIPODADO>()
				.Property(e => e.TPD_MASCARAEDICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_TRADUCAO>()
				.Property(e => e.TRA_ITEM)
				.IsUnicode(false);

			modelBuilder.Entity<FR_TRADUCAO>()
				.Property(e => e.TRA_TEXTO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_TRADUCAO>()
				.Property(e => e.TRA_TIPO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_TRADUCAO>()
				.HasMany(e => e.FR_TRADUCAO_IDIOMA)
				.WithRequired(e => e.FR_TRADUCAO)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<FR_TRADUCAO_IDIOMA>()
				.Property(e => e.TRI_TEXTO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_TRADUCAO_IDIOMA>()
				.Property(e => e.TRI_SITUACAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_TRADUCAO_IDIOMA>()
				.Property(e => e.TRI_HASH)
				.IsUnicode(false);

			modelBuilder.Entity<FR_USUARIO>()
				.Property(e => e.USR_LOGIN)
				.IsUnicode(false);

			modelBuilder.Entity<FR_USUARIO>()
				.Property(e => e.USR_SENHA)
				.IsUnicode(false);

			modelBuilder.Entity<FR_USUARIO>()
				.Property(e => e.USR_ADMINISTRADOR)
				.IsUnicode(false);

			modelBuilder.Entity<FR_USUARIO>()
				.Property(e => e.USR_TIPO_EXPIRACAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_USUARIO>()
				.Property(e => e.USR_NOME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_USUARIO>()
				.Property(e => e.USR_EMAIL)
				.IsUnicode(false);

			modelBuilder.Entity<FR_USUARIO>()
				.HasMany(e => e.FR_GRUPO)
				.WithMany(e => e.FR_USUARIO)
				.Map(m => m.ToTable("FR_USUARIO_GRUPO").MapLeftKey("USR_CODIGO").MapRightKey(new[] { "GRP_CODIGO", "SIS_CODIGO" }));

			modelBuilder.Entity<FR_USUARIO_SISTEMA>()
				.Property(e => e.SIS_CODIGO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_USUARIO_SISTEMA>()
				.Property(e => e.USS_ACESSO_EXTERNO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_USUARIO_SISTEMA>()
				.Property(e => e.USS_ADMINISTRADOR)
				.IsUnicode(false);

			modelBuilder.Entity<FR_USUARIO_SISTEMA>()
				.Property(e => e.USS_ACESSO_MAKER)
				.IsUnicode(false);

			modelBuilder.Entity<FR_USUARIO_SISTEMA>()
				.Property(e => e.USS_CRIAR_FORMULARIO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_USUARIO_SISTEMA>()
				.Property(e => e.USS_CRIAR_RELATORIO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_USUARIO_SISTEMA>()
				.Property(e => e.USS_ACESSAR)
				.IsUnicode(false);

			modelBuilder.Entity<FR_USUARIO_SISTEMA>()
				.Property(e => e.USS_CRIAR_REGRA)
				.IsUnicode(false);

			modelBuilder.Entity<FR_VERSAO>()
				.Property(e => e.VER_TIPO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_VERSAO>()
				.Property(e => e.SIS_CODIGO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_VERSAO>()
				.Property(e => e.VER_DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_VERSAO>()
				.Property(e => e.VER_VERSAO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_BALANCA>()
				.Property(e => e.PESO)
				.HasPrecision(10, 5);

			modelBuilder.Entity<GBR_BALANCA>()
				.Property(e => e.IP)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_DAY>()
				.Property(e => e.LETRAS)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_ETIQUETA_STRING>()
				.Property(e => e.DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_ETIQUETA_STRING>()
				.Property(e => e.STRING)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_ETIQUETA_STRING>()
				.Property(e => e.TIPO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_IMEI>()
				.Property(e => e.IMEI)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_IMPRESSORA>()
				.Property(e => e.IMPRESSORA)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_IMPRESSORA>()
				.Property(e => e.CAMINHO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_LINEDO>()
				.Property(e => e.LETRAS)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_LINHAS>()
				.Property(e => e.NOME_LINHA)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_MODELO>()
				.Property(e => e.SHORTCODE)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_MODELO>()
				.Property(e => e.MODELO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_MODELO>()
				.Property(e => e.ANATEL)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_MODELO>()
				.Property(e => e.NOME_MODELO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_MODELO>()
				.Property(e => e.PCBA)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_MODELO>()
				.Property(e => e.ORIGEM)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_MODELO>()
				.Property(e => e.BLUETOOTH_QD_ID)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_MODELO>()
				.Property(e => e.PI_MAIN)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_MODELO>()
				.Property(e => e.PI_SUB1)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_MODELO>()
				.Property(e => e.PI_SUB2)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_MONTH>()
				.Property(e => e.LETRAS)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_OP>()
				.Property(e => e.OP)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_POSTOS>()
				.Property(e => e.NOME)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_POSTOS>()
				.Property(e => e.CAMINHO_SAV)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_POSTOS>()
				.Property(e => e.CAMINHO_BACKUP)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_REMESSA>()
				.Property(e => e.REMESSA)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_REMESSA>()
				.Property(e => e.VARIANTE)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_REMESSA>()
				.Property(e => e.REMESSA_GAM)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SKU>()
				.Property(e => e.SKU)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SKU>()
				.Property(e => e.CLIENTE)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SKU>()
				.Property(e => e.COR)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SKU>()
				.Property(e => e.SAPCODE)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SKU>()
				.Property(e => e.EAN)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SKU>()
				.Property(e => e.PACK_STRING)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SKU>()
				.Property(e => e.PALLET_STRING)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SKU>()
				.Property(e => e.PESO_MIN)
				.HasPrecision(10, 5);

			modelBuilder.Entity<GBR_CAD_SKU>()
				.Property(e => e.PESO_MAX)
				.HasPrecision(10, 5);

			modelBuilder.Entity<GBR_CAD_SKU>()
				.Property(e => e.PESO_MAX_COLETIVO)
				.HasPrecision(10, 5);

			modelBuilder.Entity<GBR_CAD_SKU>()
				.Property(e => e.PESO_MIN_COLETIVO)
				.HasPrecision(10, 5);

			modelBuilder.Entity<GBR_CAD_SKU>()
				.Property(e => e.PRODUCTCODES)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SKU>()
				.Property(e => e.IMPORTATION_LABEL)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SKU>()
				.Property(e => e.PATH_SAP_FILE_COLETIVA)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SKU>()
				.Property(e => e.PATH_SAP_FILE_PALLET)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SKU>()
				.Property(e => e.PATH_COLMEIA)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SKU>()
				.Property(e => e.PCBA)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SKU>()
				.Property(e => e.BOM)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SKU>()
				.Property(e => e.PATH_SAP_FILE_VOLUME)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SKU>()
				.Property(e => e.VOLUME_STRING)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SKU>()
				.Property(e => e.PATH_REMESSA)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SKU>()
				.Property(e => e.SLCB_HOSTNAME)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SKU>()
				.Property(e => e.SLCB_PATHFILE_ERROS)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL>()
				.Property(e => e.SERIAL)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL>()
				.Property(e => e.USUARIO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL>()
				.Property(e => e.INSPECAO_USER)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL>()
				.Property(e => e.IMEI_1)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL>()
				.Property(e => e.IMEI_2)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL>()
				.Property(e => e.IMEI_3)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL>()
				.Property(e => e.IMEI_4)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL>()
				.Property(e => e.PACKSTRING)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL>()
				.Property(e => e.PALLETSTRING)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL>()
				.Property(e => e.PESO)
				.HasPrecision(11, 3);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL>()
				.Property(e => e.OP)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL>()
				.Property(e => e.COD_VOLUME)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL>()
				.Property(e => e.ORDEM)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL>()
				.Property(e => e.COMPUTER_NAME)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL>()
				.Property(e => e.SKU_SAV)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL>()
				.Property(e => e.ORDEM_IMEI)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL>()
				.Property(e => e.ORDEM_CX)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL>()
				.Property(e => e.ORDEM_COLETIVA)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_YEAR>()
				.Property(e => e.LETRAS)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CADASTRO_DE_PATH_FT>()
				.Property(e => e.POSTO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CADASTRO_DE_PATH_FT>()
				.Property(e => e.CAMINHO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CADASTRO_DE_PATH_FT>()
				.Property(e => e.CAMINHO_BACKUP)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CHECK_CQ_LIST>()
				.Property(e => e.PALLET)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CHECK_CQ_LIST>()
				.Property(e => e.PACK)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CHECK_CQ_LIST>()
				.Property(e => e.IMEI)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CHECK_CQ_LIST>()
				.Property(e => e.USUARIO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CHK_SMT_SERIAL>()
				.Property(e => e.OP)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CHK_SMT_SERIAL>()
				.Property(e => e.SKU)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CHK_SMT_SERIAL>()
				.Property(e => e.MODELO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CHK_SMT_SERIAL>()
				.Property(e => e.SERIAL)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CHK_SMT_SERIAL>()
				.Property(e => e.PI_MAIN)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CHK_SMT_SERIAL>()
				.Property(e => e.PI_SUB1)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CHK_SMT_SERIAL>()
				.Property(e => e.PI_SUB2)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CHK_SMT_SERIAL>()
				.Property(e => e.USUARIO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CHK_SMT_SERIAL>()
				.Property(e => e.SESSAO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CHK_SMT_SERIAL>()
				.Property(e => e.IMEI1)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CQTF_LIST>()
				.Property(e => e.LISTA)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CQTF_LIST_HIST>()
				.Property(e => e.ARQUIVO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CQTF_LIST_HIST>()
				.Property(e => e.IMEI)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CQTF_LIST_HIST>()
				.Property(e => e.CABECALHO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CQTF_LIST_HIST>()
				.Property(e => e.INPUT)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_EVENTOS>()
				.Property(e => e.EVENTO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_EVENTOS>()
				.Property(e => e.USUARIO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_EVENTOS>()
				.Property(e => e.OPERACAO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_EVENTOS>()
				.Property(e => e.DADOS_ANTERIORES)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_EVENTOS>()
				.Property(e => e.DADOS_ATUAIS)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_EVENTOS>()
				.Property(e => e.PACKSTRING)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_EVENTOS>()
				.Property(e => e.PALLET)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_EVENTOS>()
				.Property(e => e.SERIAL)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_EVENTOS>()
				.Property(e => e.IMEI)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_EXPORTAR_SERIAIS_LIST>()
				.Property(e => e.SERIAL)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_EXPORTAR_SERIAIS_LIST>()
				.Property(e => e.USUARIO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_EXPORTAR_SERIAIS_LIST>()
				.Property(e => e.SKU)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_EXPORTAR_SERIAIS_LIST>()
				.Property(e => e.CODETIQUETA)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_FT_TEST>()
				.Property(e => e.SERIAL)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_FT_TEST>()
				.Property(e => e.ARQUIVO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_IMEIS_VENDIDOS>()
				.Property(e => e.IMEI)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_IMEIS_VENDIDOS>()
				.Property(e => e.USUARIO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_IMEIS_VENDIDOS>()
				.Property(e => e.MATERIAL)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_IMEIS_VENDIDOS>()
				.Property(e => e.DEPOSITO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_IMEIS_VENDIDOS>()
				.Property(e => e.CENTRO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_IMEIS_VENDIDOS>()
				.Property(e => e.STATUS)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_IMPORT_ERROR>()
				.Property(e => e.ERRO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_IMPORT_ERROR>()
				.Property(e => e.SERIAL)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_LIST_PACK>()
				.Property(e => e.PACK_STRING)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_LIST_PACK>()
				.Property(e => e.SERIAL)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_LIST_PACK>()
				.Property(e => e.IP)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_LIST_PACK>()
				.Property(e => e.PESO)
				.HasPrecision(11, 3);

			modelBuilder.Entity<GBR_LIST_PACK>()
				.Property(e => e.PPB)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_LIST_PACK>()
				.Property(e => e.ORDEM)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_LIST_PALLET>()
				.Property(e => e.PACK_STRING)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_LIST_PALLET>()
				.Property(e => e.PALLET_STRING)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_LIST_PALLET>()
				.Property(e => e.PESO)
				.HasPrecision(11, 3);

			modelBuilder.Entity<GBR_LISTA_REMESSAS>()
				.Property(e => e.USUARIO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_LISTA_REMESSAS>()
				.Property(e => e.REMESSA)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_LISTA_REMESSAS>()
				.Property(e => e.IMEI)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_LISTA_REMESSAS>()
				.Property(e => e.CODVENDA)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_LISTA_REMESSAS>()
				.Property(e => e.FILE_PATH)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_LOG_DE_TROCA_DE_SERIAL_NO_>()
				.Property(e => e.IMEI_BASE)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_LOG_DE_TROCA_DE_SERIAL_NO_>()
				.Property(e => e.SERIAL_ANTERIOR)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_LOG_DE_TROCA_DE_SERIAL_NO_>()
				.Property(e => e.SERIAL_NOVO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_ROMANEIO>()
				.Property(e => e.DANFE)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_ROMANEIO>()
				.Property(e => e.PALLET)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_ROMANEIO>()
				.Property(e => e.SKU)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_ROMANEIO>()
				.Property(e => e.DB_DOC)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_ROMANEIO>()
				.Property(e => e.DB_COD)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_ROMANEIO>()
				.Property(e => e.SERIAL)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_ROMANEIO>()
				.Property(e => e.IMEI)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_ROMANEIO>()
				.Property(e => e.PACK)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_ROMANEIO_LIST>()
				.Property(e => e.DOC)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_ROMANEIO_LIST>()
				.Property(e => e.COD)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_ROMANIEO_DEBUG>()
				.Property(e => e.DANFE)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_ROMANIEO_DEBUG>()
				.Property(e => e.COD)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_ROMANIEO_DEBUG>()
				.Property(e => e.DOC)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_SLCB_LIST>()
				.Property(e => e.SERIAL)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_SLCB_LIST>()
				.Property(e => e.RESPOSTA_STRING)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_SLCB_LIST>()
				.Property(e => e.SKU)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_SLCB_LIST>()
				.Property(e => e.IP)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_SLCB_LIST>()
				.Property(e => e.LINHA)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_SUPERVISOR_PASS>()
				.Property(e => e.USUARIO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_SUPERVISOR_PASS>()
				.Property(e => e.SENHA)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_SYS>()
				.Property(e => e.PATH_IMPORTATION_BIN)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_SYS>()
				.Property(e => e.BACKUP_IMPORTATION_BIN)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_SYS>()
				.Property(e => e.PATH_IMEIS_VENDIDOS)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_SYS>()
				.Property(e => e.PATH_IMEIS_VENDIDOS_BACKUP)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_TACS>()
				.Property(e => e.TAC)
				.IsUnicode(false);

			modelBuilder.Entity<TEST_ALCATEL>()
				.Property(e => e.TEST_NAME)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PERMISSAO_MAKER>()
				.Property(e => e.PMK_EDITAR)
				.IsUnicode(false);

			modelBuilder.Entity<FR_PERMISSAO_MAKER>()
				.Property(e => e.PMK_EXCLUIR)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_IMEI_BACK>()
				.Property(e => e.IMEI)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL_BACK>()
				.Property(e => e.SERIAL)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL_BACK>()
				.Property(e => e.USUARIO)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL_BACK>()
				.Property(e => e.INSPECAO_USER)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL_BACK>()
				.Property(e => e.IMEI_1)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL_BACK>()
				.Property(e => e.IMEI_2)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL_BACK>()
				.Property(e => e.IMEI_3)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL_BACK>()
				.Property(e => e.IMEI_4)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL_BACK>()
				.Property(e => e.PACKSTRING)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL_BACK>()
				.Property(e => e.PALLETSTRING)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL_BACK>()
				.Property(e => e.PESO)
				.HasPrecision(11, 3);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL_BACK>()
				.Property(e => e.OP)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL_BACK>()
				.Property(e => e.COD_VOLUME)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL_BACK>()
				.Property(e => e.ORDEM)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL_BACK>()
				.Property(e => e.COMPUTER_NAME)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL_BACK>()
				.Property(e => e.SKU_SAV)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL_BACK>()
				.Property(e => e.ORDEM_IMEI)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL_BACK>()
				.Property(e => e.ORDEM_CX)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_CAD_SMT_SERIAL_BACK>()
				.Property(e => e.ORDEM_COLETIVA)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_LIST_PACK_BACK>()
				.Property(e => e.PACK_STRING)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_LIST_PACK_BACK>()
				.Property(e => e.SERIAL)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_LIST_PACK_BACK>()
				.Property(e => e.IP)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_LIST_PACK_BACK>()
				.Property(e => e.PESO)
				.HasPrecision(11, 3);

			modelBuilder.Entity<GBR_LIST_PACK_BACK>()
				.Property(e => e.PPB)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_LIST_PACK_BACK>()
				.Property(e => e.ORDEM)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_LIST_VOLUME>()
				.Property(e => e.VOLUME_STRING)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_LIST_VOLUME>()
				.Property(e => e.PACK_STRING)
				.IsUnicode(false);

			modelBuilder.Entity<GBR_LIST_VOLUME>()
				.Property(e => e.IP)
				.IsUnicode(false);

			modelBuilder.Entity<USER_HAS_ROLE>()
				.Property(e => e.authority)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_FORMULARIO_COMANDO_VI>()
				.Property(e => e.com_botao)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_FORMULARIO_COMANDO_VI>()
				.Property(e => e.com_acao)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_FORMULARIO_COMANDO_VI>()
				.Property(e => e.com_restricao)
				.IsUnicode(false);

			modelBuilder.Entity<FR_DOC_FORMULARIO_COMANDO_VI>()
				.Property(e => e.sis_codigo)
				.IsUnicode(false);

			modelBuilder.Entity<FR_SESSAO_VI>()
				.Property(e => e.SES_USUARIO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_SESSAO_VI>()
				.Property(e => e.SES_NOME_USUARIO)
				.IsUnicode(false);

			modelBuilder.Entity<FR_SESSAO_VI>()
				.Property(e => e.SES_NOME_MAQUINA)
				.IsUnicode(false);

			modelBuilder.Entity<FR_SESSAO_VI>()
				.Property(e => e.SES_END_IP)
				.IsUnicode(false);

			modelBuilder.Entity<FR_SESSAO_VI>()
				.Property(e => e.SIS_CODIGO)
				.IsUnicode(false);
		}
	}
}
