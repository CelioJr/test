namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consol
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_consol()
        {
            dw_consolal = new HashSet<dw_consolal>();
            dw_consollog = new HashSet<dw_consollog>();
            dw_consol_param = new HashSet<dw_consol_param>();
            dw_consolmo = new HashSet<dw_consolmo>();
            dw_consolpr = new HashSet<dw_consolpr>();
            dw_consolvaritmo = new HashSet<dw_consolvaritmo>();
            dw_consolre = new HashSet<dw_consolre>();
            dw_consolpemp = new HashSet<dw_consolpemp>();
            dw_consolpa = new HashSet<dw_consolpa>();
            dw_consoldef = new HashSet<dw_consoldef>();
            dw_consolcipoco = new HashSet<dw_consolcipoco>();
        }

        public decimal? seg_auto_tempocalendario { get; set; }

        public long? pcs_auto_producaoprevista { get; set; }

        public long? pcs_auto_producaorefugada { get; set; }

        public long? pcs_auto_producaobruta { get; set; }

        public long? pcs_auto_perdacavidades { get; set; }

        public long? pcs_auto_perdaciclo { get; set; }

        public long? pcs_auto_perdaparada_sp { get; set; }

        public long? pcs_auto_perdaparada { get; set; }

        public decimal? seg_auto_ciclomedio { get; set; }

        public decimal? seg_auto_perdaciclo { get; set; }

        public decimal? seg_auto_tempodisponivel { get; set; }

        public decimal? seg_auto_tempoprodutivas { get; set; }

        public decimal? seg_auto_ritmo { get; set; }

        public decimal? seg_auto_temporefugadas { get; set; }

        public decimal? seg_auto_tempotrabalhado { get; set; }

        public decimal? seg_auto_cicloimprodutivo { get; set; }

        public decimal? seg_auto_cicloprodutivo { get; set; }

        public decimal? seg_auto_tempoparada_sp { get; set; }

        public decimal? seg_auto_tempoparada { get; set; }

        public decimal? seg_auto_tempoativo { get; set; }

        [Key]
        public long id_consol { get; set; }

        public decimal? seg_manu_tempoativo { get; set; }

        public decimal? seg_manu_tempoparada { get; set; }

        public decimal? seg_manu_tempoparada_sp { get; set; }

        public long? pcs_auto_qtinjnormal { get; set; }

        public decimal? seg_auto_tmpcicnormal { get; set; }

        public decimal? seg_auto_ciclopadrao { get; set; }

        public decimal? seg_auto_perdacav { get; set; }

        public decimal? seg_manu_cicloprodutivo { get; set; }

        public decimal? seg_auto_boas { get; set; }

        public long id_consolid { get; set; }

        public decimal? seg_manu_cicloimprodutivo { get; set; }

        public decimal? seg_manu_tempotrabalhado { get; set; }

        public decimal? seg_manu_temporefugadas { get; set; }

        public decimal? seg_manu_ritmo { get; set; }

        public decimal? seg_manu_tempoprodutivas { get; set; }

        public decimal? seg_manu_tempodisponivel { get; set; }

        public decimal? seg_manu_perdaciclo { get; set; }

        public decimal? seg_manu_ciclomedio { get; set; }

        public decimal? seg_manu_ciclopadrao { get; set; }

        public decimal? seg_manu_tmpcicnormal { get; set; }

        public decimal? seg_manu_perdacav { get; set; }

        public decimal? seg_manu_boas { get; set; }

        public long? pcs_manu_perdaparada { get; set; }

        public long? pcs_manu_perdaparada_sp { get; set; }

        public long? pcs_manu_perdaciclo { get; set; }

        public long? pcs_manu_perdacavidades { get; set; }

        public long? pcs_manu_producaobruta { get; set; }

        public long? pcs_manu_producaorefugada { get; set; }

        public long? pcs_manu_producaoprevista { get; set; }

        public long? pcs_manu_qtinjnormal { get; set; }

        public decimal? seg_manu_tempocalendario { get; set; }

        public decimal? seg_auto_tempoalerta { get; set; }

        public decimal? seg_manu_tempoalerta { get; set; }

        public decimal? seg_auto_tempocalsempeso { get; set; }

        public decimal? seg_manu_tempocalsempeso { get; set; }

        public long? pcs_cav_total { get; set; }

        public long? pcs_cav_ativas { get; set; }

        public decimal? qt_ocoparadacp { get; set; }

        public decimal? qt_ocoparadasp { get; set; }

        public decimal? seg_auto_tempoparadaMTBF { get; set; }

        public decimal? seg_auto_tempoparadaMTTR { get; set; }

        public decimal? seg_auto_tempoparadaPAO { get; set; }

        public decimal? seg_auto_tempoparadaPA { get; set; }

        public decimal? seg_auto_tempoparadaPTP { get; set; }

        public decimal? seg_auto_tempoparadaSCP { get; set; }

        public decimal? seg_auto_tempoparadaMDO { get; set; }

        public decimal? g_peso_bruto { get; set; }

        public decimal? g_pelo_liquido { get; set; }

        public decimal? seg_auto_tempoparada_ab { get; set; }

        public int? pcs_auto_cav_ativas { get; set; }

        public int? pcs_manu_cav_ativas { get; set; }

        public int? pcs_manu_cav_total { get; set; }

        public int? pcs_auto_cav_total { get; set; }

        public int? pcs_auto_perdaparada_cp { get; set; }

        public int? pcs_manu_perdaparada_cp { get; set; }

        public decimal? qt_auto_cicloimprodutivo { get; set; }

        public decimal? qt_manu_cicloimprodutivo { get; set; }

        public decimal? qt_manu_cicloprodutivo { get; set; }

        public decimal? qt_auto_cicloprodutivo { get; set; }

        public decimal? qt_manu_cicloregulagem { get; set; }

        public decimal? qt_auto_cicloregulagem { get; set; }

        public decimal? qt_manu_ocoparada_cp { get; set; }

        public decimal? qt_auto_ocoparada_cp { get; set; }

        public decimal? qt_manu_ocoparada_sp { get; set; }

        public decimal? qt_auto_ocoparada_sp { get; set; }

        public decimal? qt_manu_ocoparadaFDS { get; set; }

        public decimal? qt_manu_ocoparadaIMPREV { get; set; }

        public decimal? qt_manu_ocoparadaMDO { get; set; }

        public decimal? qt_manu_ocoparadaMTBF { get; set; }

        public decimal? qt_manu_ocoparadaMTTR { get; set; }

        public decimal? qt_manu_ocoparadaPA { get; set; }

        public decimal? qt_manu_ocoparadaPAO { get; set; }

        public decimal? qt_manu_ocoparadaPP { get; set; }

        public decimal? qt_manu_ocoparadaPREV { get; set; }

        public decimal? qt_manu_ocoparadaPTP { get; set; }

        public decimal? qt_manu_ocoparadaREGULAGEM { get; set; }

        public decimal? qt_manu_ocoparadaSCP { get; set; }

        public decimal? qt_auto_ocoparadaFDS { get; set; }

        public decimal? qt_auto_ocoparadaIMPREV { get; set; }

        public decimal? qt_auto_ocoparadaMDO { get; set; }

        public decimal? qt_auto_ocoparadaMTBF { get; set; }

        public decimal? qt_auto_ocoparadaMTTR { get; set; }

        public decimal? qt_auto_ocoparadaPA { get; set; }

        public decimal? qt_auto_ocoparadaPAO { get; set; }

        public decimal? qt_auto_ocoparadaPP { get; set; }

        public decimal? qt_auto_ocoparadaPREV { get; set; }

        public decimal? qt_auto_ocoparadaPTP { get; set; }

        public decimal? qt_auto_ocoparadaREGULAGEM { get; set; }

        public decimal? qt_auto_ocoparadaSCP { get; set; }

        public decimal? SEG_AUTO_TEMPOPARADA_CP { get; set; }

        public decimal? SEG_MANU_TEMPOPARADA_CP { get; set; }

        public decimal? seg_manu_tempoparadaFDS { get; set; }

        public decimal? seg_manu_tempoparadaIMPREV { get; set; }

        public decimal? seg_manu_tempoparadaMDO { get; set; }

        public decimal? seg_manu_tempoparadaMTBF { get; set; }

        public decimal? seg_manu_tempoparadaMTTR { get; set; }

        public decimal? seg_manu_tempoparadaPA { get; set; }

        public decimal? seg_manu_tempoparadaPAO { get; set; }

        public decimal? seg_manu_tempoparadaPP { get; set; }

        public decimal? seg_manu_tempoparadaPREV { get; set; }

        public decimal? seg_manu_tempoparadaPTP { get; set; }

        public decimal? seg_manu_tempoparadaREGULAGEM { get; set; }

        public decimal? seg_manu_tempoparadaSCP { get; set; }

        public decimal? seg_auto_tempoparadaFDS { get; set; }

        public decimal? seg_auto_tempoparadaIMPREV { get; set; }

        public decimal? seg_auto_tempoparadaPP { get; set; }

        public decimal? seg_auto_tempoparadaPREV { get; set; }

        public decimal? seg_auto_tempoparadaREGULAGEM { get; set; }

        public decimal? SEG_AUTO_TEMPOPRODUTIVO { get; set; }

        public decimal? SEG_MANU_TEMPOPRODUTIVO { get; set; }

        public decimal? SEG_MANU_CICLOREGULAGEM { get; set; }

        public decimal? SEG_AUTO_CICLOREGULAGEM { get; set; }

        public decimal? qt_auto_perdamp { get; set; }

        public decimal? qt_manu_perdamp { get; set; }

        public decimal? seg_auto_cicloprodutivo_cta { get; set; }

        public decimal? seg_auto_cicloimprodutivo_cta { get; set; }

        public decimal? seg_manu_cicloprodutivo_cta { get; set; }

        public decimal? seg_manu_cicloimprodutivo_cta { get; set; }

        public decimal? seg_auto_cta { get; set; }

        public decimal? seg_manu_cta { get; set; }

        public decimal? seg_auto_tempoparada_cp_vr { get; set; }

        public decimal? seg_auto_tempoparada_sp_vr { get; set; }

        public decimal? seg_manu_tempoparada_cp_vr { get; set; }

        public decimal? seg_manu_tempoparada_sp_vr { get; set; }

        public decimal? qt_auto_ocoparada_cp_vr { get; set; }

        public decimal? qt_auto_ocoparada_sp_vr { get; set; }

        public decimal? qt_manu_ocoparada_cp_vr { get; set; }

        public decimal? qt_manu_ocoparada_sp_vr { get; set; }

        public decimal? seg_auto_tempoparada_default { get; set; }

        public decimal? seg_auto_tempoparada_sem_op { get; set; }

        public decimal? seg_auto_tempoparada_sem_evt { get; set; }

        public decimal? seg_auto_tempoparada_sem_cnx { get; set; }

        public decimal? seg_manu_tempoparada_default { get; set; }

        public decimal? seg_manu_tempoparada_sem_op { get; set; }

        public decimal? seg_manu_tempoparada_sem_evt { get; set; }

        public decimal? seg_manu_tempoparada_sem_cnx { get; set; }

        public decimal? qt_auto_tempoparada_default { get; set; }

        public decimal? qt_auto_tempoparada_sem_op { get; set; }

        public decimal? qt_auto_tempoparada_sem_evt { get; set; }

        public decimal? qt_auto_tempoparada_sem_cnx { get; set; }

        public decimal? qt_manu_tempoparada_default { get; set; }

        public decimal? qt_manu_tempoparada_sem_op { get; set; }

        public decimal? qt_manu_tempoparada_sem_evt { get; set; }

        public decimal? qt_manu_tempoparada_sem_cnx { get; set; }

        public decimal? qt_auto_cicloprevisto { get; set; }

        public decimal? qt_manu_cicloprevisto { get; set; }

        public decimal? g_auto_peso_bruto2 { get; set; }

        public decimal? g_auto_peso_liquido2 { get; set; }

        public decimal? g_manu_peso_bruto2 { get; set; }

        public decimal? g_manu_peso_liquido2 { get; set; }

        public decimal? g_auto_peso_bruto { get; set; }

        public decimal? g_auto_peso_liquido { get; set; }

        public decimal? g_manu_peso_bruto { get; set; }

        public decimal? g_manu_peso_liquido { get; set; }

        public decimal? qt_auto_cavativas { get; set; }

        public decimal? qt_auto_cavtotal { get; set; }

        public decimal? pcs_auto_producaoliquida { get; set; }

        public decimal? pcs_manu_producaoliquida { get; set; }

        public virtual dw_consolid dw_consolid { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolal> dw_consolal { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consollog> dw_consollog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consol_param> dw_consol_param { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolmo> dw_consolmo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolpr> dw_consolpr { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolvaritmo> dw_consolvaritmo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolre> dw_consolre { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolpemp> dw_consolpemp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolpa> dw_consolpa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consoldef> dw_consoldef { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolcipoco> dw_consolcipoco { get; set; }
    }
}
