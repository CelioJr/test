﻿using System;
using System.Collections.Generic;
using System.Linq;
using SEMP.DAL.Models;
using SEMP.Model.DTO;

namespace SEMP.DAL.DAO.CNQ
{
	public class daoCustoRetrabalho
	{

		public static List<CnqCustoRetDTO> ObterCustosRet(DateTime mesRef, string familia)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from c in dtbSim.tbl_CnqCustoRet
								 join f in dtbSim.tbl_CnqFamilia on c.CodFam equals f.CodFam
								 where c.MesRef == mesRef
								 select new CnqCustoRetDTO()
				{
					CodFab = c.CodFab,
					MesRef = c.MesRef,
					CodFam = c.CodFam,
					ValReTrabalho = c.ValReTrabalho,
					ProdAmount = c.ProdAmount,
					NomFam = f.NomFam
				});

				if (!String.IsNullOrEmpty(familia))
				{
					resultado = resultado.Where(x => x.CodFam.Equals(familia));
				}

				return resultado.ToList();

			}
		}

		public static CnqCustoRetDTO ObterCustoRet(DateTime mesRef, string familia, string codFab)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from c in dtbSim.tbl_CnqCustoRet
								 join f in dtbSim.tbl_CnqFamilia on c.CodFam equals f.CodFam
								 where c.MesRef == mesRef && c.CodFam.Equals(familia) && c.CodFab.Equals(codFab)
								 select new CnqCustoRetDTO()
								 {
									 CodFab = c.CodFab,
									 MesRef = c.MesRef,
									 CodFam = c.CodFam,
									 ValReTrabalho = c.ValReTrabalho,
									 ProdAmount = c.ProdAmount,
									 NomFam = f.NomFam
								 }).FirstOrDefault();
								 
				return resultado;

			}
		}

		public static void GravarCustoRef(CnqCustoRetDTO custoret)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var existe =
					dtbSim.tbl_CnqCustoRet.FirstOrDefault(
						x => x.MesRef == custoret.MesRef && x.CodFam.Equals(custoret.CodFam) && x.CodFab.Equals(custoret.CodFab));

				if (existe != null)
				{
					existe.ValReTrabalho = custoret.ValReTrabalho;
					existe.ProdAmount = custoret.ProdAmount;
					dtbSim.SaveChanges();
				}
				else
				{
					var custo = new tbl_CnqCustoRet()
					{
						CodFab = custoret.CodFab,
						CodFam = custoret.CodFam,
						MesRef = custoret.MesRef,
						ProdAmount = custoret.ProdAmount,
						ValReTrabalho = custoret.ValReTrabalho
					};

					dtbSim.tbl_CnqCustoRet.Add(custo);
					dtbSim.SaveChanges();
				}
			}
		}

		public static void RemoverCustoRef(CnqCustoRetDTO custoret)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var existe =
					dtbSim.tbl_CnqCustoRet.FirstOrDefault(
						x => x.MesRef == custoret.MesRef && x.CodFam.Equals(custoret.CodFam) && x.CodFab.Equals(custoret.CodFab));

				if (existe == null) return;

				dtbSim.tbl_CnqCustoRet.Remove(existe);
				dtbSim.SaveChanges();
			}
		}

	}
}
