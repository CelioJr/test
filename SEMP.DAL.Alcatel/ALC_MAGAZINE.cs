namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_MAGAZINE")]
    public partial class ALC_MAGAZINE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ALC_MAGAZINE()
        {
            ALC_ENGINE = new HashSet<ALC_ENGINE>();
        }

        [Key]
        [StringLength(20)]
        public string ID_MAG { get; set; }

        public DateTime? DT_CREATION { get; set; }

        public int? QTY_PROD { get; set; }

        public int? QUANTITY { get; set; }

        [StringLength(20)]
        public string STATUS { get; set; }

        [StringLength(20)]
        public string OP { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_ENGINE> ALC_ENGINE { get; set; }

        public virtual ALC_ORDER ALC_ORDER { get; set; }
    }
}
