﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.Rastreabilidade;
using SEMP.Model.DTO;

namespace SEMP.Areas.Outros.Controllers
{
	public class PlanoAcaoController : Controller
	{
		#region Listagem
		//
		// GET: /Outros/PlanoAcao/
		public ActionResult Index()
		{
			return View();
		}

		//
		// GET: /Outros/PlanoAcao/Details/5
		public ActionResult Lista(string datInicio, string datFim, int? flgStatus)
		{

			var dIni = DateTime.Parse(DateTime.Now.AddDays(-DateTime.Now.Day).AddDays(1).ToShortDateString());
			var dFim = dIni.AddMonths(1).AddDays(-1);

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
				dFim = DateTime.Parse(datFim);
			}

			var dados = PlanoAcao.ObterPlanoAcaoLista(dIni, dFim, flgStatus);

			return View(dados);
		}

		#endregion

		#region Create
		//
		// GET: /Outros/PlanoAcao/Create
		public ActionResult Create()
		{
			return View();
		}

		//
		// POST: /Outros/PlanoAcao/Create
		[HttpPost]
		public ActionResult Create(CBPlanoAcaoDTO dado)
		{
			try
			{
				
				PlanoAcao.GravarPlanoAcao(dado);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json("Erro ao gravar. " + ex.Message, JsonRequestBehavior.AllowGet);

			}
		}
		#endregion

		#region Edit
		//
		// GET: /Outros/PlanoAcao/Edit/5
		public ActionResult Edit(int id)
		{
			var dado = PlanoAcao.ObterPlanoAcao(id);

			return View(dado);
		}

		//
		// POST: /Outros/PlanoAcao/Edit/5
		[HttpPost]
		public ActionResult Edit(CBPlanoAcaoDTO dado)
		{
			try
			{
				PlanoAcao.GravarPlanoAcao(dado);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		public ActionResult EditPlano(int codPlano, string desPlano, int? flgStatus, string datConclusao)
		{
			try
			{
				var plano = PlanoAcao.ObterPlanoAcao(codPlano);
				plano.DesPlano = desPlano;
				if (flgStatus != null)
				{
					plano.FlgStatus = flgStatus;
				}
				if (!String.IsNullOrEmpty(datConclusao))
				{
					plano.DatConclusao = DateTime.Parse(datConclusao);
				}

				PlanoAcao.GravarPlanoAcao(plano);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}
		#endregion

		#region Delete
		//
		// GET: /Outros/PlanoAcao/Delete/5
		public ActionResult Delete(int id)
		{
			var dado = PlanoAcao.ObterPlanoAcao(id);

			return View(dado);
		}

		//
		// POST: /Outros/PlanoAcao/Delete/5
		[HttpPost]
		public ActionResult Delete(CBPlanoAcaoDTO dado)
		{
			try
			{
				PlanoAcao.RemoverPlanoAcao(dado.CodPlano);

				return RedirectToAction("Index");
			}
			catch (Exception ex)
			{
				ViewBag.mensagem = "Erro ao remover o registro: " + ex.Message;
				return RedirectToAction("Index");
			}
		}
		#endregion
	}
}
