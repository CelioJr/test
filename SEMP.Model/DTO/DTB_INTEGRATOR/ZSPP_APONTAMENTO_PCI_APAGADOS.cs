namespace SEMP.Model.DTO
{
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;

	public partial class ZSPP_APONTAMENTO_PCI_APAGADOS_DTO
	{
        [Key]
        [Column(Order = 0)]
        [StringLength(16)]
        public string PACKNO { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        public string EXTROW { get; set; }

        [StringLength(12)]
        public string AUFNR { get; set; }

        public decimal? YIELD { get; set; }

        [StringLength(3)]
        public string MEINS { get; set; }

        [StringLength(30)]
        public string pacote { get; set; }
    }
}
