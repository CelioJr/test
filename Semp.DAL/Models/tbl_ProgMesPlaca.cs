using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_ProgMesPlaca
    {
        public string CodFab { get; set; }
        public System.DateTime DatProducao { get; set; }
        public string CodPlaca { get; set; }
        public string CodProcesso { get; set; }
        public decimal QtdPrograma { get; set; }
        public string flgCobertura { get; set; }
        public string CodModelo { get; set; }
        public string CodAparelho { get; set; }
        public Nullable<decimal> QtdFrequencia { get; set; }
        public Nullable<decimal> QtdSaldoAnt { get; set; }
    }
}
