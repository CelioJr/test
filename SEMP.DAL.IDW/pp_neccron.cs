namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_neccron
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pp_neccron()
        {
            pp_planeccron = new HashSet<pp_planeccron>();
        }

        [Key]
        public long id_neccron { get; set; }

        public DateTime? dt_desejada { get; set; }

        public long? qt_desejada { get; set; }

        public long id_nec { get; set; }

        public DateTime? dt_estimada { get; set; }

        public virtual pp_nec pp_nec { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_planeccron> pp_planeccron { get; set; }
    }
}
