using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_SYS_USER_DTO
    {
        public ALC_SYS_USER_DTO()
        {
            this.ALC_INVENTORY = new List<ALC_INVENTORY_DTO>();
            this.ALC_LOT_LIST = new List<ALC_LOT_LIST_DTO>();
            this.ALC_LOTE_LIST = new List<ALC_LOTE_LIST_DTO>();
            this.ALC_OQC_INSPECTION = new List<ALC_OQC_INSPECTION_DTO>();
            this.ALC_ORDER = new List<ALC_ORDER_DTO>();
            this.ALC_PRODUCTION_COUNTER = new List<ALC_PRODUCTION_COUNTER_DTO>();
            this.ALC_REPAIR = new List<ALC_REPAIR_DTO>();
            this.ALC_REPAIR1 = new List<ALC_REPAIR_DTO>();
            this.ALC_SYS_LOG = new List<ALC_SYS_LOG_DTO>();
            this.ALC_ROLE_USER = new List<ALC_ROLE_USER_DTO>();
        }

        public int USER_ID { get; set; }
        public Nullable<bool> enable { get; set; }
        public string USER_LOGIN { get; set; }
        public string userPassword { get; set; }
        public string USER_NAME { get; set; }
        public Nullable<int> USER_POST { get; set; }
        public string USER_PASSWORD { get; set; }
        public virtual ICollection<ALC_INVENTORY_DTO> ALC_INVENTORY { get; set; }
        public virtual ICollection<ALC_LOT_LIST_DTO> ALC_LOT_LIST { get; set; }
        public virtual ICollection<ALC_LOTE_LIST_DTO> ALC_LOTE_LIST { get; set; }
        public virtual ICollection<ALC_OQC_INSPECTION_DTO> ALC_OQC_INSPECTION { get; set; }
        public virtual ICollection<ALC_ORDER_DTO> ALC_ORDER { get; set; }
        public virtual ALC_POST_USER_DTO ALC_POST_USER { get; set; }
        public virtual ALC_POST_USER_DTO ALC_POST_USER1 { get; set; }
        public virtual ICollection<ALC_PRODUCTION_COUNTER_DTO> ALC_PRODUCTION_COUNTER { get; set; }
        public virtual ICollection<ALC_REPAIR_DTO> ALC_REPAIR { get; set; }
        public virtual ICollection<ALC_REPAIR_DTO> ALC_REPAIR1 { get; set; }
        public virtual ICollection<ALC_SYS_LOG_DTO> ALC_SYS_LOG { get; set; }
        public virtual ICollection<ALC_ROLE_USER_DTO> ALC_ROLE_USER { get; set; }
    }
}
