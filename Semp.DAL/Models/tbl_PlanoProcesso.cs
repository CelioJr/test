using System;

namespace SEMP.DAL.Models
{
    public partial class tbl_PlanoProcesso
    {
        public string CodProcesso { get; set; }
        public string DesProcesso { get; set; }
        public Nullable<short> FlgProcesso { get; set; }
        public Nullable<short> FlgEmpenho { get; set; }
        public Nullable<short> FlgLSA { get; set; }
        public string CodAntigo { get; set; }
        public string CodAP { get; set; }
        public System.Guid rowguid { get; set; }
    }
}
