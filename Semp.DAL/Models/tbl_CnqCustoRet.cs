using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CnqCustoRet
    {
        public string CodFab { get; set; }
        public System.DateTime MesRef { get; set; }
        public string CodFam { get; set; }
        public Nullable<decimal> ValReTrabalho { get; set; }
        public Nullable<decimal> ProdAmount { get; set; }
    }
}
