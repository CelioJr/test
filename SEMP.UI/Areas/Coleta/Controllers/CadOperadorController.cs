﻿using System.Collections.Generic;
using System.Web.Mvc;
using RAST.BO.Rastreabilidade;
using SEMP.Model.DTO;

namespace SEMP.Areas.Coleta.Controllers
{
	public class CadOperadorController : Controller
	{
		//
		// GET: /Coleta/CadOperador/
		public ActionResult Index(int? codLinha)
		{
			if (codLinha == null) return View(new List<CBPostoDTO>());

			var postos = Geral.ObterPostoPorLinha(codLinha.Value, true);

			return View(postos);
		}

		public ActionResult Alterar(int codLinha, int numPosto)
		{
			var posto = Geral.ObterPosto(codLinha, numPosto);

			return View(posto);
		}

		[HttpPost]
		public ActionResult Alterar(int codLinha, int numPosto, string codDrt)
		{
			var posto = Geral.ObterPosto(codLinha, numPosto);
			posto.CodDRT = codDrt;

			var gravar = Configuracao.EditarPosto(posto);

			return RedirectToAction("Index");
		}

	}
}
