namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_gt
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_gt()
        {
            dw_consolid = new HashSet<dw_consolid>();
            dw_consolmolog = new HashSet<dw_consolmolog>();
            dw_estlocal = new HashSet<dw_estlocal>();
            dw_estmov = new HashSet<dw_estmov>();
            dw_folha = new HashSet<dw_folha>();
            dw_folhacic = new HashSet<dw_folhacic>();
            dw_folhateste = new HashSet<dw_folhateste>();
            dw_macrange = new HashSet<dw_macrange>();
            dw_operacao = new HashSet<dw_operacao>();
            dw_rota = new HashSet<dw_rota>();
            ip_balanceamento = new HashSet<ip_balanceamento>();
            om_cfg = new HashSet<om_cfg>();
            om_gt1 = new HashSet<om_gt>();
            om_homogt = new HashSet<om_homogt>();
            om_indgt = new HashSet<om_indgt>();
            om_obj = new HashSet<om_obj>();
            om_obj1 = new HashSet<om_obj>();
            om_pt = new HashSet<om_pt>();
            om_usr2 = new HashSet<om_usr>();
            pp_cp = new HashSet<pp_cp>();
            pp_planptgt = new HashSet<pp_planptgt>();
        }

        [Key]
        public long id_gt { get; set; }

        [StringLength(30)]
        public string cd_gt { get; set; }

        public int? revisao { get; set; }

        [StringLength(100)]
        public string ds_gt { get; set; }

        public DateTime? dt_revisao { get; set; }

        public DateTime? dt_stativo { get; set; }

        public short? st_ativo { get; set; }

        public long id_usrstativo { get; set; }

        public long id_usrrevisao { get; set; }

        public long id_img { get; set; }

        [StringLength(10)]
        public string ds_curta { get; set; }

        public long? id_cc { get; set; }

        public decimal? largura { get; set; }

        public decimal? altura { get; set; }

        public decimal? gridx { get; set; }

        public decimal? gridy { get; set; }

        public long id_tpgt { get; set; }

        public decimal? seg_x { get; set; }

        public decimal? seg_y { get; set; }

        public decimal? seg_z { get; set; }

        public decimal? tensao_min { get; set; }

        public decimal? tensao_nom { get; set; }

        public decimal? tensao_max { get; set; }

        [StringLength(40)]
        public string depara { get; set; }

        public decimal? ind_oee { get; set; }

        public long? id_gtfase { get; set; }

        [StringLength(11)]
        public string cor { get; set; }

        public short? tp_algoABC { get; set; }

        [StringLength(2)]
        public string cb { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolid> dw_consolid { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolmolog> dw_consolmolog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_estlocal> dw_estlocal { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_estmov> dw_estmov { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folha> dw_folha { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhacic> dw_folhacic { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhateste> dw_folhateste { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_macrange> dw_macrange { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_operacao> dw_operacao { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rota> dw_rota { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ip_balanceamento> ip_balanceamento { get; set; }

        public virtual om_cc om_cc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_gt> om_gt1 { get; set; }

        public virtual om_gt om_gt2 { get; set; }

        public virtual om_img om_img { get; set; }

        public virtual om_tpgt om_tpgt { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_homogt> om_homogt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_indgt> om_indgt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_obj> om_obj { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_obj> om_obj1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_pt> om_pt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_usr> om_usr2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cp> pp_cp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_planptgt> pp_planptgt { get; set; }
    }
}
