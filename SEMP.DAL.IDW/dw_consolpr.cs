namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolpr
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_consolpr()
        {
            dw_consolprmo = new HashSet<dw_consolprmo>();
        }

        [Key]
        public long id_consolpr { get; set; }

        public long id_consol { get; set; }

        public long? pcs_auto_producaobruta { get; set; }

        public long? pcs_auto_producaorefugada { get; set; }

        public long? pcs_manu_producaobruta { get; set; }

        public long? pcs_manu_producaorefugada { get; set; }

        public decimal? g_peso_bruto { get; set; }

        public decimal? g_peso_liquido { get; set; }

        public long id_produto { get; set; }

        public decimal? seg_auto_tempoparada { get; set; }

        public decimal? seg_auto_tempoparada_sp { get; set; }

        public decimal? g_auto_peso_bruto { get; set; }

        public decimal? g_auto_peso_liquido { get; set; }

        public decimal? g_manu_peso_bruto { get; set; }

        public decimal? g_manu_peso_liquido { get; set; }

        public decimal? pcs_Auto_Perdacavidades { get; set; }

        public decimal? pcs_Auto_Perdaciclo { get; set; }

        public decimal? pcs_Auto_Perdaparada_Sp { get; set; }

        public decimal? pcs_Auto_Perdaparada_Cp { get; set; }

        public decimal? pcs_auto_producaoprevista { get; set; }

        public decimal? seg_auto_ritmo { get; set; }

        public decimal? pcs_auto_producaoliquida { get; set; }

        public decimal? pcs_manu_producaoliquida { get; set; }

        public virtual dw_consol dw_consol { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolprmo> dw_consolprmo { get; set; }

        public virtual om_produto om_produto { get; set; }
    }
}
