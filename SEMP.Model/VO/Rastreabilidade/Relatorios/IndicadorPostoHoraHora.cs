﻿using System;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
    public class IndicadorPostoHoraHora
    {
        public string Titulo { get; set; }
        public TimeSpan HoraInicio { get; set; }
        public TimeSpan HoraFim { get; set; }
        public string Hora { get; set; }
        public decimal Quantidade { get; set; }
        public string CodDefeito { get; set; }

        public IndicadorPostoHoraHora() { }

        public IndicadorPostoHoraHora(string titulo) 
        {
            this.Titulo = titulo;
        }

    }
}
