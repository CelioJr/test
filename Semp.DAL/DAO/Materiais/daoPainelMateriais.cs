﻿using SEMP.DAL.Models;
using SEMP.Model.VO.Materiais;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.DAO.Materiais
{
	public class daoPainelMateriais
	{

		public static List<PainelMateriais> Consultar(string DataInicial, string DataFinal)
		{
			DataInicial = String.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(DataInicial)); //Converte a data para o formato do banco de dados.
			DataFinal = String.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(DataFinal)); //Converte a data para o formato do banco de dados.

			using (var db = new DTB_SIMContext())
			{
				string procedure = string.Format("EXEC dbo.SPC_ESTMOB108 NULL, NULL, NULL, NULL, NULL, '01','A', '{0}', '{1}', '00019002';", DataInicial, DataFinal);
				return db.Database.SqlQuery<PainelMateriais>(procedure).ToList();
			}
		}

		public static List<PainelMateriaisDetalhes> Detalhes(string codSeparacao, string AP, string Fab)
		{
			using (var db = new DTB_SIMContext())
			{
				string Procedure = string.Format("EXEC SPC_EST340_1 '{0}', '{1}', 1, NULL, NULL, {2} , NULL;", Fab, AP, codSeparacao);
				return db.Database.SqlQuery<PainelMateriaisDetalhes>(Procedure).ToList();
			}
		}

	}
}
