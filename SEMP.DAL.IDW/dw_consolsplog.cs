namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolsplog
    {
        [Key]
        public long id_consolsplog { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? dthr_inicio { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? dthr_fim { get; set; }

        public long id_pt { get; set; }

        public virtual om_pt om_pt { get; set; }
    }
}
