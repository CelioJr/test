﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using SEMP.DAL.Models;
using SEMP.Model.DTO;

namespace RAST.BO.Relatorios
{
	public class RelatorioProducaoMF
	{
		/// <summary>
		/// Consultar o resumo de produção por grupo de linha.
		/// </summary>
		/// <param name="DatInicio">Data início da consulta</param>
		/// <param name="DatFim">Data fim da consulta</param>
		/// <param name="Turno">Turno a consultar</param>
		/// <param name="GrupoLinha">Linhas a consultar -> se em branco consulta todas as linhas</param>
		/// <returns>Retorna a view ResumoProducao mostrando os dados de produção.</returns>
		public static List<RelatorioProducao> ObterResumo(DateTime DatInicio, DateTime DatFim, int Turno, string GrupoLinha)
		{

			//Cria o objeto que vai receber os dados do resumo de produção
			List<RelatorioProducao> resumo = new List<RelatorioProducao>();

			try
			{
				List<CBLinhaDTO> linhas = ObterLinhasMF(GrupoLinha);

				// Faz a operação para cada linha
				foreach (CBLinhaDTO linha in linhas.Where(x=> !x.FlgAtivo.Equals("0")))
				{
					// Seleciona o programa da linha atual no período
					var programaLinha = daoResumoMF.ObterProgramaByLinha(linha.CodLinhaT1, Turno, DatInicio, DatFim);

					// Seleciona a produção da linha atual no período
					var producaoDia = daoResumoMF.ObterProduzidoByLinha(linha.CodLinhaT1, Turno, DatInicio, DatFim);

					DateTime DataIni = DatInicio.AddDays(-(DatInicio.Day) + 1);
					DateTime DataFim = DatFim.AddDays(-(DatFim.Day) + 1);
					var CapacLinha = daoResumoMF.ObterCapacidadeByLinha(linha.CodLinhaT1, Turno, DataIni, DataFim);

					// Atualiza para cada linha e cada programa
					foreach (var programa in programaLinha)
					{
						RelatorioProducao r = new RelatorioProducao();
						r.CodLinha = linha.CodLinhaT1;
						r.DesLinha = linha.DesLinha;

						r.CodModelo = programa.CodModelo;
						r.DesModelo = programa.DesModelo;
						//.ObterDescricaoItem(programa.CodModelo);

						r.QtdPlano = (int)(programa.QtdProdPm ?? 0);

						if (producaoDia.Any())
						{
							var qtde = (from i in producaoDia
										where i.CodLinha == linha.CodLinhaT1 &&
												i.NumTurno == Turno &&
												i.CodModelo == programa.CodModelo
										select i.QtdProduzida).FirstOrDefault();
							r.QtdRealizado = qtde != null ? qtde.Value : 0;
						}
						else
						{
							r.QtdRealizado = 0;
						}

						if (r.QtdRealizado > 0 && r.QtdPlano > 0)
						{
							r.PercDiferenca = Math.Round(((decimal)r.QtdDiferenca / (decimal)r.QtdPlano) * (decimal)100, 2);
						}
						else
						{
							r.PercDiferenca = 100;
						}

						if (CapacLinha.Any())
						{
							var firstOrDefault = (from i in CapacLinha
												  where i.CodLinha == linha.CodLinhaT1 &&
														i.NumTurno == Turno &&
														i.CodModelo == programa.CodModelo
												  select i.QtdCapacLinha).FirstOrDefault();

							if (firstOrDefault != null)
							{
								var qtde = firstOrDefault.Value;
								r.QtdCapacidade = (int)qtde;
							}
						}

						resumo.Add(r);
					}

					foreach (ProdDiaDTO producao in producaoDia)
					{
						if (
							programaLinha.Any(
								prog =>
									prog.CodLinha == producao.CodLinha &&
									prog.CodModelo == producao.CodModelo &&
									prog.NumTurno == producao.NumTurno)) continue;

						RelatorioProducao r = new RelatorioProducao
						{
							CodLinha = linha.CodLinhaT1,
							DesLinha = linha.DesLinha,
							CodModelo = producao.CodModelo,
							DesModelo = producao.DesModelo, //daoGeral.ObterDescricaoItem(producao.CodModelo),
							QtdRealizado = producao.QtdProduzida ?? 0,
							QtdPlano = 0
						};

						if (r.QtdPlano > 0)
						{
							r.PercDiferenca = (r.QtdDiferenca / r.QtdPlano) * 100;
						}

						resumo.Add(r);
					}

				}

				return resumo;
			}
			catch
			{
				return null;
			}
		}

		public static List<CBLinhaDTO> ObterLinhasMF()
		{
			return daoResumoMF.LinhasMF();
		}

		public static List<CBLinhaDTO> ObterLinhasMF(string GrupoLinha)
		{
			if (String.IsNullOrEmpty(GrupoLinha))
			{
				return daoResumoMF.LinhasMF();
			}
			else
			{
				return daoResumoMF.ObterLinhas(GrupoLinha);
			}
		}

		public static List<CBLinhaDTO> ObterLinhasMF(string GrupoLinha, DateTime DatProducao)
		{
			if (String.IsNullOrEmpty(GrupoLinha))
			{
				return daoResumoMF.LinhasMF(DatProducao);
			}
			else
			{
				return daoResumoMF.ObterLinhas(GrupoLinha, DatProducao);
			}
		}

		public static List<ParadasLinha> ObterParadas(DateTime DatInicio, DateTime DatFim, int Turno, string GrupoLinha)
		{
			List<ParadasLinha> resumoparadas = new List<ParadasLinha>();

			List<ProdParadaDTO> paradas = new List<ProdParadaDTO>();

			var linhas = ObterLinhasMF(GrupoLinha);
			linhas.ForEach(x => paradas.AddRange(daoParadas.ObterParadas(DatInicio, DatFim, Turno, x.CodLinhaT1)));

			foreach (var p in paradas)
			{
				var paradasdep = daoParadas.ObterParadaDep(p.NumParada);

				ParadasLinha paradaslinha = new ParadasLinha();
				paradaslinha.CodParada = p.NumParada;
				paradaslinha.DesParada = p.DesObs;
				paradaslinha.CodLinha = p.CodLinha;
				paradaslinha.FlgPDL = 'S';
				paradaslinha.HoraInicio = p.HorInicio;
				paradaslinha.HoraFim = p.HorFim;
				paradaslinha.QtdAparelhos = (int)(p.QtdNaoProduzida ?? 0);
				if (paradasdep != null && paradasdep.Any())
				{
					var prodParadaDepDto = paradasdep.FirstOrDefault();
					if (prodParadaDepDto != null) paradaslinha.SetorResp = prodParadaDepDto.CodDepto;
				}

				if (!String.IsNullOrEmpty(p.HorInicio.Trim()) && !String.IsNullOrEmpty(p.HorFim))
				{
					DateTime horainicio = DateTime.Parse(DatInicio.ToShortDateString() + " " + p.HorInicio);
					DateTime horafim;

					if (daoParadas.ToTimeSpan(p.HorFim).Hours > 7)
					{
						horafim = DateTime.Parse(DatInicio.ToShortDateString() + " " + p.HorFim);
					}
					else
					{
						horafim = DateTime.Parse(DatInicio.AddDays(1).ToShortDateString() + " " + p.HorFim);
					}

					var totalhoras = horafim.Subtract(horainicio);
					paradaslinha.TotalHoras = totalhoras.Hours.ToString("00") + ":" + totalhoras.Minutes.ToString("00");
				}

				//paradaslinha.TotalHoras
				resumoparadas.Add(paradaslinha);

			}

			return resumoparadas;
		}

		/*public static List<DefeitosLinha> ObterDefeitos(DateTime DatInicio, DateTime DatFim, int Turno, string GrupoLinha)
		{

			var linhas = ObterLinhasMF(GrupoLinha);

			List<viw_CBDefeitoTurnoDTO> defeitosDTO = new List<viw_CBDefeitoTurnoDTO>();
			linhas.ForEach(x => defeitosDTO.AddRange(daoDefeitos.ObterDefeitos(DatInicio, DatFim, Turno, "FEC", x.CodLinhaT1)));

			var defeitos = (from d in defeitosDTO
							select new
							{
								d.CodLinha,
								CodDefeito = d.CodDefeito.Trim().Substring(2),
								d.CodCausa,
								d.CodOrigem,
								d.Posicao,
								d.NumECB,
								d.DesAcao,
								CodModelo = d.NumECB.Substring(0, 6),
								d.FlgRevisado
							}
				).GroupBy(g => new { g.CodLinha, g.CodDefeito, g.CodCausa, g.CodOrigem, g.Posicao, g.CodModelo, g.DesAcao, g.FlgRevisado}
				).Select(i => new
				{
					i.Key.CodLinha,
					i.Key.CodDefeito,
					i.Key.CodCausa,
					i.Key.CodOrigem,
					QtdDefeito = i.Count(),
					i.Key.Posicao,
					i.Key.CodModelo,
					i.Key.DesAcao,
					i.Key.FlgRevisado,
					DesCausa = daoDefeitos.ObterDesCausa(i.Key.CodCausa),
					DesDefeito = daoDefeitos.ObterDesDefeito(i.Key.CodDefeito),
					Origem = i.Key.CodOrigem != null ? daoDefeitos.ObterDesOrigem(i.Key.CodOrigem.Trim().PadLeft(2, '0')) : "",
					DesModelo = daoGeral.ObterDescricaoItem(i.Key.CodModelo)
				}
			).ToList();

			var totais = defeitos.GroupBy(
				g => g.CodLinha).Select(
				i =>
				{
					var totalProd = daoResumoMF.ObterProduzidoLinhaTotal(daoGeral.ObterLinhaCliente(i.Key), Turno, DatInicio, DatFim).FirstOrDefault();
					return totalProd != null
						? new DefeitosLinha()
						{
							CodLinha = i.Key,
							CodCausa = "",
							DesCausa = "ÍNDICE DE DEFEITOS % =>",
							Disposicao = (((double) i.Sum(s => s.QtdDefeito)/totalProd.QtdProduzida.Value)*100.00).ToString("0.00"),
							Origem = "Total",
							Quantidade = i.Sum(s => s.QtdDefeito),
							FlgExibir = "S",
							Status = null
						}
						: new DefeitosLinha()
						{
							CodLinha = i.Key,
							CodCausa = "",
							DesCausa = "ÍNDICE DE DEFEITOS % =>",
							Disposicao = "0",
							Origem = "Total",
							Quantidade = i.Sum(s => s.QtdDefeito),
							FlgExibir = "S",
							Status = null
						};
				}).ToList();

			var defeitoslinha = defeitos.Select(d =>
			{
				var total = totais.FirstOrDefault(x => x.CodLinha == d.CodLinha);
				return total != null ? new DefeitosLinha
														 {
															 CodDefeito = d.CodDefeito,
															 CodLinha = d.CodLinha,
															 CodCausa = d.CodCausa,
															 DesCausa = d.DesCausa,
															 Posicao = d.Posicao,
															 DesDefeito = d.DesDefeito,
															 Origem = d.Origem,
															 Quantidade = d.QtdDefeito,
															 Taxa = Math.Round(((decimal)d.QtdDefeito / (decimal)(total.Quantidade)) * 100, 2),
															 CodModelo = d.CodModelo,
															 DesModelo = d.DesModelo,
															 Acao = d.DesAcao,
															 FlgExibir = "N",
															 Status = d.FlgRevisado.ToString()
														 } : null;
			}).ToList();

			defeitoslinha = defeitoslinha.OrderBy(x => x.CodModelo).ThenBy(x => x.CodDefeito).ThenBy(x => x.Acao).ToList();
			totais.ForEach(defeitoslinha.Add);

			var defeitosRel = daoDefeitos.ObterDefeitosRelatorio(DatInicio);
			foreach (var defeito in defeitosRel)
			{
				var coddefeito = defeito.CodDefeito;
				var codlinha = defeito.CodLinha;

				var defeitosaalterar = (from d in defeitoslinha
										where d.CodDefeito == coddefeito.Trim() && d.CodLinha == codlinha
										select d).ToList();

				if (defeitosaalterar.Any())
				{
					defeitosaalterar.ForEach(x => x.FlgExibir = "S");
				}
			}

			return defeitoslinha;
		}*/

		public static List<DefeitosLinha> ObterDefeitos(DateTime DatInicio, DateTime DatFim, int? Turno, string Familia, string EsconderDef)
		{
			var defeitosDTO = new List<viw_CBDefeitoTurnoDTO>();

			if (!String.IsNullOrEmpty(Familia))
			{
				defeitosDTO = daoDefeitos.ObterDefeitosFamilia(DatInicio, DatFim, Familia, "FEC");
			}
			else
			{
				defeitosDTO = daoDefeitos.ObterDefeitosFabrica(DatInicio, DatFim, "FEC");
			}

			if (Turno != null && Turno > 0)
			{
				defeitosDTO = defeitosDTO.Where(x => x.Turno == Turno).ToList();
			}

			var defeitos = defeitosDTO.GroupBy(g => new { g.CodLinha, g.CodDefeito, g.CodCausa, g.CodOrigem, g.Posicao, g.CodModelo, g.DesAcao, g.FlgRevisado }
				).Select(i => new DefeitosLinha
				{
					CodLinha = i.Key.CodLinha,
					CodDefeito = i.Key.CodDefeito,
					CodCausa = i.Key.CodCausa,
					Origem = i.Key.CodOrigem,
					Quantidade = i.Count(),
					Posicao = i.Key.Posicao,
					CodModelo = i.Key.CodModelo,
					Acao = i.Key.DesAcao,
					Status = i.Key.FlgRevisado != null ? i.Key.FlgRevisado.ToString() : "0"
				}
			).ToList();

			defeitos = defeitos.OrderBy(x => x.CodModelo).ThenBy(x => x.CodDefeito).ThenBy(x => x.Acao).ToList();

			var defeitosRel = daoDefeitos.ObterDefeitosRelatorio(DatInicio);

			var totais = defeitos.GroupBy(g => g.CodLinha).Select(i => new { CodLinha = i.Key, Qtde = i.Sum(x => x.Quantidade) }).ToList();

			string codModelo = "", desModelo = "";
			string codDefeito = "", desDefeito = "";
			int codlinha = 0; int totaldefeito = 0;

			foreach (var defeito in defeitos)
			{

				if (codlinha != defeito.CodLinha)
				{
					codlinha = defeito.CodLinha;
					totaldefeito = totais.FirstOrDefault(x => x.CodLinha == codlinha).Qtde;
				}

				if (codDefeito != defeito.CodDefeito)
				{
					desDefeito = daoDefeitos.ObterDesDefeito(defeito.CodDefeito);
					codDefeito = defeito.CodDefeito;
				}

				if (!String.IsNullOrEmpty(defeito.CodCausa))
				{
					defeito.DesCausa = daoDefeitos.ObterDesCausa(defeito.CodCausa);
				}

				if (!String.IsNullOrEmpty(defeito.Origem))
				{
					defeito.Origem = daoDefeitos.ObterDesOrigem(defeito.Origem.Trim().PadLeft(2, '0'));
				}

				if (codModelo != defeito.CodModelo)
				{
					desModelo = daoGeral.ObterDescResumidaItem(defeito.CodModelo);
					codModelo = defeito.CodModelo;
				}

				defeito.DesDefeito = desDefeito;
				defeito.DesModelo = desModelo;
				defeito.Taxa = Math.Round(((decimal)defeito.Quantidade / (decimal)totaldefeito) * 100, 2);

				var exibir = defeitosRel.FirstOrDefault(x => x.CodDefeito == defeito.CodDefeito && x.CodLinha == defeito.CodLinha);
				defeito.FlgExibir = exibir != null ? "S" : "N";

			}
			if (!String.IsNullOrEmpty(EsconderDef))
			{
				return defeitos;
			}
			else
			{
				return defeitos.Where(x => x.FlgExibir == "S").ToList();
			}

		}

		public static List<IndiceDefeitoLinha> ObterIndiceDefeito(DateTime DatInicio, DateTime DatFim, int? Turno, string Familia){

			var linhas = daoResumoMF.LinhasMF(DateTime.Parse(DatInicio.ToShortDateString()));
			List<IndiceDefeitoLinha> indice = new List<IndiceDefeitoLinha>();

			foreach (var linha in linhas){
				var posto = daoPassagem.ObterPrimeiroPostoLeitura(linha.CodLinha);
				var qtdLeitura = daoPassagem.ObterQtdLeiturasPosto(DatInicio, DatFim, linha.CodLinha, posto);
				var qtdEmbalado = daoRelatorioTecnico.ObterProducaoLinha(DateTime.Parse(DatInicio.ToShortDateString()), DateTime.Parse(DatFim.ToShortDateString()), linha.CodLinha);
				var qtdDefeito = daoDefeitos.ObterQtdDefeitosLinha(DateTime.Parse(DatInicio.ToShortDateString()), DateTime.Parse(DatFim.ToShortDateString()), linha.CodLinha);
				var qtdFalsaFalha = daoDefeitos.ObterQtdDefeitosFFLinha(DateTime.Parse(DatInicio.ToShortDateString()), DateTime.Parse(DatFim.ToShortDateString()), linha.CodLinha);

				var indlinha = new IndiceDefeitoLinha(){
					CodLinha = linha.CodLinha,
					QtdProducao = qtdLeitura,
					QtdEmbalado = qtdEmbalado,
					QtdDefeito = qtdDefeito,
					QtdFalsaFalha = qtdFalsaFalha
				};

				indice.Add(indlinha);
			}

			return indice;
		}

		public static CBLinhaObservacaoDTO GravaObservacaoLinha(CBLinhaObservacaoDTO observacao)
		{
			return daoResumoMF.GravaObservacaoLinha(observacao);
		}

		public static void IncluirDefeitoRelatorio(int CodLinha, DateTime DatProducao, string CodDefeito)
		{
			daoDefeitos.IncluirDefeitoRelatorio(CodLinha, DatProducao, CodDefeito);
		}

		public static void RemoveDefeitoRelatorio(int CodLinha, DateTime DatProducao, string CodDefeito)
		{
			daoDefeitos.RemoveDefeitoRelatorio(CodLinha, DatProducao, CodDefeito);
		}
	}
}
