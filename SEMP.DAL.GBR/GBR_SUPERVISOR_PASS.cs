namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_SUPERVISOR_PASS
    {
        [Key]
        public int COD_PASS { get; set; }

        [StringLength(30)]
        public string USUARIO { get; set; }

        [StringLength(30)]
        public string SENHA { get; set; }

        public bool? PERMITE_REIMPRESSAO { get; set; }
    }
}
