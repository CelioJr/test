using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBAcao
    {
        public int CodAcao { get; set; }
        public string Descricao { get; set; }
        public string Tipo { get; set; }
        public Nullable<bool> FlgAtivo { get; set; }
    }
}
