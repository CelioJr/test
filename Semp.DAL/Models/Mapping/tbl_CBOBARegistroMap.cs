using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBOBARegistroMap : EntityTypeConfiguration<tbl_CBOBARegistro>
    {
        public tbl_CBOBARegistroMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodLinha, t.NumPosto, t.CodFab, t.NumRPA, t.CodPallet });

            // Properties
            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumPosto)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NumRPA)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodPallet)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodModelo)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

			this.Property(t => t.CodOperador)
				.IsFixedLength()
				.HasMaxLength(30)
                .IsUnicode(false);

			this.Property(t => t.FlgStatus)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

			this.Property(t => t.CodLinhaOri)
							.IsFixedLength()
							.HasMaxLength(8)
                .IsUnicode(false);

			this.Property(t => t.NumRPE)
							.IsFixedLength()
							.HasMaxLength(6)
                .IsUnicode(false);

			this.Property(t => t.ObsRevisao)
				.IsFixedLength()
				.HasMaxLength(500)
                .IsUnicode(false);

			// Table & Column Mappings
			this.ToTable("tbl_CBOBARegistro");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.NumPosto).HasColumnName("NumPosto");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.NumRPA).HasColumnName("NumRPA");
            this.Property(t => t.CodPallet).HasColumnName("CodPallet");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.QtdPallet).HasColumnName("QtdPallet");
            this.Property(t => t.QtdAmostras).HasColumnName("QtdAmostras");
            this.Property(t => t.DatProducao).HasColumnName("DatProducao");
            this.Property(t => t.DatAbertura).HasColumnName("DatAbertura");
            this.Property(t => t.DatFechamento).HasColumnName("DatFechamento");
			this.Property(t => t.CodOperador).HasColumnName("CodOperador");
			this.Property(t => t.FlgStatus).HasColumnName("FlgStatus");
			this.Property(t => t.CodLinhaOri).HasColumnName("CodLinhaOri");
			this.Property(t => t.NumRPE).HasColumnName("NumRPE");
			this.Property(t => t.DatLiberado).HasColumnName("DatLiberado");
			this.Property(t => t.ObsRevisao).HasColumnName("ObsRevisao");

		}
	}
}
