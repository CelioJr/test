using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_CQTF_SEQ_DTO
    {
        public int COD_CQTF { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
    }
}
