using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_LOG_PRODUCT_DTO
    {
        public string serial { get; set; }
        public string imei1 { get; set; }
        public string imei2 { get; set; }
        public string suid { get; set; }
        public Nullable<System.DateTime> system_date { get; set; }
        public string sd_receiopt { get; set; }
        public Nullable<System.DateTime> file_date { get; set; }
    }
}
