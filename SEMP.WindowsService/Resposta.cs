﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.WindowsService
{
	public class Resposta
	{
		public int Codigo { get; set; }
		public string Msg { get; set; }

		public bool IsProblem()
		{
			return !Codigo.Equals(0);
		}
	}
}
