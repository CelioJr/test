using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
	public class tbl_LocalMap : EntityTypeConfiguration<tbl_Local>
	{
		public tbl_LocalMap()
		{
			// Primary Key
			this.HasKey(t => t.CodLocal);

			// Properties
			this.Property(t => t.CodLocal)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(3)
                .IsUnicode(false);

			this.Property(t => t.CodFab)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(2)
                .IsUnicode(false);

			this.Property(t => t.DesLocal)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(30)
                .IsUnicode(false);

			this.Property(t => t.FlgEndereco)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			this.Property(t => t.FlgAtivo)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			this.Property(t => t.FlgContabil)
				.IsFixedLength()
				.HasMaxLength(1)
				.IsUnicode(false);

			this.Property(t => t.FlgDisponivel)
				.IsFixedLength()
				.HasMaxLength(1)
				.IsUnicode(false);

			this.Property(t => t.FlgProdAcabado)
				.IsFixedLength()
				.HasMaxLength(1)
				.IsUnicode(false);

			// Table & Column Mappings
			this.ToTable("tbl_Local");
			this.Property(t => t.CodLocal).HasColumnName("CodLocal");
			this.Property(t => t.CodFab).HasColumnName("CodFab");
			this.Property(t => t.DesLocal).HasColumnName("DesLocal");
			this.Property(t => t.FlgEndereco).HasColumnName("FlgEndereco");
			this.Property(t => t.FlgAtivo).HasColumnName("FlgAtivo");
			this.Property(t => t.FlgContabil).HasColumnName("FlgContabil");
			this.Property(t => t.FlgDisponivel).HasColumnName("FlgDisponivel");
			this.Property(t => t.FlgProdAcabado).HasColumnName("FlgProdAcabado");
			this.Property(t => t.Rowguid).HasColumnName("Rowguid");
		}
	}
}
