namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_t_refugo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_t_refugo()
        {
            dw_consolre = new HashSet<dw_consolre>();
            om_cfg = new HashSet<om_cfg>();
        }

        [Key]
        public long id_trefugo { get; set; }

        [StringLength(30)]
        public string cd_trefugo { get; set; }

        public int? revisao { get; set; }

        [StringLength(40)]
        public string ds_trefugo { get; set; }

        public DateTime? dt_revisao { get; set; }

        public short? st_ativo { get; set; }

        public DateTime? dt_stativo { get; set; }

        public long id_tppt { get; set; }

        public long id_usrstativo { get; set; }

        public long id_usrrevisao { get; set; }

        public long? id_area { get; set; }

        public short? is_requer_acao { get; set; }

        public short? is_requer_causa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolre> dw_consolre { get; set; }

        public virtual dw_t_area dw_t_area { get; set; }

        public virtual om_tppt om_tppt { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg { get; set; }
    }
}
