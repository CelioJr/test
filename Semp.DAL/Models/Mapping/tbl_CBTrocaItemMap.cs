﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
	public class tbl_CBTrocaItemMap : EntityTypeConfiguration<tbl_CBTrocaItem>
	{
		public tbl_CBTrocaItemMap()
		{
			// Primary Key
			this.HasKey(t => t.CodRetrabalho);

			// Properties
			this.Property(t => t.CodRetrabalho)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.CodItemAnterior)
				.IsFixedLength()
				.HasMaxLength(6);

			this.Property(t => t.CodItemNovo)
				.IsFixedLength()
				.HasMaxLength(6);

			// Table & Column Mappings
			this.ToTable("tbl_CBTrocaItem");
			this.Property(t => t.CodRetrabalho).HasColumnName("CodRetrabalho");
			this.Property(t => t.CodItemAnterior).HasColumnName("CodItemAnterior");
			this.Property(t => t.CodItemNovo).HasColumnName("CodItemNovo");
			this.Property(t => t.TipoAmarra).HasColumnName("TipoAmarra");

			// Relationships
			this.HasRequired(t => t.tbl_CBRetrabalho)
				.WithOptional(t => t.tbl_CBTrocaItem);

		}
	}

}
