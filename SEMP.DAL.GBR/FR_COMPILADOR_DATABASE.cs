namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_COMPILADOR_DATABASE
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(80)]
        public string CPL_DESCRITOR { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        public string DBA_CODIGO { get; set; }

        [StringLength(2000)]
        public string CDB_SINTAXE { get; set; }

        public virtual FR_COMPILADOR FR_COMPILADOR { get; set; }

        public virtual FR_DATABASE FR_DATABASE { get; set; }
    }
}
