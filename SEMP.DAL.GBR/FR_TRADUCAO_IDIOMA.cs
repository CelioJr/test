namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_TRADUCAO_IDIOMA
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TRA_CODIGO { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IDI_CODIGO { get; set; }

        [Required]
        [StringLength(2000)]
        public string TRI_TEXTO { get; set; }

        [Required]
        [StringLength(1)]
        public string TRI_SITUACAO { get; set; }

        [StringLength(40)]
        public string TRI_HASH { get; set; }

        public virtual FR_IDIOMA FR_IDIOMA { get; set; }

        public virtual FR_TRADUCAO FR_TRADUCAO { get; set; }
    }
}
