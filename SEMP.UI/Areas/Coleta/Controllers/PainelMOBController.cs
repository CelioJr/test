﻿using RAST.BO.Mobilidade;
using RAST.BO.Relatorios;
using SEMP.Model.VO.Mobilidade;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SEMP.Areas.Coleta.Controllers
{
	public class PainelMOBController : Controller
	{
		// GET: Coleta/PainelMOB
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult Agora()
		{
			var painel = PainelProducao.ObterPainelProducaoMOB();
			return View(painel);
		}

		public ActionResult Mes()
		{
			var dIni = DateTime.Now.AddDays(-DateTime.Now.Day).AddDays(1);
			var dFim = dIni.AddMonths(1).AddDays(-1);

			var alcatel = Alcatel.ObterResumoAlcatel(dIni, dFim);
			var engine = Alcatel.ObterResumoAlcatelEngine(dIni, dFim);

			alcatel.AddRange(engine);

			var resumo = alcatel.GroupBy(x => x.SKU)
					.Select(x => new ResumoProducaoAlcatel()
					{
						SKU = x.Key,
						QTD_PLAN = x.Sum(y => y.QTD_PLAN),
						QTD_PROD = x.Sum(y => y.QTD_PROD)
					})
					.ToList();

			return View(resumo);
		}

		public JsonResult GraficoMes()
		{
			var dIni = DateTime.Now.AddDays(-DateTime.Now.Day).AddDays(1);
			var dFim = dIni.AddMonths(1).AddDays(-1);

			var alcatel = Alcatel.ObterResumoAlcatel(dIni, dFim);
			var engine = Alcatel.ObterResumoAlcatelEngine(dIni, dFim);

			alcatel.AddRange(engine);

			var resumo = alcatel.GroupBy(x => x.SKU)
					.Select(x => new
					{
						SKU = x.Key,
						QTD_PLAN = x.Sum(y => y.QTD_PLAN),
						QTD_PROD = x.Sum(y => y.QTD_PROD)
					})
					.ToList();

			return Json(resumo, JsonRequestBehavior.AllowGet);
		}

		public ActionResult HoraAHora(string codLinha)
		{
			return View();
		}

		public ActionResult HoraAHoraCSV(string Data, string codLinha)
		{
			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			if (!String.IsNullOrEmpty(Data))
			{
				dIni = DateTime.Parse(Data);
			}

			var dados = Alcatel.ObterProducaoHoraAHora(dIni, codLinha);

			return View(dados);

		}

		public ActionResult Painel(int? codlinha)
		{
			ViewBag.CodLinha = codlinha;
			return View();
		}

		public JsonResult ObterPainel(int codLinha)
		{

			var painel = Alcatel.ObterPainelProducaoMOB(codLinha.ToString()).FirstOrDefault();


			return Json(painel, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterResumoPainel(int codlinha)
		{

			List<ResumoProducao> resumo;
			/*var datIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var datFim = datIni.AddDays(1).AddMinutes(-1);*/

			//var linha = Alcatel.ObterPainelProducaoMOB(codlinha.ToString());

			resumo = Alcatel.ObterResumoMFLinha(codlinha);

			/*var defeitos = Defeito.ObterDefeitosRelRef(datIni, datFim, codlinha);

			var dados = defeitos.GroupBy(x => x.CodModelo)
								.Select(x => new
								{
									CodModelo = x.Key,
									QtdDefeitos = x.Count(),
									QtdFF = x.Count(y => y.CodCausa.StartsWith("WX"))
								})
								.ToList();

			foreach (var item in resumo)
			{
				var def = dados.FirstOrDefault(y => y.CodModelo == item.CodModelo);
				if (def != null)
				{
					item.QtdDefeito = def.QtdDefeitos;
					item.QtdFalsaFalha = def.QtdFF;
				}

			}*/

			return Json(resumo, JsonRequestBehavior.AllowGet);
		}
	}
}