using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.DTB_SSA.Models.Mapping
{
    public class view_NumSerieMap : EntityTypeConfiguration<view_NumSerie>
    {
        public view_NumSerieMap()
        {
            // Primary Key
            this.HasKey(t => new { t.DesSerie, t.CodOF, t.CodModelo, t.Etiqueta, t.PrecisaConfig, t.PrecisaEAN, t.PrecisaNomFantasia, t.codAtual, t.vinil, t.numQtde });

            // Properties
            this.Property(t => t.DesSerie)
                .IsRequired()
                .HasMaxLength(13);

            this.Property(t => t.CodOF)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TipoMaquina)
                .HasMaxLength(8);

            this.Property(t => t.CodModelo)
                .IsRequired()
                .HasMaxLength(13);

            this.Property(t => t.DesTitulo)
                .HasMaxLength(20);

            this.Property(t => t.DesConfig)
                .HasMaxLength(750);

            this.Property(t => t.DesModelo)
                .HasMaxLength(150);

            this.Property(t => t.PartNumber)
                .HasMaxLength(50);

            this.Property(t => t.Etiqueta)
                .IsRequired()
                .HasMaxLength(11);

            this.Property(t => t.PrecisaConfig)
                .IsRequired()
                .HasMaxLength(3);

            this.Property(t => t.PrecisaEAN)
                .IsRequired()
                .HasMaxLength(3);

            this.Property(t => t.PrecisaNomFantasia)
                .IsRequired()
                .HasMaxLength(3);

            this.Property(t => t.codSO)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.OpeSepAlmox)
                .HasMaxLength(50);

            this.Property(t => t.OpeSubEstoque)
                .HasMaxLength(50);

            this.Property(t => t.OpeMontagem)
                .HasMaxLength(50);

            this.Property(t => t.OpeRunIn)
                .HasMaxLength(50);

            this.Property(t => t.OpeReparo)
                .HasMaxLength(50);

            this.Property(t => t.OpeLiberacao)
                .HasMaxLength(50);

            this.Property(t => t.OpeEmbalagem)
                .HasMaxLength(50);

            this.Property(t => t.OpeAprovacaoCQ)
                .HasMaxLength(50);

            this.Property(t => t.OpeEstoque)
                .HasMaxLength(50);

            this.Property(t => t.OpeDat10)
                .HasMaxLength(50);

            this.Property(t => t.OpeDownload)
                .HasMaxLength(50);

            this.Property(t => t.CODEAN)
                .HasMaxLength(13);

            this.Property(t => t.descliente)
                .HasMaxLength(30);

            this.Property(t => t.numQtde)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.LicencaWindows)
                .HasMaxLength(29);

            this.Property(t => t.Tipo_Linha)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.NomSO)
                .HasMaxLength(30);

            this.Property(t => t.ExpressaoHCL)
                .HasMaxLength(10);

            this.Property(t => t.LicencaSoftware)
                .HasMaxLength(29);

            this.Property(t => t.ModeloVPE)
                .HasMaxLength(16);

            this.Property(t => t.NumSerieToshiba)
                .HasMaxLength(9);

            this.Property(t => t.OpeWakeUp)
                .HasMaxLength(128);

            // Table & Column Mappings
            this.ToTable("view_NumSerie");
            this.Property(t => t.DesSerie).HasColumnName("DesSerie");
            this.Property(t => t.CodOF).HasColumnName("CodOF");
            this.Property(t => t.TipoMaquina).HasColumnName("TipoMaquina");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.DesTitulo).HasColumnName("DesTitulo");
            this.Property(t => t.DesConfig).HasColumnName("DesConfig");
            this.Property(t => t.DesModelo).HasColumnName("DesModelo");
            this.Property(t => t.PartNumber).HasColumnName("PartNumber");
            this.Property(t => t.Etiqueta).HasColumnName("Etiqueta");
            this.Property(t => t.PrecisaConfig).HasColumnName("PrecisaConfig");
            this.Property(t => t.PrecisaEAN).HasColumnName("PrecisaEAN");
            this.Property(t => t.PrecisaNomFantasia).HasColumnName("PrecisaNomFantasia");
            this.Property(t => t.codOfMae).HasColumnName("codOfMae");
            this.Property(t => t.codSO).HasColumnName("codSO");
            this.Property(t => t.datSepAlmox).HasColumnName("datSepAlmox");
            this.Property(t => t.OpeSepAlmox).HasColumnName("OpeSepAlmox");
            this.Property(t => t.dat1).HasColumnName("dat1");
            this.Property(t => t.codope1).HasColumnName("codope1");
            this.Property(t => t.OpeSubEstoque).HasColumnName("OpeSubEstoque");
            this.Property(t => t.dat2).HasColumnName("dat2");
            this.Property(t => t.codope2).HasColumnName("codope2");
            this.Property(t => t.OpeMontagem).HasColumnName("OpeMontagem");
            this.Property(t => t.dat3).HasColumnName("dat3");
            this.Property(t => t.codope3).HasColumnName("codope3");
            this.Property(t => t.OpeRunIn).HasColumnName("OpeRunIn");
            this.Property(t => t.dat4).HasColumnName("dat4");
            this.Property(t => t.codope4).HasColumnName("codope4");
            this.Property(t => t.OpeReparo).HasColumnName("OpeReparo");
            this.Property(t => t.dat5).HasColumnName("dat5");
            this.Property(t => t.codope5).HasColumnName("codope5");
            this.Property(t => t.OpeLiberacao).HasColumnName("OpeLiberacao");
            this.Property(t => t.dat6).HasColumnName("dat6");
            this.Property(t => t.codOpe6).HasColumnName("codOpe6");
            this.Property(t => t.OpeEmbalagem).HasColumnName("OpeEmbalagem");
            this.Property(t => t.datAprovacaoCQ).HasColumnName("datAprovacaoCQ");
            this.Property(t => t.CodOpeAprovacaoCQ).HasColumnName("CodOpeAprovacaoCQ");
            this.Property(t => t.OpeAprovacaoCQ).HasColumnName("OpeAprovacaoCQ");
            this.Property(t => t.dat7).HasColumnName("dat7");
            this.Property(t => t.codope7).HasColumnName("codope7");
            this.Property(t => t.OpeEstoque).HasColumnName("OpeEstoque");
            this.Property(t => t.codAtual).HasColumnName("codAtual");
            this.Property(t => t.dat10).HasColumnName("dat10");
            this.Property(t => t.codope10).HasColumnName("codope10");
            this.Property(t => t.OpeDat10).HasColumnName("OpeDat10");
            this.Property(t => t.vinil).HasColumnName("vinil");
            this.Property(t => t.datDownload).HasColumnName("datDownload");
            this.Property(t => t.codOpeDownload).HasColumnName("codOpeDownload");
            this.Property(t => t.OpeDownload).HasColumnName("OpeDownload");
            this.Property(t => t.CODEAN).HasColumnName("CODEAN");
            this.Property(t => t.descliente).HasColumnName("descliente");
            this.Property(t => t.numQtde).HasColumnName("numQtde");
            this.Property(t => t.LicencaWindows).HasColumnName("LicencaWindows");
            this.Property(t => t.datEmbalPosto1).HasColumnName("datEmbalPosto1");
            this.Property(t => t.LinhaMontagem).HasColumnName("LinhaMontagem");
            this.Property(t => t.Tipo_Linha).HasColumnName("Tipo Linha");
            this.Property(t => t.NomSO).HasColumnName("NomSO");
            this.Property(t => t.ExpressaoHCL).HasColumnName("ExpressaoHCL");
            this.Property(t => t.LicencaSoftware).HasColumnName("LicencaSoftware");
            this.Property(t => t.imagem_id).HasColumnName("imagem_id");
            this.Property(t => t.ModeloVPE).HasColumnName("ModeloVPE");
            this.Property(t => t.NumSerieToshiba).HasColumnName("NumSerieToshiba");
            this.Property(t => t.datWakeUp).HasColumnName("datWakeUp");
            this.Property(t => t.codOpeWakeUp).HasColumnName("codOpeWakeUp");
            this.Property(t => t.OpeWakeUp).HasColumnName("OpeWakeUp");
            this.Property(t => t.datLiberacao).HasColumnName("datLiberacao");
            this.Property(t => t.codOpeLiberacao).HasColumnName("codOpeLiberacao");
        }
    }
}
