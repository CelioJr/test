namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolciplog
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_consolciplog()
        {
            dw_consolcipoco = new HashSet<dw_consolcipoco>();
        }

        [Key]
        public long id_consolciplog { get; set; }

        public DateTime? dthr_icip { get; set; }

        public DateTime? dthr_fcip { get; set; }

        public long? id_consolid_inicio { get; set; }

        public long? id_consolid_fim { get; set; }

        public long? id_pt { get; set; }

        public long? id_usrEntrada { get; set; }

        public long? id_usrSaida { get; set; }

        public long? id_consolpalogEntrada { get; set; }

        public long? id_consolpalogSaida { get; set; }

        public virtual dw_consolid dw_consolid { get; set; }

        public virtual dw_consolid dw_consolid1 { get; set; }

        public virtual dw_consolpalog dw_consolpalog { get; set; }

        public virtual dw_consolpalog dw_consolpalog1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolcipoco> dw_consolcipoco { get; set; }

        public virtual om_pt om_pt { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }
    }
}
