using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class viw_Item
    {
        public string CodItem { get; set; }
        public string DesItem { get; set; }
		public string Origem { get; set; }
		public string CodFam { get; set; }
	}
}
