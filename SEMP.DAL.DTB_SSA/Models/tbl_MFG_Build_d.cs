namespace SEMP.DAL.DTB_SSA.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_MFG_Build_d
    {
        public tbl_MFG_Build_d()
        {
            this.tbl_Key_Component_d = new HashSet<tbl_Key_Component_d>();
        }
    
        public string Model_Number { get; set; }
        public string Serial_Number { get; set; }
        public string MFG_Country_Code { get; set; }
        public string Sold_Country_Code { get; set; }
        public string Asset_Tag_Number { get; set; }
        public string Production_Date { get; set; }
        public string Ship_Date { get; set; }
        public string Sales_Out_Date { get; set; }
        public System.DateTime Data_Registro { get; set; }
    
        public virtual ICollection<tbl_Key_Component_d> tbl_Key_Component_d { get; set; }
    }
}
