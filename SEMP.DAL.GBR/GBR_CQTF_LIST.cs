namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_CQTF_LIST
    {
        [Key]
        public int COD_CQTF { get; set; }

        [StringLength(20)]
        public string LISTA { get; set; }

        public int? TIPO { get; set; }

        public DateTime? DATA { get; set; }

        public bool? LIDO { get; set; }
    }
}
