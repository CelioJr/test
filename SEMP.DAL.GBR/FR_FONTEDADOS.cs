namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_FONTEDADOS
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FNT_CODIGO { get; set; }

        public int? FNT_CODIGO_PARENT { get; set; }

        [StringLength(100)]
        public string FNT_CAMPOCHAVE { get; set; }

        [StringLength(30)]
        public string FNT_TABELA { get; set; }

        [StringLength(6000)]
        public string FNT_SQLSELECT { get; set; }

        [StringLength(6000)]
        public string FNT_SQLINSERT { get; set; }

        [StringLength(6000)]
        public string FNT_SQLUPDATE { get; set; }

        [StringLength(6000)]
        public string FNT_SQLDELETE { get; set; }

        [StringLength(100)]
        public string FNT_CAMPOINCREMENTO { get; set; }

        public int FRM_CODIGO { get; set; }

        [StringLength(6000)]
        public string FNT_CAMPOGRADE { get; set; }

        [StringLength(6000)]
        public string FNT_CAMPOPESQUISA { get; set; }

        [StringLength(6000)]
        public string FNT_SQLDEFAULT { get; set; }

        [StringLength(2000)]
        public string FNT_SQLMASCARA { get; set; }

        public virtual FR_FORMULARIO FR_FORMULARIO { get; set; }
    }
}
