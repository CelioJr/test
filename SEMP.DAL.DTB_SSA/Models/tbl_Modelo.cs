using System;
using System.Collections.Generic;

namespace SEMP.DAL.DTB_SSA.Models
{
    public partial class tbl_Modelo
    {
        public tbl_Modelo()
        {
            this.tbl_OF = new List<tbl_OF>();
        }

        public string codModelo { get; set; }
        public string CODEAN { get; set; }
        public string desTitulo { get; set; }
        public string desConfig { get; set; }
        public string desModelo { get; set; }
        public string codComercial { get; set; }
        public Nullable<int> codOperador { get; set; }
        public Nullable<System.DateTime> datAtualizacao { get; set; }
        public string desPartNumber { get; set; }
        public string ModeloVPE { get; set; }
        public string flgColetaSerial { get; set; }
        public virtual tbl_Operador tbl_Operador { get; set; }
        public virtual ICollection<tbl_OF> tbl_OF { get; set; }
    }
}
