using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_EntMovRecMap : EntityTypeConfiguration<tbl_EntMovRec>
    {
        public tbl_EntMovRecMap()
        {
            // Primary Key
            this.HasKey(t => new { t.NumRomaneio, t.CodItem, t.DatReceb });

            // Properties
            this.Property(t => t.NumRomaneio)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodItem)
                .IsRequired()
                .HasMaxLength(15)
                .IsUnicode(false);

            this.Property(t => t.NomUsuario)
                .HasMaxLength(50)
                .IsUnicode(false);

            this.Property(t => t.NomMaquina)
                .HasMaxLength(30)
                .IsUnicode(false);

            this.Property(t => t.CodModelo)
                .HasMaxLength(15)
                .IsUnicode(false);

            this.Property(t => t.CodDefeito)
                .HasMaxLength(15)
                .IsUnicode(false);

            this.Property(t => t.CodStatus)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.DocInt)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.Ciente)
                .HasMaxLength(50)
                .IsUnicode(false);

            this.Property(t => t.JustCiente)
                .HasMaxLength(150)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_EntMovRec");
            this.Property(t => t.NumRomaneio).HasColumnName("NumRomaneio");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.QtdRec).HasColumnName("QtdRec");
            this.Property(t => t.DatReceb).HasColumnName("DatReceb");
            this.Property(t => t.NomUsuario).HasColumnName("NomUsuario");
            this.Property(t => t.NomMaquina).HasColumnName("NomMaquina");
            this.Property(t => t.NumControle).HasColumnName("NumControle");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.CodDefeito).HasColumnName("CodDefeito");
            this.Property(t => t.CodStatus).HasColumnName("CodStatus");
            this.Property(t => t.DocInt).HasColumnName("DocInt");
            this.Property(t => t.QtdLaudo).HasColumnName("QtdLaudo");
            this.Property(t => t.Ciente).HasColumnName("Ciente");
            this.Property(t => t.JustCiente).HasColumnName("JustCiente");
            this.Property(t => t.QtdPaga).HasColumnName("QtdPaga");
        }
    }
}
