namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_webcam
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_webcam()
        {
            om_obj = new HashSet<om_obj>();
        }

        [Key]
        public long id_webcam { get; set; }

        [StringLength(255)]
        public string url { get; set; }

        public DateTime? dt_revisao { get; set; }

        public DateTime? dt_stativo { get; set; }

        public short st_ativo { get; set; }

        [StringLength(30)]
        public string cd_webcam { get; set; }

        public int? revisao { get; set; }

        [StringLength(100)]
        public string ds_webcam { get; set; }

        [StringLength(10)]
        public string ds_curta { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_obj> om_obj { get; set; }
    }
}
