namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_nserie
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_nserie()
        {
            dw_nserieobs = new HashSet<dw_nserieobs>();
            dw_passagem2 = new HashSet<dw_passagem>();
            pp_cpnserie = new HashSet<pp_cpnserie>();
        }

        [Key]
        public long id_nserie { get; set; }

        [StringLength(40)]
        public string cb { get; set; }

        [StringLength(40)]
        public string ns { get; set; }

        public long? id_produto { get; set; }

        public long? id_est { get; set; }

        public long? id_passagem { get; set; }

        public long? id_passagemTF { get; set; }

        public int? sequencial { get; set; }

        public virtual dw_est dw_est { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_nserieobs> dw_nserieobs { get; set; }

        public virtual dw_passagem dw_passagem { get; set; }

        public virtual dw_passagem dw_passagem1 { get; set; }

        public virtual om_produto om_produto { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_passagem> dw_passagem2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cpnserie> pp_cpnserie { get; set; }
    }
}
