﻿namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
	public class IndicadorMF
	{
		public int Indicador { get; set; }
		public string Titulo { get; set; }
		public decimal Real { get; set; }
		public decimal Plan { get; set; }
		public string CodLinha { get; set; }
		
		public IndicadorMF(){}

		public IndicadorMF(int indicador, string titulo) 
		{
			Indicador = indicador;
			Titulo = titulo;
		}
	}
}
