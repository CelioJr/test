namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_USUARIO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FR_USUARIO()
        {
            FR_USUARIO_SISTEMA = new HashSet<FR_USUARIO_SISTEMA>();
            FR_GRUPO = new HashSet<FR_GRUPO>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int USR_CODIGO { get; set; }

        [Required]
        [StringLength(20)]
        public string USR_LOGIN { get; set; }

        [StringLength(64)]
        public string USR_SENHA { get; set; }

        [Required]
        [StringLength(1)]
        public string USR_ADMINISTRADOR { get; set; }

        [Required]
        [StringLength(1)]
        public string USR_TIPO_EXPIRACAO { get; set; }

        public int? USR_DIAS_EXPIRACAO { get; set; }

        public byte[] USR_IMAGEM_DIGITAL { get; set; }

        public byte[] USR_FOTO { get; set; }

        [Required]
        [StringLength(60)]
        public string USR_NOME { get; set; }

        [StringLength(120)]
        public string USR_EMAIL { get; set; }

        public long? USR_DIGITAL { get; set; }

        public DateTime? USR_INICIO_EXPIRACAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_USUARIO_SISTEMA> FR_USUARIO_SISTEMA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_GRUPO> FR_GRUPO { get; set; }
    }
}
