using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBModeloMap : EntityTypeConfiguration<tbl_CBModelo>
    {
        public tbl_CBModeloMap()
        {
            // Primary Key
            this.HasKey(t => t.CodModelo);

            // Properties
            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.Setor)
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.Fase)
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.Mascara)
                .HasMaxLength(200)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_CBModelo");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.Setor).HasColumnName("Setor");
            this.Property(t => t.Fase).HasColumnName("Fase");
            this.Property(t => t.QtdCaixa).HasColumnName("QtdCaixa");
            this.Property(t => t.QtdLote).HasColumnName("QtdLote");
            this.Property(t => t.QtdAmostrasOBA).HasColumnName("QtdAmostrasOBA");
            this.Property(t => t.FlgControlaFifo).HasColumnName("FlgControlaFifo");
            this.Property(t => t.TempoCiclo).HasColumnName("TempoCiclo");
            this.Property(t => t.Mascara).HasColumnName("Mascara");
            this.Property(t => t.PostoEntrada).HasColumnName("PostoEntrada");
        }
    }
}
