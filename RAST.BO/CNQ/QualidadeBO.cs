﻿using SEMP.DAL.DAO.CNQ;
using SEMP.Model.VO.CNQ;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAST.BO.CNQ
{
    public class QualidadeBO
    {
        public static DefeitosGeralIAC GetIndiceGeral(string data, string fase, string familia, string tipo)
        {
            return daoQualidade.GetIndices(data, fase, familia, tipo, true);
        }

        public static DefeitosGeralIAC GetIndices(string data, string fase, string familia, string tipo)
        {
            return daoQualidade.GetIndices(data, fase, familia, tipo);
        }

        public static DefeitosGeralIAC GetDefeitosPorFase(string data, string fase, string familia)
        {
            return daoQualidade.GetDefeitosPorFase(data, fase, familia);
        }

        public static List<MaioresDefeitos> GetMaioresDefeitos(DateTime dataInicial, DateTime dataFinal, string fase, string familia)
        {
            return daoQualidade.GetMaioresDefeitos(dataInicial, dataFinal, fase, familia);
        }

        public static int GetTotalProduzido(DateTime dataInicial, DateTime dataFinal, string fase, string familia)
        {
            return daoQualidade.GetTotalProduzido(dataInicial, dataFinal, fase, familia);
        }

        public static DefeitosGeralIAC GetDefeitoGeralIAC(string data)
        {
            return daoQualidade.GetDefeitoGeralIAC(data);
        }

        public static List<MaioresDefeitos> GetMaioresDefeitosIACINSPECAO(DateTime dataInicial, DateTime dataFinal, string tipo)
        {
            return daoQualidade.GetMaioresDefeitosIACINSPECAO(dataInicial, dataFinal, tipo);
        }

        public static int GetQuantidadeProduzidaIAC(DateTime dataInicial, DateTime dataFinal, string tipo)
        {
            return daoQualidade.GetQuantidadeProduzidaIAC(dataInicial, dataFinal, tipo);
        }

        public static int GetQuantidadeDefeitoIAC(DateTime dataInicial, DateTime dataFinal, string tipo)
        {
            return daoQualidade.GetQuantidadeDefeitoIAC(dataInicial, dataFinal, tipo);
        }

        public static int GetQuantidadeProduzidaIACPorLinha(DateTime dataInicial, DateTime dataFinal, int CodLinha)
        {
            return daoQualidade.GetQuantidadeProduzidaIACPorLinha(dataInicial,dataFinal,CodLinha);
        }

        public static decimal GetQuantidadeProduzidaPorLinhaEHora(
            DateTime DataInicio,
            DateTime DataFim,
            TimeSpan HoraInicio,
            TimeSpan HoraFim,
            int Turno,
            int CodLinha)
        {
            return daoQualidade.GetQuantidadeProduzidaPorLinhaEHora(DataInicio, DataFim, HoraInicio, HoraFim, Turno, CodLinha);
        }

        public static int GetQuantidadeDePontosPorLinhaEHora(DateTime DataInicio,
            DateTime DataFim,
            TimeSpan HoraInicio,
            TimeSpan HoraFim,
            int Turno,
            int CodLinha)
        {
            return daoQualidade.GetQuantidadeDePontosPorLinhaEHora(DataInicio, DataFim, HoraInicio, HoraFim, Turno, CodLinha);
        }

        public static int GetQuantidadeDefeitoIACPorLinha(DateTime dataInicial, DateTime dataFinal, int CodLinha)
        {
            return daoQualidade.GetQuantidadeDefeitoIACPorLinha(dataInicial, dataFinal, CodLinha);
        }

        public static List<MaioresDefeitos> GetMaioresDefeitosIACPorLinha(DateTime dataInicial, DateTime dataFinal, int CodLinha)
        {
            return daoQualidade.GetMaioresDefeitosIACPorLinha(dataInicial, dataFinal, CodLinha);
        }

        public static int GetQuantidadeDePontos(DateTime dataInicial, DateTime dataFinal, int CodLinha)
        {
            return daoQualidade.GetQuantidadeDePontos(dataInicial, dataFinal, CodLinha);
        }

        public static int GetQuantidadeDePontosGeral(DateTime dataInicial, DateTime dataFinal, string tipo)
        {
            return daoQualidade.GetQuantidadeDePontosGeral(dataInicial, dataFinal, tipo);
        }

        public static List<RelatorioInspecaoIACModeloLinha> GetModelosLinhasIAC(DateTime dataInicial, DateTime dataFinal, int CodLinha)
        {
            return daoQualidade.GetModelosLinhasIAC(dataInicial, dataFinal, CodLinha);
        }
    }
}
