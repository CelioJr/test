namespace SEMP.DAL.Models
{
    using System;
    
    public partial class pcRede
    {
        public decimal id { get; set; }
        public decimal? idPc { get; set; }
        public decimal? idRede { get; set; }
        public string macAddress { get; set; }
    
        public virtual pc pc { get; set; }
        public virtual rede rede { get; set; }
    }
}
