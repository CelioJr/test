namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_passmon
    {
        [Key]
        public long id_passmon { get; set; }

        public long id_passagem { get; set; }

        public long id_produto { get; set; }

        public short? is_alternativo { get; set; }

        [StringLength(40)]
        public string ds_mon { get; set; }

        [StringLength(256)]
        public string cb { get; set; }

        public int? ordem { get; set; }

        public virtual dw_passagem dw_passagem { get; set; }

        public virtual om_produto om_produto { get; set; }
    }
}
