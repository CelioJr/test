namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_joblog
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_joblog()
        {
            om_jobdetlog = new HashSet<om_jobdetlog>();
        }

        [Key]
        public long id_joblog { get; set; }

        public DateTime? dthr_iexecucao { get; set; }

        public short? st_execucao { get; set; }

        [StringLength(100)]
        public string ds_execucao { get; set; }

        public long id_job { get; set; }

        public DateTime? dthr_fexecucao { get; set; }

        public virtual om_job om_job { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_jobdetlog> om_jobdetlog { get; set; }
    }
}
