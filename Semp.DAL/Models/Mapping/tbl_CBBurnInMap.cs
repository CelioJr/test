using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBBurnInMap : EntityTypeConfiguration<tbl_CBBurnIn>
    {
        public tbl_CBBurnInMap()
        {
            // Primary Key
            this.HasKey(t => new { t.NumECB, t.DatEntrada, t.CodCarro });

            // Properties
            this.Property(t => t.NumECB)
                .IsRequired()
                .HasMaxLength(13);

            this.Property(t => t.CodDRT)
                .IsFixedLength()
                .HasMaxLength(8);

            this.Property(t => t.CodCarro)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.CodItem)
                .HasMaxLength(15);

            // Table & Column Mappings
            this.ToTable("tbl_CBBurnIn");
            this.Property(t => t.NumECB).HasColumnName("NumECB");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.NumPosto).HasColumnName("NumPosto");
            this.Property(t => t.DatEntrada).HasColumnName("DatEntrada");
            this.Property(t => t.DatDefeito).HasColumnName("DatDefeito");
            this.Property(t => t.DatSaida).HasColumnName("DatSaida");
            this.Property(t => t.CodDRT).HasColumnName("CodDRT");
            this.Property(t => t.CodCarro).HasColumnName("CodCarro");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.DatBurnIn).HasColumnName("DatBurnIn");
            this.Property(t => t.DatEncerrado).HasColumnName("DatEncerrado");
        }
    }
}
