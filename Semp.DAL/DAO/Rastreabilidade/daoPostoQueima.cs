﻿using SEMP.DAL.Models;
using SEMP.Model.DTO;
using SEMP.Model.VO.Rastreabilidade;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEMP.DAL.DAO.Rastreabilidade
{
	public class daoPostoQueima
	{
        //static daoPostoQueima()
        //{
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<tbl_CBQueimaInspecao, CBQueimaInspecaoDTO>();
        //        cfg.CreateMap<CBQueimaInspecaoDTO, tbl_CBQueimaInspecao>();
        //        cfg.CreateMap<tbl_CBQueimaTestes, CBQueimaTestesDTO>();
        //        cfg.CreateMap<CBQueimaTestesDTO, tbl_CBQueimaTestes>();
        //        cfg.CreateMap<CBQueimaRegistroDTO, tbl_CBQueimaRegistro>();
        //    });
        //}


        #region Consultas Testes (Defeitos)

        public static List<CBCadDefeitoDTO> ObterTestesQueima()
		{

			using (var contexto = new DTB_CBContext())
			{

				var series = contexto.tbl_CBCadDefeito.Where(x => x.CodDefeito.StartsWith("3")).ToList();

				return series.Select(x => new CBCadDefeitoDTO()
				{
					CodDefeito = x.CodDefeito,
					CodFab = x.CodFab,
					DesDefeito = x.DesDefeito,
					DesVerificacao = x.DesVerificacao,
					flgAtivo = x.flgAtivo,
					flgFEC = x.flgFEC,
					flgIAC = x.flgIAC,
					flgIMC = x.flgIMC,
					flgLCM = x.flgLCM
				}).ToList();


			}

		}

		public static List<CBCadSubDefeitoDTO> ObterTestesEspecificosQueima()
		{

			using (var contexto = new DTB_CBContext())
			{

				var series = contexto.tbl_CBCadSubDefeito.Where(x => x.CodDefeito.StartsWith("3")).ToList();

				return series.Select(x => new CBCadSubDefeitoDTO()
				{
					CodDefeito = x.CodDefeito,
					CodSubDefeito = x.CodSubDefeito,
					DesDefeito = x.DesDefeito,
					DesVerificacao = x.DesVerificacao,
					flgAtivo = x.flgAtivo
				}).ToList();


			}

		}

		#endregion

		#region Consultas Queima
		public static List<QueimaResumoInspecao> ObterResumoInspecao(DateTime datInicio, DateTime datFim)
		{

			using (var dtb_CB = new DTB_CBContext())
			using (var dtb_SIM = new DTB_SIMContext())
			{
				if (datInicio == datFim)
				{
					datFim = datFim.AddDays(1).AddMinutes(-1);
				}

				var programa = (from j in dtb_SIM.tbl_ProgDiaJit
								join i in dtb_SIM.tbl_Item on j.CodModelo equals i.CodItem
								from p in dtb_SIM.tbl_ProdDia.Where(x => x.CodFab == j.CodFab && x.CodModelo == j.CodModelo && x.DatProducao == j.DatProducao && x.CodLinha == j.CodLinha).DefaultIfEmpty()
								let Qtd042 = dtb_SIM.tbl_Estoque.FirstOrDefault(x => x.CodFab == j.CodFab && x.CodItem == j.CodModelo && x.CodLocal == "042").QtdEstoque
								let Qtd823 = dtb_SIM.tbl_Estoque.FirstOrDefault(x => x.CodFab == j.CodFab && x.CodItem == j.CodModelo && x.CodLocal == "823").QtdEstoque
								let Qtd067 = dtb_SIM.tbl_Estoque.FirstOrDefault(x => x.CodFab == j.CodFab && x.CodItem == j.CodModelo && x.CodLocal == "067").QtdEstoque
								let Qtd068 = dtb_SIM.tbl_Estoque.FirstOrDefault(x => x.CodFab == j.CodFab && x.CodItem == j.CodModelo && x.CodLocal == "068").QtdEstoque
								where j.DatProducao >= datInicio && j.DatProducao <= datFim
								group new { j, i, p} by new { j.CodModelo, j.DatProducao, i.DesItem, Qtd042, Qtd067, Qtd068, Qtd823 } into grupo
								select new QueimaResumoInspecao()
								{
									CodModelo = grupo.Key.CodModelo,
									Data = grupo.Key.DatProducao,
									DesModelo = grupo.Key.DesItem.Trim(),
									QtdPrograma = grupo.Sum(x => x.j.QtdProdPm ?? 0),
									QtdProduzido = grupo.Sum(z => z.p.QtdProduzida ?? 0),
									QtdP042 = grupo.Key.Qtd042??0,
									QtdP823 = grupo.Key.Qtd823 ?? 0,
									QtdP067 = grupo.Key.Qtd067 ?? 0,
									QtdP068 = grupo.Key.Qtd068 ?? 0

								})
								.ToList();

				var pallets = (from g in dtb_SIM.TBL_GpaRecItem
							   where g.DataReferencia >= datInicio && g.DataReferencia <= datFim
							   group g by new { g.CodItem, g.CodPallet } into grupo
							   select new
							   {
								   CodModelo = grupo.Key.CodItem,
								   CodPallet = grupo.Key.CodPallet,
								   QtdPallet = grupo.Count()
							   })
							   .ToList();

				var inspecao = (from i in dtb_CB.tbl_CBQueimaInspecao
								join r in dtb_CB.tbl_CBQueimaRegistro on new { i.CodFab, i.NumRPA, i.CodPallet } equals new { r.CodFab, r.NumRPA, r.CodPallet }
								where r.DatAbertura >= datInicio && r.DatAbertura <= datFim
								group new { i, r } by new { r.CodModelo, i.FlgStatus, i.NumECB } into grupo
								select new
								{
									CodModelo = grupo.Key.CodModelo,
									FlgStatus = grupo.Key.FlgStatus,
									NumECB = grupo.Key.NumECB,
									QtdInspec = grupo.Count()
								}).ToList();

				foreach (var item in programa)
				{
					var qtdPallet = pallets.Count(x => x.CodModelo == item.CodModelo);

					var qtdInspec = inspecao.Count(x => x.CodModelo == item.CodModelo);

					var consInspec = inspecao.Where(x => x.CodModelo == item.CodModelo)
												.GroupBy(x => x.CodModelo)
												.Select(x => new
												{
													CodModelo = x.Key,
													QtdAprov = x.Count(y => y.FlgStatus == "S"),
													QtdReprov = x.Count(z => z.FlgStatus == "X")
												})
												.FirstOrDefault();

					item.QtdLotes = qtdPallet;
					item.QtdInspecionados = qtdInspec;
					item.QtdAprovados = consInspec != null ? consInspec.QtdAprov : 0;
					item.QtdReprovados = consInspec != null ? consInspec.QtdReprov : 0;
				}

				return programa;

			}


		}

		public static CBLoteConfigDTO ObterConfigLote(int codLinha)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dado = contexto.tbl_CBLoteConfig.FirstOrDefault(x => x.idLinha == codLinha);

				if (dado == null)
				{
					return new CBLoteConfigDTO();
				}

				var retorno = new CBLoteConfigDTO()
				{
					idLinha = dado.idLinha,
					CodDRT = dado.CodDRT,
					Qtde = dado.Qtde,
					QtdeCC = dado.QtdeCC,
					PercAmostras = dado.PercAmostras,
					QtdAmostras = dado.QtdAmostras
				};

				return retorno;
			}

		}

		public static List<CBQueimaRegistroDTO> ObterRegistros(DateTime datInicio, DateTime datFim)
		{

			using (var contexto = new DTB_CBContext())
			{
				if (datInicio == datFim)
				{
					datFim = datFim.AddDays(1).AddMinutes(-1);
				}

				var dados = (from r in contexto.tbl_CBQueimaRegistro
							 join i in contexto.tbl_Item on r.CodModelo equals i.CodItem
							 join l in contexto.tbl_CBLinha on r.CodLinha equals l.CodLinha
							 join lOri in contexto.tbl_CBLinha on r.CodLinhaOri equals lOri.CodLinhaT1
							 join p in contexto.tbl_CBPosto on new { r.CodLinha, r.NumPosto } equals new { p.CodLinha, p.NumPosto }
							 join s in contexto.tbl_CBQueimaTipoStatus on r.FlgStatus equals s.CodStatus
							 where (r.DatAbertura >= datInicio && r.DatAbertura <= datFim) || r.DatFechamento == null
							 orderby r.DatFechamento
							 select new CBQueimaRegistroDTO()
							 {
								 CodLinha = r.CodLinha,
								 NumPosto = r.NumPosto,
								 CodFab = r.CodFab,
								 NumRPA = r.NumRPA,
								 CodPallet = r.CodPallet,
								 CodModelo = r.CodModelo,
								 QtdPallet = r.QtdPallet,
								 QtdAmostras = r.QtdAmostras,
								 DatProducao = r.DatProducao,
								 DatAbertura = r.DatAbertura,
								 DatFechamento = r.DatFechamento,
								 CodOperador = r.CodOperador,
								 FlgStatus = r.FlgStatus,
								 CodLinhaOri = r.CodLinhaOri,
								 NumRPE = r.NumRPE,
								 DatLiberado = r.DatLiberado,
								 ObsRevisao = r.ObsRevisao,

								 DesLinha = l.DesLinha,
								 DesPosto = p.DesPosto,
								 DesModelo = i.DesItem.Trim(),
								 DesLinhaOri = lOri.DesLinha,

								 DesCorStatus = s.DesCor,
								 DesStatus = s.DesStatus
							 }).ToList();

				return dados;

			}
		}

		public static List<CBQueimaRegistroDTO> ObterRegistrosStatus(DateTime datInicio, string codStatus)
		{

			using (var contexto = new DTB_CBContext())
			{
				if (datInicio.Day != 1)
				{
					datInicio = datInicio.AddDays(-datInicio.Day).AddDays(1);
				}

				var dados = (from r in contexto.tbl_CBQueimaRegistro
							 join i in contexto.tbl_Item on r.CodModelo equals i.CodItem
							 join l in contexto.tbl_CBLinha on r.CodLinha equals l.CodLinha
							 join lOri in contexto.tbl_CBLinha on r.CodLinhaOri equals lOri.CodLinhaT1
							 join p in contexto.tbl_CBPosto on new { r.CodLinha, r.NumPosto } equals new { p.CodLinha, p.NumPosto }
							 join s in contexto.tbl_CBQueimaTipoStatus on r.FlgStatus equals s.CodStatus
							 where r.FlgStatus == codStatus && r.DatAbertura >= datInicio
							 orderby r.DatFechamento
							 select new CBQueimaRegistroDTO()
							 {
								 CodLinha = r.CodLinha,
								 NumPosto = r.NumPosto,
								 CodFab = r.CodFab,
								 NumRPA = r.NumRPA,
								 CodPallet = r.CodPallet,
								 CodModelo = r.CodModelo,
								 QtdPallet = r.QtdPallet,
								 QtdAmostras = r.QtdAmostras,
								 DatProducao = r.DatProducao,
								 DatAbertura = r.DatAbertura,
								 DatFechamento = r.DatFechamento,
								 CodOperador = r.CodOperador,
								 FlgStatus = r.FlgStatus,
								 CodLinhaOri = r.CodLinhaOri,
								 NumRPE = r.NumRPE,
								 DatLiberado = r.DatLiberado,
								 ObsRevisao = r.ObsRevisao,

								 DesLinha = l.DesLinha,
								 DesPosto = p.DesPosto,
								 DesModelo = i.DesItem.Trim(),
								 DesLinhaOri = lOri.DesLinha,

								 DesCorStatus = s.DesCor,
								 DesStatus = s.DesStatus
							 }).ToList();

				return dados;

			}
		}

		public static CBQueimaRegistroDTO ObterRegistro(string codFab, string numRPA, string codPallet)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = (from r in contexto.tbl_CBQueimaRegistro
							 join i in contexto.tbl_Item on r.CodModelo equals i.CodItem
							 join l in contexto.tbl_CBLinha on r.CodLinha equals l.CodLinha
							 join lOri in contexto.tbl_CBLinha on r.CodLinhaOri equals lOri.CodLinhaT1
							 join p in contexto.tbl_CBPosto on new { r.CodLinha, r.NumPosto } equals new { p.CodLinha, p.NumPosto }
							 where r.CodFab == codFab && r.NumRPA == numRPA && r.CodPallet == codPallet
							 select new CBQueimaRegistroDTO()
							 {
								 CodLinha = r.CodLinha,
								 NumPosto = r.NumPosto,
								 CodFab = r.CodFab,
								 NumRPA = r.NumRPA,
								 CodPallet = r.CodPallet,
								 CodModelo = r.CodModelo,
								 QtdPallet = r.QtdPallet,
								 QtdAmostras = r.QtdAmostras,
								 DatProducao = r.DatProducao,
								 DatAbertura = r.DatAbertura,
								 DatFechamento = r.DatFechamento,
								 CodOperador = r.CodOperador,
								 FlgStatus = r.FlgStatus,
								 CodLinhaOri = r.CodLinhaOri,
								 NumRPE = r.NumRPE,

								 DesLinha = l.DesLinha,
								 DesPosto = p.DesPosto,
								 DesModelo = i.DesItem.Trim(),
								 DesLinhaOri = lOri.DesLinha
							 });

				return dados.FirstOrDefault();

			}
		}

		public static CBQueimaRegistroDTO ObterRegistro(string codFab, string numRPA)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = (from r in contexto.tbl_CBQueimaRegistro
							 join i in contexto.tbl_Item on r.CodModelo equals i.CodItem
							 join l in contexto.tbl_CBLinha on r.CodLinha equals l.CodLinha
							 join lOri in contexto.tbl_CBLinha on r.CodLinhaOri equals lOri.CodLinhaT1
							 join p in contexto.tbl_CBPosto on new { r.CodLinha, r.NumPosto } equals new { p.CodLinha, p.NumPosto }
							 where r.CodFab == codFab && r.NumRPA == numRPA
							 select new CBQueimaRegistroDTO()
							 {
								 CodLinha = r.CodLinha,
								 NumPosto = r.NumPosto,
								 CodFab = r.CodFab,
								 NumRPA = r.NumRPA,
								 CodPallet = r.CodPallet,
								 CodModelo = r.CodModelo,
								 QtdPallet = r.QtdPallet,
								 QtdAmostras = r.QtdAmostras,
								 DatProducao = r.DatProducao,
								 DatAbertura = r.DatAbertura,
								 DatFechamento = r.DatFechamento,
								 CodOperador = r.CodOperador,
								 FlgStatus = r.FlgStatus,
								 CodLinhaOri = r.CodLinhaOri,
								 NumRPE = r.NumRPE,
								 ObsRevisao = r.ObsRevisao,
								 DatLiberado = r.DatLiberado,

								 DesLinha = l.DesLinha,
								 DesPosto = p.DesPosto,
								 DesModelo = i.DesItem.Trim(),
								 DesLinhaOri = lOri.DesLinha
							 }).FirstOrDefault();

				return dados;

			}
		}

		public static List<QueimaRelatorioInspecao> ObterInspecoes(DateTime datInicio, DateTime datFim)
		{

			using (var contexto = new DTB_CBContext())
			{
				if (datInicio == datFim)
				{
					datFim = datFim.AddDays(1).AddMinutes(-1);
				}

				var dados = (from i in contexto.tbl_CBQueimaInspecao
							 join r in contexto.tbl_CBQueimaRegistro on new { i.CodFab, i.NumRPA, i.CodPallet } equals new { r.CodFab, r.NumRPA, r.CodPallet }
							 join l in contexto.tbl_CBLinha on i.CodLinha equals l.CodLinha
							 join p in contexto.tbl_CBPosto on new { i.CodLinha, i.NumPosto } equals new { p.CodLinha, p.NumPosto }
							 join lOri in contexto.tbl_CBLinha on r.CodLinhaOri equals lOri.CodLinhaT1
							 join item in contexto.tbl_Item on r.CodModelo equals item.CodItem
							 from f in contexto.tbl_RIFuncionario.Where(x => r.CodOperador == x.NomLogin).DefaultIfEmpty()
							 where r.DatAbertura >= datInicio && r.DatAbertura <= datFim
							 orderby new { r.CodModelo, r.NumRPA }
							 select new QueimaRelatorioInspecao()
							 {
								 NumECB = i.NumECB,
								 CodLinha = i.CodLinha,
								 NumPosto = i.NumPosto,
								 CodFab = i.CodFab,
								 NumRPA = i.NumRPA,
								 CodPallet = i.CodPallet,
								 DatLeitura = i.DatLeitura,
                                 DatEntrada = r.DatAbertura,
                                 DatSaida = r.DatFechamento,
								 FlgStatus = i.FlgStatus,
								 Observacao = i.Observacao,
								 DesLinha = l.DesLinha.Trim(),
								 DesPosto = p.DesPosto.Trim(),
								 CodLinhaOri = r.CodLinhaOri,
								 CodModelo = r.CodModelo,
								 CodOperador = r.CodOperador,
								 DatProducao = r.DatProducao.Value,
								 DesLinhaOri = lOri.DesLinha.Trim(),
								 DesModelo = item.DesItem.Trim(),
								 NomOperador = f.NomFuncionario.Trim(),
								 NumRPE = r.NumRPE
							 }).ToList();

				return dados;

			}
		}

		public static List<CBQueimaInspecaoDTO> ObterInspecoes(string codFab, string numRPA, string codPallet)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = contexto.tbl_CBQueimaInspecao.Where(x => x.CodFab == codFab && x.NumRPA == numRPA && x.CodPallet == codPallet).ToList();

				var dadosDTO = new List<CBQueimaInspecaoDTO>();

				AutoMapper.Mapper.Map(dados, dadosDTO);

				return dadosDTO;

			}
		}

		public static List<CBQueimaInspecaoDTO> ObterInspecoes(string codFab, string numRPA)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = contexto.tbl_CBQueimaInspecao.Where(x => x.CodFab == codFab && x.NumRPA == numRPA).ToList();

				var dadosDTO = new List<CBQueimaInspecaoDTO>();

				AutoMapper.Mapper.Map(dados, dadosDTO);

				return dadosDTO;

			}
		}

		public static CBQueimaInspecaoDTO ObterInspecaoSerie(string numECB, string codFab, string numRPA)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = contexto.tbl_CBQueimaInspecao.FirstOrDefault(x => x.CodFab == codFab && x.NumRPA == numRPA && x.NumECB == numECB);

				var dadosDTO = new CBQueimaInspecaoDTO();

				AutoMapper.Mapper.Map(dados, dadosDTO);

				return dadosDTO;

			}
		}

		public static List<CBQueimaTestesDTO> ObterTestes(string codFab, string numRPA, string numECB)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = (from t in contexto.tbl_CBQueimaTestes
							 join def in contexto.tbl_CBCadDefeito on t.CodDefeito equals def.CodDefeito
							 join sub in contexto.tbl_CBCadSubDefeito on new { t.CodDefeito, t.CodSubDefeito } equals new { sub.CodDefeito, sub.CodSubDefeito }
							 where t.CodFab == codFab && t.NumRPA == numRPA && t.NumECB == numECB
							 select new CBQueimaTestesDTO()
							 {
								 NumECB = t.NumECB,
								 CodLinha = t.CodLinha,
								 NumPosto = t.NumPosto,
								 CodFab = t.CodFab,
								 NumRPA = t.NumRPA,
								 CodDefeito = t.CodDefeito,
								 CodSubDefeito = t.CodSubDefeito,
								 FlgStatus = t.FlgStatus,
								 Observacao = t.Observacao,
								 DatTeste = t.DatTeste,

								 DesDefeito = def.DesDefeito,
								 DesSubDefeito = sub.DesDefeito
							 }).ToList();

				return dados;

			}
		}

		public static List<CBQueimaTestesDTO> ObterTestes(string codFab, string numRPA)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = (from t in contexto.tbl_CBQueimaTestes
							 join def in contexto.tbl_CBCadDefeito on t.CodDefeito equals def.CodDefeito
							 join sub in contexto.tbl_CBCadSubDefeito on new { t.CodDefeito, t.CodSubDefeito } equals new { sub.CodDefeito, sub.CodSubDefeito }
							 where t.CodFab == codFab && t.NumRPA == numRPA
							 select new CBQueimaTestesDTO()
							 {
								 NumECB = t.NumECB,
								 CodLinha = t.CodLinha,
								 NumPosto = t.NumPosto,
								 CodFab = t.CodFab,
								 NumRPA = t.NumRPA,
								 CodDefeito = t.CodDefeito,
								 CodSubDefeito = t.CodSubDefeito,
								 FlgStatus = t.FlgStatus,
								 Observacao = t.Observacao,
								 DatTeste = t.DatTeste,

								 DesDefeito = def.DesDefeito,
								 DesSubDefeito = sub.DesDefeito
							 }).ToList();

				return dados;

			}
		}

		public static CBQueimaTestesDTO ObterTesteSerie(string numECB, string codFab, string numRPA, string codDefeito, int codSubDefeito)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = contexto.tbl_CBQueimaTestes.FirstOrDefault(x => x.CodFab == codFab && x.NumRPA == numRPA && x.NumECB == numECB && x.CodDefeito == codDefeito && x.CodSubDefeito == codSubDefeito);

				var dadosDTO = new CBQueimaTestesDTO();

				AutoMapper.Mapper.Map(dados, dadosDTO);

				return dadosDTO;

			}
		}

		public static string ObterECBSerie(string codModelo, string numSerie)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dado = contexto.tbl_CBIdentifica.FirstOrDefault(x => x.NumECB.StartsWith(codModelo) && x.NumECB.EndsWith(numSerie));


				if (dado != null)
				{
					return dado.NumECB;
				}
				else
				{
					return "";
				}

			}

		}

		public static bool VerificaPalletTotal(string codFab, string numRPA)
		{

			using (var contexto = new DTB_CBContext())
			{

				var qtdAmostra = 0;

				var registro = contexto.tbl_CBQueimaRegistro.FirstOrDefault(x => x.CodFab == codFab && x.NumRPA == numRPA);
				if (registro != null)
				{
					qtdAmostra = registro.QtdAmostras ?? 0;
				}

				var inspecao = contexto.tbl_CBQueimaInspecao.Count(x => x.CodFab == codFab && x.NumRPA == numRPA);

				return (inspecao == qtdAmostra);

			}

		}

		public static int ObterQtdAprovPallet(string codFab, string numRPA, string flgAprov)
		{

			using (var contexto = new DTB_CBContext())
			{
				return contexto.tbl_CBQueimaInspecao.Count(x => x.CodFab == codFab && x.NumRPA == numRPA && x.FlgStatus == flgAprov);
			}

		}

		#endregion

		#region Manutenção Queima

		public static string GravarRegistro(CBQueimaRegistroDTO dados, bool flgExcluir, bool flgMovimenta)
		{
			try
			{

				using (var contexto = new DTB_CBContext())
				{

					var existe = contexto.tbl_CBQueimaRegistro.FirstOrDefault(x => x.CodFab == dados.CodFab && x.NumRPA == dados.NumRPA);

					if (existe != null)
					{
						if (flgExcluir)
						{
							contexto.tbl_CBQueimaRegistro.Remove(existe);
						}
						else
						{
							existe.QtdAmostras = dados.QtdAmostras;
							existe.FlgStatus = dados.FlgStatus;
							existe.DatAbertura = dados.DatAbertura;
							existe.DatFechamento = dados.DatFechamento;
							existe.CodOperador = dados.CodOperador;
							existe.CodLinhaOri = dados.CodLinhaOri;
							existe.DatProducao = dados.DatProducao;
							existe.NumRPE = dados.NumRPE;
							existe.DatLiberado = dados.DatLiberado;
							existe.ObsRevisao = dados.ObsRevisao;

						}
					}
					else
					{
						var dadosFinal = new tbl_CBQueimaRegistro();
;
						AutoMapper.Mapper.Map(dados, dadosFinal);

						contexto.tbl_CBQueimaRegistro.Add(dadosFinal);

						if (flgMovimenta)
						{
							var resultado = "true";//daoPostoOBA.EfetuaMovimentacao(dados.CodFab, dados.NumRPA, dados.CodModelo, dados.CodPallet, dados.CodOperador);

							if (resultado.StartsWith("false"))
							{
								return resultado;
							}
						}

					}


					contexto.SaveChanges();

					return "true";


				}

			}
			catch
			{
				return "false";
			}

		}

		public static bool GravarInspecao(CBQueimaInspecaoDTO dados, bool flgExcluir)
		{

			using (var contexto = new DTB_CBContext())
			{

				var existe = contexto.tbl_CBQueimaInspecao.FirstOrDefault(x => x.CodFab == dados.CodFab && x.NumRPA == dados.NumRPA && x.NumECB == dados.NumECB);

				if (existe != null)
				{
					if (flgExcluir)
					{
						contexto.tbl_CBQueimaInspecao.Remove(existe);
					}
					else
					{
						existe.FlgStatus = dados.FlgStatus;
						existe.Observacao = dados.Observacao;
						existe.DatLeitura = dados.DatLeitura;
					}
				}
				else
				{
					var dadosFinal = new tbl_CBQueimaInspecao();

					AutoMapper.Mapper.Map(dados, dadosFinal);

					contexto.tbl_CBQueimaInspecao.Add(dadosFinal);

				}


				contexto.SaveChanges();

				return true;


			}

		}

		public static bool GravarTeste(CBQueimaTestesDTO dados, bool flgExcluir)
		{

			using (var contexto = new DTB_CBContext())
			{

				var existe = contexto.tbl_CBQueimaTestes.FirstOrDefault(x => x.CodFab == dados.CodFab && x.NumRPA == dados.NumRPA && x.NumECB == dados.NumECB && x.CodDefeito == dados.CodDefeito && x.CodSubDefeito == dados.CodSubDefeito && x.DatTeste == dados.DatTeste);

				if (existe != null)
				{
					if (flgExcluir)
					{
						contexto.tbl_CBQueimaTestes.Remove(existe);
					}
					else
					{
						existe.FlgStatus = dados.FlgStatus;
						existe.Observacao = dados.Observacao;
					}
				}
				else
				{
					var dadosFinal = new tbl_CBQueimaTestes();

					AutoMapper.Mapper.Map(dados, dadosFinal);

					contexto.tbl_CBQueimaTestes.Add(dadosFinal);

				}

				contexto.SaveChanges();

				return true;


			}

		}

		#endregion

		#region Consultas Status

		public static List<CBQueimaTipoStatusDTO> ObterListaStatus() {

			using (var contexto = new DTB_CBContext())
			{
				var dados = contexto.tbl_CBQueimaTipoStatus
									.Select(x => new CBQueimaTipoStatusDTO() { 
										CodStatus = x.CodStatus,
										DesCor = x.DesCor,
										DesStatus = x.DesStatus
									})
									.ToList();

				return dados;

			}

		}

		public static CBQueimaTipoStatusDTO ObterTipoStatus(string codStatus)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = contexto.tbl_CBQueimaTipoStatus
									.Where(c => c.CodStatus == codStatus)
									.Select(x => new CBQueimaTipoStatusDTO()
									{
										CodStatus = x.CodStatus,
										DesCor = x.DesCor,
										DesStatus = x.DesStatus
									})
									.FirstOrDefault();

				return dados;

			}

		}

		#endregion

		#region Consultas GPA 
		public static ResumoGPA ObterPalletGPA(string numPallet, string codModelo, string numSerie)
		{

			using (var contexto = new DTB_SIMContext())
			{

				if (!String.IsNullOrEmpty(numPallet))
				{

					var dados = (from m in contexto.tbl_GpaMovimentacao
								 join g in contexto.tbl_BarrasProducao on new { m.CodItem, NumSerie = m.DocInterno2 } equals new { CodItem = g.CodModelo, NumSerie = g.NumSerieModelo }
								 join l in contexto.tbl_Linha on g.CodLinha equals l.CodLinha
								 join i in contexto.tbl_Item on g.CodModelo equals i.CodItem
								 where m.DocInterno1 == numPallet
								 group new { g, l, i } by new { m.DocInterno1, g.CodLinha, g.CodModelo, l.DesLinha, i.DesItem } into grupo
								 select new ResumoGPA()
								 {
									 CodLinha = grupo.Key.CodLinha,
									 CodModelo = grupo.Key.CodModelo,
									 CodPallet = grupo.Key.DocInterno1,
									 Data = grupo.Min(d => d.g.DatReferencia),
									 DesLinha = grupo.Key.DesLinha.Trim(),
									 DesModelo = grupo.Key.DesItem.Trim(),
									 QtdPallet = grupo.Count(),
									 NumSerieIni = grupo.Min(x => x.g.NumSerieModelo),
									 NumSerieFim = grupo.Max(x => x.g.NumSerieModelo)
								 });

					return dados.FirstOrDefault();
				}
				else
				{
					if (!String.IsNullOrEmpty(numSerie))
					{
						numSerie = numSerie.Substring(8);

						var query2 = (from m in contexto.tbl_GpaMovimentacao
									  join g in contexto.tbl_BarrasProducao on new { m.CodItem, NumSerie = m.DocInterno2 } equals new { CodItem = g.CodModelo, NumSerie = g.NumSerieModelo }
									  join l in contexto.tbl_Linha on g.CodLinha equals l.CodLinha
									  join i in contexto.tbl_Item on g.CodModelo equals i.CodItem
									  let NumPallet = contexto.tbl_GpaMovimentacao.FirstOrDefault(x => x.CodItem == codModelo && x.DocInterno2 == numSerie).DocInterno1
									  where m.DocInterno1 == NumPallet
									  group new { g, l, i } by new { m.DocInterno1, g.CodLinha, g.CodModelo, l.DesLinha, i.DesItem } into grupo
									  select new ResumoGPA()
									  {
										  CodLinha = grupo.Key.CodLinha,
										  CodModelo = grupo.Key.CodModelo,
										  CodPallet = grupo.Key.DocInterno1,
										  Data = grupo.Min(d => d.g.DatReferencia),
										  DesLinha = grupo.Key.DesLinha.Trim(),
										  DesModelo = grupo.Key.DesItem.Trim(),
										  QtdPallet = grupo.Count(),
										  NumSerieIni = grupo.Min(x => x.g.NumSerieModelo),
										  NumSerieFim = grupo.Max(x => x.g.NumSerieModelo)
									  });

						return query2.FirstOrDefault();
					}
					else
					{
						return null;
					}
				}

			}

		}

		public static ResumoGPA ObterPalletSerie(string codModelo, string numSerie)
		{

			using (var contexto = new DTB_SIMContext())
			{

				if (!String.IsNullOrEmpty(numSerie))
				{
					var query2 = (from m in contexto.TBL_GpaRecItem
									join g in contexto.tbl_BarrasProducao on new { m.CodItem, m.NumSerie} equals new { CodItem = g.CodModelo, NumSerie = g.NumSerieModelo }
									join l in contexto.tbl_Linha on g.CodLinha equals l.CodLinha
									join i in contexto.tbl_Item on g.CodModelo equals i.CodItem
									where m.NumSerie == numSerie && m.CodItem == codModelo
									group new { g, l, i } by new { m.CodPallet, g.CodLinha, g.CodModelo, l.DesLinha, i.DesItem } into grupo
									select new ResumoGPA()
									{
										CodLinha = grupo.Key.CodLinha,
										CodModelo = grupo.Key.CodModelo,
										CodPallet = grupo.Key.CodPallet.ToString(),
										Data = grupo.Min(d => d.g.DatReferencia),
										DesLinha = grupo.Key.DesLinha.Trim(),
										DesModelo = grupo.Key.DesItem.Trim(),
										QtdPallet = grupo.Count(),
										NumSerieIni = grupo.Min(x => x.g.NumSerieModelo),
										NumSerieFim = grupo.Max(x => x.g.NumSerieModelo)
									});

					return query2.FirstOrDefault();
				}
				else
				{
					return null;
				}
			
			}

		}

		public static List<string> ObterSeriesPallet(string numPallet)
		{

			using (var contexto = new DTB_SIMContext())
			{
				numPallet = numPallet.PadLeft(6, '0');
				var series = contexto.tbl_GpaMovimentacao.Where(x => x.DocInterno1 == numPallet).Select(x => x.DocInterno2.Trim()).ToList();

				return series;


			}

		}

		public static List<ResumoGPA> ObterPalletsGPA(DateTime datInicio, DateTime datFim)
		{

			using (var contexto = new DTB_SIMContext())
			{

				var dados = (from g in contexto.TBL_GpaRecItem
							 join l in contexto.tbl_Linha on g.CodLinha equals l.CodLinha
							 join i in contexto.tbl_Item on g.CodItem equals i.CodItem
							 where g.DataReferencia >= datInicio && g.DataReferencia <= datFim
							 group new { g, l, i } by new { g.CodPallet, g.CodLinha, g.CodItem, g.DataReferencia, l.DesLinha, i.DesItem } into grupo
							 select new ResumoGPA()
							 {
								 CodLinha = grupo.Key.CodLinha,
								 CodModelo = grupo.Key.CodItem,
								 CodPallet = grupo.Key.CodPallet.ToString(),
								 Data = grupo.Key.DataReferencia,
								 DesLinha = grupo.Key.DesLinha.Trim(),
								 DesModelo = grupo.Key.DesItem.Trim(),
								 QtdPallet = grupo.Count(),
								 NumSerieIni = grupo.Min(x => x.g.NumSerie),
								 NumSerieFim = grupo.Max(x => x.g.NumSerie),
							 }).ToList();

				return dados;

			}

		}
		#endregion

	}
}
