namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ms_pt_coleta_login
    {
        [Key]
        public long id_coletalogin { get; set; }

        public long id_pt { get; set; }

        public DateTime? dthr_ilogin { get; set; }

        public long? id_usr { get; set; }

        public virtual ms_pt_coleta ms_pt_coleta { get; set; }

        public virtual om_usr om_usr { get; set; }
    }
}
