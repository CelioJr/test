using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
	/// <summary>
	/// Classe respons�vel por recuperar os dados que ser�o exibidos na tela do Tipo de amarra��o.
	/// </summary>
	public class CBTipoAmarraDTO
	{
		[Display(Name = "C�digo")]
		public int CodTipoAmarra { get; set; }
		[Display(Name = "Descri��o")]
		public string DesTipoAmarra { get; set; }
		[Display(Name = "Fam�lia")]
		public string CodFam { get; set; }
		[Display(Name = "Usa m�scara padr�o?")]
		public string FlgMascaraPadrao { get; set; }
		[Display(Name = "M�scara")]
		public string MascaraPadrao { get; set; }
		[Display(Name = "Placa/Painel")]
		public int? FlgPlacaPainel { get; set; }		[Display(Name = "C�digo de Processo")]
		public string CodProcesso { get; set; }	}
}
