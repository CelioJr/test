using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_RPEMap : EntityTypeConfiguration<tbl_RPE>
    {
        public tbl_RPEMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.NumRPE });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NumRPE)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumRPA)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.FlgProdConf)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.Usuario)
                .HasMaxLength(25)
                .IsUnicode(false);

            this.Property(t => t.NomRespSetor)
                .HasMaxLength(25)
                .IsUnicode(false);

            this.Property(t => t.NomRespGQ)
                .HasMaxLength(25)
                .IsUnicode(false);

            this.Property(t => t.ObsRPE)
                .HasMaxLength(512)
                .IsUnicode(false);

            this.Property(t => t.FlgClasse)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_RPE");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.NumRPE).HasColumnName("NumRPE");
            this.Property(t => t.NumRPA).HasColumnName("NumRPA");
            this.Property(t => t.DatAprovRPE).HasColumnName("DatAprovRPE");
            this.Property(t => t.QtdAprovRPE).HasColumnName("QtdAprovRPE");
            this.Property(t => t.FlgProdConf).HasColumnName("FlgProdConf");
            this.Property(t => t.Usuario).HasColumnName("Usuario");
            this.Property(t => t.NomRespSetor).HasColumnName("NomRespSetor");
            this.Property(t => t.DatRespSetor).HasColumnName("DatRespSetor");
            this.Property(t => t.NomRespGQ).HasColumnName("NomRespGQ");
            this.Property(t => t.DatRespGQ).HasColumnName("DatRespGQ");
            this.Property(t => t.ObsRPE).HasColumnName("ObsRPE");
            this.Property(t => t.FlgClasse).HasColumnName("FlgClasse");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
        }
    }
}
