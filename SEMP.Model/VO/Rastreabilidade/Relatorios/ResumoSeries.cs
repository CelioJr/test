﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
	public class ResumoSeries
	{
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public string Projeto { get; set; }
		public string MenorSerie { get; set; }
		public string MaiorSerie { get; set; }
		public int DifSerie { get { return int.Parse(MaiorSerie) - int.Parse(MenorSerie); } }
		public int TotalRegistros { get; set; }
		public int Diferenca { get { return DifSerie - TotalRegistros; }  }
		public int Incompletos { get; set; }
	}
}
