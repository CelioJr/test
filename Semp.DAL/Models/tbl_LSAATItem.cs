using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_LSAATItem
    {
        public string CodFab { get; set; }
        public string NumAT { get; set; }
        public string CodItem { get; set; }
        public string CodItemAlt { get; set; }
        public string CodIdentifica { get; set; }
        public string CodModelo { get; set; }
        public string CodAplAlt { get; set; }
        public decimal QtdFrequencia { get; set; }
        public decimal QtdNecessaria { get; set; }
        public Nullable<decimal> QtdNota { get; set; }
        public Nullable<decimal> QtdPaga { get; set; }
        public Nullable<decimal> QtdEstNormal { get; set; }
        public Nullable<decimal> QtdEstHannan { get; set; }
        public Nullable<decimal> QtdPadrao { get; set; }
        public Nullable<decimal> QtdSaldoExterno { get; set; }
        public Nullable<decimal> QtdRetorno { get; set; }
        public string CodNumTec { get; set; }
        public Nullable<decimal> PrecoUnitario { get; set; }
        public string Observacao { get; set; }
        public Nullable<decimal> QtdLibMaior { get; set; }
    }
}
