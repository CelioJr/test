﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
	public class IndiceDefeito
	{

		public IndiceDefeito()
		{
			Dia = default(DateTime);
			QtdProduzida = 0;
			QtdDefeitos = 0;
			IndiceGeral = 0;
			QtdFalsaFalha = 0;
			IndiceFalsaFalha = 0;
			IndiceSemFalsaFalha = 0;										
		}

		public DateTime Dia {get; set;}
		public int QtdProduzida { get; set; }
		public int QtdDefeitos { get; set; }
		public double IndiceGeral { get; set; }
		public int QtdFalsaFalha { get; set; }		
		public double IndiceFalsaFalha { get; set; }
		public double IndiceSemFalsaFalha { get; set; }			
		
	}
}
