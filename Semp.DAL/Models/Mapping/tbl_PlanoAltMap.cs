using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_PlanoAltMap : EntityTypeConfiguration<tbl_PlanoAlt>
    {
        public tbl_PlanoAltMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodModelo, t.CodItem, t.NumPosicao, t.CodItemAlt, t.DatInicio, t.rowguid });

            // Properties
            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumPosicao)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.CodItemAlt)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.FlgAtivo)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.NumNAEnt)
                .IsFixedLength()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.NumNASai)
                .IsFixedLength()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.CodBreakdown)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NomUsuario)
                .IsFixedLength()
                .HasMaxLength(20)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_PlanoAlt");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.NumPosicao).HasColumnName("NumPosicao");
            this.Property(t => t.CodItemAlt).HasColumnName("CodItemAlt");
            this.Property(t => t.DatInicio).HasColumnName("DatInicio");
            this.Property(t => t.FlgAtivo).HasColumnName("FlgAtivo");
            this.Property(t => t.NumNAEnt).HasColumnName("NumNAEnt");
            this.Property(t => t.NumNASai).HasColumnName("NumNASai");
            this.Property(t => t.DatFim).HasColumnName("DatFim");
            this.Property(t => t.CodBreakdown).HasColumnName("CodBreakdown");
            this.Property(t => t.NomUsuario).HasColumnName("NomUsuario");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
        }
    }
}
