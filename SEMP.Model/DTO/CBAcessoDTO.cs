﻿using System.ComponentModel.DataAnnotations;


namespace SEMP.Model.DTO
{
	public class CBAcessoDTO
	{

		[Key]
		public string CodAcesso { get; set; }
		public string AcessoDepto { get; set; }
		public string AcessoUser { get; set; }

	}
}
