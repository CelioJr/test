using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_CAMPO_DTO
    {
        public string TAB_NOME { get; set; }
        public string CMP_NOME { get; set; }
        public string CMP_DESCRICAO { get; set; }
        public string CMP_VALORPADRAO { get; set; }
        public Nullable<int> TPD_CODIGO { get; set; }
        public virtual FR_TABELA_DTO FR_TABELA { get; set; }
    }
}
