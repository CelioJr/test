namespace SEMP.DAL.Integrator
{
	using System.ComponentModel.DataAnnotations;

	public partial class ZSPP_APONTAMENTO_SISAP
    {
        [Key]
        [StringLength(16)]
        public string PACKNO { get; set; }
    }
}
