using System;
using System.Collections.Generic;

namespace SEMP.DTB_CB.Models
{
    public partial class tbl_CBProdDiaDAT
    {
        public string CodFab { get; set; }
        public string CodLinha { get; set; }
        public string CodModelo { get; set; }
        public System.DateTime DatProducao { get; set; }
        public string CodCin { get; set; }
        public Nullable<int> QtdProgramada { get; set; }
        public Nullable<int> QtdProduzida { get; set; }
        public Nullable<int> QtdFaturado { get; set; }
        public string FlagTP { get; set; }
    }
}
