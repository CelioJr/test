using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class hdtconfig_DTO
    {
        public long id { get; set; }
        public string hdtProcessName { get; set; }
        public string urlPortal { get; set; }
    }
}
