using System;

namespace SEMP.Model.DTO
{
    public class CBHorarioLinhaDTO
    {
        public int idHorario { get; set; }
        public int Turno { get; set; }
        public int CodLinha { get; set; }
        public string Fabrica { get; set; }
        public System.DateTime DatInicio { get; set; }
        public Nullable<System.DateTime> DatFim { get; set; }
        public string CodLinhaCliente { get; set; }
    }
}
