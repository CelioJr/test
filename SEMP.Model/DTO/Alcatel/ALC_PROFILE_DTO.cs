using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_PROFILE_DTO
    {
        public ALC_PROFILE_DTO()
        {
            this.ALC_ROLE_PROF = new List<ALC_ROLE_PROF_DTO>();
        }

        public decimal id { get; set; }
        public Nullable<System.DateTime> createdAt { get; set; }
        public Nullable<System.DateTime> updatedAt { get; set; }
        public string avatar { get; set; }
        public string email { get; set; }
        public bool enabled { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string password { get; set; }
        public Nullable<System.DateTime> passwordResetExpiry { get; set; }
        public string passwordResetToken { get; set; }
        public string profileType { get; set; }
        public string username { get; set; }
        public string uuid { get; set; }
        public virtual ICollection<ALC_ROLE_PROF_DTO> ALC_ROLE_PROF { get; set; }
    }
}
