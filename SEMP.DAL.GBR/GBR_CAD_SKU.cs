namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_CAD_SKU
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public GBR_CAD_SKU()
        {
            GBR_CADASTRO_DE_PATH_FT = new HashSet<GBR_CADASTRO_DE_PATH_FT>();
        }

        [Key]
        public int COD_SKU { get; set; }

        [StringLength(20)]
        public string SKU { get; set; }

        public int? COD_MODELO { get; set; }

        [StringLength(50)]
        public string CLIENTE { get; set; }

        [StringLength(20)]
        public string COR { get; set; }

        [StringLength(30)]
        public string SAPCODE { get; set; }

        [StringLength(13)]
        public string EAN { get; set; }

        [Required]
        [StringLength(3)]
        public string PACK_STRING { get; set; }

        [Required]
        [StringLength(3)]
        public string PALLET_STRING { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PESO_MIN { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PESO_MAX { get; set; }

        public bool? PESO_ATIVO { get; set; }

        public int? TIME_OUT { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PESO_MAX_COLETIVO { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PESO_MIN_COLETIVO { get; set; }

        public bool? PESO_COLETIVO_ATIVO { get; set; }

        public bool? CONFIRMAR_CAMPOS_ADICIONAIS { get; set; }

        public bool? CONFIRMAR_2D_CX_COL { get; set; }

        [StringLength(32)]
        public string PRODUCTCODES { get; set; }

        [StringLength(50)]
        public string IMPORTATION_LABEL { get; set; }

        public int? QTD_PRODUTO_COLETIVO { get; set; }

        public int? QTD_PRODUTO_PALLET { get; set; }

        public bool? ATIVO { get; set; }

        public DateTime? DATA { get; set; }

        public bool? BALANCA_RAPIDA { get; set; }

        [StringLength(100)]
        public string PATH_SAP_FILE_COLETIVA { get; set; }

        [StringLength(100)]
        public string PATH_SAP_FILE_PALLET { get; set; }

        public int? QTD_COLMEIA { get; set; }

        [StringLength(100)]
        public string PATH_COLMEIA { get; set; }

        public int? TOTAL_COLMEIA { get; set; }

        [StringLength(20)]
        public string PCBA { get; set; }

        [StringLength(20)]
        public string BOM { get; set; }

        public int? QTD_CHECK_CQ { get; set; }

        public bool? CHK_CQ_PALLET { get; set; }

        public bool? CONFIRMAR_IMEI_PALLET { get; set; }

        public bool? CHK_SERIAL_CX_INDIVIDUAL { get; set; }

        public int? DELAY_CX_INDIVIDUAL { get; set; }

        public bool? CHK_IMEI_AP_COLETIVA { get; set; }

        public int? QTD_PRODUTO_VOLUME { get; set; }

        [StringLength(100)]
        public string PATH_SAP_FILE_VOLUME { get; set; }

        [StringLength(3)]
        public string VOLUME_STRING { get; set; }

        public bool? IGNORAR { get; set; }

        public bool? RETIRA_ESPACO_PALLET { get; set; }

        [StringLength(100)]
        public string PATH_REMESSA { get; set; }

        public bool? VERIFICAR_SKU_SAV { get; set; }

        public bool? CONTADORES_ATIVOS { get; set; }

        public int? QTD_COLETIVA_LIN { get; set; }

        public int? QTD_COLETIVA_COL { get; set; }

        public int? QTD_COLETIVA_BARCODE { get; set; }

        public int? SLCB_PORTA { get; set; }

        [StringLength(50)]
        public string SLCB_HOSTNAME { get; set; }

        public int? SLCB_TIMEOUT { get; set; }

        public int? SLCB_ORDEM_IMEI { get; set; }

        public int? SLCB_ORDEM_SKU { get; set; }

        public int? SLCB_ORDEM_EAN { get; set; }

        public int? SLCB_ORDEM_SAP { get; set; }

        [StringLength(100)]
        public string SLCB_PATHFILE_ERROS { get; set; }

        public bool? SLCB_PESO_ATIVO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GBR_CADASTRO_DE_PATH_FT> GBR_CADASTRO_DE_PATH_FT { get; set; }
    }
}
