namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_indtppt
    {
        [Key]
        public long id_indtppt { get; set; }

        public decimal? ind_inferior { get; set; }

        public decimal? ind_superior { get; set; }

        public decimal? ind_meta { get; set; }

        public long id_tppt { get; set; }

        public long id_ind { get; set; }

        public virtual om_ind om_ind { get; set; }

        public virtual om_tppt om_tppt { get; set; }
    }
}
