﻿using SEMP.DAL.Models;
using SEMP.Model.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEMP.DAL.DAO.Materiais
{
	public class daoEntradaIAC
	{
		/// <summary>
		/// Obter dados de entrada de materiais por Ordem de Produção
		/// </summary>
		/// <param name="numOP"></param>
		/// <returns></returns>
		public static List<CBEntradaIACDTO> ObterEntradasIAC(string numOP, DateTime datInicio, DateTime datFim) {

			using (var contexto = new DTB_CBContext())
			{

				var dados = contexto.tbl_CBEntradaIAC.Where(x => x.NumOP == numOP && x.DatLeitura >= datInicio && x.DatLeitura <= datFim).ToList();

				var retorno = dados.Select(x => new CBEntradaIACDTO() {
					NumOP = x.NumOP,
					BarrasFornecedor = x.BarrasFornecedor,
					BarrasUD = x.BarrasUD,
					CodItem = x.CodItem,
					DatLeitura = x.DatLeitura,
					NomUsuario = x.NomUsuario,
					NumUD = x.NumUD,
					QtdItem = x.QtdItem
				}).ToList();

				return retorno;

			}
		
		}

		/// <summary>
		/// Obter os dados de uma etiqueta lida
		/// </summary>
		/// <param name="BarrasUD">Código de barras da etiqueta</param>
		/// <returns></returns>
		public static CBEntradaIACDTO ObterLeituraUD(string BarrasUD)
		{

			using (var contexto = new DTB_CBContext())
			{

				var dados = contexto.tbl_CBEntradaIAC.FirstOrDefault(x => x.BarrasUD == BarrasUD);
				if (dados != null)
				{
					var retorno = new CBEntradaIACDTO()
					{
						NumOP = dados.NumOP,
						BarrasFornecedor = dados.BarrasFornecedor,
						BarrasUD = dados.BarrasUD,
						CodItem = dados.CodItem,
						DatLeitura = dados.DatLeitura,
						NomUsuario = dados.NomUsuario,
						NumUD = dados.NumUD,
						QtdItem = dados.QtdItem
					};

					return retorno;
				}
				else
				{
					return null;
				}

			}

		}

		/// <summary>
		/// Obter os dados de uma etiqueta de fornecedor lida
		/// </summary>
		/// <param name="BarrasFornecedor">Código de barras da etiqueta</param>
		/// <returns></returns>
		public static CBEntradaIACDTO ObterLeituraFornecedor(string BarrasFornecedor)
		{

			using (var contexto = new DTB_CBContext())
			{

				var dados = contexto.tbl_CBEntradaIAC.FirstOrDefault(x => x.BarrasFornecedor == BarrasFornecedor);
				if (dados != null)
				{
					var retorno = new CBEntradaIACDTO()
					{
						NumOP = dados.NumOP,
						BarrasFornecedor = dados.BarrasFornecedor,
						BarrasUD = dados.BarrasUD,
						CodItem = dados.CodItem,
						DatLeitura = dados.DatLeitura,
						NomUsuario = dados.NomUsuario,
						NumUD = dados.NumUD,
						QtdItem = dados.QtdItem
					};

					return retorno;
				}
				else
				{
					return new CBEntradaIACDTO();
				}

			}

		}

		/// <summary>
		/// Obter os dados de um range de UDs
		/// </summary>
		/// <param name="numUD">Lista com as UDs a consultar</param>
		/// <returns></returns>
		public static List<CBEntradaIACDTO> ObterLeituraUD(List<string> numUD)
		{

			using (var contexto = new DTB_CBContext())
			{

				var dados = contexto.tbl_CBEntradaIAC.Where(x => numUD.Contains(x.NumUD)).ToList();
				if (dados != null)
				{
					var retorno = dados.Select(x => new CBEntradaIACDTO()
					{
						NumOP = x.NumOP,
						BarrasFornecedor = x.BarrasFornecedor,
						BarrasUD = x.BarrasUD,
						CodItem = x.CodItem,
						DatLeitura = x.DatLeitura,
						NomUsuario = x.NomUsuario,
						NumUD = x.NumUD,
						QtdItem = x.QtdItem
					}).ToList();

					return retorno;
				}
				else
				{
					return null;
				}

			}

		}

		public static bool GravarEntradaIAC(CBEntradaIACDTO dados){

			using (var contexto = new DTB_CBContext())
			{

				var gravar = contexto.tbl_CBEntradaIAC.FirstOrDefault(x => x.BarrasUD == dados.BarrasUD);

				if (gravar != null)
				{
					gravar.BarrasUD = dados.BarrasUD;
					gravar.DatLeitura = dados.DatLeitura;
					gravar.NomUsuario = dados.NomUsuario;
				}
				else
				{
					gravar = new tbl_CBEntradaIAC() {
						BarrasFornecedor = dados.BarrasFornecedor,
						NomUsuario = dados.NomUsuario,
						DatLeitura = dados.DatLeitura, 
						BarrasUD = dados.BarrasUD,
						CodItem = dados.CodItem,
						NumOP = dados.NumOP,
						NumUD = dados.NumUD,
						QtdItem = dados.QtdItem
					};

					contexto.tbl_CBEntradaIAC.Add(gravar);
				}

				contexto.SaveChanges();

				return true;

			}

		}

	}

}
