﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using DotNet.Highcharts.Helpers;

namespace SEMP.Models
{
	public class DadosGrafico
	{
		public string Titulo { get; set; }
		public Data Dados { get; set; }
		public Color Cor { get; set; }
	}

    public class GraficoLinhaIAC
    {
        public DotNet.Highcharts.Highcharts Grafico { get; set; }
        public List<SEMP.Model.VO.CNQ.MaioresDefeitos> TabelaDefeito { get; set; }
    }
}