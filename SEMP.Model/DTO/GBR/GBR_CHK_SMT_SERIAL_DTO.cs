using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_CHK_SMT_SERIAL_DTO
    {
        public int COD_SMT { get; set; }
        public string OP { get; set; }
        public string SKU { get; set; }
        public string MODELO { get; set; }
        public string SERIAL { get; set; }
        public string PI_MAIN { get; set; }
        public string PI_SUB1 { get; set; }
        public string PI_SUB2 { get; set; }
        public Nullable<bool> VERIFICADO { get; set; }
        public string USUARIO { get; set; }
        public string SESSAO { get; set; }
        public string IMEI1 { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
    }
}
