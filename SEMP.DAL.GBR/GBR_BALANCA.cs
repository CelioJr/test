namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_BALANCA
    {
        [Key]
        public int COD_BALANCA { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PESO { get; set; }

        public DateTime? DATA { get; set; }

        [StringLength(15)]
        public string IP { get; set; }

        public DateTime? DATA_LIDA { get; set; }

        public bool? F_PROCESSADO { get; set; }
    }
}
