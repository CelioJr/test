namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolpamo
    {
        [Key]
        public long id_consolpamo { get; set; }

        public long id_consolpa { get; set; }

        public long id_consolmo { get; set; }

        public decimal? qt_auto_tempoparada_cp { get; set; }

        public decimal? qt_auto_tempoparada_sp { get; set; }

        public decimal? qt_manu_tempoparada_cp { get; set; }

        public decimal? qt_manu_tempoparada_sp { get; set; }

        public decimal? seg_auto_tempoparada_cp { get; set; }

        public decimal? seg_auto_tempoparada_sp { get; set; }

        public decimal? seg_manu_tempoparada_cp { get; set; }

        public decimal? seg_manu_tempoparada_sp { get; set; }

        public decimal? seg_auto_tempoparada_cp_vr { get; set; }

        public decimal? seg_auto_tempoparada_sp_vr { get; set; }

        public decimal? seg_manu_tempoparada_cp_vr { get; set; }

        public decimal? seg_manu_tempoparada_sp_vr { get; set; }

        public decimal? qt_auto_ocoparada_cp_vr { get; set; }

        public decimal? qt_auto_ocoparada_sp_vr { get; set; }

        public decimal? qt_manu_ocoparada_cp_vr { get; set; }

        public decimal? qt_manu_ocoparada_sp_vr { get; set; }

        public decimal? seg_auto_tempoparada_default { get; set; }

        public decimal? seg_auto_tempoparada_sem_op { get; set; }

        public decimal? seg_auto_tempoparada_sem_evt { get; set; }

        public decimal? seg_auto_tempoparada_sem_cnx { get; set; }

        public decimal? seg_manu_tempoparada_default { get; set; }

        public decimal? seg_manu_tempoparada_sem_op { get; set; }

        public decimal? seg_manu_tempoparada_sem_evt { get; set; }

        public decimal? seg_manu_tempoparada_sem_cnx { get; set; }

        public decimal? qt_auto_tempoparada_default { get; set; }

        public decimal? qt_auto_tempoparada_sem_op { get; set; }

        public decimal? qt_auto_tempoparada_sem_evt { get; set; }

        public decimal? qt_auto_tempoparada_sem_cnx { get; set; }

        public decimal? qt_manu_tempoparada_default { get; set; }

        public decimal? qt_manu_tempoparada_sem_op { get; set; }

        public decimal? qt_manu_tempoparada_sem_evt { get; set; }

        public decimal? qt_manu_tempoparada_sem_cnx { get; set; }

        public decimal? seg_auto_cta { get; set; }

        public decimal? seg_manu_cta { get; set; }

        public virtual dw_consolmo dw_consolmo { get; set; }
    }
}
