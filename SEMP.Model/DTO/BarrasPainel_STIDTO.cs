﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.DTO
{
	public class BarrasPainel_STIDTO
	{
		public string CodModelo { get; set; }
		public int QtdEmbalada { get; set; }
		public int QtdAprovada { get; set; }
		public string NumAP { get; set; }
		public int QtdPrograma { get; set; }
		public int QtdApontada { get; set; }
		public string DesModelo { get; set; }
		public string FlgDivida { get; set; }
		public string DesLinha { get; set; }

	}
}
