namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_passagem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_passagem()
        {
            dw_nserie = new HashSet<dw_nserie>();
            dw_nserie1 = new HashSet<dw_nserie>();
            dw_passcau1 = new HashSet<dw_passcau>();
            dw_passdef = new HashSet<dw_passdef>();
            dw_passmon = new HashSet<dw_passmon>();
            dw_passtf = new HashSet<dw_passtf>();
        }

        [Key]
        public long id_passagem { get; set; }

        public DateTime? dthr { get; set; }

        public int? ms_Dthr { get; set; }

        public long id_nserie { get; set; }

        public long id_consolid { get; set; }

        public long? id_usroperador { get; set; }

        public long? id_usrsupervisor { get; set; }

        public long id_pt { get; set; }

        public short? st_nserie { get; set; }

        public decimal? seg_ciclo { get; set; }

        public DateTime? dthr_inicio { get; set; }

        public int? ms_dthrinicio { get; set; }

        public long? id_est { get; set; }

        public long? id_passcau { get; set; }

        public short? is_tf_finalizado { get; set; }

        [StringLength(256)]
        public string ds_diariobordo { get; set; }

        public long? id_area { get; set; }

        public virtual dw_consolid dw_consolid { get; set; }

        public virtual dw_est dw_est { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_nserie> dw_nserie { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_nserie> dw_nserie1 { get; set; }

        public virtual dw_nserie dw_nserie2 { get; set; }

        public virtual dw_t_area dw_t_area { get; set; }

        public virtual dw_passcau dw_passcau { get; set; }

        public virtual om_pt om_pt { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_passcau> dw_passcau1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_passdef> dw_passdef { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_passmon> dw_passmon { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_passtf> dw_passtf { get; set; }
    }
}
