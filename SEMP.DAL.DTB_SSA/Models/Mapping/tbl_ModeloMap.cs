using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.DTB_SSA.Models.Mapping
{
    public class tbl_ModeloMap : EntityTypeConfiguration<tbl_Modelo>
    {
        public tbl_ModeloMap()
        {
            // Primary Key
            this.HasKey(t => t.codModelo);

            // Properties
            this.Property(t => t.codModelo)
                .IsRequired()
                .HasMaxLength(13);

            this.Property(t => t.CODEAN)
                .HasMaxLength(13);

            this.Property(t => t.desTitulo)
                .HasMaxLength(20);

            this.Property(t => t.desConfig)
                .HasMaxLength(750);

            this.Property(t => t.desModelo)
                .HasMaxLength(150);

            this.Property(t => t.codComercial)
                .HasMaxLength(16);

            this.Property(t => t.desPartNumber)
                .HasMaxLength(50);

            this.Property(t => t.ModeloVPE)
                .HasMaxLength(16);

            this.Property(t => t.flgColetaSerial)
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("tbl_Modelo");
            this.Property(t => t.codModelo).HasColumnName("codModelo");
            this.Property(t => t.CODEAN).HasColumnName("CODEAN");
            this.Property(t => t.desTitulo).HasColumnName("desTitulo");
            this.Property(t => t.desConfig).HasColumnName("desConfig");
            this.Property(t => t.desModelo).HasColumnName("desModelo");
            this.Property(t => t.codComercial).HasColumnName("codComercial");
            this.Property(t => t.codOperador).HasColumnName("codOperador");
            this.Property(t => t.datAtualizacao).HasColumnName("datAtualizacao");
            this.Property(t => t.desPartNumber).HasColumnName("desPartNumber");
            this.Property(t => t.ModeloVPE).HasColumnName("ModeloVPE");
            this.Property(t => t.flgColetaSerial).HasColumnName("flgColetaSerial");

        }
    }
}
