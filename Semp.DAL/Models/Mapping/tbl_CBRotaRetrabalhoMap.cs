﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
	public class tbl_CBRotaRetrabalhoMap : EntityTypeConfiguration<tbl_CBRotaRetrabalho>
	{
		public tbl_CBRotaRetrabalhoMap()
		{
			// Primary Key
			this.HasKey(t => new { t.CodRetrabalho, t.CodLinha, t.NumPosto });

			// Properties
			this.Property(t => t.CodRetrabalho)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.CodLinha)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.NumPosto)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.FlgUltimoPosto)
				.IsFixedLength()
				.HasMaxLength(1);

			// Table & Column Mappings
			this.ToTable("tbl_CBRotaRetrabalho");
			this.Property(t => t.CodRetrabalho).HasColumnName("CodRetrabalho");
			this.Property(t => t.CodLinha).HasColumnName("CodLinha");
			this.Property(t => t.NumPosto).HasColumnName("NumPosto");
			this.Property(t => t.FlgUltimoPosto).HasColumnName("FlgUltimoPosto");

			// Relationships
			this.HasRequired(t => t.tbl_CBRetrabalho)
				.WithMany(t => t.tbl_CBRotaRetrabalho)
				.HasForeignKey(d => d.CodRetrabalho);

		}
	}
}
