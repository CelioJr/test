using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_Departamento
    {
        public string CodFab { get; set; }
        public string CodDepto { get; set; }
        public string NomDepto { get; set; }
        public string NumDepto { get; set; }
        public System.Guid rowguid { get; set; }
        public string FlgAtivo { get; set; }
    }
}
