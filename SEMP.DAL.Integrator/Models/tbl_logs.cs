namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_logs
    {
        public int ID { get; set; }

        public DateTime DATAEXEC { get; set; }

        public int ID_TAREFA { get; set; }

        public int ID_ERRO { get; set; }

        [StringLength(300)]
        public string MSG { get; set; }

        [StringLength(2)]
        public string TIPO { get; set; }

        public virtual tbl_erros tbl_erros { get; set; }

        public virtual tbl_TAREFA tbl_TAREFA { get; set; }
    }
}
