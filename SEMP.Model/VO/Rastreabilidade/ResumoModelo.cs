﻿using System;

namespace SEMP.Model.VO.Rastreabilidade
{
	public class ResumoModelo
	{
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public DateTime? Data { get; set; }
		public int QtdProduzido { get; set; }
		public int QtdPlanejado { get; set; }
		public double Eficiencia { get; set; }
		public int Ordem { get; set; }

	}
}
