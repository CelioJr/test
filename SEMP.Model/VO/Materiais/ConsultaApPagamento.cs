﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Materiais
{
	public class ConsultaApPagamento
	{
		public string NumAP { get; set; }
		public string CodFab { get; set; }
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public decimal QtdNecessaria { get; set; }
		public decimal? QtdProduzida { get; set; }
		public decimal? QtdPaga { get; set; }
		public string CodItem { get; set; }
		public string DesItem { get; set; }		
		  
	}
}
