using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_COVER_LED_DTO
    {
        public string ID_COVER { get; set; }
        public Nullable<System.DateTime> PROD_DATE { get; set; }
        public string OP { get; set; }
        public Nullable<int> COD_LINE { get; set; }
        public Nullable<int> COD_SKU { get; set; }
        public virtual ALC_ORDER_DTO ALC_ORDER { get; set; }
    }
}
