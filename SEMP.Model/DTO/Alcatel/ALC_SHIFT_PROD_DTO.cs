using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_SHIFT_PROD_DTO
	{
        public ALC_SHIFT_PROD_DTO()
        {
            this.ALC_INTERVAL_PROD = new List<ALC_INTERVAL_PROD_DTO>();
        }

        public int idShift { get; set; }
        public string description { get; set; }
        public bool enable { get; set; }
        public Nullable<int> HOUR_FINAL { get; set; }
        public Nullable<int> HOUR_INICIAL { get; set; }
        public Nullable<int> MIN_FINAL { get; set; }
        public Nullable<int> MIN_INI { get; set; }
        public int id_shift { get; set; }
        public virtual ICollection<ALC_INTERVAL_PROD_DTO> ALC_INTERVAL_PROD { get; set; }
    }
}
