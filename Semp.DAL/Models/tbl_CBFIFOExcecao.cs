using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBFIFOExcecao
    {
        public string NumLote { get; set; }
        public string NumCC { get; set; }
        public Nullable<System.DateTime> Data { get; set; }
        public string Observacao { get; set; }
        public string NomUsuario { get; set; }
    }
}
