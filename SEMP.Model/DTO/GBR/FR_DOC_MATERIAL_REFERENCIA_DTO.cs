using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DOC_MATERIAL_REFERENCIA_DTO
    {
        public int DOC_CODIGO { get; set; }
        public string REF_BIBLIOGRAFIA { get; set; }
        public string REF_TIPO { get; set; }
        public virtual FR_DOC_PRINCIPAL_DTO FR_DOC_PRINCIPAL { get; set; }
    }
}
