namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ORDER_PARTNERS
    {
        public Guid ID { get; set; }

        public Guid ORDER_HEADER_IN_ID { get; set; }

        [StringLength(2)]
        public string PARTN_ROLE { get; set; }

        [StringLength(10)]
        public string PARTN_NUMB { get; set; }

        [StringLength(40)]
        public string NAME { get; set; }

        [StringLength(35)]
        public string STREET { get; set; }

        [StringLength(10)]
        public string HOUSE_NUM1 { get; set; }

        [StringLength(40)]
        public string DISTRICT { get; set; }

        [StringLength(10)]
        public string HOUSE_NUM2 { get; set; }

        [StringLength(10)]
        public string POSTL_CODE { get; set; }

        [StringLength(40)]
        public string CITY { get; set; }

        [StringLength(3)]
        public string REGION { get; set; }

        [StringLength(3)]
        public string COUNTRY { get; set; }

        [StringLength(10)]
        public string TRANSPZONE { get; set; }

        [StringLength(30)]
        public string TELEPHONE { get; set; }

        [StringLength(30)]
        public string TELEPHONE2 { get; set; }

        [StringLength(241)]
        public string SMTP_ADDR { get; set; }

        [StringLength(11)]
        public string STCD2 { get; set; }

        [StringLength(16)]
        public string STCD1 { get; set; }

        [StringLength(18)]
        public string STCD3 { get; set; }

        [StringLength(1)]
        public string NATPERS { get; set; }

        public virtual ORDER_HEADER_IN ORDER_HEADER_IN { get; set; }
    }
}
