using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DOC_CASO_USO_INCLUSAO_DTO
    {
        public int DOC_CODIGO { get; set; }
        public int USO_CODIGO_PRINCIPAL { get; set; }
        public int USO_CODIGO_SUB_CASO { get; set; }
        public virtual FR_DOC_CASO_USO_DTO FR_DOC_CASO_USO { get; set; }
    }
}
