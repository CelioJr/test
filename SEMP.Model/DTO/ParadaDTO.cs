using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
	public class ParadaDTO
	{
		[Display(Name = "F�b.")]
		public string CodFab { get; set; }
		[Display(Name = "C�digo")]
		public string CodParada { get; set; }
		[Display(Name = "Descri��o")]
		public string DesParada { get; set; }
		[Display(Name = "Tipo")]
		public string TipParada { get; set; }
		[Display(Name = "rowguid")]
		public System.Guid rowguid { get; set; }

		[Display(Name = "Descri��o")]
		public string DesFabrica { get; set; }
	}
}
