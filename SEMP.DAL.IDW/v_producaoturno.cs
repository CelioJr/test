﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEMP.DAL.IDW
{
	public partial class v_producaoturno
	{
		[Key]
		[Column(Order = 1)]
		public Nullable<DateTime> dt_referencia { get; set; }
		[Key]
		[Column(Order = 2)]
		public string cd_turno { get; set; }
		public Nullable<DateTime> dthr_iturno { get; set; }
		public Nullable<DateTime> dthr_fturno { get; set; }
		public string cd_gt { get; set; }
		[Key]
		[Column(Order = 3)]
		public string cd_pt { get; set; }
		[Key]
		[Column(Order = 4)]
		public string cd_cp { get; set; }
		public string cd_folha { get; set; }
		public Nullable<long> producaobruta { get; set; }
		public Int16? is_aponGt { get; set; }

		public string codModelo { get {
				return !String.IsNullOrEmpty(cd_cp) && cd_cp.Length >= 6 ? cd_cp.Substring(0, 6) : "";
		} }

	}
}