using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GpaAreaDTO
    {
        public string CodArea { get; set; }
        public string DesArea { get; set; }
    }
}
