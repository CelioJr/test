namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.alc_api_user")]
    public partial class alc_api_user
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public alc_api_user()
        {
            alc_api_role = new HashSet<alc_api_role>();
            alc_api_role1 = new HashSet<alc_api_role>();
        }

        public long id { get; set; }

        [StringLength(255)]
        public string password { get; set; }

        [StringLength(150)]
        public string username { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<alc_api_role> alc_api_role { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<alc_api_role> alc_api_role1 { get; set; }
    }
}
