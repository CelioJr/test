using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBDestinatarioEmailMap : EntityTypeConfiguration<tbl_CBDestinatarioEmail>
    {
        public tbl_CBDestinatarioEmailMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodRotina, t.EmailDestinatario });

            // Properties
            this.Property(t => t.CodRotina)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.EmailDestinatario)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.NomDestinatario)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("tbl_CBDestinatarioEmail");
            this.Property(t => t.CodRotina).HasColumnName("CodRotina");
            this.Property(t => t.EmailDestinatario).HasColumnName("EmailDestinatario");
            this.Property(t => t.NomDestinatario).HasColumnName("NomDestinatario");
        }
    }
}
