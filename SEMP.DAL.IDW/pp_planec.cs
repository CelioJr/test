namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_planec
    {
        [Key]
        public long id_planec { get; set; }

        public long id_plano { get; set; }

        public long id_nec { get; set; }

        public int? prioridade { get; set; }

        public virtual pp_nec pp_nec { get; set; }

        public virtual pp_plano pp_plano { get; set; }
    }
}
