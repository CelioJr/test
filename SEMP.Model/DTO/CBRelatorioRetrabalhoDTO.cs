﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.DTO
{
    public class CBRelatorioRetrabalhoDTO
    {
        public string NumECB { get; set; }
        public string Modelo { get; set; }
        public string CodModelo { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public System.DateTime DatEvento { get; set; }
        public string Linha { get; set; }

        public int Total { get; set; }
        public int TotalModelo { get; set; }
        
    }
}
