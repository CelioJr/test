﻿namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
    public class LeiturasPostos
	{
		public int CodLinha { get; set; }
		public string DesLinha { get; set; }
		public int NumPosto { get; set; }
		public string DesPosto { get; set; }
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public decimal Total { get; set; }
		public int QtdDefeito { get; set; }
		public int Ordem { get; set; }
		public int QtdRepetido { get; set; }
		public int QtdFalta { get; set; }
		public string FlgObrigatorio { get; set; }
	}
}
