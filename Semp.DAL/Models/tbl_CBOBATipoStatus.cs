﻿namespace SEMP.DAL.Models
{
	public class tbl_CBOBATipoStatus
	{
		public string CodStatus { get; set; }
		public string DesStatus { get; set; }
		public string DesCor { get; set; }
	}
}
