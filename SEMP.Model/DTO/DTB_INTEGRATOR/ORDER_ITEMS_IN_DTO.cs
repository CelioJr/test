namespace SEMP.Model.DTO
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;

	public partial class ORDER_ITEMS_IN_DTO
	{
        public Guid ID { get; set; }

        public Guid ORDER_HEADER_IN_ID { get; set; }

        [StringLength(100)]
        public string ITM_NUMBER { get; set; }

        [StringLength(4)]
        public string ITEM_CATEG { get; set; }

        [StringLength(40)]
        public string MATERIAL { get; set; }

        public decimal? TARGET_QTY { get; set; }

        [StringLength(4)]
        public string PLANT { get; set; }

        [StringLength(4)]
        public string STORE_LOC { get; set; }

        [StringLength(12)]
        public string ORDERID { get; set; }

        [StringLength(10)]
        public string PURCH_DATE { get; set; }

        [StringLength(10)]
        public string PO_DAT_S { get; set; }
    }
}
