using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_RPAMap : EntityTypeConfiguration<tbl_RPA>
    {
        public tbl_RPAMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.NumRPA });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NumRPA)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodDepto)
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.NomRequisitante)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(25)
                .IsUnicode(false);

            this.Property(t => t.CodMotivo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodItem)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumSerieMod)
                .IsFixedLength()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.FlgReintegra)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.ObsRPA)
                .HasMaxLength(512)
                .IsUnicode(false);

            this.Property(t => t.Usuario)
                .IsFixedLength()
                .HasMaxLength(25)
                .IsUnicode(false);

            this.Property(t => t.NomRespSetor)
                .HasMaxLength(25)
                .IsUnicode(false);

            this.Property(t => t.NomDiretor)
                .HasMaxLength(25)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_RPA");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.NumRPA).HasColumnName("NumRPA");
            this.Property(t => t.DatEmissao).HasColumnName("DatEmissao");
            this.Property(t => t.CodDepto).HasColumnName("CodDepto");
            this.Property(t => t.NomRequisitante).HasColumnName("NomRequisitante");
            this.Property(t => t.CodMotivo).HasColumnName("CodMotivo");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.NumSerieMod).HasColumnName("NumSerieMod");
            this.Property(t => t.QtdRequisitado).HasColumnName("QtdRequisitado");
            this.Property(t => t.DatRetornoExp).HasColumnName("DatRetornoExp");
            this.Property(t => t.QtdRetorno).HasColumnName("QtdRetorno");
            this.Property(t => t.FlgReintegra).HasColumnName("FlgReintegra");
            this.Property(t => t.ObsRPA).HasColumnName("ObsRPA");
            this.Property(t => t.Usuario).HasColumnName("Usuario");
            this.Property(t => t.NomRespSetor).HasColumnName("NomRespSetor");
            this.Property(t => t.DatRespSetor).HasColumnName("DatRespSetor");
            this.Property(t => t.NomDiretor).HasColumnName("NomDiretor");
            this.Property(t => t.DatDiretor).HasColumnName("DatDiretor");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
        }
    }
}
