namespace SEMP.Model.DTO
{
    public class CBPerfilMascaraDTO
    {
        public string CodItem { get; set; }
        public string Mascara { get; set; }

		public string DesItem { get; set; }
	}
}
