using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_ProdAmplitude
    {
        public string CodFab { get; set; }
        public string CodDepto { get; set; }
        public string NomUsuario { get; set; }
        public string TipUsuario { get; set; }
        public string NomEmail { get; set; }
        public System.Guid rowguid { get; set; }
    }
}
