namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_propaihomo
    {
        [Key]
        public long id_propaihomo { get; set; }

        public long? id_produtopai { get; set; }

        public long? id_produto { get; set; }

        public virtual om_produto om_produto { get; set; }

        public virtual om_produto om_produto1 { get; set; }
    }
}
