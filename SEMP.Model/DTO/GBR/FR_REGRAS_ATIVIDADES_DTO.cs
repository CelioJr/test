using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_REGRAS_ATIVIDADES_DTO
    {
        public int ATV_COD { get; set; }
        public string ATV_NOME { get; set; }
        public string ATV_NOME_REAL { get; set; }
        public string ATV_DESCRICAO { get; set; }
        public string ATV_PARAMS { get; set; }
        public string ATV_RETORNO { get; set; }
        public string ATV_COMPATIBILIDADE { get; set; }
    }
}
