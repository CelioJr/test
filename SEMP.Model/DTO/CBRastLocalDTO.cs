using System;

namespace SEMP.Model.DTO
{
    public class CBRastLocalDTO
    {
        public string NumSerieRastLocal { get; set; }
        public string NumSerieProd { get; set; }
        public Nullable<System.DateTime> DatRegistro { get; set; }
        public string CodDRT { get; set; }
    }
}
