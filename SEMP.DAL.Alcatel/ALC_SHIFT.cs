namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_SHIFT")]
    public partial class ALC_SHIFT
    {
        [Key]
        public int ID_SHIFT { get; set; }

        [Required]
        [StringLength(30)]
        public string DESCRIPTION { get; set; }
    }
}
