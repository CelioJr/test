﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.Rastreabilidade;
using SEMP.Model.DTO;

namespace SEMP.Areas.Coleta.Controllers
{
	public class GerirSisapController : Controller
	{
		//
		// GET: /Coleta/GerirSisap/
		public ActionResult Index()
		{

			var config = new Configuracao();

			return View(config.ObterLinhas().Where(x => x.CodLinhaT1 != null).ToList());

		}

		public ActionResult LinhasSetor(string setor, string flgAtivo) {

			var config = new Configuracao();

			var linhas = config.ObterLinhas().Where(x => x.Setor == setor && x.CodLinhaT1 != null).ToList();

			if (flgAtivo == "S") {
				linhas = linhas.Where(x => x.FlgAtivo == "1").ToList();
			}

			var postos = new List<CBPostoDTO>();

			foreach (var linha in linhas)
			{
				postos.AddRange(config.ObterPostos(linha.CodLinha).Where(x => x.FlgAtivo == "S").ToList());
			}

			ViewBag.postos = postos;

			return View(linhas);


		}

		public ActionResult Postos(int id)
		{

			var config = new Configuracao();

			ViewBag.Linha = config.ObterLinha(id);

			return View(config.ObterPostos(id).ToList());

		}

		public ActionResult AtivarPosto(int codLinha, int numPosto)
		{
			var config = new Configuracao();

			config.AtivaDesativaPosto(codLinha, numPosto, true);

			return Json(false, JsonRequestBehavior.AllowGet);
		}

		public ActionResult DesativarPosto(int codLinha, int numPosto)
		{
			var config = new Configuracao();

			config.AtivaDesativaPosto(codLinha, numPosto, false);

			return Json(false, JsonRequestBehavior.AllowGet);
		}
	}
}