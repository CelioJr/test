using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
	public class tbl_Linha
	{
		public string CodLinha { get; set; }
		public string DesDep { get; set; }
		public string DesLinha { get; set; }
		public string Usuario { get; set; }
		public Nullable<System.Guid> rowguid { get; set; }
		public string FlgTipo { get; set; }
		public string FlgAtivo { get; set; }
	}
}
