using System;

namespace SEMP.Model.DTO
{
    /// <summary>
    /// Classe respons�vel por recuperar os dados que ser�o exibidos na tela do Perfil de amarra��o.
    /// </summary>
	[Serializable]
    public class viw_CBPerfilAmarraDTO
    {
        public int CodLinha { get; set; }
        public int NumPosto { get; set; }
        public string CodModelo { get; set; }
        public int CodTipoAmarra { get; set; }
        public string CodItem { get; set; }
        public string Mascara { get; set; }
        public string DesTipoAmarra { get; set; }
        public string DesModelo { get; set; }
        public string DesItem { get; set; }

        public bool IsDispositivoIntegrado { get; set; }
        public int DipositivoID { get; set; }

        public string numAP { get; set; }

        public int? flgPlacaPainel;
    }
}
