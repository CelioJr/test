using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DTB_CB.Models.Mapping
{
    public class tbl_CBTipoPostoMap : EntityTypeConfiguration<tbl_CBTipoPosto>
    {
        public tbl_CBTipoPostoMap()
        {
            // Primary Key
            this.HasKey(t => t.CodTipoPosto);

            // Properties
            this.Property(t => t.CodTipoPosto)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DesTipoPosto)
                .HasMaxLength(50);

            //this.Property(t => t.DesAcao)
            //    .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("tbl_CBTipoPosto");
            this.Property(t => t.CodTipoPosto).HasColumnName("CodTipoPosto");
            this.Property(t => t.DesTipoPosto).HasColumnName("DesTipoPosto");
            //this.Property(t => t.DesAcao).HasColumnName("DesAcao");
        }
    }
}
