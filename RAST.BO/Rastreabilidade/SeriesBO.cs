﻿using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.DTO;
using SEMP.Model.VO.Rastreabilidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAST.BO.Rastreabilidade
{
	public class SeriesBO
	{

		public static List<EstruturaModelo> ObterEstrutura(string codModelo){

			return daoSeries.ObterEstrutura(codModelo);

		}

		public static List<CBEmbaladaDTO> ObterListaEmbalagem(DateTime datInicio, DateTime datFim, string codModelo) 
		{
			return daoSeries.ObterListaEmbalagem(datInicio, datFim, codModelo);
		}

        public static List<CBEmbaladaDTO> ObterListaEmbalagemAP(string codFab, string numAP)
        {
            return daoSeries.ObterListaEmbalagemAP(codFab, numAP);
        }

    }
}
