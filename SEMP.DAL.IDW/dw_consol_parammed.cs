namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consol_parammed
    {
        [Key]
        public long id_consolparammed { get; set; }

        public long? id_consolparam { get; set; }

        public long? id_consolmedparamlog { get; set; }

        public long? id_consolpa_param { get; set; }

        public long? id_consolpaoco { get; set; }

        public virtual dw_consol_param dw_consol_param { get; set; }

        public virtual dw_consolmedparamlog dw_consolmedparamlog { get; set; }

        public virtual dw_consolpa_param dw_consolpa_param { get; set; }

        public virtual dw_consolpaoco dw_consolpaoco { get; set; }
    }
}
