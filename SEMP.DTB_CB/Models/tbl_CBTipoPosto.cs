using System;
using System.Collections.Generic;

namespace SEMP.DTB_CB.Models
{
    public partial class tbl_CBTipoPosto
    {
        public tbl_CBTipoPosto()
        {
            this.tbl_CBPosto = new List<tbl_CBPosto>();
            //this.tbl_CBPostoSequencia = new List<tbl_CBPostoSequencia>();
        }

        public int CodTipoPosto { get; set; }
        public string DesTipoPosto { get; set; }
        //public string DesAcao { get; set; }
        public virtual ICollection<tbl_CBPosto> tbl_CBPosto { get; set; }
        //public virtual ICollection<tbl_CBPostoSequencia> tbl_CBPostoSequencia { get; set; }
    }
}
