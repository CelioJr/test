﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.PM;
using SEMP.BO;
using SEMP.Model.DTO;

namespace SEMP.Areas.Outros.Controllers
{
	public class ParadaDepController : Controller
	{
		//
		// GET: /Outros/ParadaDep/
		public ActionResult Index(string codFab, string numParada)
		{
			var dados = ParadaDeLinha.ObterListaParadaDep(codFab, numParada);

			return View(dados);
		}


		//
		// GET: /Outros/ParadaDep/Create
		public ActionResult Create()
		{
			return View();
		}

		//
		// POST: /Outros/ParadaDep/Create
		[HttpPost]
		public string Create(ProdParadaDepDTO dados)
		{
			try
			{
				ParadaDeLinha.GravarParadaDep(dados);

				return "";
			}
			catch (Exception ex)
			{
				return "Erro ao cadastrar: " + ex.Message;
			}
		}

		//
		// GET: /Outros/ParadaDep/Edit/5
		public ActionResult Edit(string codFab, string numParada, string codDepto)
		{
			var dado = ParadaDeLinha.ObterParadaDep(codFab, numParada, codDepto);

			return View(dado);
		}

		//
		// POST: /Outros/ParadaDep/Edit/5
		[HttpPost]
		public string Edit(ProdParadaDepDTO parada)
		{
			try
			{
				ParadaDeLinha.GravarParadaDep(parada);

				return "";
			}
			catch (Exception ex)
			{
				return "Erro ao atualizar: " + ex.Message;
			}
		}

		//
		// GET: /Outros/ParadaDep/Delete/5
		public ActionResult Delete(string codFab, string numParada, string codDepto)
		{
			var dado = ParadaDeLinha.ObterParadaDep(codFab, numParada, codDepto);

			return View(dado);
		}

		//
		// POST: /Outros/ParadaDep/Apagar/5
		[HttpPost]
		public string Apagar(string codFab, string numParada, string codDepto)
		{
			try
			{
				ParadaDeLinha.RemoverParadaDep(numParada, codFab, codDepto);

				return "";
			}
			catch (Exception ex)
			{
				return "Erro ao remover: " + ex.Message;
			}
		}

	}
}
