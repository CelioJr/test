using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class CBTAModeloDTO
    {
        public string CodModelo { get; set; }
        public System.DateTime Data { get; set; }
        public Nullable<int> QtdAparelhos { get; set; }
        public Nullable<int> QtdDefeitos { get; set; }
        public Nullable<int> TempoIAC { get; set; }
        public Nullable<int> TempoIMC { get; set; }
        public Nullable<int> TempoMF { get; set; }
        public Nullable<int> WIP1 { get; set; }
        public Nullable<int> WIP2 { get; set; }
        public Nullable<int> TempoAtravessa { get; set; }
        public Nullable<int> TempoCorrido { get; set; }
		public Nullable<int> TempoWIP { get; set; }
		public Nullable<int> TempoProducao { get; set; }
		public Nullable<int> TempoTecnico { get; set; }
		public string Observacao { get; set; }
    }
}
