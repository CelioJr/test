namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolmooco
    {
        [Key]
        public long id_consolmooco { get; set; }

        public DateTime? dthr_ilogin { get; set; }

        public int? ms_dthrilogin { get; set; }

        public DateTime? dthr_flogin { get; set; }

        public int? ms_dthrflogin { get; set; }

        public short? is_continuaproximoperiodo { get; set; }

        public long id_consolmo { get; set; }

        public long id_consolmolog { get; set; }

        public virtual dw_consolmo dw_consolmo { get; set; }

        public virtual dw_consolmolog dw_consolmolog { get; set; }
    }
}
