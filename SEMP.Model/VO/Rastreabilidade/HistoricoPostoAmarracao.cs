﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade
{	
    /// <summary>
    /// Classe responsável por exibir o que foi lido no posto de amarração. 
    /// </summary>
    public class HistoricoPostoAmarracao
    {        
        public string Descricao { get; set; }
        public string Valor { get; set; }
        public int Tipo { get; set; }
        public int sequenciaOrdenacao { get; set; }
    }
}
