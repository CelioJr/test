using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.DTB_SSA.Models.Mapping
{
    public class tbl_PostoMap : EntityTypeConfiguration<tbl_Posto>
    {
        public tbl_PostoMap()
        {
            // Primary Key
            this.HasKey(t => t.posto_id);

            // Properties
            this.Property(t => t.posto_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.nome_posto)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.flgCategoria)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.codSigla)
                .IsFixedLength()
                .HasMaxLength(20);

            this.Property(t => t.flgTipoProduto)
                .IsFixedLength()
                .HasMaxLength(4);

            this.Property(t => t.flgTipoPosto)
                .IsFixedLength()
                .HasMaxLength(3);

            // Table & Column Mappings
            this.ToTable("tbl_Posto");
            this.Property(t => t.posto_id).HasColumnName("posto_id");
            this.Property(t => t.nome_posto).HasColumnName("nome_posto");
            this.Property(t => t.flgCategoria).HasColumnName("flgCategoria");
            this.Property(t => t.codSigla).HasColumnName("codSigla");
            this.Property(t => t.codAgrupamento).HasColumnName("codAgrupamento");
            this.Property(t => t.NumPosto).HasColumnName("NumPosto");
            this.Property(t => t.flgTipoProduto).HasColumnName("flgTipoProduto");
            this.Property(t => t.CodLinhaEmbal).HasColumnName("CodLinhaEmbal");
            this.Property(t => t.flgTipoPosto).HasColumnName("flgTipoPosto");     
        }
    }
}
