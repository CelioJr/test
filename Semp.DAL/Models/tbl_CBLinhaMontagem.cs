using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBLinhaMontagem
    {
        public tbl_CBLinhaMontagem()
        {
            this.tbl_CBLinhaModelo = new List<tbl_CBLinhaModelo>();
        }

        public int codLinha { get; set; }
        public System.DateTime DatVigenciaIni { get; set; }
        public System.DateTime DatVigenciaFim { get; set; }
        public byte numero { get; set; }
        public string tipo { get; set; }
        public int producao_horaria { get; set; }
        public string status { get; set; }
        public Nullable<System.DateTime> inicio_producao { get; set; }
        public System.DateTime Dia_inicio_producao { get; set; }
        public Nullable<System.DateTime> Dia_Intervalo_1_Inicio { get; set; }
        public Nullable<System.DateTime> Dia_Intervalo_1_Fim { get; set; }
        public Nullable<System.DateTime> Dia_Intervalo_2_Inicio { get; set; }
        public Nullable<System.DateTime> Dia_Intervalo_2_Fim { get; set; }
        public Nullable<System.DateTime> Dia_Intervalo_3_Inicio { get; set; }
        public Nullable<System.DateTime> Dia_Intervalo_3_Fim { get; set; }
        public Nullable<System.DateTime> Dia_Fim_producao { get; set; }
        public Nullable<System.DateTime> Noite_inicio_producao { get; set; }
        public Nullable<System.DateTime> Noite_Intervalo_1_Inicio { get; set; }
        public Nullable<System.DateTime> Noite_Intervalo_1_Fim { get; set; }
        public Nullable<System.DateTime> Noite_Intervalo_2_Inicio { get; set; }
        public Nullable<System.DateTime> Noite_Intervalo_2_Fim { get; set; }
        public Nullable<System.DateTime> Noite_Intervalo_3_Inicio { get; set; }
        public Nullable<System.DateTime> Noite_Intervalo_3_Fim { get; set; }
        public Nullable<System.DateTime> Noite_Fim_producao { get; set; }
        public Nullable<decimal> tempo_ciclo { get; set; }
        public Nullable<decimal> tempo_aviso_sonoro { get; set; }
        public Nullable<int> QtdPessoas { get; set; }
        public Nullable<decimal> Eficiencia { get; set; }
        public string flgAtivo { get; set; }
        public virtual ICollection<tbl_CBLinhaModelo> tbl_CBLinhaModelo { get; set; }
    }
}
