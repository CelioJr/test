using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
	public class tbl_CBTransfLoteMap : EntityTypeConfiguration<tbl_CBTransfLote>
	{
		public tbl_CBTransfLoteMap()
		{
			// Primary Key
			this.HasKey(t => new { t.CodLinhaOri, t.NumPostoOri, t.NumLote, t.FabDestino, t.CodLinhaDest });

			// Properties
			this.Property(t => t.CodLinhaOri)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.NumPostoOri)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.NumLote)
				.IsRequired()
				.HasMaxLength(18)
                .IsUnicode(false);

			this.Property(t => t.CodDRT)
				.IsFixedLength()
				.HasMaxLength(8)
                .IsUnicode(false);

			this.Property(t => t.FabDestino)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.CodLinhaDest)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.CodFab)
				.IsFixedLength()
				.HasMaxLength(2)
                .IsUnicode(false);

			this.Property(t => t.NumAP)
				.IsFixedLength()
				.HasMaxLength(6)
                .IsUnicode(false);

			// Table & Column Mappings
			this.ToTable("tbl_CBTransfLote");
			this.Property(t => t.CodLinhaOri).HasColumnName("CodLinhaOri");
			this.Property(t => t.NumPostoOri).HasColumnName("NumPostoOri");
			this.Property(t => t.NumLote).HasColumnName("NumLote");
			this.Property(t => t.CodDRT).HasColumnName("CodDRT");
			this.Property(t => t.FabDestino).HasColumnName("FabDestino");
			this.Property(t => t.CodLinhaDest).HasColumnName("CodLinhaDest");
			this.Property(t => t.DatTransf).HasColumnName("DatTransf");
			this.Property(t => t.DatRecebimento).HasColumnName("DatRecebimento");
			this.Property(t => t.CodFab).HasColumnName("CodFab");
			this.Property(t => t.NumAP).HasColumnName("NumAP");
		}
	}
}
