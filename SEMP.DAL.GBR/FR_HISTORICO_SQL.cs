namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_HISTORICO_SQL
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SQL_CODIGO { get; set; }

        [Required]
        public string SQL_SCRIPT { get; set; }

        public DateTime SQL_DATA { get; set; }

        [Required]
        [StringLength(3)]
        public string SIS_CODIGO { get; set; }

        [StringLength(100)]
        public string SQL_TABELA { get; set; }

        public virtual FR_SISTEMA FR_SISTEMA { get; set; }
    }
}
