﻿using MATERIAIS.BO.Estoque;
using System;
using System.Web.Mvc;

namespace SEMP.Areas.Materiais.Controllers
{
    public class PainelMateriaisController : Controller
    {
        // GET: Outros/PainelMateriais
        public ActionResult Index()
        {
			DateTime primeiroDia = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

			var dados = PainelMateriaisBO.Consultar(primeiroDia.ToString(), DateTime.Today.ToString());

			return View(dados);
		}

		public ActionResult Detalhes(string Pedido, string AP, string Fab)
		{
			var dados = PainelMateriaisBO.Detalhes(Pedido, AP, Fab);

			return View(dados);
		}

	}
}