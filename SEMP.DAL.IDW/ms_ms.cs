namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ms_ms
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ms_ms()
        {
            ms_msicup = new HashSet<ms_msicup>();
            ms_msihm = new HashSet<ms_msihm>();
        }

        [Key]
        public long id_ms { get; set; }

        [StringLength(30)]
        public string cd_ms { get; set; }

        public int? revisao { get; set; }

        [StringLength(100)]
        public string ds_ms { get; set; }

        public DateTime? dthr_stativo { get; set; }

        public DateTime? dthr_revisao { get; set; }

        public int? st_ativo { get; set; }

        [StringLength(100)]
        public string url_conexao { get; set; }

        public DateTime? dthr_heartbeat { get; set; }

        public short? seg_heartbeat { get; set; }

        public long id_usr { get; set; }

        public int? tp_calculoAndon { get; set; }

        public short? is_importoutm { get; set; }

        public virtual ms_usr ms_usr { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_msicup> ms_msicup { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_msihm> ms_msihm { get; set; }
    }
}
