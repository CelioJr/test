namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_desalimpendcontag
    {
        [Key]
        public long id_desalimpendcontag { get; set; }

        public long? id_estlocalpro { get; set; }

        public DateTime dthr_desalim { get; set; }

        public decimal qt_desalim { get; set; }

        public virtual dw_estlocalpro dw_estlocalpro { get; set; }
    }
}
