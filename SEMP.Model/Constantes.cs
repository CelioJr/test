﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model
{
	public static class Constantes
	{
		#region Fases dos produtos Notebook e Tablet

		public const int FASE_SUBESTOQUE = 10;
		public const int FASE_MONTAGEM = 20;
		public const int FASE_RUNIN = 30;
		public const int FASE_DOWNLOAD = 50;
		public const int FASE_REPARO = 40;
		public const int FASE_EMBALAGEM = 60;
		public const int FASE_CQ = 70;
		public const int FASE_ESTOQUE = 80;

		#endregion

		#region Erros gerais e de banco de dados

		public const string DB_OK = "E0001";
		public const string DB_ERRO = "E0002";
		public const string DB_TEMPOEXCEDIDO = "E0003";
		public const string DB_CHAVEPRIMARIA = "E0004";
		public const string CLIENT_ERROR = "E0005";

		public const string LOGIN_FUNC_NAOEXISTE = "E0006";
		public const string LOGIN_FUNC_DEMITIDO = "E0007";
		public const string NE_FILTRO_PRIVACIDADE_INVALIDO = "E0008";

		public const string MAC_LEITURA_INVALIDA = "E0009";
		public const string KIT_LEITURA_INVALIDA = "E0010";
		public const string IMAGEM_TOSHIBA_INVALIDA = "E0011";

		public const string ERRO_AO_REALIZAR_IMPRESSAO = "E0012";

		#endregion

		#region Erros de rastreabilidade

		public const string RAST_OK = "R0001";
		public const string RAST_NAOLIBERADO_DEFEITO = "R0002";
		public const string RAST_LEITURA_REPETIDA = "R0003";
		public const string RAST_FORA_SEQUENCIA = "R0004";
		public const string RAST_REGISTRONAOENCONTRADO = "R0005";
		public const string RAST_PLACANAOEXISTE = "R0006";
		public const string RAST_PLACA_EMBALADA = "R0007";
		public const string RAST_AP_FINALIZADA = "R0008";
		public const string RAST_MODELO_INCOMPATIVEL = "R0009";
		public const string RAST_LOTE_CHEIO = "R0010";
		public const string RAST_LOTE_VAZIO = "R0011";
		public const string RAST_LOTE_FECHADO = "R0012";
		public const string RAST_LOTE_NAOEXISTE = "R0013";
		public const string RAST_LOTE_NAOFINALIZADO = "R0014";
		public const string RAST_LOTE_FORASEQUENCIA = "R0015";
		public const string RAST_LOTE_ITEM_INEXISTENTE = "R0016";
		public const string RAST_REGISTRO_NAO_ENCONTRADO = "R0017";
		public const string RAST_TEMPO_LIMITE = "R0018";
		public const string RAST_REGISTRO_EXISTENTE = "R0019";
		public const string ITEM_CANCELADO = "R0020";
		public const string RAST_ERRO_TRANSF = "R0021";
		public const string RAST_TRANSF_OK = "R0022";
		public const string RAST_ERRO_FIFO = "R0023";
		public const string RAST_LOTE_NAO_RECEBIDO = "R0024";
		public const string RAST_IMPRESSAO_NAO_REALIZADA = "R0025";
		public const string RAST_DEFEITO_INEXISTENTE = "R0026";

		#endregion

		#region Tipos de Postos

		public const int POSTO_ENTRADA = 1;
		public const int POSTO_PASSAGEMDEFEITO = 2;
		public const int POSTO_PASSAGEM = 3;
		public const int POSTO_EMBALAGEM = 4;
		public const int POSTO_TECNICO = 5;
		public const int POSTO_AMARRACAO = 6;
		public const int POSTO_TROCAPECA = 7;
		public const int POSTO_REVISAO = 8;
		public const int POSTO_AMARRAFINAL = 9;
		public const int POSTO_LEITURA_EXPED = 10;
		public const int POSTO_LEITURALOTE = 11;
		public const int POSTO_CONFERENCIA = 12;
		public const int POSTO_TESTE_FECHAMENTO_DE_LOTE = 15;

		public const int POSTO_RECEBIMENTO = 18;
		public const int POSTO_AMARRA_NOTEBOOK = 20;
		public const int POSTO_AMARRA_LOTE_NOTEBOOK = 21;
		public const int POSTO_AMARRA_LOTE_TABLET = 22;
		public const int POSTO_AMARRA_TABLET = 23;
		public const int POSTO_PASSAGEM_TABLET = 27;
		public const int POSTO_AMARRA_LOTE_TABLET_IMPRESSAO_ETIQUETA_PESO = 29;
		public const int POSTO_AMARRA_LOTE_TABLET_IMPRESSAO_ETIQUETA = 30;
		public const int POSTO_AMARRA_LOTE_ENTREPOSTO_IAC = 31;
		public const int POSTO_PAINEL_IAC = 33;
		
		
		

		#endregion

		#region Geral
		public const string IAC = "IAC";
		public const string IMC = "IMC";
		public const string DAT = "DAT";
		public const string FEC = "FEC";
		public const string CANCELAR = "CANCELAR";
		public const int TAM_DEFEITO = 5;
		public const int TAM_DEFEITO_NOVO = 4;
		public const int TAM_PLACA = 12;
		public const int TAM_PRODUTO = 16;
		public const int TAM_PRODUTO_INFORMATICA = 9;
		public const int TAM_PLACA_INFORMATICA = 12;
		public const int TAM_LOTE = 18;
		public const string ERRO_SESSAO = "SESSAO";
		public const int FASE_IAC = 1;
		public const int FASE_IMC = 2;
		public const int FASE_MF = 3;
		#endregion

		#region Flags Defeitos
		public const int DEF_ABERTO = 0; // Defeito registrado no posto de leitura
		public const int DEF_REVISADO = 1; // Defeito revisado no posto técnico
		public const int DEF_CANCELADO = 2; // Defeito cancelado no posto de leitura
		public const int DEF_SCRAP = 3; // Scrap registrado no posto técnico
		public const int DEF_ALTERADO = 4; // Defeito alterado no posto técnico
		#endregion

		#region Tipo de amarração

		public const int AMARRA_PLACA = 1;
		public const int AMARRA_KIT = 2;
		public const int AMARRA_PAINEL = 3;
		public const int AMARRA_SUPORTE = 4;
		public const int AMARRA_PEDESTAL = 5;
		public const int AMARRA_PRODUTO = 6;
		public const int AMARRA_BASE = 7;
		public const int AMARRA_FONTE = 8;
		public const int AMARRA_PAINEL_LCD = 9;
		public const int AMARRA_KIT_NOTEBOOK = 10;
		public const int AMARRA_NOTEBOOK = 11;
		public const int AMARRA_TABLET = 12;
		public const int AMARRA_PAINEL_LCD_LCM = 25;

		public const int AMARRA_OCULOS = 17;
		public const int AMARRA_ESPACADOR = 16;

		public const int AMARRA_PLACA_MAE_TABLET = 13;
		public const int AMARRA_PLACA_AUX_TABLET = 14;
		public const int AMARRA_PLACA_PCI_PRINCIPAL = 18;
		public const int AMARRA_PCI_LED_BAR_LCM = 22;
		public const int AMARRA_LICENCA_NERO = 19;
		public const int AMARRA_EAN = 998;

		#endregion

		#region setores Lote

		public const int SETOR_IAC = 1;
		public const int SETOR_IMC = 2;
		public const int SETOR_FEC = 3;
		public const int SETOR_INF = 4;

		#endregion

		#region Sequência de leitura

		public const int SEQ_PRODUTO = -1;
		public const int SEQ_CAIXA = 0;
		public const int SEQ_PAINEL = 2;
		public const int SEQ_PLACA = 3;
		public const int SEQ_BASE = 3;
		public const int SEQ_PEDESTAL = 5;
		public const int SEQ_KIT_ACESSORIO = 6;
		public const int SEQ_SUPORTE = 7;
		public const int SEQ_FONTE = 8;
		public const int SEQ_Oculos = 4;
		public const int SEQ_Espacador = 4;

		#endregion

		#region Posto Tecnico

		public const string DEFEITO_PADRAO = "SF882";
		public const string SCRAP_DIRECIONAR_QUALIDADE = "SCRAP - DIRECIONAR PARA QUALIDADE";
		public const string DESASSOCIAR_PLACA_E_PAINEL = "DESASSOCIAR PLACA E PAINEL";
		public const string DESASSOCIAR_PLACA = "DESASSOCIAR PLACA";
		public const string DESASSOCIAR_PAINEL = "DESASSOCIAR PAINEL";

		public const int ACAO_ATUALIZACAO_DE_SOFTWARE = 1;
		public const int ACAO_DEFEITO_NAO_ENCONTRADO = 2;
		public const int ACAO_INSERIR_COMPONENTE = 3;
		public const int ACAO_REPOSICIONAR_COMPONENTE = 4;
		public const int ACAO_RESOLDAR_COMPONENTE  = 5;
		public const int ACAO_RETESTAR_PCI_NA_AOI = 6;
		public const int ACAO_REMOVER_EXCESSO_DE_SOLDA = 7;
		public const int ACAO_REMOVER_CURTO_DE_SOLDA = 8;
		public const int ACAO_RETESTAR_PCI_NO_TRI = 9;
		public const int ACAO_TROCAR_COMPONENTE = 10;
		public const int ACAO_REMOVER_CORPO_ESTRANHO = 11;

		public const int ACAO_SCRAP = 12;
		public const int ACAO_DESASSOCIAR_PLACA_E_PAINEL = 13;
		public const int ACAO_DESASSOCIAR_PLACA = 14;
		public const int ACAO_DESASSOCIAR_PAINEL = 15;

		public const int ACAO_REINICIALIZACAO_DE_PLACA_OU_APARELHO = 16;
		public const int ACAO_APROVADO_PELA_QUALIDADE = 17;
		public const int ACAO_PLACA_IMPORTADA = 18;

		public const int ACAO_DESASSOCIAR_ITEM = 19;

		public static List<string> ListaAcao
		{
			get
			{
				return new List<string>
				{
					"ATUALIZAÇÃO DE SOFTWARE",
					"DEFEITO NÃO ENCONTRADO",
					"INSERIR COMPONENTE",
					"REPOSICIONAR COMPONENTE",                                                           
					"RESOLDAR COMPONENTE",                    
					"RETESTAR PCI NA AOI",
					"REMOVER EXCESSO DE SOLDA",
					"REMOVER CURTO DE SOLDA",
					"RETESTAR PCI NO TRI",
					"TROCAR COMPONENTE",					
					"REMOVER CORPO ESTRANHO",
					"SCRAP_DIRECIONAR_QUALIDADE",
					"DESASSOCIAR_PLACA_E_PAINEL",
					"DESASSOCIAR_PLACA",
					"DESASSOCIAR_PAINEL",
					"REINICIALIZAÇÃO DE PLACA OU APARELHO"
				};
			}
		}

		#endregion

		#region Lote
		public const int LOTE_ABERTO = 0;
		public const int LOTE_FECHADO = 1;
		public const int LOTE_TRANSFERIDO = 2;
		public const int LOTE_RECEBIDO = 3;
		public const int LOTE_DEVOLVIDO = 4;
		public const int LOTE_RECEBIDO_OBA = 5;
		public const int LOTE_REPROVADO_OBA = 6;
		public const int LOTE_APROVADO_OBA = 7;
		public const int LOTE_ACEITE_OBA = 8;
		#endregion

		#region Propriedades e variáveis para a fábrica nova
		public static bool IsOldFab
		{
			get
			{
				try
				{
					var sessao = System.Web.HttpContext.Current.Session;
					if (sessao != null && sessao["Empresa"] != null)
					{
						return !sessao["Empresa"].ToString().Equals("dtb_SEMP");
					}
					else
					{
						return false;
					}
				}
				catch 
				{
					return false;
				}
				
			}
		}

		private static string _ConexaoString;
		public static string ConexaoString
		{
			get
			{
				try
				{
					var sessao = System.Web.HttpContext.Current.Session;

					return sessao["Conexao"].ToString();
				}
				catch
				{
					return _ConexaoString;
				}

			}
			set
			{
				try
				{
					System.Web.HttpContext.Current.Session["Conexao"] = value;
				}
				catch
				{
					_ConexaoString = value;
				}
				
			}
		}

		public static string CodFab02
		{
			get
			{
				if (IsOldFab)
				{
					return "02";
				}
				else
				{
					return "60";
				}
			}
		}

		public static string CodFab20
		{
			get
			{
				if (IsOldFab)
				{
					return "20";
				}
				else
				{
					return "61";
				}
			}
		}

		public static string CodFab21
		{
			get
			{
				if (IsOldFab)
				{
					return "21";
				}
				else
				{
					return "62";
				}
			}
		}
		#endregion
	}
}
