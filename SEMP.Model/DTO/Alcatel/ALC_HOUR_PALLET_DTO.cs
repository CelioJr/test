using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_HOUR_PALLET_DTO
    {
        public string idHour { get; set; }
        public Nullable<System.DateTime> dtInput { get; set; }
        public Nullable<int> quantity { get; set; }
        public Nullable<int> line_COD_LINHAS { get; set; }
        public Nullable<System.DateTime> idinput { get; set; }
    }
}
