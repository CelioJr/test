namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_IDIOMA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FR_IDIOMA()
        {
            FR_TRADUCAO_IDIOMA = new HashSet<FR_TRADUCAO_IDIOMA>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IDI_CODIGO { get; set; }

        [Required]
        [StringLength(60)]
        public string IDI_NOME { get; set; }

        [Required]
        [StringLength(20)]
        public string IDI_SIGLA { get; set; }

        public int IDI_ORDEM { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_TRADUCAO_IDIOMA> FR_TRADUCAO_IDIOMA { get; set; }
    }
}
