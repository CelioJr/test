namespace SEMP.Model.DTO
{
    public class ProdReferenciaDTO
    {
        public System.DateTime MesReferencia { get; set; }
        public System.DateTime DatReferencia { get; set; }
    }
}
