﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.ApontaProd;
using SEMP.Model.DTO;

namespace SEMP.Areas.Outros.Controllers
{
	[Authorize]
	public class MapaScrapController : Controller
	{
		//
		// GET: /Coleta/MapaScrap/
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult Lista(
			string datInicio,
			string datFim,
			string modelo,
			string item,
			string divergente,
			string pendencia)
		{
			Response.AddHeader("Content-Type", "application/vnd.ms-excel");


			var dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			var dFim = dIni.AddDays(1).AddMinutes(-1);

			if (!String.IsNullOrEmpty(datInicio) && !String.IsNullOrEmpty(datFim))
			{
				dIni = DateTime.Parse(datInicio);
				dFim = DateTime.Parse(datFim);
			}
			try
			{

				var Mapa = MapaScrapBO.ObterListagem(dIni, dFim);

				if (!String.IsNullOrEmpty(modelo)) Mapa = Mapa.Where(x => x.CodModelo == modelo).ToList();

				if (!String.IsNullOrEmpty(item)) Mapa = Mapa.Where(x => x.CodItem == item).ToList();

				if (!String.IsNullOrEmpty(pendencia)) Mapa = Mapa.Where(x => x.DatBaixa == null).ToList();

				if (!String.IsNullOrEmpty(divergente)) Mapa = Mapa.Where(x => x.QtdEstoque == null || x.QtdEstoque == 0).ToList();

				return View(Mapa);
			}
			catch (Exception ex)
			{
				if (ex.InnerException != null)
				{
					return View(new List<MAPScrapBaixaDTO>() {new MAPScrapBaixaDTO() {CodModelo = "Erro", DesModelo = ex.InnerException.Message}});
				}
				else
				{
					return View(new List<MAPScrapBaixaDTO>() { new MAPScrapBaixaDTO() { CodModelo = "Erro", DesModelo = ex.Message } });
				}
			}
		}

		public ActionResult Editar(int idScrap)
		{
			MAPScrapBaixaDTO mapa = MapaScrapBO.ObterMapa(idScrap);

			return View(mapa);
		}

		[HttpPost]
		public ActionResult Editar(MAPScrapBaixaDTO mapa)
		{
			try
			{
				MapaScrapBO.Alterar(mapa.IDScrap, mapa.CodItem, mapa.QtdPerda ?? 0);

				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}
		}

		public string Operacoes(int id, string datInicio, string datFim)
		{
			var dIni = DateTime.Parse(datInicio);
			var dFim = DateTime.Parse(datFim);

			var dHoje = DateTime.Today.ToShortDateString();

			if (dIni == DateTime.Parse(dHoje) || dFim == DateTime.Parse(dHoje))
				return "Data precisa ser diferente de hoje";

			var retornologin = (SEMP.Model.VO.RetornoLogin)Session["RetornoLogin"];
			string usuario = retornologin.NomUsuario;

			if (!usuario.ToUpper().Equals("ANDREN") && !usuario.ToUpper().Equals("THIAGORR") && !usuario.ToUpper().Equals("HELANAT") && !usuario.ToUpper().Equals("INGRIDS"))
				return "Sem acesso ao comando";
			try
			{
				switch (id)
				{
					case 1: // Gerar
						if (MapaScrapBO.VerificaData(dIni))
							return "Já existe dados gerados nessa data";

						MapaScrapBO.GerarMapa(dIni, dFim, usuario);
						break;
					case 2: // Baixar
						MapaScrapBO.BaixarScrap(dIni, dFim, usuario);
						break;

				}

			}
			catch (Exception ex)
			{
				return "Erro ao realizar a operação. " + ex.Message;
			}

			return "Operação realizada";
		}

	}
}
