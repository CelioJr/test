﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade
{
	public class OBAResumoInspecao
	{

		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public decimal QtdPrograma { get; set; }
		public int QtdProduzido { get; set; }
		public int QtdLotes { get; set; }
		public int QtdInspecionados { get; set; }
		public int QtdAprovados { get; set; }
		public int QtdReprovados { get; set; }
		public DateTime Data { get; set; }

		public decimal QtdP042 { get; set; }
		public decimal QtdP823 { get; set; }
		public decimal QtdP067 { get; set; }
		public decimal QtdP068 { get; set; }

	}
}
