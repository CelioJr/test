using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_CQTF_LIST_DTO
    {
        public int COD_CQTF { get; set; }
        public string LISTA { get; set; }
        public Nullable<int> TIPO { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
        public Nullable<bool> LIDO { get; set; }
    }
}
