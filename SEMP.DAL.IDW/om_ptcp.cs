namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_ptcp
    {
        [Key]
        public long id_ptcp { get; set; }

        public int? qt_ciclos { get; set; }

        public long? id_pt { get; set; }

        public long? id_cp { get; set; }

        public int? qt_ciclosregulagem { get; set; }

        public DateTime? dthr_entrada { get; set; }

        public DateTime? dthr_saida { get; set; }

        public virtual om_pt om_pt { get; set; }

        public virtual pp_cp pp_cp { get; set; }
    }
}
