namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_grnts
    {
        [Key]
        public long id_grnts { get; set; }

        public long id_usrgrp { get; set; }

        public long id_resgui { get; set; }

        public short? tp_acesso { get; set; }

        public virtual om_resgui om_resgui { get; set; }

        public virtual om_usrgrp om_usrgrp { get; set; }
    }
}
