using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBDestinatarioEmail
    {
        public string CodRotina { get; set; }
        public string EmailDestinatario { get; set; }
        public string NomDestinatario { get; set; }
    }
}
