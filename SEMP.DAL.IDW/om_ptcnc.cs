namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_ptcnc
    {
        [Key]
        public long id_ptcnc { get; set; }

        public long id_ptFilho { get; set; }

        public long? id_pt { get; set; }

        public virtual om_pt om_pt { get; set; }

        public virtual om_pt om_pt1 { get; set; }
    }
}
