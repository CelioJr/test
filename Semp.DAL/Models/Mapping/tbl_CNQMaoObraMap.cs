using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CNQMaoObraMap : EntityTypeConfiguration<tbl_CNQMaoObra>
    {
        public tbl_CNQMaoObraMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodMaoObra, t.DesMaoObraP, t.DesMaoObraE });

            // Properties
            this.Property(t => t.CodMaoObra)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.DesMaoObraP)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false);

            this.Property(t => t.DesMaoObraE)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_CNQMaoObra");
            this.Property(t => t.CodMaoObra).HasColumnName("CodMaoObra");
            this.Property(t => t.DesMaoObraP).HasColumnName("DesMaoObraP");
            this.Property(t => t.DesMaoObraE).HasColumnName("DesMaoObraE");
        }
    }
}
