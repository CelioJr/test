using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_PlanoEtapaMap : EntityTypeConfiguration<tbl_PlanoEtapa>
    {
        public tbl_PlanoEtapaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodModelo, t.NumRevisao, t.NumEtapa, t.CodProcesso, t.rowguid, t.FlgLeftRight });

            // Properties
            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumRevisao)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NumEtapa)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodProcesso)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.CodOrdAlim)
                .IsFixedLength()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.NumPriPosicao)
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.NumPadrao)
                .IsFixedLength()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.Comentario)
                .HasMaxLength(3000)
                .IsUnicode(false);

            this.Property(t => t.NomPrograma)
                .HasMaxLength(100)
                .IsUnicode(false);

            this.Property(t => t.FlgLeftRight)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DesSkip)
                .HasMaxLength(20)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_PlanoEtapa");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.NumRevisao).HasColumnName("NumRevisao");
            this.Property(t => t.NumEtapa).HasColumnName("NumEtapa");
            this.Property(t => t.CodProcesso).HasColumnName("CodProcesso");
            this.Property(t => t.CodOrdAlim).HasColumnName("CodOrdAlim");
            this.Property(t => t.NumPriPosicao).HasColumnName("NumPriPosicao");
            this.Property(t => t.NumPadrao).HasColumnName("NumPadrao");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
            this.Property(t => t.Comentario).HasColumnName("Comentario");
            this.Property(t => t.NomPrograma).HasColumnName("NomPrograma");
            this.Property(t => t.FlgLeftRight).HasColumnName("FlgLeftRight");
            this.Property(t => t.DesSkip).HasColumnName("DesSkip");
        }
    }
}
