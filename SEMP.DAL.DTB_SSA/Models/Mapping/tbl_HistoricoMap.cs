using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.DTB_SSA.Models.Mapping
{
    public class tbl_HistoricoMap : EntityTypeConfiguration<tbl_Historico>
    {
        public tbl_HistoricoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.codOF, t.desSerie });

            // Properties
            this.Property(t => t.codOF)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.desSerie)
                .IsRequired()
                .HasMaxLength(13);

            this.Property(t => t.LicencaWindows)
                .HasMaxLength(29);

            this.Property(t => t.NumPatrimonio)
                .HasMaxLength(15);

            this.Property(t => t.LicencaBetwin)
                .IsFixedLength()
                .HasMaxLength(21);

            this.Property(t => t.ExpressaoHCL)
                .HasMaxLength(10);

            this.Property(t => t.LicencaSoftware)
                .HasMaxLength(29);

            // Table & Column Mappings
            this.ToTable("tbl_Historico");
            this.Property(t => t.codOF).HasColumnName("codOF");
            this.Property(t => t.desSerie).HasColumnName("desSerie");
            this.Property(t => t.datSepAlmox).HasColumnName("datSepAlmox");
            this.Property(t => t.dat1).HasColumnName("dat1");
            this.Property(t => t.codOpe1).HasColumnName("codOpe1");
            this.Property(t => t.dat2).HasColumnName("dat2");
            this.Property(t => t.codOpe2).HasColumnName("codOpe2");
            this.Property(t => t.dat3).HasColumnName("dat3");
            this.Property(t => t.codOpe3).HasColumnName("codOpe3");
            this.Property(t => t.dat4).HasColumnName("dat4");
            this.Property(t => t.codOpe4).HasColumnName("codOpe4");
            this.Property(t => t.dat5).HasColumnName("dat5");
            this.Property(t => t.codOpe5).HasColumnName("codOpe5");
            this.Property(t => t.dat6).HasColumnName("dat6");
            this.Property(t => t.codOpe6).HasColumnName("codOpe6");
            this.Property(t => t.dat7).HasColumnName("dat7");
            this.Property(t => t.codOpe7).HasColumnName("codOpe7");
            this.Property(t => t.codAtual).HasColumnName("codAtual");
            this.Property(t => t.codOntem).HasColumnName("codOntem");
            this.Property(t => t.codPointControl).HasColumnName("codPointControl");
            this.Property(t => t.flgSemiProduto).HasColumnName("flgSemiProduto");
            this.Property(t => t.codRetorno).HasColumnName("codRetorno");
            this.Property(t => t.LicencaWindows).HasColumnName("LicencaWindows");
            this.Property(t => t.codLinha).HasColumnName("codLinha");
            this.Property(t => t.dat10).HasColumnName("dat10");
            this.Property(t => t.codOpe10).HasColumnName("codOpe10");
            this.Property(t => t.cifrado).HasColumnName("cifrado");
            this.Property(t => t.NumPatrimonio).HasColumnName("NumPatrimonio");
            this.Property(t => t.vinil).HasColumnName("vinil");
            this.Property(t => t.LicencaBetwin).HasColumnName("LicencaBetwin");
            this.Property(t => t.datDownload).HasColumnName("datDownload");
            this.Property(t => t.codOpeDownload).HasColumnName("codOpeDownload");
            this.Property(t => t.imagem_id).HasColumnName("imagem_id");
            this.Property(t => t.ExpressaoHCL).HasColumnName("ExpressaoHCL");
            this.Property(t => t.codRegistroMulticast).HasColumnName("codRegistroMulticast");
            this.Property(t => t.codLinhaEmbal).HasColumnName("codLinhaEmbal");
            this.Property(t => t.datEmbalPosto1).HasColumnName("datEmbalPosto1");
            this.Property(t => t.codOpeEmbalPosto1).HasColumnName("codOpeEmbalPosto1");
            this.Property(t => t.datImpEtiqMetalizada).HasColumnName("datImpEtiqMetalizada");
            this.Property(t => t.CodOpeImpEtiqMetalizada).HasColumnName("CodOpeImpEtiqMetalizada");
            this.Property(t => t.datAprovacaoCQ).HasColumnName("datAprovacaoCQ");
            this.Property(t => t.CodOpeAprovacaoCQ).HasColumnName("CodOpeAprovacaoCQ");
            this.Property(t => t.firstDat1).HasColumnName("firstDat1");
            this.Property(t => t.firstDat2).HasColumnName("firstDat2");
            this.Property(t => t.firstDat3).HasColumnName("firstDat3");
            this.Property(t => t.firstDat4).HasColumnName("firstDat4");
            this.Property(t => t.firstDat5).HasColumnName("firstDat5");
            this.Property(t => t.firstDat6).HasColumnName("firstDat6");
            this.Property(t => t.firstDat7).HasColumnName("firstDat7");
            this.Property(t => t.firstDat10).HasColumnName("firstDat10");
            this.Property(t => t.firstDatDownload).HasColumnName("firstDatDownload");
            this.Property(t => t.firstDatEmbalPosto1).HasColumnName("firstDatEmbalPosto1");
            this.Property(t => t.firstDatSepAlmox).HasColumnName("firstDatSepAlmox");
            this.Property(t => t.firstdatAprovacaoCQ).HasColumnName("firstdatAprovacaoCQ");
            this.Property(t => t.LicencaSoftware).HasColumnName("LicencaSoftware");
            this.Property(t => t.valPesoKit).HasColumnName("valPesoKit");
        }
    }
}
