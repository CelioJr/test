using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;

namespace SEMP.DAL.Models
{
    public partial class tbl_ProdParada
    {
        public String CodFab { get; set; }
        public String CodLinha { get; set; }
        public String NumParada { get; set; }
        public String CodModelo { get; set; }
        public System.DateTime DatParada { get; set; }
        public String CodParada { get; set; }
        public String CodItem { get; set; }
        public String HorInicio { get; set; }
        public String HorFim { get; set; }
        public String StatusPerda { get; set; }
        public Nullable<decimal> QtdItem { get; set; }
        public Nullable<decimal> QtdNaoProduzida { get; set; }
        public String CodTurno { get; set; }
        public String HorAlmoco { get; set; }
        public Nullable<int> NumFuncionarios { get; set; }
        public Nullable<decimal> QtdFalta { get; set; }
		[StringLength(5000)]
        public String DesObs { get; set; }
        public String StatusEmail { get; set; }
		public String NumDrt { get; set; }
        public System.Guid rowguid { get; set; }
    }
}
