namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_t_area
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_t_area()
        {
            dw_consoldef = new HashSet<dw_consoldef>();
            dw_passagem = new HashSet<dw_passagem>();
            dw_passdef = new HashSet<dw_passdef>();
            dw_procativ = new HashSet<dw_procativ>();
            dw_t_refugo = new HashSet<dw_t_refugo>();
        }

        [Key]
        public long id_area { get; set; }

        [StringLength(30)]
        public string cd_area { get; set; }

        public int? revisao { get; set; }

        [StringLength(100)]
        public string ds_area { get; set; }

        public short? st_ativo { get; set; }

        public DateTime? dthr_stativo { get; set; }

        public DateTime? dthr_revisao { get; set; }

        public long? id_usrrevisao { get; set; }

        public long? id_usrstativo { get; set; }

        [StringLength(255)]
        public string email { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consoldef> dw_consoldef { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_passagem> dw_passagem { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_passdef> dw_passdef { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_procativ> dw_procativ { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_t_refugo> dw_t_refugo { get; set; }
    }
}
