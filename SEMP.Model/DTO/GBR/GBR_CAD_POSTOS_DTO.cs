using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_CAD_POSTOS_DTO
    {
        public int COD_POSTO { get; set; }
        public string NOME { get; set; }
        public string CAMINHO_SAV { get; set; }
        public string CAMINHO_BACKUP { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
        public Nullable<bool> DESATIVADO { get; set; }
        public Nullable<bool> METODO2 { get; set; }
        public Nullable<int> COD_LINHA { get; set; }
    }
}
