using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DOC_PRINCIPAL_DTO
    {
        public FR_DOC_PRINCIPAL_DTO()
        {
            this.FR_DOC_APROVACAO = new List<FR_DOC_APROVACAO_DTO>();
            this.FR_DOC_ATOR = new List<FR_DOC_ATOR_DTO>();
            this.FR_DOC_BENEFICIO = new List<FR_DOC_BENEFICIO_DTO>();
            this.FR_DOC_CASO_USO = new List<FR_DOC_CASO_USO_DTO>();
            this.FR_DOC_DIAGRAMA_CONTEXTO = new List<FR_DOC_DIAGRAMA_CONTEXTO_DTO>();
            this.FR_DOC_FORMULARIO = new List<FR_DOC_FORMULARIO_DTO>();
            this.FR_DOC_FORMULARIO_IMAGEM = new List<FR_DOC_FORMULARIO_IMAGEM_DTO>();
            this.FR_DOC_INTERFACE = new List<FR_DOC_INTERFACE_DTO>();
            this.FR_DOC_INTERPRETACAO_CAMPO = new List<FR_DOC_INTERPRETACAO_CAMPO_DTO>();
            this.FR_DOC_LIMITE = new List<FR_DOC_LIMITE_DTO>();
            this.FR_DOC_MATERIAL_REFERENCIA = new List<FR_DOC_MATERIAL_REFERENCIA_DTO>();
            this.FR_DOC_MODO_OPERACAO = new List<FR_DOC_MODO_OPERACAO_DTO>();
            this.FR_DOC_REGRAS_IMAGEM = new List<FR_DOC_REGRAS_IMAGEM_DTO>();
            this.FR_DOC_RELATORIO = new List<FR_DOC_RELATORIO_DTO>();
            this.FR_DOC_REQUISITO = new List<FR_DOC_REQUISITO_DTO>();
            this.FR_DOC_RESTRICAO = new List<FR_DOC_RESTRICAO_DTO>();
            this.FR_DOC_VERSAO = new List<FR_DOC_VERSAO_DTO>();
        }

        public int DOC_CODIGO { get; set; }
        public string DOC_AUTORIA { get; set; }
        public string SIS_CODIGO { get; set; }
        public string DOC_DATA { get; set; }
        public string DOC_EMPRESA { get; set; }
        public string DOC_LOCAL { get; set; }
        public byte[] DOC_MODELO_DADOS { get; set; }
        public string DOC_OBJETIVO { get; set; }
        public string DOC_MISSAO { get; set; }
        public string DOC_COMPONENTES { get; set; }
        public string DOC_VISAO_GERAL { get; set; }
        public string DOC_CONVENCOES { get; set; }
        public string DOC_OBSERVACOES { get; set; }
        public string DOC_USUARIO_DESCRICAO { get; set; }
        public virtual ICollection<FR_DOC_APROVACAO_DTO> FR_DOC_APROVACAO { get; set; }
        public virtual ICollection<FR_DOC_ATOR_DTO> FR_DOC_ATOR { get; set; }
        public virtual ICollection<FR_DOC_BENEFICIO_DTO> FR_DOC_BENEFICIO { get; set; }
        public virtual ICollection<FR_DOC_CASO_USO_DTO> FR_DOC_CASO_USO { get; set; }
        public virtual ICollection<FR_DOC_DIAGRAMA_CONTEXTO_DTO> FR_DOC_DIAGRAMA_CONTEXTO { get; set; }
        public virtual ICollection<FR_DOC_FORMULARIO_DTO> FR_DOC_FORMULARIO { get; set; }
        public virtual ICollection<FR_DOC_FORMULARIO_IMAGEM_DTO> FR_DOC_FORMULARIO_IMAGEM { get; set; }
        public virtual ICollection<FR_DOC_INTERFACE_DTO> FR_DOC_INTERFACE { get; set; }
        public virtual ICollection<FR_DOC_INTERPRETACAO_CAMPO_DTO> FR_DOC_INTERPRETACAO_CAMPO { get; set; }
        public virtual ICollection<FR_DOC_LIMITE_DTO> FR_DOC_LIMITE { get; set; }
        public virtual ICollection<FR_DOC_MATERIAL_REFERENCIA_DTO> FR_DOC_MATERIAL_REFERENCIA { get; set; }
        public virtual ICollection<FR_DOC_MODO_OPERACAO_DTO> FR_DOC_MODO_OPERACAO { get; set; }
        public virtual FR_SISTEMA_DTO FR_SISTEMA { get; set; }
        public virtual ICollection<FR_DOC_REGRAS_IMAGEM_DTO> FR_DOC_REGRAS_IMAGEM { get; set; }
        public virtual ICollection<FR_DOC_RELATORIO_DTO> FR_DOC_RELATORIO { get; set; }
        public virtual ICollection<FR_DOC_REQUISITO_DTO> FR_DOC_REQUISITO { get; set; }
        public virtual ICollection<FR_DOC_RESTRICAO_DTO> FR_DOC_RESTRICAO { get; set; }
        public virtual ICollection<FR_DOC_VERSAO_DTO> FR_DOC_VERSAO { get; set; }
    }
}
