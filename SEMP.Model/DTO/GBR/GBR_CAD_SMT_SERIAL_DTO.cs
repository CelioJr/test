using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_CAD_SMT_SERIAL_DTO
    {
        public int COD_SMT { get; set; }
        public string SERIAL { get; set; }
        public Nullable<int> COD_SKU { get; set; }
        public string USUARIO { get; set; }
        public Nullable<int> COD_MODELO { get; set; }
        public Nullable<bool> VERIFICADO { get; set; }
        public Nullable<bool> DEBUG_SAV { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
        public string INSPECAO_USER { get; set; }
        public Nullable<System.DateTime> INSPECAO_DATA { get; set; }
        public Nullable<bool> INSPECAO_VERIFICADO { get; set; }
        public Nullable<int> INSPECAO_SESSAO { get; set; }
        public Nullable<bool> IMEI_RECEBIDO { get; set; }
        public Nullable<bool> CAXA_COLETIVA { get; set; }
        public Nullable<bool> CAIXA_INDIVIDUAL { get; set; }
        public Nullable<bool> PALLETIZADO { get; set; }
        public string IMEI_1 { get; set; }
        public string IMEI_2 { get; set; }
        public string IMEI_3 { get; set; }
        public string IMEI_4 { get; set; }
        public Nullable<int> COD_LINHA { get; set; }
        public string PACKSTRING { get; set; }
        public string PALLETSTRING { get; set; }
        public Nullable<int> COD_LINHA_IMEI { get; set; }
        public Nullable<int> COD_LINHA_INDIVIDUAL { get; set; }
        public Nullable<int> COD_LINHA_COLETIVA { get; set; }
        public Nullable<int> COD_LINHA_PALLET { get; set; }
        public Nullable<System.DateTime> DATA_LINHA_IMEI { get; set; }
        public Nullable<System.DateTime> DATA_LINHA_INDIVIDUAL { get; set; }
        public Nullable<System.DateTime> DATA_LINHA_COLETIVA { get; set; }
        public Nullable<System.DateTime> DATA_LINHA_PALLET { get; set; }
        public Nullable<bool> SERIAL_ENGINES { get; set; }
        public Nullable<System.DateTime> ENGINES_EXPORTADO_DATA { get; set; }
        public Nullable<decimal> PESO { get; set; }
        public string OP { get; set; }
        public string COD_VOLUME { get; set; }
        public string ORDEM { get; set; }
        public Nullable<int> COD_LINHA_FT { get; set; }
        public Nullable<System.DateTime> DATA_LINHA_FT { get; set; }
        public string COMPUTER_NAME { get; set; }
        public string SKU_SAV { get; set; }
        public string ORDEM_IMEI { get; set; }
        public string ORDEM_CX { get; set; }
        public string ORDEM_COLETIVA { get; set; }
    }
}
