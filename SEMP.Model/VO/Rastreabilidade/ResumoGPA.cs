﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade
{
	public class ResumoGPA
	{

		public string CodPallet { get; set; }
		public string CodLinha { get; set; }
		public string CodModelo { get; set; }
		public string DesLinha { get; set; }
		public string DesModelo { get; set; }
		public DateTime? Data { get; set; }
		public int QtdPallet { get; set; }
		public string NumSerieIni { get; set; }
		public string NumSerieFim { get; set; }

	}
}
