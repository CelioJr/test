using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_ProgMesResumoMap : EntityTypeConfiguration<tbl_ProgMesResumo>
    {
        public tbl_ProgMesResumoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.DatProducao, t.CodModelo, t.FlagTP });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.Usuario)
                .IsFixedLength()
                .HasMaxLength(25)
                .IsUnicode(false);

            this.Property(t => t.flgCobertura)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.Observacao)
                .HasMaxLength(300)
                .IsUnicode(false);

            this.Property(t => t.FlagTP)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_ProgMesResumo");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.DatProducao).HasColumnName("DatProducao");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.QtdProdPm).HasColumnName("QtdProdPm");
            this.Property(t => t.Usuario).HasColumnName("Usuario");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
            this.Property(t => t.flgCobertura).HasColumnName("flgCobertura");
            this.Property(t => t.NumPrioridade).HasColumnName("NumPrioridade");
            this.Property(t => t.Observacao).HasColumnName("Observacao");
            this.Property(t => t.FlagTP).HasColumnName("FlagTP");
        }
    }
}
