﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
	public class ResumoDefeito
	{
		public string CodDefeito { get; set; }
		public string DesDefeito { get; set; }
		public int QtdDefeito { get; set; }
		public int QtdAcumulada { get; set; }
		public int QtdFalsaFalha { get; set; }
		public string Observacao { get; set; }
		public System.Drawing.Color Cor { get; set; }
	}
}
