using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CapacMdoLinha
    {
        public string CodLinha { get; set; }
        public Nullable<int> Quantidade { get; set; }
        public Nullable<System.DateTime> DatInicio { get; set; }
        public Nullable<System.DateTime> DatFim { get; set; }
        public int NumTurno { get; set; }
    }
}
