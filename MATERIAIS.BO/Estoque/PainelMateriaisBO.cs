﻿using SEMP.DAL.DAO.Materiais;
using SEMP.Model.VO.Materiais;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MATERIAIS.BO.Estoque
{
	public class PainelMateriaisBO
	{


		public static List<PainelMateriais> Consultar(string DataInicial, string DataFinal)
		{
			return daoPainelMateriais.Consultar(DataInicial, DataFinal);
		}

		public static List<PainelMateriaisDetalhes> Detalhes(string codSeparacao, string AP, string Fab)
		{

			return daoPainelMateriais.Detalhes(codSeparacao, AP, Fab);

		}

	}
}
