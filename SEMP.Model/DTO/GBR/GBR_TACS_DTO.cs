using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_TACS_DTO
    {
        public int COD_TAC { get; set; }
        public Nullable<int> COD_MODELO { get; set; }
        public string TAC { get; set; }
        public Nullable<int> IMEI_BEGIN { get; set; }
        public Nullable<int> IMEI_END { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
    }
}
