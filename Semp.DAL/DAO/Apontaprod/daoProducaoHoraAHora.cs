﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using SEMP.DAL.Models;
using SEMP.Model.VO.Rastreabilidade;
using SEMP.Model.VO.Rastreabilidade.Relatorios;

namespace SEMP.DAL.DAO.Rastreabilidade
{
	public class daoProducaoHoraAHora
	{
		public static List<ProducaoHoraAHora> ObterProducaoHoraAHora(DateTime Data)
		{
			try
			{
				using (DTB_SIMContext dtbSim = new DTB_SIMContext())
				{
					var producaohora = dtbSim.Database.SqlQuery<ProducaoHoraAHora>(
						"EXEC spc_BarrasProducaoHora @Data, @flgMes, @CodLinha",
						new SqlParameter("Data", Data),
						new SqlParameter("flgMes", "S"),
						new SqlParameter("CodLinha", "")
					).ToList();

					return producaohora;
				}
			}
			catch
			{
				return null;
			}

		}

		public static List<ProducaoHoraAHora> ObterProducaoHoraAHoraIMC(DateTime Data)
		{
			try
			{
				using (DTB_SIMContext dtbSim = new DTB_SIMContext())
				{
					var producaohora = dtbSim.Database.SqlQuery<ProducaoHoraAHora>(
						"EXEC spc_BarrasProducaoPlacaHora @Data, @flgMes, @CodLinha",
						new SqlParameter("Data", Data),
						new SqlParameter("flgMes", "S"),
						new SqlParameter("CodLinha", "")
						).ToList();
					
					
					using (DTB_CBContext dtbCb = new DTB_CBContext())
					{
						var producaohora2 = dtbCb.Database.SqlQuery<ProducaoHoraAHora>(
							"EXEC spc_BarrasProducaoPlacaHora @Data, @flgMes, @CodLinha",
							new SqlParameter("Data", Data),
							new SqlParameter("flgMes", "S"),
							new SqlParameter("CodLinha", "")
							).ToList();

						producaohora.AddRange(producaohora2.Where(y => !producaohora.Select(x => x.CodLinha).Contains(y.CodLinha) ));
					}

					return producaohora;
				}
			}
			catch
			{
				return null;
			}

		}

		public static List<CBTempoMedioLinha> ObterCBTempoMedioLinha(DateTime datIni, DateTime datFim, int codLinha, string horaInicio, string horaFim)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				return dtbCB.spc_CBTempoMedioLinha(datIni, datFim, codLinha, horaInicio, horaFim).ToList();
			}
		}

		public static List<CBTempoMedioLinha> ObterCBTempoMedioModelo(DateTime datIni, DateTime datFim, string codModelo, int codLinha, string horaInicio, string horaFim)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				return dtbCB.spc_CBTempoMedioModelo(datIni, datFim, codModelo, codLinha, horaInicio, horaFim).ToList();
			}
		}

		public static void GravarTempo(int codLinha, int numPosto, string codModelo, decimal tempoMedio)
		{

			using (var dtbCB = new DTB_CBContext())
			{

				var dado =
					dtbCB.tbl_CBLinhaPostoPerfil.FirstOrDefault(
						x => x.CodLinha == codLinha && x.NumPosto == numPosto && x.CodModelo == codModelo);
				if (dado != null)
				{
					dado.TempoCiclo = tempoMedio;

					dtbCB.SaveChanges();
				}

			}

		}

	}
}
