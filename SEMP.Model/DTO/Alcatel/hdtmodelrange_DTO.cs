using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class hdtmodelrange_DTO
    {
        public long id { get; set; }
        public Nullable<System.DateTime> dateCreated { get; set; }
        public Nullable<System.DateTime> dateEnded { get; set; }
        public int imeiEnd { get; set; }
        public int imeiNext { get; set; }
        public int imeiStart { get; set; }
        public int imeiTac { get; set; }
        public int macEnd { get; set; }
        public int macNext { get; set; }
        public int macStart { get; set; }
        public int macTac { get; set; }
        public string status { get; set; }
        public string userNameCreated { get; set; }
        public string userNameEnded { get; set; }
        public Nullable<long> hdtModel_id { get; set; }
        public virtual hdtmodel_DTO hdtmodel { get; set; }
    }
}
