﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.VO.CNQ
{
	public class PrincipaisDefeitos
	{
		[Display(Name = "Modelo")]
		public string CodModelo { get; set; }
		[Display(Name = "Descrição")]
		public string DesModelo { get; set; }
		[Display(Name = "Posição")]
		public string Posicao { get; set; }
		[Display(Name = "Causa")]
		public string CodCausa { get; set; }
		[Display(Name = "Defeito")]
		public string DesDefeito { get; set; }
		[Display(Name = "Desc. Causa")]
		public string DesCausa { get; set; }
		[Display(Name = "Qtde")]
		public int Qtde { get; set; }
		[Display(Name = "Origem")]
		public string DesOrigem { get; set; }
		[Display(Name = "Produção")]
		public int? QtdProducao { get; set; }

		[Display(Name = "%")]
		[DisplayFormat(DataFormatString = "{0:0.00}")]
		public float Indice
		{
			get
			{
				if (QtdProducao.HasValue && QtdProducao > 0)
				{
					return (Qtde / (float)QtdProducao) * 100;
				}
				
				return 0;
			}
		}
	}
}
