﻿using log4net;
using System;
using System.Linq;

namespace SEMP.WindowsService
{
	public class OrdemProducaoService
	{

		//private static ILog _logger = LogManager.GetLogger(typeof(OrdemProducaoService));
		private static ILog _logger = LogManager.GetLogger("OrdemProducao");

		public Resposta RunJob()
		{
			Resposta retorno = new Resposta { Codigo = 0, Msg = "" };

			FazerImportacao();

			return new Resposta();
		}

		private void FazerImportacao()
		{
			try
			{
				SetStatus("Iniciando importação Ordem de Produção");

				var datIni = DateTime.Now.AddDays(-DateTime.Now.Day).AddDays(1).ToShortDateString();
				var datFim = DateTime.Now.AddDays(-DateTime.Now.Day).AddDays(1).AddMonths(1).AddDays(-1).ToShortDateString();

				var ordens = MATERIAIS.BO.Estoque.AP.ObterOP(datIni, datFim);

				if (ordens != null)
				{
					ordens = ordens.Where(x => x.AP == null).ToList();

					foreach (var item in ordens)
					{
						SetStatus($"Importando Ordem: {item.AUFNR}");

						var dados = MATERIAIS.BO.Estoque.AP.SAP_ImportarOrdem(item.AUFNR);

						if (dados)
						{
							SetStatus("Importação de Ordem de Produção realizada com sucesso");
							//Email.EnviaEmail("Teste de Importação de Ordem", $"Importação da ordem {item.AUFNR} realizado com sucesso");
						}
						else
						{
							SetStatus("Erro na importação de Ordem de Produção");
						}
					}

				}
				else
				{
					SetStatus("Nenhuma ordem a importar");
				}
			}
			catch (Exception ex)
			{
				_logger.Error("Erro ao executar a importação das Ordens de Produção.", ex);
			}
		}

		public void SetStatus(string msg)
		{
			_logger.Info(msg);
		}


	}
}
