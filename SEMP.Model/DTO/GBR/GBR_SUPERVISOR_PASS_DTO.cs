using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_SUPERVISOR_PASS_DTO
    {
        public int COD_PASS { get; set; }
        public string USUARIO { get; set; }
        public string SENHA { get; set; }
        public Nullable<bool> PERMITE_REIMPRESSAO { get; set; }
    }
}
