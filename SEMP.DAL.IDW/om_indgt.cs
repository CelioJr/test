namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_indgt
    {
        [Key]
        public long id_indgt { get; set; }

        public decimal? num_inf { get; set; }

        public decimal? num_superior { get; set; }

        public decimal? num_meta { get; set; }

        public long id_ind { get; set; }

        public long id_gt { get; set; }

        public virtual om_gt om_gt { get; set; }

        public virtual om_ind om_ind { get; set; }
    }
}
