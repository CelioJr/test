using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_ProdReferencia
    {
        public System.DateTime MesReferencia { get; set; }
        public System.DateTime DatReferencia { get; set; }
    }
}
