﻿using System.Data.Entity;

namespace SEMP.DAL.Models
{
    public class DtbEngContext : DbContext
    {
        static DtbEngContext()
        {
            Database.SetInitializer<DtbEngContext>(null);
        }

		#if DEBUG
        public DtbEngContext()
            : base("Name=DTB_ENGContextDEBUG")
        {
        }
		#else
		public DtbEngContext()
            : base("Name=DTB_ENGContext")
        {
        }
		#endif

        public DbSet<pc> Pc { get; set; }
        public DbSet<pcRede> PcRede { get; set; }
        public DbSet<rede> Rede { get; set; }
    }
}
