namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_tpplano
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pp_tpplano()
        {
            pp_plano = new HashSet<pp_plano>();
        }

        [Key]
        public long id_tpplano { get; set; }

        [StringLength(100)]
        public string ds_tpplano { get; set; }

        public short? tp_algoritmo { get; set; }

        public decimal? ind_oee { get; set; }

        public short? is_considerarRAP { get; set; }

        public short? is_considerarMP { get; set; }

        public short? is_considerarEst { get; set; }

        public short? is_considerarCal { get; set; }

        public short? is_considerarIndisp { get; set; }

        public short? is_considerarMO { get; set; }

        public short? is_determinadoCal { get; set; }

        public short? is_considerarOEEFinalSerie { get; set; }

        public short? is_considerarProdutoTurno { get; set; }

        public short? is_considerarCM { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_plano> pp_plano { get; set; }
    }
}
