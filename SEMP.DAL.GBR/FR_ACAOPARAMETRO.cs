namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_ACAOPARAMETRO
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ACO_CODIGO { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(30)]
        public string ACP_NOME { get; set; }

        public int ACT_CODIGO { get; set; }

        public virtual FR_ACAO FR_ACAO { get; set; }

        public virtual FR_ACPTIPO FR_ACPTIPO { get; set; }
    }
}
