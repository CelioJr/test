using System;
using System.Collections.Generic;

namespace SEMP.DAL.DTB_SSA.Models
{
    public partial class viw_STI_ModeloEtiqueta
    {
        public string CodItem { get; set; }
        public string CodEstrutura { get; set; }
        public string DesModelo { get; set; }
        public string DesResumida { get; set; }
        public string DesDetalhe { get; set; }
        public string CodEAN { get; set; }
        public string CodEANCxColetiva { get; set; }
        public Nullable<decimal> QtdEmbUni { get; set; }
    }
}
