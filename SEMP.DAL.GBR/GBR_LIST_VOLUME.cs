namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_LIST_VOLUME
    {
        [Key]
        public int COD_VOLUME { get; set; }

        [StringLength(15)]
        public string VOLUME_STRING { get; set; }

        [StringLength(15)]
        public string PACK_STRING { get; set; }

        public int? COD_USUARIO { get; set; }

        public int? COD_SKU { get; set; }

        [StringLength(15)]
        public string IP { get; set; }

        public bool? FECHADO { get; set; }

        public DateTime? DATA { get; set; }
    }
}
