﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using RAST.BO.Embalagem;
using RAST.BO.Rastreabilidade;
using SEMP.BO;
using SEMP.Model;
using SEMP.Model.DTO;
using SEMP.Model.VO;
using SEMP.Model.VO.Embalagem;
using SEMP.Model.VO.Rastreabilidade;
using RAST.BO.Lote;

namespace SEMP.Areas.Coleta.Controllers
{
	public class EmbalagemController : Controller
	{
		/// <summary>
		/// Método chamado pela View PostoAmarracaoLote.cshtml para fazer a embalagem do produto
		/// </summary>
		/// <returns></returns>
		[HttpPost]
		public JsonResult InserirEmbalagem ()
		{
			Stopwatch stopWatch = new Stopwatch();
			stopWatch.Start();

			var fabrica = Request.Form["Fabrica"];
			var codDrt = Request.Form["CodDRT"];
			var numSerie = Request.Form["NumSerie"];
			var numeroAp = Request.Form["NumeroAP"];
			var statusReimpressao = Convert.ToBoolean(Request.Form["statusReimpressao"]);

			CBEmbaladaDTO produtoEmbalar;
			var listaAmarra = new List<CBAmarraDTO>();
			var retorno = new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "LIME", DesMensagem = "LEITURA OK. LEIA PRÓXIMO." };
			double pesoCaixa = 0;
			var error = false;
			var sessao = Session;
	   
			try
			{
				if (sessao == null)
				{
					error = true;
					retorno = Defeito.ObterMensagem( Constantes.CLIENT_ERROR);
				}

				if (!error)
				{
					CBPostoDTO posto = (CBPostoDTO)Session["Posto"];
					CBLinhaDTO linhaAtual = ((CBLinhaDTO)Session["Linha"]);
					Passagem passagem = (Passagem)Session["passagem"];
					List<viw_CBPerfilAmarraDTO> perfils = (List<viw_CBPerfilAmarraDTO>)Session["GetPerfilPosto"];
					int turno = (int)Session["Turno"];
					IEmbalagem embalagem = Embalagem.Recuperar_Posto_Embalagem((CBPostoDTO)Session["Posto"]);

					var perfilMasc = (from p in perfils
									  where new Regex(p.Mascara).IsMatch(numSerie) && p.numAP == numeroAp
									  select p).FirstOrDefault();

					foreach (var perfil in perfils.Where(a => a.DesTipoAmarra != "Produto" && a.DesTipoAmarra != "EAN13" && a.numAP == numeroAp).OrderBy(o => o.CodTipoAmarra).ToList())
					{
						var valor = Request.Form[perfil.DesTipoAmarra].Split(',');

						for (int x = 0; x < valor.Length; x++)
						{
							if (!listaAmarra.Exists(a => a.NumECB.Equals(valor[x])))
							{
								CBAmarraDTO amarra = new CBAmarraDTO()
								{
									NumSerie = numSerie,
									NumECB = valor[x],
									CodModelo = perfil.CodModelo,
									FlgTipo = perfil.CodTipoAmarra,
									DatAmarra = DateTime.Now,
									CodLinha = passagem.posto.CodLinha,
									NumPosto = passagem.posto.NumPosto,
									CodFab = fabrica,
									NumAP = perfil.numAP,
									CodItem = perfil.CodItem
								};

								listaAmarra.Add(amarra);
							}
						}
					}
					
					produtoEmbalar= (new CBEmbaladaDTO
					{
						CodFab = fabrica,
						CodLinha = passagem.posto.CodLinha,
						CodModelo = perfilMasc.CodModelo,
						CodOperador = codDrt,
						DatLeitura = DateTime.Now,
						DatReferencia = DateTime.Now.ToShortDateString(),
						NumAP = perfilMasc.numAP,
						NumECB = numSerie,
						NumPosto = passagem.posto.NumPosto,
						Linha = linhaAtual,
						Turno = turno
					});

					#region Embalando produtos

						switch (passagem.posto.flgImprimeEtq)
						{
							case "N":                    //se não trabalha com impressão, então embalada
								retorno = embalagem.Embalar_Produto(produtoEmbalar, listaAmarra, passagem, posto);
								break;

							case "S":
								if (!statusReimpressao)  //se trabalha com impressão e a reimpressão ta desativada, então embala
									retorno = embalagem.Embalar_Produto(produtoEmbalar, listaAmarra, passagem, posto);
								break;
						}

						if (retorno.CodMensagem != Constantes.RAST_OK)
							error = true;
						
					#endregion

					#region Trabalhando com Impressão
						if (!error)
						{
							if (passagem.posto.flgImprimeEtq == "S")
							{
								if (linhaAtual.Setor != "IAC")
								{
									var ret2 = embalagem.ImprimirEtiquetasIndividuais(perfilMasc.numAP, numSerie, perfilMasc.CodModelo, posto, pesoCaixa);
									if (ret2.CodMensagem != Constantes.RAST_OK)
									{
										retorno = ret2;
										error = true;
									}

								}
								else
								{
									// gravar na tabela tbl_CBImpressaoEtiqueta

								}
							}
						}
					#endregion
				}
				
			}
			catch (Exception ex)
			{
				error = true;
				retorno.CodMensagem = Constantes.DB_ERRO;
				retorno.DesMensagem = ex.Message;
				retorno.DesCor = "red";
			}

			stopWatch.Stop();
			TimeSpan ts = stopWatch.Elapsed;

			string tempoExecucaoMetodo = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
				ts.Hours, ts.Minutes, ts.Seconds,
				ts.Milliseconds / 10);


			var jsonData = new
			{
				error,
				message = retorno.DesMensagem,
				retorno,
				tempo = tempoExecucaoMetodo,
				tempoBanco = retorno.MensagemAuxiliar
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Método para fazer a validação de cada item que é lido no posto de embalagem
		/// </summary>
		/// <param name="fabrica"></param>
		/// <param name="leitura"></param>
		/// <param name="codDrt"></param>
		/// <param name="idxPerfil"></param>
		/// <returns></returns>
		public JsonResult ValidarLeituraEmbalagem (string fabrica, string leitura, string codDrt, int idxPerfil, bool ?reimpressaoAtivada)
		{
			var stopWatch = new Stopwatch();
			stopWatch.Start();
			var bar = leitura.ToUpper();
			var error = false;
			var indice = idxPerfil;
			var codTipoAmarra = 0;
			var codItem = string.Empty;
			var retorno = new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "Lime", DesMensagem = "LEITURA OK. LEIA PRÓXIMO." };
			var sessao = Session;
			var numeroAp = "";

			if (sessao != null)
			{
				var passagem = (Passagem)Session["passagem"];
				var perfils = (List<viw_CBPerfilAmarraDTO>)Session["GetPerfilPosto"];
				var linhaAtual = ((CBLinhaDTO)Session["Linha"]);                

				var perfilMasc = perfils[idxPerfil];

				numeroAp = perfilMasc.numAP;
				IEmbalagem produto = Embalagem.Recuperar_Posto_Embalagem((CBPostoDTO)Session["Posto"]);

				if (perfilMasc.CodTipoAmarra.Equals(Constantes.AMARRA_PRODUTO) ||
					perfilMasc.CodTipoAmarra.Equals(Constantes.AMARRA_TABLET) ||
					perfilMasc.CodTipoAmarra.Equals(Constantes.AMARRA_NOTEBOOK))
				{
					var codigoCinescopio = (string) Session["CodigoCinescopio"];
					retorno = produto.ValidarProduto(passagem, codDrt, perfilMasc.numAP, bar, perfilMasc.CodModelo, fabrica, codigoCinescopio, linhaAtual);
					switch (retorno.CodMensagem)
					{
						case Constantes.RAST_OK:
							if (reimpressaoAtivada != null && reimpressaoAtivada.Value)
							{
								error = true;
								retorno = Defeito.ObterMensagem(Constantes.RAST_REGISTRO_NAO_ENCONTRADO);
								retorno.DesMensagem = "Produto ainda não lido. Verifique se a reimpressão está desativada";
							}
							break;
						case Constantes.RAST_LEITURA_REPETIDA:
							if (reimpressaoAtivada != null && reimpressaoAtivada.Value)
							{
								retorno = new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "Lime", DesMensagem = "LEITURA OK. LEIA PRÓXIMO." };                                
							}
							else
							{
								error = true;
								if (Embalagem.BloquearPostoSeLeituraRepetida(passagem, bar))
								{ 
									retorno = Defeito.ObterMensagem(Constantes.RAST_LEITURA_REPETIDA);
									retorno.DesMensagem = "Posto Bloqueado por Leitura Duplicada";
									retorno.MensagemAuxiliar = "LeituraDuplicada";
								}
							}
							break;
						default: error = true;
							break;
					}
				  
				}
				else
				{
					var numKitProvisorio = string.Empty;

					if (linhaAtual.Familia.Equals("INF"))
					{
						switch (perfilMasc.CodTipoAmarra)
						{
							//armazena o número do kit para validar a etiqueta nero. usado somente para notebooks
							case Constantes.AMARRA_KIT:
								Session["numeroKit"] = leitura;
								break;
							case Constantes.AMARRA_LICENCA_NERO:
								numKitProvisorio = (String) Session["numeroKit"];
								Session["numeroKit"] = string.Empty;
								break;
						}
					}

					if (perfilMasc.CodTipoAmarra != 999) //caixa EAN - LCD
					{
						retorno = produto.ValidarLeitura(perfilMasc.CodTipoAmarra, leitura, perfilMasc.numAP, bar,
							numKitProvisorio, linhaAtual);
					}
					if (retorno.CodMensagem != Constantes.RAST_OK)
					{
						error = true;
					}
				}

				codTipoAmarra = perfilMasc.CodTipoAmarra;
				codItem = perfilMasc.CodItem;
			}
			else
			{
				error = true;
				retorno = Defeito.ObterMensagem(Constantes.CLIENT_ERROR);
			}

			stopWatch.Stop();
			var ts = stopWatch.Elapsed;

			var tempoExecucaoMetodo = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
				ts.Hours, ts.Minutes, ts.Seconds,
				ts.Milliseconds / 10);

			var jsonData = new
			{
				error,
				message = retorno.DesMensagem,
				serial = bar,
				indice,
				tipo = codTipoAmarra,
				codItem,
				retorno,
				numAP = numeroAp,
				tempo = tempoExecucaoMetodo
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);

		}

		/// <summary>
		/// Método que faz a Re-impressão da etiqueta da caixa coletiva
		/// </summary>
		/// <returns></returns>        
		public JsonResult ImprimirEtiquetaCaixaColetiva (string numeroCc)
		{
			var erro = false;
			var retorno = new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "LIME", DesMensagem = "Etiqueta reimpressa com sucesso." };
			var posto = (CBPostoDTO)Session["Posto"];
			var linha = (CBLinhaDTO)Session["Linha"];

			if (!numeroCc.Substring(0, 1).Equals("C"))
			{
				//não é número de caixa coletiva, então recupera o número da caixa a qual o item ta associado
				numeroCc = Embalagem.RecuperarCCProdutoEmbalado(numeroCc);
				if (string.IsNullOrEmpty(numeroCc))
				{ 
					erro = true;
					retorno = Defeito.ObterMensagem(Constantes.RAST_IMPRESSAO_NAO_REALIZADA);
					retorno.DesMensagem = "Número de série não reconhecido";
				}
			}

			if (!erro)
			{
				if (posto.flgImprimeEtq == "S")
				{
					var produto = Embalagem.Recuperar_Posto_Embalagem((CBPostoDTO)Session["Posto"]);                    

					retorno = produto.ImprimirCaixaColetiva(numeroCc, linha, posto);

					if (retorno.CodMensagem != Constantes.RAST_OK)
					{
						erro = true;
					}
				}
				else
				{
					erro = true;
					retorno = Defeito.ObterMensagem(Constantes.RAST_IMPRESSAO_NAO_REALIZADA);
					retorno.DesMensagem = "Posto não configurado para imprimir etiqueta";
				}
			}

			var jsonData = new
			{
				error = erro,
				message = retorno.DesMensagem,
				retorno
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Método que faz o fechamento do lote passado como parâmetro
		/// </summary>   
		/// <param name="numAp"></param>
		/// <param name="codDrt"></param>
		/// <param name="justificativa"></param>
		/// <param name="codFabrica"></param>
		/// <returns></returns>    
		public JsonResult FecharCaixaColetiva (string codFabrica, string numAp, string codDrt, string justificativa)
		{
			var erro = false;
			var linha = (CBLinhaDTO)Session["Linha"];
			var posto = (CBPostoDTO)Session["Posto"];
			var retorno = Defeito.ObterMensagem(Constantes.RAST_OK);
			var embalagem = Embalagem.Recuperar_Posto_Embalagem(posto);

			try
			{
				var numCc = embalagem.RecuperarNumeroCaixaColetivaAberta(numAp, linha.CodLinha, codFabrica);

				if (string.IsNullOrEmpty(numCc))
				{
					erro = true;
					retorno = Defeito.ObterMensagem(Constantes.RAST_LOTE_NAOEXISTE);
				}
				else
				{
					retorno = embalagem.FecharCaixaColetiva(numCc, codDrt, justificativa);

					if (retorno.CodMensagem != Constantes.RAST_OK)
					{
						erro = true;
						retorno = Defeito.ObterMensagem(Constantes.RAST_LOTE_NAOFINALIZADO);
					}
					else
					{
						retorno = embalagem.ImprimirCaixaColetiva(numCc, linha, posto);
					}
				}
			}
			catch (Exception)
			{
				erro = true;                
			}

			var jsonData = new
			{
				error = erro,
				message = retorno.DesMensagem,
				retorno
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// fazer
		/// </summary>
		/// <param name="codFabrica"></param>
		/// <param name="numAp"></param>
		/// <param name="codDrt"></param>
		/// <param name="justificativa"></param>
		/// <returns></returns>
		public JsonResult FecharLote(string codFabrica, string numAp, string codDrt, string justificativa)
		{
			var erro = false;
			var linha = (CBLinhaDTO)Session["Linha"];
			var posto = (CBPostoDTO)Session["Posto"];
			var retorno = Defeito.ObterMensagem(Constantes.RAST_OK);
			var embalagem = Embalagem.Recuperar_Posto_Embalagem(posto);

			try
			{
				var numCc = embalagem.RecuperarLoteAberto(codFabrica, numAp, linha.CodLinha);

				if (string.IsNullOrEmpty(numCc))
				{
					erro = true;
					retorno = Defeito.ObterMensagem(Constantes.RAST_LOTE_NAOEXISTE);
				}
				else
				{
					retorno = Embalagem.FecharLote(numCc, codDrt, justificativa);

					if (retorno.CodMensagem != Constantes.RAST_OK)
					{
						erro = true;
						retorno = Defeito.ObterMensagem(Constantes.RAST_LOTE_NAOFINALIZADO);
					}
				}
			}
			catch (Exception)
			{
				erro = true;
			}

			var jsonData = new
			{
				error = erro,
				message = retorno.DesMensagem,
				retorno
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Método que recupera os produtos do lote que estão aberto para determinada AP/Linha/Modelo/Fabrica
		/// </summary>
		/// <param name="numAp"></param>
		/// <param name="codModelo"></param>
		/// <param name="codFab"></param>
		/// <returns></returns>
		public JsonResult RecuperarProdutosDeCaixaColetivaAberta (string numAp, string codModelo, string codFab)
		{
			var erro = false;
			//var messageErro = "Existe Lote em aberto para esta linha. Produtos foram recuperados";
			var messageErro = "";
			var posto = (CBPostoDTO)Session["Posto"];
			var listaProdutos = new List<string>();
			var numeroLote = "";

			try
			{
				var embalagem = Embalagem.Recuperar_Posto_Embalagem(posto);

				var listaEmbalados = embalagem.RecuperarItensEmbaladosDaCaixaColetivaAberta(numAp, codModelo, posto.CodLinha, codFab);
				if (listaEmbalados.Count > 0)
				{
					listaProdutos = listaEmbalados.Select(res => res.NumECB).ToList();
					numeroLote = listaEmbalados.Select(res => res.NumLote).First();
				}
			}
			catch (Exception)
			{
				erro = true;
				messageErro = "Erro ao Recuperar Lista de Produtos do Lote em Aberto";
			}

			var jsonData = new
			{
				error = erro,
				message = messageErro,
				ListaProdutos = listaProdutos,
				numLote = numeroLote
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ValidarLoginAutorizacao(string cracha)
		{
			var ret = new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "LIME", DesMensagem = "Crachá autenticado" };

			var erro = false;
		   
			try
			{                  
				var retorno = Login.ValidaCracha(cracha);
				 
				if (retorno.Status.CodMensagem == Constantes.DB_ERRO)
				{
					erro = true;
					ret = Defeito.ObterMensagem(Constantes.DB_ERRO);
					ret.DesMensagem = "Crachá Inválido";
				}

				if (!erro)
					if (!(retorno.Cargo.Equals("069") || retorno.Cargo.Equals("079"))) //reserva de linha ou supervisora de producao
					{
						erro = true;
						ret = Defeito.ObterMensagem(Constantes.DB_ERRO);
						ret.DesMensagem = "Crachá sem permissão para autorizar este tipo de operação";
					} 
			}
			catch (Exception)
			{
				erro = true;
				ret = Defeito.ObterMensagem(Constantes.DB_ERRO);
				ret.DesMensagem = "Problemas ao validar crachá. Favor tentar novamente";
			}

			var jsonData = new
			{
				error = erro,
				retorno = ret,
				cracha = cracha
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Método que faz a validação do crachá para que a etiqueta possa ser reimpressa
		/// </summary>
		/// <param name="cracha"></param>
		/// <param name="codDrt"></param>
		/// <param name="statusAtual"></param>
		/// <returns></returns>
		public JsonResult AtualizarStatusReimpressao(string codDrt)
		{
			var ret = new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "LIME", DesMensagem = "" };

			var erro = false;
			var statusAt = false;

			try
			{
				var passagem = (Passagem)Session["passagem"];

				//var usuario = Login.ObterFuncionarioByCPF(codDrt);

				Embalagem.Atualizar_Modo_Retrabalho(passagem.posto.CodLinha, passagem.posto.NumPosto, Convert.ToInt32(codDrt));

				statusAt = Embalagem.Recuperar_Status_Retrabalho(passagem.posto.CodLinha, passagem.posto.NumPosto);

				ret.DesMensagem = (statusAt ? "Reimpressão ativada" : "Reimpressão desativada");                 
			}

			catch (Exception)
			{
				erro = true;
				ret = Defeito.ObterMensagem(Constantes.DB_ERRO);                
				ret.DesMensagem = "Erro ao atualizar status da reimpressão. Favor tente novamente";
			}

			var jsonData = new
			{
				error = erro,
				retorno =  ret,
				statusAtual = statusAt,
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Método que verifica se a reimpressão para o posto está ativa ou não
		/// </summary>
		/// <returns></returns>
		public JsonResult RecuperarStatusReimpressao ()
		{
			var erro = false;
			var messageErro = "";
			var statusAt = false;

			try
			{
				var passagem = (Passagem)Session["passagem"];

				statusAt = Embalagem.Recuperar_Status_Retrabalho(passagem.posto.CodLinha, passagem.posto.NumPosto);
			}

			catch (Exception)
			{
				erro = true;
				messageErro = "Erro ao recuperar status da reimpressão";
			}

			var jsonData = new
			{
				error = erro,
				message = messageErro,
				statusAtual = statusAt,
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Altera o tamanho do lote 
		/// </summary>
		/// <param name="tamanho"></param>
		/// <returns></returns>
		public JsonResult AlterarTamanhoCaixaColetiva (int tamanho)
		{
			bool erro;
			var messageErro = "";

			try
			{
				var passagem = (Passagem)Session["passagem"];

				var embalagem = Embalagem.Recuperar_Posto_Embalagem((CBPostoDTO)Session["Posto"]);

				//se voltar true, significa que deu tudo certo, então a função retorna false, pois não deu erro.
				erro = !embalagem.AlterarTamanhoCaixaColetiva(passagem.posto.CodLinha, tamanho);
			}

			catch (Exception)
			{
				erro = true;
				messageErro = "Erro ao alterar tamanho do lote";
			}

			var jsonData = new
			{
				error = erro,
				valor = tamanho,
				message = messageErro 
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);

		}

		/// <summary>
		/// Altera o tamanho do lote 
		/// </summary>
		/// <param name="tamanho"></param>
		/// <returns></returns>
		public JsonResult AlterarTamanhoLote(int tamanho)
		{
			bool erro;
			var messageErro = "";

			try
			{
				var passagem = (Passagem)Session["passagem"];

				var embalagem = Embalagem.Recuperar_Posto_Embalagem((CBPostoDTO)Session["Posto"]);

				//se voltar true, significa que deu tudo certo, então a função retorna false, pois não deu erro.
				erro = !embalagem.AlterarTamanhoLote(passagem.posto.CodLinha, tamanho);
			}

			catch (Exception)
			{
				erro = true;
				messageErro = "Erro ao alterar tamanho do lote";
			}

			var jsonData = new
			{
				error = erro,
				valor = tamanho,
				message = messageErro
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);

		}

		public ActionResult RecuperarDadosHoraHora (string sidx, string sord, int page, int rows, string codLinha)
		{
			var linhaAtual = ((CBLinhaDTO)Session["Linha"]);

			var dados = linhaAtual.Setor.Equals("FEC") ? Embalagem.carregaHoraHoraFEC(codLinha) : Embalagem.carregaHoraHoraPlaca(codLinha);

		 //   int pageIndex = Convert.ToInt32(page) - 1;
			int pageSize = rows;
			int totalRecords = (dados == null ? 0 : dados.Count());
			int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

			var jsonData = new
			{
				total = totalPages,
				page,
				records = totalRecords,
				rows = dados.ToArray()
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterTotalCaixaColetiva (string codFabrica, string numAp)
		{
			bool error = false;
			string total = "0";


			var numerosAPs = numAp.Split(new[] { ',' }).ToList();
			var sessao = Session;
			Hashtable lista = new Hashtable();
			Hashtable lista2 = new Hashtable();

			try
			{
				if (sessao != null)
				{
					Passagem passagem = new Passagem((DadosPosto)Session["DadosPosto"]);
					var linhaAtual = ((CBLinhaDTO)Session["Linha"]);
					var embalagem = Embalagem.Recuperar_Posto_Embalagem((CBPostoDTO)Session["Posto"]);

					foreach (var num in numerosAPs)
					{
						lista.Add(num, embalagem.ObterTotalProduzidoCaixaColetiva(codFabrica, num, linhaAtual.CodLinha));
						if (linhaAtual.Setor == "IAC")
						{
							lista2.Add(num, Geral.QtdeCaixasColetivasNoLote(linhaAtual.CodLinha));
						}
					}
				}
				else
				{
					error = true;
				}
			}
			catch
			{
				error = true;
			}

			var jsonData = new
			{
				error = error,
				total = total,
				ArrayAPRetorno = lista,
				ArrayAPLote = lista2
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}
 
		public JsonResult AjustarImpressoraEtiquetaTraseiraTablet (int quantidade)
		{
			var retorno = new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "LIME", DesMensagem = "Etiquetas Impressas com Sucesso." };
			var erro = false;
			var posto = (CBPostoDTO)Session["Posto"];
			var etiquetasPorPosto = Embalagem.RecuperarEtiquetasPorPosto(posto.CodLinha, posto.NumPosto, true);
			var etiqueta = etiquetasPorPosto.FirstOrDefault(x => x.NomeEtiqueta == "etq_tablet_individual_traseira");

			if (quantidade > 10)
			{
				erro = true;
				retorno = Defeito.ObterMensagem(Constantes.ERRO_AO_REALIZAR_IMPRESSAO);
				retorno.DesMensagem = "Quantidade Inválida. Favor imprimir no máximo 10 etiquetas por vez.";
			}
			
			if (!erro)
			{ 
				if (etiqueta == null)
				{
					erro = true;
					retorno = Defeito.ObterMensagem(Constantes.ERRO_AO_REALIZAR_IMPRESSAO);
					retorno.DesMensagem = "Etique não ativa para impressão. Favor habilitar impressão no posto";
				}
				else
				{
					if (!String.IsNullOrEmpty(etiqueta.NumIP_Impressora))
					{                     
						retorno = EmbalagemTablet.AjustarImpressoraEtiquetaTraseiraTablet(quantidade, etiqueta);
					}
					else
					{
						erro = true;
						retorno = Defeito.ObterMensagem(Constantes.CLIENT_ERROR);
					}
				}
			}
		   

			var jsonData = new
			{
				error = erro,
				message = retorno.DesMensagem,
				retorno
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);

		}

		public JsonResult AtivarImpressaoDeEtiqueta (string nomeEtiqueta)
		{
			var erro = false;
			var messageErro = "";
			string nomeAux = "";

			switch (nomeEtiqueta)
			{
				case "chkEtqReImpTablet_Coletiva_Fina":
					nomeAux = "etq_tablet_caixa_coletiva_fina";
					break;

				case "chkEtqReImpTablet_Coletiva":
					nomeAux = "etq_tablet_caixa_coletiva";
					break;

				case "chkEtqReImpTablet_IndvTraseira":
					nomeAux = "etq_tablet_individual_traseira";
					break;

				case "chkEtqReImpTablet_IndvCaixaSimples":
					nomeAux = "etq_tablet_manual";
					break;

				case "chkEtqReImpTablet_IndvCaixaCompleta":
					nomeAux = "etq_tablet_caixa";
					break;
			}

			try
			{
				var passagem = (Passagem)Session["passagem"];

				//se voltar true, significa que deu tudo certo, então a função retorna false, pois não deu erro.
				bool retorno = Embalagem.AtivarImpressaoDeEtiqueta(passagem.posto.CodLinha, passagem.posto.NumPosto, nomeAux);
				if (!retorno)
				{
					erro = true;
					messageErro = "Erro ao ativar/desativar impressão da etiqueta";
				}
			}

			catch (Exception)
			{
				erro = true;
				messageErro = "Erro ao ativar/desativar impressão da etiqueta";
			}

			var jsonData = new
			{
				error = erro,
				message = messageErro
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		public JsonResult RecuperarMascaras ()
		{
			var perfils = (List<viw_CBPerfilAmarraDTO>)Session["GetPerfilPosto"];

			var mascaras = perfils.Select(s => new { Tipo = s.DesTipoAmarra, Mascara = s.Mascara, Ap = s.numAP }).ToList();

			var jsonData = new
			{
				mascara = mascaras
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

	}
}