namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_cm
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pp_cm()
        {
            pp_cmcom = new HashSet<pp_cmcom>();
        }

        [Key]
        public long id_cm { get; set; }

        [StringLength(30)]
        public string cd_cm { get; set; }

        public int? revisao { get; set; }

        public DateTime? dt_stativo { get; set; }

        public DateTime? dt_revisao { get; set; }

        public long id_usrrevisao { get; set; }

        public long id_usrstativo { get; set; }

        [StringLength(40)]
        public string ds_cm { get; set; }

        public DateTime? dthr_vigor { get; set; }

        public short? is_consumirMP { get; set; }

        public short? st_ativo { get; set; }

        public DateTime? dthr_sai { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cmcom> pp_cmcom { get; set; }
    }
}
