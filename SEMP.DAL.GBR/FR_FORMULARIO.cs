namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_FORMULARIO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FR_FORMULARIO()
        {
            FR_COMPONENTE = new HashSet<FR_COMPONENTE>();
            FR_CONSULTA_AVANCADA = new HashSet<FR_CONSULTA_AVANCADA>();
            FR_FONTEDADOS = new HashSet<FR_FONTEDADOS>();
            FR_MENU = new HashSet<FR_MENU>();
            FR_PERMISSAO = new HashSet<FR_PERMISSAO>();
            FR_CATEGORIA = new HashSet<FR_CATEGORIA>();
            FR_SISTEMA = new HashSet<FR_SISTEMA>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FRM_CODIGO { get; set; }

        public int? IMG_INCLUIR { get; set; }

        public int? IMG_ALTERAR { get; set; }

        public int? IMG_EXCLUIR { get; set; }

        public int? IMG_GRAVAR { get; set; }

        public int? IMG_GRAVAR_MAIS { get; set; }

        public int? IMG_CANCELAR { get; set; }

        public int? IMG_ATUALIZAR { get; set; }

        public int? IMG_VALORES_PADRAO { get; set; }

        public int? IMG_UTILITARIO { get; set; }

        public int? IMG_LOG { get; set; }

        public int? IMG_SAIR { get; set; }

        public int? IMG_IMPRIMIR { get; set; }

        public int? IMG_AJUDA { get; set; }

        public int? IMG_PROXIMO { get; set; }

        public int? IMG_ULTIMO { get; set; }

        public int? IMG_PRIMEIRO { get; set; }

        public int? IMG_ANTERIOR { get; set; }

        [Required]
        [StringLength(100)]
        public string FRM_DESCRICAO { get; set; }

        [StringLength(1)]
        public string FRM_TIPO { get; set; }

        public int? FRM_POSICAOX { get; set; }

        public int? FRM_POSICAOY { get; set; }

        public int? FRM_TAMANHO { get; set; }

        public int? FRM_ALTURA { get; set; }

        [Required]
        [StringLength(1)]
        public string FRM_TIPO_CRIACAO { get; set; }

        [StringLength(38)]
        public string FRM_GUID { get; set; }

        public int? REL_CODIGO { get; set; }

        public int? USR_CODIGO { get; set; }

        [Required]
        [StringLength(1)]
        public string FRM_LOG { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_COMPONENTE> FR_COMPONENTE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_CONSULTA_AVANCADA> FR_CONSULTA_AVANCADA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_FONTEDADOS> FR_FONTEDADOS { get; set; }

        public virtual FR_RELATORIO FR_RELATORIO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_MENU> FR_MENU { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_PERMISSAO> FR_PERMISSAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_CATEGORIA> FR_CATEGORIA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_SISTEMA> FR_SISTEMA { get; set; }
    }
}
