using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.DTB_SSA.Models.Mapping
{
    public class tbl_HistoricoToshibaMap : EntityTypeConfiguration<tbl_HistoricoToshiba>
    {
        public tbl_HistoricoToshibaMap()
        {
            // Primary Key
            this.HasKey(t => t.NumSerieSTI);

            // Properties
            this.Property(t => t.NumSerieSTI)
                .IsRequired()
                .HasMaxLength(9);

            this.Property(t => t.NumSerieToshiba)
                .IsRequired()
                .HasMaxLength(9);

            // Table & Column Mappings
            this.ToTable("tbl_HistoricoToshiba");
            this.Property(t => t.NumSerieSTI).HasColumnName("NumSerieSTI");
            this.Property(t => t.NumSerieToshiba).HasColumnName("NumSerieToshiba");
        }
    }
}
