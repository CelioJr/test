namespace SEMP.Model.DTO
{
    public class CnqOrigemDTO
    {
        public string CodOrigem { get; set; }
        public string DesOrigem { get; set; }
    }
}
