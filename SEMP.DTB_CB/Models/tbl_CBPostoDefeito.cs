using System;
using System.Collections.Generic;

namespace SEMP.DTB_CB.Models
{
    public partial class tbl_CBPostoDefeito
    {
        public int CodLinha { get; set; }
        public int NumPosto { get; set; }
        public string CodDefeito { get; set; }
        public virtual tbl_CBPosto tbl_CBPosto { get; set; }
    }
}
