using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
	public partial class CBPlanoAcaoDTO
	{
		[Display(Name = "C�digo")]
		public int CodPlano { get; set; }
		[Display(Name = "Linha")]
		public int CodLinha { get; set; }
		[Display(Name = "Posto")]
		public int? NumPosto { get; set; }
		[Display(Name = "Data")]
		[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
		public DateTime? DatPlano { get; set; }
		[Display(Name = "Respons�vel")]
		public string CodDRT { get; set; }
		[Display(Name = "Descri��o")]
		public string DesPlano { get; set; }
		[Display(Name = "Status")]
		public int? FlgStatus { get; set; }
		[Display(Name = "Conclus�o")]
		[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
		public DateTime? DatConclusao { get; set; }
		[Display(Name = "Hora")]
		public int? Hora { get; set; }

		[Display(Name = "Descri��o")]
		public string DesLinha { get; set; }
		[Display(Name = "Descri��o")]
		public string DesPosto { get; set; }
		[Display(Name = "Nome")]
		public string NomResponsavel { get; set; }

	}
}
