﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;
using RAST.BO.Rastreabilidade;
using SEMP.Model;
using SEMP.Model.DTO;
using SEMP.BO;
using MATERIAIS.BO.Estoque;
using RAST.BO.Mobilidade;

namespace SEMP.Areas.Coleta.Controllers
{
	public class UtilController : Controller
	{
		//
		// GET: /Coleta/Util/

		public String ObterDescricaoItem(string CodItem)
		{
			return Geral.ObterDescricaoItem(CodItem);
		}

		public String ObterNomFuncionario(string codDRT)
		{
			var retorno = Login.ObterFuncionarioByDRT(codDRT);
			return retorno != null && retorno.CodDRT != null ? retorno.NomFuncionario.Trim() : "";
		}

		public JsonResult ObterItem(string codItem)
		{
			var item = Geral.ObterItem(codItem);

			return Json(item, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ObterItens()
		{
			var item = Geral.ObterItens().Take(10);

			return Json(item, JsonRequestBehavior.AllowGet);

		}

		/// <summary>
		/// Obter modelos de produto para consultas
		/// </summary>
		/// <returns></returns>
		public JsonResult ObterModelos(String searchTerm, int page = 0, int rows = 0)
		{
			var retorno = new Retorno();

			if (string.IsNullOrEmpty(searchTerm))
			{
				retorno.rows.AddRange(Geral.ObterModelos());
			}
			else
			{
				retorno.rows.AddRange(Geral.ObterModelos(searchTerm));
			}

			int totalPages = 0;
			int start = 0;

			if (retorno.records > 0)
			{
				totalPages = (retorno.records / rows);
			}
			if (page > totalPages)
			{
				page = totalPages;
			}

			start = rows * page - rows;
			if (totalPages > 0)
			{
				retorno.rows = retorno.rows.Skip(start).Take(rows).ToList();
			}
			else if (rows > 0)
			{
				retorno.rows = retorno.rows.Take(rows).ToList();
			}
			retorno.total = totalPages;
			retorno.page = page;
			return Json(retorno, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ObterModelosAll(string fase)
		{
			var item = Geral.ObterModelosAll().Select(x => new {x.CodItem, DesItem = x.DesItem.Trim()});

			if (!String.IsNullOrEmpty(fase) && fase != "FEC")
			{
				item = item.Where(x => !x.CodItem.StartsWith("9"));
			}
			else
			{
				item = item.Where(x => x.CodItem.StartsWith("9"));
			}

			return Json(item, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ObterModelosPrograma(String searchTerm, string datPrograma, int page = 0, int rows = 0)
		{
			var retorno = new Retorno();

			if (string.IsNullOrEmpty(searchTerm))
			{
				retorno.rows.AddRange(Geral.ObterModelosPrograma("", DateTime.Parse(datPrograma)));
			}
			else
			{
				retorno.rows.AddRange(Geral.ObterModelosPrograma(searchTerm, DateTime.Parse(datPrograma)));
			}

			int totalPages = 0;
			int start = 0;

			if (retorno.records > 0)
			{
				totalPages = (retorno.records / rows);
			}
			if (page > totalPages)
			{
				page = totalPages;
			}

			start = rows * page - rows;
			if (totalPages > 0)
			{
				retorno.rows = retorno.rows.Skip(start).Take(rows).ToList();
			}
			else if (rows > 0)
			{
				retorno.rows = retorno.rows.Take(rows).ToList();
			}
			retorno.total = totalPages;
			retorno.page = page;
			return Json(retorno, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ObterModelosProgramaMes(String searchTerm, string datPrograma, int page = 0, int rows = 0)
		{
			var retorno = new Retorno();

			if (string.IsNullOrEmpty(searchTerm))
			{
				retorno.rows.AddRange(Geral.ObterModelosProgramaMes("", DateTime.Parse(datPrograma)));
			}
			else
			{
				retorno.rows.AddRange(Geral.ObterModelosProgramaMes(searchTerm, DateTime.Parse(datPrograma)));
			}

			int totalPages = 0;
			int start = 0;

			if (retorno.records > 0)
			{
				totalPages = (retorno.records / rows);
			}
			if (page > totalPages)
			{
				page = totalPages;
			}

			start = rows * page - rows;
			if (totalPages > 0)
			{
				retorno.rows = retorno.rows.Skip(start).Take(rows).ToList();
			}
			else if (rows > 0)
			{
				retorno.rows = retorno.rows.Take(rows).ToList();
			}
			retorno.total = totalPages;
			retorno.page = page;
			return Json(retorno, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ObterModelosData(string datPrograma, string term, string q, string _type, int? page)
		{
			var retorno = new RetornoSelect2();
			var dados = Geral.ObterModelosProgramaMes(term, DateTime.Parse(datPrograma));

			var transf = dados.Select(x => new Results()
			{
				id = x.CodItem,
				text = x.CodItem + " - " + x.DesItem
			}).ToList();

			retorno.results = transf;

			return Json(retorno, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ObterModelosSelect(string search, string q, string _type, int? page)
		{
			var retorno = new RetornoSelect2();
			var dados = Geral.ObterModelos(search??"", page);

			var transf = dados.Select(x => new Results()
			{
				id = x.CodItem,
				text = x.CodItem + " - " + x.DesItem.Trim()
			}).ToList();

			retorno.results = transf;

			return Json(retorno, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ObterModelosPCISelect(string search, string q, string _type, int? page)
		{
			var retorno = new RetornoSelect2();
			var dados = Geral.ObterModelosPCI(search ?? "", page);

			var transf = dados.Select(x => new Results()
			{
				id = x.CodItem,
				text = x.CodItem + " - " + x.DesItem.Trim()
			}).ToList();

			retorno.results = transf;

			return Json(retorno, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ObterLinhasSelect(string search, string q, string _type, int? page)
		{
			var retorno = new RetornoSelect2();
			var dados = Geral.ObterLinhas(search ?? "", page);

			var transf = dados.Select(x => new Results()
			{
				id = x.CodLinha,
				text = x.CodLinha + " - " + x.DesLinha.Trim()
			}).ToList();

			retorno.results = transf;

			return Json(retorno, JsonRequestBehavior.AllowGet);

		}


		public String ObterDescricaoLinha(string codLinha)
		{
			if (String.IsNullOrEmpty(codLinha)) return "";
			var linha = Geral.ObterLinha(codLinha);

			return linha.DesLinha.Trim();

		}

		public String ObterDescricaoFabrica(string codFab)
		{

			var fab = Geral.ObterFabrica(codFab);
			if (fab.CodFab == null)
			{
				return "FÁBRICA NÃO ENCONTRADA";
			}
			return fab.NomFab.Trim();

		}

		public String ObterDesDepto(string codFab, string codDepto)
		{

			var depto = Geral.ObterDesDepartamento(codFab, codDepto);

			return depto.Trim();

		}

		public JsonResult ObterUsuario(string codFab, string nomUsuario)
		{
			var usuario = Geral.ObterUsuario(codFab, nomUsuario);

			return Json(usuario, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ObterLinha(int codLinha)
		{
			var dados = Geral.ObterLinha(codLinha);

			return Json(dados, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ObterQtdDefeitos(int codLinha){

			var datIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var datFim = datIni.AddDays(1).AddMinutes(-1);

			var defeitos = Defeito.ObterDefeitosRelRef(datIni, datFim, codLinha);


			var dados = defeitos.GroupBy(x => x.CodLinha)
								.Select(x => new { 
									CodLinha = x.Key,
									QtdDefeitos = x.Count(),
									QtdFF = x.Count(y => y.CodCausa.StartsWith("WX"))
								})
								.ToList();

			return Json(dados, JsonRequestBehavior.AllowGet);

		}

		#region Mobilidade
		public JsonResult ObterLinhaMOB(int codLinha)
		{
			var dados = GBR.ObterLinha(codLinha);

			return Json(dados, JsonRequestBehavior.AllowGet);

		}
		#endregion

	}

	class Retorno
	{
		public int page { get; set; }

		public int records
		{
			get { return rows.Count(); }
		}

		public int total { get; set; }

		public List<viw_ItemDTO> rows { get; set; }

		public Retorno()
		{
			rows = new List<viw_ItemDTO>();
		}

	}

	class RetornoSelect2
	{
		public List<Results> results { get; set; }
		public Pagination pagination { get; set; }

		public RetornoSelect2(){
			results = new List<Results>();
			pagination = new Pagination();

		}

	}

	public class Results {
		public Results(){
			selected = false;
			disabled = false;
		}

		public string id { get; set; }
		public string text { get; set; }
		public bool selected { get; set; }
		public bool disabled { get; set; }
	}
	public class Pagination {
		public Pagination(){
			more = true;
		}
		public bool more { get; set; }
	}
}
