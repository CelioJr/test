using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;

namespace SEMP.Model.DTO
{
	public class viw_CBDefeitoTurnoDTO
	{
		[Display(Name = "Num. S�rie")]
		public string NumECB { get; set; }

		[Display(Name = "Linha")]
		public int CodLinha { get; set; }

		[Display(Name = "Posto")]
		public int NumPosto { get; set; }

		[Display(Name = "Defeito")]
		public string CodDefeito { get; set; }

		[Display(Name = "Revisado")]
		public Nullable<int> FlgRevisado { get; set; }

		[Display(Name = "Causa")]
		public string CodCausa { get; set; }

		[Display(Name = "Posi��o")]
		public string Posicao { get; set; }

		[Display(Name = "Tipo")]
		public string FlgTipo { get; set; }

		[Display(Name = "Origem")]
		public string CodOrigem { get; set; }

		[Display(Name = "A��o")]
		public string DesAcao { get; set; }

		[Display(Name = "Data Evento")]
		public System.DateTime DatEvento { get; set; }

		[Display(Name = "Data Revisado")]
		public Nullable<System.DateTime> DatRevisado { get; set; }

		[Display(Name = "Data Ent. Revis�o")]
		public Nullable<System.DateTime> DatEntRevisao { get; set; }

		[Display(Name = "Usu�rio")]
		public string NomUsuario { get; set; }

		[Display(Name = "IP")]
		public string NumIP { get; set; }

		[Display(Name = "Cod. RMA")]
		public Nullable<int> CodRma { get; set; }

		[Display(Name = "Posto Revis�o")]
		public Nullable<int> NumPostoRevisado { get; set; }

		[Display(Name = "DRT")]
		public string CodDRT { get; set; }

		[Display(Name = "DRT Revisor")]
		public string CodDRTRevisor { get; set; }

		[Display(Name = "Defeito Teste")]
		public string CodDefeitoTest { get; set; }

		[Display(Name = "Observa��o Defeito")]
		public string ObsDefeito { get; set; }

		[Display(Name = "Defeito Incorreto")]
		public string DefIncorreto { get; set; }

		[Display(Name = "DRT Teste")]
		public string CodDRTTest { get; set; }

		[Display(Name = "Usu�rio Teste")]
		public string NomUsuarioTest { get; set; }

		[Display(Name = "S�rie Pai")]
		public string NumSeriePai { get; set; }

		[Display(Name = "Linha Revisado")]
		public int? CodLinhaRevisado { get; set; }

		[Display(Name = "Romaneio")]
		public string Romaneio { get; set; }

		[Display(Name = "NE Posi��o")]
		public string CodItem { get; set; } 

		[Display(Name = "Data Refer�ncia")]
		public DateTime? DatReferencia { get; set; }

		[Display(Name = "Turno")]
		public int? Turno { get; set; }

		[Display(Name = "Descri��o Defeito")]
		public String DesDefeito { get; set; }

		[Display(Name = "Descri��o Causa")]
		public String DesCausa { get; set; }

		[Display(Name = "Descri��o Origem")]
		public String DesOrigem { get; set; }

		[Display(Name = "Descri��o Posto")]
		public String DesPosto { get; set; }

		[Display(Name = "Descri��o Modelo")]
		public String DesModelo { get; set; }

		[Display(Name = "Qtd. Acum. M�s")]
		public int QtdMes { get; set; }

		[Display(Name = "Tempo reparo")]
		public TimeSpan? TempoReparo { get { return DatRevisado - DatEvento; } }

		[Display(Name = "Cod. Modelo")]
		public string CodModelo
		{
			get
			{
				if (String.IsNullOrEmpty(CodigoModelo))
				{
					if (String.IsNullOrEmpty(NumECB) || NumECB.Length < 6)
					{
						return "";
					}
					else
					{
						return NumECB.Substring(0, 6);
					}
				}
				else
				{
					return CodigoModelo;
				}

			}
		}
		public string CodigoModelo { get; set; }

		[Display(Name = "Descri��o Linha")]
		public String DesLinha { get; set; }

		[Display(Name = "Descri��o Posicao")]
		public String DesPosicao { get; set; }

        [Display(Name = "Descri��o Linha")]
        public String DesUltimaLinhaIAC { get; set; }

		[Display(Name = "Setor da F�brica")]
		public string Setor { get; set; }

		public viw_CBDefeitoTurnoDTO()
		{
			DesLinha = String.Empty;
			DesModelo = String.Empty;
			DesOrigem = String.Empty;
			DesCausa = String.Empty;
			DesDefeito = String.Empty;
			DesAcao = String.Empty;
			DesPosto = String.Empty;
			CodOrigem = String.Empty;
			CodCausa = String.Empty;
		}
	}
}
