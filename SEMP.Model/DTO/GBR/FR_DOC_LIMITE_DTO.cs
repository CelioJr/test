using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DOC_LIMITE_DTO
    {
        public int DOC_CODIGO { get; set; }
        public string LIM_DESCRICAO { get; set; }
        public virtual FR_DOC_PRINCIPAL_DTO FR_DOC_PRINCIPAL { get; set; }
    }
}
