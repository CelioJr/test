﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade
{
	public class TempoAtravessamento
	{

		public DateTime DatReferencia { get; set; }
		public string CodModelo { get; set; }
		public string DesItem { get; set; }
		public decimal TempoAtravessa { get; set; }
		public decimal? TempoCorrido { get; set; }
		public decimal? TempoProducao { get; set; }
		public decimal? TempoEspera { get; set; }
		public decimal? TempoIntervalo { get; set; }
		public decimal? TempoTecnico { get; set; }
		public decimal? TempoManutencao { get; set; }
		public string Observacao { get; set; }
		public int? QtdAnalisados { get; set; }
		public int? QtdProduzido { get; set; }
		public string TA2 { get; set; }
		public string TA4 { get; set; }
	}
}
