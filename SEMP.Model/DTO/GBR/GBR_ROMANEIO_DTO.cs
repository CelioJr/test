using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_ROMANEIO_DTO
    {
        public int COD_ROMANEIO { get; set; }
        public string DANFE { get; set; }
        public string PALLET { get; set; }
        public string SKU { get; set; }
        public Nullable<int> QTD { get; set; }
        public Nullable<int> DB_QTD { get; set; }
        public string DB_DOC { get; set; }
        public Nullable<int> DB_SERIE { get; set; }
        public string DB_COD { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
        public Nullable<bool> GERADO { get; set; }
        public string SERIAL { get; set; }
        public string IMEI { get; set; }
        public string PACK { get; set; }
    }
}
