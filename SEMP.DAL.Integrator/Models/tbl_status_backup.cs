namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_status_backup
    {
        [Key]
        [Column(Order = 0)]
        public int id { get; set; }

        public int? id_pai { get; set; }

        [StringLength(50)]
        public string codapp { get; set; }

        [StringLength(100)]
        public string name { get; set; }

        [Key]
        [Column(Order = 1)]
        public bool status { get; set; }

        public DateTime? date { get; set; }
    }
}
