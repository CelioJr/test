using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.DTB_SSA.Models.Mapping
{
    public class tbl_OperadorMap : EntityTypeConfiguration<tbl_Operador>
    {
        public tbl_OperadorMap()
        {
            // Primary Key
            this.HasKey(t => t.codOperador);

            // Properties
            this.Property(t => t.codOperador)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.desLogin)
                .IsRequired()
                .HasMaxLength(14);

            this.Property(t => t.desNome)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.desSenha)
                .IsRequired()
                .HasMaxLength(6);

            this.Property(t => t.NivelFullControl)
                .HasMaxLength(3);

            this.Property(t => t.NivelSistCQ)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.nivel_sist_defeito)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.nivel_sist_SGI)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.nivel_sist_Ind)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.nivel_sist_componente)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.nivel_sist_sec)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.nivel_sist_revisao)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.nivel_sist_PSI)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.nivel_sist_revisaoPeriodica)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.nivel_sist_ExpedicaoNF)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.flgRamalAtivo)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.codFab)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.codDepto)
                .HasMaxLength(11);

            this.Property(t => t.nivel_sec_sem)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.nivel_colex)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.nivel_sist_MM)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.nivel_sist_PlanProd)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.nivel_sist_PcEmb)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.nivel_sist_CheckFrete)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.nivel_sist_gdtissa)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.nivel_sist_separacao)
                .HasMaxLength(3);

            this.Property(t => t.nivel_sist_siet)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.nivel_sist_rh)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.miner_depto_PlanoAcao)
                .IsFixedLength()
                .HasMaxLength(11);

            this.Property(t => t.Email)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("tbl_Operador");
            this.Property(t => t.codOperador).HasColumnName("codOperador");
            this.Property(t => t.desLogin).HasColumnName("desLogin");
            this.Property(t => t.desNome).HasColumnName("desNome");
            this.Property(t => t.desSenha).HasColumnName("desSenha");
            this.Property(t => t.codGrupo).HasColumnName("codGrupo");
            this.Property(t => t.NivelFullControl).HasColumnName("NivelFullControl");
            this.Property(t => t.NivelSistCQ).HasColumnName("NivelSistCQ");
            this.Property(t => t.nivel_sist_defeito).HasColumnName("nivel_sist_defeito");
            this.Property(t => t.nivel_sist_SGI).HasColumnName("nivel_sist_SGI");
            this.Property(t => t.nivel_sist_Ind).HasColumnName("nivel_sist_Ind");
            this.Property(t => t.nivel_sist_componente).HasColumnName("nivel_sist_componente");
            this.Property(t => t.nivel_sist_sec).HasColumnName("nivel_sist_sec");
            this.Property(t => t.nivel_sist_revisao).HasColumnName("nivel_sist_revisao");
            this.Property(t => t.nivel_sist_PSI).HasColumnName("nivel_sist_PSI");
            this.Property(t => t.nivel_sist_revisaoPeriodica).HasColumnName("nivel_sist_revisaoPeriodica");
            this.Property(t => t.nivel_sist_ExpedicaoNF).HasColumnName("nivel_sist_ExpedicaoNF");
            this.Property(t => t.flgRamalAtivo).HasColumnName("flgRamalAtivo");
            this.Property(t => t.codFab).HasColumnName("codFab");
            this.Property(t => t.codDepto).HasColumnName("codDepto");
            this.Property(t => t.nivel_sec_sem).HasColumnName("nivel_sec_sem");
            this.Property(t => t.nivel_colex).HasColumnName("nivel_colex");
            this.Property(t => t.nivel_sist_MM).HasColumnName("nivel_sist_MM");
            this.Property(t => t.nivel_sist_PlanProd).HasColumnName("nivel_sist_PlanProd");
            this.Property(t => t.nivel_sist_PcEmb).HasColumnName("nivel_sist_PcEmb");
            this.Property(t => t.nivel_sist_CheckFrete).HasColumnName("nivel_sist_CheckFrete");
            this.Property(t => t.nivel_sist_gdtissa).HasColumnName("nivel_sist_gdtissa");
            this.Property(t => t.dataExpiracao).HasColumnName("dataExpiracao");
            this.Property(t => t.nivel_sist_separacao).HasColumnName("nivel_sist_separacao");
            this.Property(t => t.nivel_sist_siet).HasColumnName("nivel_sist_siet");
            this.Property(t => t.dataExpiracaoSIGA).HasColumnName("dataExpiracaoSIGA");
            this.Property(t => t.nivel_sist_rh).HasColumnName("nivel_sist_rh");
            this.Property(t => t.miner_depto_PlanoAcao).HasColumnName("miner_depto_PlanoAcao");
            this.Property(t => t.Email).HasColumnName("Email");

            // Relationships
            //this.HasMany(t => t.tbl_SIGA_Perfil)
            //    .WithMany(t => t.tbl_Operador)
            //    .Map(m =>
            //        {
            //            m.ToTable("tbl_SIGA_OperadorPerfil");
            //            m.MapLeftKey("codOperador");
            //            m.MapRightKey("codPerfil");
            //        });

            //this.HasRequired(t => t.tbl_Grupo)
            //    .WithMany(t => t.tbl_Operador)
            //    .HasForeignKey(d => d.codGrupo);

        }
    }
}
