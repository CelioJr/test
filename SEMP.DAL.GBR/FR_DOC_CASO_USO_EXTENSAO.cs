namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_DOC_CASO_USO_EXTENSAO
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DOC_CODIGO { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int USO_CODIGO_PRINCIPAL { get; set; }

        [StringLength(255)]
        public string EXT_CONDICAO { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int USO_CODIGO_EXTENDIDO { get; set; }

        public virtual FR_DOC_CASO_USO FR_DOC_CASO_USO { get; set; }

        public virtual FR_DOC_CASO_USO FR_DOC_CASO_USO1 { get; set; }
    }
}
