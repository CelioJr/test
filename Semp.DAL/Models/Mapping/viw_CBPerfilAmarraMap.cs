using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class viw_CBPerfilAmarraMap : EntityTypeConfiguration<viw_CBPerfilAmarra>
    {
        public viw_CBPerfilAmarraMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodLinha, t.NumPosto, t.CodModelo, t.CodTipoAmarra, t.CodItem, t.DesModelo, t.DesItem });

            // Properties
            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumPosto)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodTipoAmarra)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.Mascara)
                .HasMaxLength(8000)
                .IsUnicode(false);

            this.Property(t => t.DesTipoAmarra)
                .HasMaxLength(15)
                .IsUnicode(false);

            this.Property(t => t.DesModelo)
                .IsRequired()
                .HasMaxLength(40)
                .IsUnicode(false);

            this.Property(t => t.DesItem)
                .IsRequired()
                .HasMaxLength(40)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("viw_CBPerfilAmarra");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.NumPosto).HasColumnName("NumPosto");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.CodTipoAmarra).HasColumnName("CodTipoAmarra");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.Mascara).HasColumnName("Mascara");
            this.Property(t => t.DesTipoAmarra).HasColumnName("DesTipoAmarra");
            this.Property(t => t.DesModelo).HasColumnName("DesModelo");
            this.Property(t => t.DesItem).HasColumnName("DesItem");
        }
    }
}
