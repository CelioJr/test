﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.Model.DTO;

namespace SEMP.Model.VO.Rastreabilidade
{
    public class RecebimentoLoteVO
    {
        public bool Error { get; set; }
        public CBLoteDTO PrimeiroLoteASair { get; set; }
    }
}
