﻿using System;

namespace SEMP.SAPConnector.VO
{
	public class ZTPP_OPMAO
	{

		public string MANDT { get; set; }
		public int ID { get; set; }
		public string WERKS { get; set; }
		public string AUFNR { get; set; }
		public string TYPE { get; set; }
		public string MATNR { get; set; }
		public string ARBPL { get; set; }
		public Decimal OQTY { get; set; }
		public Decimal WEMNG { get; set; }
		public DateTime DTCV { get; set; }
		public DateTime DTIN { get; set; }
		public DateTime DTFM { get; set; }
		public DateTime DTREF { get; set; }
		public DateTime DTMOD { get; set; }
		public string SYSTEM_STATUS { get; set; }
		public string STAT1 { get; set; }
		public DateTime DTPI { get; set; }
		public string STAT2 { get; set; }
		public DateTime DTSISAP { get; set; }
		public string PACKNO { get; set; }
	}
}
