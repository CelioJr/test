﻿using log4net;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.ServiceProcess;

namespace SEMP.WindowsService
{
	static class Program
	{
		//private static ILog _logger = LogManager.GetLogger(typeof(Program));
		private static ILog _logger = LogManager.GetLogger("Service");

		/// <summary>
		/// Ponto de entrada principal para o aplicativo.
		/// </summary>
		static void Main(string[] args)
		{
			// configure the Log4Net framework based on the config file
			log4net.Config.XmlConfigurator.Configure();

			_logger.Info("Iniciando...");

			#region Instala o windows services
			try
			{
				
				if (args != null && args.Length == 1 && args[0].Length > 1
				  && (args[0][0] == '-' || args[0][0] == '/'))
				{
					switch (args[0].Substring(1).ToLower())
					{
						default:
							break;
						case "install":
						case "i":
							SelfInstaller.InstallMe();
							break;
						case "uninstall":
						case "u":
							SelfInstaller.UninstallMe();
							break;
						case "console":
						case "c":
							//MyConsoleHost.Launch();
							break;
					}
					return;
				}
				
			}
			catch (Exception ex)
			{
				_logger.Error("Erro na instalação do serviço.", ex);
			}
			#endregion


			#region Inicia todos os windows services
			try
			{
				
				// list of services to run
				List<ServiceBase> servicesToRunList = new List<ServiceBase>();

				// Look for all Services in this assembly
				// a Service NEED to inherits ServiceBase class

				// get all types
				Type[] types = Assembly.GetExecutingAssembly().GetTypes();

				// foreach class type
				for (int i = 0; i < types.Length; i++)
				{
					Type type = types[i];

					// verify if inherits a ServiceBase class
					if (type.IsSubclassOf(typeof(ServiceBase)))
					{
						// instance with an activator (reflection)   
						ServiceBase service = (ServiceBase)Activator.CreateInstance(type);
						// add to services to run list
						servicesToRunList.Add(service);
					}
				}

				_logger.Info("Iniciando os serviços...");
				_logger.Info($"Quantidade de serviços a iniciar: {servicesToRunList.Count.ToString()}");

				// put all list to start if exists 
				if (servicesToRunList.Count > 0)
				{
					ServiceBase.Run(servicesToRunList.ToArray());
				}
				
			}
			catch (Exception ex)
			{
				_logger.Error("Erro ao iniciar serviços.", ex);
			}
			#endregion
		}
	}
}
