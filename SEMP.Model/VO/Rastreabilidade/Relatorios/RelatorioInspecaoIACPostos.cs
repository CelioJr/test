﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
    public class RelatorioInspecaoIACPostos
    {
        public string Descricao { get; set; }

        public int TotalInspecionado { get; set; }
        public int TotalInspecionadoAcumulado { get; set; }
        public int TotalDefeito { get; set; }
        public int PPMAcumulado { get; set; }


    }
}
