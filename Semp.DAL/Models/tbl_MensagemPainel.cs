using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_MensagemPainel
    {
        public long CodMensagem { get; set; }
        public string Destino { get; set; }
        public string Mensagem { get; set; }
        public Nullable<System.DateTime> Data { get; set; }
        public string flgAtivo { get; set; }
        public string nomusuario { get; set; }
    }
}
