using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DTB_CB.Models.Mapping
{
    public class tbl_CBEmbaladaMap : EntityTypeConfiguration<tbl_CBEmbalada>
    {
        public tbl_CBEmbaladaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.NumECB, t.DatLeitura, t.CodLinha });

            // Properties
            this.Property(t => t.NumECB)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            //this.Property(t => t.NumLote)
            //    .HasMaxLength(18);

            this.Property(t => t.CodModelo)
                .IsFixedLength()
                .HasMaxLength(6);

            this.Property(t => t.CodLinhaFec)
                .IsFixedLength()
                .HasMaxLength(8);

            this.Property(t => t.NumAP)
                .IsFixedLength()
                .HasMaxLength(6);

            this.Property(t => t.CodOperador)
                .HasMaxLength(8);

            this.Property(t => t.CodTestador)
                .HasMaxLength(8);

            this.Property(t => t.CodFab)
                .IsFixedLength()
                .HasMaxLength(2);

            // Table & Column Mappings
            this.ToTable("tbl_CBEmbalada");
            this.Property(t => t.NumECB).HasColumnName("NumECB");
            this.Property(t => t.DatLeitura).HasColumnName("DatLeitura");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.NumLote).HasColumnName("NumLote");
            this.Property(t => t.DatReferencia).HasColumnName("DatReferencia");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.CodLinhaFec).HasColumnName("CodLinhaFec");
            this.Property(t => t.NumAP).HasColumnName("NumAP");
            this.Property(t => t.CodOperador).HasColumnName("CodOperador");
            this.Property(t => t.CodTestador).HasColumnName("CodTestador");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            //this.Property(t => t.NumPosto).HasColumnName("NumPosto");
        }
    }
}
