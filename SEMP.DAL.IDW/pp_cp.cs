namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_cp
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pp_cp()
        {
            dw_consolid = new HashSet<dw_consolid>();
            dw_consolpalog = new HashSet<dw_consolpalog>();
            dw_consolpt = new HashSet<dw_consolpt>();
            dw_estlocalpro = new HashSet<dw_estlocalpro>();
            om_pt = new HashSet<om_pt>();
            om_ptcp = new HashSet<om_ptcp>();
            pp_cp_pre = new HashSet<pp_cp_pre>();
            pp_cp_pre1 = new HashSet<pp_cp_pre>();
            pp_cpentsai = new HashSet<pp_cpentsai>();
            pp_cpneccron = new HashSet<pp_cpneccron>();
            pp_cpnserie = new HashSet<pp_cpnserie>();
            pp_cpproduto = new HashSet<pp_cpproduto>();
            pp_cpfaltamp = new HashSet<pp_cpfaltamp>();
        }

        [Key]
        public long id_cp { get; set; }

        [StringLength(60)]
        public string cd_cp { get; set; }

        public int? revisao { get; set; }

        public DateTime? dt_revisao { get; set; }

        public DateTime? dt_stativo { get; set; }

        public short? st_ativo { get; set; }

        public long id_plano { get; set; }

        public long? id_pt { get; set; }

        public long? id_cliente { get; set; }

        public long? id_usrRevisao { get; set; }

        public long? id_usrstativo { get; set; }

        public long? id_folha { get; set; }

        public long? id_est { get; set; }

        public int? prioridade { get; set; }

        public short? is_antecipacao { get; set; }

        public short? st_cp { get; set; }

        public DateTime? dthr_inicio { get; set; }

        public DateTime? dthr_final { get; set; }

        public short? tp_cp { get; set; }

        public long? id_cal { get; set; }

        public long? id_gt { get; set; }

        public short? is_FinalSerie { get; set; }

        public short? is_Massa { get; set; }

        public short? is_tryout { get; set; }

        public short? is_cm { get; set; }

        public short? is_barrasete { get; set; }

        public DateTime? dt_cobertura { get; set; }

        public long? id_rota { get; set; }

        public int? passo { get; set; }

        public short? is_faltamp { get; set; }

        public short? is_ap_aberta { get; set; }

        public DateTime? dthr_inicioreal { get; set; }

        public DateTime? dthr_finalreal { get; set; }

        public long? id_nec { get; set; }

        public DateTime? dthr_isetup { get; set; }

        public DateTime? dthr_fsetup { get; set; }

        public virtual dw_cal dw_cal { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolid> dw_consolid { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolpalog> dw_consolpalog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolpt> dw_consolpt { get; set; }

        public virtual dw_est dw_est { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_estlocalpro> dw_estlocalpro { get; set; }

        public virtual dw_folha dw_folha { get; set; }

        public virtual dw_rota dw_rota { get; set; }

        public virtual om_gt om_gt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_pt> om_pt { get; set; }

        public virtual om_pt om_pt1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_ptcp> om_ptcp { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        public virtual pp_cliente pp_cliente { get; set; }

        public virtual pp_nec pp_nec { get; set; }

        public virtual pp_plano pp_plano { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cp_pre> pp_cp_pre { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cp_pre> pp_cp_pre1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cpentsai> pp_cpentsai { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cpneccron> pp_cpneccron { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cpnserie> pp_cpnserie { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cpproduto> pp_cpproduto { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cpfaltamp> pp_cpfaltamp { get; set; }
    }
}
