﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.VO;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using SEMP.Model.DTO;

namespace RAST.BO.Relatorios
{
	public class Indicadores
	{
		public static List<IndicadorGlobal> ObterIndicadorGlobal(DateTime DatInicio, DateTime DatFim)
		{

			List<IndicadorGlobal> indicador = new List<IndicadorGlobal>();

			// Declarar objetos que receberão os dados
			IndicadorGlobal TotalProduzido = new IndicadorGlobal(1, "Total Produzido");
			IndicadorGlobal Eficacia = new IndicadorGlobal(2, "Índice de Produção (%)");
			IndicadorGlobal Defeitos = new IndicadorGlobal(3, "Total Defeitos");
			IndicadorGlobal IndiceDefeito = new IndicadorGlobal(4, "Índice Defeitos (%)");
			//IndicadorGlobal IndOEE = new IndicadorGlobal(5, "OEE");

			// Buscar os dados de produção para as fases (Programa e Realizado)
			TotalProduzido.RealIAC = daoIndicadores.ObterTotalIAC(DatInicio, DatFim);
			TotalProduzido.PlanIAC = daoIndicadores.ObterPlano("IAC", DatInicio, DatFim);
			TotalProduzido.RealIMC = daoIndicadores.ObterTotalEmbalado("IMC", DatInicio, DatFim);
			TotalProduzido.PlanIMC = daoIndicadores.ObterPlano("IMC", DatInicio, DatFim);
			TotalProduzido.RealMF = daoIndicadores.ObterTotalEmbalado("FEC", DatInicio, DatFim);
			TotalProduzido.PlanMF = daoIndicadores.ObterPlano("FEC", DatInicio, DatFim);

			// Calcular a eficiência com base na produção encontrada (Realizado / Programa)
			Eficacia.RealIAC = (TotalProduzido.PlanIAC == 0) ? 0 : (TotalProduzido.RealIAC / TotalProduzido.PlanIAC) * 100;
			Eficacia.RealIAC = Math.Round(Eficacia.RealIAC, 2);
			Eficacia.PlanIAC = daoIndicadores.ObterPadraoIndicador(2, "IAC");
			Eficacia.RealIMC = (TotalProduzido.PlanIMC == 0) ? 0 : (TotalProduzido.RealIMC / TotalProduzido.PlanIMC) * 100;
			Eficacia.RealIMC = Math.Round(Eficacia.RealIMC, 2);
			Eficacia.PlanIMC = daoIndicadores.ObterPadraoIndicador(2, "IMC");
			Eficacia.RealMF = (TotalProduzido.PlanMF == 0) ? 0 : (TotalProduzido.RealMF / TotalProduzido.PlanMF) * 100;
			Eficacia.RealMF = Math.Round(Eficacia.RealMF, 2);
			Eficacia.PlanMF = daoIndicadores.ObterPadraoIndicador(2, "FEC");

			// Encontrar os padrões de indicadores de defeito, também para calcular a quantidade de defeitos
			IndiceDefeito.PlanIAC = daoIndicadores.ObterPadraoIndicador(4, "IAC");
			IndiceDefeito.PlanIMC = daoIndicadores.ObterPadraoIndicador(4, "IMC");
			IndiceDefeito.PlanMF = daoIndicadores.ObterPadraoIndicador(4, "FEC");

			// Obter os dados dos defeitos
			Defeitos.RealIAC = daoIndicadores.ObterTotalDefeito("IAC", DatInicio, DatFim);
			Defeitos.RealIMC = daoIndicadores.ObterTotalDefeito("IMC", DatInicio, DatFim);
			Defeitos.RealMF = daoIndicadores.ObterTotalDefeito("FEC", DatInicio, DatFim);

			// Calcular a quantidade de defeitos padrão (alvo)
			Defeitos.PlanIAC = (TotalProduzido.PlanIAC == 0) ? 0 : Math.Round((IndiceDefeito.PlanIAC / 100) * TotalProduzido.PlanIAC, 0);
			Defeitos.PlanIMC = (TotalProduzido.PlanIMC == 0) ? 0 : Math.Round((IndiceDefeito.PlanIMC / 100) * TotalProduzido.PlanIMC, 0);
			Defeitos.PlanMF = (TotalProduzido.PlanMF == 0) ? 0 : Math.Round((IndiceDefeito.PlanMF / 100) * TotalProduzido.PlanMF, 0);

			// Calcular o índice de defeitos na linha
			IndiceDefeito.RealIAC = (TotalProduzido.RealIAC == 0) ? 0 : (Defeitos.RealIAC / TotalProduzido.RealIAC) * 100;
			IndiceDefeito.RealIAC = Math.Round(IndiceDefeito.RealIAC, 2);
			IndiceDefeito.RealIMC = (TotalProduzido.RealIMC == 0) ? 0 : (Defeitos.RealIMC / TotalProduzido.RealIMC) * 100;
			IndiceDefeito.RealIMC = Math.Round(IndiceDefeito.RealIMC, 2);
			IndiceDefeito.RealMF = (TotalProduzido.RealMF == 0) ? 0 : (Defeitos.RealMF / TotalProduzido.RealMF) * 100;
			IndiceDefeito.RealMF = Math.Round(IndiceDefeito.RealMF, 2);

			// Encontrar os padrões de indicadores de OEE
			//IndOEE.PlanIAC = daoIndicadores.ObterPadraoIndicador(5, "IAC");
			//IndOEE.PlanIMC = daoIndicadores.ObterPadraoIndicador(5, "IMC");
			//IndOEE.PlanMF = daoIndicadores.ObterPadraoIndicador(5, "FEC");

			// Incluir no objeto de retorno
			indicador.Add(TotalProduzido);
			indicador.Add(Eficacia);
			indicador.Add(Defeitos);
			indicador.Add(IndiceDefeito);
			//indicador.Add(IndOEE);

			return indicador;

		}

		public static List<IndicadorMF> ObterIndicadorMF(DateTime DatInicio, DateTime DatFim, string Fabrica, string Familia)
		{

			var linhas = daoIndicadores.GetLinhas(Familia, Fabrica);

			List<IndicadorMF> indicador = new List<IndicadorMF>();

			IndicadorMF TotalProduzido = null;
			IndicadorMF Eficacia = null;
			IndicadorMF Defeitos = null;
			IndicadorMF IndiceDefeito = null;

			foreach (var item in linhas)
			{
				TotalProduzido = new IndicadorMF(1, "Total Produzido");
				TotalProduzido.Real = daoIndicadores.ObterTotalEmbalado(DatInicio, DatFim, item.CodLinha);
				TotalProduzido.Plan = daoIndicadores.ObterPlano(Fabrica, DatInicio, DatFim, daoGeral.ObterLinhaCliente(item.CodLinha));
				TotalProduzido.CodLinha = item.CodLinha.ToString();
				indicador.Add(TotalProduzido);

				Eficacia = new IndicadorMF(2, "Eficácia (%)");
				Eficacia.Real = (TotalProduzido.Plan == 0) ? 0 : (TotalProduzido.Real / TotalProduzido.Plan) * 100;
				Eficacia.Real = Math.Round(Eficacia.Real, 2);
				Eficacia.Plan = daoIndicadores.ObterPadraoIndicador(2, Fabrica, item.CodLinha);
				Eficacia.CodLinha = item.CodLinha.ToString();
				indicador.Add(Eficacia);

				Defeitos = new IndicadorMF(3, "Total Defeitos");
				Defeitos.Real = daoIndicadores.ObterTotalDefeito(DatInicio, DatFim, item.CodLinha);
				Defeitos.CodLinha = item.CodLinha.ToString();

				IndiceDefeito = new IndicadorMF(4, "Índice Defeitos (%)");
				IndiceDefeito.Real = (TotalProduzido.Real == 0) ? 0 : (Defeitos.Real / TotalProduzido.Real) * 100;
				IndiceDefeito.Real = Math.Round(IndiceDefeito.Real, 2);
				IndiceDefeito.Plan = daoIndicadores.ObterPadraoIndicador(4, Fabrica, item.CodLinha);
				IndiceDefeito.CodLinha = item.CodLinha.ToString();
				indicador.Add(IndiceDefeito);

				Defeitos.Plan = Decimal.Round(TotalProduzido.Plan * (IndiceDefeito.Plan / 100), 2);
				indicador.Add(Defeitos);

			}

			return indicador;

		}

		public static decimal ObterPlano(String Fabrica, DateTime DatInicio, DateTime DatFim, string CodLinha)
		{
			return daoIndicadores.ObterPlano(Fabrica, DatInicio, DatFim, CodLinha);
		}

		public static List<CBLinhaDTO> GetLinhas(string Familia, string Setor)
		{
			return daoIndicadores.GetLinhas(Familia, Setor);
		}

		public static List<IndicadorPostos> ObterIndicadoresQuantidadeProduzidaPostos(DateTime DatInicio, DateTime DatFim, int CodLinha, int Turno)
		{
			return daoIndicadores.ObterIndicadoresQuantidadeProduzidaPostos(DatInicio, DatFim, CodLinha, Turno);
		}

		public static List<IndicadorPostos> ObterIndicadoresQuantidadeDefeitos(DateTime DatInicio, DateTime DatFim, int CodLinha, int Turno)
		{
			return daoIndicadores.ObterIndicadoresQuantidadeDefeitos(DatInicio, DatFim, CodLinha, Turno);
		}

		public static decimal ObterTotalDefeitoReparadosPorFabrica(DateTime DatInicio, DateTime DatFim, string Setor, string Familia)
		{
			return daoIndicadores.ObterTotalDefeitoReparadosPorFabrica(DatInicio, DatFim, Setor, Familia);
		}

		public static decimal ObterTotalDefeitoReparadosPorLinha(DateTime DatInicio, DateTime DatFim, int CodLinha)
		{
			return daoIndicadores.ObterTotalDefeitoReparadosPorLinha(DatInicio, DatFim, CodLinha);
		}

		public static decimal ObterTotalDefeitoReparadosPorPosto(DateTime DatInicio, DateTime DatFim, int CodLinha, int NumPosto)
		{
			return daoIndicadores.ObterTotalDefeitoReparadosPorPosto(DatInicio, DatFim, CodLinha, NumPosto);
		}

		public static List<CBPostoDTO> GetPostos(int CodLinha)
		{
			return daoIndicadores.GetPostos(CodLinha);
		}

		public static CBLinhaDTO GetLinhaByCodigo(int CodLinha)
		{
			return daoIndicadores.GetLinhaByCodigo(CodLinha);
		}

		public static CBHorarioDTO GetTurno(int Turno)
		{
			return daoIndicadores.GetTurno(Turno);
		}

		public static List<IndicadorPostoHoraHora> GetHorarios(int CodTurno)
		{
			List<IndicadorPostoHoraHora> horarios = new List<IndicadorPostoHoraHora>();
			CBHorarioDTO turno = daoIndicadores.GetTurno(CodTurno);
			DateTime data = DateTime.Now;

			TimeSpan time = data.TimeOfDay;

			// se o inicio for maior que o fim quer dizer que o turno começa em um dia e termina em outro dia
			if (TimeSpan.Parse(turno.HoraInicio) > TimeSpan.Parse(turno.HoraFim))
			{
				time = DateTime.Parse(data.AddDays(1).ToShortDateString() + " " + turno.HoraFim.ToString()).Subtract(DateTime.Parse(data.ToShortDateString() + " " + turno.HoraInicio.ToString()));
			}
			else
			{
				time = DateTime.Parse(data.ToShortDateString() + " " + turno.HoraFim.ToString()).Subtract(DateTime.Parse(data.ToShortDateString() + " " + turno.HoraInicio.ToString()));
			}


			int hora = 0;
			int total = (int)Math.Round(time.TotalHours);
			int horasInicial = DateTime.Parse(data.ToShortDateString() + " " + turno.HoraInicio.ToString()).TimeOfDay.Hours;
			int horasFinal = DateTime.Parse(data.ToShortDateString() + " " + turno.HoraFim.ToString()).TimeOfDay.Hours;

			for (int i = 0; i <= total; i++)
			{
				hora = horasInicial + i;

				if (hora == 24)
				{
					for (int j = 0; j <= horasFinal; j++)
					{
						hora = j;

						horarios.Add(new IndicadorPostoHoraHora
						{
							Hora = new TimeSpan(hora, 0, 0).ToString() + " - " + new TimeSpan(hora + 1, 0, 0).ToString(),
							HoraInicio = new TimeSpan(hora, hora == horasInicial ? TimeSpan.Parse(turno.HoraInicio).Minutes : 0, 0),
							HoraFim = new TimeSpan(hora == horasFinal ? horasFinal : hora + 1, hora == horasFinal ? TimeSpan.Parse(turno.HoraFim).Minutes : 0, 0)
						});
					}
					break;
				}
				else
				{
					horarios.Add(new IndicadorPostoHoraHora
					{
						Hora = new TimeSpan(hora, 0, 0).ToString() + " - " + new TimeSpan(hora == 23 ? 0 : hora + 1, 0, 0).ToString(),
						HoraInicio = new TimeSpan(hora, hora == horasInicial ? TimeSpan.Parse(turno.HoraInicio).Minutes : 0, 0),
						HoraFim = hora == 23 ?
						new TimeSpan(hora == horasFinal ? horasFinal : 23, hora == horasFinal ? TimeSpan.Parse(turno.HoraFim).Minutes : 59, 59)
						:
						new TimeSpan(hora == horasFinal ? horasFinal : hora + 1, hora == horasFinal ? TimeSpan.Parse(turno.HoraFim).Minutes : 0, 0)
					});
				}
			}

			return horarios;
		}

		public static decimal GetQuantidadeProduzidaPostoEHora(
			DateTime DataInicio,
			DateTime DataFim,
			TimeSpan HoraInicio,
			TimeSpan HoraFim,
			int NumPosto,
			int Turno,
			int CodLinha)
		{
			return daoIndicadores.GetQuantidadeProduzidaPostoEHora(DataInicio, DataFim, HoraInicio, HoraFim, NumPosto, Turno, CodLinha);
		}

		public static decimal GetQuantidadeDefeitosPostoEHora(
			DateTime DataInicio,
			DateTime DataFim,
			TimeSpan HoraInicio,
			TimeSpan HoraFim,
			int NumPosto,
			int Turno,
			int CodLinha)
		{
			return daoIndicadores.GetQuantidadeDefeitosPostoEHora(DataInicio, DataFim, HoraInicio, HoraFim, NumPosto, Turno, CodLinha);
		}

		public static decimal ObterPadraoIndicadorFabrica(int Indicador, string Fabrica)
		{
			return daoIndicadores.ObterPadraoIndicador(Indicador, Fabrica);
		}

		public static decimal ObterPadraoIndicador(int Indicador, String Fabrica, int CodLinha)
		{
			return daoIndicadores.ObterPadraoIndicador(Indicador, Fabrica, CodLinha);
		}

		public static List<viw_CBDefeitoTurnoDTO> GetDefeitos(DateTime DatInicio, DateTime DatFim, string Fabrica, string Familia, int? CodLinha, int? NumPosto)
		{
			var defeitos = daoIndicadores.GetDefeitos(DatInicio, DatFim, Fabrica, Familia, CodLinha, NumPosto);

			defeitos.ForEach(x => x.DesDefeito = daoDefeitos.ObterDesDefeito(x.CodDefeito));
			defeitos.ForEach(x => x.DesCausa = daoDefeitos.ObterDesCausa(x.CodCausa));
			defeitos.ForEach(x => x.DesOrigem = !String.IsNullOrEmpty(x.CodOrigem) ? daoDefeitos.ObterDesOrigem(x.CodOrigem.Trim().PadLeft(2, '0')) : "");
			return defeitos;
		}

		public static decimal ObterTotalEmbalado(String Fabrica, DateTime DatInicio, DateTime DatFim)
		{
			return daoIndicadores.ObterTotalEmbalado(Fabrica, DatInicio, DatFim);
		}

		public static decimal ObterTotalIAC(DateTime datInicio, DateTime datFim)
		{
			return daoIndicadores.ObterTotalIAC(datInicio, datInicio);
		}
	}
}
