﻿using SEMP.DAL.DTB_SSA.Models;
using SEMP.DAL.IDW;
using SEMP.DAL.Models;

namespace SEMP.DAL
{
	public class Acesso
	{
		private static Acesso _instance;

		private Acesso() {}

		public static Acesso Instance
		{
			get { return _instance ?? (_instance = new Acesso()); }
		}

        
		public readonly DTB_SIMContext dtb_SIM = new DTB_SIMContext();
		public readonly DTB_CBContext dtb_CB = new DTB_CBContext();
		public readonly IDWContext IDW = new IDWContext();
        public readonly DtbSsaContext DtbSsa = new DtbSsaContext();
        public readonly DtbEngContext DtbEng = new DtbEngContext();


		public void Commit() { }
	}
}
