using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBPostoDefeito
    {
        public int CodLinha { get; set; }
        public int NumPosto { get; set; }
        public string CodDefeito { get; set; }
    }
}
