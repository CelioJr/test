namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_SYS
    {
        [Key]
        public int COD_SYS { get; set; }

        public int? SESSAO { get; set; }

        public DateTime? ALIVE { get; set; }

        [StringLength(50)]
        public string PATH_IMPORTATION_BIN { get; set; }

        [StringLength(50)]
        public string BACKUP_IMPORTATION_BIN { get; set; }

        [StringLength(100)]
        public string PATH_IMEIS_VENDIDOS { get; set; }

        [StringLength(100)]
        public string PATH_IMEIS_VENDIDOS_BACKUP { get; set; }
    }
}
