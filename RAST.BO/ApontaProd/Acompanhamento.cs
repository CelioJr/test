﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.Apontaprod;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.DTO;
using SEMP.Model.VO.Rastreabilidade.Relatorios;

namespace RAST.BO.Relatorios
{
	public class Acompanhamento
	{

		public static List<LeiturasMF> ObterLeituras(DateTime DatIni, DateTime DatFim, String CodLinha, String Codigo,
			String NumSerie, String Tipo)
		{
			DatFim = DateTime.Parse(DatFim.AddDays(1).ToShortDateString() + " 02:20");

			if (!String.IsNullOrEmpty(Codigo) && !String.IsNullOrEmpty(NumSerie))
			{
				switch (Tipo)
				{
					case "acessorio":
						return daoAcompanhamento.ObterLeiturasPorAcessorio(Codigo, NumSerie);
					case "painel":
						return daoAcompanhamento.ObterLeiturasPorPainel(Codigo, NumSerie);
					case "outro":
						return daoAcompanhamento.ObterLeiturasPorOutro(Codigo, NumSerie);
					default:
						return daoAcompanhamento.ObterLeiturasPorModelo(Codigo, NumSerie);
				}
			}
			else if (!String.IsNullOrEmpty(CodLinha) && String.IsNullOrEmpty(Codigo))
			{
				return daoAcompanhamento.ObterLeituras(DatIni, DatFim, CodLinha);
			}
			else if (!String.IsNullOrEmpty(CodLinha) && !String.IsNullOrEmpty(Codigo))
			{
				return daoAcompanhamento.ObterLeituras(DatIni, DatFim, CodLinha, Codigo);
			}
			else
			{
				return daoAcompanhamento.ObterLeituras(DatIni, DatFim);
			}
		}

		public static List<LeiturasMF> ObterLeituras(string codFab, string numAP, DateTime? Datinicio, DateTime? DatFim, String CodLinha) {

			return daoAcompanhamento.ObterLeituras(codFab, numAP, Datinicio, DatFim, CodLinha);
		}

		public static List<LeiturasMF> ObterLeiturasIMC(DateTime DatIni, DateTime DatFim, String CodLinha, String Codigo,
			String NumSerie)
		{
			DatFim = DateTime.Parse(DatFim.AddDays(1).ToShortDateString() + " 02:20");

			if (!String.IsNullOrEmpty(Codigo) && !String.IsNullOrEmpty(NumSerie))
			{
				return daoAcompanhamento.ObterLeiturasPorModeloIMC(Codigo, NumSerie);
			}
			else if (!String.IsNullOrEmpty(CodLinha) && String.IsNullOrEmpty(Codigo))
			{
				return daoAcompanhamento.ObterLeiturasIMC(DatIni, DatFim, CodLinha);
			}
			else if (!String.IsNullOrEmpty(CodLinha) && !String.IsNullOrEmpty(Codigo))
			{
				return daoAcompanhamento.ObterLeiturasIMC(DatIni, DatFim, CodLinha, Codigo);
			}
			else
			{
				return daoAcompanhamento.ObterLeiturasIMC(DatIni, DatFim);
			}
		}

		public static List<LeiturasMF> ObterLeiturasDAT(DateTime DatIni, DateTime DatFim, String CodLinha, String Codigo,
			String NumSerie)
		{
			DatFim = DateTime.Parse(DatFim.AddDays(1).ToShortDateString() + " 02:20");

			if (!String.IsNullOrEmpty(Codigo) && !String.IsNullOrEmpty(NumSerie))
			{
				return daoAcompanhamento.ObterLeiturasPorModeloDAT(Codigo, NumSerie);
			}
			else if (!String.IsNullOrEmpty(CodLinha) && String.IsNullOrEmpty(Codigo))
			{
				return daoAcompanhamento.ObterLeiturasDAT(DatIni, DatFim, CodLinha);
			}
			else if (!String.IsNullOrEmpty(CodLinha) && !String.IsNullOrEmpty(Codigo))
			{
				return daoAcompanhamento.ObterLeiturasDAT(DatIni, DatFim, CodLinha, Codigo);
			}
			else
			{
				return daoAcompanhamento.ObterLeiturasDAT(DatIni, DatFim);
			}
		}

		public static List<viw_BarrasSAPContingenciaDTO> ObterLeiturasSAP(DateTime Datinicio, DateTime DatFim)
		{

			return daoAcompanhamento.ObterLeiturasSAP(Datinicio, DatFim);
		}


	}
}
