using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
	public partial class tbl_CBTransfLote
	{
		public int CodLinhaOri { get; set; }
		public int NumPostoOri { get; set; }
		public string NumLote { get; set; }
		public string CodDRT { get; set; }
		public int FabDestino { get; set; }
		public int CodLinhaDest { get; set; }
		public Nullable<System.DateTime> DatTransf { get; set; }
		public Nullable<System.DateTime> DatRecebimento { get; set; }
		public string CodFab { get; set; }
		public string NumAP { get; set; }
	}
}
