namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class v_perdacomponente_semiacabado
    {
        [StringLength(30)]
        public string maquina { get; set; }

        [Key]
        public DateTime dthr { get; set; }

        [StringLength(30)]
        public string cd_produto { get; set; }

        [StringLength(30)]
        public string componente { get; set; }

        public decimal? qtde_perdida { get; set; }

        public decimal? custo { get; set; }
    }
}
