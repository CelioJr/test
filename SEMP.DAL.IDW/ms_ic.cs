namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ms_ic
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ms_ic()
        {
            ms_msicup = new HashSet<ms_msicup>();
        }

        [Key]
        public long id_ic { get; set; }

        [Required]
        [StringLength(30)]
        public string cd_ic { get; set; }

        [StringLength(100)]
        public string ds_ic { get; set; }

        public int revisao { get; set; }

        public DateTime? dthr_stativo { get; set; }

        public DateTime? dthr_revisao { get; set; }

        public int? st_ativo { get; set; }

        [StringLength(100)]
        public string url_conexao { get; set; }

        public int? tp_ic { get; set; }

        public DateTime? dthr_heartbeat { get; set; }

        public long id_usr { get; set; }

        public int? tp_icregistraperdaconexao { get; set; }

        [StringLength(100)]
        public string firmware { get; set; }

        public long? id_perfilandon { get; set; }

        public virtual ms_perfilandon ms_perfilandon { get; set; }

        public virtual ms_usr ms_usr { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_msicup> ms_msicup { get; set; }
    }
}
