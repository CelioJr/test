namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_ProdDia
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(2)]
        public string CodFab { get; set; }

        public int? CodLinha { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CodModelo { get; set; }

        [StringLength(10)]
        public string DatProducao { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(1)]
        public string CodCin { get; set; }

        public int? QtdProduzida { get; set; }

        public int? QtdApontada { get; set; }

        public int? QtdDivida { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(1)]
        public string FlagTP { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NumTurno { get; set; }

        public Guid? rowguid { get; set; }

        [StringLength(20)]
        public string NumAP { get; set; }

        public DateTime? DatInicio { get; set; }

        public DateTime? DatFim { get; set; }
    }
}
