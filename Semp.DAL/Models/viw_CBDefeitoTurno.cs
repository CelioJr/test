using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class viw_CBDefeitoTurno
    {
        public string NumECB { get; set; }
        public int CodLinha { get; set; }
        public int NumPosto { get; set; }
        public string CodDefeito { get; set; }
        public Nullable<int> FlgRevisado { get; set; }
        public string CodCausa { get; set; }
        public string Posicao { get; set; }
        public string FlgTipo { get; set; }
        public string CodOrigem { get; set; }
        public string DesAcao { get; set; }
        public System.DateTime DatEvento { get; set; }
        public Nullable<System.DateTime> DatRevisado { get; set; }
        public Nullable<System.DateTime> DatEntRevisao { get; set; }
        public string NomUsuario { get; set; }
        public string NumIP { get; set; }
        public Nullable<int> CodRma { get; set; }
        public Nullable<int> NumPostoRevisado { get; set; }
        public string CodDRT { get; set; }
        public string CodDRTRevisor { get; set; }
        public string CodDefeitoTest { get; set; }
        public string ObsDefeito { get; set; }
        public string DefIncorreto { get; set; }
        public string CodDRTTest { get; set; }
        public string NomUsuarioTest { get; set; }
        public string NumSeriePai { get; set; }
        public Nullable<int> CodLinhaRevisado { get; set; }
	    public string Romaneio { get; set; }
	    public string CodItem { get; set; } 
        public Nullable<System.DateTime> DatReferencia { get; set; }
        public Nullable<int> Turno { get; set; }
		public Nullable<int> CodSubDefeito { get; set; }


    }
}
