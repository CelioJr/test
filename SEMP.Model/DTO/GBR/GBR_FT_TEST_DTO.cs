using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_FT_TEST_DTO
    {
        public int COD_FT { get; set; }
        public string SERIAL { get; set; }
        public Nullable<bool> APROVADO { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
        public string ARQUIVO { get; set; }
    }
}
