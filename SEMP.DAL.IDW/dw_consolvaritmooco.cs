namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolvaritmooco
    {
        [Key]
        public long id_consolvartimooco { get; set; }

        public long id_consolvaritmo { get; set; }

        public DateTime? dthr_ivaritmo { get; set; }

        public DateTime? dthr_fvaritmo { get; set; }

        public long id_consolvaritmolog { get; set; }

        public virtual dw_consolvaritmo dw_consolvaritmo { get; set; }

        public virtual dw_consolvaritmolog dw_consolvaritmolog { get; set; }
    }
}
