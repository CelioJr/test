namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_LogsServico
    {
        public int id { get; set; }

        [StringLength(50)]
        public string NomPrograma { get; set; }

        public DateTime? DatExecucao { get; set; }

        [Column(TypeName = "text")]
        public string Mensagem { get; set; }
    }
}
