﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.DTO;

namespace RAST.BO.Relatorios
{
    public class RelatorioMonitorOperadorBO
    {
        public static List<MonitorOperadorDTO> ListaQtdLiberada(DateTime dataIni, DateTime dataFim, int linha, int posto)
        {
            return daoMonitorOperador.ListaQtdLiberada(dataIni, dataFim, linha, posto);
        }
    }
}
