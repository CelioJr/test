using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DOC_INTERFACE_DTO
    {
        public int DOC_CODIGO { get; set; }
        public string INT_DESCRICAO { get; set; }
        public string INT_TIPO { get; set; }
        public virtual FR_DOC_PRINCIPAL_DTO FR_DOC_PRINCIPAL { get; set; }
    }
}
