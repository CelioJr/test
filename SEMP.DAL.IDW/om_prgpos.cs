namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_prgpos
    {
        [Key]
        public long id_prgpos { get; set; }

        public long id_pa { get; set; }

        public long id_produto { get; set; }

        [StringLength(100)]
        public string scantrack { get; set; }

        [StringLength(100)]
        public string feedertrack { get; set; }

        [StringLength(100)]
        public string pocket { get; set; }

        [StringLength(100)]
        public string name { get; set; }

        [StringLength(100)]
        public string feedertable { get; set; }

        public long id_prg { get; set; }

        public decimal? posicao { get; set; }

        public int? ordem { get; set; }

        public long? id_rapTipoMesa { get; set; }

        public long? id_rapTipoFeeder { get; set; }

        public decimal? qt_usada { get; set; }

        public virtual dw_rap dw_rap { get; set; }

        public virtual dw_rap dw_rap1 { get; set; }

        public virtual om_pa om_pa { get; set; }

        public virtual om_prg om_prg { get; set; }

        public virtual om_produto om_produto { get; set; }
    }
}
