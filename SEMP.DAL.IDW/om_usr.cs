namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_usr
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_usr()
        {
            dw_cal = new HashSet<dw_cal>();
            dw_cal1 = new HashSet<dw_cal>();
            dw_consolallog = new HashSet<dw_consolallog>();
            dw_consolciplog = new HashSet<dw_consolciplog>();
            dw_consolciplog1 = new HashSet<dw_consolciplog>();
            dw_consolestlocalprotemp = new HashSet<dw_consolestlocalprotemp>();
            dw_consollog = new HashSet<dw_consollog>();
            dw_consolmo = new HashSet<dw_consolmo>();
            dw_consolmolog = new HashSet<dw_consolmolog>();
            dw_consolvaritmologcau = new HashSet<dw_consolvaritmologcau>();
            dw_est = new HashSet<dw_est>();
            dw_est1 = new HashSet<dw_est>();
            dw_estlocal = new HashSet<dw_estlocal>();
            dw_estlocal1 = new HashSet<dw_estlocal>();
            dw_estmov = new HashSet<dw_estmov>();
            dw_expcvs = new HashSet<dw_expcvs>();
            dw_expcvs1 = new HashSet<dw_expcvs>();
            dw_folha = new HashSet<dw_folha>();
            dw_folha1 = new HashSet<dw_folha>();
            dw_ft_etapa = new HashSet<dw_ft_etapa>();
            dw_ft_etapa1 = new HashSet<dw_ft_etapa>();
            dw_ft_grupo = new HashSet<dw_ft_grupo>();
            dw_ft_grupo1 = new HashSet<dw_ft_grupo>();
            dw_grpativ = new HashSet<dw_grpativ>();
            dw_grpativ1 = new HashSet<dw_grpativ>();
            dw_grupo_ferramenta = new HashSet<dw_grupo_ferramenta>();
            dw_grupo_ferramenta1 = new HashSet<dw_grupo_ferramenta>();
            dw_macrange = new HashSet<dw_macrange>();
            dw_nserieobs = new HashSet<dw_nserieobs>();
            dw_operacao = new HashSet<dw_operacao>();
            dw_operacao1 = new HashSet<dw_operacao>();
            dw_passagem = new HashSet<dw_passagem>();
            dw_passagem1 = new HashSet<dw_passagem>();
            dw_procedimento = new HashSet<dw_procedimento>();
            dw_procedimento1 = new HashSet<dw_procedimento>();
            dw_proreaativ = new HashSet<dw_proreaativ>();
            dw_proreausr = new HashSet<dw_proreausr>();
            dw_rap = new HashSet<dw_rap>();
            dw_rap1 = new HashSet<dw_rap>();
            dw_rota = new HashSet<dw_rota>();
            dw_rota1 = new HashSet<dw_rota>();
            dw_t_acao = new HashSet<dw_t_acao>();
            dw_t_acao1 = new HashSet<dw_t_acao>();
            dw_t_alerta = new HashSet<dw_t_alerta>();
            dw_t_alerta1 = new HashSet<dw_t_alerta>();
            dw_t_defeito = new HashSet<dw_t_defeito>();
            dw_t_defeito1 = new HashSet<dw_t_defeito>();
            dw_t_parada = new HashSet<dw_t_parada>();
            dw_t_parada1 = new HashSet<dw_t_parada>();
            dw_t_refugo = new HashSet<dw_t_refugo>();
            dw_t_refugo1 = new HashSet<dw_t_refugo>();
            dw_t_ritmo = new HashSet<dw_t_ritmo>();
            dw_t_ritmo1 = new HashSet<dw_t_ritmo>();
            dw_turno = new HashSet<dw_turno>();
            dw_turno1 = new HashSet<dw_turno>();
            ip_balanceamento = new HashSet<ip_balanceamento>();
            ip_balanceamento1 = new HashSet<ip_balanceamento>();
            ip_balanceamento2 = new HashSet<ip_balanceamento>();
            ms_detector = new HashSet<ms_detector>();
            ms_detector1 = new HashSet<ms_detector>();
            ms_pt_coleta = new HashSet<ms_pt_coleta>();
            ms_pt_coleta1 = new HashSet<ms_pt_coleta>();
            ms_pt_coleta2 = new HashSet<ms_pt_coleta>();
            ms_pt_coleta_login = new HashSet<ms_pt_coleta_login>();
            om_alim = new HashSet<om_alim>();
            om_alimrea = new HashSet<om_alimrea>();
            om_cargo = new HashSet<om_cargo>();
            om_cargo1 = new HashSet<om_cargo>();
            om_cc = new HashSet<om_cc>();
            om_cc1 = new HashSet<om_cc>();
            om_cfg = new HashSet<om_cfg>();
            om_cfg1 = new HashSet<om_cfg>();
            om_cfg2 = new HashSet<om_cfg>();
            om_cfgabc = new HashSet<om_cfgabc>();
            om_cfgabc1 = new HashSet<om_cfgabc>();
            om_clp = new HashSet<om_clp>();
            om_clp1 = new HashSet<om_clp>();
            om_for = new HashSet<om_for>();
            om_for1 = new HashSet<om_for>();
            om_gt = new HashSet<om_gt>();
            om_gt1 = new HashSet<om_gt>();
            om_homogt = new HashSet<om_homogt>();
            om_homogt1 = new HashSet<om_homogt>();
            om_homopt = new HashSet<om_homopt>();
            om_homopt1 = new HashSet<om_homopt>();
            om_im = new HashSet<om_im>();
            om_im1 = new HashSet<om_im>();
            om_img = new HashSet<om_img>();
            om_img1 = new HashSet<om_img>();
            om_job = new HashSet<om_job>();
            om_job1 = new HashSet<om_job>();
            om_mapa = new HashSet<om_mapa>();
            om_mapa1 = new HashSet<om_mapa>();
            om_pa = new HashSet<om_pa>();
            om_pa1 = new HashSet<om_pa>();
            om_produto = new HashSet<om_produto>();
            om_produto1 = new HashSet<om_produto>();
            om_progrp = new HashSet<om_progrp>();
            om_progrp1 = new HashSet<om_progrp>();
            om_pt = new HashSet<om_pt>();
            om_pt1 = new HashSet<om_pt>();
            om_regras_nscb = new HashSet<om_regras_nscb>();
            om_regras_nscb1 = new HashSet<om_regras_nscb>();
            om_tpgt = new HashSet<om_tpgt>();
            om_tpgt1 = new HashSet<om_tpgt>();
            om_tppt = new HashSet<om_tppt>();
            om_tppt1 = new HashSet<om_tppt>();
            om_unidmedida = new HashSet<om_unidmedida>();
            om_unidmedida1 = new HashSet<om_unidmedida>();
            om_usr1 = new HashSet<om_usr>();
            om_usr11 = new HashSet<om_usr>();
            pp_cliente = new HashSet<pp_cliente>();
            pp_cliente1 = new HashSet<pp_cliente>();
            pp_cm = new HashSet<pp_cm>();
            pp_cm1 = new HashSet<pp_cm>();
            pp_cp = new HashSet<pp_cp>();
            pp_cp1 = new HashSet<pp_cp>();
            pp_indisp = new HashSet<pp_indisp>();
            pp_indisp1 = new HashSet<pp_indisp>();
            pp_nec = new HashSet<pp_nec>();
            pp_nec1 = new HashSet<pp_nec>();
            pp_necimp = new HashSet<pp_necimp>();
            pp_necimp1 = new HashSet<pp_necimp>();
            pp_necimplog = new HashSet<pp_necimplog>();
            pp_plano = new HashSet<pp_plano>();
            pp_plano1 = new HashSet<pp_plano>();
            te_concessionaria = new HashSet<te_concessionaria>();
            te_concessionaria1 = new HashSet<te_concessionaria>();
            te_tipo_consumidor = new HashSet<te_tipo_consumidor>();
            te_tipo_consumidor1 = new HashSet<te_tipo_consumidor>();
        }

        [Key]
        public long id_usr { get; set; }

        public long? id_usrrevisao { get; set; }

        public long? id_usrstativo { get; set; }

        [StringLength(30)]
        public string cd_usr { get; set; }

        public int? revisao { get; set; }

        public DateTime? dt_revisao { get; set; }

        public DateTime? dt_stativo { get; set; }

        public short? st_ativo { get; set; }

        [StringLength(30)]
        public string login { get; set; }

        [StringLength(100)]
        public string senha { get; set; }

        [StringLength(100)]
        public string ds_nome { get; set; }

        public long id_usrgrp { get; set; }

        [StringLength(10)]
        public string ds_apelido { get; set; }

        public long? id_cc { get; set; }

        public long? id_cargo { get; set; }

        [StringLength(255)]
        public string url_sms { get; set; }

        [StringLength(255)]
        public string url_email { get; set; }

        public long? id_gt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_cal> dw_cal { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_cal> dw_cal1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolallog> dw_consolallog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolciplog> dw_consolciplog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolciplog> dw_consolciplog1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolestlocalprotemp> dw_consolestlocalprotemp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consollog> dw_consollog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolmo> dw_consolmo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolmolog> dw_consolmolog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolvaritmologcau> dw_consolvaritmologcau { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_est> dw_est { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_est> dw_est1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_estlocal> dw_estlocal { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_estlocal> dw_estlocal1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_estmov> dw_estmov { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_expcvs> dw_expcvs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_expcvs> dw_expcvs1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folha> dw_folha { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folha> dw_folha1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_ft_etapa> dw_ft_etapa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_ft_etapa> dw_ft_etapa1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_ft_grupo> dw_ft_grupo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_ft_grupo> dw_ft_grupo1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_grpativ> dw_grpativ { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_grpativ> dw_grpativ1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_grupo_ferramenta> dw_grupo_ferramenta { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_grupo_ferramenta> dw_grupo_ferramenta1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_macrange> dw_macrange { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_nserieobs> dw_nserieobs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_operacao> dw_operacao { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_operacao> dw_operacao1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_passagem> dw_passagem { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_passagem> dw_passagem1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_procedimento> dw_procedimento { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_procedimento> dw_procedimento1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_proreaativ> dw_proreaativ { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_proreausr> dw_proreausr { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rap> dw_rap { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rap> dw_rap1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rota> dw_rota { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rota> dw_rota1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_t_acao> dw_t_acao { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_t_acao> dw_t_acao1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_t_alerta> dw_t_alerta { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_t_alerta> dw_t_alerta1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_t_defeito> dw_t_defeito { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_t_defeito> dw_t_defeito1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_t_parada> dw_t_parada { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_t_parada> dw_t_parada1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_t_refugo> dw_t_refugo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_t_refugo> dw_t_refugo1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_t_ritmo> dw_t_ritmo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_t_ritmo> dw_t_ritmo1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_turno> dw_turno { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_turno> dw_turno1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ip_balanceamento> ip_balanceamento { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ip_balanceamento> ip_balanceamento1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ip_balanceamento> ip_balanceamento2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_detector> ms_detector { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_detector> ms_detector1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_pt_coleta> ms_pt_coleta { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_pt_coleta> ms_pt_coleta1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_pt_coleta> ms_pt_coleta2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_pt_coleta_login> ms_pt_coleta_login { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_alim> om_alim { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_alimrea> om_alimrea { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cargo> om_cargo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cargo> om_cargo1 { get; set; }

        public virtual om_cargo om_cargo2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cc> om_cc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cc> om_cc1 { get; set; }

        public virtual om_cc om_cc2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfgabc> om_cfgabc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfgabc> om_cfgabc1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_clp> om_clp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_clp> om_clp1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_for> om_for { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_for> om_for1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_gt> om_gt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_gt> om_gt1 { get; set; }

        public virtual om_gt om_gt2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_homogt> om_homogt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_homogt> om_homogt1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_homopt> om_homopt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_homopt> om_homopt1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_im> om_im { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_im> om_im1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_img> om_img { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_img> om_img1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_job> om_job { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_job> om_job1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_mapa> om_mapa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_mapa> om_mapa1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_pa> om_pa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_pa> om_pa1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_produto> om_produto { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_produto> om_produto1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_progrp> om_progrp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_progrp> om_progrp1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_pt> om_pt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_pt> om_pt1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_regras_nscb> om_regras_nscb { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_regras_nscb> om_regras_nscb1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_tpgt> om_tpgt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_tpgt> om_tpgt1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_tppt> om_tppt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_tppt> om_tppt1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_unidmedida> om_unidmedida { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_unidmedida> om_unidmedida1 { get; set; }

        public virtual om_usrgrp om_usrgrp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_usr> om_usr1 { get; set; }

        public virtual om_usr om_usr2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_usr> om_usr11 { get; set; }

        public virtual om_usr om_usr3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cliente> pp_cliente { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cliente> pp_cliente1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cm> pp_cm { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cm> pp_cm1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cp> pp_cp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cp> pp_cp1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_indisp> pp_indisp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_indisp> pp_indisp1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_nec> pp_nec { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_nec> pp_nec1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_necimp> pp_necimp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_necimp> pp_necimp1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_necimplog> pp_necimplog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_plano> pp_plano { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_plano> pp_plano1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<te_concessionaria> te_concessionaria { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<te_concessionaria> te_concessionaria1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<te_tipo_consumidor> te_tipo_consumidor { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<te_tipo_consumidor> te_tipo_consumidor1 { get; set; }
    }
}
