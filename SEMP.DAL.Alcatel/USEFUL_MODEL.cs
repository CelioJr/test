namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.USEFUL_MODEL")]
    public partial class USEFUL_MODEL
    {
        public long? ID { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int COD_SKU { get; set; }

        public int? COD_MODELO { get; set; }

        [StringLength(20)]
        public string SKU { get; set; }

        [StringLength(13)]
        public string EAN { get; set; }

        [StringLength(20)]
        public string MODEL { get; set; }

        [StringLength(13)]
        public string MODEL_NAME { get; set; }

        public int? QTY_BOX { get; set; }

        public int? QTY_PALLET { get; set; }

        public int? QTY_LOT { get; set; }
    }
}
