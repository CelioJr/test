using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBIdentifica
    {
        public string NumECB { get; set; }
        public string CodPlacaIAC { get; set; }
        public string CodPlacaIMC { get; set; }
        public string CodModelo { get; set; }
        public Nullable<System.DateTime> DatIdentifica { get; set; }
        public Nullable<System.DateTime> DatIMC { get; set; }
        public Nullable<System.DateTime> DatModelo { get; set; }
        public string CodLinhaIAC { get; set; }
        public string CodLinhaIMC { get; set; }
        public string CodLinhaFEC { get; set; }
        public string MacAddress { get; set; }
    }
}
