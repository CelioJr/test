using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBPerfilAmarraMap : EntityTypeConfiguration<tbl_CBPerfilAmarra>
    {
        public tbl_CBPerfilAmarraMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodLinha, t.NumPosto, t.CodModelo, t.CodTipoAmarra, t.CodItem });

            // Properties
            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumPosto)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodTipoAmarra)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_CBPerfilAmarra");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.NumPosto).HasColumnName("NumPosto");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.CodTipoAmarra).HasColumnName("CodTipoAmarra");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
        }
    }
}
