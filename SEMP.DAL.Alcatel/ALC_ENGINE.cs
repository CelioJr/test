namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_ENGINE")]
    public partial class ALC_ENGINE
    {
        [Key]
        [StringLength(30)]
        public string SERIAL { get; set; }

        public DateTime? DT_PASS { get; set; }

        [StringLength(20)]
        public string ORDER_ENG { get; set; }

        public DateTime? DT_CHECK { get; set; }

        public int? COD_SERIAL { get; set; }

        [StringLength(20)]
        public string OP_PA { get; set; }

        [StringLength(20)]
        public string MAGAZINE { get; set; }

        public DateTime? DT_MAGAZINE { get; set; }

        [StringLength(25)]
        public string BENCH { get; set; }

        [StringLength(25)]
        public string JIG_SN { get; set; }

        public int? LINE { get; set; }

        public virtual ALC_ORDER ALC_ORDER { get; set; }

        public virtual ALC_MAGAZINE ALC_MAGAZINE { get; set; }

        public virtual ALC_ORDER ALC_ORDER1 { get; set; }
    }
}
