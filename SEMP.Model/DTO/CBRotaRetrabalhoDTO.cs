﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
	public class CBRotaRetrabalhoDTO
	{
		public int CodRetrabalho { get; set; }
		public int CodLinha { get; set; }
		public int NumPosto { get; set; }
		public string FlgUltimoPosto { get; set; }		
	}
}
