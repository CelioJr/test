using System;
using System.Collections.Generic;

namespace SEMP.DAL.DTB_SSA.Models
{
    public partial class tbl_Posto
    {
        public tbl_Posto()
        {
        }

        public int posto_id { get; set; }
        public string nome_posto { get; set; }
        public string flgCategoria { get; set; }
        public string codSigla { get; set; }
        public int? codAgrupamento { get; set; }
        public int? NumPosto { get; set; }
        public string flgTipoProduto { get; set; }
        public int? CodLinhaEmbal { get; set; }
        public string flgTipoPosto { get; set; }

    }
}
