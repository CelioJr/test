using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBLinhaModeloMap : EntityTypeConfiguration<tbl_CBLinhaModelo>
    {
        public tbl_CBLinhaModeloMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodLinha, t.CodIdentificador, t.DatIniProd });

            // Properties
            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodIdentificador)
                .IsRequired()
                .HasMaxLength(6);

            // Table & Column Mappings
            this.ToTable("tbl_CBLinhaModelo");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.CodIdentificador).HasColumnName("CodIdentificador");
            this.Property(t => t.DatIniProd).HasColumnName("DatIniProd");
            this.Property(t => t.DatFimProd).HasColumnName("DatFimProd");
            this.Property(t => t.CodOpeCriacao).HasColumnName("CodOpeCriacao");
            this.Property(t => t.DatExclusao).HasColumnName("DatExclusao");
            this.Property(t => t.CodOpeExclusao).HasColumnName("CodOpeExclusao");
            this.Property(t => t.TempoParada).HasColumnName("TempoParada");

            // Relationships
            this.HasRequired(t => t.tbl_CBLinhaMontagem)
                .WithMany(t => t.tbl_CBLinhaModelo)
                .HasForeignKey(d => d.CodLinha);

        }
    }
}
