﻿using System;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
using System.Web.Mvc;
using SEMP.Model.DTO;
using SEMP.Model.VO;
using SEMP.Model;
using SEMP.BO;
using System.Web.Routing;

namespace SEMP.Controllers
{
	/// <summary>
	/// Classe Login controla o acesso ao sistema.ssss
	/// </summary>
	[Serializable]
    public class LoginController : Controller
    {
        /// <summary>
        /// Método Index controla o acesso a página inicial do sitema.
        /// </summary>
        /// <returns>Retorna a View Index.cshtml</returns>
        public ActionResult Index()
        {
            var controle = Session["ControlePadrao"];
            //string nome = SEMP.BO.Login.ObterFuncionario("");
            //ViewBag.data = Util.GetDate();

            if (controle == null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", controle.ToString());
            }
        }

        /// <summary>
        /// Método Index controla o acesso a página inicial do sitema.
        /// </summary>
        /// <param name="username">Parâmetro username, nome do usuario.</param>
        /// <param name="password">Parâmetro password, senha do usuario.</param>
        /// <param name="returnUrl">Parâmetro returnUrl, url que será redirecionada.</param>
        /// <returns>Retorna a View Index.cshtml</returns>
        [HttpPost]
        public ActionResult Index(string username, string password, string returnUrl, string fabrica)
        {

            try
            {
				if (!String.IsNullOrEmpty(fabrica))
				{
					Session["Empresa"] = fabrica.Equals("SEMP") ? "dtb_SEMP" : "dtb_SIM";
				}

                if ("devLogin".Equals(username) && "d3vL0g1n".Equals(password))
                {
                    RetornoLogin dev = new RetornoLogin();
                    dev.CodDRT = "99999";
                    dev.Cracha = "999999999999";
                    dev.Depto = "FUCAPI";
                    dev.Cargo = "Analista";
                    dev.NomFuncionario = "Desenv Fucapi";
                    dev.NomUsuario = username;
                    dev.Status = new Mensagem();
                    dev.UsuarioCracha = false;
                    Session.Add("RetornoLogin", dev);
                    Session.Add("ControlePadrao", "Menu");

                    System.Web.Security.FormsAuthentication.SetAuthCookie(username, false);

                    return RedirectToAction("Index", "Menu");

                }
                else if (password.Equals(String.Empty))
                {
                    if (username.Length == 12)
                    {
                        username = username.Substring(0, 11);
                    }

                    RetornoLogin retorno = Login.ValidaCracha(username);

                    if (retorno != null && retorno.Status.CodMensagem == Constantes.DB_OK)
                    {
                        Session.Add("RetornoLogin", retorno);
                        Session.Add("ControlePadrao", "Coleta/Home");
                        System.Web.Security.FormsAuthentication.SetAuthCookie(username, false);

                        return RedirectToAction("Index", "Coleta/Home");
                    }
                    else
                    {
                        ViewBag.Erro = "Usuário não encontrado";
                        return View();
                    }
                }
                else
                {
                    if (FU_Login(username, password))
                    {
						RetornoLogin retorno;

						if (username.ToLower() != "consulta") { 

							retorno = Login.ValidaLogin(username);

						}
						else
						{
							retorno = new RetornoLogin(){
								CodDRT = "99999999",
								Cracha = "999999999999",
								Depto = "Consulta",
								Cargo = "Consulta",
								NomFuncionario = "Consulta",
								NomUsuario = username,
								Status = new Mensagem(),
								UsuarioCracha = false	 
							};
						}

                        //retorno.NomFuncionario = username;
                        Session.Add("RetornoLogin", retorno);
                        Session.Add("ControlePadrao", "Menu");

                        System.Web.Security.FormsAuthentication.SetAuthCookie(username, false);

                        if (!string.IsNullOrEmpty(returnUrl))
                        {
							var url = Server.UrlDecode(returnUrl.Substring(1, returnUrl.Length - 1));

							if (url.Contains("?"))
							{
								var posicao = url.IndexOf("?");
								var parametros = url.Substring(posicao + 1, url.Length - posicao - 1).Split('&');

								var values = new RouteValueDictionary();

								foreach (var param in parametros)
								{
									var dado = param.Split('=');

									values.Add(dado[0], dado[1]);
								}

								url = url.Substring(0, posicao);
								
								return RedirectToAction("Index", url, values);
							}
							else { 

								return RedirectToAction("Index", url);
							}
                        }
						/*if (SEMP.Helpers.Extensions.Permissao("NovoInicio", retorno))
						{
							return RedirectToAction("Index_Novo", "Menu");
						}*/
                        return RedirectToAction("Index", "Menu");
                    }
                    ViewBag.Erro = "Erro ao fazer login";
                    return View();
                }
            }

            catch (Exception ex)
            {
                ViewBag.Erro = "Erro no servidor: " + ex.Message;
                return View();
            }
        }

        /// <summary>
        /// Método para efetuar o login via Ajax, sem redirecionamento de página.
        /// </summary>
        /// <param name="username">Parâmetro username, nome do usuario.</param>
        /// <param name="password">Parâmetro password, senha do usuario.</param>
        /// <returns>Retorna o nome do usuário</returns>
        [HttpPost]
        public String LoginAjax(string username, string password)
        {

            try
            {

                if (password.Equals(String.Empty))
                {
                    if (username.Length == 12)
                    {
                        username = username.Substring(0, 11);
                    }

                    RetornoLogin retorno = Login.ValidaCracha(username);

                    if (retorno != null && retorno.Status.CodMensagem == Constantes.DB_OK)
                    {
                        Session.Add("RetornoLogin", retorno);
                        Session.Add("ControlePadrao", "Coleta/Home");
                        System.Web.Security.FormsAuthentication.SetAuthCookie(username, false);

                        return retorno.NomFuncionario;
                    }

                    ViewBag.Erro = "Usuário não encontrado";
                    return "";
                }
                else
                {

                    RetornoLogin retorno = Login.ValidaLogin(username);

                    Session.Add("RetornoLogin", retorno);
                    Session.Add("ControlePadrao", "Menu");

                    System.Web.Security.FormsAuthentication.SetAuthCookie(username, false);

                    return retorno.NomFuncionario;
                }

            }

            catch (Exception ex)
            {
                ViewBag.Erro = "Erro no servidor: " + ex.Message;
                return "";
            }
        }

        /// <summary>
        /// Método Logout responsável por eliminar o usuario da sessão.
        /// </summary>
        /// <returns>Retorna  View Index.cshtml</returns>
        public ActionResult Logout()
        {

            Session.RemoveAll();
            System.Web.Security.FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Login");

        }

        /// <summary>
        /// Método FU_Login
        /// </summary>
        /// <param name="Domain">Dominio</param>
        /// <param name="Username">Nome do usuario</param>
        /// <param name="Password">Senha</param>
        /// <returns></returns>
        public bool FU_Login(String Domain, String Username, String Password)
        {
            bool Success = false;
            DirectoryEntry Entry = new DirectoryEntry("LDAP://" + Domain, Username, Password);
            DirectorySearcher Searcher = new DirectorySearcher(Entry);
            Searcher.SearchScope = SearchScope.OneLevel;
            try
            {
                SearchResult Results = Searcher.FindOne();
                Success = Results != null;
            }
            catch
            {
                Success = false;
            }
            return Success;
        }

        public bool FU_Login(string username, String passorwd)
        {
            // create a "principal context" - e.g. your domain (could be machine, too)
            using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, "ST.Corp"))
            {
                // validate the credentials
                return pc.ValidateCredentials(username, passorwd, ContextOptions.ServerBind);
            }
        }

        public bool Logado()
        {

            return User.Identity.IsAuthenticated;

        }

        public bool UsuarioCracha()
        {
            if (!Logado()) return false;


            var retornologin = (RetornoLogin)Session["RetornoLogin"];
            if (retornologin == null)
            {
                return false;
            }

            return retornologin.UsuarioCracha;
        }

        public JsonResult teste(string drt)
        {
            UsuarioMobileDTO user = Login.ObterFuncionarioDRT(drt);
           
            return Json(user, JsonRequestBehavior.AllowGet);
        }

   

    }
}
