using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_GRUPO_DTO
    {
        public FR_GRUPO_DTO()
        {
            this.FR_PERMISSAO = new List<FR_PERMISSAO_DTO>();
            this.FR_USUARIO = new List<FR_USUARIO_DTO>();
        }

        public int GRP_CODIGO { get; set; }
        public string SIS_CODIGO { get; set; }
        public string GRP_NOME { get; set; }
        public string GRP_FILTRO_DICIONARIO { get; set; }
        public virtual ICollection<FR_PERMISSAO_DTO> FR_PERMISSAO { get; set; }
        public virtual ICollection<FR_USUARIO_DTO> FR_USUARIO { get; set; }
    }
}
