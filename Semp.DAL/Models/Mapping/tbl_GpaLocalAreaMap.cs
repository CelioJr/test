using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_GpaLocalAreaMap : EntityTypeConfiguration<tbl_GpaLocalArea>
    {
        public tbl_GpaLocalAreaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodLocal, t.CodArea });

            // Properties
            this.Property(t => t.CodLocal)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.CodArea)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_GpaLocalArea");
            this.Property(t => t.CodLocal).HasColumnName("CodLocal");
            this.Property(t => t.CodArea).HasColumnName("CodArea");
        }
    }
}
