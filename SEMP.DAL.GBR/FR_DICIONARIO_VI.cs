namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_DICIONARIO_VI
    {
        [Key]
        [Column(Order = 0)]
        public string TABELA { get; set; }

        [StringLength(128)]
        public string CAMPO { get; set; }

        [StringLength(200)]
        public string DESCRICAO { get; set; }

        [StringLength(128)]
        public string TIPO { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PK { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FK { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short TAMANHO { get; set; }

        [Key]
        [Column(Order = 4)]
        public byte PRECISAO { get; set; }

        public int? NULO { get; set; }
    }
}
