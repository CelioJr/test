﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade
{
    public class TecnicoPosicao
    {
        public string Posicao { get; set; }
        public string Descricao { get; set; }
        public string CodItem { get; set; }
    }
}
