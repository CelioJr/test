namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolaloco
    {
        [Key]
        public long id_consolaloco { get; set; }

        public DateTime? dthr_ialerta { get; set; }

        public int? ms_dthrialerta { get; set; }

        public DateTime? dthr_falerta { get; set; }

        public int? ms_dthrfalerta { get; set; }

        public short? is_continuaproximoperiodo { get; set; }

        public long id_consolal { get; set; }

        public long? id_consolallog { get; set; }

        public int? seg_auto_tempoalerta { get; set; }

        public int? seg_manu_tempoalerta { get; set; }

        public virtual dw_consolal dw_consolal { get; set; }

        public virtual dw_consolallog dw_consolallog { get; set; }
    }
}
