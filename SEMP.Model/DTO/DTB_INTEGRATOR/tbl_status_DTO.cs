namespace SEMP.Model.DTO
{
	using System;
	using System.ComponentModel.DataAnnotations;

	public partial class tbl_status_DTO
	{
        public int id { get; set; }

        public int? id_pai { get; set; }

        [StringLength(50)]
        public string codapp { get; set; }

        [StringLength(100)]
        public string name { get; set; }

        public bool status { get; set; }

        public DateTime? date { get; set; }
		
    }
}
