﻿using System.Web.Mvc;

namespace SEMP.Areas.Outros
{
    public class OutrosAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Outros";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Outros_default",
                "Outros/{controller}/{action}/{id}",
				new { controle = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}