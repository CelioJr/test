namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_PERMISSAO_MAKER
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PMK_CODIGO { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int GRP_CODIGO { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(1)]
        public string PMK_EDITAR { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(1)]
        public string PMK_EXCLUIR { get; set; }

        public int? FRM_CODIGO { get; set; }

        public int? REL_CODIGO { get; set; }

        public int? REG_COD { get; set; }
    }
}
