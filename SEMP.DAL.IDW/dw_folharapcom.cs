namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_folharapcom
    {
        [Key]
        public long id_folharapcom { get; set; }

        public long id_folharap { get; set; }

        public decimal? qt_total { get; set; }

        public decimal? qt_ativa { get; set; }

        public long? id_produto { get; set; }

        public short? idredzproduto { get; set; }

        public virtual dw_folharap dw_folharap { get; set; }

        public virtual om_produto om_produto { get; set; }
    }
}
