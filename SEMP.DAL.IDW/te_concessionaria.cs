namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class te_concessionaria
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public te_concessionaria()
        {
            te_tarifas = new HashSet<te_tarifas>();
        }

        [Key]
        public long id_concessionaria { get; set; }

        [StringLength(60)]
        public string cd_concessionaria { get; set; }

        public int? revisao { get; set; }

        public DateTime? dt_revisao { get; set; }

        public short? st_ativo { get; set; }

        public DateTime? dt_stativo { get; set; }

        [StringLength(40)]
        public string ds_concessionaria { get; set; }

        public long? id_usrstativo { get; set; }

        public long? id_usrrevisao { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<te_tarifas> te_tarifas { get; set; }
    }
}
