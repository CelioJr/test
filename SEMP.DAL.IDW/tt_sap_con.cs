namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tt_sap_con
    {
        [Key]
        public long id_sapcon { get; set; }

        [StringLength(30)]
        public string deposito { get; set; }

        [StringLength(30)]
        public string conhecimento { get; set; }

        public DateTime? dt_conhecimento { get; set; }

        [StringLength(30)]
        public string centro { get; set; }

        public DateTime? dthr_referencia { get; set; }

        public short? is_importado { get; set; }

        [StringLength(256)]
        public string ds_erro { get; set; }
    }
}
