using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_PPB_TARGET_DTO
    {
        public decimal TARGET_ID { get; set; }
        public string MATERIAL { get; set; }
        public Nullable<double> TARGET { get; set; }
        public Nullable<int> TARGET_YEAR { get; set; }
    }
}
