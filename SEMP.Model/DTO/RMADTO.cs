using System;

namespace SEMP.Model.DTO
{
    public class RMADTO
    {
        public int CodRMA { get; set; }
        public Nullable<System.DateTime> DatReg { get; set; }
        public string CodUser { get; set; }
        public string CodStatus { get; set; }
        public string NotaFiscal { get; set; }
        public string NumGrim { get; set; }
        public string CodFab { get; set; }
    }
}
