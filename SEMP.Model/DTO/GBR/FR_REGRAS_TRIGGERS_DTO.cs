using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_REGRAS_TRIGGERS_DTO
    {
        public string TAB_NOME { get; set; }
        public int TAB_EVENTO { get; set; }
        public int REG_COD { get; set; }
        public string TAB_PARAMS { get; set; }
        public string TAB_SCRIPT { get; set; }
        public Nullable<System.DateTime> TAB_COMPILACAO { get; set; }
    }
}
