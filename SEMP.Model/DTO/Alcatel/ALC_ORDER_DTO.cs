using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_ORDER_DTO
    {
        public ALC_ORDER_DTO()
        {
            this.ALC_COVER_LED = new List<ALC_COVER_LED_DTO>();
            this.ALC_DOWNTIME = new List<ALC_DOWNTIME_DTO>();
            this.ALC_ENGINE = new List<ALC_ENGINE_DTO>();
            this.ALC_ENGINE1 = new List<ALC_ENGINE_DTO>();
            this.ALC_LOT_LIST = new List<ALC_LOT_LIST_DTO>();
            this.ALC_LOTE_LIST = new List<ALC_LOTE_LIST_DTO>();
            this.ALC_MAGAZINE = new List<ALC_MAGAZINE_DTO>();
            this.ALC_VOLUME = new List<ALC_VOLUME_DTO>();
            this.ALC_PRODUCTION_COUNTER = new List<ALC_PRODUCTION_COUNTER_DTO>();
            this.ALC_VOLUME1 = new List<ALC_VOLUME_DTO>();
            this.ALC_PRODUCTION_COUNTER1 = new List<ALC_PRODUCTION_COUNTER_DTO>();
        }

        public string idOrder { get; set; }
        public Nullable<System.DateTime> dtCreated { get; set; }
        public Nullable<System.DateTime> dtEnd { get; set; }
        public Nullable<System.DateTime> dtStart { get; set; }
        public Nullable<int> PROD_QUANT { get; set; }
        public int quantity { get; set; }
        public int line_COD_LINHAS { get; set; }
        public int sku_COD_SKU { get; set; }
        public Nullable<int> user_USER_ID { get; set; }
        public string status { get; set; }
        public string ppb_ppb { get; set; }
        public string fullSku { get; set; }
        public Nullable<int> CHECKED_QUANT { get; set; }
        public Nullable<bool> IS_ENGINE { get; set; }
        public Nullable<System.DateTime> dt_created { get; set; }
        public Nullable<System.DateTime> dt_end { get; set; }
        public Nullable<System.DateTime> dt_start { get; set; }
        public Nullable<int> TARGET_HOUR { get; set; }
        public Nullable<bool> IS_COVER_LED { get; set; }
        public Nullable<int> MAGANIZE_QUANT { get; set; }
        public virtual ICollection<ALC_COVER_LED_DTO> ALC_COVER_LED { get; set; }
        public virtual ICollection<ALC_DOWNTIME_DTO> ALC_DOWNTIME { get; set; }
        public virtual ICollection<ALC_ENGINE_DTO> ALC_ENGINE { get; set; }
        public virtual ICollection<ALC_ENGINE_DTO> ALC_ENGINE1 { get; set; }
        public virtual ICollection<ALC_LOT_LIST_DTO> ALC_LOT_LIST { get; set; }
        public virtual ICollection<ALC_LOTE_LIST_DTO> ALC_LOTE_LIST { get; set; }
        public virtual ICollection<ALC_MAGAZINE_DTO> ALC_MAGAZINE { get; set; }
        public virtual ICollection<ALC_VOLUME_DTO> ALC_VOLUME { get; set; }
        public virtual ICollection<ALC_PRODUCTION_COUNTER_DTO> ALC_PRODUCTION_COUNTER { get; set; }
        public virtual ICollection<ALC_VOLUME_DTO> ALC_VOLUME1 { get; set; }
        public virtual ALC_TARGET_HOUR_DTO ALC_TARGET_HOUR { get; set; }
        public virtual ALC_PPB_DTO ALC_PPB { get; set; }
        public virtual ICollection<ALC_PRODUCTION_COUNTER_DTO> ALC_PRODUCTION_COUNTER1 { get; set; }
        public virtual ALC_SYS_USER_DTO ALC_SYS_USER { get; set; }
    }
}
