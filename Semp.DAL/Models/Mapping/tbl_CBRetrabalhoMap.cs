﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
	public class tbl_CBRetrabalhoMap : EntityTypeConfiguration<tbl_CBRetrabalho>
	{
		public tbl_CBRetrabalhoMap()
		{
			// Primary Key
			this.HasKey(t => t.CodRetrabalho);

			// Properties
			this.Property(t => t.CodRetrabalho)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.Justificativa)
				.IsRequired()
				.HasMaxLength(200);

			this.Property(t => t.CodDRT)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(8);

			this.Property(t => t.CodFab)
				.IsFixedLength()
				.HasMaxLength(2);

			this.Property(t => t.NumAP)
				.IsFixedLength()
				.HasMaxLength(6);

			// Table & Column Mappings
			this.ToTable("tbl_CBRetrabalho");
			this.Property(t => t.CodRetrabalho).HasColumnName("CodRetrabalho");
			this.Property(t => t.Justificativa).HasColumnName("Justificativa");
			this.Property(t => t.DatEvento).HasColumnName("DatEvento");
			this.Property(t => t.CodDRT).HasColumnName("CodDRT");
			this.Property(t => t.Status).HasColumnName("Status");
			this.Property(t => t.CodFab).HasColumnName("CodFab");
			this.Property(t => t.NumAP).HasColumnName("NumAP");
			this.Property(t => t.Quantidade).HasColumnName("Quantidade");
		}
	}
}
