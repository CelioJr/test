using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
	public class tbl_CBEntradaIACMap : EntityTypeConfiguration<tbl_CBEntradaIAC>
	{
		public tbl_CBEntradaIACMap()
		{
			// Primary Key
			this.HasKey(t => new { t.NumOP, t.CodItem, t.NumUD });

			// Properties
			this.Property(t => t.NumOP)
				.IsRequired()
				.HasMaxLength(10)
                .IsUnicode(false);

			this.Property(t => t.CodItem)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(6)
                .IsUnicode(false);

			this.Property(t => t.NumUD)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(10)
                .IsUnicode(false);

			this.Property(t => t.BarrasUD)
				.HasMaxLength(40)
                .IsUnicode(false);

			this.Property(t => t.BarrasFornecedor)
				.HasMaxLength(50)
                .IsUnicode(false);

			this.Property(t => t.NomUsuario)
				.HasMaxLength(20)
                .IsUnicode(false);

			// Table & Column Mappings
			this.ToTable("tbl_CBEntradaIAC");
			this.Property(t => t.NumOP).HasColumnName("NumOP");
			this.Property(t => t.CodItem).HasColumnName("CodItem");
			this.Property(t => t.NumUD).HasColumnName("NumUD");
			this.Property(t => t.BarrasUD).HasColumnName("BarrasUD");
			this.Property(t => t.BarrasFornecedor).HasColumnName("BarrasFornecedor");
			this.Property(t => t.QtdItem).HasColumnName("QtdItem");
			this.Property(t => t.DatLeitura).HasColumnName("DatLeitura");
			this.Property(t => t.NomUsuario).HasColumnName("NomUsuario");
		}
	}
}
