using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBLinhaDefeitoRelatorioMap : EntityTypeConfiguration<tbl_CBLinhaDefeitoRelatorio>
    {
        public tbl_CBLinhaDefeitoRelatorioMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodLinha, t.DatProducao, t.CodDefeito });

            // Properties
            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodDefeito)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5);

            // Table & Column Mappings
            this.ToTable("tbl_CBLinhaDefeitoRelatorio");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.DatProducao).HasColumnName("DatProducao");
            this.Property(t => t.CodDefeito).HasColumnName("CodDefeito");
        }
    }
}
