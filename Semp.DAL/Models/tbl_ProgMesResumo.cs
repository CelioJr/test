using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_ProgMesResumo
    {
        public string CodFab { get; set; }
        public System.DateTime DatProducao { get; set; }
        public string CodModelo { get; set; }
        public Nullable<int> QtdProdPm { get; set; }
        public string Usuario { get; set; }
        public System.Guid rowguid { get; set; }
        public string flgCobertura { get; set; }
        public Nullable<int> NumPrioridade { get; set; }
        public string Observacao { get; set; }
        public string FlagTP { get; set; }
    }
}
