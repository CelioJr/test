using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_LIST_ORDER_DTO
    {
        public string imei { get; set; }
        public string codorder { get; set; }
        public string fullsku { get; set; }
        public string lot { get; set; }
        public string packing { get; set; }
        public string pallet { get; set; }
        public string shipping { get; set; }
    }
}
