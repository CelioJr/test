using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBPlanoAcaoAcompanhamento
    {
        public int CodPlano { get; set; }
        public int CodPlanoCausa { get; set; }
        public int CodAcompanha { get; set; }
        public string DesAcompanha { get; set; }
        public Nullable<System.DateTime> DatAcompanha { get; set; }
        public string NomAcompanhante { get; set; }
        public string FlgStatus { get; set; }
        public Nullable<System.DateTime> DatInicio { get; set; }
        public Nullable<System.DateTime> DatFim { get; set; }
        public Nullable<System.DateTime> DatConclusao { get; set; }
		public string Observacao { get; set; }
    }
}
