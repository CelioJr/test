namespace SEMP.Model.DTO
{
    /// <summary>
    /// Esta classe � respons�vel por exibir o que � lido do posto de amarra��o
    /// </summary>
    public class viw_CBPerfilAmarraPostoDTO
    {
        public int CodLinha { get; set; }
        public int NumPosto { get; set; }
        public int CodTipoAmarra { get; set; }
        public string DesTipoAmarra { get; set; }
        public string CodFam { get; set; }
    }
}
