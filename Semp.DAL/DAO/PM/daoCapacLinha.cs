﻿using System.Data.Entity;
using SEMP.DAL.Models;
using SEMP.Model.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.DAO.PM
{
	public class daoCapacLinha
	{
		public static List<CapacLinhaDTO> ObterCapacidades(DateTime mesRef, string codLinha, string codModelo)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from c in dtbSim.tbl_CapacLinha
								 join d in dtbSim.tbl_Item on c.CodModelo equals d.CodItem
								 join e in dtbSim.tbl_Linha on c.CodLinha equals e.CodLinha
								 where c.DatProducao == mesRef && c.CodLinha.Contains(codLinha) && c.CodModelo.Contains(codModelo)
								 select new CapacLinhaDTO()
								 {
									 CodLinha = c.CodLinha,
									 CodModelo = c.CodModelo,
									 DescricaoLinha = c.CodLinha+"-"+e.DesLinha,
									 DescricaoModelo = c.CodModelo+"-"+d.DesItem,
									 DatProducao = c.DatProducao,
									 NumTurno = c.NumTurno,
									 QtdCapacLinha = c.QtdCapacLinha,
									 QtdCapacMaxima = c.QtdCapacMaxima
								 }).ToList();

				return resultado;

			}
		}

		public static CapacLinhaDTO ObterCapacidade(DateTime mesRef, string codLinha, string codModelo)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from c in dtbSim.tbl_CapacLinha
									join linha in dtbSim.tbl_Linha on c.CodLinha equals linha.CodLinha
									join modelo in dtbSim.tbl_Item on c.CodModelo equals  modelo.CodItem
								 where c.DatProducao == mesRef && c.CodLinha.Equals(codLinha) && c.CodModelo.Equals(codModelo)
								 select new CapacLinhaDTO()
								 {
									 CodLinha = c.CodLinha,
									 CodModelo = c.CodModelo,
									 DatProducao = c.DatProducao,
									 NumTurno = c.NumTurno,
									 QtdCapacLinha = c.QtdCapacLinha,
									 QtdCapacMaxima = c.QtdCapacMaxima,
									 DescricaoLinha = linha.DesLinha,
									 DescricaoModelo = modelo.DesItem
								 }).FirstOrDefault();

				return resultado;

			}
		}

		public static void Gravar(CapacLinhaDTO capacidade)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var existe = dtbSim.tbl_CapacLinha.FirstOrDefault(
						x => x.DatProducao == capacidade.DatProducao && x.CodLinha.Equals(capacidade.CodLinha) && x.CodModelo.Equals(capacidade.CodModelo));

				if (existe != null)
				{
					existe.QtdCapacLinha = capacidade.QtdCapacLinha;
					existe.QtdCapacMaxima = capacidade.QtdCapacMaxima;
					dtbSim.SaveChanges();
				}
				else
				{
					var capac = new tbl_CapacLinha()
					{
						CodLinha = capacidade.CodLinha,
						CodModelo = capacidade.CodModelo,
						DatProducao = capacidade.DatProducao,
						NumTurno = capacidade.NumTurno,
						QtdCapacLinha = capacidade.QtdCapacLinha,
						QtdCapacMaxima = capacidade.QtdCapacMaxima,
						rowguid = Guid.NewGuid()
					};

					dtbSim.tbl_CapacLinha.Add(capac);
					dtbSim.SaveChanges();
				}
			}
		}

		public static void Remover(CapacLinhaDTO capacidade)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var existe =
					dtbSim.tbl_CapacLinha.FirstOrDefault(
						x => x.DatProducao == capacidade.DatProducao && x.CodLinha.Equals(capacidade.CodLinha) && x.CodModelo.Equals(capacidade.CodModelo));

				if (existe == null) return;

				dtbSim.tbl_CapacLinha.Remove(existe);
				dtbSim.SaveChanges();
			}
		}

		public static bool VerificaSeModeloExiste(string codigo)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				return dtbSim.tbl_Item.Any(a => a.CodItem == codigo);
			}
		}

		public static bool VerificaSeLinhaExiste(string codigo)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				return dtbSim.tbl_Linha.Any(a => a.CodLinha == codigo);
			}
		}

		public static bool ClonarMes(DateTime mesRef)
		{
			try
			{
				using (var dtbSim = new DTB_SIMContext())
				{

					var resultado = dtbSim.tbl_CapacLinha.Where(c => c.DatProducao == mesRef).ToList();

					var mesAtual = DateTime.Parse("01/" + DateTime.Now.ToShortDateString().Substring(3));

					var capacAtual = dtbSim.tbl_CapacLinha.Where(c => c.DatProducao == mesAtual).ToList();

					foreach (var linha in resultado.Where(linha => !capacAtual.Any(x => x.CodLinha == linha.CodLinha && x.CodModelo == linha.CodModelo)))
					{
						dtbSim.Entry(linha).State = EntityState.Added;
						linha.DatProducao = mesAtual;
					}

					dtbSim.SaveChanges();

					return true;

				}

			}
			catch (Exception)
			{

				return false;
			}

		}


        public static bool ClonarMes(DateTime mesRef, DateTime mesDest)
        {
            try
            {
                using (var dtbSim = new DTB_SIMContext())
                {

                    // busca as capacidades do mês de referência
                    var resultado = dtbSim.tbl_CapacLinha.Where(c => c.DatProducao == mesRef).ToList();

                    //var mesAtual = DateTime.Parse("01/" + DateTime.Now.ToShortDateString().Substring(3));

                    //verifica os dados que já estão cadastrados
                    var capacAtual = dtbSim.tbl_CapacLinha.Where(c => c.DatProducao == mesDest).ToList();

                    //faz o loop nos registros que não existem no mês atual
                    foreach (var linha in resultado.Where(linha => !capacAtual.Any(x => x.CodLinha == linha.CodLinha && x.CodModelo == linha.CodModelo)))
                    {
						var registro = new tbl_CapacLinha(){
							CodLinha = linha.CodLinha,
							CodModelo = linha.CodModelo,
							DatProducao = mesDest,
							NumTurno = linha.NumTurno,
							QtdCapacLinha = linha.QtdCapacLinha,
							QtdCapacMaxima = linha.QtdCapacMaxima,
							Usuario = linha.Usuario,
							rowguid = Guid.NewGuid()
						};

                        // inclui o registro atual
						dtbSim.tbl_CapacLinha.Add(registro);
                        //dtbSim.Entry(linha).State = EntityState.Added;
                    }

                    dtbSim.SaveChanges();

                    return true;

                }

            }
            catch (Exception)
            {

                return false;
            }

        }
	}

}
