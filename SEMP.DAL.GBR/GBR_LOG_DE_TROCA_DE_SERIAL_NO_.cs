namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_LOG_DE_TROCA_DE_SERIAL_NO_
    {
        [Key]
        public int GBR_LOG_ID { get; set; }

        [Required]
        [StringLength(20)]
        public string IMEI_BASE { get; set; }

        [StringLength(20)]
        public string SERIAL_ANTERIOR { get; set; }

        [StringLength(20)]
        public string SERIAL_NOVO { get; set; }

        public DateTime? DATA { get; set; }
    }
}
