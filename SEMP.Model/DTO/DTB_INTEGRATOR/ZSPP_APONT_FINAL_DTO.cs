namespace SEMP.Model.DTO
{
	using System.ComponentModel.DataAnnotations;

	public partial class ZSPP_APONT_FINAL_DTO
	{
        public int ID { get; set; }

        [StringLength(16)]
        public string PACKNO { get; set; }

        [StringLength(11)]
        public string EXTROW { get; set; }

        [StringLength(4)]
        public string WERKS { get; set; }

        [StringLength(12)]
        public string AUFNR { get; set; }

        [StringLength(40)]
        public string MATNR1 { get; set; }

        [StringLength(18)]
        public string EAN13 { get; set; }

        [StringLength(20)]
        public string SERIAL1 { get; set; }

        [StringLength(10)]
        public string CREDATE1 { get; set; }

        [StringLength(40)]
        public string MATNR2 { get; set; }

        [StringLength(40)]
        public string SERIAL2 { get; set; }

        [StringLength(10)]
        public string CREDATE2 { get; set; }

        [StringLength(10)]
        public string CRETIME2 { get; set; }

        [StringLength(10)]
        public string CRETIME1 { get; set; }

        [StringLength(8)]
        public string ARBPL { get; set; }

        [StringLength(10)]
        public string CODLINHA { get; set; }

        [StringLength(10)]
        public string CODPOSTO { get; set; }

        [StringLength(8)]
        public string DRT { get; set; }

        [StringLength(10)]
        public string TPAMAR { get; set; }

        [StringLength(10)]
        public string GESME { get; set; }

        [StringLength(2)]
        public string STATCONF { get; set; }

        [StringLength(10)]
        public string DTCONF { get; set; }

        [StringLength(10)]
        public string MBLNR { get; set; }

        [StringLength(1)]
        public string STATVD { get; set; }

        [StringLength(1)]
        public string STBCKFLUSH { get; set; }

        [StringLength(10)]
        public string DTBCKFLUSH { get; set; }

        [StringLength(12)]
        public string USERNAME { get; set; }

        [StringLength(1)]
        public string SISAP { get; set; }

        [StringLength(4)]
        public string AUART { get; set; }
    }
}
