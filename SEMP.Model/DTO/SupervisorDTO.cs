namespace SEMP.Model.DTO
{
    public class SupervisorDTO
    {
        public string CodDRT { get; set; }
        public string CodSeguranca { get; set; }
    }
}
