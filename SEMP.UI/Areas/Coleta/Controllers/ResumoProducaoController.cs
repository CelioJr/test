﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using RAST.BO.Mobilidade;
using RAST.BO.Relatorios;
using SEMP.Model.VO.Mobilidade;
using SEMP.Model.VO.Rastreabilidade.Relatorios;

namespace SEMP.Areas.Coleta.Controllers
{
	[Authorize]
	public class ResumoProducaoController : Controller
	{
		//
		// GET: /Coleta/ResumoProducao/

		public ActionResult Index(String DatInicio, String DatFim, String Modelo, string Familia, string Fase, string FlgResumo, string FlgDivergentes, string flgDifMes, string linha)
		{

			DateTime dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			DateTime dFim = dIni.AddDays(1).AddMinutes(-1);

			if (!String.IsNullOrEmpty(DatInicio) && !String.IsNullOrEmpty(DatFim))
			{
				dIni = DateTime.Parse(DatInicio);
				dFim = DateTime.Parse(DatFim);
				dFim = dFim.AddDays(1).AddMinutes(-1);
			}

			if (String.IsNullOrEmpty(Fase))
			{
				Fase = "FEC";
			}

			List<ResumoProducao> resumoProducao;

			if (Fase.Equals("IMC"))
			{
				if (String.IsNullOrEmpty(FlgResumo))
				{
					resumoProducao = ResumoProducaoBO.ObterResumoIMC(dIni, dFim, Modelo);
				}
				else
				{
					resumoProducao = ResumoProducaoBO.ObterResumoIMCModelo(dIni, dFim, Modelo);
				}
			}
			else if (Fase.Equals("IAC"))
			{
				if (String.IsNullOrEmpty(FlgResumo))
				{
					resumoProducao = ResumoProducaoBO.ObterResumoIAC(dIni, dFim, Modelo).OrderBy(x => x.Turno).ThenBy(x => x.CodLinha).ThenBy(x => x.DesModelo).ToList();

					//temporariamente tirando as linhas Radiais da consulta
					//resumoProducao = resumoProducao.Where(x => !x.CodLinha.Contains("RH")).ToList();
				}
				else
				{
					resumoProducao = ResumoProducaoBO.ObterResumoIACModelo(dIni, dFim, Modelo);
				}

			}
			else
			{
				if (String.IsNullOrEmpty(FlgResumo))
				{
					resumoProducao = ResumoProducaoBO.ObterResumoMF(dIni, dFim, Modelo, Familia);
				}
				else
				{
					resumoProducao = ResumoProducaoBO.ObterResumoModelo(dIni, dFim, Modelo, Familia);
				}
			}


			if (!string.IsNullOrEmpty(linha))
			{

				resumoProducao = resumoProducao.Where(x => x.CodLinha.StartsWith(linha.Trim())).ToList();

			}

			var alcatel = new List<ResumoProducaoAlcatel>();

			if (String.IsNullOrEmpty(linha) && String.IsNullOrEmpty(Modelo) && (String.IsNullOrEmpty(Familia) || Familia == "ALCATEL"))
			{
				if (Fase.Equals("FEC"))
				{
					alcatel = Alcatel.ObterResumoAlcatel(dIni, dFim);
				}
				else if (Fase.Equals("IMC"))
				{
					alcatel = Alcatel.ObterResumoAlcatelEngine(dIni, dFim);
					//alcatel.AddRange(Alcatel.ObterResumoAlcatelEngineFT(dIni, dFim));
				}

				ViewBag.Alcatel = alcatel;
			}

			return View(resumoProducao);
		}


	}
}
