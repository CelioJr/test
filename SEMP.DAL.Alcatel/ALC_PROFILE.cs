namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_PROFILE")]
    public partial class ALC_PROFILE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ALC_PROFILE()
        {
            ALC_ROLE_PROF = new HashSet<ALC_ROLE_PROF>();
        }

        [Column(TypeName = "numeric")]
        public decimal id { get; set; }

        public DateTime? createdAt { get; set; }

        public DateTime? updatedAt { get; set; }

        [StringLength(255)]
        public string avatar { get; set; }

        [StringLength(255)]
        public string email { get; set; }

        public bool enabled { get; set; }

        [StringLength(255)]
        public string firstName { get; set; }

        [StringLength(255)]
        public string lastName { get; set; }

        [StringLength(255)]
        public string password { get; set; }

        public DateTime? passwordResetExpiry { get; set; }

        [StringLength(255)]
        public string passwordResetToken { get; set; }

        [StringLength(255)]
        public string profileType { get; set; }

        [Required]
        [StringLength(255)]
        public string username { get; set; }

        [StringLength(255)]
        public string uuid { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_ROLE_PROF> ALC_ROLE_PROF { get; set; }
    }
}
