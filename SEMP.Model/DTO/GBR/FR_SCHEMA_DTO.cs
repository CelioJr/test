using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_SCHEMA_DTO
    {
        public string SCH_NOME { get; set; }
        public int SCH_VERSAO { get; set; }
    }
}
