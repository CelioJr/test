﻿using System;

namespace SEMP.Model.VO
{
	[Serializable]
	public class Mensagem
	{
		public string CodMensagem { get; set; }
		public string DesMensagem { get; set; }
		public string DesCor { get; set; }
		public string DesSom { get; set; }
		public string StackErro { get; set; }
		public Nullable<DateTime> DatStatus { get; set; }

        public string MensagemAuxiliar { get; set; }

		public Mensagem() {
			DatStatus = DateTime.Now;
			DesCor = "green";
			DesSom = "";
		}

	}
}
