﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade
{
	public class TempoAtravessamentoResumoAP
	{
		public DateTime Data { get; set; }
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public decimal QtdProduzido { get; set; }
		public int QtdDefeitos { get; set; }
		public decimal? TempoMedioProd { get; set; }
		public decimal? TempoMedioEsp { get; set; }
		public decimal? TempoMedioAtravessa { get; set; }
		public decimal? MenorTempo { get; set; }
		public decimal? MaiorTempo { get; set; }
		public decimal? TempoMedioCorrido { get; set; }
		public decimal? TempoEsperaTecnico { get; set; }
		public string CodFab { get; set; }
		public string NumAP { get; set; }
		public string Setor { get; set; }
		public int Ordem { get; set; }
		public int? QtdLoteAP { get; set; }
		public int? QtdApontada { get; set; }
		public DateTime? DatAbertura { get; set; }
		public DateTime? DatLiberacao { get; set; }
		public DateTime? DatSeparacao { get; set; }
		public DateTime? DatProducao { get; set; }
		public DateTime? DatEncerramento { get; set; }
		public decimal? TempoAP { get; set; }
	}
}
