namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ms_cck
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id_campo { get; set; }

        public DateTime? datahora { get; set; }
    }
}
