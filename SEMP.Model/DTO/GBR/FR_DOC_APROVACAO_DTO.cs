using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DOC_APROVACAO_DTO
    {
        public string APR_NOME { get; set; }
        public int DOC_CODIGO { get; set; }
        public string APR_CARGO { get; set; }
        public string APR_PARTICIPACAO { get; set; }
        public virtual FR_DOC_PRINCIPAL_DTO FR_DOC_PRINCIPAL { get; set; }
    }
}
