namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_ROMANEIO
    {
        [Key]
        public int COD_ROMANEIO { get; set; }

        [StringLength(50)]
        public string DANFE { get; set; }

        [StringLength(15)]
        public string PALLET { get; set; }

        [StringLength(20)]
        public string SKU { get; set; }

        public int? QTD { get; set; }

        public int? DB_QTD { get; set; }

        [StringLength(15)]
        public string DB_DOC { get; set; }

        public int? DB_SERIE { get; set; }

        [StringLength(30)]
        public string DB_COD { get; set; }

        public DateTime? DATA { get; set; }

        public bool? GERADO { get; set; }

        [StringLength(20)]
        public string SERIAL { get; set; }

        [StringLength(20)]
        public string IMEI { get; set; }

        [StringLength(20)]
        public string PACK { get; set; }
    }
}
