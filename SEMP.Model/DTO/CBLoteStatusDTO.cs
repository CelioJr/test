﻿using System;
using System.ComponentModel.DataAnnotations;


namespace SEMP.Model.DTO
{
    public class CBLoteStatusDTO
    {
        [Display(Name = "Código do Status")]
        public int CodStatus { get; set; }

        [Display(Name = "Descrição")]
        public string Descricao { get; set; }
        
        [Display(Name = "Status Ativo")]
        public bool flgAtivo { get; set; }
        
    }
}
