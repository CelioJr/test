namespace SEMP.DAL.DTB_SSA.Models
{
    using System;
    
    public partial class tbl_SGI_Imagem
    {
        public int imagem_id { get; set; }
        public string codEstrutura { get; set; }
        public string codImagem { get; set; }
        public int codOperador { get; set; }
        public string desImagem { get; set; }
        public string tipParticao { get; set; }
        public string tipMaquina { get; set; }
        public string codItem { get; set; }
        public DateTime? dataHoraArquivo { get; set; }
        public int? codOpeUpdate { get; set; }
        public System.DateTime datInsercao { get; set; }
        public DateTime? datAtualizacao { get; set; }
        public decimal? tamanhoarquivo { get; set; }
        public int tipImagem { get; set; }
        public string codImagemToshiba { get; set; }
    }
}
