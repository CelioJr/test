namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_calpt
    {
        [Key]
        public long id_calpt { get; set; }

        public DateTime? dthr_stativo { get; set; }

        public short? st_ativo { get; set; }

        public long id_cal { get; set; }

        public DateTime? dthrivalidade { get; set; }

        public long id_pt { get; set; }

        public virtual dw_cal dw_cal { get; set; }

        public virtual om_pt om_pt { get; set; }
    }
}
