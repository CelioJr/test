﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.DTO;
using SEMP.Model.VO.Rastreabilidade.Relatorios;

namespace RAST.BO.Relatorios
{
	public class SaldoPlacas
	{
		public static List<PCP201b_19DTO> ObterSaldoPlacas(DateTime Data, String Familia, String codModelo, String codPlaca, int numEstudo)
		{

			return daoSaldoPlaca.ObterSaldoPlacas(Data, Familia, codModelo, codPlaca, numEstudo);

		}

		public static List<LSAAPDTO> ObterAPsMes(String codModelo, DateTime datReferencia){
			
			return daoSaldoPlaca.ObterAPsMes(codModelo, datReferencia);

		}

		public static List<LSAATDTO> ObterATsMes(String codModelo, DateTime datReferencia)
		{

			return daoSaldoPlaca.ObterATsMes(codModelo, datReferencia);

		}

	}
}
