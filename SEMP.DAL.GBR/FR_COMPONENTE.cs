namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_COMPONENTE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FR_COMPONENTE()
        {
            FR_ACAOCOMPONENTE = new HashSet<FR_ACAOCOMPONENTE>();
            FR_PROPRIEDADE = new HashSet<FR_PROPRIEDADE>();
        }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FRM_CODIGO { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int COM_CODIGO { get; set; }

        public int? IMG_CODIGO { get; set; }

        [StringLength(30)]
        public string COM_TIPO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_ACAOCOMPONENTE> FR_ACAOCOMPONENTE { get; set; }

        public virtual FR_FORMULARIO FR_FORMULARIO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_PROPRIEDADE> FR_PROPRIEDADE { get; set; }
    }
}
