namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_SISTEMA_CATEGORIA
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(3)]
        public string SIS_CODIGO { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CAT_CODIGO { get; set; }

        public virtual FR_CATEGORIA FR_CATEGORIA { get; set; }
    }
}
