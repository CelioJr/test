﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.DTO;

namespace RAST.BO.Rastreabilidade
{
    public class ProducaoHoraPostoBO
    {
        public static List<ProducaoHoraPostoDTO> ListaQtdPosto(DateTime dataIni, DateTime dataFim, int linha, 
            int horaInicio, int horaFim, int? posto)
        {
            return daoProducaoHoraPosto.ListaQtdPosto(dataIni, dataFim, linha, horaInicio, horaFim,posto);
        }

        //public static List<ProducaoHoraPostoDTO> ListaQtdPosto(DateTime dataIni, DateTime dataFim, int linha, int posto,
        //   int horaInicio, int horaFim)
        //{
        //    return daoProducaoHoraPosto.ListaQtdPosto(dataIni, dataFim, linha, horaInicio, horaFim);
        //}


    }
}
