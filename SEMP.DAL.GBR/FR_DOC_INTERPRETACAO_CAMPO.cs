namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_DOC_INTERPRETACAO_CAMPO
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(255)]
        public string CAM_PADRAO { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(255)]
        public string CAM_TIPO { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DOC_CODIGO { get; set; }

        public virtual FR_DOC_PRINCIPAL FR_DOC_PRINCIPAL { get; set; }
    }
}
