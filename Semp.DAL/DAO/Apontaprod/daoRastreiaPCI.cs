﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.DAL.Models;
using SEMP.Model.DTO;

namespace SEMP.DAL.DAO.Apontaprod
{
	public class daoRastreiaPCI
	{
		public static List<CBRastreiaPCIDTO> ObterRastreio(string Codigo)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{

				var resumo = dtbCb.Database.SqlQuery<CBRastreiaPCIDTO>(
					"EXEC spc_CBRastreiaPCI @NumECB",
					new SqlParameter("NumECB", Codigo)
					).ToList();

				//var desModelo = daoGeral.ObterDescricaoItem(Codigo.Substring(0, 6));
				//if (!String.IsNullOrEmpty(desModelo))
				//{
				//	resumo.ForEach(x => x.DesModelo = String.Format("{0} - {1}", Codigo.Substring(0,6), desModelo));
				//}

				return resumo;

			}

		}

		public static List<CBRastreiaPCIDTO> ObterRastreioOld(string Codigo)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{

				var resumo = dtbSim.Database.SqlQuery<CBRastreiaPCIDTO>(
					"EXEC dtb_CB..spc_CBRastreiaPCI @NumECB",
					new SqlParameter("NumECB", Codigo)
					).ToList();

				return resumo;

			}

		}
	}
}
