﻿namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
	public class ResumoRelatorioTecnico
	{
		public ResumoRelatorioTecnico()
		{
			QtdProduzida = 0;
			QtdDefeitos = 0;
			QtdTotalDefeitos = 0;
			QtdPlacasAcumuladas = 0;
			TempoMedio = "00:00:00";
			MenorTempo = "00:00:00";
			MaiorTempo = "00:00:00";
			IndiceTotal = 0;
			Indice = 0;
			IndiceFalsaFalha = 0;
			QtdFalsaFalha = 0;
		}

		public int QtdProduzida { get; set; }
		public int QtdDefeitos { get; set; }
		public int QtdTotalDefeitos { get; set; }
		public int QtdPlacasAcumuladas { get; set; }
		public string TempoMedio { get; set; }
		public string MenorTempo { get; set; }
		public string MaiorTempo { get; set; }
		public float IndiceTotal { get; set; }
		public float Indice { get; set; }
		public float IndiceFalsaFalha { get; set; }

		public int QtdFalsaFalha { get; set; }

	}
}
