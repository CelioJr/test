﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.PM;
using SEMP.Model.DTO;

namespace SEMP.Areas.Outros.Controllers
{
	public class AmplitudeController : Controller
	{
		//
		// GET: /Outros/Amplitude/
		public ActionResult Index()
		{
			return View();
		}

		//
		// GET: /Outros/Amplitude/Details/5
		public ActionResult Lista(string codDepto, string codFab)
		{
			Response.AddHeader("content-disposition", "attachment; filename=Amplitude.xls");
			Response.ContentType = "application/ms-excel";

			var dados = ParadaDeLinha.ObterListaAmplitude();

			if (!String.IsNullOrEmpty(codFab))
			{
				dados = dados.Where(x => x.CodFab.Trim() == codFab).ToList();
			}

			if (!String.IsNullOrEmpty(codDepto))
			{
				dados = dados.Where(x => x.CodDepto.Trim() == codDepto).ToList();
			}

			return View(dados);
		}

		//
		// GET: /Outros/Amplitude/Create
		public ActionResult Create()
		{
			return View();
		}

		//
		// POST: /Outros/Amplitude/Create
		[HttpPost]
		public ActionResult Create(ProdAmplitudeDTO dados)
		{
			try
			{
				ParadaDeLinha.GravarAmplitude(dados);

				var resposta = new { resultado = true, mensagem = "" };

				return Json(resposta, JsonRequestBehavior.AllowGet);
			}
			catch(Exception ex)
			{
				var mensagem = ex.InnerException != null ? ex.InnerException.InnerException != null ? ex.InnerException.InnerException.Message : ex.InnerException.Message : ex.Message;

				var resposta = new { resultado = false, mensagem = mensagem };


				return Json(resposta, JsonRequestBehavior.AllowGet);
			}
		}

		//
		// GET: /Outros/Amplitude/Edit/5
		public ActionResult Edit(string codFab, string codDepto, string nomUsuario)
		{
			var dado = ParadaDeLinha.ObterAmplitude(codFab, codDepto, nomUsuario);
			
			return View(dado);
		}

		//
		// POST: /Outros/Amplitude/Edit/5
		[HttpPost]
		public ActionResult Edit(ProdAmplitudeDTO dado)
		{
			try
			{
				ParadaDeLinha.GravarAmplitude(dado);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}

		//
		// GET: /Outros/Amplitude/Delete/5
		public ActionResult Delete(string codFab, string codDepto, string nomUsuario)
		{

			var dado = ParadaDeLinha.ObterAmplitude(codFab, codDepto, nomUsuario);

			return View(dado);
		}

		//
		// POST: /Outros/Amplitude/Delete/5
		[HttpPost]
		public ActionResult Delete(ProdAmplitudeDTO dado)
		{
			try
			{
				ParadaDeLinha.RemoverAmplitude(dado.CodFab, dado.CodDepto, dado.NomUsuario);

				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}
		}
	}
}
