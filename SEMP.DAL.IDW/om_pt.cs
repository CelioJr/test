namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_pt
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_pt()
        {
            dw_calpt = new HashSet<dw_calpt>();
            dw_consolatlog = new HashSet<dw_consolatlog>();
            dw_consolciplog = new HashSet<dw_consolciplog>();
            dw_consolid = new HashSet<dw_consolid>();
            dw_consolmolog = new HashSet<dw_consolmolog>();
            dw_consolpalog = new HashSet<dw_consolpalog>();
            dw_consolpt = new HashSet<dw_consolpt>();
            dw_consolsplog = new HashSet<dw_consolsplog>();
            dw_consolvaritmolog = new HashSet<dw_consolvaritmolog>();
            dw_estlocal = new HashSet<dw_estlocal>();
            dw_estmov = new HashSet<dw_estmov>();
            dw_expcvs = new HashSet<dw_expcvs>();
            dw_folhacic = new HashSet<dw_folhacic>();
            dw_nserieobs = new HashSet<dw_nserieobs>();
            dw_operacao = new HashSet<dw_operacao>();
            dw_passagem = new HashSet<dw_passagem>();
            dw_rotapasso = new HashSet<dw_rotapasso>();
            dw_rotapasso_pt = new HashSet<dw_rotapasso_pt>();
            dw_rt = new HashSet<dw_rt>();
            ms_trigger = new HashSet<ms_trigger>();
            om_cfgptdetcoleta = new HashSet<om_cfgptdetcoleta>();
            om_homopt = new HashSet<om_homopt>();
            om_indpt = new HashSet<om_indpt>();
            om_licenca_pt = new HashSet<om_licenca_pt>();
            om_mapa = new HashSet<om_mapa>();
            om_obj = new HashSet<om_obj>();
            om_pa = new HashSet<om_pa>();
            om_papro = new HashSet<om_papro>();
            om_prg = new HashSet<om_prg>();
            om_ptcnc = new HashSet<om_ptcnc>();
            om_ptcnc1 = new HashSet<om_ptcnc>();
            om_ptcp = new HashSet<om_ptcp>();
            pp_cp1 = new HashSet<pp_cp>();
            pp_cpentsai = new HashSet<pp_cpentsai>();
            pp_indisp_rappt = new HashSet<pp_indisp_rappt>();
            pp_nec = new HashSet<pp_nec>();
            pp_nec1 = new HashSet<pp_nec>();
            pp_nec2 = new HashSet<pp_nec>();
            pp_nec3 = new HashSet<pp_nec>();
            pp_planptgt = new HashSet<pp_planptgt>();
        }

        [Key]
        public long id_pt { get; set; }

        [StringLength(30)]
        public string cd_pt { get; set; }

        public int? revisao { get; set; }

        public DateTime? dt_revisao { get; set; }

        public DateTime? dt_stativo { get; set; }

        public long id_usrstativo { get; set; }

        public long id_usrrevisao { get; set; }

        public long? id_clp { get; set; }

        [StringLength(255)]
        public string url_conexao { get; set; }

        [StringLength(100)]
        public string ds_pt { get; set; }

        [StringLength(10)]
        public string ds_curta { get; set; }

        public long? id_cc { get; set; }

        public long id_tppt { get; set; }

        public long? id_alimpre { get; set; }

        public long? id_alimcorrente { get; set; }

        public long? id_alim { get; set; }

        public long? id_gt { get; set; }

        [StringLength(10)]
        public string depara { get; set; }

        public short tp_impprog { get; set; }

        public short? st_ativo { get; set; }

        public long? id_folha { get; set; }

        public short? is_planGt { get; set; }

        [StringLength(18)]
        public string estagio { get; set; }

        public short? is_aponGt { get; set; }

        public int? is_alimcorexc { get; set; }

        public decimal? ind_oee { get; set; }

        public short? is_semop { get; set; }

        public short? is_offline { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? dthr_heartbeat { get; set; }

        public short? is_aponParadaGt { get; set; }

        public short? is_solicitaqtde { get; set; }

        public short? is_devepassarns { get; set; }

        public decimal? seg_auto_tempoParadaNoCiclo { get; set; }

        public long? id_cp { get; set; }

        public short? is_paralinha { get; set; }

        public short? tp_sessao { get; set; }

        [StringLength(40)]
        public string ds_sessao { get; set; }

        public short? is_consolpendente { get; set; }

        public short? is_perdasinc { get; set; }

        public short? is_habilita_cip { get; set; }

        public short? is_habilita_varitmo { get; set; }

        public decimal? perc_varitmo { get; set; }

        public int? qt_varitmo { get; set; }

        public short? is_logingt { get; set; }

        public short? tp_classeABC { get; set; }

        public short? is_cip_ativado { get; set; }

        public short? is_parada_fechaciclo { get; set; }

        public int? qt_eventosnoclp { get; set; }

        [StringLength(255)]
        public string url_impressoracb { get; set; }

        [StringLength(255)]
        public string url_impressoradoc { get; set; }

        public short? is_apontarposicaomecanica { get; set; }

        public int? ordemNogt { get; set; }

        public short? is_ciclocomrefugo { get; set; }

        public short? is_trocaopgt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_calpt> dw_calpt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolatlog> dw_consolatlog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolciplog> dw_consolciplog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolid> dw_consolid { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolmolog> dw_consolmolog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolpalog> dw_consolpalog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolpt> dw_consolpt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolsplog> dw_consolsplog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolvaritmolog> dw_consolvaritmolog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_estlocal> dw_estlocal { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_estmov> dw_estmov { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_expcvs> dw_expcvs { get; set; }

        public virtual dw_folha dw_folha { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhacic> dw_folhacic { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_nserieobs> dw_nserieobs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_operacao> dw_operacao { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_passagem> dw_passagem { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rotapasso> dw_rotapasso { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rotapasso_pt> dw_rotapasso_pt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rt> dw_rt { get; set; }

        public virtual ms_pt_coleta ms_pt_coleta { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_trigger> ms_trigger { get; set; }

        public virtual om_alim om_alim { get; set; }

        public virtual om_alim om_alim1 { get; set; }

        public virtual om_alim om_alim2 { get; set; }

        public virtual om_cc om_cc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfgptdetcoleta> om_cfgptdetcoleta { get; set; }

        public virtual om_clp om_clp { get; set; }

        public virtual om_gt om_gt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_homopt> om_homopt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_indpt> om_indpt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_licenca_pt> om_licenca_pt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_mapa> om_mapa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_obj> om_obj { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_pa> om_pa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_papro> om_papro { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_prg> om_prg { get; set; }

        public virtual pp_cp pp_cp { get; set; }

        public virtual om_tppt om_tppt { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_ptcnc> om_ptcnc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_ptcnc> om_ptcnc1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_ptcp> om_ptcp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cp> pp_cp1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cpentsai> pp_cpentsai { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_indisp_rappt> pp_indisp_rappt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_nec> pp_nec { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_nec> pp_nec1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_nec> pp_nec2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_nec> pp_nec3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_planptgt> pp_planptgt { get; set; }
    }
}
