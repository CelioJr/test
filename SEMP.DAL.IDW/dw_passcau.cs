namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_passcau
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_passcau()
        {
            dw_passagem = new HashSet<dw_passagem>();
            dw_passdef = new HashSet<dw_passdef>();
            dw_passtf = new HashSet<dw_passtf>();
        }

        [Key]
        public long id_passcau { get; set; }

        public long id_passagem { get; set; }

        public long id_tacao { get; set; }

        public long id_tdefeito { get; set; }

        public long? id_produto { get; set; }

        [StringLength(200)]
        public string ds_posicaomecanica { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_passagem> dw_passagem { get; set; }

        public virtual dw_passagem dw_passagem1 { get; set; }

        public virtual om_produto om_produto { get; set; }

        public virtual dw_t_acao dw_t_acao { get; set; }

        public virtual dw_t_defeito dw_t_defeito { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_passdef> dw_passdef { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_passtf> dw_passtf { get; set; }
    }
}
