﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
    public class RelatorioInspecaoIACModeloLinha
    {
        public string Modelo { get; set; }        
        public string DescModelo { get; set; }
        public string NE { get; set; }
        public string DescNE { get; set; }
        public int QuantidadePonto { get; set; }
        public int QuantidadeInspecionada { get; set; }
        public int QuantidadeDefeito { get; set; }
        public decimal PPM { get; set; }
    }
}
