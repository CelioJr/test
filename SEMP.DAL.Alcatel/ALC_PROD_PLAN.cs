namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_PROD_PLAN")]
    public partial class ALC_PROD_PLAN
    {
        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal id_plan { get; set; }

        [StringLength(40)]
        public string full_sku { get; set; }

        public DateTime? ini_date { get; set; }

        public int? made { get; set; }

        public int? planned { get; set; }

        public int line { get; set; }

        [Required]
        [StringLength(5)]
        public string ppb { get; set; }

        public int sku { get; set; }

        public int? target { get; set; }

        public double? curve { get; set; }

        public int? ttl_defects { get; set; }

        public int? capacity { get; set; }

        public virtual ALC_PPB ALC_PPB { get; set; }

        public virtual ALC_TARGET_HOUR ALC_TARGET_HOUR { get; set; }
    }
}
