namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_DEP_REGRA_CAMPO
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int REG_COD { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(96)]
        public string TAB_NOME { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(96)]
        public string CMP_NOME { get; set; }
    }
}
