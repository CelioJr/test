namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ZCSD_INTERFACE_ITEM_ORDEM
    {
        public int id { get; set; }

        [Required]
        [StringLength(16)]
        public string PACKNO { get; set; }

        [Required]
        [StringLength(10)]
        public string EXTROW { get; set; }

        [StringLength(12)]
        public string NUMEROPEDIDO { get; set; }

        [StringLength(8)]
        public string NUMEROITEM { get; set; }

        [StringLength(46)]
        public string NEMATERIAL { get; set; }

        [StringLength(8)]
        public string CATEGORIAITEM { get; set; }

        [StringLength(8)]
        public string FABRICA { get; set; }

        [StringLength(8)]
        public string DEPOSITO { get; set; }

        [StringLength(16)]
        public string QUANTIDADE { get; set; }

        [StringLength(18)]
        public string PRECOUNITARIO { get; set; }

        [StringLength(38)]
        public string ORDEMCOMPRA { get; set; }

        [StringLength(4)]
        public string MOTIVORECUSA { get; set; }

        [StringLength(18)]
        public string IMPOSTOSICMS { get; set; }

        [StringLength(18)]
        public string IMPOSTOSICMSST { get; set; }

        [StringLength(18)]
        public string IMPOSTOSIPI { get; set; }

        [StringLength(18)]
        public string IMPOSTOSPIS { get; set; }

        [StringLength(18)]
        public string IMPOSTOSCOFINS { get; set; }

        [StringLength(18)]
        public string IMPOSTOSFCP { get; set; }

        [StringLength(18)]
        public string IMPOSTOSDIFAL { get; set; }

        [StringLength(18)]
        public string VALORFRETE { get; set; }

        [StringLength(18)]
        public string JUROS { get; set; }

        [StringLength(18)]
        public string VERBA { get; set; }

        [StringLength(18)]
        public string PRECOLIQUIDO { get; set; }

        [StringLength(18)]
        public string PRECOREFERENCIA { get; set; }

        [StringLength(14)]
        public string REFERENCIAINVOICE { get; set; }

        [StringLength(8)]
        public string REFERENCIAITEM { get; set; }
    }
}
