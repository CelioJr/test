namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_t_alerta
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_t_alerta()
        {
            dw_consolal = new HashSet<dw_consolal>();
            dw_t_parada = new HashSet<dw_t_parada>();
            om_cfg = new HashSet<om_cfg>();
        }

        [Key]
        public long id_talerta { get; set; }

        [StringLength(30)]
        public string cd_talerta { get; set; }

        public int? revisao { get; set; }

        [StringLength(40)]
        public string ds_talerta { get; set; }

        public DateTime? dt_revisao { get; set; }

        public short? st_ativo { get; set; }

        public DateTime? dt_stativo { get; set; }

        public long id_tppt { get; set; }

        public long id_usrstativo { get; set; }

        public long id_usrrevisao { get; set; }

        public short? is_automatico { get; set; }

        public short? is_timeout { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolal> dw_consolal { get; set; }

        public virtual om_tppt om_tppt { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_t_parada> dw_t_parada { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg { get; set; }
    }
}
