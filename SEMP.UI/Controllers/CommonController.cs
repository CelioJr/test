﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using RAST.BO.Rastreabilidade;
using RAST.BO.Relatorios;
using SEMP.Model.DTO;

namespace SEMP.Controllers
{
	public class CommonController : Controller
	{
		#region Consultas Gerais
		// GET api/common/5
		public JsonResult ObterLinhas()
		{
			var linhas = RelatorioProducaoMF.ObterLinhasMF();

			return Json(linhas, JsonRequestBehavior.AllowGet);
		}
		#endregion

		#region Dados de Programação e Produção

		[OutputCache(Duration = 0)]
		public JsonResult ObterProgramaLinha(string datInicio, string codLinha)
		{
			DateTime dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));

			if (datInicio != null)
			{
				dIni = Convert.ToDateTime(datInicio);
			}

			var ultLeitura = Geral.ObterUltimoEmbaladoLinha(codLinha);
			string codModelo = ultLeitura != null ? ultLeitura.CodModelo: "";

			var consulta = Geral.ObterPrograma(codLinha, dIni, codModelo);

			var programa = consulta != null ? consulta.QtdProdPm : 0;

			return Json(programa, JsonRequestBehavior.AllowGet);
		}

		[OutputCache(Duration = 0)]
		public JsonResult ObterProduzidoLinha(string datInicio, string codLinha)
		{
			DateTime dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));

			if (datInicio != null)
			{
				dIni = Convert.ToDateTime(datInicio);
			}

			var ultLeitura = Geral.ObterUltimoEmbaladoLinha(codLinha);
			string codModelo = ultLeitura != null ? ultLeitura.CodModelo : "";

			var consulta = Geral.ObterProduzido(codLinha, dIni, codModelo);

			var programa = consulta != null ? consulta.QtdApontada : 0;

			return Json(programa, JsonRequestBehavior.AllowGet);
		}

		[OutputCache(Duration = 0)]
		public JsonResult ObterResumoLinha(string codLinha)
		{

			try
			{

				var consulta = String.IsNullOrEmpty(codLinha) ? PainelProducao.ObterPainelProducao() : PainelProducao.ObterPainelProducao(codLinha);
				
				return Json(consulta, JsonRequestBehavior.AllowGet);
			}
			catch (Exception)
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}

		}

		[OutputCache(Duration = 0)]
		public JsonResult ObterModelosLinha(string codLinha, string datInicio, string datFim)
		{

			DateTime dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			DateTime dFim = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));

			try
			{
				if (datInicio != null && datFim != null)
				{
					dIni = Convert.ToDateTime(datInicio);
					dFim = Convert.ToDateTime(datFim);
				}

				var resumoProducao = ResumoProducaoBO.ObterResumoMF(dIni, dFim, "", "").Where(x => x.CodLinha == codLinha).ToList();

				return Json(resumoProducao, JsonRequestBehavior.AllowGet);
			}
			catch (Exception)
			{

				return Json(false, JsonRequestBehavior.AllowGet);
			}
			
		}

		#endregion

	}
}

