using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBHistoricoReimpressaoMap : EntityTypeConfiguration<tbl_CBHistoricoReimpressao>
    {
        public tbl_CBHistoricoReimpressaoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.DesSerie, t.DatImpressao });

            // Properties
            this.Property(t => t.DesSerie)
                .IsRequired()
                .HasMaxLength(39);
            
            // Table & Column Mappings
            this.ToTable("tbl_CBHistoricoReimpressao");
            this.Property(t => t.DesSerie).HasColumnName("DesSerie");
            this.Property(t => t.DatImpressao).HasColumnName("DatImpressao");
            this.Property(t => t.CodOpe).HasColumnName("CodOpe");
        }
    }
}
