using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_SLCB_LIST_DTO
    {
        public int COD { get; set; }
        public Nullable<int> COD_PESO { get; set; }
        public string SERIAL { get; set; }
        public Nullable<bool> LIDO { get; set; }
        public byte[] IMAGEM { get; set; }
        public Nullable<bool> RESPOSTA { get; set; }
        public string RESPOSTA_STRING { get; set; }
        public byte[] RESPOSTA_IMAGEM { get; set; }
        public string SKU { get; set; }
        public string IP { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
        public string LINHA { get; set; }
    }
}
