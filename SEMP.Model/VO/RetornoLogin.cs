﻿using System;
namespace SEMP.Model.VO
{
	[Serializable]
	public class RetornoLogin
	{

		public string CodDRT { get; set; }
		public string NomFuncionario { get; set; }
		public string Depto { get; set; }
		public string Cargo { get; set; }
		public string Cracha { get; set; }
		public string NomUsuario { get; set; }
		public bool UsuarioCracha { get; set; }
		public Mensagem Status { get; set; }

		public RetornoLogin() {
			Status = new Mensagem();
		}

	}
}
