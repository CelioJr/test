using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_ProgProcesso
    {
        public string CodTipo { get; set; }
        public string CodProcesso { get; set; }
        public string DesTipo { get; set; }
        public string DesTitulo { get; set; }
        public System.Guid rowguid { get; set; }
    }
}
