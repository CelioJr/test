using System;

namespace SEMP.Model.DTO
{
    public class ValidacaoDTO
    {
        public string CodRotina { get; set; }
        public string CodChave { get; set; }
        public Nullable<System.DateTime> DatInicial { get; set; }
        public string Descricao { get; set; }
        public string Status { get; set; }
        public Nullable<System.DateTime> DatStatus { get; set; }
        public string NomUsuario { get; set; }
        public int QtdValida { get; set; }
        public Nullable<int> TempoValida { get; set; }
        public System.Guid rowguid { get; set; }
        public string NomUsuarioCad { get; set; }
        public string NomEstacao { get; set; }
        public string ExecComando { get; set; }
        public string ExecComandoRep { get; set; }
        public string flgOrdem { get; set; }
        public string Observacao { get; set; }
    }
}
