using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBHorarioIntervalo
    {
        public int CodLinha { get; set; }
        public int Turno { get; set; }
        public string TipoIntervalo { get; set; }
        public string HoraInicio { get; set; }
        public string HoraFim { get; set; }
        public Nullable<int> TempoIntervalo { get; set; }
    }
}
