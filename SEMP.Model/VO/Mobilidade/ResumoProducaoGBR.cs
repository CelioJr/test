using System;
using System.Collections.Generic;

namespace SEMP.Model.VO.Mobilidade
{
    public partial class ResumoProducaoGBR
    {
        public int? COD_SKU { get; set; }
		public string SKU { get; set; }
		public DateTime DATA { get; set; }
		public int QTD_PLAN { get; set; }
		public int QTD_PROD { get; set; }
	}
}
