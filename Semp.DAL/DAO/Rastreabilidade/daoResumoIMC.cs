﻿using SEMP.DAL.Models;
using SEMP.Model.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.DAO.Rastreabilidade
{
	public class daoResumoIMC
	{
        //static daoResumoIMC() {
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<tbl_ProgDiaPlaca, ProgDiaPlacaDTO>();
        //        cfg.CreateMap<tbl_ProdDiaPlaca, ProdDiaPlacaDTO>();
        //        cfg.CreateMap<tbl_CapacLinha, CapacLinhaDTO>();

        //    });
        //}

		public static List<CBLinhaDTO> LinhasIMC(string Setor, DateTime? datProducao)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				var linhas = (from l in dtbCb.tbl_CBLinha
							  from o in dtbCb.tbl_CBLinhaObservacao.Where(x => l.CodLinha == x.CodLinha && x.DatProducao == datProducao).DefaultIfEmpty()
							  where l.Setor == Setor && l.CodLinhaT1.Trim().Substring(0, 3) == "111"
							  select new CBLinhaDTO()
							  {
								  CodLinha = l.CodLinha,
								  CodFam = l.CodFam,
								  CodLinhaT1 = l.CodLinhaT1,
								  CodModelo = l.CodModelo,
								  CodPlaca = l.CodPlaca,
								  DesLinha = l.DesLinha,
								  DesObs = l.DesObs,
								  Observacao = o.Observacao,
								  Familia = l.Familia,
								  Setor = l.Setor,
                                  FlgAtivo = l.flgAtivo
							  }).ToList();

				return linhas;
			}
		}

		public static List<CBLinhaDTO> LinhasIMCSetor(string Setor, DateTime? datProducao)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				var linhas = (from l in dtbCb.tbl_CBLinha
							  from o in dtbCb.tbl_CBLinhaObservacao.Where(x => l.CodLinha == x.CodLinha && x.DatProducao == datProducao).DefaultIfEmpty()
							  where l.Setor == Setor && l.flgAtivo == "1"
							  select new CBLinhaDTO()
							  {
								  CodLinha = l.CodLinha,
								  CodFam = l.CodFam,
								  CodLinhaT1 = l.CodLinhaT1,
								  CodModelo = l.CodModelo,
								  CodPlaca = l.CodPlaca,
								  DesLinha = l.DesLinha,
								  DesObs = l.DesObs,
								  Observacao = o.Observacao,
								  Familia = l.Familia,
								  Setor = l.Setor,
								  FlgAtivo = l.flgAtivo
							  }).ToList();

				return linhas;
			}
		}

		public static List<ProgDiaPlacaDTO> ObterProgramaByLinha(string CodLinha, int Turno, DateTime DatInicio, DateTime DatFim)
		{
			try
			{
				using (DTB_SIMContext dtbSim = new DTB_SIMContext())
				{
					var programa = (from p in dtbSim.tbl_ProgDiaPlaca
									where p.CodLinha == CodLinha && p.NumTurno == Turno &&
											p.DatProducao >= DatInicio && p.DatProducao <= DatFim
									group p by new { p.CodLinha, p.NumTurno, p.CodModelo } into g
									select new
									{
										CodLinha = g.Key.CodLinha,
										NumTurno = g.Key.NumTurno,
										CodModelo = g.Key.CodModelo,
										QtdProdPM = g.Sum(s => s.QtdProdPM)
									}).ToList();

					List<tbl_ProgDiaPlaca> prog = programa.Select(i => new tbl_ProgDiaPlaca
					{
						CodLinha = i.CodLinha,
						NumTurno = i.NumTurno,
						CodModelo = i.CodModelo,
						QtdProdPM = i.QtdProdPM
					}).ToList();

					List<ProgDiaPlacaDTO> progDTO = new List<ProgDiaPlacaDTO>();
					AutoMapper.Mapper.Map(prog, progDTO);

					return progDTO;
				}
			}
			catch
			{
				return null;
			}
		}

		public static List<ProdDiaPlacaDTO> ObterProduzidoByLinha(string CodLinha, int Turno, DateTime DatInicio, DateTime DatFim)
		{
			try
			{
				using (DTB_SIMContext dtbSim = new DTB_SIMContext())
				{
					var produzido = (from p in dtbSim.tbl_ProdDiaPlaca
									 where p.CodLinha == CodLinha && p.NumTurno == Turno && (p.DatProducao >= DatInicio && p.DatProducao <= DatFim)
									 group p by new { p.CodLinha, p.NumTurno, p.CodPlaca } into g
									 select new
									 {
										 CodLinha = g.Key.CodLinha,
										 NumTurno = g.Key.NumTurno,
										 CodPlaca = g.Key.CodPlaca,
										 QtdProduzida = g.Sum(s => s.QtdProduzida),
										 QtdApontada = g.Sum(s => s.QtdApontada)
									 }).ToList();

					List<tbl_ProdDiaPlaca> prod = produzido.Select(i => new tbl_ProdDiaPlaca
					{
						CodLinha = i.CodLinha,
						NumTurno = i.NumTurno,
						CodPlaca = i.CodPlaca,
						QtdProduzida = i.QtdProduzida,
						QtdApontada = i.QtdApontada
					}).ToList();

					List<ProdDiaPlacaDTO> progDTO = new List<ProdDiaPlacaDTO>();

					AutoMapper.Mapper.Map(prod, progDTO);

					return progDTO;
				}
			}
			catch
			{
				return null;
			}
		}

		public static List<ProdDiaPlacaDTO> ObterProduzidoLinhaTotal(string CodLinha, int Turno, DateTime DatInicio, DateTime DatFim)
		{
			try
			{
				using (DTB_SIMContext dtbSim = new DTB_SIMContext())
				{
					var produzido = (from p in dtbSim.tbl_ProdDiaPlaca
									 where p.CodLinha == CodLinha && p.NumTurno == Turno && (p.DatProducao >= DatInicio && p.DatProducao <= DatFim)
									 group p by new { p.CodLinha, p.NumTurno } into g
									 select new
									 {
										 CodLinha = g.Key.CodLinha,
										 NumTurno = g.Key.NumTurno,
										 QtdProduzida = g.Sum(s => s.QtdProduzida),
										 QtdApontada = g.Sum(s => s.QtdApontada)
									 }).ToList();

					List<tbl_ProdDiaPlaca> prod = produzido.Select(i => new tbl_ProdDiaPlaca
					{
						CodLinha = i.CodLinha,
						NumTurno = i.NumTurno,
						QtdProduzida = i.QtdProduzida,
						QtdApontada = i.QtdApontada
					}).ToList();

					List<ProdDiaPlacaDTO> progDTO = new List<ProdDiaPlacaDTO>();

					AutoMapper.Mapper.Map(prod, progDTO);

					return progDTO;
				}
			}
			catch
			{
				return null;
			}
		}

		public static List<CapacLinhaDTO> ObterCapacidadeByLinha(string CodLinha, int Turno, DateTime DatInicio, DateTime DatFim)
		{
			try
			{
				using (DTB_SIMContext dtbSim = new DTB_SIMContext())
				{
					var programa = (from p in dtbSim.tbl_CapacLinha
									where p.CodLinha == CodLinha && p.NumTurno == Turno &&
											p.DatProducao >= DatInicio && p.DatProducao <= DatFim
									group p by new { p.CodLinha, p.NumTurno, p.CodModelo, p.DatProducao } into g
									select new
									{
										CodLinha = g.Key.CodLinha,
										NumTurno = g.Key.NumTurno,
										CodModelo = g.Key.CodModelo,
										DatProducao = g.Key.DatProducao,
										QtdCapacLinha = g.Sum(s => s.QtdCapacLinha)
									});

					List<tbl_CapacLinha> prog = programa.ToList().Select(i => new tbl_CapacLinha
					{
						CodLinha = i.CodLinha,
						NumTurno = i.NumTurno,
						CodModelo = i.CodModelo,
						QtdCapacLinha = i.QtdCapacLinha
					}).ToList();

					List<CapacLinhaDTO> capacLinhaDTO = new List<CapacLinhaDTO>();

					AutoMapper.Mapper.Map(prog, capacLinhaDTO);

					return capacLinhaDTO;
				}
			}
			catch
			{
				return null;
			}
		}

	}
}
