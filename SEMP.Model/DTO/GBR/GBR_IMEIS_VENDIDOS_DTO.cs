using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_IMEIS_VENDIDOS_DTO
    {
        public int COD_LISTA_IMEI { get; set; }
        public string IMEI { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
        public string USUARIO { get; set; }
        public string MATERIAL { get; set; }
        public string DEPOSITO { get; set; }
        public string CENTRO { get; set; }
        public string STATUS { get; set; }
    }
}
