﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.Rastreabilidade;
using SEMP.Model;
using SEMP.BO;

namespace SEMP.Areas.Coleta.Controllers
{
	public class OperacoesController : Controller
	{
		//
		// GET: /Coleta/Operacoes/ValidaPassagem/?numecb=&cracha=
		public string ValidaPassagem(string numecb, string cracha)
		{
			if (string.IsNullOrEmpty(cracha))
			{
				return "ERRO|CRACHÁ INVÁLIDO";
			}

			if (string.IsNullOrEmpty(numecb))
			{
				return "ERRO|ETIQUETA INVÁLIDA";
			}

			var func = Login.ValidaCracha(cracha.Substring(0, cracha.Length-1));

			if (func.Status.CodMensagem == Constantes.DB_ERRO) return String.Format("ERRO|{0}", func.Status.DesMensagem);

			var posto = Geral.ObterPostoPorDRT(func.CodDRT);

			if (posto == null || posto.CodDRT == null) return "ERRO|DRT NÃO ASSOCIADO";

			var p = new Passagem(posto.CodLinha, posto.NumPosto);

			var valida = p.ValidaPassagem(numecb, func.CodDRT);

			if (valida.CodMensagem.Equals(Constantes.RAST_OK) || valida.CodMensagem.Equals(Constantes.DB_OK))
				return String.Format("OK|{0}", func.NomFuncionario);

			return String.Format("ERRO|{0}", valida.DesMensagem);

		}

		//
		// GET: /Coleta/Operacoes/GravaPassagem/?numecb=&cracha=
		public string GravaPassagem(string numecb, string cracha)
		{
			if (string.IsNullOrEmpty(cracha))
			{
				return "ERRO|CRACHÁ INVÁLIDO";
			}

			if (string.IsNullOrEmpty(numecb))
			{
				return "ERRO|ETIQUETA INVÁLIDA";
			}

			var func = Login.ValidaCracha(cracha.Substring(0, cracha.Length - 1));

			if (func.Status.CodMensagem == Constantes.DB_ERRO) return String.Format("ERRO|{0}", func.Status.DesMensagem);

			var posto = Geral.ObterPostoPorDRT(func.CodDRT);

			if (posto == null || posto.CodDRT == null) return "ERRO|DRT NÃO ASSOCIADO";

			var p = new Passagem(posto.CodLinha, posto.NumPosto);

			var valida = p.GravarPassagem(numecb, func.CodDRT);

			if (valida.CodMensagem.Equals(Constantes.RAST_OK) || valida.CodMensagem.Equals(Constantes.DB_OK))
				return String.Format("OK|{0}", func.NomFuncionario);

			return String.Format("ERRO|{0}", valida.DesMensagem);

		}

		//
		// GET: /Coleta/Operacoes/GravaDefeito/?numecb=&cracha=&coddefeito=
		public string GravaDefeito(string numecb, string cracha, string codDefeito)
		{
			if (string.IsNullOrEmpty(cracha))
			{
				return "ERRO|CRACHÁ INVÁLIDO";
			}

			if (string.IsNullOrEmpty(numecb))
			{
				return "ERRO|ETIQUETA INVÁLIDA";
			}

			var func = Login.ValidaCracha(cracha.Substring(0, cracha.Length - 1));

			if (func.Status.CodMensagem == Constantes.DB_ERRO) return String.Format("ERRO|{0}", func.Status.DesMensagem);

			var posto = Geral.ObterPostoPorDRT(func.CodDRT);

			if (posto == null || posto.CodDRT == null) return "ERRO|DRT NÃO ASSOCIADO";

			var valida = Defeito.GravarDefeito(posto.CodLinha, posto.NumPosto, numecb, codDefeito, posto.CodDRT, "");

			if (valida.CodMensagem.Equals(Constantes.RAST_OK) || valida.CodMensagem.Equals(Constantes.DB_OK))
				return String.Format("OK|{0}", func.NomFuncionario);

			return String.Format("ERRO|{0}", valida.DesMensagem);

		}

	}
}
