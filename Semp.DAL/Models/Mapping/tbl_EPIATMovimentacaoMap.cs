using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_EPIATMovimentacaoMap : EntityTypeConfiguration<tbl_EPIATMovimentacao>
    {
        public tbl_EPIATMovimentacaoMap()
        {
            // Primary Key
            this.HasKey(t => t.CodMovimentacao);

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodMovimentacao)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.DocInterno1)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.DocInterno2)
                .IsFixedLength()
                .HasMaxLength(9)
                .IsUnicode(false);

            this.Property(t => t.CodItem)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodTra)
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.CodLocal)
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.DocExterno)
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.NomUsuario)
                .HasMaxLength(30)
                .IsUnicode(false);

            this.Property(t => t.NomEstacao)
                .HasMaxLength(30)
                .IsUnicode(false);

            this.Property(t => t.CodGalpao)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodRua)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodBloco)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.CodApto)
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.SubApto)
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.CodNivel)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.SubNivel)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodLocalAP)
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.CodModeloAP)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.AgeExterno)
                .IsFixedLength()
                .HasMaxLength(4)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_EPIATMovimentacao");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.CodMovimentacao).HasColumnName("CodMovimentacao");
            this.Property(t => t.DocInterno1).HasColumnName("DocInterno1");
            this.Property(t => t.DocInterno2).HasColumnName("DocInterno2");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.CodTra).HasColumnName("CodTra");
            this.Property(t => t.CodLocal).HasColumnName("CodLocal");
            this.Property(t => t.DatTra).HasColumnName("DatTra");
            this.Property(t => t.DocExterno).HasColumnName("DocExterno");
            this.Property(t => t.QtdTra).HasColumnName("QtdTra");
            this.Property(t => t.DatDig).HasColumnName("DatDig");
            this.Property(t => t.QtdRecebida).HasColumnName("QtdRecebida");
            this.Property(t => t.DatRecebida).HasColumnName("DatRecebida");
            this.Property(t => t.NomUsuario).HasColumnName("NomUsuario");
            this.Property(t => t.NomEstacao).HasColumnName("NomEstacao");
            this.Property(t => t.CodGalpao).HasColumnName("CodGalpao");
            this.Property(t => t.CodRua).HasColumnName("CodRua");
            this.Property(t => t.CodBloco).HasColumnName("CodBloco");
            this.Property(t => t.CodApto).HasColumnName("CodApto");
            this.Property(t => t.SubApto).HasColumnName("SubApto");
            this.Property(t => t.CodNivel).HasColumnName("CodNivel");
            this.Property(t => t.SubNivel).HasColumnName("SubNivel");
            this.Property(t => t.CodLocalAP).HasColumnName("CodLocalAP");
            this.Property(t => t.CodModeloAP).HasColumnName("CodModeloAP");
            this.Property(t => t.AgeExterno).HasColumnName("AgeExterno");
            this.Property(t => t.QtdLibRec).HasColumnName("QtdLibRec");
        }
    }
}
