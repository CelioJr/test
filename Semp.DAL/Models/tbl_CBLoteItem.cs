﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.Models
{
    public class tbl_CBLoteItem
    {
        public string NumLote {get;set;}
        public string NumCC {get;set;}

        public string NumECB {get;set;}

        public int Status {get;set;}

        public Nullable<bool> flgAmostra { get; set; }

        public Nullable<System.DateTime> DatInclusao { get; set; }
        
        public Nullable<System.DateTime> DatInspecao { get; set; }

        public string ResultadoInspecao { get; set; }

    }
}