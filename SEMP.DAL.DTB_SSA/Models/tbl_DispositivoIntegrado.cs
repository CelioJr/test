using System;

namespace SEMP.DAL.DTB_SSA.Models
{
    public partial class tbl_DispositivoIntegrado
    {
        public string NE { get; set; }
        public int dispositivo_id { get; set; }
        public Nullable<int> frequencia { get; set; }
    }
}
