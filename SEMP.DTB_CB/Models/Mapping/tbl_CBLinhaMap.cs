using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DTB_CB.Models.Mapping
{
    public class tbl_CBLinhaMap : EntityTypeConfiguration<tbl_CBLinha>
    {
        public tbl_CBLinhaMap()
        {
            // Primary Key
            this.HasKey(t => t.CodLinha);

            // Properties
            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DesLinha)
                .HasMaxLength(50);

            this.Property(t => t.DesObs)
                .HasMaxLength(50);

            this.Property(t => t.Setor)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.Familia)
                .HasMaxLength(10);

            this.Property(t => t.CodLinhaT1)
                .IsFixedLength()
                .HasMaxLength(8);

            this.Property(t => t.CodLinhaT2)
                .IsFixedLength()
                .HasMaxLength(8);

            this.Property(t => t.CodLinhaT3)
                .IsFixedLength()
                .HasMaxLength(8);

            this.Property(t => t.CodPlaca)
                .IsFixedLength()
                .HasMaxLength(6);

            this.Property(t => t.CodModelo)
                .IsFixedLength()
                .HasMaxLength(6);

            this.Property(t => t.CodAuxiliar)
                .IsFixedLength()
                .HasMaxLength(6);

            this.Property(t => t.CodFam)
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("tbl_CBLinha");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.DesLinha).HasColumnName("DesLinha");
            this.Property(t => t.DesObs).HasColumnName("DesObs");
            this.Property(t => t.Setor).HasColumnName("Setor");
            this.Property(t => t.Familia).HasColumnName("Familia");
            this.Property(t => t.CodLinhaT1).HasColumnName("CodLinhaT1");
            this.Property(t => t.CodLinhaT2).HasColumnName("CodLinhaT2");
            this.Property(t => t.CodLinhaT3).HasColumnName("CodLinhaT3");
            this.Property(t => t.CodPlaca).HasColumnName("CodPlaca");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.CodAuxiliar).HasColumnName("CodAuxiliar");
            this.Property(t => t.CodFam).HasColumnName("CodFam");
        }
    }
}
