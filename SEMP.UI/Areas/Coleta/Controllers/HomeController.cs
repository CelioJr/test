﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.Rastreabilidade;
using SEMP.Model;
using SEMP.Model.VO.Rastreabilidade;
using SEMP.Model.DTO;
using SEMP.BO;
using RAST.BO.Embalagem;


namespace SEMP.Areas.Coleta.Controllers
{
	/// <summary>
	/// Classe que controla as Action da página inicial da area Coleta.
	/// </summary>
	[Authorize]
	public class HomeController : Controller
	{
		/// <summary>
		/// Action Index método que controla a pagina Inicial da Home.
		/// </summary>
		/// <returns>Retorna a View Index.cshtml</returns>
		public ActionResult Index()
		{
			try
			{
				HttpCookie cookie = Request.Cookies["Posto"];

				if (cookie == null)
				{
					return RedirectToAction("Index", "Setup");
				}

				int codLinha = int.Parse(cookie["codLinha"]);
				int numPosto = int.Parse(cookie["numPosto"]);

				Posto posto = new Posto(codLinha, numPosto);

				Session.Add("DadosPosto", posto.posto);

				if (posto.posto.DesAcao.Equals("Passagem") && posto.posto.flgPedeAP.Equals("S"))
				{
					posto.posto.DesAcao = "Amarracao";
				}

				if (posto.posto.DesAcao.Equals("Embalagem") && posto.posto.flgPedeAP.Equals("S"))
				{
					posto.posto.DesAcao = "AmarraLote";
				}

				if (posto.posto.DesAcao.Equals("AmarraLoteConfigIAC"))
					return RedirectToAction(posto.posto.DesAcao, "CriarLoteIAC");

				return RedirectToAction(posto.posto.DesAcao, "Home");
			}
			catch (Exception)
			{
				return RedirectToAction("Index", "Setup");
			}

		}

		/// <summary>
		/// Action Passagem método que controla a pagina Passagem.
		/// </summary>
		/// <returns>Retorna a View Passagem.cshtml</returns>
		public ActionResult Passagem() {

			HttpCookie cookie = Request.Cookies["Posto"];

			if (cookie == null || Session["DadosPosto"] == null)
			{
				return RedirectToAction("Index", "Setup");
			}

			DateTime dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			DateTime dFim = dIni.AddDays(1).AddMinutes(-1);

			var postoCookie = Request.Cookies["Posto"];

			Passagem passagem = new Passagem((DadosPosto)Session["DadosPosto"]);
			ViewBag.passagens = passagem.ObterPassagemPosto();
			var posto = Geral.ObterPosto(int.Parse(postoCookie.Values["codLinha"]), int.Parse(postoCookie.Values["numPosto"]));
			var linha = Geral.ObterLinha(int.Parse(postoCookie.Values["codLinha"]));

			ViewBag.Fabrica = postoCookie.Values["Fabrica"];
			ViewBag.Posto = posto.DesPosto;
			ViewBag.Linha = linha.DesLinha;
			ViewBag.CodTipoPosto = posto.CodTipoPosto;

			Geral.ObterTotalAprovadas(int.Parse(postoCookie.Values["codLinha"]), int.Parse(postoCookie.Values["numPosto"]));
			Geral.ObterTotalReprovadas(int.Parse(postoCookie.Values["codLinha"]), int.Parse(postoCookie.Values["numPosto"]));
			Geral.ObterTotal(int.Parse(postoCookie.Values["codLinha"]), int.Parse(postoCookie.Values["numPosto"]));

			return View(Session["DadosPosto"]);
		}

		/// <summary>
		///  Action Amarracao método que controla a pagina Amarracao.
		/// </summary>
		/// <returns>Retorna a View Amarracao.cshtml</returns>
		public ActionResult Amarracao(){

			var postoCookie = Request.Cookies["Posto"];
			
			var posto = Geral.ObterPosto(int.Parse(postoCookie.Values["codLinha"]), int.Parse(postoCookie.Values["numPosto"]));
			var linha = Geral.ObterLinha(int.Parse(postoCookie.Values["codLinha"]));

			ViewBag.Fabrica = postoCookie.Values["Fabrica"];
			ViewBag.Posto = posto.DesPosto;
			ViewBag.Linha = linha.DesLinha;

			CBAmarraDTO amarra = new CBAmarraDTO();
			amarra.DataReferencia = DateTime.Now.ToShortDateString();

			return View(amarra);
			
		}

		/// <summary>
		/// Método responsável por receber e validar os dados de uma AP e redirecionar o usuária a tela do posto de amarração.
		/// </summary>
		/// <param name="CBAmarraDTO"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult Amarracao(CBAmarraDTO amarra)
		{
			var sessao = Session;

			try
			{
				if (sessao != null)
				{
					var postoCookie = Request.Cookies["Posto"];

					var posto = Geral.ObterPosto(int.Parse(postoCookie.Values["codLinha"]), int.Parse(postoCookie.Values["numPosto"]));
					var linha = Geral.ObterLinha(int.Parse(postoCookie.Values["codLinha"]));

					ViewBag.Fabrica = postoCookie.Values["Fabrica"];
					ViewBag.Posto = posto.DesPosto;
					ViewBag.Linha = linha.DesLinha;

					DateTime DataRef = DateTime.Now;
					string validarAP = "";
					bool dataReferenciaValida = true;
					bool validarCrachaSupervisor = true;

					try
					{
						if (!string.IsNullOrWhiteSpace(amarra.DataReferencia))
						{
							DataRef = DateTime.Parse(amarra.DataReferencia);  
						}
						else
						{
							dataReferenciaValida = false;
						}
					}
					catch 
					{
						dataReferenciaValida = false;
					}

					RIFuncionarioDTO funcionario = null;

					if (dataReferenciaValida)
					{
						string cracha = string.Empty;

						if (!string.IsNullOrWhiteSpace(amarra.CrachaSupervisor))
						{
							cracha = amarra.CrachaSupervisor.Substring(0, 11);
						}

						funcionario = Login.ObterFuncionarioByCPF(cracha);
						DateTime dataDeHoje = DateTime.Now;

						if (DataRef.Date < dataDeHoje.Date)
						{
							if ((funcionario == null || String.IsNullOrEmpty(funcionario.CodDRT)) && funcionario.CodCargo != "79")
							{
								validarCrachaSupervisor = false;
							}
						}
					}

					validarAP = Geral.ValidarAPAmarra(amarra.CodFab, amarra.NumAP, DataRef, linha, posto.NumPosto);

					if (ModelState.IsValid && dataReferenciaValida == true && string.IsNullOrWhiteSpace(validarAP) && validarCrachaSupervisor)
					{
						int codFabrica = int.Parse(amarra.CodFab);
						var numeroAP = amarra.NumAP;

						string codModelo = Geral.ObterCodigoModelo(codFabrica.ToString("00"), numeroAP);
						string descItem = Geral.ObterDescricaoItem(codModelo);

						Passagem passagem = new Passagem((DadosPosto)Session["DadosPosto"]);

						var perfil = RAST.BO.Rastreabilidade.Amarracao.GetPerfilPosto(passagem.posto.CodLinha, passagem.posto.NumPosto, codModelo.ToString(), codFabrica.ToString("00"), numeroAP);

						Session["GetPerfilPosto"] = perfil.ToList();

						List<string> colunas = perfil.Where(a => a.DesTipoAmarra != "EAN13").Select(s => s.DesTipoAmarra).ToList();

						colunas.Add("Data Leitura");

						List<string> listaColunas = new List<string>();

						colunas.ForEach(f => listaColunas.Add(f.Replace("\"", "")));

						ViewBag.ColunasHistorico = string.Join(",", listaColunas.ToArray());
						ViewBag.Fabrica = postoCookie.Values["Fabrica"];
						ViewBag.Posto = posto.DesPosto;
						ViewBag.CodTipoPosto = posto.CodTipoPosto;
						ViewBag.Linha = linha.DesLinha;
						ViewBag.CodigoFabrica = codFabrica.ToString("00");
						ViewBag.NumeroAP = numeroAP;
						ViewBag.Modelo = codModelo;
						ViewBag.DescItem = descItem;
						ViewBag.Turno = null;// turno;
						ViewBag.DataReferencia = null;// data;
						ViewBag.TempoLeitura = posto.TempoLeitura == null ? 5 : posto.TempoLeitura.Value;
						ViewBag.Perfil = string.Join(",", perfil.Select(s => s.DesTipoAmarra.Replace("\"", "")).ToArray());
						ViewBag.PerfilTipo = string.Join(",", perfil.Select(s => s.CodTipoAmarra).ToArray());
						ViewBag.PostoPassagem = posto.CodTipoPosto == 2 || posto.CodTipoPosto == 27 ? true : false;//perfil.Count() == 1 ? true : false;

						if(posto.CodTipoPosto != 18)
							return View("PostoAmarracao");
						else
							return View("Recebimento");
						
					}
					else
					{
						if (amarra.CodFab == null)
						{
							ModelState.AddModelError("CodFab", "Informe o código da fábrica.");
						}
						else if (amarra.NumAP == null)
						{
							ModelState.AddModelError("NumAP", "Informe o número da AP.");
						}
						else if(amarra.DataReferencia == null)
						{
							ModelState.AddModelError("DataReferencia", "Informe a data de referência.");
						}

						if (dataReferenciaValida == false)
						{
							ModelState.AddModelError("", "Data de referência inválida.");
						}

						if (!string.IsNullOrWhiteSpace(validarAP))
						{
							ModelState.AddModelError("", validarAP);
						}

						if (validarCrachaSupervisor == false)
						{
							ModelState.AddModelError("", "O código de crachá deve ser de um supervisor.");
						}

						return View(amarra);
					}

				}
				else
				{
					ModelState.AddModelError("", "Sessão esgotada, faça novamente o login no sistema.");
					return View(amarra);
				}

			}
			catch(Exception ex)
			{
				ModelState.AddModelError("", ex.Message);
				return View(amarra);
			}
		}

		/// <summary>
		///  Action Amarracao método que controla a pagina Amarracao.
		/// </summary>
		/// <returns>Retorna a View Amarracao.cshtml</returns>
		public ActionResult AmarraLote()
		{
			try
			{
				var postoCookie = Request.Cookies["Posto"];

				var posto = Geral.ObterPosto(int.Parse(postoCookie.Values["codLinha"]), int.Parse(postoCookie.Values["numPosto"]));
				var linha = Geral.ObterLinha(int.Parse(postoCookie.Values["codLinha"]));

				ViewBag.Fabrica = postoCookie.Values["Fabrica"];
				ViewBag.Posto = posto.DesPosto;
				ViewBag.Linha = linha.DesLinha;
				ViewBag.Setor = linha.Setor;
				ViewBag.PostoBloqueado = posto.PostoBloqueado;

			}
			catch
			{
				return RedirectToAction("Index");                
			}

			CBEmbaladaDTO embalada = new CBEmbaladaDTO();
			embalada.DatReferencia = DateTime.Now.ToShortDateString();

			return View(embalada);
		}

		[HttpPost]
		public ActionResult AmarraLote(CBEmbaladaDTO embalada)
		{
			var sessao = Session;
			bool error = false;

			try
			{
				if (sessao != null)
				{
					var postoCookie = Request.Cookies["Posto"];
					var posto = Geral.ObterPosto(int.Parse(postoCookie.Values["codLinha"]), int.Parse(postoCookie.Values["numPosto"]));
					var linha = Geral.ObterLinha(int.Parse(postoCookie.Values["codLinha"]));
					DateTime DataRef = DateTime.Now;
					string validarAP = string.Empty;
					bool dataReferenciaValida = true;
					string mensagemErro = string.Empty;
					string codModeloIAC_IMC = string.Empty;

					ViewBag.Posto = posto.DesPosto;
					ViewBag.Linha = linha.DesLinha;
					ViewBag.Setor = linha.Setor;
					
					#region Recuperando APs que foram informadas para leitura
						//adiciona na lista de APs a primeira AP, que é obrigatória.
						embalada.dadosAP = new List<CBEmbaladaDTO.DadosAP>
						{
							new CBEmbaladaDTO.DadosAP() {CodModelo = "", DesModelo = "", numAP = embalada.NumAP.ToUpper()}
						};

						//Recuperando APs informadas após a primeira
						var quantidadeApInformada = Request.Form["contAP"];

						for (int i = 0; i < int.Parse(quantidadeApInformada); i++)
							embalada.dadosAP.Add(new CBEmbaladaDTO.DadosAP() { CodModelo = "", DesModelo = "", numAP = Request.Form["numAP_" + (i + 1)].ToString() });

					#endregion 

					#region validação de posto bloqueado
						if (posto.PostoBloqueado != null)
						{
							if (posto.PostoBloqueado.Equals("S"))
							{
								if (string.IsNullOrEmpty(embalada.CrachaSupervisor))
								{
									error = true;
									mensagemErro = "Posto bloqueado por leitura repetida. Favor Solicitar ao supervisor a liberação";
								}
								else
								{
									var cracha = string.Empty;

									if (!string.IsNullOrWhiteSpace(embalada.CrachaSupervisor))
									{
										cracha = embalada.CrachaSupervisor.Substring(0, 11);
									}

									var funcionario = Login.ObterFuncionarioByCPF(cracha);

									if (funcionario == null || String.IsNullOrEmpty(funcionario.CodDRT) || !funcionario.CodCargo.Trim().Equals("079"))
									{
										error = true;
										mensagemErro = "Crachá do supervisor inválido";
									}
									else
									{
										try
										{
											var blnOk = Embalagem.DesbloquearPosto(new Passagem((DadosPosto)Session["DadosPosto"]));

											if (blnOk)
											{
												posto.PostoBloqueado = "N";
											}
											else
											{
												error = true;
												mensagemErro = "Erro ao desbloquear Posto";
											}
										}
										catch (Exception ex)
										{
											error = true;
											mensagemErro = "Erro ao desbloquear Posto" + ex.Message;
										}
									}
								}
							}

							ViewBag.PostoBloqueado = (string.IsNullOrEmpty(posto.PostoBloqueado.Trim()) ? "N" : posto.PostoBloqueado.Trim());
						}
						else
						{
							ViewBag.PostoBloqueado = "N";
						}
					#endregion
					
					#region Validando datas e APs
					if (!error)
					{
						try
						{
							if (Convert.ToDateTime(embalada.DatReferencia) != DateTime.Today)
								dataReferenciaValida = false;

							DataRef = Convert.ToDateTime(embalada.DatReferencia);  
						}
						catch 
						{
							dataReferenciaValida = false;
							error = true;
						}

						if (posto.flgImprimeEtq == "S" && string.IsNullOrEmpty(posto.NumIP))
						{
							error = true;
							mensagemErro = "Posto configurado para imprimir etiquetas. Favor configurar IP do Servidor de Impressão";
						}
					
						 
							foreach (var dados in embalada.dadosAP)
							{
								validarAP = Geral.ValidarAP(embalada.CodFab, dados.numAP, DataRef, linha);
						
								if (!string.IsNullOrWhiteSpace(validarAP))
								{
									error = true;
									break;
								}
							}
					}
					#endregion 
					
					if (ModelState.IsValid && !error)
					{                   
						int codFabrica = int.Parse(embalada.CodFab);

						var APs = new List<LSAAPDTO>();

						#region Formatando Cabeçalho da pagina, onde ficam as informações das APs
						var ResumoDadosAps = " <div style='width: 380px; height: 18px; overflow: hidden;' title='" + linha.CodLinha + "-" + linha.DesLinha + "-" + posto.DesPosto + "'><b> Estação: </b> " +  linha.DesLinha + " | " + posto.DesPosto + " </div>";
							foreach (var dados in embalada.dadosAP)
							{
								/*dados.CodModelo = Geral.ObterCodigoModelo(codFabrica.ToString("00"), Convert.ToInt32(dados.numAP).ToString("000000"));
								dados.DesModelo = Geral.ObterDescricaoItem(dados.CodModelo);*/
								var ap = Geral.ObterAP(codFabrica.ToString("00"), dados.numAP.Trim().PadLeft(6, '0'));
								ap.DesModelo = Geral.ObterDescricaoItem(ap.CodModelo);
								APs.Add(ap);

							//dados.CodModelo = Geral.ObterCodigoModelo(codFabrica.ToString("00"), Convert.ToInt32(dados.numAP).ToString("000000"));
							dados.CodModelo = ap.CodModelo;
							dados.DesModelo = ap.DesModelo;
							ResumoDadosAps += String.Format(" <b> AP: </b> {0} - {1} &nbsp;&nbsp; | <b> Qtd. AP: </b><span id='qtdLote'>{2}</span> &nbsp;&nbsp; / <b> Embalado: </b> <span id='qtdEmbalado_{1}'>{3}</span> </br> <b> Modelo: </b> {4} - {5} </br>", 
								postoCookie.Values["Fabrica"], 
								dados.numAP, 
								ap.QtdLoteAP, 
								ap.QtdEmbalado,
								dados.CodModelo,
								dados.DesModelo);
						}
								
						#endregion 

						int qtdLote = Geral.ObterQuantidadeLotePorLinha(linha.CodLinha);
						int qtdCC = Geral.ObterQuantidadeCaixaColetivaPorLinha(linha.CodLinha);
						int turno = Geral.RecuperarTurnoAtual(linha.CodLinha);

						Passagem passagem = new Passagem((DadosPosto)Session["DadosPosto"]);
						passagem.posto.flgImprimeEtq = (string.IsNullOrEmpty(passagem.posto.flgImprimeEtq) ? "N" : "S");//(string.IsNullOrWhiteSpace(passagem.posto.flgImprimeEtq)) ? "N" : passagem.posto.flgImprimeEtq);

						List<viw_CBPerfilAmarraDTO> perfil = new List<viw_CBPerfilAmarraDTO>();
						
						foreach(var dados in embalada.dadosAP)
						{
							perfil.AddRange(RAST.BO.Rastreabilidade.Amarracao.GetPerfilPosto(passagem.posto.CodLinha, passagem.posto.NumPosto, dados.CodModelo, codFabrica.ToString("00"), String.Format("{000000:0}", dados.numAP.Trim())));
						}

						perfil.ForEach(a => a.numAP = String.Format("{000000:0}", a.numAP.Trim()));
						
						// o trecho "embalada.dadosAP[0].numAP" é usado no código porque CodigoCinescopio só será usado no setor FEC. Para este setor só existe uma AP
						switch (linha.Setor)
						{
							case "FEC":
								Session["CodigoCinescopio"] = Geral.GetCodigoCinescopio(embalada.CodFab, embalada.dadosAP[0].numAP, DataRef);
								break;
							
							case "IAC":
								var apIAC = APs.FirstOrDefault(x => x.NumAP == embalada.dadosAP[0].numAP);
								codModeloIAC_IMC = apIAC.CodRadial;
								if (String.IsNullOrEmpty(codModeloIAC_IMC)) {
									codModeloIAC_IMC = Embalagem.ObterModeloFase(embalada.dadosAP[0].CodModelo, "02");
								}
								perfil[0].Mascara = "^(" + codModeloIAC_IMC + "|" + embalada.dadosAP[0].CodModelo + ")(\\w{6})$";
								//--> 02 é a fase IMC. O ponto de parada para a procedure é IMC, já que eu quero o NE que roda na IMC
								break;

                            case "IMC":
                                var apIMC = APs.FirstOrDefault(x => x.NumAP == embalada.dadosAP[0].numAP);
                                codModeloIAC_IMC = apIMC.CodRadial;
                                if (String.IsNullOrEmpty(codModeloIAC_IMC))
                                {
                                    codModeloIAC_IMC = Embalagem.ObterModeloFase(embalada.dadosAP[0].CodModelo, "01");
                                }
                                perfil[0].Mascara = "^(" + codModeloIAC_IMC + "|" + embalada.dadosAP[0].CodModelo + ")(\\w{6})$";
                                //--> 02 é a fase IMC. O ponto de parada para a procedure é IMC, já que eu quero o NE que roda na IMC
                                break;
                        }
						
						Session["Posto"] = posto;
						Session["Turno"] = turno;
						Session["Linha"] = linha;
						Session["passagem"] = passagem;
						Session["GetPerfilPosto"] = perfil;
						
						if (linha.CodLinha.Equals(42))
						{   //somente para notebooks, o kit é incluído via código. isso acontece porque o kit não tem NE e nem família
							perfil.Add(new viw_CBPerfilAmarraDTO()
							{
								CodLinha = linha.CodLinha,
								NumPosto = posto.NumPosto,
								CodModelo = embalada.dadosAP[0].CodModelo,
								CodTipoAmarra = Constantes.AMARRA_KIT_NOTEBOOK,
								CodItem = "552499",     //código temporário. kit acessorio não tem ne. deve ser solicitado que seja criado ne para este item
								DesItem = "Kit Acessório Notebook",
								Mascara = "^(\\d{9,15})$",
								DesTipoAmarra = "Produto"
							});
						}
						//teste
						#region Preparando Grid de Histórico
						List<string> colunas = new List<string>
						{
							"Produto"     //feito assim para evitar ter dois "Produtos" quando existir leitura de duas APs
						};
						colunas.AddRange(perfil.Where(a => a.DesTipoAmarra != "EAN13" && a.DesTipoAmarra != "Produto").Select(s => s.DesTipoAmarra).ToList());                        
							colunas.Add("Data Leitura");
							colunas.Add("Caixa Coletiva");
							colunas.Add("Lote");
							ViewBag.ColunasHistorico = string.Join(",", colunas.ToArray());
						#endregion

						ViewBag.CodTipoPosto = posto.CodTipoPosto;
						ViewBag.CodPosto = posto.NumPosto;
						ViewBag.DesPosto = posto.DesPosto;
						ViewBag.CodLinha = linha.CodLinha;
						ViewBag.DesLinha = linha.DesLinha;
						ViewBag.CodLinhaT1 = linha.CodLinhaT1;

						ViewBag.DadosAPs = APs.Select(x => new { CodFab = x.CodFab, NumAP = x.NumAP, QtdLoteAP = x.QtdLoteAP, QtdEmbalado = x.QtdEmbalado, CodModelo = x.CodModelo, DesModelo = x.DesModelo }).ToList();

						ViewBag.Fabrica = postoCookie.Values["Fabrica"];
						ViewBag.CodigoFabrica = codFabrica.ToString("00");
						ViewBag.ResumoDadosAps = ResumoDadosAps;
						ViewBag.Turno = turno;
						ViewBag.DataReferencia = embalada.DatReferencia;
						ViewBag.Perfil = string.Join(",", perfil.Select(s => s.DesTipoAmarra).ToArray());
						ViewBag.PerfilAP = string.Join(",", perfil.Select(s => s.numAP).ToArray());                 
						ViewBag.QtdeLote = qtdLote;
						ViewBag.qtdeCC = qtdCC;
						ViewBag.TempoLeitura = posto.TempoLeitura == null ? 5 : posto.TempoLeitura.Value;
						ViewBag.ImprimeEtq = posto.flgImprimeEtq;

						if (linha.Setor == "IAC" || linha.Setor == "IMC")
						{
							return View("PostoAmarraLote");
						}
						else
						{
							return View("PostoAmarracaoLote");
						}
					}
					else
					{
						if (embalada.CodFab == null)
						{
							ModelState.AddModelError("CodFab", "Informe o código da fábrica.");
						}
						else if (embalada.NumAP == null)
						{
							ModelState.AddModelError("NumAP", "Informe o número da AP.");
						}
						else if (string.IsNullOrEmpty(embalada.DatReferencia))
						{
							ModelState.AddModelError("DatReferencia", "Informe a data de referência.");
						}

						if (dataReferenciaValida == false)
						{
							ModelState.AddModelError("", "Data de referência inválida.");
						}

						if (!string.IsNullOrWhiteSpace(validarAP))
						{
							ModelState.AddModelError("", validarAP);
						}

						if (!string.IsNullOrWhiteSpace(mensagemErro))
						{
							ModelState.AddModelError("", mensagemErro);
						}
						

						return View(embalada);
					}
				}
				else
				{
					ModelState.AddModelError("", "Sessão esgotada, faça novamente o login no sistema.");
					return View(embalada);
				}

			}
			catch(Exception ex)
			{
				ModelState.AddModelError("", ex.Message);
				return View(embalada);
			}
		}

		public JsonResult PreencherArrayAP () 
		{
			var perfil = (List<viw_CBPerfilAmarraDTO>)Session["GetPerfilPosto"];

			var listaApsLeitura = (from p in perfil
								   group p by p.numAP into grupo
								   select new { AP = grupo.Key, QtdEsp = grupo.Count(), QtdeLido = 0, Fab = grupo }).ToList();

			return Json(listaApsLeitura, JsonRequestBehavior.AllowGet);

		}

		/// <summary>
		/// Este método é responsável por chamar a tela que amarra a uma placa ou itens a um produto.
		/// </summary>
		/// <returns></returns>
		public ActionResult PostoAmarracao()
		{
			return View();
		}

		/// <summary>
		/// Este método é responsável por chamar a tela que amarra a uma placa ou itens a um produto, além de agrupar os produtos em lotes.
		/// </summary>
		/// <returns></returns>
		public ActionResult PostoAmarracaoLote()
		{
			return View();
		}

		public ActionResult PostoAmarraLote() {
			return View();
		}

		/// <summary>
		/// CarregaPerfil
		/// </summary>
		/// <param name="sidx"></param>
		/// <param name="sord"></param>
		/// <param name="page"></param>
		/// <param name="rows"></param>
		/// <param name="Fabrica"></param>
		/// <param name="NumAP"></param>
		/// <param name="Modelo"></param>
		/// <returns></returns>
		public ActionResult CarregaPerfil(string sidx, string sord, int page, int rows, string Fabrica, string NumAP, string Modelo)
		{
			Passagem passagem = new Passagem((DadosPosto)Session["DadosPosto"]);

			var context = (List<viw_CBPerfilAmarraDTO>)Session["GetPerfilPosto"];//RAST.BO.Rastreabilidade.Amarracao.GetPerfilPosto(passagem.posto.CodLinha, passagem.posto.NumPosto, Modelo, Fabrica, NumAP);
			
			int pageIndex = Convert.ToInt32(page) - 1;
			int pageSize = rows;
			int totalRecords = context.Count();
			int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

			var authors = context.OrderBy(a => sord).Skip(pageIndex * pageSize).Take(pageSize).ToList();
			
			var jsonData = new
			{
				total = totalPages,
				page,
				records = totalRecords,
				rows = (from n in authors
						select new
						{
							i = n.DesTipoAmarra.Replace("\"", ""),
							cell = new string[] { "" + n.DesTipoAmarra.Replace("\"", ""), "" + n.CodItem, n.DesItem.Trim(), "" }
						}).ToArray()
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Método responsável por exibir o que foi lido no posto de amarração
		/// </summary>
		/// <param name="sidx"></param>
		/// <param name="sord"></param>
		/// <param name="page"></param>
		/// <param name="rows"></param>
		/// <param name="Fabrica"></param>
		/// <param name="NumAP"></param>
		/// <param name="Modelo"></param>
		/// <returns></returns>
		public ActionResult HistoricoAmarracaoSimples(string sidx, string sord, int page, int rows, string Fabrica, string NumAP, string Modelo)
		{
			Passagem passagem = new Passagem((DadosPosto)Session["DadosPosto"]);

			var colunas = (List<viw_CBPerfilAmarraDTO>)Session["GetPerfilPosto"];//RAST.BO.Rastreabilidade.Amarracao.GetPerfilPosto(passagem.posto.CodLinha, passagem.posto.NumPosto, Modelo.ToString(), Fabrica, NumAP);
			var context = RAST.BO.Rastreabilidade.Amarracao.HistoricoAmarracaoPostoSimples(passagem.posto.CodLinha, passagem.posto.NumPosto);

			var numSeries = context.GroupBy(g => g.NumSerie).Select(s => new { NumSerie = s.Key }).ToList();
			colunas = colunas.Where(a => a.DesTipoAmarra != "EAN13").ToList();

			var list = new List<object>();
			var valores = new List<HistoricoPostoAmarracao>();

			foreach (var s in numSeries)
			{
				valores = (from t in colunas
						   join u in context on t.CodTipoAmarra equals u.FlgTipo into ut
						   from sub in ut.DefaultIfEmpty()
						   where sub != null ? sub.NumSerie == s.NumSerie : true
						   select new HistoricoPostoAmarracao
						   {
							   Descricao = t.DesTipoAmarra,
							   Tipo = t.CodTipoAmarra == 6 ? 0 : t.CodTipoAmarra,
							   Valor = t.CodTipoAmarra == 6 ? s.NumSerie : sub == null ? "" : sub.NumECB

						   }).OrderBy(o => o.Tipo).ToList();

				if (valores.Count < colunas.Count)
				{
					for (int i = 0; i < colunas.Count; i++)
					{
						if (!valores.Any(a => a.Descricao == colunas[i].DesTipoAmarra))
						{
							valores.Add(new HistoricoPostoAmarracao
							{
								Descricao = colunas[i].DesTipoAmarra,
								Tipo = colunas[i].CodTipoAmarra,
								Valor = ""
							});
						}
					}
				}

				valores.Add(new HistoricoPostoAmarracao
				{
					Descricao = "Data Leitura",
					Tipo = 50,
					Valor = context.FirstOrDefault(a => a.NumSerie == s.NumSerie).DatAmarra.ToString()
				});

				list.Add(new
				{
					i = s.NumSerie,
					cell = valores.OrderBy(o => o.Tipo).Select(d => d.Valor).ToArray()
				});
			}


			int pageIndex = Convert.ToInt32(page) - 1;
			int pageSize = rows;
			int totalRecords = context.Count();
			int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

			var authors = context.OrderBy(a => sord).Skip(pageIndex * pageSize).Take(pageSize).ToList();

			var jsonData = new
			{
				total = totalPages,
				page,
				records = totalRecords,
				rows = list.ToArray()
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		///  Método responsável por exibir o que foi lido na amarração de lote
		/// </summary>
		/// <param name="sidx"></param>
		/// <param name="sord"></param>
		/// <param name="page"></param>
		/// <param name="rows"></param>
		/// <param name="Fabrica"></param>
		/// <param name="NumAP"></param>
		/// <param name="Modelo"></param>
		/// <returns></returns>
		//public ActionResult HistoricoAmarracaoLote(string sidx, string sord, int page, int rows, string Fabrica, string NumAP, string Modelo)
		public ActionResult HistoricoAmarracaoLote (string sidx, string sord, int page, int rows, string Fabrica)
		{
			Passagem passagem = new Passagem((DadosPosto)Session["DadosPosto"]);

			//var colunas = ((List<viw_CBPerfilAmarraDTO>)Session["GetPerfilPosto"]);
			var aux = (List<viw_CBPerfilAmarraDTO>)Session["GetPerfilPosto"];
			

			var colunas = aux.Where( a => a.numAP == a.numAP ).Select(x => new viw_CBPerfilAmarraDTO
																	{   CodModelo = x.CodModelo,
																		DesTipoAmarra = x.DesTipoAmarra,
																		numAP = x.numAP,
																		Mascara = x.Mascara,
																		NumPosto = x.NumPosto,
																		CodItem = x.CodItem,
																		CodLinha = x.CodLinha,
																		CodTipoAmarra = x.CodTipoAmarra,
																		DesItem = x.DesItem,
																		DesModelo = x.DesModelo,
																		DipositivoID = x.DipositivoID,
																		IsDispositivoIntegrado = x.IsDispositivoIntegrado }).ToList();

			var qtde = colunas.Count(x => x.DesTipoAmarra == "Produto");
			if (qtde > 1)
			{
				var idx = colunas.FindIndex(x => x.DesTipoAmarra == "Produto");
				var obj = colunas[idx];

				colunas.RemoveAll(x => x.DesTipoAmarra == "Produto" && x != obj);
			}

			var context = RAST.BO.Rastreabilidade.Amarracao.HistoricoAmarracaoPostoLote(passagem.posto.CodLinha, passagem.posto.NumPosto);

			var numSeries = context.GroupBy(g => g.NumSerie).Select(s => new { NumSerie = s.Key }).ToList();
			colunas = colunas.Where(a => a.DesTipoAmarra != "EAN13").ToList();

			var list = new List<object>();
			var valores = new List<HistoricoPostoAmarracao>();

			foreach (var s in numSeries)
			{
				valores = (from t in colunas
							join u in context on t.CodTipoAmarra equals u.FlgTipo into ut
							from sub in ut.DefaultIfEmpty()
							where sub != null ? sub.NumSerie == s.NumSerie : true
							select new HistoricoPostoAmarracao
							{
								Descricao = t.DesTipoAmarra,
								Tipo = t.CodTipoAmarra == 6 ? 0 : t.CodTipoAmarra,
								Valor = t.CodTipoAmarra == 6 ? s.NumSerie : sub == null ? "" : sub.NumECB,
								sequenciaOrdenacao = Geral.GetSequenciaPorTipoAmarracao(t.CodTipoAmarra)								   
							}).OrderBy(o => o.sequenciaOrdenacao).ToList();

				if (valores.Count < colunas.Count)
				{
					for (int i = 0; i < colunas.Count; i++)
					{
						if (!valores.Any(a => a.Descricao == colunas[i].DesTipoAmarra))
						{
							valores.Add(new HistoricoPostoAmarracao
							{
								Descricao = colunas[i].DesTipoAmarra,
								Tipo = colunas[i].CodTipoAmarra,
								Valor = "",
								sequenciaOrdenacao = Geral.GetSequenciaPorTipoAmarracao(colunas[i].CodTipoAmarra)
							});
						}
					}
				}

				valores.Add(new HistoricoPostoAmarracao
				{
					Descricao = "Data Leitura",
					Tipo = 100,
					Valor = context.FirstOrDefault(a => a.NumSerie == s.NumSerie).DatAmarra.ToString(),
					sequenciaOrdenacao = 100
				});

				var numCCTemp = context.FirstOrDefault(a => a.NumSerie == s.NumSerie).numCC;
				valores.Add(new HistoricoPostoAmarracao
				{
					Descricao = "Caixa Coletiva",
					Tipo = 100,
					Valor = numCCTemp == null ? "" : numCCTemp.ToString(),
					sequenciaOrdenacao = 100
				});

				var numLoteTemp = context.FirstOrDefault(a => a.NumSerie == s.NumSerie).numLote;
				valores.Add(new HistoricoPostoAmarracao
				{
					Descricao = "Lote",
					Tipo = 100,
					Valor = numLoteTemp == null ? "" : numLoteTemp.ToString(),
					sequenciaOrdenacao = 100
				});

				list.Add(new
				{
					i = s.NumSerie,
					cell = valores.OrderBy(o => o.sequenciaOrdenacao).Select(d => d.Valor).ToArray()
				});
			}

			int pageIndex = Convert.ToInt32(page) - 1;
			int pageSize = rows;
			int totalRecords = context.Count();
			int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

			var jsonData = new
			{
				total = totalPages,
				page,
				records = totalRecords,
				rows = list.ToArray()
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Carrega os dados para o grid com as leituras do lote atualmente aberto 
		/// </summary>
		/// <param name="sidx"></param>
		/// <param name="sord"></param>
		/// <param name="page"></param>
		/// <param name="rows"></param>
		/// <param name="Fabrica"></param>
		/// <returns></returns>
		public ActionResult HistoricoAmarracaoLoteAtual(string sidx, string sord, int page, int rows, string Fabrica, string numAP)
		{
			Passagem passagem = new Passagem((DadosPosto)Session["DadosPosto"]);

			var aux = (List<viw_CBPerfilAmarraDTO>)Session["GetPerfilPosto"];

			var colunas = new List<viw_CBPerfilAmarraDTO>();
			colunas.Add(new viw_CBPerfilAmarraDTO(){DesTipoAmarra = "Produto", CodTipoAmarra = 6});

			if (numAP.Length > 6)
			{
				numAP = numAP.Substring(0, 6);
			}

			var ap = Geral.ObterAP(Fabrica, numAP) ?? new LSAAPDTO(){CodModelo = ""};

			var context = RAST.BO.Rastreabilidade.Amarracao.HistoricoAmarracaoPostoLoteAtual(passagem.posto.CodLinha, passagem.posto.NumPosto, ap.CodModelo);

			var numSeries = context.GroupBy(g => g.NumSerie).Select(s => new { NumSerie = s.Key }).ToList();

			var list = new List<object>();

			foreach (var s in numSeries)
			{
				var valores = new List<HistoricoPostoAmarracao>();

				valores.Add(new HistoricoPostoAmarracao()
				{
					Descricao = "Produto",
					Tipo = 6,
					Valor = s.NumSerie,
					sequenciaOrdenacao = 1
				});

				valores.Add(new HistoricoPostoAmarracao
				{
					Descricao = "Data Leitura",
					Tipo = 100,
					Valor = context.FirstOrDefault(a => a.NumSerie == s.NumSerie).DatAmarra.ToString(),
					sequenciaOrdenacao = 100
				});

				var numCCTemp = context.FirstOrDefault(a => a.NumSerie == s.NumSerie).numCC;
				valores.Add(new HistoricoPostoAmarracao
				{
					Descricao = "Caixa Coletiva",
					Tipo = 100,
					Valor = numCCTemp == null ? "" : numCCTemp.ToString(),
					sequenciaOrdenacao = 100
				});

				var numLoteTemp = context.FirstOrDefault(a => a.NumSerie == s.NumSerie).numLote;
				valores.Add(new HistoricoPostoAmarracao
				{
					Descricao = "Lote",
					Tipo = 100,
					Valor = numLoteTemp == null ? "" : numLoteTemp.ToString(),
					sequenciaOrdenacao = 100
				});

				list.Add(new
				{
					i = s.NumSerie,
					cell = valores.OrderBy(o => o.sequenciaOrdenacao).Select(d => d.Valor).ToArray()
				});

			}

			int pageIndex = Convert.ToInt32(page) - 1;
			int pageSize = rows;
			int totalRecords = context.Count();
			int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

			var jsonData = new
			{
				total = totalPages,
				page,
				records = totalRecords,
				rows = list.ToArray()
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Este método é responsável por retornar os dados lidos no posto de passagem.
		/// </summary>
		/// <param name="sidx"></param>
		/// <param name="sord"></param>
		/// <param name="page"></param>
		/// <param name="rows"></param>
		/// <param name="Fabrica"></param>
		/// <param name="NumAP"></param>
		/// <param name="Modelo"></param>
		/// <returns></returns>
		public ActionResult HistoricoPostoPassagem(string sidx, string sord, int page, int rows, string Fabrica, string NumAP, string Modelo)
		{
			Passagem passagem = new Passagem((DadosPosto)Session["DadosPosto"]);

			var context = RAST.BO.Rastreabilidade.Amarracao.HistoricoPostoPassagem(passagem.posto.CodLinha, passagem.posto.NumPosto);

			int pageIndex = Convert.ToInt32(page) - 1;
			int pageSize = rows;
			int totalRecords = context.Count();
			int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

			var authors = context.OrderBy(a => sord).Skip(pageIndex * pageSize).Take(pageSize).ToList();

			var jsonData = new
			{
				total = totalPages,
				page,
				records = totalRecords,
				rows = (from n in authors
						select new
						{
							i = n.NumSerie,
							cell = new string[] { "" + n.NumSerie, n.DatAmarra.ToString(), "" }
						}).ToArray()
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Método para exibir o histórico das últimas leituras realizadas
		/// </summary>
		/// <param name="sidx"></param>
		/// <param name="sord"></param>
		/// <param name="page"></param>
		/// <param name="rows"></param>
		/// <returns></returns>
		public ActionResult HistoricoPassagem(string sidx, string sord, int page, int rows)
		{
			Passagem passagem = new Passagem((DadosPosto)Session["DadosPosto"]);
			var context = passagem.ObterPassagemPosto();
			

			int pageIndex = Convert.ToInt32(page) - 1;
			int pageSize = rows;
			int totalRecords = context.Count();
			int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

			var authors = context.OrderBy(a => sord).Skip(pageIndex * pageSize).Take(pageSize).ToList();

			var jsonData = new
			{
				total = totalPages,
				page,
				records = totalRecords,
				rows = (from n in authors
						select new
						{
							i = n.NumECB,
							cell = new string[] { "" + n.NumECB, n.DatEvento.ToString("HH:mm:ss"), Geral.ObterDescricaoItem(n.NumECB.Substring(0, 6).ToString()) }
						}).ToArray()
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Método para exibir o contador de leituras aprovadas, reprovadas e total
		/// </summary>
		/// <param name="sidx"></param>
		/// <param name="sord"></param>
		/// <param name="page"></param>
		/// <param name="rows"></param>
		/// <returns></returns>
		public ActionResult Contador(string sidx, string sord, int page, int rows)
		{
			var postoCookie = Request.Cookies["Posto"];
			var aprovados = Geral.ObterTotalAprovadas(int.Parse(postoCookie.Values["codLinha"]), int.Parse(postoCookie.Values["numPosto"]));
			int reprovados = Geral.ObterTotalReprovadas(int.Parse(postoCookie.Values["codLinha"]), int.Parse(postoCookie.Values["numPosto"]));
			int total = Geral.ObterTotal(int.Parse(postoCookie.Values["codLinha"]), int.Parse(postoCookie.Values["numPosto"]));

			int pageIndex = Convert.ToInt32(page) - 1;
			int pageSize = rows;
			int totalRecords = 1;
			int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

			var lista = new List<object>();
			lista.Add(new 
						{
							i = aprovados.ToString(),
							cell = new string[] { aprovados.ToString(), reprovados.ToString(), total.ToString() }
						});
			
			var jsonData = new
			{
				total = totalPages,
				page,
				records = totalRecords,
				rows = lista.ToArray()
						
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		public ActionResult Tecnico()
		{
			var postoCookie = Request.Cookies["Posto"];

			var posto = Geral.ObterPosto(int.Parse(postoCookie.Values["codLinha"]), int.Parse(postoCookie.Values["numPosto"]));
			var linha = Geral.ObterLinha(int.Parse(postoCookie.Values["codLinha"]));

			ViewBag.Fabrica = postoCookie.Values["Fabrica"];
			ViewBag.Posto = posto.DesPosto;
			ViewBag.Linha = linha.DesLinha;
			ViewBag.ListaAcao = Acao.Listar();//SEMP.Model.Constantes.ListaAcao.OrderBy(a=>a).ToList();
			ViewBag.ListaCausa = RAST.BO.Rastreabilidade.Tecnico.ListaCausa();
			ViewBag.ListaOrigem = RAST.BO.Rastreabilidade.Tecnico.ListaOrigem();

			return View();
		}

		public ActionResult ConsultarCodDefeito()
		{
			var Lista = RAST.BO.Rastreabilidade.Tecnico.CodListDefeitos();
			return View(Lista);
		}

		public ActionResult ConsultarItem()
		{
			return View();
		}

		public ActionResult Transferencia()
		{
			var postoCookie = Request.Cookies["Posto"];

			var posto = Geral.ObterPosto(int.Parse(postoCookie.Values["codLinha"]), int.Parse(postoCookie.Values["numPosto"]));
			var linha = Geral.ObterLinha(int.Parse(postoCookie.Values["codLinha"]));

			ViewBag.Fabrica = postoCookie.Values["Fabrica"];
			ViewBag.Posto = posto.DesPosto;
			ViewBag.Linha = linha.DesLinha;

			return View();
	}

		public ActionResult TransferenciaEmAberto(string sidx, string sord, int page, int rows)
		{			
			var postoCookie = Request.Cookies["Posto"];
			
			var context = TransferenciaLote.ObterTransferenciaAberta(int.Parse(postoCookie.Values["codLinha"]), int.Parse(postoCookie.Values["numPosto"]));

			int pageIndex = Convert.ToInt32(page) - 1;
			int pageSize = rows;
			int totalRecords = context.Count();
			int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

			var authors = context.OrderBy(a => sord).Skip(pageIndex * pageSize).Take(pageSize).ToList();

			var jsonData = new
			{
				total = totalPages,
				page,
				records = totalRecords,
				rows = (from n in authors
						select new
						{
							i = n.NumLote,
							cell = new string[] { "" + n.NumLote }
						}).ToArray()
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		public ActionResult TransferenciaDisponivel(string sidx, string sord, int page, int rows)
		{
			var postoCookie = Request.Cookies["Posto"];

			string codModelo = TransferenciaLote.ObterModeloTransferencia(int.Parse(postoCookie.Values["codLinha"]), int.Parse(postoCookie.Values["numPosto"]));

			var context = TransferenciaLote.ObterLoteDisponivel(codModelo);			

			int pageIndex = Convert.ToInt32(page) - 1;
			int pageSize = rows;
			int totalRecords = context.Count();
			int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

			var authors = context.OrderBy(a => sord).Skip(pageIndex * pageSize).Take(pageSize).ToList();

			var jsonData = new
			{
				total = totalPages,
				page,
				records = totalRecords,
				rows = (from n in authors
						select new
						{
							i = n.NumLote,
							cell = new string[] { "" + n.NumLote }
						}).ToArray()
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		public ActionResult Recebimento()
		{
			var postoCookie = Request.Cookies["Posto"];

			var posto = Geral.ObterPosto(int.Parse(postoCookie.Values["codLinha"]), int.Parse(postoCookie.Values["numPosto"]));
			var linha = Geral.ObterLinha(int.Parse(postoCookie.Values["codLinha"]));

			ViewBag.Fabrica = postoCookie.Values["Fabrica"];
			ViewBag.Posto = posto.DesPosto;
			ViewBag.Linha = linha.DesLinha;
			ViewBag.CodLinha = linha.CodLinha;

			return View();
		}

		public JsonResult GetApsModelo(string codProcesso ,string codModelo)
		{
			var listaAp = Geral.GetApsModelo(codProcesso,codModelo);
			
			return Json(listaAp, JsonRequestBehavior.AllowGet);
		}

		public ActionResult ObterItensRastreaveisAP(string codFab, string numAP)
		{
			var dadosAP = Geral.ObterItensRastreaveis(codFab, numAP);

			var postoCookie = Request.Cookies["Posto"];

			var familias = RAST.BO.Rastreabilidade.Amarracao.GetFamiliaLeitura(int.Parse(postoCookie.Values["codLinha"]), int.Parse(postoCookie.Values["numPosto"]));

			ViewBag.familias = familias;
			return View(dadosAP);

		}

		public ActionResult LeituraLote()
		{
			HttpCookie cookie = Request.Cookies["Posto"];

			if (cookie == null || Session["DadosPosto"] == null)
			{
				return RedirectToAction("Index", "Setup");
			}

			DateTime dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			DateTime dFim = dIni.AddDays(1).AddMinutes(-1);

			var postoCookie = Request.Cookies["Posto"];

			Passagem passagem = new Passagem((DadosPosto)Session["DadosPosto"]);

			var posto = Geral.ObterPosto(int.Parse(postoCookie.Values["codLinha"]), int.Parse(postoCookie.Values["numPosto"]));
			var linha = Geral.ObterLinha(int.Parse(postoCookie.Values["codLinha"]));

			ViewBag.Fabrica = postoCookie.Values["Fabrica"];
			ViewBag.Posto = posto.DesPosto;
			ViewBag.Linha = linha.DesLinha;
			ViewBag.CodTipoPosto = posto.CodTipoPosto;

			return View(Session["DadosPosto"]);
		}

		#region Nova tela de lote

		/// <summary>
		///  View para a tela de Teste com Lote.
		/// </summary>
		/// <returns>Retorna a View TesteLote.cshtml</returns>
		public ActionResult TesteLote()
		{
			HttpCookie cookie = Request.Cookies["Posto"];

			if (cookie == null || Session["DadosPosto"] == null)
			{
				return RedirectToAction("Index", "Setup");
			}

			var postoCookie = Request.Cookies["Posto"];

			Passagem passagem = new Passagem((DadosPosto)Session["DadosPosto"]);
			
			var linha = Geral.ObterLinha(int.Parse(postoCookie.Values["codLinha"]));

			ViewBag.Fabrica = postoCookie.Values["Fabrica"];
			ViewBag.Posto = passagem.posto.DesPosto;
			ViewBag.Linha = linha.DesLinha;
			ViewBag.CodTipoPosto = passagem.posto.CodTipoPosto;

			return View(Session["DadosPosto"]);
		}

		public ActionResult Oba()
		{
			HttpCookie cookie = Request.Cookies["Posto"];

			if (cookie == null || Session["DadosPosto"] == null)
			{
				return RedirectToAction("Index", "Setup");
			}

			var postoCookie = Request.Cookies["Posto"];

			Passagem passagem = new Passagem((DadosPosto)Session["DadosPosto"]);

			var linha = Geral.ObterLinha(int.Parse(postoCookie.Values["codLinha"]));
			var posto = Geral.ObterPosto(int.Parse(postoCookie.Values["codLinha"]), int.Parse(postoCookie.Values["numPosto"]));

			Session["Posto"] = posto;
			Session["Linha"] = linha;

			ViewBag.Fabrica = postoCookie.Values["Fabrica"];
			ViewBag.Posto = posto.DesPosto;
			ViewBag.Linha = linha.DesLinha;
			ViewBag.CodTipoPosto = passagem.posto.CodTipoPosto;

			return View(Session["DadosPosto"]);
		}

		#endregion

		public ActionResult ReimpressaoSerial() {
			return View();
		}
	}
}
