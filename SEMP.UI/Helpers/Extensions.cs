﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using SEMP.Models;
using SEMP.BO;
using SEMP.Model.VO;
using System.Web;
using ZXing;
using ZXing.Common;
using System.IO;
using System.Drawing.Imaging;

namespace SEMP.Helpers
{
	public static class Extensions
	{
		public static MvcHtmlString LinkVoltar(this HtmlHelper html, string idLink, string textoLink = "Voltar")
		{
			string strLink = String.Format("<a id=\"{0}\" href=\"javascript:history.go(-1);\">{1}</a>", idLink, textoLink); 
			return new MvcHtmlString(strLink);
		}

		public static MvcHtmlString MontarMenu(this HtmlHelper helper, List<Menu> menus, string classSubMenu = "", string classLi = "", string classA = "")
		{
			string resultado = "";

			foreach (Menu item in menus)
			{
				resultado += String.Format("<li class='{0}'><a href='{1}' alt='{2}' class='{3}'>{4}</a>\n", classLi, item.Endereco, item.Legenda, classA, item.Titulo);
 
					if (item.Items != null && item.Items.Count > 0)
					{
						resultado += String.Format("<ul class='{0}'>{1}</ul>", MontarMenu(helper, item.Items), classSubMenu);
					}
				resultado += "</li>";
			}
			return new MvcHtmlString(resultado);
		}

		/// <summary>
		/// Obter dados a partir de uma URL via AJAX e colocar o resultado dentro de uma tag do corpo do documento
		/// </summary>
		/// <param name="html"></param>
		/// <param name="endereco">URL destino da requisição</param>
		/// <param name="parametros">Os parâmetros que serão enviados para a requisição</param>
		/// <param name="idDestino">ID do objeto no corpo do html que receberá o resultado da requisição</param>
		/// <returns></returns>
		public static MvcHtmlString ObterDadoAjaxHtml(this HtmlHelper html, string endereco, string parametros, string idDestino)
		{
			var resultado = String.Format(
											"	$.ajax({{" +
											"		type: 'GET'," +
											"		url: '{0}'," +
											"		data: {{ {1} }}," +
											"		cache: false," +
											"		success: function (data) {{" +
											"			$('#{2}').html(data);" +
											"		}}" +
											"	}});"
										  , endereco, parametros, idDestino);

			
			return new MvcHtmlString(resultado);
		}

		/// <summary>
		/// Obter dados a partir de uma URL via AJAX e colocar o resultado dentro de um objeto de formulário
		/// </summary>
		/// <param name="html"></param>
		/// <param name="endereco">URL destino da requisição</param>
		/// <param name="parametros">Os parâmetros que serão enviados para a requisição</param>
		/// <param name="idDestino">ID do objeto do formulário que receberá o resultado da requisição</param>
		/// <returns></returns>
		public static MvcHtmlString ObterDadoAjaxVal(this HtmlHelper html, string endereco, string parametros, string idDestino)
		{
			var resultado = String.Format(
											"	$.ajax({{\n" +
											"		type: 'GET',\n" +
											"		url: '{0}',\n" +
											"		data: {{ {1} }},\n" +
											"		cache: false,\n" +
											"		success: function (data) {{\n" +
											"			$('#{2}').val(data);\n" +
											"		}}\n" +
											"	}})\n;"
										  , endereco, parametros, idDestino);


			return new MvcHtmlString(resultado);
		}

		public static int NumberOfWeeks(DateTime dateFrom, DateTime dateTo)
		{
			TimeSpan Span = dateTo.Subtract(dateFrom);

			if (Span.Days <= 7)
			{
				if (dateFrom.DayOfWeek > dateTo.DayOfWeek)
				{
					return 2;
				}

				return 1;
			}

			int Days = Span.Days - 7 + (int)dateFrom.DayOfWeek;
			int WeekCount = 1;
			int DayCount = 0;

			for (WeekCount = 1; DayCount < Days; WeekCount++)
			{
				DayCount += 7;
			}

			return WeekCount;
		}

		public static bool IsReleaseBuild()
		{
		#if DEBUG
			return false;
		#else
			return true;
		#endif
		}

		public static string FormatarTempo(double tempo) {

			if (tempo == 0) {
				return "0m";

			}

			var tAtravessa = "";

			var difTempo = TimeSpan.FromMinutes(tempo);


			if (difTempo.Days > 0)
			{
                var horas = 0;
                horas = (difTempo.Days * 24) + difTempo.Hours;

				tAtravessa = String.Format("{0:00}h{1:00}m", horas, difTempo.Minutes);
			}
			else if (difTempo.Hours > 0)
			{
				tAtravessa = String.Format("{0:00}h{1:00}m", difTempo.Hours, difTempo.Minutes);
			}
			else
			{
				tAtravessa = String.Format("{0:00}m", difTempo.Minutes);
			}

			return tAtravessa;

		}

		public static string FormatarTempo(decimal tempo)
		{

			return FormatarTempo(Convert.ToDouble(tempo));

		}

		public static string FormatarTempo(int tempo)
		{

			return FormatarTempo(Convert.ToDouble(tempo));

		}


        public static string FormatarHora(double tempo)
        {

            if (tempo == 0)
            {
                return "0m";

            }

            var tAtravessa = "";

            var difTempo = TimeSpan.FromMinutes(tempo);


            if (difTempo.Days > 0)
            {
                var horas = 0;
                horas = (difTempo.Days * 24) + difTempo.Hours;

                tAtravessa = String.Format("{0:00}:{1:00}", horas, difTempo.Minutes);
            }
            else if (difTempo.Hours > 0)
            {
                tAtravessa = String.Format("{0:00}:{1:00}", difTempo.Hours, difTempo.Minutes);
            }
            else
            {
                tAtravessa = String.Format("00:{0:00}", difTempo.Minutes);
            }

            return tAtravessa;

        }

        public static string FormatarHora(decimal tempo)
        {

            return FormatarHora(Convert.ToDouble(tempo));

        }

        public static string FormatarHora(int tempo)
        {

            return FormatarHora(Convert.ToDouble(tempo));

        }

        public static bool Permissao(string Permissao, RetornoLogin login) {

			var depto = login.Depto ?? "";
			var nomusuario = login.NomUsuario ?? "";

			return Util.VerPermissaoAcesso(Permissao, depto.Trim().ToUpper(), nomusuario.Trim().ToUpper());

		}

		public static IHtmlString GenerateQrCode(this HtmlHelper html, string url, string alt = "QR code", int height = 500, int width = 500, int margin = 0)
		{
			var qrWriter = new BarcodeWriter();
			qrWriter.Format = BarcodeFormat.QR_CODE;
			qrWriter.Options = new EncodingOptions() { Height = height, Width = width, Margin = margin };

			using (var q = qrWriter.Write(url))
			{
				using (var ms = new MemoryStream())
				{
					q.Save(ms, ImageFormat.Png);
					var img = new TagBuilder("img");
					img.Attributes.Add("src", String.Format("data:image/png;base64,{0}", Convert.ToBase64String(ms.ToArray())));
					img.Attributes.Add("alt", alt);
					return MvcHtmlString.Create(img.ToString(TagRenderMode.SelfClosing));
				}
			}
		}

	}

}
