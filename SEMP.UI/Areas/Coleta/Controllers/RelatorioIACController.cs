﻿using RAST.BO.CNQ;
using RAST.BO.Relatorios;
using SEMP.Model.DTO;
using SEMP.Model.VO.CNQ;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEMP.Areas.Coleta.Controllers
{
    public class RelatorioIACController : Controller
    {
        //
        // GET: /Coleta/RelatorioIAC/
        public ActionResult Index(string Mensagem)
        {
            ViewBag.mensagem = Mensagem;
            return View();
        }

        /// <summary>
        /// Método GetModelos retorna as Modelos da IAC de acordo com a Linha informada.
        /// </summary>
        /// <param name="datInicio"></param>
        /// <param name="datFim"></param>
        /// <param name="codlinha"></param>
        /// <returns></returns>
        public JsonResult GetModelos(string datInicio, string datFim, int codlinha, int? turno)
        {
            DateTime dIni = Convert.ToDateTime(datInicio);
            DateTime dFim = Convert.ToDateTime(datFim).AddDays(1);

            int t = 1;
            if (turno != null && turno != 0)
            {
                t = (int)turno;
            }

            var linha = PainelProducao.ObterCodLinha(codlinha);

            var Modelos = RelatorioProducaoIAC.ObterResumoIAC(dIni, dFim, linha.CodLinhaT3, t);

            return Json(Modelos, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Relatorio()
        {
            return RedirectToAction("Index", "RelatorioIAC");
        }

        [HttpPost]
        public ActionResult Relatorio(string datInicio, string datFim, int? codLinha, int? turno, string codDRT, string codModelo, string fase)
        {

            DateTime dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
            DateTime dFim = dIni.AddDays(1).AddMinutes(-1);

            if (!String.IsNullOrEmpty(datInicio) && !String.IsNullOrEmpty(datFim))
            {
                dIni = DateTime.Parse(datInicio);
                dFim = DateTime.Parse(datFim);
            }
                      
            return View();
        }

        public ActionResult Postos(string DatInicio, string DatFim, int CodLinha, string CodModelo, int? Turno)
        {

            try
            {
                var CodModeloFinal = RelatorioProducaoIAC.ObterModeloFase(CodModelo, "02");

                var Fabrica = "2";

                DateTime dIni = DateTime.Now;
                DateTime dFim = dIni.AddDays(1).AddMinutes(-1);

                if (DatInicio != null && DatFim != null)
                {
                    dIni = Convert.ToDateTime(DatInicio);
                    dFim = Convert.ToDateTime(DatFim).AddDays(1).AddMinutes(-1);
                }

                int t = 1;
                if (Turno != null && Turno != 0)
                {
                    t = (int)Turno;
                }

                List<IndicadorPostos> listaIndicadores = new List<IndicadorPostos>();
                List<IndicadorMF> resumoPosto = new List<IndicadorMF>();
                List<IndicadorMF> postoTecnico = new List<IndicadorMF>();

                List<string> TitulosResumoPosto = new List<string> { "Qtd Produzida", "Qtd Defeitos", "PPM Acumulado" };

                int QuantidadePontosModelo = RelatorioProducaoIAC.ObterQuantidadePontosPorModelo(CodModelo);

                List<CBPostoDTO> postos = Indicadores.GetPostos(CodLinha);
                List<IndicadorPostos> listaIndicadoresQuantidadeProduzidaConsulta = RelatorioProducaoIAC.ObterIndicadoresQuantidadeProduzidaPostosModelo(dIni, dFim, CodLinha, t, CodModeloFinal);
                List<IndicadorPostos> listaIndicadoresQuantidadeDefeitoConsulta = RelatorioProducaoIAC.ObterIndicadoresQuantidadeDefeitosModelo(dIni, dFim, CodLinha, t, CodModeloFinal);

                CBLinhaDTO Linha = Indicadores.GetLinhaByCodigo(CodLinha);

                IndicadorPostos qtdProduzida = null;
                IndicadorPostos qtdDefeitos = null;
                IndicadorPostos indiceDefeito = null;

                foreach (CBPostoDTO p in postos)
                {
                    qtdProduzida = new IndicadorPostos
                    {
                        Titulo = "Qtd. Produzida",
                        NumPosto = p.NumPosto,
                        Quantidade = listaIndicadoresQuantidadeProduzidaConsulta.Any(a => a.NumPosto.Equals(p.NumPosto)) == true ?
                        listaIndicadoresQuantidadeProduzidaConsulta.FirstOrDefault(a => a.NumPosto.Equals(p.NumPosto)).Quantidade
                        :
                        0
                    };

                    listaIndicadores.Add(qtdProduzida);

                    qtdDefeitos = new IndicadorPostos
                    {
                        Titulo = "Qtd. Defeitos",
                        NumPosto = p.NumPosto,
                        Quantidade = listaIndicadoresQuantidadeDefeitoConsulta.Any(a => a.NumPosto.Equals(p.NumPosto)) == true ?
                        listaIndicadoresQuantidadeDefeitoConsulta.Where(a => a.NumPosto.Equals(p.NumPosto)).FirstOrDefault().Quantidade
                        :
                        0
                    };

                    listaIndicadores.Add(qtdDefeitos);

                    indiceDefeito = new IndicadorPostos
                    {
                        Titulo = "PPM Acumulado",
                        NumPosto = p.NumPosto,
                        Quantidade = Math.Round((qtdProduzida.Quantidade > 0 ? qtdDefeitos.Quantidade / (qtdProduzida.Quantidade * QuantidadePontosModelo) : 0) * 1000000, 2)
                    };

                    listaIndicadores.Add(indiceDefeito);
                }

                ViewBag.Fabrica = Fabrica;
                ViewBag.Modelo = CodModelo;
                ViewBag.CodLinha = CodLinha;
                ViewBag.DatIni = DatInicio;
                ViewBag.DatFim = DatFim;
                ViewBag.Postos = postos;
                ViewBag.CountPostos = postos.Count + 1;
                ViewBag.Turno = t;
                ViewBag.IndicadorPosto = listaIndicadores;
                ViewBag.Linha = Linha.DesLinha.ToUpper();

                return View();
            }
            catch (Exception ex)
            {
                ViewBag.Erro = "Erro: " + ex.Message;
            }
            return View();
        }

        public ActionResult PostoDetalhe(string DatIni, string DatFim, string Fabrica, string Familia, int CodLinha, int Posto, string CodModelo, int? Turno)
        {

            try
            {

                DateTime dIni = DateTime.Now;
                DateTime dFim = dIni.AddDays(1).AddMinutes(-1);

                if (DatIni != null && DatFim != null)
                {
                    dIni = Convert.ToDateTime(DatIni);
                    dFim = Convert.ToDateTime(DatFim).AddDays(1).AddMinutes(-1);
                }

                int t = 1;
                if (Turno != null)
                {
                    t = (int)Turno;
                }

                var CodModeloFinal = RelatorioProducaoIAC.ObterModeloFase(CodModelo, "02");

                int QuantidadePontosModelo = RelatorioProducaoIAC.ObterQuantidadePontosPorModelo(CodModelo);

                var horarios = Indicadores.GetHorarios(t);

                List<IndicadorPostoHoraHora> listaIndicadoresPostoHora = new List<IndicadorPostoHoraHora>();
                List<string> TitulosResumoPosto = new List<string> { "Qtd Inspecionada", "Qtd Inspecionada Acumulada", "Qtd Defeitos", "Qtd Defeitos Acumulado", "PPM Acumulado" };
                List<string> TitulosPostoTecnico = new List<string> { "Qtd Defeitos Apontados", "Qtd Defeitos Reparados", "Qtd Pendentes Reparo" };
                List<IndicadorMF> resumoPosto = new List<IndicadorMF>();
                List<IndicadorMF> postoTecnico = new List<IndicadorMF>();

                IndicadorPostoHoraHora qtdProduzida = null;
                IndicadorPostoHoraHora qtdDefeitos = null;
                IndicadorPostoHoraHora indiceDefeito = null;

                var listaDefeitosPosicoes = RelatorioProducaoIAC.GetDefeitosLinhaModelo(dIni, dFim, CodLinha, CodModeloFinal);
                List<MaioresDefeitos> listaHoraHoraDefeitoPosicao = new List<MaioresDefeitos>();
                List<IndicadorPostoHoraHora> listaPosiceosIndicadoresHora = new List<IndicadorPostoHoraHora>();
                IndicadorPostoHoraHora indicadorPosicao = null;

                decimal inspecaoAcumulada = 0;
                decimal defeitosAcumulados = 0;

                foreach (var item in horarios)
                {
                    qtdProduzida = new IndicadorPostoHoraHora("Qtd Inspecionada");
                    qtdProduzida.Hora = item.Hora;
                    qtdProduzida.HoraInicio = item.HoraInicio;
                    qtdProduzida.HoraFim = item.HoraFim;
                    qtdProduzida.Quantidade = RelatorioProducaoIAC.GetQuantidadeProduzidaPostoEHoraModelo(dIni, dFim, item.HoraInicio, item.HoraFim, Posto, t, CodLinha, CodModeloFinal);
                    listaIndicadoresPostoHora.Add(qtdProduzida);
                    inspecaoAcumulada += qtdProduzida.Quantidade;

                    qtdDefeitos = new IndicadorPostoHoraHora("Qtd Defeitos");
                    qtdDefeitos.Hora = item.Hora;
                    qtdDefeitos.HoraInicio = item.HoraInicio;
                    qtdDefeitos.HoraFim = item.HoraFim;
                    qtdDefeitos.Quantidade = RelatorioProducaoIAC.GetQuantidadeDefeitosPostoEHoraModelo(dIni, dFim, item.HoraInicio, item.HoraFim, Posto, t, CodLinha, CodModeloFinal);
                    listaIndicadoresPostoHora.Add(qtdDefeitos);
                    defeitosAcumulados += qtdDefeitos.Quantidade;

                    qtdDefeitos = new IndicadorPostoHoraHora("Qtd Defeitos Acumulado");
                    qtdDefeitos.Hora = item.Hora;
                    qtdDefeitos.HoraInicio = item.HoraInicio;
                    qtdDefeitos.HoraFim = item.HoraFim;
                    qtdDefeitos.Quantidade = defeitosAcumulados;
                    listaIndicadoresPostoHora.Add(qtdDefeitos);

                    indiceDefeito = new IndicadorPostoHoraHora("PPM Acumulado");
                    indiceDefeito.Hora = item.Hora;
                    indiceDefeito.HoraInicio = item.HoraInicio;
                    indiceDefeito.HoraFim = item.HoraFim;
                    indiceDefeito.Quantidade = Math.Round((qtdProduzida.Quantidade > 0 ? defeitosAcumulados / (qtdProduzida.Quantidade * QuantidadePontosModelo) : 0) * 1000000, 2);
                    listaIndicadoresPostoHora.Add(indiceDefeito);

                    qtdProduzida = new IndicadorPostoHoraHora("Qtd Inspecionada Acumulada");
                    qtdProduzida.Hora = item.Hora;
                    qtdProduzida.HoraInicio = item.HoraInicio;
                    qtdProduzida.HoraFim = item.HoraFim;
                    qtdProduzida.Quantidade = inspecaoAcumulada;
                    listaIndicadoresPostoHora.Add(qtdProduzida);

                    var listaPosicao = listaDefeitosPosicoes.Where(a => a.DatEvento.TimeOfDay >=  item.HoraInicio && a.DatEvento.TimeOfDay <= item.HoraFim).ToList();

                    foreach (var pos in listaPosicao.GroupBy(g => new { Posicao = g.Posicao, CodDefeito = g.CodDefeito}).Select(s => new { Posicao = s.Key.Posicao, CodDefeito = s.Key.CodDefeito, Quantidade = s.Count() }).ToList())
                    {
                        indicadorPosicao = new IndicadorPostoHoraHora();
                        indicadorPosicao.Hora = item.Hora;
                        indicadorPosicao.HoraInicio = item.HoraInicio;
                        indicadorPosicao.HoraFim = item.HoraFim;
                        indicadorPosicao.Titulo = pos.Posicao;
                        indicadorPosicao.CodDefeito = pos.CodDefeito;
                        indicadorPosicao.Quantidade = pos.Quantidade;
                        listaPosiceosIndicadoresHora.Add(indicadorPosicao);
                    }
                }

                ViewBag.Horarios = horarios;
                ViewBag.CountColunas = horarios.Count + 1;
                ViewBag.Fabrica = Fabrica;
                ViewBag.Familia = string.IsNullOrWhiteSpace(Familia) == true ? "LCD" : Familia; ;
                ViewBag.CodLinha = CodLinha;
                ViewBag.DatIni = DatIni;
                ViewBag.DatFim = DatFim;
                ViewBag.Turno = t;
                ViewBag.Posto = Posto;
                ViewBag.ListaIndicadorPostoHora = listaIndicadoresPostoHora;
                ViewBag.ListaPosiceosIndicadoresHora = listaPosiceosIndicadoresHora;

                return View();
            }
            catch (Exception ex)
            {
                ViewBag.Erro = "Erro: " + ex.Message;
            }
            return View();
        }

    }
}
