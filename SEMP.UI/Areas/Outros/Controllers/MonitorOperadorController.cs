﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.Rastreabilidade;
using RAST.BO.Relatorios;
using SEMP.Model.DTO;

namespace SEMP.Areas.Outros.Controllers
{
    public class MonitorOperadorController : Controller
    {
        //
        // GET: /Outros/MonitorOperador/

        public ActionResult Index()
        {
            var linhas = new Configuracao().ObterLinhas();
            ViewBag.ListaLinhas = linhas;

            var tipoPosto = new Configuracao().ObterTiposPosto();
            ViewBag.TipoPostos = tipoPosto;

            return View();

        }

        public ActionResult Lista(string dataini, string datafim, int CodLinha, int cmbPosto)
        {
            var dtInicio = string.IsNullOrWhiteSpace(dataini) ? DateTime.Now : DateTime.Parse(dataini);
            var dtFim = string.IsNullOrWhiteSpace(datafim) ? DateTime.Now : DateTime.Parse(datafim);
            dtFim = dtFim.AddDays(1).AddSeconds(-1);

           // var postos = new Configuracao().ObterPostos(linha).Where(s => s.CodTipoPosto == TipoPosto).Select(x => x.NumPosto).ToList();

            var lista = RelatorioMonitorOperadorBO.ListaQtdLiberada(dtInicio, dtFim, CodLinha, cmbPosto);


            return View(lista);
        }



        //public JsonResult ListaPostos(int linha)
        //{

        //   var postos = new Configuracao().ObterPostos(linha);

        //    return Json(postos, JsonRequestBehavior.AllowGet);
        //}

    }
}
