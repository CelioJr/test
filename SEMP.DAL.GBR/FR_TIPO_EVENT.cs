namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_TIPO_EVENT
    {
        [Key]
        public int FTE_COD { get; set; }

        [Required]
        [StringLength(20)]
        public string FTE_DESCRICAO { get; set; }

        [Required]
        [StringLength(1)]
        public string FTE_SIGLA { get; set; }
    }
}
