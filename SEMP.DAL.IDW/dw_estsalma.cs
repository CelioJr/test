namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_estsalma
    {
        [Key]
        public long id_estsalma { get; set; }

        public DateTime? dt_referencia { get; set; }

        public decimal? qt_saldofinalmes { get; set; }

        public long id_produto { get; set; }

        public virtual om_produto om_produto { get; set; }

        public virtual om_produto om_produto1 { get; set; }
    }
}
