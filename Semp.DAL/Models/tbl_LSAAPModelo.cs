using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_LSAAPModelo
    {
        public string CodFab { get; set; }
        public string NumAP { get; set; }
        public string CodModelo { get; set; }
        public string CodModeloApont { get; set; }
        public string CodProcesso { get; set; }
        public Nullable<decimal> QtdEmbalado { get; set; }
        public Nullable<decimal> QtdApontado { get; set; }
        public Nullable<decimal> QtdRejeitado { get; set; }
        public Nullable<int> Nivel { get; set; }
        public Nullable<int> Ordem { get; set; }
        public Nullable<int> QtdItens { get; set; }
        public string FlgApontar { get; set; }
        public string CodLocalCRE { get; set; }
        public string CodLocalDEB { get; set; }
        public string CodLinha { get; set; }
        public Nullable<decimal> QtdNecessidade { get; set; }
        public Nullable<decimal> QtdSaldoAnt { get; set; }
    }
}
