﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.Model.DTO;
using SEMP.DAL.DAO.Rastreabilidade;

namespace RAST.BO.Rastreabilidade
{
    public class TempoModelo
    {
        public static List<CBTempoModeloDTO> Lista()
        {
            return daoTempoModelo.ListaTempoModelo();            
        }

        public static CBTempoModeloDTO DetalheItem(int id)
        {
            return daoTempoModelo.DetalheItem(id);            
        }

        public static bool Criar(CBTempoModeloDTO tempoModelo)
        {
            return daoTempoModelo.Criar(tempoModelo);          
        }

        public static bool Editar(CBTempoModeloDTO tempoModelo)
        {
            return daoTempoModelo.Editar(tempoModelo);           
        }

        public static bool Deletar(int id)
        {
            return daoTempoModelo.Deletar(id);            
        }

    }
}
