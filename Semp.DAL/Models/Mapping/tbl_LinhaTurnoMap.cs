using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_LinhaTurnoMap : EntityTypeConfiguration<tbl_LinhaTurno>
    {
        public tbl_LinhaTurnoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodLinhaOri, t.CodLinhaDest, t.Turno });

            // Properties
            this.Property(t => t.CodLinhaOri)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.CodLinhaDest)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.Turno)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("tbl_LinhaTurno");
            this.Property(t => t.CodLinhaOri).HasColumnName("CodLinhaOri");
            this.Property(t => t.CodLinhaDest).HasColumnName("CodLinhaDest");
            this.Property(t => t.Turno).HasColumnName("Turno");
        }
    }
}
