using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
	public class CBEmbaladaDTO
	{
		public string NumECB { get; set; }
		public System.DateTime DatLeitura { get; set; }
		public int CodLinha { get; set; }
		public string NumLote { get; set; }        
		public string CodModelo { get; set; }
		public string CodLinhaFec { get; set; }
		
		public string CodOperador { get; set; }
		public string CodTestador { get; set; }        
		public int? NumPosto { get; set; }		
		public CBLinhaDTO Linha { get; set; }
		public int Turno { get; set; }

		[Display(Name = "C�digo da f�brica")]
		[Required]
		public string CodFab { get; set; }

		[Display(Name="N�mero da AP")]
		[Required]
		public string NumAP { get; set; }

		[Display(Name = "Data de refer�ncia")]
		[Required]        
		public string DatReferencia { get; set; }

		public string DescricaoLinha { get; set; }
		public string DescricaoPosto { get; set; }
		public string Operador { get; set; }
		public string DescricaoModelo { get; set; }
        public string NumCC { get; set; }

		public int QtdDefeitos { get; set; }

		public List<DadosAP> dadosAP;

        public class DadosAP
        {
            public string numAP;
            public string CodModelo;
            public string DesModelo;

        }

        [Display(Name = "Crach� do supervisor")]
        public string CrachaSupervisor { get; set; }
	}
}
