namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_TACS
    {
        [Key]
        public int COD_TAC { get; set; }

        public int? COD_MODELO { get; set; }

        [StringLength(8)]
        public string TAC { get; set; }

        public int? IMEI_BEGIN { get; set; }

        public int? IMEI_END { get; set; }

        public DateTime? DATA { get; set; }
    }
}
