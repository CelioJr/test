namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_SLCB_LIST
    {
        [Key]
        public int COD { get; set; }

        public int? COD_PESO { get; set; }

        [StringLength(20)]
        public string SERIAL { get; set; }

        public bool? LIDO { get; set; }

        [Column(TypeName = "image")]
        public byte[] IMAGEM { get; set; }

        public bool? RESPOSTA { get; set; }

        [StringLength(200)]
        public string RESPOSTA_STRING { get; set; }

        [Column(TypeName = "image")]
        public byte[] RESPOSTA_IMAGEM { get; set; }

        [StringLength(30)]
        public string SKU { get; set; }

        [StringLength(16)]
        public string IP { get; set; }

        public DateTime? DATA { get; set; }

        [StringLength(30)]
        public string LINHA { get; set; }
    }
}
