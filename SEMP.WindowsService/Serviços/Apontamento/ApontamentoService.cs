﻿using log4net;
using SEMP.BO.Integrator;
using SEMP.SAPConnector;
using SEMP.SAPConnector.VO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEMP.WindowsService
{
	public class ApontamentoService
	{


		//private static ILog _logger = LogManager.GetLogger(typeof(ApontamentoService));
		private static ILog _logger = LogManager.GetLogger("Apontamento");

		public Resposta RunJob()
		{
			Resposta retorno = new Resposta { Codigo = 0, Msg = "" };

			FazerApontamento();

			return new Resposta();
		}

		private void FazerApontamento()
		{
			var pacote = new ZSPP_APONTAMENTO_SISAP();

			try
			{

				SetStatus("Iniciando apontamento");
				SetStatus($"String de conexão: {Integrador.ObterStringConexao()}");
				var dados = Integrador.ObterApontamentoPendenteEnvio();

				SetStatus("Verificando os dados");
				if (dados != null && dados.Count() > 0)
				{
					SetStatus("Criando objetos e obtendo PACKNO");
					pacote = new ZSPP_APONTAMENTO_SISAP
					{
						PACKNO = Integrador.ObterPackNumber()
					};

					SetStatus("Atualizando número do pacote nos apontamentos");
					Integrador.AtualizarPackNOApontamento(pacote.PACKNO);

					SetStatus("Criando objeto com os dados");
					var lInttrow = new List<ZSPP_APONTAMENTO_PCI>();
					lInttrow.AddRange(dados.Select(x => new ZSPP_APONTAMENTO_PCI()
					{
						EXTROW = x.EXTROW.Trim(),
						AUFNR = x.AUFNR.Trim(),
						YIELD = x.YIELD.Value,
						MEINS = x.MEINS.Trim()//"ST"
					}));

					pacote.INTTROW = lInttrow.ToArray();

					SetStatus("Enviando para o SAP...");
					var resposta = SAPFunctions.ZFPP_APONTAMENTO_PCI_SISAP(pacote);

					if (resposta.Status)
					{
						SetStatus("Carregando dados apontados");
						var apontados = SAPFunctions.ObterApontamentoPacote(pacote.PACKNO);

						SetStatus("Carregar os dados do legado");
						dados = Integrador.ObterApontamentoPorPacote(pacote.PACKNO);

						if (apontados.Count == dados.Count)
						{
							SetStatus("Apontamento realizado com sucesso");
						}

						if (apontados.Where(x => x.NUCONF == 0).Count() > 0)
						{
							var destEmail = GetDestinatarios();

							Email.EnviaEmailPara("Itens a apontar", $"O apontamento do pacote '{pacote.PACKNO}' possui placas que não foram confirmadas.", destEmail);

							SetStatus("Há itens que foram enviados e não foram apontados. Foi enviado um e-mail.");
                            SetStatus($"Destinaários do email: {destEmail}");
						}
					}
					else
					{
						if (pacote != null)
						{
							var resultado = Integrador.DesfazerPackNOApontamento(pacote.PACKNO);
							_logger.Info("Desfeito o packnumber. Resultado: " + resultado.ToString());
						}
						var dest = GetDestinatarios();
						Email.EnviaEmailPara("Erro na integração", $"A execução da integração apresentou a seguinte mensagem: {resposta.Mensagem}. Com isso os itens voltaram a ficar pendentes de envio e serão enviados novamente no próximo horário de integração", dest);
					}

					SetStatus(resposta.Mensagem);
				}
				else
				{
					SetStatus("Nenhum registro a ser apontado");
				}
			}
			catch (Exception ex)
			{
				if (pacote != null)
				{
					var resultado = Integrador.DesfazerPackNOApontamento(pacote.PACKNO);
					_logger.Info("Desfeito o packnumber. Resultado: " + resultado.ToString());
				}
				_logger.Error("Erro ao executar o apontamento.", ex);
				Email.EnviaEmailPara("Erro na integração", $"A execução da integração causou uma exceção: {ex.Message}. Com isso os itens voltaram a ficar pendentes de envio e serão enviados novamente no próximo horário de integração", "");
			}
		}

		public void SetStatus(string msg)
		{
			_logger.Info(msg);
		}

		public string GetDestinatarios(){
			var destEmail = "";

			try
			{
				var dests = RAST.BO.Rastreabilidade.Geral.ObterDestinatariosEmail("ApontamentoPCI");
				destEmail = String.Join(";", dests.Select(x => x.EmailDestinatario));
			}
			catch { }

			return destEmail;
		}
	}
}
