namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_INTERVAL_PROD")]
    public partial class ALC_INTERVAL_PROD
    {
        [Key]
        public int idHour { get; set; }

        public int? finalHour { get; set; }

        public int? finalMin { get; set; }

        public int? inicialHour { get; set; }

        public int? inicialMin { get; set; }

        public int? shift_idShift { get; set; }

        public virtual ALC_SHIFT_PROD ALC_SHIFT_PROD { get; set; }
    }
}
