namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_cpentsai
    {
        [Key]
        public long id_cpentsai { get; set; }

        public long id_cp { get; set; }

        public long id_pt { get; set; }

        public DateTime dthr_inicio { get; set; }

        public DateTime? dthr_fim { get; set; }

        public virtual om_pt om_pt { get; set; }

        public virtual pp_cp pp_cp { get; set; }
    }
}
