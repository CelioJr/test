﻿using SEMP.DAL.Models;
using SEMP.Model.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.DAO.Rastreabilidade
{
	public class daoDAT
	{

		public static List<viw_RelOrdemServicoSTSDTO> ObterOSPorSerie(string codItem, string numSerie) {

			using (var contexto = new DTB_SIMContext())

			{

				var dados = contexto.viw_RelOrdemServicoSTS.Where(x => x.CodModelo == codItem && x.NumSerie == numSerie)
						.Select(x => new viw_RelOrdemServicoSTSDTO()
						{
							NumOS = x.NumOS,
							DatOS = x.DatOS,
							CodModelo = x.CodModelo,
							DesModelo = x.DesModelo,
							CodFam = x.CodFam,
							NomFam = x.NomFam,
							CodEstado = x.CodEstado,
							NomEstado = x.NomEstado,
							NumSerie = x.NumSerie,
							NumNotaFiscal = x.NumNotaFiscal,
							CodItem = x.CodItem,
							DesItem = x.DesItem,
							CodDefExec = x.CodDefExec,
							DesDefExec = x.DesDefExec,
							CodDefPeca = x.CodDefPeca,
							DesDefPeca = x.DesDefPeca,
							Posicao = x.Posicao,
							Quantidade = x.Quantidade,
							DefReclamado = x.DefReclamado,
							DefConstatado = x.DefConstatado,
							CodTecnico = x.CodTecnico,
							NomTecnico = x.NomTecnico,
							FlgStatus = x.FlgStatus,
							NomStatus = x.NomStatus
						});

				return dados.ToList();
			}

		}

	}
}
