namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_folharap
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_folharap()
        {
            dw_folharapcom = new HashSet<dw_folharapcom>();
        }

        [Key]
        public long id_folharap { get; set; }

        public long id_folha { get; set; }

        public long id_rap { get; set; }

        public decimal? qt_usada { get; set; }

        public decimal? seg_tempopreparacao { get; set; }

        public virtual dw_folha dw_folha { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folharapcom> dw_folharapcom { get; set; }

        public virtual dw_rap dw_rap { get; set; }
    }
}
