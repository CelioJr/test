using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_DCREstoqueMap : EntityTypeConfiguration<tbl_DCREstoque>
    {
        public tbl_DCREstoqueMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.CodItem, t.NumDCR });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumDCR)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.flgTipo)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_DCREstoque");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.NumDCR).HasColumnName("NumDCR");
            this.Property(t => t.flgTipo).HasColumnName("flgTipo");
            this.Property(t => t.QtdEstoque).HasColumnName("QtdEstoque");
            this.Property(t => t.QtdPrevisao).HasColumnName("QtdPrevisao");
            this.Property(t => t.DatEstoque).HasColumnName("DatEstoque");
        }
    }
}
