﻿using System;
using System.Collections.Generic;
using System.Linq;
using SEMP.DAL.Models;
using SEMP.Model.DTO;
using System.Data.SqlClient;
using System.Data;
using SEMP.Model;
using SEMP.Model.VO.Rastreabilidade;

namespace SEMP.DAL.DAO.Rastreabilidade
{
	public class daoLote
	{

		#region Consultas
		public static CBLoteDTO UltimoLote(int codLinha, int numPosto)
		{

			using (var dtbCB = new DTB_CBContext())
			{

				try
				{
					return dtbCB.tbl_CBLote.Where(x => x.CodLinha == codLinha && x.NumPosto == numPosto && x.NumLotePai == null)
						.OrderByDescending(x => x.DatAbertura)
						.Select(x => new CBLoteDTO(){
							NumLote = x.NumLote,
							Qtde = x.Qtde,
							DatAbertura = x.DatAbertura,
							DatFechamento = x.DatFechamento,
							Status = x.Status,
							DatTransferencia = x.DatTransferencia,
							DatRecebimento = x.DatRecebimento,
							CodDRTFechamento = x.CodDRTFechamento,
							JustFechamento = x.JustFechamento,
							CodDRTRecebimento = x.CodDRTRecebimento,
							CodModelo = x.CodModelo,
							CodFab = x.CodFab,
							NumAP = x.NumAP,
							TamLote = x.TamLote,
							NumLotePai = x.NumLotePai,
							CodLinha = x.CodLinha,
							NumPosto = x.NumPosto,
							TamAmostras = x.TamAmostras
						}).FirstOrDefault();
				}
				catch
				{
					return new CBLoteDTO();
					throw;
				}

			}

		}

		public static CBLoteDTO UltimoLoteCC(int codLinha, int numPosto)
		{

			using (var dtbCB = new DTB_CBContext())
			{

				try
				{
					return dtbCB.tbl_CBLote.Where(x => x.CodLinha == codLinha && x.NumPosto == numPosto && x.NumLotePai != null && x.DatFechamento == null)
						.OrderByDescending(x => x.DatAbertura)
						.Select(x => new CBLoteDTO()
						{
							NumLote = x.NumLote,
							Qtde = x.Qtde,
							DatAbertura = x.DatAbertura,
							DatFechamento = x.DatFechamento,
							Status = x.Status,
							DatTransferencia = x.DatTransferencia,
							DatRecebimento = x.DatRecebimento,
							CodDRTFechamento = x.CodDRTFechamento,
							JustFechamento = x.JustFechamento,
							CodDRTRecebimento = x.CodDRTRecebimento,
							CodModelo = x.CodModelo,
							CodFab = x.CodFab,
							NumAP = x.NumAP,
							TamLote = x.TamLote,
							NumLotePai = x.NumLotePai,
							CodLinha = x.CodLinha,
							NumPosto = x.NumPosto,
							TamAmostras = x.TamAmostras
						}).FirstOrDefault();
				}
				catch
				{
					return new CBLoteDTO();
					throw;
				}

			}

		}

		public static string NumUltimoLote(int codLinha, int numPosto)
		{

			using (var dtbCB = new DTB_CBContext()){

				try
				{
					return dtbCB.tbl_CBLote.Where(x => x.CodLinha == codLinha && x.NumPosto == numPosto).OrderByDescending(x => x.DatAbertura).FirstOrDefault().NumLote;
				}
				catch
				{
					return "";
					throw;
				}

			}

		}

		public static int QtdTamLote(string numLote)
		{

			using (var dtbCB = new DTB_CBContext())
			{

				try
				{
					var tamLote = dtbCB.tbl_CBLote.FirstOrDefault(x => x.NumLote == numLote).TamLote ?? 0;

					return tamLote;
				}
				catch
				{
					return 0;
					throw;
				}

			}

		}

		public static int QtdLidoLote(string numLote)
		{

			using (var dtbCB = new DTB_CBContext())
			{

				try
				{
					return dtbCB.tbl_CBLoteItem.Count(x => x.NumLote == numLote);
				}
				catch
				{
					return 0;
					throw;
				}

			}

		}

		public static int QtdLidoLoteCC(string numLote)
		{

			using (var dtbCB = new DTB_CBContext())
			{

				try
				{
					return dtbCB.tbl_CBLoteItem.Count(x => x.NumCC == numLote);
				}
				catch
				{
					return 0;
					throw;
				}

			}

		}

		public static int QtdAprovadoLoteCC(string numLote)
		{

			using (var dtbCB = new DTB_CBContext())
			{

				try
				{
					return dtbCB.tbl_CBLoteItem.Count(x => x.NumCC == numLote && x.Status == SEMP.Model.Constantes.LOTE_APROVADO_OBA);
				}
				catch
				{
					return 0;
					throw;
				}

			}

		}

		public static bool LoteFechadoAmostras(string numLote)
		{

			using (var dtbCB = new DTB_CBContext())
			{

				try
				{
					var lote = (from l in dtbCB.tbl_CBLote
								let qtdAmostra = dtbCB.tbl_CBLoteItem.Count(x => x.NumCC == numLote && x.flgAmostra == true && x.Status == Constantes.LOTE_APROVADO_OBA)
								where l.NumLote == numLote
								select new {
									TamAmostras = l.TamAmostras,
									QtdAmostras = qtdAmostra
								}).FirstOrDefault();


					return (lote.TamAmostras <= lote.QtdAmostras);
				}
				catch
				{
					return false;
					throw;
				}

			}

		}

		public static List<CBLoteItemDTO> ObterPassagemPostoLote(int codLinha, int numPosto)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				dtb_CB.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

				var ultLeitura = (from p in dtb_CB.tbl_CBLoteItem
									 join l in dtb_CB.tbl_CBLote on p.NumCC equals l.NumLote
									 where l.CodLinha == codLinha && l.NumPosto == numPosto
									 select p).OrderByDescending(x => x.DatInclusao).FirstOrDefault();

				if (ultLeitura != null)
				{
					var passagens = dtb_CB.tbl_CBLoteItem.Where(x => x.NumCC == ultLeitura.NumCC)
					.Select(x => new CBLoteItemDTO() {
						NumLote = x.NumLote,
						NumCC = x.NumCC,
						NumECB = x.NumECB,
						Status = x.Status,
						flgAmostra = x.flgAmostra,
						DatInclusao = x.DatInclusao,
						DatInspecao = x.DatInspecao,
						ResultadoInspecao = x.ResultadoInspecao
					}).ToList();

					return passagens;
				}
				else
				{
					return new List<CBLoteItemDTO>();
				}
			}
		}

		public static List<CBLoteItemDTO> ObterPassagemPostoLote(string numLote)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				dtb_CB.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

				var passagens = dtb_CB.tbl_CBLoteItem.Where(x => x.NumCC == numLote)
				.Select(x => new CBLoteItemDTO()
				{
					NumLote = x.NumLote,
					NumCC = x.NumCC,
					NumECB = x.NumECB,
					Status = x.Status,
					flgAmostra = x.flgAmostra,
					DatInclusao = x.DatInclusao,
					DatInspecao = x.DatInspecao,
					ResultadoInspecao = x.ResultadoInspecao
				}).ToList();

				return passagens;

			}
		}

		public static List<CBLoteItemDTO> ObterItensLoteSerie(string numECB)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				dtb_CB.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");


				var registro = dtb_CB.tbl_CBLoteItem.FirstOrDefault(x => x.NumECB == numECB);

				if (registro != null) { 
					var passagens = dtb_CB.tbl_CBLoteItem.Where(x => x.NumCC == registro.NumCC)
															.Select(x => new CBLoteItemDTO()
															{
																NumLote = x.NumLote,
																NumCC = x.NumCC,
																NumECB = x.NumECB,
																Status = x.Status,
																flgAmostra = x.flgAmostra,
																DatInclusao = x.DatInclusao,
																DatInspecao = x.DatInspecao,
																ResultadoInspecao = x.ResultadoInspecao
															})
															.ToList();
				
					return passagens;
				}
				else
				{
					return new List<CBLoteItemDTO>();
				}
			}
		}

		public static CBLoteDTO ObterLote(string numLote)
		{

			using (var dtbCB = new DTB_CBContext())
			{

				try
				{


                    var query = (from lote in dtbCB.tbl_CBLote
                                 join linha in dtbCB.tbl_CBLinha on lote.CodLinha equals linha.CodLinha
                                 join modelo in dtbCB.tbl_Item on lote.CodModelo equals modelo.CodItem
                                 let qtdRec = dtbCB.tbl_CBLote.Count(x => x.NumLotePai == lote.NumLote && x.Status == 3)
                                 where lote.NumLote == numLote
                                 orderby new { lote.CodModelo, lote.DatAbertura }
                                 select new { lote, linha, modelo, qtdRec }).Select(x => new CBLoteDTO()
                                 {
                                     CodDRTFechamento = x.lote.CodDRTFechamento,
                                     CodDRTRecebimento = x.lote.CodDRTRecebimento,
                                     CodFab = x.lote.CodFab,
                                     CodLinha = x.lote.CodLinha,
                                     CodModelo = x.lote.CodModelo,
                                     DatAbertura = x.lote.DatAbertura,
                                     DatFechamento = x.lote.DatFechamento,
                                     DatRecebimento = x.lote.DatRecebimento,
                                     DatTransferencia = x.lote.DatTransferencia,
                                     JustFechamento = x.lote.JustFechamento,
                                     NumAP = x.lote.NumAP,
                                     NumLote = x.lote.NumLote,
                                     NumLotePai = x.lote.NumLotePai,
                                     NumPosto = x.lote.NumPosto,
                                     Qtde = x.lote.Qtde,
                                     Status = x.lote.Status,
                                     TamAmostras = x.lote.TamAmostras,
                                     TamLote = x.lote.TamLote,
                                     Setor = x.linha.Setor,
                                     DesLinha = x.linha.DesLinha.Trim(),
                                     DesModelo = x.modelo.DesItem.Trim(),
                                     QtdRec = x.qtdRec
                                 });


                    return query.FirstOrDefault();

                }
				catch
				{
					return new CBLoteDTO();
					throw;
				}

			}

		}

		public static CBLoteDTO ObterLoteCCSerie(string numECB)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				dtb_CB.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

				var registro = dtb_CB.tbl_CBLoteItem.FirstOrDefault(x => x.NumECB == numECB && x.Status != 6);

				if (registro != null)
				{
					var passagens = dtb_CB.tbl_CBLote
											.Where(x => x.NumLote == registro.NumCC && x.Status != Constantes.LOTE_REPROVADO_OBA)
											.Select(x => new CBLoteDTO()
											{
												NumLote = x.NumLote,
												Qtde = x.Qtde,
												DatAbertura = x.DatAbertura,
												DatFechamento = x.DatFechamento,
												Status = x.Status,
												DatTransferencia = x.DatTransferencia,
												DatRecebimento = x.DatRecebimento,
												CodDRTFechamento = x.CodDRTFechamento,
												JustFechamento = x.JustFechamento,
												CodDRTRecebimento = x.CodDRTRecebimento,
												CodModelo = x.CodModelo,
												CodFab = x.CodFab,
												NumAP = x.NumAP,
												TamLote = x.TamLote,
												NumLotePai = x.NumLotePai,
												CodLinha = x.CodLinha,
												NumPosto = x.NumPosto,
												TamAmostras = x.TamAmostras
											})
											.FirstOrDefault();

					return passagens;
				}
				else
				{
					return new CBLoteDTO();
				}
			}
		}

		public static CBLoteDTO ObterLoteCCSerie(string numECB, int codLinha)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				dtb_CB.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

				var registro = dtb_CB.tbl_CBEmbalada.FirstOrDefault(x => x.NumECB == numECB && x.CodLinha == codLinha);

				if (registro != null)
				{
					var passagens = dtb_CB.tbl_CBLote
											.Where(x => x.NumLote == registro.NumCC && x.Status != Constantes.LOTE_REPROVADO_OBA)
											.Select(x => new CBLoteDTO()
											{
												NumLote = x.NumLote,
												Qtde = x.Qtde,
												DatAbertura = x.DatAbertura,
												DatFechamento = x.DatFechamento,
												Status = x.Status,
												DatTransferencia = x.DatTransferencia,
												DatRecebimento = x.DatRecebimento,
												CodDRTFechamento = x.CodDRTFechamento,
												JustFechamento = x.JustFechamento,
												CodDRTRecebimento = x.CodDRTRecebimento,
												CodModelo = x.CodModelo,
												CodFab = x.CodFab,
												NumAP = x.NumAP,
												TamLote = x.TamLote,
												NumLotePai = x.NumLotePai,
												CodLinha = x.CodLinha,
												NumPosto = x.NumPosto,
												TamAmostras = x.TamAmostras
											})
											.FirstOrDefault();

					return passagens;
				}
				else
				{
					return new CBLoteDTO();
				}
			}
		}

		public static CBLoteDTO ObterLoteSerie(string numECB)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				dtb_CB.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

				var registro = dtb_CB.tbl_CBLoteItem.FirstOrDefault(x => x.NumECB == numECB);

				if (registro != null)
				{
					var passagens = dtb_CB.tbl_CBLote
											.Where(x => x.NumLote == registro.NumLote && x.Status != SEMP.Model.Constantes.LOTE_REPROVADO_OBA)
											.Select(x => new CBLoteDTO()
											{
												NumLote = x.NumLote,
												Qtde = x.Qtde,
												DatAbertura = x.DatAbertura,
												DatFechamento = x.DatFechamento,
												Status = x.Status,
												DatTransferencia = x.DatTransferencia,
												DatRecebimento = x.DatRecebimento,
												CodDRTFechamento = x.CodDRTFechamento,
												JustFechamento = x.JustFechamento,
												CodDRTRecebimento = x.CodDRTRecebimento,
												CodModelo = x.CodModelo,
												CodFab = x.CodFab,
												NumAP = x.NumAP,
												TamLote = x.TamLote,
												NumLotePai = x.NumLotePai,
												CodLinha = x.CodLinha,
												NumPosto = x.NumPosto,
												TamAmostras = x.TamAmostras
											})
											.FirstOrDefault();

					return passagens;
				}
				else
				{
					return new CBLoteDTO();
				}
			}
		}

		public static bool PertenceItemLote(string numECB, string numLote)
		{

			using (var dtbCB = new DTB_CBContext())
			{

				try
				{
					var retorno = dtbCB.tbl_CBLoteItem.Count(x => x.NumCC == numLote && x.NumECB == numECB);

					return (retorno > 0);
				}
				catch
				{
					return false;
					throw;
				}
			}

		}

		public static string ObterFabricaPlaca(string numECB)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				var dado = dtbCB.tbl_CBEmbalada.OrderByDescending(x => x.DatLeitura).FirstOrDefault(x => x.NumECB == numECB && x.CodFab != "");

				if (dado != null)
				{
					return dado.CodFab;
				}
				else
				{
					return Constantes.CodFab02;
				}

			}

		}

		public static bool LiberarReimpressaoLote(string numCC) {

			using (var contexto = new DTB_CBContext())
			{
				try
				{
					var lote = contexto.tbl_CBImpressaoLote.FirstOrDefault(x => x.NumLote == numCC);

					lote.Status = false;

					contexto.SaveChanges();

				}
				catch
				{
					return false;
				}
			}

			return true;
		
		}

		#endregion

		#region Manutenção
		public static Boolean InserePassagemProcedureLote(tbl_CBPassagem passagem, string CodDRT, string numLote, string numCC)
		{
			try
			{
				using (var contexto = new DTB_CBContext())
				{

					var resultado = contexto.Database.SqlQuery<spc_CBFecGravarPassagem>(
							"EXEC spc_CBFecGravarPassagemLote @CodLinha, @NumPosto, @NumSerie, @DRT, @CodFab, @NumAP, @NumLote, @NumCC",
							new SqlParameter("CodLinha", passagem.CodLinha),
							new SqlParameter("NumPosto", passagem.NumPosto),
							new SqlParameter("NumSerie", passagem.NumECB),
							new SqlParameter("DRT", passagem.CodDRT),
							new SqlParameter("CodFab", ""),
							new SqlParameter("NumAP", ""),
							new SqlParameter("NumLote", numLote),
							new SqlParameter("NumCC", numCC)
							);

					var retorno = resultado.FirstOrDefault();

					if (retorno.Result == 0)
					{
						return true;
					}
					else
					{
						return false;
					}

				}
			}
			catch
			{
				return false;
			}
		}

		public static Boolean AprovaLeituraItemLote(string numECB, string numCC)
		{
			try
			{
				using (var contexto = new DTB_CBContext())
				{
					var item = contexto.tbl_CBLoteItem.FirstOrDefault(x => x.NumECB == numECB && x.NumCC == numCC);

					if (item != null)
					{
						item.flgAmostra = true;
						item.DatInspecao = DateTime.Now;
						item.Status = SEMP.Model.Constantes.LOTE_APROVADO_OBA;
						item.ResultadoInspecao = "APROVADO OBA";

						contexto.SaveChanges();

						return true;
					}
					else
					{
						return false;
					}
				}
			}
			catch
			{
				return false;
			}
		}

		/// <summary>
		/// Método que faz a abertura de uma nova caixa coletiva
		/// </summary>
		/// <param name="idLinha"></param>
		/// <param name="serial"></param>
		/// <param name="codModelo"></param>
		/// <returns></returns>
		public static string AbrirNovaCaixaColetiva(int codLinha, int numPosto, string codModelo, string codFab, string numAp)
		{
			string novaCC = "";
			string ret = "";

			using (var dtbCb = new DTB_CBContext())
			{
				//recupera o setor, de acordo com a linha
				var setor = (from b in dtbCb.tbl_CBLinha where b.CodLinha == codLinha select b.Setor).FirstOrDefault();

				switch (setor)
				{
					case "IAC":
						setor = "1";
						break;
					case "IMC":
						setor = "2";
						break;
					case "FEC":
						setor = "3";
						break;
				}

                SqlParameter ParametroRetorno = new SqlParameter("Retorno", ret)
                {
                    Direction = ParameterDirection.Output
                };
                SqlParameter[] parameters =
				{    
					new SqlParameter("@idLinha", SqlDbType.VarChar) {Value= codLinha},
					new SqlParameter("@numPosto", SqlDbType.VarChar) {Value= numPosto},
					new SqlParameter("@codModelo", SqlDbType.VarChar) {Value = codModelo},
					new SqlParameter("@setor", SqlDbType.VarChar) {Value = setor},
					new SqlParameter("@DataReferencia", SqlDbType.SmallDateTime) {Value = DateTime.Now},
					new SqlParameter("@CodFab", SqlDbType.VarChar) {Value = codFab},
					new SqlParameter("@NumAP", SqlDbType.VarChar) { Value = numAp},
					ParametroRetorno
				};

				SqlConnection con = new SqlConnection(dtbCb.Database.Connection.ConnectionString);
				con.Open();
				try
				{
					string cmd = "EXEC spc_CBNovaCaixaColetiva @idLinha, @numPosto, @codModelo, @setor, @DataReferencia, @CodFab, @NumAP, @Retorno";
					SqlCommand oCom = new SqlCommand(cmd, con);

					foreach (SqlParameter param in parameters)
						oCom.Parameters.Add(param);

					DataSet oDs = new DataSet();

					using (SqlDataAdapter oDa = new SqlDataAdapter())
					{

						oDa.SelectCommand = oCom;

						oDa.Fill(oDs);
					}
					var result = "";

					if (oDs != null)
					{
						foreach (DataTable table in oDs.Tables)
						{
							foreach (DataColumn coluna in table.Columns)
							{
								if (coluna.ColumnName == "RESULT")
									result = table.Rows[0]["RESULT"].ToString();

								else if (coluna.ColumnName == "SECRESULT")
									novaCC = table.Rows[0]["SECRESULT"].ToString();
							}

						}
					}
				}
				catch (Exception ex)
				{
					throw new Exception("Erro ao abrir Caixa Coletiva", ex);
				}
				finally
				{
					con.Close();
				}
			}
			return novaCC;
		}

		/// <summary>
		/// Método que faz a abertura de um novo lote
		/// </summary>
		/// <param name="idLinha"></param>
		/// <param name="serial"></param>
		/// <param name="codModelo"></param>
		/// <returns></returns>
		public static string AbrirNovoLote(int codLinha, int numPosto, string codModelo, string codFab, string numAp)
		{
			string novoLote;
			string ret = "";

			using (var dtbCb = new DTB_CBContext())
			{
				//recupera o setor, de acordo com a linha
				var setor = (from b in dtbCb.tbl_CBLinha where b.CodLinha == codLinha select b.Setor).FirstOrDefault();

				switch (setor)
				{
					case "IAC":
						setor = "1";
						break;
					case "IMC":
						setor = "2";
						break;
					case "FEC":
						setor = "3";
						break;
				}

                SqlParameter ParametroRetorno = new SqlParameter("Retorno", ret)
                {
                    Direction = ParameterDirection.Output
                };
                var resultado = dtbCb.Database.SqlQuery<spc_CBFecOpenLote>(
					"EXEC spc_CBNovoLote @idLinha, @numPosto, @codModelo, @setor, @DataReferencia, @CodFab, @NumAP, @Retorno",
					new SqlParameter("idLinha", codLinha),
					new SqlParameter("numPosto", numPosto),
					new SqlParameter("codModelo", codModelo),
					new SqlParameter("setor", setor),
					new SqlParameter("DataReferencia", DateTime.Now),
					new SqlParameter("CodFab", codFab),
					new SqlParameter("NumAP", numAp),
					ParametroRetorno
					);

				novoLote = resultado.First().RESULT;

				dtbCb.SaveChanges();
			}

			return novoLote;
		}

		/// <summary>
		/// Método que faz o fechamento da Caixa Coletiva atual
		/// </summary>
		/// <param name="numLote"></param>
		/// <param name="codDrt"></param>
		/// <param name="justificativa"></param>
		/// <returns></returns>
		public static bool FecharCaixaColetiva(string numLote, string codDrt, string justificativa)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				try
				{
					var res = dtbCb.Database.SqlQuery<spc_CBFecCloseLote>(
						"EXEC spc_CBFecharCaixaColetiva @numLote, @codDRT, @justificativa",
						new SqlParameter("numLote", numLote),
						new SqlParameter("codDRT", codDrt),
						new SqlParameter("justificativa", justificativa));

					var valor = res.First().Result;

					dtbCb.SaveChanges();
				}
				catch (Exception)
				{
					return false;
				}
			}

			return true;
		}

		/// <summary>
		/// Método que faz o fechamento do Lote atual
		/// </summary>
		/// <param name="numLote"></param>
		/// <param name="codDrt"></param>
		/// <param name="justificativa"></param>
		/// <returns></returns>
		public static bool FecharLote(string numLote, string codDrt, string justificativa)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				try
				{
					var res = dtbCb.Database.SqlQuery<spc_CBFecCloseLote>(
						"EXEC spc_CBNovoFecharLote @numLote, @codDRT, @justificativa",
						new SqlParameter("numLote", numLote),
						new SqlParameter("codDRT", codDrt),
						new SqlParameter("justificativa", justificativa));

					var valor = res.First().Result;

					dtbCb.SaveChanges();
				}
				catch (Exception)
				{
					return false;
				}
			}

			return true;
		}
				
		public static bool ExcluirLote(string numLote)
		{

			using (var dtbCB = new DTB_CBContext())
			{

				try
				{

					var lote = dtbCB.tbl_CBLote.FirstOrDefault(x => x.NumLote == numLote);

					if (lote != null)
					{
						var existeLeitura = new tbl_CBLoteItem();

						if (lote.NumLotePai == null) { 
							existeLeitura = dtbCB.tbl_CBLoteItem.FirstOrDefault(x => x.NumLote == numLote);
						}
						else
						{
							existeLeitura = dtbCB.tbl_CBLoteItem.FirstOrDefault(x => x.NumCC== numLote);
						}

						if (existeLeitura == null)
						{

							dtbCB.tbl_CBLote.Remove(lote);
							dtbCB.SaveChanges();

						}
					}

				}
				catch (Exception)
				{
					return false;
				}

				return true;
			}

		}

		public static Boolean AprovarLoteOBA(string numCC, string codDRT, int codLinha, int numPosto)
		{
			try
			{
				using (var contexto = new DTB_CBContext())
				{

					// encontra o registro do lote
					var lote = contexto.tbl_CBLote.FirstOrDefault(x => x.NumLote == numCC);

					if (lote != null)
					{
						// atualiza a flag do status
						lote.Status = SEMP.Model.Constantes.LOTE_APROVADO_OBA;

						// pega todos os itens do lote
						var itens = contexto.tbl_CBLoteItem.Where(x => x.NumCC == numCC).ToList();

						// atualiza os dados dos itens do lote
						foreach (var item in itens)
						{
							item.DatInspecao = DateTime.Now;
							item.Status = SEMP.Model.Constantes.LOTE_APROVADO_OBA;
							item.ResultadoInspecao = "APROVADO OBA";
							if (item.flgAmostra == null)
							{
								item.flgAmostra = false;
							}

							// registra a passagem dos itens, se não forem o da amostra
							if (item.flgAmostra == false) { 
								var passagem = new tbl_CBPassagem
								{
									CodFab = "",
									NumAP = "",
									CodLinha = codLinha,
									NumPosto = numPosto,
									NumECB = item.NumECB,
									DatEvento = item.DatInspecao.Value,
									CodDRT = codDRT
								};

								contexto.tbl_CBPassagem.Add(passagem);
							}

							var tabela = new tbl_CBEmbalada
							{
								CodFab = "",
								CodLinha = codLinha,
								CodLinhaFec = "",
								CodModelo = lote.CodModelo,
								CodOperador = codDRT,
								CodTestador = item.flgAmostra == true ? "SIM" : "",
								DatLeitura = item.DatInspecao.Value,
								DatReferencia = Convert.ToDateTime(item.DatInspecao.Value.ToShortDateString()),
								NumAP = "",
								NumECB = item.NumECB,
								NumLote = item.NumLote,
								NumPosto = numPosto,
								NumCC = item.NumCC
							};

							contexto.tbl_CBEmbalada.Add(tabela);

						}

						// grava as alterações
						contexto.SaveChanges();

						return true;
					}
					else
					{
						return false;
					}

				}
			}
			catch
			{
				return false;
			}
		}

		public static Boolean ReprovarItemLoteOBA(string numCC, string numECB, string codDefeito)
		{
			try
			{
				using (var contexto = new DTB_CBContext())
				{

					// encontra o registro do lote
					var lote = contexto.tbl_CBLote.FirstOrDefault(x => x.NumLote == numCC);

					if (lote != null)
					{
						// atualiza a flag do status
						lote.Status = SEMP.Model.Constantes.LOTE_REPROVADO_OBA;

						// pega todos os itens do lote
						var itens = contexto.tbl_CBLoteItem.Where(x => x.NumCC == numCC).ToList();

						// declara o objeto que vai receber os registros de passagem
						var passagens = new List<tbl_CBPassagem>();

						// atualiza os dados dos itens do lote
						foreach (var item in itens)
						{
							item.DatInspecao = DateTime.Now;
							item.Status = SEMP.Model.Constantes.LOTE_REPROVADO_OBA;
							item.ResultadoInspecao = String.Format("REPROVADO OBA COM O DEFEITO: {0}", codDefeito);

							if (item.flgAmostra == null)
							{
								item.flgAmostra = false;
							}
							if (item.NumECB == numECB)
							{
								item.flgAmostra = true;
							}

							// seleciona as passagens do número de série atual
							var passagem = contexto.tbl_CBPassagem.Where(x => x.NumECB == item.NumECB).ToList();
							// adiciona ao objeto que irá receber as passagens para excluir
							passagens.AddRange(passagem);

						}

						// exclui os registros de passagem
						contexto.tbl_CBPassagem.RemoveRange(passagens);

						// grava as alterações
						contexto.SaveChanges();

						return true;
					}
					else
					{
						return false;
					}

				}
			}
			catch
			{
				return false;
			}
		}

		public static Boolean ImprimirEtiquetaMagazine(string numCC, string codDRT, int codLinha)
		{
			try
			{
				using (var contexto = new DTB_CBContext())
				{

					var novodado = new tbl_CBImpressaoLote() {
						NumLote = numCC,
						fase = codLinha,
						CodDrt = codDRT,
						Status = false,
						DataImpressao = Common.daoUtil.GetDate()
					};

					contexto.tbl_CBImpressaoLote.Add(novodado);
					// grava as alterações
					contexto.SaveChanges();

					return true;

				}
			}
			catch
			{
				return false;
			}
		}
		#endregion

		#region Relatório de Lotes
		public static List<CBLoteDTO> ObterLotes(Func<CBLoteDTO, bool> predicate)
		{
			using (var dtbCB = new DTB_CBContext())
			{

				var query = (from lote in dtbCB.tbl_CBLote
							 join linha in dtbCB.tbl_CBLinha on lote.CodLinha equals linha.CodLinha
							 join modelo in dtbCB.tbl_Item on lote.CodModelo equals modelo.CodItem
							 let qtdRec = dtbCB.tbl_CBLote.Count(x => x.NumLotePai == lote.NumLote && x.Status == 3)
							 orderby new { lote.CodModelo, lote.DatAbertura }
							 select new {lote, linha, modelo, qtdRec}).Select(x => new CBLoteDTO(){
								 CodDRTFechamento = x.lote.CodDRTFechamento,
								 CodDRTRecebimento = x.lote.CodDRTRecebimento,
								 CodFab = x.lote.CodFab,
								 CodLinha = x.lote.CodLinha,
								 CodModelo = x.lote.CodModelo,
								 DatAbertura = x.lote.DatAbertura,
								 DatFechamento = x.lote.DatFechamento,
								 DatRecebimento = x.lote.DatRecebimento,
								 DatTransferencia = x.lote.DatTransferencia,
								 JustFechamento = x.lote.JustFechamento,
								 NumAP = x.lote.NumAP,
								 NumLote = x.lote.NumLote,
								 NumLotePai = x.lote.NumLotePai,
								 NumPosto = x.lote.NumPosto,
								 Qtde = x.lote.Qtde,
								 Status = x.lote.Status,
								 TamAmostras = x.lote.TamAmostras,
								 TamLote = x.lote.TamLote,
								 Setor = x.linha.Setor,
								 DesLinha = x.linha.DesLinha.Trim(),
								 DesModelo = x.modelo.DesItem.Trim(),
								 QtdRec = x.qtdRec
				});

				return query.Where(predicate).ToList();

			}

		}

		public static List<CBLoteDTO> ObterLotes(DateTime datInicio, string setor)
		{
			using (var dtbCB = new DTB_CBContext())
			{

				var query = (from lote in dtbCB.tbl_CBLote
							 join linha in dtbCB.tbl_CBLinha on lote.CodLinha equals linha.CodLinha
							 join modelo in dtbCB.tbl_Item on lote.CodModelo equals modelo.CodItem
							 let qtdRec = dtbCB.tbl_CBLote.Count(x => x.NumLotePai == lote.NumLote && x.Status == 3)
							 where lote.NumLotePai == null && lote.DatAbertura >= datInicio && linha.Setor == setor
							 orderby new { lote.CodModelo, lote.DatAbertura }
							 select new { lote, linha, modelo, qtdRec }).Select(x => new CBLoteDTO()
							 {
								 CodDRTFechamento = x.lote.CodDRTFechamento,
								 CodDRTRecebimento = x.lote.CodDRTRecebimento,
								 CodFab = x.lote.CodFab,
								 CodLinha = x.lote.CodLinha,
								 CodModelo = x.lote.CodModelo,
								 DatAbertura = x.lote.DatAbertura,
								 DatFechamento = x.lote.DatFechamento,
								 DatRecebimento = x.lote.DatRecebimento,
								 DatTransferencia = x.lote.DatTransferencia,
								 JustFechamento = x.lote.JustFechamento,
								 NumAP = x.lote.NumAP,
								 NumLote = x.lote.NumLote,
								 NumLotePai = x.lote.NumLotePai,
								 NumPosto = x.lote.NumPosto,
								 Qtde = x.lote.Qtde,
								 Status = x.lote.Status,
								 TamAmostras = x.lote.TamAmostras,
								 TamLote = x.lote.TamLote,
								 Setor = x.linha.Setor,
								 DesLinha = x.linha.DesLinha.Trim(),
								 DesModelo = x.modelo.DesItem.Trim(),
								 QtdRec = x.qtdRec
							 });

				return query.ToList();

			}

		}

		public static List<CBLoteDTO> ObterLotes(DateTime datInicio, DateTime? datFim = null)
		{
			using (var dtbCB = new DTB_CBContext())
			{

				var query = (from lote in dtbCB.tbl_CBLote
							 join linha in dtbCB.tbl_CBLinha on lote.CodLinha equals linha.CodLinha
							 join modelo in dtbCB.tbl_Item on lote.CodModelo equals modelo.CodItem
							 where lote.DatAbertura	>= datInicio && lote.NumLotePai == null
							 orderby lote.DatAbertura
							 select new { lote, linha, modelo }).Select(x => new CBLoteDTO()
							 {
								 CodDRTFechamento = x.lote.CodDRTFechamento,
								 CodDRTRecebimento = x.lote.CodDRTRecebimento,
								 CodFab = x.lote.CodFab,
								 CodLinha = x.lote.CodLinha,
								 CodModelo = x.lote.CodModelo,
								 DatAbertura = x.lote.DatAbertura,
								 DatFechamento = x.lote.DatFechamento,
								 DatRecebimento = x.lote.DatRecebimento,
								 DatTransferencia = x.lote.DatTransferencia,
								 JustFechamento = x.lote.JustFechamento,
								 NumAP = x.lote.NumAP,
								 NumLote = x.lote.NumLote,
								 NumLotePai = x.lote.NumLotePai,
								 NumPosto = x.lote.NumPosto,
								 Qtde = x.lote.Qtde,
								 Status = x.lote.Status,
								 TamAmostras = x.lote.TamAmostras,
								 TamLote = x.lote.TamLote,
								 Setor = x.linha.Setor,
								 DesLinha = x.linha.DesLinha.Trim(),
								 DesModelo = x.modelo.DesItem.Trim()
							 });

				if (datFim != null)
				{
					query = query.Where(x => x.DatAbertura <= datFim.Value);
				}

				return query.ToList();

			}

		}

		public static List<CBLoteDTO> ObterCaixasColetivas(string numLote)
		{
			using (var dtbCB = new DTB_CBContext())
			{

				var query = (from lote in dtbCB.tbl_CBLote
							 join linha in dtbCB.tbl_CBLinha on lote.CodLinha equals linha.CodLinha
							 join modelo in dtbCB.tbl_Item on lote.CodModelo equals modelo.CodItem
							 where lote.NumLotePai == numLote
							 orderby lote.DatAbertura
							 select new { lote, linha, modelo });

				return query.Select(x => new CBLoteDTO()
				{
					CodDRTFechamento = x.lote.CodDRTFechamento,
					CodDRTRecebimento = x.lote.CodDRTRecebimento,
					CodFab = x.lote.CodFab,
					CodLinha = x.lote.CodLinha,
					CodModelo = x.lote.CodModelo,
					DatAbertura = x.lote.DatAbertura,
					DatFechamento = x.lote.DatFechamento,
					DatRecebimento = x.lote.DatRecebimento,
					DatTransferencia = x.lote.DatTransferencia,
					JustFechamento = x.lote.JustFechamento,
					NumAP = x.lote.NumAP,
					NumLote = x.lote.NumLote,
					NumLotePai = x.lote.NumLotePai,
					NumPosto = x.lote.NumPosto,
					Qtde = x.lote.Qtde,
					Status = x.lote.Status,
					TamAmostras = x.lote.TamAmostras,
					TamLote = x.lote.TamLote,
					Setor = x.linha.Setor,
					DesLinha = x.linha.DesLinha.Trim(),
					DesModelo = x.modelo.DesItem.Trim()
				}).ToList();

			}

		}

		public static List<LotesDaVez> ObterLotesDaVez(string setor, string codModelo, int? codLinha)
		{
			using (var dtbCB = new DTB_CBContext())
			{

				var query = (from lote in dtbCB.tbl_CBLote
							 join linha in dtbCB.tbl_CBLinha on lote.CodLinha equals linha.CodLinha
							 join modelo in dtbCB.tbl_Item on lote.CodModelo equals modelo.CodItem
							 where lote.Status == 1 && lote.DatRecebimento == null && !lote.NumLote.StartsWith("C") && linha.Setor == setor && lote.DatTransferencia != null
									&& !dtbCB.tbl_CBFIFOExcecao.Select(x => x.NumLote).ToList().Contains(lote.NumLote)
							 group new { lote, linha, modelo } by new { linha.CodLinha, linha.DesLinha, modelo.CodItem, modelo.DesItem } into grupo
							 orderby new { grupo.Key.DesItem }
							 select new LotesDaVez()
							 {
								 CodLinha = grupo.Key.CodLinha,
								 CodModelo = grupo.Key.CodItem,
								 NumLote = grupo.Min(x => x.lote.NumLote),
								 DesLinha = grupo.Key.DesLinha.Trim(),
								 DesModelo = grupo.Key.DesItem.Trim()
							 });
				
				if (!String.IsNullOrEmpty(codModelo)) {
					query = query.Where(x => x.CodModelo == codModelo);
				}

				if (codLinha != null) {
					query = query.Where(x => x.CodLinha == codLinha);
				}


				return query.ToList();

			}

		}

		public static List<ItensLote> ObterEmbaladosLote(string numCC){

			using (var contexto = new DTB_CBContext())
			{

				try
				{

					var dados = (from emb in contexto.tbl_CBEmbalada
								join item in contexto.tbl_Item on emb.CodModelo equals item.CodItem
								join linha in contexto.tbl_CBLinha on emb.CodLinha equals linha.CodLinha
								join posto in contexto.tbl_CBPosto on emb.NumPosto equals posto.NumPosto
								from amarra in contexto.tbl_CBAmarra.Where(x => x.NumECB == emb.NumECB).DefaultIfEmpty()
								 where emb.NumCC == numCC && posto.CodLinha == emb.CodLinha
								 select new ItensLote { 
									NumLotePai = emb.NumLote,
									NumCC = emb.NumCC,
									NumECB = emb.NumECB,
									CodModelo = emb.CodModelo,
									DatLeitura = emb.DatLeitura,
									DatReferencia = emb.DatReferencia,
									CodFab = emb.CodFab,
									NumAP = emb.NumAP,
									CodOperador = emb.CodOperador,
									CodLinha = emb.CodLinha,
									NumPosto = emb.NumPosto,
									DatAmarraFinal = amarra.DatAmarra,

									DesModelo = item.DesItem.Trim(),
									DesLinha = linha.DesLinha,
									DesPosto = posto.DesPosto
								 });

					return dados.ToList();

				}
				catch
				{
					return new List<ItensLote>();
				}

			}

		}

		#endregion
	}
}
