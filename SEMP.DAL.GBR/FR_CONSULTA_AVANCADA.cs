namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_CONSULTA_AVANCADA
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FRM_CODIGO { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CAV_CODIGO { get; set; }

        [Required]
        [StringLength(60)]
        public string CAV_DESCRICAO { get; set; }

        public string CAV_TEXTO { get; set; }

        public virtual FR_FORMULARIO FR_FORMULARIO { get; set; }
    }
}
