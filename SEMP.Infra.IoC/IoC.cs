﻿using System;
using System.Collections.Generic;
using Ninject;
using Microsoft.Practices.ServiceLocation;
using CommonServiceLocator.NinjectAdapter.Unofficial;

namespace ArquiteturaERP.Infra.IoC
{
    public class IoC
    {
        private static StandardKernel _kernel;

        public static void Init()
        {
            //faz a configuração do container de injeção de dependência
            _kernel = new StandardKernel(new IoCModule());
            ServiceLocator.SetLocatorProvider(() => new NinjectServiceLocator(_kernel));    //adapta uma interface para outra.
        }

        public static IEnumerable<object> GetAll(Type type)
        {
            return _kernel.GetAll(type);
        }

        public static object Get(Type type)
        {
            return _kernel.TryGet(type);
        }

    }
}
