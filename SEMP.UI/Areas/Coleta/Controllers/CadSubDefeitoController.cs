﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.Rastreabilidade;
using SEMP.Model.VO.Rastreabilidade;
using SEMP.Model.DTO;
using SEMP.Model.VO;
using SEMP.Model;

namespace SEMP.Areas.Coleta.Controllers
{
	public class CadSubDefeitoController : Controller
	{
		//
		// GET: /Coleta/CadSubDefeito/

		public ActionResult Index(string flgAtivo)
		{
			var defeitos = Defeito.ObterSubDefeitos();

			if (String.IsNullOrEmpty(flgAtivo))
			{
				defeitos = defeitos.Where(x => x.flgAtivo == "1").ToList();
			}
			else
			{
				if (flgAtivo.Equals("A"))
					defeitos = defeitos.Where(x => x.flgAtivo == "1").ToList();
				else if (flgAtivo.Equals("I"))
					defeitos = defeitos.Where(x => x.flgAtivo == "0").ToList();
			}

			return View(defeitos);
		}

		//
		// GET: /Coleta/CadSubDefeito/Details/5

		public ActionResult Details(int id)
		{
			return View();
		}

		//
		// GET: /Coleta/CadSubDefeito/Create

		public ActionResult Create()
		{
			return View();
		}

		//
		// POST: /Coleta/CadSubDefeito/Create

		[HttpPost]
		public ActionResult Create(FormCollection collection)
		{
			try
			{
				// TODO: Add insert logic here

				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}
		}

		//
		// GET: /Coleta/CadSubDefeito/Edit/5

		/*public ActionResult Edit(int id)
		{
			return View();
		}*/

		//
		// POST: /Coleta/CadSubDefeito/Edit/5

		[HttpPost]
		public ActionResult Edit(CBCadSubDefeitoDTO defeito)
		{
			try
			{
				if (String.IsNullOrEmpty(defeito.flgAtivo))
				{
					defeito.flgAtivo = "0";
				}

				Defeito.GravarSubDefeito(defeito);

				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}
		}

		//
		// GET: /Coleta/CadSubDefeito/Delete/5

		public ActionResult Delete(int id)
		{
			return View();
		}

		//
		// POST: /Coleta/CadSubDefeito/Delete/5

		[HttpPost]
		public ActionResult Delete(int id, FormCollection collection)
		{
			try
			{
				// TODO: Add delete logic here

				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}
		}

		public ActionResult ObterDefeitos()
		{
			var defeitos = Defeito.ObterListaDefeitos();

			defeitos = defeitos.Where(x => x.flgAtivo == "1").ToList();

			return Json(defeitos, JsonRequestBehavior.AllowGet);
		}

		public ActionResult GravarSubDefeito(string CodDefeito, string DesDefeito, string DesVerificacao, bool flgAtivo)
		{
			Boolean resultado = false;

			CBCadSubDefeitoDTO defeitoDTO = new CBCadSubDefeitoDTO();

			defeitoDTO.CodDefeito = CodDefeito;
			defeitoDTO.DesDefeito = DesDefeito;
			defeitoDTO.DesVerificacao = DesVerificacao;
			if (flgAtivo == false)			
				defeitoDTO.flgAtivo = "0";			
			else
				defeitoDTO.flgAtivo = "1";
						
			defeitoDTO.CodSubDefeito = Defeito.ObterIndiceSubDefeito(defeitoDTO.CodDefeito) + 1;

			resultado = Defeito.GravarSubDefeito(defeitoDTO);

			return Json(resultado, JsonRequestBehavior.AllowGet);
		}

		public ActionResult Edit(string CodDefeito, string CodSubDefeito)
		{
			var defeito = Defeito.ObterCadSubDefeito(CodDefeito, CodSubDefeito);

			return View(defeito);
		}
	}
}
