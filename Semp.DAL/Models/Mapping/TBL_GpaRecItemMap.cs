using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class TBL_GpaRecItemMap : EntityTypeConfiguration<TBL_GpaRecItem>
    {
        public TBL_GpaRecItemMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdRegistro, t.CodItem, t.NumSerie });

            // Properties
            this.Property(t => t.IdRegistro)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodLinha)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.CodItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumSerie)
                .IsRequired()
                .HasMaxLength(9)
                .IsUnicode(false);

            this.Property(t => t.Operador)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.Status)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.Situacao)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.NumAP)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.TipoAP)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("TBL_GpaRecItem");
            this.Property(t => t.IdRegistro).HasColumnName("IdRegistro");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.CodPallet).HasColumnName("CodPallet");
            this.Property(t => t.NumSerie).HasColumnName("NumSerie");
            this.Property(t => t.DataLeitura).HasColumnName("DataLeitura");
            this.Property(t => t.DataReferencia).HasColumnName("DataReferencia");
            this.Property(t => t.Operador).HasColumnName("Operador");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.Situacao).HasColumnName("Situacao");
            this.Property(t => t.NumAP).HasColumnName("NumAP");
            this.Property(t => t.TipoAP).HasColumnName("TipoAP");
        }
    }
}
