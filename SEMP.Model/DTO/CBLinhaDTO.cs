using System;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
    [Serializable]
    public class CBLinhaDTO
    {
        public int CodLinha { get; set; }
        public string DesLinha { get; set; }
        public string DesObs { get; set; }
        public string Setor { get; set; }
        public string Familia { get; set; }
		[Display(Name = "Codigo Linha SEMP")]
		[Required(ErrorMessage = "Informe Codigo da Linha SEMP")]
        public string CodLinhaT1 { get; set; }
        public string CodLinhaT2 { get; set; }
        public string CodLinhaT3 { get; set; }
        public string CodPlaca { get; set; }
        public string CodModelo { get; set; }
        public string CodAuxiliar { get; set; }
        public string CodFam { get; set; }
		public string Observacao { get; set; }
        public int CodLinhaOrigem { get; set; }
		public string SetorOrigem { get; set; }
        public string FlgAtivo { get; set; }
		public string FamiliaOrigem { get; set; }

		public int QtdPostos { get; set; }
    }
}
