﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.Apontaprod;
using SEMP.Model.DTO;

namespace RAST.BO.ApontaProd
{
	public class MapaScrapBO
	{
		public static List<MAPScrapBaixaDTO> ObterListagem(DateTime DatInicio, DateTime DatFim)
		{
			var listagem = daoMapaScrap.ObterListagem(DatInicio, DatFim);



			return listagem;
		}

		public static MAPScrapBaixaDTO ObterMapa(int idScrap)
		{
			return daoMapaScrap.ObterMapa(idScrap);

		}

		public static void Alterar(int idScrap, string CodItem, decimal QtdPerda)
		{
			daoMapaScrap.Alterar(idScrap, CodItem, QtdPerda);
		}

		public static void GerarMapa(DateTime DatInicio, DateTime DatFim, string Usuario)
		{
			daoMapaScrap.GerarMapa(DatInicio, DatFim, Usuario);
		}

		public static void BaixarScrap(DateTime DatInicio, DateTime DatFim, string Usuario)
		{
			daoMapaScrap.BaixarScrap(DatInicio, DatFim, Usuario);
		}

		public static bool VerificaData(DateTime data)
		{
			return daoMapaScrap.VerificaData(data);
		}

	}
}
