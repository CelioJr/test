namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_PARAMETRO
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FRM_CODIGO { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int COM_CODIGO { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ACO_CODIGO { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(30)]
        public string ACC_MOMENTO { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(30)]
        public string PAR_NOME { get; set; }

        [StringLength(5000)]
        public string PAR_VALOR { get; set; }

        public virtual FR_ACAOCOMPONENTE FR_ACAOCOMPONENTE { get; set; }
    }
}
