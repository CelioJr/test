﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_RetrabalhoMap : EntityTypeConfiguration<tbl_Retrabalho>
    {
        public tbl_RetrabalhoMap()
        {
            // Primary Key
            this.HasKey(t => t.NS);

            // Properties
            this.Property(t => t.NS)
                .IsRequired()
                .HasMaxLength(18);

            // Table & Column Mappings
            this.ToTable("tbl_Retrabalho");
            this.Property(t => t.NS).HasColumnName("NS");
            this.Property(t => t.CodLote).HasColumnName("CodLote");
            this.Property(t => t.DatLeitura).HasColumnName("DatLeitura");
        }
    }
}