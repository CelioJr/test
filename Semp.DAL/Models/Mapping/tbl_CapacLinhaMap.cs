using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
	public class tbl_CapacLinhaMap : EntityTypeConfiguration<tbl_CapacLinha>
	{
		public tbl_CapacLinhaMap()
		{
			// Primary Key
			this.HasKey(t => new { t.DatProducao, t.CodLinha, t.CodModelo, t.rowguid, t.NumTurno });

			// Properties
			this.Property(t => t.CodLinha)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(8)
                .IsUnicode(false);

			this.Property(t => t.CodModelo)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(6)
                .IsUnicode(false);

			this.Property(t => t.Usuario)
				.IsFixedLength()
				.HasMaxLength(25)
                .IsUnicode(false);

			// Table & Column Mappings
			this.ToTable("tbl_CapacLinha");
			this.Property(t => t.DatProducao).HasColumnName("DatProducao");
			this.Property(t => t.CodLinha).HasColumnName("CodLinha");
			this.Property(t => t.CodModelo).HasColumnName("CodModelo");
			this.Property(t => t.QtdCapacLinha).HasColumnName("QtdCapacLinha");
			this.Property(t => t.QtdCapacMaxima).HasColumnName("QtdCapacMaxima");
			this.Property(t => t.Usuario).HasColumnName("Usuario");
			this.Property(t => t.rowguid).HasColumnName("rowguid");
			this.Property(t => t.NumTurno).HasColumnName("NumTurno");
		}
	}
}
