﻿using System;
using System.Linq;
using System.Web.Mvc;
using RAST.BO.Rastreabilidade;
using SEMP.Model.DTO;

namespace SEMP.Areas.Coleta.Controllers
{
	public class DefeitosController : Controller
	{
		//
		// GET: /Coleta/Defeitos/

		public ActionResult Index(string flgAtivo)
		{
			var defeitos = Defeito.ObterListaDefeitos();

			if (String.IsNullOrEmpty(flgAtivo))
			{
				defeitos = defeitos.Where(x => x.flgAtivo == "1").ToList();
			}
			else
			{
				if (flgAtivo.Equals("A"))
					defeitos = defeitos.Where(x => x.flgAtivo == "1").ToList();
				else if (flgAtivo.Equals("I"))
					defeitos = defeitos.Where(x => x.flgAtivo == "0").ToList();
			}
			
			return View(defeitos);
		}

		//
		// GET: /Coleta/Defeitos/Create

		public ActionResult Create()
		{
			return View();
		}

		//
		// POST: /Coleta/Defeitos/Create

		[HttpPost]
		public ActionResult Create(CBCadDefeitoDTO defeito)
		{
			try
			{
				if (String.IsNullOrEmpty(defeito.flgAtivo))
				{
					defeito.flgAtivo = "0";
				}
				if (defeito.flgFEC == null)
				{
					defeito.flgFEC = 0;
				}
				if (defeito.flgIMC == null)
				{
					defeito.flgIMC = 0;
				}
				if (defeito.flgIAC == null)
				{
					defeito.flgIAC = 0;
				}
                if (defeito.flgLCM == null)
                {
                    defeito.flgLCM = 0;
                }

				var ultimodef = Defeito.ObterUltimoDefeito();
				ultimodef = (int.Parse(ultimodef) + 1).ToString();

				defeito.CodDefeito = ultimodef;

				Defeito.GravarDefeito(defeito);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}

		//
		// GET: /Coleta/Defeitos/Edit/5

		public ActionResult Edit(string codDefeito)
		{
			var defeito = Defeito.ObterCadDefeito(codDefeito);

			return View(defeito);
		}

		//
		// POST: /Coleta/Defeitos/Edit/5

		[HttpPost]
		public ActionResult Edit(CBCadDefeitoDTO defeito)
		{
			try
			{
				if (String.IsNullOrEmpty(defeito.flgAtivo))
				{
					defeito.flgAtivo = "0";
				}
				if (defeito.flgFEC == null)
				{
					defeito.flgFEC = 0;
				}
				if (defeito.flgIMC == null)
				{
					defeito.flgIMC = 0;
				}
				if (defeito.flgIAC == null)
				{
					defeito.flgIAC = 0;
				}
                if (defeito.flgLCM == null)
                {
                    defeito.flgLCM = 0;
                }
				Defeito.GravarDefeito(defeito);

				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}
		}

		//
		// GET: /Coleta/Defeitos/Delete/5

		public ActionResult Delete(string codDefeito)
		{

			var defeito = Defeito.ObterCadDefeito(codDefeito);

			return View(defeito);
		}

		//
		// POST: /Coleta/Defeitos/Delete/5

		[HttpPost]
		public ActionResult Delete(CBCadDefeitoDTO defeito)
		{
			try
			{
				Defeito.RemoverDefeito(defeito);

				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}
		}

		public string ObterDesDefeito(string codDefeito)
		{

			try
			{
				var defeito = Defeito.ObterCadDefeito(codDefeito).DesDefeito;

				return defeito;
			}
			catch
			{
				return "";
			}

		}

		public JsonResult ObterDesDefeitoAtivos()
		{
			return Json(Defeito.ObterDefeitosCadastrados().Where(x => x.flgAtivo == "1").Select(t => new { CodDefeito = t.CodDefeito, DesDefeito = t.DesDefeito }).ToList(), JsonRequestBehavior.AllowGet);
		}
	}
}
