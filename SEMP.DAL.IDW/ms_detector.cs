namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ms_detector
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ms_detector()
        {
            ms_detectorusr = new HashSet<ms_detectorusr>();
            ms_detectorusr1 = new HashSet<ms_detectorusr>();
            ms_trigger = new HashSet<ms_trigger>();
        }

        [Key]
        public long id_detector { get; set; }

        [StringLength(30)]
        public string cd_detector { get; set; }

        public int? revisao { get; set; }

        [StringLength(100)]
        public string ds_detector { get; set; }

        public int? st_ativo { get; set; }

        public DateTime? dthr_revisao { get; set; }

        public DateTime? dthr_stativo { get; set; }

        public long id_usrRevisao { get; set; }

        public long? id_usrstativo { get; set; }

        public long id_usrgrp { get; set; }

        public short? is_email { get; set; }

        public short? is_sms { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_detectorusr> ms_detectorusr { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_detectorusr> ms_detectorusr1 { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        public virtual om_usrgrp om_usrgrp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_trigger> ms_trigger { get; set; }
    }
}
