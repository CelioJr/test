﻿using System;
using System.Web.Mvc;
using RAST.BO.PM;
using SEMP.Model.DTO;

namespace SEMP.Areas.Outros.Controllers
{
	public class ParadaController : Controller
	{
		//
		// GET: /Outros/Parada/
		public ActionResult Index()
		{
			return View();
		}

		//
		// GET: /Outros/Parada/Details/5
		public ActionResult Lista(string datInicio, string datFim, string linha, string codFab, string codModelo, string tipoParada, string status, string codDepto)
		{
			var dIni = DateTime.Parse(datInicio);
			var dFim = DateTime.Parse(datFim);

			var dados = ParadaDeLinha.ObterParadas(dIni, dFim, linha, codFab, codModelo, tipoParada, status,
				codDepto);

			return View(dados);
		}

		//
		// GET: /Outros/Parada/Create
		public ActionResult Create()
		{
			return View();
		}

		//
		// POST: /Outros/Parada/Create
		[HttpPost]
		public ActionResult Create(ProdParadaDTO dados)
		{
			try
			{
				var parada = ParadaDeLinha.GravarParada(dados);

				if (String.IsNullOrEmpty(parada)){
					return Json(false, JsonRequestBehavior.AllowGet);
				}

				return Json(parada, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}

		public string ObterDescricaoMotivo(string codParada, string codFab)
		{
			var motivo = ParadaDeLinha.ObterMotivo(codParada, codFab);

			return motivo.DesParada.Trim();
		}

		public JsonResult ObterMotivo(string codParada, string codFab)
		{
			var motivo = ParadaDeLinha.ObterMotivo(codParada, codFab);

			return Json(motivo, JsonRequestBehavior.AllowGet); 
		}

		public string ObterDesGrupo(string codFab, string codGrupo)
		{
			var dado = ParadaDeLinha.ObterDesGrupo(codFab, codGrupo);

			return dado;
		}

		public string VerificaUsuariosCadastrados(string codFab, string numParada)
		{
			try
			{
				var dados = ParadaDeLinha.ObterListaAmplitudeParada(codFab, numParada);
				if (dados != null)
				{
					return "true";
				}
				else
				{
					return "false|Não existe usuários cadastrados para envio de e-mail automático";
				}
			}
			catch (Exception ex)
			{
				return "false|" + ex.Message;
			}
		}

		//
		// GET: /Outros/Parada/Edit/5
		public ActionResult Edit(string codFab, string numParada)
		{
			var dado = ParadaDeLinha.ObterParada(codFab, numParada);

			return View(dado);
		}

		//
		// POST: /Outros/Parada/Edit/5
		[HttpPost]
		public ActionResult Edit(ProdParadaDTO dado)
		{
			try
			{
				var parada = ParadaDeLinha.GravarParada(dado);

				return Json(parada, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json(ex.Message, JsonRequestBehavior.AllowGet);
			}
		}

		//
		// GET: /Outros/Parada/Delete/5
		public ActionResult Delete(string codFab, string numParada)
		{
			var dado = ParadaDeLinha.ObterParada(codFab, numParada);

			return View(dado);
		}

		//
		// POST: /Outros/Parada/Delete/5
		[HttpPost]
		public ActionResult Delete(ProdParadaDTO dado)
		{
			try
			{
				ParadaDeLinha.RemoverParada(dado.CodFab, dado.NumParada);

				return RedirectToAction("Index");
			}
			catch
			{
				return RedirectToAction("Index");
			}
		}

		public ActionResult Responder(string codFab, string numParada, string codParada, string desParada)
		{
			var retornologin = (SEMP.Model.VO.RetornoLogin)Session["RetornoLogin"];

			if (retornologin != null)
			{
				var depto = "";
				depto = retornologin.Depto.Trim();
				if(depto == "CPD") { depto = "DTI"; }

				var dado = ParadaDeLinha.ObterParadaDepItem(codFab, numParada, depto);

				if (dado != null)
				{
					ViewBag.codParada = codParada;
					ViewBag.desParada = desParada;
					return View(dado);
				}
				else
				{
					ViewBag.Mensagem = "Seu departamento não foi acionado para essa parada de linha";
					return View("DeptoInvalido");
				}
			}
			else
			{
				ViewBag.Mensagem = "Departamento inválido, faça login novamente";
				return View("DeptoInvalido");
			}
		}

		[HttpPost]
		public string Responder(ProdParadaDepItemDTO item)
		{

			try
			{
				item.DatRetorno = DateTime.Parse(DateTime.Now.ToShortDateString());

				ParadaDeLinha.GravarParadaDepItem(item);

				return "Registro salvo com sucesso";
			}
			catch (Exception ex)
			{
				return ex.Message;
			}

		}

		[HttpPost]
		public string ResponderEmail(ProdParadaDepItemDTO item)
		{
			var retornologin = (SEMP.Model.VO.RetornoLogin)Session["RetornoLogin"];

			if (retornologin != null)
			{
				var depto = "";
				depto = retornologin.Depto;

				item.DatEnvio = DateTime.Parse(DateTime.Now.ToShortDateString());

				return "";
			}
			else
			{
				return "Departamento inválido, faça login novamente";
			}
		}

		[HttpPost]
		public string EnviarEmailDepto(string codFab, string numParada)
		{

			try
			{
				var resultado = ParadaDeLinha.EnviarEmailDepto(codFab, numParada);

                if (resultado != "") {
                    return resultado;
                }
			}
			catch (Exception ex)
			{

				return ex.Message;
			}

			return "E-mail enviado com sucesso";
		}

		[HttpPost]
		public string EnviarEmailResposta(string codFab, string numParada, string codDepto)
		{

			try
			{
				return ParadaDeLinha.EnviarEmailResposta(codFab, numParada, codDepto);
			}
			catch (Exception ex)
			{

				return "False|" + ex.Message;
			}

		}

		#region Relatórios

		public ActionResult RelatorioSumarizado(string codFab, string datInicio, string datFim, string codDepto, string codFabDepto)
		{

			var dIni = DateTime.Parse(datInicio);
			var dFim = DateTime.Parse(datFim);

			var dados = ParadaDeLinha.ObterParadas(dIni, dFim, "", codFab, "", "", "", codDepto);

			/*var relatorio = new List<RelDeptos>();
			relatorio = from d in dados
						group d by new {d.CodDep}*/

			return View(dados);
		}

		#endregion
	}

}
