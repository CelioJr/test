﻿using System;
using System.Collections.Generic;
using System.Linq;
using SEMP.DAL.Models;
using SEMP.Model.DTO;

namespace SEMP.DAL.DAO.Rastreabilidade
{
    public class daoTransferencia
	{
        //static daoTransferencia()
        //{
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<tbl_CBLote, CBLoteDTO>();
        //        cfg.CreateMap<tbl_CBTransfLote, CBTransfLoteDTO>();
        //        cfg.CreateMap<tbl_CBEmbalada, CBEmbaladaDTO>();
        //        cfg.CreateMap<CBLoteDTO, tbl_CBLote>();
        //        cfg.CreateMap<CBTransfLoteDTO, tbl_CBTransfLote>();
        //    });
        //}

        #region Verificações

        public static Boolean VerificaLoteFechado(string NumLote)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var lote = dtb_CB.tbl_CBLote.Where(x => x.NumLote == NumLote).FirstOrDefault();

				if(lote == null)
					return false;

				if (lote.DatFechamento == null && lote.DatRecebimento == null)
					return false;
				else
					return true;
			}
		}

		public static Boolean VerificaTransferencia(CBTransfLoteDTO transfLote)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var transferencia = dtb_CB.tbl_CBTransfLote.Where(x => x.CodLinhaOri == transfLote.CodLinhaOri && x.NumPostoOri == transfLote.NumPostoOri
									&& x.NumLote == transfLote.NumLote).FirstOrDefault();

				if (transferencia == null)				
					return false;
				else
					return true;
			}
		}

		#endregion

		#region Consultas

		public static CBLoteDTO ObterLote(string numLote)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var lote = (from p in dtb_CB.tbl_CBLote
							  where p.NumLote == numLote
							  select p);

				var p2 = lote.FirstOrDefault();

				CBLoteDTO cBLoteDTO = new CBLoteDTO();

				AutoMapper.Mapper.Map(p2, cBLoteDTO);

				return cBLoteDTO;
			}
		}

		public static CBTransfLoteDTO ObterLoteTransf(string numLote)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var lote = (from p in dtb_CB.tbl_CBTransfLote
							where p.NumLote == numLote
							select p);

				var p2 = lote.FirstOrDefault();

				CBTransfLoteDTO cBTrasnfLoteDTO = new CBTransfLoteDTO();

				AutoMapper.Mapper.Map(p2, cBTrasnfLoteDTO);

				return cBTrasnfLoteDTO;
			}
		}

		public static CBEmbaladaDTO ObterEmbalada(string NumECB)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var embalada = (from p in dtb_CB.tbl_CBEmbalada
							where p.NumECB == NumECB
							select p);

				var p2 = embalada.ToList();

				CBEmbaladaDTO cBEmbaladaDTO = new CBEmbaladaDTO();

				AutoMapper.Mapper.Map(p2, cBEmbaladaDTO);

				return cBEmbaladaDTO;
			}
		}

		public static List<CBTransfLoteDTO> ObterTransferenciaAberta(int CodLinha, int NumPosto)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var lotes = (from p in dtb_CB.tbl_CBTransfLote
								where p.CodLinhaOri == CodLinha && p.NumPostoOri == NumPosto && p.DatTransf == null
								select p);

				var p2 = lotes.ToList();

				List<CBTransfLoteDTO> cBTransfLoteDTO = new List<CBTransfLoteDTO>();

				AutoMapper.Mapper.Map(p2, cBTransfLoteDTO);

				return cBTransfLoteDTO;
			}
		}

		public static string ObterNumeroLote(string NumECB)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				string NumLote = (from p in dtb_CB.tbl_CBEmbalada
								where p.NumECB == NumECB
								select p.NumLote).FirstOrDefault();

				
				return NumLote;
			}
		}

		public static List<CBLoteDTO> ObterLoteDisponivel(string CodModelo)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var NumLote = (from p in dtb_CB.tbl_CBLote								
								where p.DatTransferencia == null && p.DatFechamento != null && p.CodModelo == CodModelo && p.Status == 1
									&& p.DatRecebimento == null && p.NumLotePai == null
										select p);

				var p2 = NumLote.ToList();

				List<CBLoteDTO> cBLoteDTO = new List<CBLoteDTO>();

				AutoMapper.Mapper.Map(p2, cBLoteDTO);

				return cBLoteDTO;
			}

		}

		public static String ObterModeloTransferencia(int CodLinha, int NumPosto)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var CodModelo = (from p in dtb_CB.tbl_CBTransfLote
							   where p.CodLinhaOri == CodLinha && p.NumPostoOri == NumPosto && p.DatTransf == null && p.DatRecebimento == null
							   select p.NumLote.Substring(7, 6)).FirstOrDefault();
				

				return CodModelo;
			}
		}

		public static List<String> ObterModeloDisponivel(string fabrica)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var codModelo = (from p in dtb_CB.tbl_CBLote
											where p.DatFechamento != null && p.DatRecebimento == null && p.NumLotePai == null && p.NumLotePai.Substring(0, 1) == fabrica
										select p.CodModelo);
				
				List<String> lista = codModelo.ToList();

				return lista;
			}
		}		

		public static String ObterNumeroLoteCC(string NumCC)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				string NumLote = (from p in dtb_CB.tbl_CBLote
								 where p.NumLote == NumCC
								 select p.NumLotePai).FirstOrDefault();
				
				return NumLote;
			}
		}

		public static int ContadorCaixaColetiva(string NumLote)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				int contador = (from p in dtb_CB.tbl_CBLote
								where p.NumLotePai == NumLote
								  select p).Count();

				return contador;
			}
		}

		public static int ObterLinhaDestino(int CodLinha, int NumPosto)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				int linhaDestino = (from p in dtb_CB.tbl_CBTransfLote
								where p.CodLinhaOri == CodLinha && p.NumPostoOri == NumPosto
								select p.CodLinhaDest).FirstOrDefault();

				return linhaDestino;
			}
		}		

		#endregion

		#region Gravações

		public static Boolean GravaDatRecebimento(CBLoteDTO loteDTO)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				try
				{
					if(loteDTO.DatRecebimento.HasValue)
						return true;

					loteDTO.DatRecebimento = Common.daoUtil.GetDate();					
					tbl_CBLote lote = dtb_CB.tbl_CBLote.Where(a => a.NumLote == loteDTO.NumLote).FirstOrDefault();
					AutoMapper.Mapper.Map(loteDTO, lote);
					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		public static Boolean GravarTransLote(CBTransfLoteDTO transfLoteDTO)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				try
				{
					tbl_CBTransfLote transfLote = new tbl_CBTransfLote();					
					AutoMapper.Mapper.Map(transfLoteDTO, transfLote);
					dtb_CB.tbl_CBTransfLote.Add(transfLote);
					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		public static Boolean DesfazTransferencia(int CodLinha, int NumPosto)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				try
				{
					List<tbl_CBTransfLote> transfLote = (from p in dtb_CB.tbl_CBTransfLote
														where p.CodLinhaOri == CodLinha && p.NumPostoOri == NumPosto && p.DatTransf == null
														select p).ToList();					

					foreach(tbl_CBTransfLote item in transfLote)
					{
						tbl_CBLote lote = (from x in dtb_CB.tbl_CBLote
											where x.NumLote == item.NumLote
											select x).FirstOrDefault();
						lote.DatRecebimento = null;						
					}
					

					dtb_CB.tbl_CBTransfLote.RemoveRange(transfLote);
					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		public static Boolean Trasnferir(List<CBTransfLoteDTO> transfLoteDTO)
		{
			DateTime datTransf = Common.daoUtil.GetDate();

			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				try
				{					

					foreach (CBTransfLoteDTO item in transfLoteDTO)
					{
						var transfLote = (from x in dtb_CB.tbl_CBTransfLote
										where x.NumLote == item.NumLote
										select x).FirstOrDefault();

						transfLote.DatTransf = datTransf;

						var lote = (from p in dtb_CB.tbl_CBLote
									where p.NumLote == item.NumLote || p.NumLotePai == item.NumLote
									select p).FirstOrDefault();

						lote.DatTransferencia = datTransf;
						lote.Status = SEMP.Model.Constantes.LOTE_TRANSFERIDO;
					}										

					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}

		}

		/// <summary>
		/// Chamada da Procedure spc_MovimentaINS no dtb_SIM para realizar a movimentação de material no estoque durante a transferência
		/// de placas ou produtos.
		/// </summary>
		/// <param name="CodTra"></param>
		/// <param name="DatTra"></param>
		/// <param name="CodItem"></param>
		/// <param name="DocInterno1"></param>
		/// <param name="DocInterno2"></param>
		/// <param name="AgeExterno"></param>
		/// <param name="QtdTra"></param>
		/// <param name="DocExterno"></param>
		/// <param name="Empresa"></param>
		/// <param name="PrecoUnitario"></param>
		/// <param name="UnidPreco"></param>
		/// <param name="LocFixoCr"></param>
		/// <param name="LocFixoDb"></param>
		/// <param name="EmpDebito"></param>
		/// <param name="flgNegativo"></param>
		/// <param name="EndCr"></param>
		/// <param name="EndDb"></param>
		/// <param name="NomUsuario"></param>
		/// <returns></returns>
		public static Boolean MovimentaINS(string CodTra, DateTime DatTra, string CodItem, string DocInterno1, string DocInterno2, string AgeExterno,
								int QtdTra, string DocExterno, string Empresa, int PrecoUnitario, string UnidPreco, string LocFixoCr,
								string LocFixoDb, string EmpDebito, string flgNegativo, string EndCr, 
								string EndDb, string NomUsuario)
		{
			
			return true;
		}

		#endregion

	}
}
