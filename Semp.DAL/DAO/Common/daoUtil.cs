﻿using System;
using System.Linq;
using SEMP.DAL.Models;
using SEMP.Model.DTO;
using System.Collections.Generic;

namespace SEMP.DAL.Common
{
	public class daoUtil
	{

		public static DateTime GetDate()
		{

			try
			{
				using (DTB_SIMContext dtbSim = new DTB_SIMContext())
				{
					dtbSim.Database.Connection.ConnectionString=dtbSim.Database.Connection.ConnectionString;
					DateTime dbServerTime;
					dbServerTime = dtbSim.Database.SqlQuery<DateTime>("SELECT GETDATE() ").AsEnumerable().FirstOrDefault();

					return dbServerTime;
				}
			}
			catch (Exception)
			{
				return DateTime.Now;
			}

		}

		public static List<CBAcessoDTO> ObterAcessos(string Acesso, string Depto, string User) {

			using (var contexto = new DTB_CBContext())
			{

				var dados = contexto.tbl_CBAcesso.AsQueryable();

				if (!String.IsNullOrEmpty(Acesso)) {
					dados = dados.Where(x => x.CodAcesso == Acesso);
				}
				
				if (!String.IsNullOrEmpty(Depto)) {
					dados = dados.Where(x => x.AcessoDepto.Contains(Depto));
				}

				if (!String.IsNullOrEmpty(User))
				{
					dados = dados.Where(x => x.AcessoUser.Contains(User));
				}

				return dados.Select(x => new CBAcessoDTO() { CodAcesso = x.CodAcesso, AcessoDepto = x.AcessoDepto, AcessoUser = x.AcessoUser }).ToList();

			}

		}

		public static List<CBAcessoDTO> ObterAcessos(string Acesso)
		{

			using (var contexto = new DTB_CBContext())
			{

				var dados = contexto.tbl_CBAcesso.AsQueryable();

				if (!String.IsNullOrEmpty(Acesso))
				{
					dados = dados.Where(x => x.CodAcesso == Acesso);
				}

				return dados.Select(x => new CBAcessoDTO() { CodAcesso = x.CodAcesso, AcessoDepto = x.AcessoDepto ?? "", AcessoUser = x.AcessoUser ?? "" }).ToList();

			}

		}

	}
}
