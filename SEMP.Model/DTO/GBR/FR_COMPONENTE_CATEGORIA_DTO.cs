using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_COMPONENTE_CATEGORIA_DTO
    {
        public int FRM_CODIGO { get; set; }
        public int COM_CODIGO { get; set; }
        public int CAT_CODIGO { get; set; }
        public virtual FR_CATEGORIA_DTO FR_CATEGORIA { get; set; }
    }
}
