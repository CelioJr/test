using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_AcessoMaoMap : EntityTypeConfiguration<tbl_AcessoMao>
    {
        public tbl_AcessoMaoMap()
        {
            // Primary Key
            this.HasKey(t => t.Codigo);

            // Properties
            this.Property(t => t.Codigo)
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.Acesso)
                .HasMaxLength(8000)
                .IsUnicode(false);

			this.Property(t => t.CodigoPai)
				.HasMaxLength(20)
                .IsUnicode(false);

			this.Property(t => t.Sistema)
				.HasMaxLength(15)
				.IsUnicode(false);

			// Table & Column Mappings
			this.ToTable("tbl_AcessoMao");
            this.Property(t => t.Codigo).HasColumnName("Codigo");
            this.Property(t => t.Acesso).HasColumnName("Acesso");
            this.Property(t => t.CodigoPai).HasColumnName("CodigoPai");
			this.Property(t => t.Sistema).HasColumnName("Sistema");
			this.Property(t => t.rowguid).HasColumnName("rowguid");
		}
    }
}
