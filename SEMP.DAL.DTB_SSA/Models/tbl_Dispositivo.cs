using System;
using System.Collections.Generic;

namespace SEMP.DAL.DTB_SSA.Models
{
    public partial class tbl_Dispositivo
    {
        public tbl_Dispositivo()
        {
        }

        public int dispositivo_id { get; set; }
        public string dispositivo { get; set; }
        public string flgTipoProduto { get; set; }
        public string flgExigeNE { get; set; }
        public int? codAgrupamento { get; set; }
        public string flgRastreavel { get; set; }
        public string flgFrequencia { get; set; }
        public string flgOpcional { get; set; }
        public string flgMontagem { get; set; }
        public string flgAtivo { get; set; }
        public string flgReutilizavel { get; set; }
        public string flgTipoConsumo { get; set; }
        public string flgAssociacaoControlada { get; set; }
        public string flgGEDB { get; set; }
        public string flgColetaSerial { get; set; }
    }
}
