namespace SEMP.Model.DTO
{
	using System.ComponentModel.DataAnnotations;

	public partial class tbl_tipo_DTO
	{

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string NOME { get; set; }
    }
}
