using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_ProdReferenciaMap : EntityTypeConfiguration<tbl_ProdReferencia>
    {
        public tbl_ProdReferenciaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.MesReferencia, t.DatReferencia });

            // Properties
            // Table & Column Mappings
            this.ToTable("tbl_ProdReferencia");
            this.Property(t => t.MesReferencia).HasColumnName("MesReferencia");
            this.Property(t => t.DatReferencia).HasColumnName("DatReferencia");
        }
    }
}
