namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_estlocal
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_estlocal()
        {
            dw_estlocalpro = new HashSet<dw_estlocalpro>();
            om_cfg = new HashSet<om_cfg>();
        }

        [Key]
        public long id_estlocal { get; set; }

        [StringLength(60)]
        public string cd_local { get; set; }

        public int? revisao { get; set; }

        [StringLength(256)]
        public string ds_local { get; set; }

        public long? id_est { get; set; }

        public short? st_ativo { get; set; }

        public DateTime? dt_revisao { get; set; }

        public DateTime? dt_stativo { get; set; }

        public long id_usrrevisao { get; set; }

        public long id_usrstativo { get; set; }

        public short? is_automatico { get; set; }

        public short? tp_localestoque { get; set; }

        public long? id_gt { get; set; }

        public long? id_pt { get; set; }

        public long? id_pa { get; set; }

        public virtual dw_est dw_est { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_estlocalpro> dw_estlocalpro { get; set; }

        public virtual om_gt om_gt { get; set; }

        public virtual om_pa om_pa { get; set; }

        public virtual om_pt om_pt { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg { get; set; }
    }
}
