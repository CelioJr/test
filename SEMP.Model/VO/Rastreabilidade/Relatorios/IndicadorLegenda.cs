﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
	public class IndicadorLegenda
	{
		public string Codigo { get; set; }
		public string Descricao { get; set; }
		public int Quantidade { get; set; }
		public Color Cor { get; set; }
	}
}
