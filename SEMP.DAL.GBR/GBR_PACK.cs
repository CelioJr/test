namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_PACK
    {
        [Key]
        public int COD_PACK { get; set; }

        public int? COD_SKU { get; set; }

        public DateTime? DATA { get; set; }
    }
}
