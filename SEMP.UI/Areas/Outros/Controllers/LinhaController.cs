﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.PM;
using SEMP.Model.DTO;

namespace SEMP.Areas.Outros.Controllers
{
	public class LinhaController : Controller
	{
		//
		// GET: /Outros/Linha/
		public ActionResult Index()
		{
			return View();
		}

		//
		// GET: /Outros/Linha/Details/5
		public ActionResult Lista(string flgAtivo, string flgTipo)
		{
			var dados = CadLinha.ObterLinhas();

			if (flgAtivo != null && flgAtivo.Equals("S"))
			{
				dados = dados.Where(x => x.FlgAtivo == "S").ToList();
			}
			else if (flgAtivo != null && flgAtivo.Equals("N"))
			{
				dados = dados.Where(x => x.FlgAtivo == "N").ToList();
			}

			if (!String.IsNullOrEmpty(flgTipo) && flgTipo.Equals("P"))
			{
				dados = dados.Where(x => x.CodLinha.EndsWith("0")).ToList();
			}
			else if (!String.IsNullOrEmpty(flgTipo) && flgTipo.Equals("Q"))
			{
				dados = dados.Where(x => !x.CodLinha.EndsWith("0")).ToList();
			}

			return View(dados);
		}

		//
		// GET: /Outros/Linha/Create
		public ActionResult Create()
		{
			return View();
		}

		//
		// POST: /Outros/Linha/Create
		[HttpPost]
		public ActionResult Create(LinhaDTO linha)
		{
			try
			{
				CadLinha.GravarLinha(linha);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}

		//
		// GET: /Outros/Linha/Edit/5
		public ActionResult Edit(string codLinha)
		{
			var dado = CadLinha.ObterLinha(codLinha);

			return View(dado);
		}

		//
		// POST: /Outros/Linha/Edit/5
		[HttpPost]
		public ActionResult Edit(LinhaDTO linha)
		{
			try
			{
				CadLinha.GravarLinha(linha);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}

		//
		// GET: /Outros/Linha/Delete/5
		public ActionResult Delete(string codLinha)
		{
			var dado = CadLinha.ObterLinha(codLinha);

			return View(dado);
		}

		//
		// POST: /Outros/Linha/Delete/5
		[HttpPost]
		public ActionResult Delete(LinhaDTO linha)
		{
			try
			{
				CadLinha.RemoverLinha(linha.CodLinha);

				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}
		}
	}
}
