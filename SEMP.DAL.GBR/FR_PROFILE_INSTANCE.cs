namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_PROFILE_INSTANCE
    {
        [Key]
        [StringLength(38)]
        public string PRI_CODIGO { get; set; }

        [Required]
        [StringLength(128)]
        public string PRI_DESCRIPTION { get; set; }

        public DateTime PRI_DATE { get; set; }

        public int USR_CODIGO { get; set; }

        [Required]
        [StringLength(3)]
        public string SIS_CODIGO { get; set; }
    }
}
