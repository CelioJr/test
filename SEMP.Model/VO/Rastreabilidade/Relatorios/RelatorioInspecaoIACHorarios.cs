﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
    public class RelatorioInspecaoIACHorarios
    {
        public TimeSpan HoraInicio { get; set; }
        public TimeSpan HoraFim { get; set; }
        public string Hora { get; set; }
        public string Posicao { get; set; }
        public string CodDefeito { get; set; }
        public string DesDefeito { get; set; }
        public string Maquina { get; set; }
        public int Quantidade { get; set; }

        public int CodLinha { get; set; }
        public int NumPosto { get; set; }
    }
}
