using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_ProdCinMap : EntityTypeConfiguration<tbl_ProdCin>
    {
        public tbl_ProdCinMap()
        {
            // Primary Key
            this.HasKey(t => new { t.DatBase, t.CodCinescopio, t.rowguid });

            // Properties
            this.Property(t => t.CodCinescopio)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_ProdCin");
            this.Property(t => t.DatBase).HasColumnName("DatBase");
            this.Property(t => t.CodCinescopio).HasColumnName("CodCinescopio");
            this.Property(t => t.QtdPlanejada).HasColumnName("QtdPlanejada");
            this.Property(t => t.QtdProduzida).HasColumnName("QtdProduzida");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
        }
    }
}
