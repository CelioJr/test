﻿using RAST.BO.Rastreabilidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEMP.UIColeta.Controllers
{
    public class PostoController : Controller
    {
        // GET: Posto
        public ActionResult Index()
        {
			var sessao = Session;

			if (sessao == null || sessao["CodLinha"] == null) {
				return RedirectToAction("Index", "Home");
			}

			int codLinha = (int)sessao["CodLinha"];
			int numPosto = (int)sessao["NumPosto"];

			ViewBag.linha = Geral.ObterLinha(codLinha);
			ViewBag.posto = Geral.ObterPosto(codLinha, numPosto);

            return View();
        }



		public ActionResult Passagem()
		{
			return View();

		}
    }
}