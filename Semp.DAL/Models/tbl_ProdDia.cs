using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_ProdDia
    {
        public string CodFab { get; set; }
        public string CodLinha { get; set; }
        public string CodModelo { get; set; }
        public System.DateTime DatProducao { get; set; }
        public string CodCin { get; set; }
        public Nullable<int> QtdProduzida { get; set; }
        public System.Guid rowguid { get; set; }
        public Nullable<int> QtdApontada { get; set; }
        public Nullable<int> QtdDivida { get; set; }
        public string FlagTP { get; set; }
        public int NumTurno { get; set; }
    }
}
