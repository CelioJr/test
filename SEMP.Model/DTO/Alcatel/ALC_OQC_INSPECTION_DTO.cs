using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_OQC_INSPECTION_DTO
    {
        public int id_inspection { get; set; }
        public string box { get; set; }
        public Nullable<System.DateTime> creation_date { get; set; }
        public Nullable<System.DateTime> finTime { get; set; }
        public string observation { get; set; }
        public string sku { get; set; }
        public string defect { get; set; }
        public int line { get; set; }
        public Nullable<int> oqc_operator { get; set; }
        public int product { get; set; }
        public Nullable<int> target { get; set; }
        public string status { get; set; }
        public string family { get; set; }
        public string lot { get; set; }
        public Nullable<bool> engine { get; set; }
        public virtual ALC_DEFECT_DTO ALC_DEFECT { get; set; }
        public virtual ALC_OQC_TARGET_DTO ALC_OQC_TARGET { get; set; }
        public virtual ALC_SYS_USER_DTO ALC_SYS_USER { get; set; }
        public virtual ALC_VOLUME_DTO ALC_VOLUME { get; set; }
    }
}
