﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.PM
{
    public class ResumoHorasParadas
    {
        public string CodFab { get; set; }
        public string CodFabDepto { get; set; }
        public string CodDepto { get; set; }
        public decimal QtdTV { get; set; }
        public decimal HorTV { get; set; }
        public decimal QtdAudio { get; set; }
        public decimal HorAudio { get; set; }
        public decimal QtdVCR { get; set; }
        public decimal HorVCR { get; set; }
        public decimal QtdFone { get; set; }
        public decimal HorFone { get; set; }
        public decimal QtdDVD { get; set; }
        public decimal HorDVD { get; set; }
        public decimal QtdIAC { get; set; }
        public decimal HorIAC { get; set; }
        public decimal QtdIMC { get; set; }
        public decimal HorIMC { get; set; }
        public decimal QtdMSC { get; set; }
        public decimal HorMSC { get; set; }
        public decimal QtdOutros { get; set; }
        public decimal HorOutros { get; set; }
        public decimal QtdTotal { get; set; }
        public decimal HorTotal { get; set; }
        public string NomDepto { get; set; }						
    }
}
