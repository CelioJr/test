namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolre
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_consolre()
        {
            dw_consolremo = new HashSet<dw_consolremo>();
        }

        [Key]
        public long id_consolre { get; set; }

        public long id_consol { get; set; }

        public long? pcs_auto_producaorefugada { get; set; }

        public long? pcs_manu_producaorefugada { get; set; }

        public long id_trefugo { get; set; }

        public virtual dw_consol dw_consol { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolremo> dw_consolremo { get; set; }

        public virtual dw_t_refugo dw_t_refugo { get; set; }
    }
}
