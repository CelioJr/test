namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolpt
    {
        [Key]
        public long id_consolpt { get; set; }

        public long id_pt { get; set; }

        public long? id_cp { get; set; }

        public long? id_folha { get; set; }

        public long? id_cal { get; set; }

        public DateTime? dthr_inicio { get; set; }

        public DateTime? dthr_fim { get; set; }

        public long? id_consolid_turno { get; set; }

        public long? id_consolid_hora { get; set; }

        public long? id_consolid_mes { get; set; }

        public long? id_consolid_acu { get; set; }

        public long? id_rtcic { get; set; }

        public long? id_consolpalog { get; set; }

        public virtual dw_cal dw_cal { get; set; }

        public virtual dw_consolid dw_consolid { get; set; }

        public virtual dw_consolid dw_consolid1 { get; set; }

        public virtual dw_consolid dw_consolid2 { get; set; }

        public virtual dw_consolid dw_consolid3 { get; set; }

        public virtual dw_consolpalog dw_consolpalog { get; set; }

        public virtual pp_cp pp_cp { get; set; }

        public virtual dw_folha dw_folha { get; set; }

        public virtual om_pt om_pt { get; set; }

        public virtual dw_rtcic dw_rtcic { get; set; }
    }
}
