﻿using System;
using System.Collections.Generic;

namespace SEMP.Model.VO.SAP
{
	public class BAPI_PRODORD_GET_DETAIL
	{
		public BAPIRET2 RETURN { get; set; }
		public String COLLECTIVE_ORDER { get; set; }
		public string NUMBER { get; set; }
		public BAPI_PP_ORDER_OBJECTS ORDER_OBJECTS = new BAPI_PP_ORDER_OBJECTS();

		public List<BAPI_ORDER_COMPONENT> COMPONENT = new List<BAPI_ORDER_COMPONENT>();
		public List<BAPI_ORDER_HEADER1> HEADER = new List<BAPI_ORDER_HEADER1>();
		public List<BAPI_ORDER_OPERATION1> OPERATION = new List<BAPI_ORDER_OPERATION1>();
		public List<BAPI_ORDER_ITEM> POSITION = new List<BAPI_ORDER_ITEM>();

	}

	public struct BAPIRET2
	{
		public String TYPE;
		public String ID;
		public int NUMBER;
		public String MESSAGE;
		public String LOG_NO;
		public int LOG_MSG_NO;
		public String MESSAGE_V1;
		public String MESSAGE_V2;
		public String MESSAGE_V3;
		public String MESSAGE_V4;
		public String PARAMETER;
		public int ROW;
		public String FIELD;
		public String SYSTEM;
	}

	public struct BAPI_PP_ORDER_OBJECTS
	{
		public String HEADER;
		public String POSITIONS;
		public String SEQUENCES;
		public String OPERATIONS;
		public String COMPONENTS;
		public String PROD_REL_TOOLS;
		public String TRIGGER_POINTS;
		public String SUBOPERATIONS;
	}

}
