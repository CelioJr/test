namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_EVENTOS
    {
        [Key]
        public int COD_EVENTO { get; set; }

        [StringLength(500)]
        public string EVENTO { get; set; }

        public DateTime? DATA { get; set; }

        [StringLength(30)]
        public string USUARIO { get; set; }

        [StringLength(30)]
        public string OPERACAO { get; set; }

        [StringLength(30)]
        public string DADOS_ANTERIORES { get; set; }

        [StringLength(30)]
        public string DADOS_ATUAIS { get; set; }

        [StringLength(15)]
        public string PACKSTRING { get; set; }

        [StringLength(15)]
        public string PALLET { get; set; }

        [StringLength(30)]
        public string SERIAL { get; set; }

        [StringLength(25)]
        public string IMEI { get; set; }

        public int? TIPO { get; set; }
    }
}
