﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
	public class tbl_CBResumoIACMap : EntityTypeConfiguration<tbl_CBResumoIAC>
	{
		public tbl_CBResumoIACMap()
		{
			// Primary Key
			this.HasKey(t => new { t.Data, t.Turno, t.Linha, t.CodModelo, t.FlagTP });

			// Properties
			this.Property(t => t.Turno)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.Linha)
				.IsRequired()
				.HasMaxLength(15);

			this.Property(t => t.CodModelo)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(6);

			this.Property(t => t.DesModelo)
				.HasMaxLength(50);

			this.Property(t => t.OcorrenciaParada)
				.HasMaxLength(1000);

			this.Property(t => t.OcorrenciaFaltaApont)
				.HasMaxLength(1000);

			this.Property(t => t.CM_1_PICKUP)
				.HasMaxLength(100);

			this.Property(t => t.CM_1_MOUNT)
				.HasMaxLength(100);

			this.Property(t => t.CM_2_PICKUP)
				.HasMaxLength(100);

			this.Property(t => t.CM_2_MOUNT)
				.HasMaxLength(100);

			this.Property(t => t.EficienciaRealizacao)
				.HasMaxLength(100);

			this.Property(t => t.Produtividade)
				.HasMaxLength(100);

			this.Property(t => t.TempoPadrao).HasPrecision(18, 2);

			this.Property(t => t.FlagTP)
				.HasMaxLength(1);

			// Table & Column Mappings
			this.ToTable("tbl_CBResumoIAC");
			this.Property(t => t.Data).HasColumnName("Data");
			this.Property(t => t.Turno).HasColumnName("Turno");
			this.Property(t => t.Linha).HasColumnName("Linha");
			this.Property(t => t.CodModelo).HasColumnName("CodModelo");
			this.Property(t => t.DesModelo).HasColumnName("DesModelo");
			this.Property(t => t.QtdPlano).HasColumnName("QtdPlano");
			this.Property(t => t.QtdReal).HasColumnName("QtdReal");
			this.Property(t => t.QtdDefeitos).HasColumnName("QtdDefeitos");
			this.Property(t => t.OcorrenciaParada).HasColumnName("OcorrenciaParada");
			this.Property(t => t.Apontado).HasColumnName("Apontado");
			this.Property(t => t.ApontEstoque).HasColumnName("ApontEstoque");
			this.Property(t => t.OcorrenciaFaltaApont).HasColumnName("OcorrenciaFaltaApont");
			this.Property(t => t.QtdPontos).HasColumnName("QtdPontos");
			this.Property(t => t.TempoPadrao).HasColumnName("TempoPadrao");
			this.Property(t => t.GargaloReal).HasColumnName("GargaloReal");
			this.Property(t => t.CM_1_PICKUP).HasColumnName("CM_1_PICKUP");
			this.Property(t => t.CM_1_MOUNT).HasColumnName("CM_1_MOUNT");
			this.Property(t => t.CM_2_PICKUP).HasColumnName("CM_2_PICKUP");
			this.Property(t => t.CM_2_MOUNT).HasColumnName("CM_2_MOUNT");
			this.Property(t => t.TempoDisp).HasColumnName("TempoDisp");
			this.Property(t => t.EficienciaRealizacao).HasColumnName("EficienciaRealizacao");
			this.Property(t => t.Produtividade).HasColumnName("Produtividade");
			this.Property(t => t.FlagTP).HasColumnName("FlagTP");
		}
	}
}
