using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DICIONARIO_FK_VI_DTO
    {
        public string TABELA { get; set; }
        public string TABELAFK { get; set; }
        public string CAMPOFK { get; set; }
    }
}
