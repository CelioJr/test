using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBHorario
    {
        public int idHorario { get; set; }
        public Nullable<int> Turno { get; set; }
        public string HoraInicio { get; set; }
        public string HoraFim { get; set; }
        public string flgViraDia { get; set; }
    }
}
