﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using System.Web.Mvc;
using SEMP.Model.VO;

namespace SEMP.Controllers
{
    public class RetornoController : ApiController
    {

		public RetornoLogin Get(int id)
		{
			var sessao = HttpContext.Current.Session;
			return (RetornoLogin)sessao["RetornoLogin"];
		}

		public string Get(RetornoLogin retornologin)
		{
			if (retornologin != null) {
				return retornologin.CodDRT;
			} else {
				return "Não informado";
			}
		}

    }
}
