using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_SISTEMA_DTO
    {
        public FR_SISTEMA_DTO()
        {
            this.FR_DOC_PRINCIPAL = new List<FR_DOC_PRINCIPAL_DTO>();
            this.FR_HISTORICO_SQL = new List<FR_HISTORICO_SQL_DTO>();
            this.FR_MENU = new List<FR_MENU_DTO>();
            this.FR_TAREFA = new List<FR_TAREFA_DTO>();
            this.FR_FORMULARIO = new List<FR_FORMULARIO_DTO>();
        }

        public string SIS_CODIGO { get; set; }
        public string SIS_DESCRICAO { get; set; }
        public Nullable<int> IMG_CODIGO { get; set; }
        public Nullable<int> IMG_CODIGO_ICONE { get; set; }
        public string SIS_SQLDATALIMITE { get; set; }
        public string SIS_SQLDADOSENTIDADE { get; set; }
        public string SIS_SQLINFORMACOES { get; set; }
        public string SIS_CHECK { get; set; }
        public Nullable<bool> SIS_ACESSOEXTERNO { get; set; }
        public Nullable<int> SIS_GRUPOEXTERNO { get; set; }
        public string SIS_INFORMACAO { get; set; }
        public string SIS_RESUMO { get; set; }
        public virtual ICollection<FR_DOC_PRINCIPAL_DTO> FR_DOC_PRINCIPAL { get; set; }
        public virtual ICollection<FR_HISTORICO_SQL_DTO> FR_HISTORICO_SQL { get; set; }
        public virtual FR_IMAGEM_DTO FR_IMAGEM { get; set; }
        public virtual ICollection<FR_MENU_DTO> FR_MENU { get; set; }
        public virtual ICollection<FR_TAREFA_DTO> FR_TAREFA { get; set; }
        public virtual ICollection<FR_FORMULARIO_DTO> FR_FORMULARIO { get; set; }
    }
}
