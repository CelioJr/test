using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
	public partial class tbl_CBTipoAmarra
	{
		public int CodTipoAmarra { get; set; }
		public string DesTipoAmarra { get; set; }
		public string CodFam { get; set; }
		public string FlgMascaraPadrao { get; set; }
		public string MascaraPadrao { get; set; }
		public Nullable<int> FlgPlacaPainel { get; set; }
		public string CodProcesso { get; set; }
	}}
