using System;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
	public class ProdParadaDepDTO
	{
		[Display(Name = "F�b.")]
		public string CodFab { get; set; }
		[Display(Name = "No. Parada")]
		public string NumParada { get; set; }
		[Display(Name = "Departamento")]
		[Required]
		public string CodDepto { get; set; }
		[Display(Name = "F�b. do Departamento")]
		[Required]
		public string CodFabDepto { get; set; }
		[Display(Name = "Tipo de Aviso")]
		[Required]
		public string TipItem { get; set; }
		[Display(Name = "Grupo de Email")]
		[Required]
		public string CodGrupo { get; set; }
		[Display(Name = "Data Envio")]
		public Nullable<System.DateTime> DatEnvio { get; set; }
		public System.Guid rowguid { get; set; }

		[Display(Name = "Descri��o")]
		public string DesFabrica{ get; set; }
		[Display(Name = "Descri��o")]
		public string DesFabDep { get; set; }
		[Display(Name = "Descri��o")]
		public string DesDepartamento { get; set; }
		[Display(Name = "Descri��o")]
		public string DesGrupoEmail { get; set; }
	}
}
