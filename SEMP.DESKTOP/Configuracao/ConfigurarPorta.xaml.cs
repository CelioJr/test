﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SISAP.Configuracao
{
    /// <summary>
    /// Interaction logic for ConfigurarPorta.xaml
    /// </summary>
    public partial class ConfigurarPorta : Window
    {
        private SerialPort com;

        public ConfigurarPorta()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {

            ComboBoxItem typeItem = null;
            string velo = "", bitdados = "", paridade = "", bitParada = "", fluxo = "";
                
            typeItem = (ComboBoxItem)cbVelocidade.SelectedItem;
            velo = typeItem.Content.ToString();

            typeItem = (ComboBoxItem)cbBitDados.SelectedItem;
            bitdados = typeItem.Content.ToString();

            typeItem = (ComboBoxItem)cbParidade.SelectedItem;
            paridade = typeItem.Content.ToString();

            typeItem = (ComboBoxItem)cbBitParada.SelectedItem;
            bitParada = typeItem.Content.ToString();

            typeItem = (ComboBoxItem)cbFluxoControle.SelectedItem;
            fluxo = typeItem.Content.ToString();

            setVelocidade(velo);
            setBitDados(bitdados);
            setParity(paridade);
            setBitParada(bitParada);
            setControlFluxo(fluxo);
            com.ReadTimeout = 500;
            com.WriteTimeout = 500;

            SEMP.DESKTOP.Configuracao.Config.velocidade = velo;
            SEMP.DESKTOP.Configuracao.Config.bitDados = bitdados;
            SEMP.DESKTOP.Configuracao.Config.paridade = paridade;
            SEMP.DESKTOP.Configuracao.Config.bitParada = bitParada;
            SEMP.DESKTOP.Configuracao.Config.fluxo = fluxo;

            Close();
        }

        #region Configuracao do nome da porta
        public void setSerial(SerialPort com)
        {
            this.com = com;
            cbVelocidade.SelectedIndex = 5;
            cbBitDados.SelectedIndex = 3;
            cbParidade.SelectedIndex = 0;
            cbBitParada.SelectedIndex = 2;
            cbFluxoControle.SelectedIndex = 1;

        }
        private void setParity(String parity)
        {
            switch (parity)
            {
                case "odd":
                    com.Parity = Parity.Odd;
                    break;
                case "Even":
                    com.Parity = Parity.Even;
                    break;
                case "Mark":
                    com.Parity = Parity.Mark;
                    break;
                case "Space":
                    com.Parity = Parity.Space;
                    break;
                default:
                    com.Parity = Parity.None;
                    break;
            }
        }
        private void setBitParada(String bitParada)
        {

            switch (bitParada)
            {
                case "1":
                    com.StopBits = StopBits.One;
                    break;
                case "1.5":
                    com.StopBits = StopBits.OnePointFive;
                    break;
                case "2":
                    com.StopBits = StopBits.Two;
                    break;
                default:
                    com.StopBits = StopBits.None;
                    break;
            }
        }
        private void setControlFluxo(String controlFluxo)
        {

            switch (controlFluxo)
            {
                case "XON/XOFF":
                    com.Handshake = Handshake.XOnXOff;
                    break;
                case "RTS/CTS":
                    com.RtsEnable = true;//Enabled;
                    //com.CtsHolding = Handshake.XOnXOff;
                    break;
                case "DSR/DTR":

                    com.DtrEnable = true;//Enabled;

                    break;
                default:
                    com.Handshake = Handshake.None;
                    break;
            }
        }
        private void setVelocidade(String velocidade)
        {
            if (velocidade == null)
            {
                MessageBox.Show("Velocidade Invalida");
            }
            else
            {
                com.BaudRate = Int32.Parse(velocidade);
            }
        }
        private void setBitDados(String bitDados)
        {
            if (bitDados == null)
            {
                MessageBox.Show("Bit de Dados Invalido");
            }
            else
            {
                com.DataBits = Int32.Parse(bitDados);
            }
        }

        #endregion

        private void Window_Closed_1(object sender, EventArgs e)
        {
            Close();
        }
    }
}
