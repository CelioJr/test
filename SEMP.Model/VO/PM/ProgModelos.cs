﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.PM
{
	public class ProgModelos
	{
		public String CodFab { get; set; }
		public String CodModelo { get; set; }
		public String DesModelo { get; set; }
		public String CodLinha { get; set; }
		public String DesLinha { get; set; }
		public int NumTurno { get; set; }
		public decimal? QtdPrograma { get; set; }
		public decimal? QtdProduzido { get; set; }

		public decimal? QtdDiferenca
		{
			get { return QtdProduzido - QtdPrograma; }
		}

		public decimal? QtdCapacidade { get; set; }
		public String Familia { get; set; }
		public decimal? QtdPlanoMes { get; set; }
        public string FlagTP { get; set; }

	}
}
