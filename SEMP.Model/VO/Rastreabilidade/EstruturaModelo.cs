﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade
{
	public class EstruturaModelo
	{
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public string CodFilho { get; set; }
		public string DesFilho { get; set; }
		public bool Fantasma { get; set; }
		public int Indice { get; set; }

		public EstruturaModelo(string codModelo, string desModelo, string codFilho, string desFilho, bool fantasma, int indice)
		{
			CodModelo = codModelo;
			CodFilho = codFilho;
			Fantasma = fantasma;
			Indice = indice;
			DesModelo = desModelo;
			DesFilho = desFilho;

		}
	}
}
