
namespace SEMP.Model.DTO
{
    using System;

    public partial class Key_Component_dDTO
    {
        public string Model_Number { get; set; }
        public string Serial_Number { get; set; }
        public string MFG_Part_Number { get; set; }
        public string MFG_Serial_Number { get; set; }
        public System.DateTime Data_Registro { get; set; }
        public int? cod_operador { get; set; }
        public DateTime? Data_Desativacao { get; set; }
        public int? cod_OpeDesat { get; set; }
        public int? dispositivo_id { get; set; }
  
    }
}
