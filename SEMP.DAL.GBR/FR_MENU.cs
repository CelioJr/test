namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_MENU
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(3)]
        public string SIS_CODIGO { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MNU_CODIGO { get; set; }

        [StringLength(100)]
        public string MNU_DESCRICAO { get; set; }

        public int? MNU_CODIGO_PARENT { get; set; }

        public int MNU_INDICE { get; set; }

        public int? FRM_CODIGO { get; set; }

        [StringLength(30)]
        public string MNU_TECLA { get; set; }

        public int? IMG_CODIGO { get; set; }

        [StringLength(1)]
        public string MNU_SEPARADOR { get; set; }

        [StringLength(38)]
        public string MNU_GUID { get; set; }

        [StringLength(1)]
        public string MNU_TIPO { get; set; }

        public int? REL_CODIGO { get; set; }

        public virtual FR_FORMULARIO FR_FORMULARIO { get; set; }

        public virtual FR_SISTEMA FR_SISTEMA { get; set; }
    }
}
