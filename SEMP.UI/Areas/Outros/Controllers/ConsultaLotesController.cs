﻿using RAST.BO.Lote;
using SEMP.Model.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEMP.Areas.Outros.Controllers
{
    public class ConsultaLotesController : Controller
    {
        // GET: Outros/ConsultaLotes
        public ActionResult Index()
        {
            return View();
        }

		public ActionResult VerLotes(string setor, string codModelo, int? codLinha) {

			var dados = Lote.ObterLotesDaVez(setor, codModelo, codLinha);

			return View(dados);
		}

		public ActionResult ObterLotes(string codModelo, string datInicio, string datFim, string setor, int? codLinha, int? status, string numLote) 
		{ 
		
			DateTime dIni = DateTime.Now;
			DateTime? dFim = null;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if(!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}

            var resultado = new List<CBLoteDTO>();

            if (String.IsNullOrEmpty(numLote))
            {
                resultado = Lote.ObterLotes(dIni, setor, codModelo, codLinha, status, dFim);
            }

            else
            {
                resultado.Add(Lote.ObterLote(numLote));
            }

            return View(resultado);
		
		}

		public ActionResult ObterCaixasColetivas(string numLote)
		{

			var resultado = Lote.ObterCaixasColetivas(numLote);

			return View(resultado);

		}

		public ActionResult ObterPlacasEmbaladas(string numCC){

			var dados = Lote.ObterEmbaladosLote(numCC);

			return View(dados);

		}
    }
}