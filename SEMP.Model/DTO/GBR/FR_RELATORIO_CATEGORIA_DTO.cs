using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_RELATORIO_CATEGORIA_DTO
    {
        public int REL_CODIGO { get; set; }
        public int CAT_CODIGO { get; set; }
        public virtual FR_CATEGORIA_DTO FR_CATEGORIA { get; set; }
    }
}
