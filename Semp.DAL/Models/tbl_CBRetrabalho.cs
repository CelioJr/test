﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.Models
{
	public partial class tbl_CBRetrabalho
	{
		public tbl_CBRetrabalho()
		{			
			this.tbl_CBItensRetrabalho = new List<tbl_CBItensRetrabalho>();
			this.tbl_CBRotaRetrabalho = new List<tbl_CBRotaRetrabalho>();
		}

		public int CodRetrabalho { get; set; }
		public string Justificativa { get; set; }
		public System.DateTime DatEvento { get; set; }
		public string CodDRT { get; set; }
		public int Status { get; set; }
		public string CodFab { get; set; }
		public string NumAP { get; set; }
		public int Quantidade { get; set; }		
		public virtual ICollection<tbl_CBItensRetrabalho> tbl_CBItensRetrabalho { get; set; }
		public virtual ICollection<tbl_CBRotaRetrabalho> tbl_CBRotaRetrabalho { get; set; }
		public virtual tbl_CBTrocaItem tbl_CBTrocaItem { get; set; }
	}
}
