namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolvaritmologcau
    {
        [Key]
        public long id_consolvartimologcau { get; set; }

        public long id_consolvaritmolog { get; set; }

        public long? id_usr { get; set; }

        public long id_tritmo { get; set; }

        public DateTime? dthr_varitmo { get; set; }

        public virtual dw_consolvaritmolog dw_consolvaritmolog { get; set; }

        public virtual dw_t_ritmo dw_t_ritmo { get; set; }

        public virtual om_usr om_usr { get; set; }
    }
}
