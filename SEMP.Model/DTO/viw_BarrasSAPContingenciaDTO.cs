﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.DTO
{
	public class viw_BarrasSAPContingenciaDTO

	{
		public string Werks { get; set; }
		public string AUART { get; set; }
		public string AUFNR { get; set; }
		public string MATNR1 { get; set; }
		public string EAN13 { get; set; }
		public string SERIAL1 { get; set; }
		public string CREDATE1 { get; set; }
		public string MATNR2 { get; set; }
		public string SERIAL2 { get; set; }
		public string CREDATE2 { get; set; }
		public string PRUEFLOS { get; set; }
		public string CRETIME2 { get; set; }
		public string CRETIME1 { get; set; }
		public string ARBPL { get; set; }
		public string CODLINHA { get; set; }
		public Nullable<int> CODPOSTO { get; set; }
		public string DRT { get; set; }
		public Nullable<int> TPAMAR { get; set; }
		public Nullable<int> GESME { get; set; }
		public string STATCONF { get; set; }
		public string DTCONF { get; set; }
		public string MBLNR { get; set; }
		public string STATVD { get; set; }
		public string STBCKFLUSH { get; set; }
		public string DTBCKFLUSH { get; set; }
		public string USERNAME { get; set; }
		public string SISAP { get; set; }
		public string PACKNO { get; set; }
		public string VCODE { get; set; }
		public string OBA { get; set; }
		public Nullable<System.DateTime> DatReferencia { get; set; }
	}
}
