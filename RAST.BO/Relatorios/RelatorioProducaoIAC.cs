﻿using System;
using System.Collections.Generic;
using System.Linq;
using SEMP.Model.DTO;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using SEMP.Model.VO.CNQ;
using SEMP.Model;

namespace RAST.BO.Relatorios
{
	public class RelatorioProducaoIAC
	{

		public static List<CBResumoIACDTO> ObterResumoIAC2(DateTime DatInicio, DateTime DatFim)
		{
			// Carrega os dados da produção pelo IDW
			List<v_paradasDTO> paradasIDW = daoIDW.ObterParadasIDW(DatInicio, DatFim);
			List<v_producaoturnoDTO> producaoIDW = daoIDW.ObterProducaoIDW(DatInicio, DatFim);
			producaoIDW = producaoIDW.Where(a => a.producaobruta > 0 &&
						  //a.cd_pt.Contains("-2") || a.cd_pt.Contains("DT") &&
						  a.is_aponGt == 1 /*&&
						  (a.cd_turno == "1" || a.cd_turno == "2" || a.cd_turno == "3")*/).ToList();


			List<string> linhas = producaoIDW.GroupBy(x => x.cd_gt).Select(p => p.Max(s => s.cd_pt)).ToList();

			List<v_producaoturnoDTO> producaoidw = producaoIDW.Where(x => linhas.Contains(x.cd_pt)).ToList();

			// Monta a lista de linhas DE-PARA para achas as paradas do IDW
			var linhasDEPARA = daoIDW.ObterLinhasDePara();

			// Busca o programa e o resumo a partir dos dados da STA
			List<CBResumoIACDTO> resumo = daoResumoIAC.ObterResumoIAC(DatInicio, DatFim);
			List<CBResumoIACDTO> programa = daoResumoIAC.ObterProgIAC(DatInicio, DatFim);

			// Buscar os horários de produção(obtem-se por padrao a linha 34
			List<CBHorarioDTO> horarios = daoGeral.ObterHorariosFabrica("IAC", 34);

			//var Defeitos = ObterIndiceDefeito(DatInicio, DatFim, 1);
			var def = daoDefeitos.ObterDefeitos(DatInicio, DatFim).Where(x => x.FlgRevisado != 2 && x.FlgRevisado != 4).ToList();

			var Defeitos = def.GroupBy(x => new { x.CodLinha, x.CodModelo, x.DatReferencia}).Select(x => new { 
				CodLinha = x.Key.CodLinha,
				CodModelo = x.Key.CodModelo,
                DatReferencia = x.Key.DatReferencia,
				QtdDefeito = x.Count(),
				QtdDefeitoReal = x.Count(y => !(y.CodCausa == "WX00 " || y.DesAcao == "2"))
			}).ToList();

			// navega nos dados de resumo que encontrou e atualiza os dados
			foreach (CBResumoIACDTO r in resumo)
			{
				if (programa != null)
				{
					var prog =
						programa.FirstOrDefault(
							p => p.Data == r.Data && p.Turno == r.Turno && p.Linha.Trim() == r.Linha.Trim() && p.CodModelo == r.CodModelo && p.FlagTP == r.FlagTP);
					if (prog != null)
					{
						r.QtdPlano = prog.QtdPlano;
						if (prog.Apontado != null)
							r.Apontado = prog.Apontado;
					}
					else
					{
						r.QtdPlano = 0;
					}   
				}

				r.QtdReal = (int?)(from p in producaoidw
								   where p.dt_referencia != null && (p.dt_referencia.Value == r.Data &&
																	 p.cd_cp == r.CodModelo &&
																	 p.cd_gt.Trim() == r.Linha.Trim() &&
																	 p.cd_turno == r.Turno.ToString())
								   select p.producaobruta).FirstOrDefault();

				r.QtdReal = r.QtdReal ?? 0;

				if (r.QtdReal == 0)
				{
					//buscar produção no banco de dados da semp

					r.QtdReal = RecuperarQuantidadeProduzida(r.Data, r.Linha.Trim(), r.CodModelo, r.Turno, r.FlagTP);
				}


				/*var paradasidw = (from p in paradasIDW
								  join l in linhasDEPARA on p.maquina.Trim() equals l.CodLinhaDE
								  where l.CodLinhaPARA.Trim() == r.Linha.Trim()
								  select p.descricao).Distinct().ToList();*/

				//if (paradasidw.Any())
				//{

				//    var paradas = string.Join("; ", paradasidw);

				//    r.OcorrenciaParada = paradas;
				//}

				r.TempoDisp = (int)horarios.Where(x => x.Turno == r.Turno).Select(x => x.TempoDisp).FirstOrDefault();

				r.QtdPontos = daoResumoIAC.ObterQuantidadePontos(r.CodModelo);

				int codlinha = RecuperarCodLinhaFromT3(r.Linha);

                var umDefeito = Defeitos.Where(x => x.CodLinha == codlinha && x.CodModelo == r.CodModelo && x.DatReferencia == r.Data).FirstOrDefault();

                if (umDefeito != null)
				{
					r.QtdDefeitos = umDefeito.QtdDefeito;
					r.QtdDefeitosReal = umDefeito.QtdDefeitoReal;
				}
				else
				{
					r.QtdDefeitos = 0;
					r.QtdDefeitosReal = 0;
				}

			}

			if (programa != null)
			{
				var novos = programa.Except(resumo).ToList();

				foreach (CBResumoIACDTO r in novos)
				{
					r.QtdReal = (int?)(from p in producaoidw
									   where p.dt_referencia != null && (p.dt_referencia.Value == r.Data &&
																		 p.cd_cp == r.CodModelo &&
																		 p.cd_gt.Trim() == r.Linha.Trim() &&
																		 p.cd_turno == r.Turno.ToString())
									   select p.producaobruta).FirstOrDefault();

					r.QtdReal = r.QtdReal ?? 0;

					if (r.QtdReal == 0)
					{ 
						//buscar produção no banco de dados da semp

						r.QtdReal = RecuperarQuantidadeProduzida(r.Data, r.Linha.Trim(), r.CodModelo, r.Turno, r.FlagTP);
					}

					string paradas = string.Join("; ", paradasIDW.Where(x => x.maquina == r.Linha).Select(x => x.descricao));

					r.OcorrenciaParada = paradas;

					r.TempoDisp = (int)horarios.Where(x => x.Turno == r.Turno).Select(x => x.TempoDisp).FirstOrDefault();

					r.QtdPontos = daoResumoIAC.ObterQuantidadePontos(r.CodModelo);

					int codlinha = RecuperarCodLinhaFromT3(r.Linha);
					if (Defeitos.Where(x => x.CodLinha == codlinha).FirstOrDefault() != null)
						r.QtdDefeitos = Defeitos.Where(x => x.CodLinha == codlinha).FirstOrDefault().QtdDefeito;
						
					else
						r.QtdDefeitos = 0;

					r.FlagTP = r.FlagTP ?? "";
				}

				// inclui no objeto os dados novos
				resumo.AddRange(novos);
			}

			resumo.ForEach(x => daoResumoIAC.AtualizaResumo(x));
			resumo.RemoveAll(x => x.QtdPlano == 0 && x.QtdReal == 0);

			return resumo.OrderBy(x => x.Linha).ThenBy(x => x.Data).ToList();
		}

		public static List<CBResumoIACDTO> ObterResumoIAC(DateTime DatInicio, DateTime DatFim){

			var resumoProducao = ResumoProducaoBO.ObterResumoIAC(DatInicio, DatFim, "").OrderBy(x => x.Turno).ThenBy(x => x.CodLinha).ThenBy(x => x.DesModelo).ToList();

			var resultado = resumoProducao.Where(x => x.Turno < 9).Select(x => new CBResumoIACDTO() { 
				Linha = x.CodLinha,
				Apontado = x.QtdApontado,
				CodModelo = x.CodModelo,
				Data = x.DatProducao,
				DesModelo = x.DesModelo,
				FlagTP = x.FlagTp,
				QtdDefeitos = (int)x.QtdDefeito,
				QtdPlano = x.QtdPlano,
				QtdReal = x.QtdRealizado,
				Turno = x.Turno
			}).ToList();

			var def = daoDefeitos.ObterDefeitos(DatInicio, DatFim).Where(x => x.FlgRevisado != 2 && x.FlgRevisado != 4).ToList();

			var Defeitos = def.GroupBy(x => new { x.CodLinha, x.CodModelo, x.DatReferencia }).Select(x => new {
				CodLinha = x.Key.CodLinha,
				CodModelo = x.Key.CodModelo,
				DatReferencia = x.Key.DatReferencia,
				QtdDefeito = x.Count(),
				QtdDefeitoReal = x.Count(y => !(y.CodCausa == "WX00 " || y.DesAcao == Constantes.ACAO_DEFEITO_NAO_ENCONTRADO.ToString()))
			}).ToList();

			// Buscar os horários de produção(obtem-se por padrao a linha 34
			List<CBHorarioDTO> horarios = daoGeral.ObterHorariosFabrica("IAC", 34);

			resultado.ForEach(x => {

				x.TempoDisp = (int)horarios.Where(z => z.Turno == x.Turno).Select(y => y.TempoDisp).FirstOrDefault();
				x.QtdPontos = daoResumoIAC.ObterQuantidadePontos(x.CodModelo);

				int codlinha = RecuperarCodLinhaFromT3(x.Linha);

				var umDefeito = Defeitos.Where(z => z.CodLinha == codlinha && z.CodModelo == x.CodModelo && z.DatReferencia == x.Data).FirstOrDefault();

				if (umDefeito != null)
				{
					x.QtdDefeitos = umDefeito.QtdDefeito;
					x.QtdDefeitosReal = umDefeito.QtdDefeitoReal;
				}
				else
				{
					x.QtdDefeitos = 0;
					x.QtdDefeitosReal = 0;
				}
				daoResumoIAC.AtualizaResumo(x);
			});

			return resultado;


		}

		public static CBResumoIACDTO ObterProducaoIAC(DateTime Data, int Turno, string Linha, string CodModelo)
		{
			return daoResumoIAC.ObterResumoIAC(Data, Turno, Linha, CodModelo);
		}

		public static CBResumoIACDTO AtualizaResumo(CBResumoIACDTO resumo)
		{
			return daoResumoIAC.AtualizaResumo(resumo);
		}

		public static List<ResumoProducao> ObterResumoIAC(DateTime DatInicio, DateTime DatFim, string Linha, int turno)
		{
			return daoResumoProducao.ObterResumoIAC(DatInicio, DatFim).Where(x => x.CodLinha.Trim() == Linha.Trim() && x.Turno == turno).ToList();
		}

		public static string ObterModeloFase(string codModelo, string fase)
		{
			return daoPainelProducao.ObterModeloFase(codModelo, fase);
		}

		public static List<IndicadorPostos> ObterIndicadoresQuantidadeProduzidaPostosModelo(DateTime DatInicio, DateTime DatFim, int CodLinha, int Turno, string Modelo)
		{
			return daoRelatorioProducaoIAC.ObterIndicadoresQuantidadeProduzidaPostosModelo(DatInicio, DatFim, CodLinha, Turno, Modelo);
		}

		public static List<IndicadorPostos> ObterIndicadoresQuantidadeDefeitosModelo(DateTime DatInicio, DateTime DatFim, int CodLinha, int Turno, string modelo)
		{
			return daoRelatorioProducaoIAC.ObterIndicadoresQuantidadeDefeitosModelo(DatInicio, DatFim, CodLinha, Turno, modelo);
		}

		public static int ObterQuantidadePontosPorModelo(string codModelo)
		{
			return daoRelatorioProducaoIAC.ObterQuantidadePontosPorModelo(codModelo);
		}

		public static decimal GetQuantidadeProduzidaPostoEHoraModelo(
			DateTime DataInicio,
			DateTime DataFim,
			TimeSpan HoraInicio,
			TimeSpan HoraFim,
			int NumPosto,
			int Turno,
			int CodLinha,
			string modelo)
		{
			return daoRelatorioProducaoIAC.GetQuantidadeProduzidaPostoEHoraModelo(DataInicio, DataFim, HoraInicio, HoraFim, NumPosto, Turno, CodLinha, modelo);
		}


		public static decimal GetQuantidadeDefeitosPostoEHoraModelo(
			DateTime DataInicio,
			DateTime DataFim,
			TimeSpan HoraInicio,
			TimeSpan HoraFim,
			int NumPosto,
			int Turno,
			int CodLinha,
			string modelo)
		{
			return daoRelatorioProducaoIAC.GetQuantidadeDefeitosPostoEHoraModelo(DataInicio, DataFim, HoraInicio, HoraFim, NumPosto, Turno, CodLinha, modelo);
		}

		public static List<MaioresDefeitos> GetDefeitosLinhaModelo(DateTime dataInicial, DateTime dataFinal, int CodLinha, string CodModelo)
		{
			return daoRelatorioProducaoIAC.GetDefeitosLinhaModelo(dataInicial, dataFinal, CodLinha, CodModelo);
		}

		public static List<IndiceDefeitoLinha> ObterIndiceDefeito(DateTime DatInicio, DateTime DatFim, int? Turno)
		{

			var linhas = ObterLinhasIAC(DatInicio);
			 
			List<IndiceDefeitoLinha> indice = new List<IndiceDefeitoLinha>();

			foreach (var linha in linhas)
			{
				var posto = daoPassagem.ObterPrimeiroPostoLeitura(linha.CodLinha);
				//var qtdLeitura = daoPassagem.ObterQtdLeiturasPosto(DatInicio, DatFim, linha.CodLinha, posto);
				//var qtdEmbalado = daoRelatorioTecnico.ObterProducaoLinha(DateTime.Parse(DatInicio.ToShortDateString()), DateTime.Parse(DatFim.ToShortDateString()), linha.CodLinha);
				var qtdDefeito = daoDefeitos.ObterQtdDefeitosLinha(DateTime.Parse(DatInicio.ToShortDateString()), DateTime.Parse(DatFim.ToShortDateString()), linha.CodLinha);
				var qtdFalsaFalha = daoDefeitos.ObterQtdDefeitosFFLinha(DateTime.Parse(DatInicio.ToShortDateString()), DateTime.Parse(DatFim.ToShortDateString()), linha.CodLinha);

				var indlinha = new IndiceDefeitoLinha()
				{
					CodLinha = linha.CodLinha,                    
					//QtdProducao = qtdLeitura,
					//QtdEmbalado = qtdEmbalado,
					QtdDefeito = qtdDefeito,
					QtdFalsaFalha = qtdFalsaFalha,
					QtDefeitoReal = qtdDefeito - qtdFalsaFalha
				};

				indice.Add(indlinha);
			}

			return indice;
		}

		public static List<CBLinhaDTO> ObterLinhasIAC(DateTime? datProducao)
		{
			return daoResumoIAC.LinhasIAC("IAC", datProducao);
		}

		public static int RecuperarCodLinhaFromT3(string codlinha)
		{
			return daoResumoIAC.RecuperarCodLinhaFromT3(codlinha);
		}

		public static string RecuperarCodLinhaT1FromT3(string codlinha)
		{
			return daoResumoIAC.RecuperarCodLinhaT1FromT3(codlinha);
		}

		public static int RecuperarQuantidadeProduzida(DateTime dataproducao, string codlinhaT3, string modelo, int turno, string flagTP)
		{

			string codlinhaT1 = RecuperarCodLinhaT1FromT3(codlinhaT3);
			var quantidade = 0;

			if (!string.IsNullOrEmpty(codlinhaT1))
			{  
				quantidade = daoResumoIAC.ObterQuantidadeProduzida(dataproducao, codlinhaT1, modelo, turno, flagTP);
			}


			return quantidade;
		}
	}

}
