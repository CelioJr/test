namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_rota
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_rota()
        {
            dw_rotapasso = new HashSet<dw_rotapasso>();
            om_obj = new HashSet<om_obj>();
            pp_cp = new HashSet<pp_cp>();
        }

        [Key]
        public long id_rota { get; set; }

        [StringLength(30)]
        public string cd_rota { get; set; }

        public int? revisao { get; set; }

        public DateTime? dt_revisao { get; set; }

        public short? st_ativo { get; set; }

        public DateTime? dt_stativo { get; set; }

        [StringLength(40)]
        public string ds_rota { get; set; }

        public long id_gt { get; set; }

        public long id_produto { get; set; }

        public short? is_modelo { get; set; }

        public long id_usrrevisao { get; set; }

        public long id_usrstativo { get; set; }

        public decimal? gridx { get; set; }

        public decimal? gridy { get; set; }

        public decimal? largura { get; set; }

        public decimal? altura { get; set; }

        public short? is_passaadiante { get; set; }

        public decimal? seg_setup { get; set; }

        public virtual om_gt om_gt { get; set; }

        public virtual om_produto om_produto { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rotapasso> dw_rotapasso { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_obj> om_obj { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cp> pp_cp { get; set; }
    }
}
