using System;

namespace SEMP.DAL.Models
{
    public partial class tbl_PlanoEtapaMaq
    {
        public string CodModelo { get; set; }
        public string NumRevisao { get; set; }
        public string NumEtapa { get; set; }
        public string NomMaq { get; set; }
        public string CodProcesso { get; set; }
        public Nullable<decimal> TempoPadrao { get; set; }
        public System.Guid rowguid { get; set; }
        public short FlgLeftRight { get; set; }
        public string CodInsersora { get; set; }
        public string CodFam { get; set; }
        public string CodBloco { get; set; }
    }
}
