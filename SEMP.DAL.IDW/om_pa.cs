namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_pa
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_pa()
        {
            dw_estlocal = new HashSet<dw_estlocal>();
            om_mapapa = new HashSet<om_mapapa>();
            om_papro = new HashSet<om_papro>();
            om_prgpos = new HashSet<om_prgpos>();
        }

        [Key]
        public long id_pa { get; set; }

        [StringLength(30)]
        public string cd_pa { get; set; }

        public int? revisao { get; set; }

        [StringLength(256)]
        public string ds_pa { get; set; }

        public DateTime? dt_revisao { get; set; }

        public DateTime? dt_stativo { get; set; }

        public short? st_ativo { get; set; }

        public long id_usrstativo { get; set; }

        public long id_usrrevisao { get; set; }

        public int? ordem { get; set; }

        [StringLength(30)]
        public string desvio { get; set; }

        public long id_pt { get; set; }

        [StringLength(10)]
        public string depara { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_estlocal> dw_estlocal { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_mapapa> om_mapapa { get; set; }

        public virtual om_pt om_pt { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_papro> om_papro { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_prgpos> om_prgpos { get; set; }
    }
}
