using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_ProdParadaDepItem
    {
        public string CodFab { get; set; }
        public string NumParada { get; set; }
        public string CodDepto { get; set; }
        public Nullable<System.DateTime> DatRetorno { get; set; }
        public string DesCausa { get; set; }
        public string DesAcao { get; set; }
        public string NomResponsavel { get; set; }
        public System.Guid rowguid { get; set; }
        public Nullable<System.DateTime> DatEnvio { get; set; }
        public string FlgStatus { get; set; }
        public string DesObs { get; set; }
        public string CodFabDepto { get; set; }
    }
}
