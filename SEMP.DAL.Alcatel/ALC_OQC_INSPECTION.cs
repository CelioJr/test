namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_OQC_INSPECTION")]
    public partial class ALC_OQC_INSPECTION
    {
        [Key]
        public int id_inspection { get; set; }

        [StringLength(50)]
        public string box { get; set; }

        public DateTime? creation_date { get; set; }

        public DateTime? finTime { get; set; }

        [StringLength(255)]
        public string observation { get; set; }

        [Required]
        [StringLength(50)]
        public string sku { get; set; }

        [StringLength(10)]
        public string defect { get; set; }

        public int line { get; set; }

        public int? oqc_operator { get; set; }

        public int product { get; set; }

        public int? target { get; set; }

        [StringLength(20)]
        public string status { get; set; }

        [StringLength(10)]
        public string family { get; set; }

        [StringLength(25)]
        public string lot { get; set; }

        public bool? engine { get; set; }

        public virtual ALC_DEFECT ALC_DEFECT { get; set; }

        public virtual ALC_OQC_TARGET ALC_OQC_TARGET { get; set; }

        public virtual ALC_SYS_USER ALC_SYS_USER { get; set; }

        public virtual ALC_VOLUME ALC_VOLUME { get; set; }
    }
}
