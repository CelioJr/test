namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_folhamedtemphorcfg
    {
        [Key]
        public long id_folhamedtemphorcfg { get; set; }

        public long id_folhamedtemphorario { get; set; }

        [Required]
        [StringLength(40)]
        public string ds_cfg { get; set; }

        public decimal? lim_inf_temp { get; set; }

        public decimal? lim_sup_temp { get; set; }

        [Required]
        [StringLength(11)]
        public string cor_intervalo { get; set; }

        public virtual dw_folhamedtemhor dw_folhamedtemhor { get; set; }
    }
}
