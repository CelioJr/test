using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBKitAcessorioMap : EntityTypeConfiguration<tbl_CBKitAcessorio>
    {
        public tbl_CBKitAcessorioMap()
        {
            // Primary Key
            this.HasKey(t => t.CodKit);

            // Properties
            this.Property(t => t.NumSerie)
                .HasMaxLength(10);

            this.Property(t => t.SerialAdaptador)
                .HasMaxLength(30);

            this.Property(t => t.NumLicencaNero)
                .HasMaxLength(39);

            // Table & Column Mappings
            this.ToTable("tbl_CBKitAcessorio");
            this.Property(t => t.CodKit).HasColumnName("CodKit");
            this.Property(t => t.DatImpressao).HasColumnName("DatImpressao");
            this.Property(t => t.CodOperador).HasColumnName("CodOperador");
            this.Property(t => t.CodOF).HasColumnName("CodOF");
            this.Property(t => t.NumSerie).HasColumnName("NumSerie");
            this.Property(t => t.SerialAdaptador).HasColumnName("SerialAdaptador");
            this.Property(t => t.NumLicencaNero).HasColumnName("NumLicencaNero");

        }
    }
}
