namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_TAREFA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FR_TAREFA()
        {
            FR_TAREFA_TEMPO = new HashSet<FR_TAREFA_TEMPO>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TRF_CODIGO { get; set; }

        [Required]
        [StringLength(255)]
        public string TRF_DESCRICAO { get; set; }

        [Required]
        [StringLength(3)]
        public string SIS_CODIGO { get; set; }

        public int REG_CODIGO { get; set; }

        public DateTime? TRF_DATA_INICIAL { get; set; }

        public DateTime? TRF_DATA_FINAL { get; set; }

        [Required]
        [StringLength(1)]
        public string TRF_ATIVA { get; set; }

        public string TRF_REGRA_PARAMETROS { get; set; }

        [StringLength(15)]
        public string TRF_TIPO_AGENDAMENTO { get; set; }

        public virtual FR_REGRAS FR_REGRAS { get; set; }

        public virtual FR_SISTEMA FR_SISTEMA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_TAREFA_TEMPO> FR_TAREFA_TEMPO { get; set; }
    }
}
