using System;

namespace SEMP.DAL.Models
{
    public partial class tbl_PlanoAlt
    {
        public string CodModelo { get; set; }
        public string CodItem { get; set; }
        public string NumPosicao { get; set; }
        public string CodItemAlt { get; set; }
        public System.DateTime DatInicio { get; set; }
        public string FlgAtivo { get; set; }
        public string NumNAEnt { get; set; }
        public string NumNASai { get; set; }
        public Nullable<System.DateTime> DatFim { get; set; }
        public string CodBreakdown { get; set; }
        public string NomUsuario { get; set; }
        public System.Guid rowguid { get; set; }
    }
}
