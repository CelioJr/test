using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class EntMovDTO
    {
        public int NumRomaneio { get; set; }
        public string CodOrigem { get; set; }
        public string CodFab { get; set; }
        public string CodTipo { get; set; }
        public string NomUsuario { get; set; }
        public string NomMaquina { get; set; }
        public string CodStatus { get; set; }
        public System.DateTime DatGerac { get; set; }
        public Nullable<int> QtdImpressa { get; set; }
        public Nullable<System.DateTime> DtUltimaImp { get; set; }
        public string UltImpUser { get; set; }
    }
}
