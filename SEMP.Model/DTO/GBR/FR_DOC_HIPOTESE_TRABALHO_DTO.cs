using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DOC_HIPOTESE_TRABALHO_DTO
    {
        public int ATO_CODIGO { get; set; }
        public int DOC_CODIGO { get; set; }
        public string TRA_DESCRICAO { get; set; }
        public virtual FR_DOC_ATOR_DTO FR_DOC_ATOR { get; set; }
    }
}
