﻿namespace SEMP.Model.VO.SAP
{
	public class ZSPP_APONTAMENTO_SISAP
	{

		public string PACKNO { get; set; }

		public ZSPP_APONTAMENTO_PCI[] INTTROW { get; set; }
	}

	public class ZSPP_APONTAMENTO_PCI
	{
		public string EXTROW { get; set; }

		public string AUFNR { get; set; }

		public decimal YIELD { get; set; }

		public string MEINS { get; set; }
	}
}
