﻿using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.DTO;
using SEMP.Model.VO.Rastreabilidade;
using System;
using System.Collections.Generic;
using System.Linq;
using SEMP.Model;

namespace RAST.BO.Rastreabilidade
{
	public class PostoOBA
	{

		#region Consultas RPA


		public static RPADTO ObterRPA(string codFab, string numRPA)
		{

			var dado = daoPostoOBA.ObterRPA(codFab, numRPA);

			dado.DesItem = Geral.ObterDescricaoItem(dado.CodItem);

			return dado;
		}


		#endregion

		#region Consultas GPA

		public static List<ResumoGPA> ObterPalletsGPA(DateTime datInicio, DateTime datFim)
		{
			return daoPostoOBA.ObterPalletsGPA(datInicio, datFim);

		}

		public static ResumoGPA ObterPalletGPA(string numPallet, string codModelo, string numSerie)
		{
			return daoPostoOBA.ObterPalletGPA(numPallet, codModelo, numSerie);

		}

		public static List<string> ObterSeriesPallet(string numPallet)
		{
			return daoPostoOBA.ObterSeriesPallet(numPallet);

		}


		#endregion

		#region Consultas Testes (Defeitos)

		public static List<CBCadDefeitoDTO> ObterTestesOBA()
		{

			return daoPostoOBA.ObterTestesOBA();

		}

		public static List<CBCadSubDefeitoDTO> ObterTestesEspecificosOBA()
		{

			return daoPostoOBA.ObterTestesEspecificosOBA();

		}

		#endregion


		#region Consultas OBA
		public static List<OBAResumoInspecao> ObterResumoInspecao(DateTime datInicio, DateTime datFim)
		{
			return daoPostoOBA.ObterResumoInspecao(datInicio, datFim);
		
		}


		public static CBLoteConfigDTO ObterConfigLote(int codLinha)
		{

			return daoPostoOBA.ObterConfigLote(codLinha);

		}


		public static List<CBOBARegistroDTO> ObterRegistros(DateTime datInicio, DateTime datFim)
		{

			return daoPostoOBA.ObterRegistros(datInicio, datFim);
		
		}

		public static List<CBOBARegistroDTO> ObterRegistrosDefeitos(DateTime datInicio)
		{

			return daoPostoOBA.ObterRegistrosStatus(datInicio, "X");

		}

		public static CBOBARegistroDTO ObterRegistro(string codFab, string numRPA, string codPallet)
		{

			return daoPostoOBA.ObterRegistro(codFab, numRPA, codPallet);

		}

		public static CBOBARegistroDTO ObterRegistro(string codFab, string numRPA)
		{

			return daoPostoOBA.ObterRegistro(codFab, numRPA);

		}


		public static List<OBARelatorioInspecao> ObterInspecoes(DateTime datInicio, DateTime datFim)
		{
			var dados = daoPostoOBA.ObterInspecoes(datInicio, datFim);
			return dados;

		}

		public static List<CBOBAInspecaoDTO> ObterInspecoes(string codFab, string numRPA, string codPallet)
		{

			if (String.IsNullOrEmpty(codPallet))
			{
				return daoPostoOBA.ObterInspecoes(codFab, numRPA);
			}
			else
			{
				return daoPostoOBA.ObterInspecoes(codFab, numRPA, codPallet);
			}

		}

		public static CBOBAInspecaoDTO ObterInspecaoSerie(string numECB, string codFab, string numRPA)
		{

			return daoPostoOBA.ObterInspecaoSerie(numECB, codFab, numRPA);

		}


		public static List<CBOBATestesDTO> ObterTestes(string codFab, string numRPA, string numECB)
		{
			if (String.IsNullOrEmpty(numECB))
			{
				return daoPostoOBA.ObterTestes(codFab, numRPA);
			}
			else
			{
				return daoPostoOBA.ObterTestes(codFab, numRPA, numECB);
			}
			

		}

		public static CBOBATestesDTO ObterTesteSerie(string numECB, string codFab, string numRPA, string codDefeito, int codSubDefeito)
		{

			return daoPostoOBA.ObterTesteSerie(numECB, codFab, numRPA, codDefeito, codSubDefeito);

		}


		public static string ObterECBSerie(string codModelo, string numSerie)
		{
			return daoPostoOBA.ObterECBSerie(codModelo, numSerie);

		}

		public static bool VerificaPalletTotal(string codFab, string numRPA)
		{
			return daoPostoOBA.VerificaPalletTotal(codFab, numRPA);
		}

		public static int ObterQtdAprovPallet(string codFab, string numRPA, string flgAprov = "S")
		{
			return daoPostoOBA.ObterQtdAprovPallet(codFab, numRPA, flgAprov);
		}
		#endregion


		#region Manutenção OBA

		public static bool GravarInspecao(CBOBAInspecaoDTO dadosInspec, List<CBOBATestesDTO> testes) {

			try
			{
				//identifica se a inspeção foi realizada com o aparelho aprovado
				var flgStatus = "S";
				var qtdReprov = testes.Count(x => x.FlgStatus == "X");
				if (qtdReprov > 0) {
					flgStatus = "X";
				}
				dadosInspec.FlgStatus = flgStatus;
				dadosInspec.DatLeitura = DateTime.Now;

				//gravar a inspeção da série
				GravarInspecao(dadosInspec);

				//gravar os dados dos testes
				testes.ForEach(x => {
					if (x.FlgStatus == "X")
					{
						var defeito = new CBDefeitoDTO()
						{
							NumECB = x.NumECB,
							CodLinha = x.CodLinha,
							NumPosto = x.NumPosto,
							CodDefeito = x.CodDefeito,
							CodSubDefeito = x.CodSubDefeito,
							DatEvento = SEMP.BO.Util.GetDate(),
							ObsDefeito = x.Observacao,
							FlgRevisado = Constantes.DEF_ABERTO
						};
						daoDefeitos.InsereDefeito(defeito); //insere o defeito para revisão no posto técnico
					}
					// grava os dados do teste em questão
					GravarTeste(x); 
				});

				return true;
			}
			catch
			{
				return false;
			}


		}

		public static bool FecharInspecao(string codFab, string numRPA, string codPallet) {

			try
			{
				var dado = ObterRegistro(codFab, numRPA, codPallet);
				var series = ObterInspecoes(codFab, numRPA, codPallet);

				dado.DatFechamento = SEMP.BO.Util.GetDate();
				if (series.Count(x => x.FlgStatus == "X") > 0)
				{
					dado.FlgStatus = "X";
				}
				else
				{
					dado.FlgStatus = "F";
				}

				if (numRPA.StartsWith("P")) {
					dado.NumRPE = numRPA;
					dado.DatLiberado = dado.DatFechamento;
				}

				GravarRegistro(dado);

				return true;
			}
			catch
			{
				return false;
			}

		}


		public static string GravarRegistro(CBOBARegistroDTO dados, bool flgExcluir = false, bool flgMovimenta = false) {

			return daoPostoOBA.GravarRegistro(dados, flgExcluir, flgMovimenta);

		}

		public static bool GravarInspecao(CBOBAInspecaoDTO dados, bool flgExcluir = false)
		{
			return daoPostoOBA.GravarInspecao(dados, flgExcluir);
		}

		public static bool GravarTeste(CBOBATestesDTO dados, bool flgExcluir = false)
		{
			
			return daoPostoOBA.GravarTeste(dados, flgExcluir);

		}



		#endregion

		#region Consultas Status

		public List<CBOBATipoStatusDTO> ObterListaStatus() {

			return daoPostoOBA.ObterListaStatus();

		}

		public CBOBATipoStatusDTO ObterTipoStatus(string codStatus)
		{

			return daoPostoOBA.ObterTipoStatus(codStatus);

		}
		#endregion
	}
}
