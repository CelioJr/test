namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consoldef
    {
        [Key]
        public long id_consoldef { get; set; }

        public decimal? qt_defeitos { get; set; }

        public long? id_area { get; set; }

        public long id_consol { get; set; }

        public long? id_tdefeito { get; set; }

        public virtual dw_consol dw_consol { get; set; }

        public virtual dw_t_area dw_t_area { get; set; }

        public virtual dw_t_defeito dw_t_defeito { get; set; }
    }
}
