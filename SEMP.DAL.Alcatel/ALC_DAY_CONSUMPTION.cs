namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_DAY_CONSUMPTION")]
    public partial class ALC_DAY_CONSUMPTION
    {
        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal id_consumption { get; set; }

        public DateTime? dtCreated { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? energy { get; set; }

        public bool seal { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? water { get; set; }

        public int? people { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? energyDiff { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? waterDiff { get; set; }
    }
}
