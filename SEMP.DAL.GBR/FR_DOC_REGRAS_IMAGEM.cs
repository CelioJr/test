namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_DOC_REGRAS_IMAGEM
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DOC_CODIGO { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int REG_COD { get; set; }

        public string REG_DEPENDENCIA { get; set; }

        public byte[] REG_IMAGEM { get; set; }

        [StringLength(3)]
        public string SIS_COD { get; set; }

        [StringLength(32)]
        public string REG_HASH { get; set; }

        public virtual FR_DOC_PRINCIPAL FR_DOC_PRINCIPAL { get; set; }

        public virtual FR_REGRAS FR_REGRAS { get; set; }
    }
}
