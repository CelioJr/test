﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.Rastreabilidade;
using RAST.BO.Relatorios;

namespace SEMP.Areas.Outros.Controllers
{
    public class ProducaoHoraPostoController : Controller
    {
        //
        // GET: /Outros/ProducaoHoraPosto/

        public ActionResult Index()
        {
            var linhas = new Configuracao().ObterLinhas();
            var postos = new Configuracao().ObterPostos(10);
           
            ViewBag.ListaLinhas= linhas;
            ViewBag.Postos = postos;
            return View();
        }

        [HttpPost]
        public ActionResult Lista(string dataini, string datafim, int horainicio, int horafim, int CodLinha, int? cmbPosto)
        {
            
            var dtInicio = string.IsNullOrWhiteSpace(dataini) ? DateTime.Now : DateTime.Parse(dataini);
            var dtFim = string.IsNullOrWhiteSpace(datafim) ? DateTime.Now : DateTime.Parse(datafim);
            dtFim = dtFim.AddDays(1).AddSeconds(-1);
            var lista = ProducaoHoraPostoBO.ListaQtdPosto(dtInicio, dtFim, CodLinha, horainicio, horafim, cmbPosto);
            //var postos = new Configuracao().ObterPostos(linha);
           // ViewBag.Postos = postos;
            
            return View(lista);
        }


        //public ActionResult Lista(string dataini, string datafim, int horainicio, int horafim, int linha , int posto)
        //{

        //    var dtInicio = string.IsNullOrWhiteSpace(dataini) ? DateTime.Now : DateTime.Parse(dataini);
        //    var dtFim = string.IsNullOrWhiteSpace(datafim) ? DateTime.Now : DateTime.Parse(datafim);
        //    dtFim = dtFim.AddDays(1).AddSeconds(-1);
        //    var lista = ProducaoHoraPostoBO.ListaQtdPosto(dtInicio, dtFim, linha, horainicio, horafim,posto);
        //    var postos = new Configuracao().ObterPostos(linha);
        //    ViewBag.Postos = postos;

        //    return View(lista);
        //}


    }
}
