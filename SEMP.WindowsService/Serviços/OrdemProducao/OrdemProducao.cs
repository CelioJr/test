﻿using log4net;
using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.Timers;

namespace SEMP.WindowsService
{
	partial class OrdemProducao : ServiceBase
	{
		#region Constants

		//ATENÇÃO: Deixar o nome do EVENT_LOG_SOURCE diferente do nome do serviço.
		// event log constantes
		private const String EVENT_LOG_SOURCE = "OrdemProducaoLOG";
		private const String EVENT_LOG_NAME = "SEMP TCL";

		//Configuration keys
		private const String CONFIG_ORDEMPRODUCAO_POOLING_TIME = "ORDEMPRODUCAO_POOLING_TIME";

		// default time (in minutes)
		private static int DEFAULT_TIME = Properties.Settings.Default.DEFAULT_TIMEOP;
		#endregion

		#region Attributes

		private Timer timer;
		private Int32 poolingTime = DEFAULT_TIME;

		private static EventLog eventLog = null;
		//private static ILog _logger = LogManager.GetLogger(typeof(OrdemProducao));
		private static ILog _logger = LogManager.GetLogger("OrdemProducao");
		#endregion

		public OrdemProducao()
		{
			InitializeComponent();

			//Create Log Event
			if (eventLog == null)
			{
				_logger.Info("Iniciando eventlog");

				// start a new one
				eventLog = new EventLog();

				// verify if exists source and create if does not exist
				if (!EventLog.SourceExists(EVENT_LOG_SOURCE))
				{
					EventLog.CreateEventSource(EVENT_LOG_SOURCE, EVENT_LOG_NAME);
				}
				eventLog.Source = EVENT_LOG_SOURCE;
				eventLog.ModifyOverflowPolicy(OverflowAction.OverwriteAsNeeded, 0);
				eventLog.MaximumKilobytes = 5120; //5Mb

			}

			//Set Timer Properly            
			timer = new Timer
			{
				Interval = DEFAULT_TIME * 60000
			};

			timer.Elapsed += new ElapsedEventHandler(OnTimer);

		}

		protected override void OnStart(string[] args)
		{
			Log("Service:" + this.ServiceName + " started", EventLogEntryType.Information);
			Log("Execution Parameters: Pooling Time = " + poolingTime.ToString(System.Globalization.CultureInfo.InvariantCulture) + "min.", EventLogEntryType.Information);
			Email.EnviaEmailPara("Iniciado serviço de integração", "Serviço de integração (ORDEM DE PRODUÇÃO) iniciado", "jarlisson.xavier@semptcl.com.br;andre.nascimento@semptcl.com.br");
			PutStatus(true);
			//Start timer
			timer.Start();
		}
		protected override void OnStop()
		{
			timer.Enabled = false;
			PutStatus(false);
			Log($"Service:{this.ServiceName} stopped", EventLogEntryType.Information);
			Email.EnviaEmailPara("Parado serviço de integração", "Serviço de integração (ORDEM DE PRODUÇÃO) parado", "jarlisson.xavier@semptcl.com.br;andre.nascimento@semptcl.com.br");
		}

		public void OnTimer(object sender, ElapsedEventArgs args)
		{
			Log("Execute: " + this.ServiceName, EventLogEntryType.Information);
			PutStatus(true);
			try
			{
				// call execute abstract method
				ExecuteService();
			}
			catch (System.Exception exc)
			{
				Log($"Error at {this.ServiceName} in execute: {exc.Message}", EventLogEntryType.Error);
			}
			finally
			{
				Log("Final Execute: " + this.ServiceName, EventLogEntryType.Information);
			}
		}

		/// <summary>
		/// Execute the service logic
		/// </summary>
		private void ExecuteService()
		{
			try
			{
				OrdemProducaoService job = new OrdemProducaoService();

				var ret = job.RunJob();

				if (ret.IsProblem())
				{
					Log("Error at " + this.ServiceName + " in execute: "
					   + ret.Msg, EventLogEntryType.Error);
				}
			}
			catch (System.Exception ex)
			{
				Log(ex.StackTrace, EventLogEntryType.Error);
			}
		}
		public void PutStatus(bool status)
		{

			try
			{
				//var gravar = BO.Integrator.Integrador.AtualizarStatus(status);
				/*if (gravar == false)
				{
					var destinatarios = Geral.ObterDestinatariosEmail("ApontamentoPCI");
					var email = String.Join("; ", destinatarios);
					Email.EnviaEmailPara("MonitroSAP: Erro ao gravar no banco ( APT ).", "Ocorreu um erro ao tentar gravar o status do integrador", email);
				}*/

			}
			catch (Exception ex)
			{
				Log(ex.Message, EventLogEntryType.Error);
				Email.EnviaEmailPara("MonitorSAP: Erro ao gravar no banco ( APT ) .", ex.Message, "");
			}


		}

		/// <summary>
		/// Log method
		/// </summary>
		/// <param name="msg">Message to log</param>
		/// <param name="type">Type of log message</param>
		private void Log(String msg, EventLogEntryType type)
		{
			if (type == EventLogEntryType.Error)
			{
				_logger.Error(msg);
			}
			else
			{
				_logger.Info(msg);
			}


			if (eventLog != null)
			{
				eventLog.WriteEntry(msg, type);
			}
		}
	}
}
