namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_ORDER")]
    public partial class ALC_ORDER
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ALC_ORDER()
        {
            ALC_COVER_LED = new HashSet<ALC_COVER_LED>();
            ALC_DOWNTIME = new HashSet<ALC_DOWNTIME>();
            ALC_ENGINE = new HashSet<ALC_ENGINE>();
            ALC_ENGINE1 = new HashSet<ALC_ENGINE>();
            ALC_LOT_LIST = new HashSet<ALC_LOT_LIST>();
            ALC_LOTE_LIST = new HashSet<ALC_LOTE_LIST>();
            ALC_MAGAZINE = new HashSet<ALC_MAGAZINE>();
            ALC_VOLUME = new HashSet<ALC_VOLUME>();
            ALC_PRODUCTION_COUNTER = new HashSet<ALC_PRODUCTION_COUNTER>();
            ALC_VOLUME1 = new HashSet<ALC_VOLUME>();
            ALC_PRODUCTION_COUNTER1 = new HashSet<ALC_PRODUCTION_COUNTER>();
        }

        [Key]
        [StringLength(20)]
        public string idOrder { get; set; }

        public DateTime? dtCreated { get; set; }

        public DateTime? dtEnd { get; set; }

        public DateTime? dtStart { get; set; }

        public int? PROD_QUANT { get; set; }

        public int quantity { get; set; }

        public int line_COD_LINHAS { get; set; }

        public int sku_COD_SKU { get; set; }

        public int? user_USER_ID { get; set; }

        [StringLength(20)]
        public string status { get; set; }

        [StringLength(5)]
        public string ppb_ppb { get; set; }

        [StringLength(40)]
        public string fullSku { get; set; }

        public int? CHECKED_QUANT { get; set; }

        public bool? IS_ENGINE { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? dt_created { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? dt_end { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? dt_start { get; set; }

        public int? TARGET_HOUR { get; set; }

        public bool? IS_COVER_LED { get; set; }

        public int? MAGANIZE_QUANT { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_COVER_LED> ALC_COVER_LED { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_DOWNTIME> ALC_DOWNTIME { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_ENGINE> ALC_ENGINE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_ENGINE> ALC_ENGINE1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_LOT_LIST> ALC_LOT_LIST { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_LOTE_LIST> ALC_LOTE_LIST { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_MAGAZINE> ALC_MAGAZINE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_VOLUME> ALC_VOLUME { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_PRODUCTION_COUNTER> ALC_PRODUCTION_COUNTER { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_VOLUME> ALC_VOLUME1 { get; set; }

        public virtual ALC_TARGET_HOUR ALC_TARGET_HOUR { get; set; }

        public virtual ALC_PPB ALC_PPB { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_PRODUCTION_COUNTER> ALC_PRODUCTION_COUNTER1 { get; set; }

        public virtual ALC_SYS_USER ALC_SYS_USER { get; set; }
    }
}
