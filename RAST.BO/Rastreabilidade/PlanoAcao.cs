﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.DTO;

namespace RAST.BO.Rastreabilidade
{
	public class PlanoAcao
	{

		#region Tipo de Causa

		public static List<CBPlanoAcaoTipoCausaDTO> ObterTipoCausaLista()
		{
			return daoPlanoAcao.ObterTipoCausaLista();
		}

		public static CBPlanoAcaoTipoCausaDTO ObterTipoCausa(int codTipoCausa)
		{
			return daoPlanoAcao.ObterTipoCausa(codTipoCausa);
		}


		public static void GravarTipoCausa(CBPlanoAcaoTipoCausaDTO dado)
		{
			daoPlanoAcao.GravarTipoCausa(dado);
		}

		public static void RemoverTipoCausa(int codTipoCausa)
		{
			daoPlanoAcao.RemoverTipoCausa(codTipoCausa);

		}

		#endregion

		#region Plano de Ação

		public static List<CBPlanoAcaoDTO> ObterPlanoAcaoLista(DateTime datInicio, DateTime datFim, int? flgStatus)
		{
			return daoPlanoAcao.ObterPlanoAcaoLista(datInicio, datFim, flgStatus);
		}

		public static CBPlanoAcaoDTO ObterPlanoAcao(int codPlano)
		{
			return daoPlanoAcao.ObterPlanoAcao(codPlano);
		}

		public static List<CBPlanoAcaoDTO> ObterPlanoAcao(int codLinha, DateTime datEvento)
		{
			return daoPlanoAcao.ObterPlanoAcao(codLinha, datEvento);
		}

		public static void GravarPlanoAcao(CBPlanoAcaoDTO dado)
		{
			if (dado.DatConclusao != null && dado.FlgStatus != 2)
			{
				dado.FlgStatus = 2;
			}

			if (dado.DatConclusao == null && dado.FlgStatus == 2)
			{
				dado.DatConclusao = DateTime.Parse(DateTime.Now.ToShortDateString());
			}

			daoPlanoAcao.GravarPlanoAcao(dado);
		}

		public static void RemoverPlanoAcao(int codPlano)
		{
			daoPlanoAcao.RemoverPlanoAcao(codPlano);

		}

		#endregion

		#region Plano de Ação - Causa

		public static List<CBPlanoAcaoCausaDTO> ObterPlanoAcaoCausaLista(int codPlano)
		{
			return daoPlanoAcao.ObterPlanoAcaoCausaLista(codPlano);
		}

		public static CBPlanoAcaoCausaDTO ObterPlanoAcaoCausa(int codPlano, int codPlanoCausa)
		{
			return daoPlanoAcao.ObterPlanoAcaoCausa(codPlano, codPlanoCausa);
		}

		public static void GravarPlanoAcaoCausa(CBPlanoAcaoCausaDTO dado)
		{
			daoPlanoAcao.GravarPlanoAcaoCausa(dado);
		}

		public static void RemoverPlanoAcaoCausa(int codPlano, int codPlanoCausa)
		{
			daoPlanoAcao.RemoverPlanoAcaoCausa(codPlano, codPlanoCausa);

		}

		#endregion

		#region Plano de Ação - Acompanhamento

		public static List<CBPlanoAcaoAcompanhamentoDTO> ObterPlanoAcaoAcompanhamentoLista(int codPlano, int codPlanoCausa)
		{
			return daoPlanoAcao.ObterPlanoAcaoAcompanhamentoLista(codPlano, codPlanoCausa);
		}

		public static CBPlanoAcaoAcompanhamentoDTO ObterPlanoAcaoAcompanhamento(int codPlano, int codPlanoCausa, int codAcompanha)
		{
			return daoPlanoAcao.ObterPlanoAcaoAcompanhamento(codPlano, codPlanoCausa, codAcompanha);
		}

		public static void GravarPlanoAcaoAcompanhamento(CBPlanoAcaoAcompanhamentoDTO dado)
		{
			daoPlanoAcao.GravarPlanoAcaoAcompanhamento(dado);
		}

		public static void RemoverPlanoAcaoAcompanhamento(int codPlano, int codPlanoCausa, int codAcompanha)
		{
			daoPlanoAcao.RemoverPlanoAcaoAcompanhamento(codPlano, codPlanoCausa, codAcompanha);

		}

		#endregion

	}
}
