namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_COMPILADOR
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FR_COMPILADOR()
        {
            FR_COMPILADOR_DATABASE = new HashSet<FR_COMPILADOR_DATABASE>();
        }

        [Key]
        [StringLength(80)]
        public string CPL_DESCRITOR { get; set; }

        [Required]
        [StringLength(350)]
        public string CPL_ESPECIFICACAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_COMPILADOR_DATABASE> FR_COMPILADOR_DATABASE { get; set; }
    }
}
