namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_DOC_ATOR
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FR_DOC_ATOR()
        {
            FR_DOC_CASO_USO_ATOR = new HashSet<FR_DOC_CASO_USO_ATOR>();
            FR_DOC_HIPOTESE_TRABALHO = new HashSet<FR_DOC_HIPOTESE_TRABALHO>();
        }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ATO_CODIGO { get; set; }

        [StringLength(255)]
        public string ATO_FREQUENCIA_USO { get; set; }

        [StringLength(255)]
        public string ATO_NIVEL_INSTRUCAO { get; set; }

        [StringLength(255)]
        public string ATO_NOME { get; set; }

        [StringLength(255)]
        public string ATO_PROFICIENCIA_APLICACAO { get; set; }

        [StringLength(255)]
        public string ATO_PROFICIENCIA_INFORMATICA { get; set; }

        public int? ATO_USUARIO { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DOC_CODIGO { get; set; }

        public virtual FR_DOC_PRINCIPAL FR_DOC_PRINCIPAL { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_CASO_USO_ATOR> FR_DOC_CASO_USO_ATOR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_HIPOTESE_TRABALHO> FR_DOC_HIPOTESE_TRABALHO { get; set; }
    }
}
