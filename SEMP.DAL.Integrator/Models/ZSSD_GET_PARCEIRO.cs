namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ZSSD_GET_PARCEIRO
    {
        public int id { get; set; }

        [Required]
        [StringLength(16)]
        public string PACKNO { get; set; }

        [StringLength(10)]
        public string EXTROW { get; set; }

        [StringLength(10)]
        public string CLIENTE { get; set; }

        [StringLength(4)]
        public string ORGANIZACAOVENDAS { get; set; }

        [StringLength(2)]
        public string CANALDISTRIBUICAO { get; set; }

        [StringLength(2)]
        public string SETORATIVIDADE { get; set; }

        [StringLength(2)]
        public string FUNCAOPARCEIRO { get; set; }

        [StringLength(10)]
        public string CLIENTEPARCEIRO { get; set; }

        [StringLength(10)]
        public string FORNECEDOR { get; set; }
    }
}
