namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_DEPARTMENT")]
    public partial class ALC_DEPARTMENT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ALC_DEPARTMENT()
        {
            ALC_DOWNTIME = new HashSet<ALC_DOWNTIME>();
        }

        [Key]
        public int ID_DEPARTMENT { get; set; }

        [StringLength(255)]
        public string DESCRIPTION { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_DOWNTIME> ALC_DOWNTIME { get; set; }
    }
}
