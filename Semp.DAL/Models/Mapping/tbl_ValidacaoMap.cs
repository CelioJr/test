using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_ValidacaoMap : EntityTypeConfiguration<tbl_Validacao>
    {
        public tbl_ValidacaoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodRotina, t.QtdValida, t.rowguid });

            // Properties
            this.Property(t => t.CodRotina)
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false);

            this.Property(t => t.CodChave)
                .HasMaxLength(100)
                .IsUnicode(false);

            this.Property(t => t.Status)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.NomUsuario)
                .HasMaxLength(30)
                .IsUnicode(false);

            this.Property(t => t.QtdValida)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NomUsuarioCad)
                .HasMaxLength(30)
                .IsUnicode(false);

            this.Property(t => t.NomEstacao)
                .HasMaxLength(30)
                .IsUnicode(false);

            this.Property(t => t.ExecComando)
                .HasMaxLength(6000)
                .IsUnicode(false);

            this.Property(t => t.ExecComandoRep)
                .HasMaxLength(6000)
                .IsUnicode(false);

            this.Property(t => t.flgOrdem)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_Validacao");
            this.Property(t => t.CodRotina).HasColumnName("CodRotina");
            this.Property(t => t.CodChave).HasColumnName("CodChave");
            this.Property(t => t.DatInicial).HasColumnName("DatInicial");
            this.Property(t => t.Descricao).HasColumnName("Descricao");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.DatStatus).HasColumnName("DatStatus");
            this.Property(t => t.NomUsuario).HasColumnName("NomUsuario");
            this.Property(t => t.QtdValida).HasColumnName("QtdValida");
            this.Property(t => t.TempoValida).HasColumnName("TempoValida");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
            this.Property(t => t.NomUsuarioCad).HasColumnName("NomUsuarioCad");
            this.Property(t => t.NomEstacao).HasColumnName("NomEstacao");
            this.Property(t => t.ExecComando).HasColumnName("ExecComando");
            this.Property(t => t.ExecComandoRep).HasColumnName("ExecComandoRep");
            this.Property(t => t.flgOrdem).HasColumnName("flgOrdem");
            this.Property(t => t.Observacao).HasColumnName("Observacao");
        }
    }
}
