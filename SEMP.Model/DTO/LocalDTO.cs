﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEMP.Model.DTO
{
	public class LocalDTO
	{
		[Key]
		[Column(TypeName = "char(3)")]
		public string CodLocal { get; set; }
		[Required]
		[Column(TypeName = "char(2)")]
		public string CodFab { get; set; }
		[Required]
		[Column(TypeName = "char(30)")]
		public string DesLocal { get; set; }
		[Required]
		[Column(TypeName = "char(1)")]
		public string FlgEndereco { get; set; }
		[Column(TypeName = "char(1)")]
		public string FlgAtivo { get; set; }
		[Column(TypeName = "char(1)")]
		public string FlgContabil { get; set; }
		[Column(TypeName = "char(1)")]
		public string FlgDisponivel { get; set; }
		[Column(TypeName = "char(1)")]
		public string FlgProdAcabado { get; set; }
		[Column("rowguid")]
		public Guid Rowguid { get; set; }
	}
}
