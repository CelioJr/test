﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.CNQ;
using SEMP.Model.DTO;

namespace RAST.BO.CNQ
{
	public class CustoRestrabalhoBO
	{

		public static List<CnqCustoRetDTO> ObterCustosRet(DateTime mesRef, string familia)
		{
			return daoCustoRetrabalho.ObterCustosRet(mesRef, familia);
		}

		public static CnqCustoRetDTO ObterCustoRet(DateTime mesRef, string familia, string codFab)
		{
			return daoCustoRetrabalho.ObterCustoRet(mesRef, familia, codFab);
		}

		public static void GravarCustoRef(CnqCustoRetDTO custoret)
		{
			daoCustoRetrabalho.GravarCustoRef(custoret);
		}

		public static void RemoverCustoRef(CnqCustoRetDTO custoret)
		{
			daoCustoRetrabalho.RemoverCustoRef(custoret);
		}

	}
}
