using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class CBLinhaObservacaoDTO
    {
        public int CodLinha { get; set; }
        public System.DateTime DatProducao { get; set; }
        public string Observacao { get; set; }
        public string NomUsuario { get; set; }
        public Nullable<System.DateTime> DatModificacao { get; set; }
    }
}
