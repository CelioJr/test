using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBPerfilAmarra
    {
        public int CodLinha { get; set; }
        public int NumPosto { get; set; }
        public string CodModelo { get; set; }
        public int CodTipoAmarra { get; set; }
        public string CodItem { get; set; }
    }
}
