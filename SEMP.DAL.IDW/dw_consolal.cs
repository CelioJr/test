namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolal
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_consolal()
        {
            dw_consolalmo = new HashSet<dw_consolalmo>();
            dw_consolaloco = new HashSet<dw_consolaloco>();
        }

        [Key]
        public long id_consolal { get; set; }

        public long id_consol { get; set; }

        public decimal? seg_auto_tempoalerta { get; set; }

        public decimal? seg_manu_tempoalerta { get; set; }

        public long id_talerta { get; set; }

        public virtual dw_consol dw_consol { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolalmo> dw_consolalmo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolaloco> dw_consolaloco { get; set; }

        public virtual dw_t_alerta dw_t_alerta { get; set; }
    }
}
