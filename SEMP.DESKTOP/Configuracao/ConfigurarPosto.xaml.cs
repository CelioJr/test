﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SEMP.BO;
using RAST.BO.Rastreabilidade;
using SEMP.Model.DTO;

namespace SISAP.Configuracao
{
    /// <summary>
    /// Interaction logic for ConfigurarPosto.xaml
    /// </summary>
    public partial class ConfigurarPosto : Window
    {
        Passagem passagem = null;
        public ConfigurarPosto(Passagem passagem)
        {
            this.passagem = passagem;
            InitializeComponent();
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            if (SalvarDados())
            {
                Close();
            }
        }

        private void btnOK_Cancelar(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OnMyComboBoxChanged(object sender, SelectionChangedEventArgs e)
        {
            string fabrica = (e.AddedItems[0] as ComboBoxItem).Content as string;

            var linhas = Geral.ObterLinhaPorFabrica(fabrica);

            cbLinha.ItemsSource = linhas.ToList();
            cbLinha.DisplayMemberPath = "DesLinha";
            cbLinha.SelectedValuePath = "CodLinha";
            
        }

        private void cbLinha_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {

            int codLinha = (e.AddedItems[0] as CBLinhaDTO).CodLinha;

            var postos = Geral.ObterPostoPorLinha(codLinha);

            cbPosto.ItemsSource = postos.ToList();
            cbPosto.DisplayMemberPath = "DesPosto";
            cbPosto.SelectedValuePath = "NumPosto";
        }

        private bool SalvarDados()
        {
            try
            {
                ComboBoxItem typeItem = null;
            string fabrica = "";
            int linha = 0, posto = 0;

            int codFabrica = 0;
            string numAP;

            bool retorno = true;

            if (cbFabrica.SelectedItem == null)
            {
                retorno = false;
                MessageBox.Show("Selecione uma família", "SISAP");
            }
            else if (cbLinha.SelectedItem == null)
            {
                retorno = false;
                MessageBox.Show("Selecione uma linha", "SISAP");
            }
            else if (cbPosto.SelectedItem == null)
            {
                retorno = false;
                MessageBox.Show("Selecione um posto", "SISAP");
            }
            else if (string.IsNullOrWhiteSpace(txtCodigoFabrica.Text))
            {
                retorno = false;
                MessageBox.Show("Informe o código da fábrica", "SISAP");
            }
            else if (string.IsNullOrWhiteSpace(txtNumAP.Text))
            {
                retorno = false;
                MessageBox.Show("Informe a AP", "SISAP");
            }
            else if (string.IsNullOrWhiteSpace(dtDataReferencia.SelectedDate.Value.ToString()))
            {
                retorno = false;
                MessageBox.Show("Informe uma data de referência", "SISAP");
            }
            else
            {

                DateTime DataRef = DateTime.Now;
                string validarAP = "";
                bool dataReferenciaValida = true;

                try
                {
                    if (dtDataReferencia.SelectedDate != null)
                    {
                        DataRef = dtDataReferencia.SelectedDate.Value;
                    }
                    else
                    {
                        dataReferenciaValida = false;
                    }
                }
                catch
                {
                    dataReferenciaValida = false;
                }


                //Crachá do supervisor
                //RIFuncionarioDTO funcionario = null;

                //if (dataReferenciaValida)
                //{
                //    string cracha = string.Empty;

                //    if (!string.IsNullOrWhiteSpace(amarra.CrachaSupervisor))
                //    {
                //        cracha = amarra.CrachaSupervisor.Substring(0, 11);
                //    }

                //    funcionario = Login.ObterFuncionarioByCPF(cracha);
                //    DateTime dataDeHoje = DateTime.Now;

                //    if (DataRef.Date < dataDeHoje.Date)
                //    {
                //        if ((funcionario == null || String.IsNullOrEmpty(funcionario.CodDRT)) && funcionario.CodCargo != "79")
                //        {
                //            validarCrachaSupervisor = false;
                //        }
                //    }
                //}

                typeItem = (ComboBoxItem)cbFabrica.SelectedItem;
                fabrica = typeItem.Content.ToString();

                CBLinhaDTO tbl_cbLinha = (CBLinhaDTO)cbLinha.SelectedItem;
                linha = tbl_cbLinha.CodLinha;

                CBPostoDTO tbl_cbPosto = (CBPostoDTO)cbPosto.SelectedItem;
                posto = tbl_cbPosto.NumPosto;

                numAP = txtNumAP.Text;
                codFabrica = int.Parse(txtCodigoFabrica.Text);

                validarAP = Geral.ValidarAP(codFabrica.ToString("00"), numAP, DataRef, tbl_cbLinha);

                if (dataReferenciaValida == true && string.IsNullOrWhiteSpace(validarAP))
                {
                    string codModelo = Geral.ObterCodigoModelo(codFabrica.ToString("00"), int.Parse(numAP).ToString("000000"));
                    string descItem = Geral.ObterDescricaoItem(codModelo);

                    passagem.Modelo = codModelo;
                    passagem.NumAP = int.Parse(numAP).ToString("000000");
                    passagem.CodLinha = tbl_cbLinha.CodLinha;
                    passagem.NumPosto = tbl_cbPosto.NumPosto;
                    passagem.CodFab = int.Parse(txtCodigoFabrica.Text).ToString("00");

                    passagem.SetarPosto();
                    passagem.SetarPassagem();
                    passagem.SetarTotalProduzido();
                    passagem.SetarUltimasLeituras();

                    passagem.lbEstacao.Content = tbl_cbLinha.DesLinha + " | " + tbl_cbPosto.DesPosto;
                    passagem.lbAP.Content = txtCodigoFabrica.Text + " - " + txtNumAP.Text;
                    passagem.lbModelo.Content = codModelo+" - "+descItem;

                    SEMP.DESKTOP.Properties.Settings.Default.fabrica = fabrica;
                    SEMP.DESKTOP.Properties.Settings.Default.linha = linha.ToString();
                    SEMP.DESKTOP.Properties.Settings.Default.posto = posto.ToString();
                }
                else
                {
                    if (!dataReferenciaValida)
                    {
                        MessageBox.Show("Data de referência inválida.","ERRO");
                    }
                    else if (!string.IsNullOrWhiteSpace(validarAP))
                    {
                        MessageBox.Show(validarAP,"ERRO");
                    }
                }

            }


            return retorno;
            }
            catch
            {
                MessageBox.Show("ERRO AO SALVAR POSTO","SISAP");
                return false;
            }
        }

    }
}
