﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
	public class ProducaoData
	{
		[Key]
		[Column(Order = 1)] 
		public int CodLinha { get; set; }
		public String DesLinha { get; set; }
		[Key]
		[Column(Order = 2)]
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }

		[Key]
		[Column(Order = 3)] 
		public int NumPosto { get; set; }
		public String DesPosto { get; set; }
		public int SeqPosto { get; set; }
		public int FlgGargalo { get; set; }

		[Key]
		[Column(Order = 4)] 
		public DateTime Data { get; set; }
		public DateTime DataFim { get; set; }

		public int QtdDias { get; set; }

		public decimal QtdMeta { get; set; }
		public int QtdProduzido { get; set; }
		public decimal QtdPlano { get; set; }

		public string Cor
		{
			get
			{
				switch (FlgGargalo)
				{
					case 2:
						return "Red";
					case 1:
						return "cadetblue";
					default:
						return "Aquamarine";
				}
			}
		}
	}
}
