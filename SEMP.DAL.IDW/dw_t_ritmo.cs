namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_t_ritmo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_t_ritmo()
        {
            dw_consolvaritmo = new HashSet<dw_consolvaritmo>();
            dw_consolvaritmologcau = new HashSet<dw_consolvaritmologcau>();
            om_cfg = new HashSet<om_cfg>();
        }

        [Key]
        public long id_tritmo { get; set; }

        [StringLength(60)]
        public string cd_tritmo { get; set; }

        public int? revisao { get; set; }

        [StringLength(100)]
        public string ds_tritmo { get; set; }

        public short? st_ativo { get; set; }

        public DateTime? dthr_stativo { get; set; }

        public DateTime? dthr_revisao { get; set; }

        public long id_usrStativo { get; set; }

        public long id_usrrevisao { get; set; }

        public long id_tppt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolvaritmo> dw_consolvaritmo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolvaritmologcau> dw_consolvaritmologcau { get; set; }

        public virtual om_tppt om_tppt { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg { get; set; }
    }
}
