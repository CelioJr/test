using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_LSAAPModeloMap : EntityTypeConfiguration<tbl_LSAAPModelo>
    {
        public tbl_LSAAPModeloMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.NumAP, t.CodModelo, t.CodModeloApont, t.CodProcesso });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NumAP)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodModeloApont)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodProcesso)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.FlgApontar)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.CodLocalCRE)
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.CodLocalDEB)
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.CodLinha)
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_LSAAPModelo");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.NumAP).HasColumnName("NumAP");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.CodModeloApont).HasColumnName("CodModeloApont");
            this.Property(t => t.CodProcesso).HasColumnName("CodProcesso");
            this.Property(t => t.QtdEmbalado).HasColumnName("QtdEmbalado");
            this.Property(t => t.QtdApontado).HasColumnName("QtdApontado");
            this.Property(t => t.QtdRejeitado).HasColumnName("QtdRejeitado");
            this.Property(t => t.Nivel).HasColumnName("Nivel");
            this.Property(t => t.Ordem).HasColumnName("Ordem");
            this.Property(t => t.QtdItens).HasColumnName("QtdItens");
            this.Property(t => t.FlgApontar).HasColumnName("FlgApontar");
            this.Property(t => t.CodLocalCRE).HasColumnName("CodLocalCRE");
            this.Property(t => t.CodLocalDEB).HasColumnName("CodLocalDEB");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.QtdNecessidade).HasColumnName("QtdNecessidade");
            this.Property(t => t.QtdSaldoAnt).HasColumnName("QtdSaldoAnt");
        }
    }
}
