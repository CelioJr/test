using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
	public class tbl_LinhaMap : EntityTypeConfiguration<tbl_Linha>
	{
		public tbl_LinhaMap()
		{
			// Primary Key
			this.HasKey(t => t.CodLinha);

			// Properties
			this.Property(t => t.CodLinha)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(8)
                .IsUnicode(false);

			this.Property(t => t.DesDep)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(30)
                .IsUnicode(false);

			this.Property(t => t.DesLinha)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(30)
                .IsUnicode(false);

			this.Property(t => t.Usuario)
				.IsFixedLength()
				.HasMaxLength(25)
                .IsUnicode(false);

			this.Property(t => t.FlgTipo)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			this.Property(t => t.FlgAtivo)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			// Table & Column Mappings
			this.ToTable("tbl_Linha");
			this.Property(t => t.CodLinha).HasColumnName("CodLinha");
			this.Property(t => t.DesDep).HasColumnName("DesDep");
			this.Property(t => t.DesLinha).HasColumnName("DesLinha");
			this.Property(t => t.Usuario).HasColumnName("Usuario");
			this.Property(t => t.rowguid).HasColumnName("rowguid");
			this.Property(t => t.FlgTipo).HasColumnName("FlgTipo");
			this.Property(t => t.FlgAtivo).HasColumnName("FlgAtivo");
		}
	}
}
