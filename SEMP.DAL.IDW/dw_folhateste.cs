namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_folhateste
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_folhateste()
        {
            dw_testesub = new HashSet<dw_testesub>();
        }

        [Key]
        public long id_folhateste { get; set; }

        public long id_folha { get; set; }

        public long id_gt { get; set; }

        public long id_produto { get; set; }

        public int? frequenciaHz { get; set; }

        public decimal? tensao_min { get; set; }

        public decimal? tensao_nom { get; set; }

        public decimal? tensao_max { get; set; }

        public virtual dw_folha dw_folha { get; set; }

        public virtual om_gt om_gt { get; set; }

        public virtual om_produto om_produto { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_testesub> dw_testesub { get; set; }
    }
}
