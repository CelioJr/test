namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_garantia
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_garantia()
        {
            om_garpro = new HashSet<om_garpro>();
        }

        [Key]
        public long id_garantia { get; set; }

        [StringLength(30)]
        public string cd_garantia { get; set; }

        public int? revisao { get; set; }

        public DateTime? dthr_revisao { get; set; }

        public DateTime? dthr_stativo { get; set; }

        public short? st_ativo { get; set; }

        [StringLength(256)]
        public string ds_garantia { get; set; }

        public long? id_contato { get; set; }

        [StringLength(10)]
        public string doc_cliente { get; set; }

        public DateTime? dthr_iniciovalidade { get; set; }

        public DateTime? dthr_fimvalidade { get; set; }

        [StringLength(20)]
        public string vl_licencas { get; set; }

        [StringLength(20)]
        public string vl_contratomanu { get; set; }

        public virtual om_contato om_contato { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_garpro> om_garpro { get; set; }
    }
}
