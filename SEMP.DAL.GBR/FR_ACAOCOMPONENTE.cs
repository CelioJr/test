namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_ACAOCOMPONENTE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FR_ACAOCOMPONENTE()
        {
            FR_PARAMETRO = new HashSet<FR_PARAMETRO>();
        }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FRM_CODIGO { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int COM_CODIGO { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ACO_CODIGO { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(30)]
        public string ACC_MOMENTO { get; set; }

        [StringLength(30)]
        public string ACC_CONDICAO { get; set; }

        public virtual FR_ACAO FR_ACAO { get; set; }

        public virtual FR_COMPONENTE FR_COMPONENTE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_PARAMETRO> FR_PARAMETRO { get; set; }
    }
}
