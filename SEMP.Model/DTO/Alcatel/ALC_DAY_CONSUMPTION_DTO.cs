using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_DAY_CONSUMPTION_DTO
    {
        public decimal id_consumption { get; set; }
        public Nullable<System.DateTime> dtCreated { get; set; }
        public Nullable<decimal> energy { get; set; }
        public bool seal { get; set; }
        public Nullable<decimal> water { get; set; }
        public Nullable<int> people { get; set; }
        public Nullable<decimal> energyDiff { get; set; }
        public Nullable<decimal> waterDiff { get; set; }
    }
}
