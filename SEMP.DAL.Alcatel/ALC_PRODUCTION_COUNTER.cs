namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_PRODUCTION_COUNTER")]
    public partial class ALC_PRODUCTION_COUNTER
    {
        [Key]
        public int idCounter { get; set; }

        public DateTime? dtPallet { get; set; }

        [StringLength(20)]
        public string order_idOrder { get; set; }

        public int? product_COD_SMT { get; set; }

        public int? RESP_PALLET { get; set; }

        public int? line_COD_LINHAS { get; set; }

        [StringLength(255)]
        public string packing { get; set; }

        [StringLength(255)]
        public string volume { get; set; }

        [StringLength(25)]
        public string ID_LOTE { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? dt_pallet { get; set; }

        public virtual ALC_ORDER ALC_ORDER { get; set; }

        public virtual ALC_ORDER ALC_ORDER1 { get; set; }

        public virtual ALC_VOLUME ALC_VOLUME { get; set; }

        public virtual ALC_SYS_USER ALC_SYS_USER { get; set; }
    }
}
