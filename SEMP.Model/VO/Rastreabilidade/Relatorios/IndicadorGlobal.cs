﻿namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
	public class IndicadorGlobal
	{
		public int Indicador { get; set; }
		public string Titulo { get; set; }
		public decimal RealIAC { get; set; }
		public decimal PlanIAC { get; set; }
		public decimal RealIMC { get; set; }
		public decimal PlanIMC { get; set; }
		public decimal RealMF { get; set; }
		public decimal PlanMF { get; set; }

		public IndicadorGlobal(){}

		public IndicadorGlobal(int indicador, string titulo) 
		{
			Indicador = indicador;
			Titulo = titulo;
		}
	}
}
