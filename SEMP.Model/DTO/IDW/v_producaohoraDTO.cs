﻿using System;

namespace SEMP.Model.DTO
{
	public partial class v_producaohoraDTO
	{
		public Nullable<DateTime> dthr_ihora { get; set; }
		public Nullable<DateTime> dthr_fhora { get; set; }
		public string cd_pt { get; set; }
		public string cd_cp { get; set; }
		public string cd_folha { get; set; }
		public Nullable<long> producaobruta { get; set; }

	}
}
