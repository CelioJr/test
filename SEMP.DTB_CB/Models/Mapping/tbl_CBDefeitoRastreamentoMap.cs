using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DTB_CB.Models.Mapping
{
    public class tbl_CBDefeitoRastreamentoMap : EntityTypeConfiguration<tbl_CBDefeitoRastreamento>
    {
        public tbl_CBDefeitoRastreamentoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.NumECB, t.CodLinha, t.NumPosto, t.CodDefeito, t.DatEvento });

            // Properties
            this.Property(t => t.NumECB)
                .IsRequired()
                .HasMaxLength(40);

            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumPosto)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodDefeito)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5);

            this.Property(t => t.NomUsuario)
                .HasMaxLength(30);

            //this.Property(t => t.CodDRT)
            //    .HasMaxLength(8);

            // Table & Column Mappings
            this.ToTable("tbl_CBDefeitoRastreamento");
            this.Property(t => t.NumECB).HasColumnName("NumECB");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.NumPosto).HasColumnName("NumPosto");
            this.Property(t => t.CodDefeito).HasColumnName("CodDefeito");
            this.Property(t => t.DatEvento).HasColumnName("DatEvento");
            this.Property(t => t.NomUsuario).HasColumnName("NomUsuario");
            //this.Property(t => t.CodDRT).HasColumnName("CodDRT");
        }
    }
}
