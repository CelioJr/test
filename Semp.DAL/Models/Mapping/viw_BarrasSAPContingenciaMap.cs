﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
	public class viw_BarrasSAPContingenciaMap : EntityTypeConfiguration<viw_BarrasSAPContingencia>
	{
		public viw_BarrasSAPContingenciaMap()
		{
			// Primary Key
			this.HasKey(t => new { t.AUFNR, t.MATNR1, t.SERIAL1 });

			// Properties
			this.Property(t => t.Werks)
				.HasMaxLength(4);

			this.Property(t => t.AUART)
				.HasMaxLength(4);

			this.Property(t => t.AUFNR)
				.IsRequired()
				.HasMaxLength(12);

			this.Property(t => t.MATNR1)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(6);

			this.Property(t => t.EAN13)
				.IsFixedLength()
				.HasMaxLength(4);

			this.Property(t => t.SERIAL1)
				.IsRequired()
				.HasMaxLength(21);

			this.Property(t => t.CREDATE1)
				.HasMaxLength(10);

			this.Property(t => t.MATNR2)
				.IsFixedLength()
				.HasMaxLength(6);

			this.Property(t => t.SERIAL2)
				.IsFixedLength()
				.HasMaxLength(14);

			this.Property(t => t.CREDATE2)
				.HasMaxLength(10);

			this.Property(t => t.PRUEFLOS)
				.HasMaxLength(1);

			this.Property(t => t.CRETIME2)
				.IsFixedLength()
				.HasMaxLength(10);

			this.Property(t => t.CRETIME1)
				.IsFixedLength()
				.HasMaxLength(10);

			this.Property(t => t.ARBPL)
				.HasMaxLength(3);

			this.Property(t => t.CODLINHA)
				.IsFixedLength()
				.HasMaxLength(4);

			this.Property(t => t.DRT)
				.IsFixedLength()
				.HasMaxLength(8);

			this.Property(t => t.STATCONF)
				.HasMaxLength(1);

			this.Property(t => t.DTCONF)
				.HasMaxLength(1);

			this.Property(t => t.MBLNR)
				.HasMaxLength(1);

			this.Property(t => t.STATVD)
				.HasMaxLength(1);

			this.Property(t => t.STBCKFLUSH)
				.HasMaxLength(1);

			this.Property(t => t.DTBCKFLUSH)
				.HasMaxLength(1);

			this.Property(t => t.USERNAME)
				.IsFixedLength()
				.HasMaxLength(30);

			this.Property(t => t.SISAP)
				.HasMaxLength(1);

			this.Property(t => t.PACKNO)
				.HasMaxLength(11);

			this.Property(t => t.VCODE)
				.HasMaxLength(1);

			this.Property(t => t.OBA)
				.HasMaxLength(1);

			// Table & Column Mappings
			this.ToTable("viw_BarrasSAPContingencia");
			this.Property(t => t.Werks).HasColumnName("Werks");
			this.Property(t => t.AUART).HasColumnName("AUART");
			this.Property(t => t.AUFNR).HasColumnName("AUFNR");
			this.Property(t => t.MATNR1).HasColumnName("MATNR1");
			this.Property(t => t.EAN13).HasColumnName("EAN13");
			this.Property(t => t.SERIAL1).HasColumnName("SERIAL1");
			this.Property(t => t.CREDATE1).HasColumnName("CREDATE1");
			this.Property(t => t.MATNR2).HasColumnName("MATNR2");
			this.Property(t => t.SERIAL2).HasColumnName("SERIAL2");
			this.Property(t => t.CREDATE2).HasColumnName("CREDATE2");
			this.Property(t => t.PRUEFLOS).HasColumnName("PRUEFLOS");
			this.Property(t => t.CRETIME2).HasColumnName("CRETIME2");
			this.Property(t => t.CRETIME1).HasColumnName("CRETIME1");
			this.Property(t => t.ARBPL).HasColumnName("ARBPL");
			this.Property(t => t.CODLINHA).HasColumnName("CODLINHA");
			this.Property(t => t.CODPOSTO).HasColumnName("CODPOSTO");
			this.Property(t => t.DRT).HasColumnName("DRT");
			this.Property(t => t.TPAMAR).HasColumnName("TPAMAR");
			this.Property(t => t.GESME).HasColumnName("GESME");
			this.Property(t => t.STATCONF).HasColumnName("STATCONF");
			this.Property(t => t.DTCONF).HasColumnName("DTCONF");
			this.Property(t => t.MBLNR).HasColumnName("MBLNR");
			this.Property(t => t.STATVD).HasColumnName("STATVD");
			this.Property(t => t.STBCKFLUSH).HasColumnName("STBCKFLUSH");
			this.Property(t => t.DTBCKFLUSH).HasColumnName("DTBCKFLUSH");
			this.Property(t => t.USERNAME).HasColumnName("USERNAME");
			this.Property(t => t.SISAP).HasColumnName("SISAP");
			this.Property(t => t.PACKNO).HasColumnName("PACKNO");
			this.Property(t => t.VCODE).HasColumnName("VCODE");
			this.Property(t => t.OBA).HasColumnName("OBA");
			this.Property(t => t.DatReferencia).HasColumnName("DatReferencia");
		}
	}
}
