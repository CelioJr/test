namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolallog
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_consolallog()
        {
            dw_consolaloco = new HashSet<dw_consolaloco>();
        }

        [Key]
        public long id_consolallog { get; set; }

        public DateTime? dthr_ialerta { get; set; }

        public int? ms_dthrialerta { get; set; }

        public DateTime? dthr_falerta { get; set; }

        public int? ms_dthrfalerta { get; set; }

        public decimal? seg_auto_tempoalerta { get; set; }

        public decimal? seg_manu_tempoalerta { get; set; }

        public long? id_pt { get; set; }

        public long? id_talerta { get; set; }

        [StringLength(256)]
        public string obs { get; set; }

        public long? id_usr { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolaloco> dw_consolaloco { get; set; }

        public virtual om_usr om_usr { get; set; }
    }
}
