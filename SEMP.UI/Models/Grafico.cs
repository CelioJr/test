﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using DotNet.Highcharts;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;

namespace SEMP.Models
{
	public class Grafico
	{
		/// <summary>
		///  Método que gera um gráfico de barras.
		/// </summary>
		/// <param name="data">Parâmetro data, contém as informações que serão geradas.</param>
		/// <param name="categories">Parâmetro categories contém o nome das categorias.</param>
		/// <param name="titulo">Parâmetro titutlo recebe o nome do gráfico que será gerado.</param>
		/// <returns>Retorna o objeto do tipo Highcharts</returns>
		public static Highcharts GerarGraficoBarra(Data data, string[] categories, string titulo, string idgrafico, int largura = 320, int altura = 300)
		{
			try
			{
				if (String.IsNullOrEmpty(idgrafico))
				{
					idgrafico = "chart";
				}
				Highcharts chart = new Highcharts(idgrafico)
					.InitChart(new Chart { DefaultSeriesType = ChartTypes.Column, Height = altura, Width = largura })
					.SetTitle(new Title { Text = titulo })
					.SetXAxis(new XAxis { Categories = categories })
					.SetYAxis(new YAxis { Title = new YAxisTitle { Text = "" } })
					.SetLegend(new Legend { Enabled = false })
					.SetTooltip(new Tooltip { Formatter = "TooltipFormatter" })
					.SetPlotOptions(new PlotOptions
					{
						Column = new PlotOptionsColumn
						{
							Cursor = Cursors.Pointer,
							Point = new PlotOptionsColumnPoint { Events = new PlotOptionsColumnPointEvents { Click = "ColumnPointClick", MouseOver = "MouseOver", MouseOut = "MouseOut" } },
							DataLabels = new PlotOptionsColumnDataLabels
							{
								Enabled = true,
								Color = Color.FromName("colors[1]"),
								Formatter = "function() { return this.y +''; }",
								Style = "fontWeight: 'bold'"
							}
						}
					})
					.SetSeries(new Series
					{
						Name = titulo,
						Data = data,
						Color = Color.White
					})
					.SetExporting(new Exporting { Enabled = true })
					.AddJavascripFunction(
						"TooltipFormatter",
						@"var point = this.point, s = this.x +':<b>'+ this.y +'</b><br/>';
					  if (point.drilldown) {
						s += 'Click to view '+ point.category +' versions';
					  } else {
						s += '';
					  }
					  return s;"
					)
					.AddJavascripFunction(
						"ColumnPointClick",
						@"var drilldown = this.drilldown;
					  if (drilldown) { // drill down
						setChart(drilldown.name, drilldown.categories, drilldown.data.data, drilldown.color);
					  } else { // restore
						setChart(name, categories, data.data);
					  }"
					)
					.AddJavascripFunction(
						"setChart",
						@"chart.xAxis[0].setCategories(categories);
					  chart.series[0].remove();
					  chart.addSeries({
						 name: name,
						 data: data,
						 color: color || 'white'
					  });",
						"name", "categories", "data", "color"
					)
					.AddJavascripFunction(
						"MouseOver",
						@"alterafundo(this.category);"
					)
					.AddJavascripFunction(
						"MouseOut",
						@"voltafundo(this.category);"
					)
					.AddJavascripVariable("colors", "Highcharts.getOptions().colors")
					.AddJavascripVariable("categories", JsonSerializer.Serialize(categories))
					.AddJavascripVariable("data", JsonSerializer.Serialize(data));

				return chart;

			}
			catch (Exception ex)
			{
				//Log.AppLog().ErrorException("\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name, ex);
				Log.AppLog().Error(ex, "\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name);
			}

			return null;

		}

		// Gera gráfico com duas séries de dados
		public static Highcharts GerarGraficoBarra(Data data1, Data data2, string[] categories, string titulo1, string titulo2, string idgrafico, int largura = 320, int altura = 300, Color? cor1 = null, Color? cor2 = null)
		{
			try
			{
				if (String.IsNullOrEmpty(idgrafico))
				{
					idgrafico = "chart";
				}
				Highcharts chart = new Highcharts(idgrafico)
					.InitChart(new Chart { DefaultSeriesType = ChartTypes.Column, Height = altura, Width = largura })
					.SetTitle(new Title { Text = titulo1 })
					.SetXAxis(new XAxis { Categories = categories })
					.SetYAxis(new YAxis { Title = new YAxisTitle { Text = "" } })
					.SetLegend(new Legend { Enabled = true, Align = HorizontalAligns.Right, VerticalAlign = VerticalAligns.Bottom })
					.SetTooltip(new Tooltip
					{
						//Formatter = @"function() { return ' '+ this.x +': <b>'+ this.y + '</b><br/>' ; }",
						HeaderFormat = "<span style=\"font-size:10px\">{point.x}</span><table>",
						PointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td>" + 
										"<td style=\"padding:0\"><b>{point.y} </b></td></tr>",
						FooterFormat = "</table>",
						Shared = true,
						UseHTML = true
					})
					.SetPlotOptions(new PlotOptions
					{
						Column = new PlotOptionsColumn
						{
							Cursor = Cursors.Pointer,
							Point = new PlotOptionsColumnPoint { Events = new PlotOptionsColumnPointEvents { MouseOver = "MouseOver", MouseOut = "MouseOut" } },
							DataLabels = new PlotOptionsColumnDataLabels
							{
								Enabled = true/*,
								Color = Color.FromName("colors[0]"),
								Formatter = "function() { return this.y +''; }",
								Style = "fontWeight: 'bold'"*/
							}
						}
					})
					.SetSeries(new[]
					{
						new Series { Name = titulo1, Data = data1, Color = cor1 ?? Color.Red },
						new Series { Name = titulo2, Data = data2, Color = cor2 ?? Color.Tomato }
					})
					.SetExporting(new Exporting { Enabled = true })
					.AddJavascripFunction(
						"setChart",
						@"chart.xAxis[0].setCategories(categories);
					  chart.series[0].remove();
					  chart.addSeries({
						 name: name,
						 data: data,
						 color: color || 'white'
					  });",
						"name", "categories", "data", "color"
					)
					.AddJavascripFunction(
						"MouseOver",
						@"alterafundo(this.category);"
					)
					.AddJavascripFunction(
						"MouseOut",
						@"voltafundo(this.category);"
					)
					.AddJavascripVariable("colors", "Highcharts.getOptions().colors")
					.AddJavascripVariable("categories", JsonSerializer.Serialize(categories))
					.AddJavascripVariable("data", JsonSerializer.Serialize(data1));

				return chart;

			}
			catch
			{
			}

			return null;

		}

		/// <summary>
		/// Método que gera um gráfico de pizza
		/// </summary>
		/// <param name="data">Parâmetro data, contém as informações que serão geradas.</param>
		/// <param name="categories">Parâmetro categories contém o nome das categorias.</param>
		/// <param name="titulo">Parâmetro titutlo recebe o nome do gráfico que será gerado.</param>
		/// <returns>Retorna o objeto do tipo Highcharts</returns>
		public static Highcharts GerarGraficoPizza(Data data, string[] categories, string titulo)
		{

			try
			{
				Highcharts chart = new Highcharts("chart")
					.InitChart(new Chart { PlotShadow = false })
					.SetTitle(new Title { Text = titulo })
					.SetTooltip(new Tooltip { Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.y +''; }" })
					.SetPlotOptions(new PlotOptions
					{
						Pie = new PlotOptionsPie
						{
							AllowPointSelect = true,
							Cursor = Cursors.Pointer,
							DataLabels = new PlotOptionsPieDataLabels
							{
								Color = ColorTranslator.FromHtml("#000000"),
								ConnectorColor = ColorTranslator.FromHtml("#000000"),
								Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.y +''; }"
							}
						}
					})
					.SetSeries(new Series
					{
						Type = ChartTypes.Pie,
						Name = titulo,
						Data = data
					});

				return chart;
			}
			catch (Exception ex)
			{
				//Log.AppLog().ErrorException("\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name, ex);
				Log.AppLog().Error(ex, "\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name);
			}
			return null;
		}


		/// <summary>
		///  Método que gera um gráfico de barras 3d.
		/// </summary>
		/// <param name="data">Parâmetro data, contém as informações que serão geradas.</param>
		/// <param name="categories">Parâmetro categories contém o nome das categorias.</param>
		/// <param name="titulo">Parâmetro titutlo recebe o nome do gráfico que será gerado.</param>
		/// <returns>Retorna o objeto do tipo Highcharts</returns>
		public static Highcharts GerarGraficoBarra3d(Data data, string[] categories, string titulo, string idgrafico)
		{
			try
			{
				if (String.IsNullOrEmpty(idgrafico))
				{
					idgrafico = "chart";
				}
				Highcharts chart = new Highcharts(idgrafico)
					.InitChart(new Chart
					{
						Type = ChartTypes.Column,
						Margin = new[]{75},
						Options3d = new ChartOptions3d
						{
							Enabled = true,
							Alpha = 15,
							Beta = 15,
							Depth = 50,
							ViewDistance = 25
						}
					})
					.SetTitle(new Title { Text = titulo })
					.SetXAxis(new XAxis { Categories = categories })
					.SetYAxis(new YAxis { Title = new YAxisTitle { Text = "" } })
					.SetLegend(new Legend { Enabled = false })
					.SetTooltip(new Tooltip { Formatter = "TooltipFormatter" })
					.SetPlotOptions(new PlotOptions
					{
						Column = new PlotOptionsColumn
						{
							Cursor = Cursors.Pointer,
							Point = new PlotOptionsColumnPoint { Events = new PlotOptionsColumnPointEvents { Click = "ColumnPointClick" } },
							DataLabels = new PlotOptionsColumnDataLabels
							{
								Enabled = true,
								Color = Color.FromName("colors[0]"),
								Formatter = "function() { return this.y +''; }",
								Style = "fontWeight: 'bold'"
							}
						}
					})
					.SetSeries(new Series
					{
						Name = titulo,
						Data = data,
						Color = Color.White
					})
					.SetExporting(new Exporting { Enabled = true })
					.AddJavascripFunction(
						"TooltipFormatter",
						@"var point = this.point, s = this.x +':<b>'+ this.y +'</b><br/>';
					  if (point.drilldown) {
						s += 'Click to view '+ point.category +' versions';
					  } else {
						s += '';
					  }
					  return s;"
					)
					.AddJavascripFunction(
						"ColumnPointClick",
						@"var drilldown = this.drilldown;
					  if (drilldown) { // drill down
						setChart(drilldown.name, drilldown.categories, drilldown.data.data, drilldown.color);
					  } else { // restore
						setChart(name, categories, data.data);
					  }"
					)
					.AddJavascripFunction(
						"setChart",
						@"chart.xAxis[0].setCategories(categories);
					  chart.series[0].remove();
					  chart.addSeries({
						 name: name,
						 data: data,
						 color: color || 'white'
					  });",
						"name", "categories", "data", "color"
					)
					.AddJavascripVariable("colors", "Highcharts.getOptions().colors")
					.AddJavascripVariable("categories", JsonSerializer.Serialize(categories))
					.AddJavascripVariable("data", JsonSerializer.Serialize(data));

				return chart;

			}
			catch (Exception ex)
			{
				//Log.AppLog().ErrorException("\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name, ex);
				Log.AppLog().Error(ex, "\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name);
			}

			return null;

		}


		// Gera gráfico de linha com duas séries de dados 
		public static Highcharts GerarGraficoLinha(List<DadosGrafico> dados, string[] categories, string tituloGrafico, string idgrafico, int largura = 320, int altura = 300, string tituloX = "", string tituloY = "")
		{
			try
			{
				var series = dados.Select(registro => new Series() {Name = registro.Titulo, Data = registro.Dados, Color = registro.Cor}).ToArray();

				if (String.IsNullOrEmpty(idgrafico))
				{
					idgrafico = "chart";
				}
				Highcharts chart = new Highcharts(idgrafico)
					.InitChart(new Chart
					{
						DefaultSeriesType = ChartTypes.Line, 
						Height = altura, 
						Width = largura,
						ClassName = "chart"
					})
					.SetTitle(new Title {Text = tituloGrafico})
					.SetXAxis(new XAxis {Categories = categories, 
											Title = new XAxisTitle()
											{
												Text = tituloX
											}
										})
					.SetYAxis(new YAxis
					{
						Title = new YAxisTitle { Text = tituloY }
					})
					.SetTooltip(new Tooltip
					{
						Formatter = @"function() {
										return '<b>'+ this.series.name +'</b><br/>'+
									this.x +':00 : '+ this.y +'%';
								}"
					})/*SetTooltip(new Tooltip
					{
						HeaderFormat = "<span style=\"font-size:10px\">{point.x}</span><table>",
						PointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td>" +
									  "<td style=\"padding:0\"><b>{point.y} </b></td></tr>",
						FooterFormat = "</table>",
						Shared = true,
						UseHTML = true
					})*/
					.SetSeries(series);

				return chart;

			}
			catch (Exception ex)
			{
				//Log.AppLog().ErrorException("\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name, ex);
				Log.AppLog().Error(ex, "\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name);
			}

			return null;

		}

		// Gera gráfico de linha com duas séries de dados 
		public static Highcharts GerarGraficoLinhaPPM(List<DadosGrafico> dados, string[] categories, string tituloGrafico, string idgrafico, int largura = 320, int altura = 300, string tituloX = "", string tituloY = "")
		{
			try
			{
				var series = dados.Select(registro => new Series() { Name = registro.Titulo, Data = registro.Dados, Color = registro.Cor }).ToArray();

				if (String.IsNullOrEmpty(idgrafico))
				{
					idgrafico = "chart";
				}
				Highcharts chart = new Highcharts(idgrafico)
					.InitChart(new Chart
					{
						DefaultSeriesType = ChartTypes.Line,
						Height = altura,
						Width = largura,
						ClassName = "chart"
					})
					.SetTitle(new Title { Text = tituloGrafico })
					.SetXAxis(new XAxis
					{
						Categories = categories,
						Title = new XAxisTitle()
						{
							Text = tituloX
						}
					})
					.SetYAxis(new YAxis
					{
						Title = new YAxisTitle { Text = tituloY }
					})
					.SetTooltip(new Tooltip
					{
						Formatter = @"function() {
										return '<b>'+ this.series.name +'</b><br/>'+
									this.x +':00 : '+ this.y;
								}"
					})
					.SetSeries(series);

				return chart;

			}
			catch (Exception ex)
			{
				//Log.AppLog().ErrorException("\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name, ex);
				Log.AppLog().Error(ex, "\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name);
			}

			return null;

		}

		// Gera gráfico de linha com duas séries de dados 
		public static Highcharts GerarGraficoLinha2(List<Series> dados, List<string> categories, string tituloGrafico, string idgrafico, int largura = 320, int altura = 300, string tituloX = "", string tituloY = "")
		{
			try
			{

				if (String.IsNullOrEmpty(idgrafico))
				{
					idgrafico = "chart";
				}
				Highcharts chart = new Highcharts(idgrafico)
					.InitChart(new Chart
					{
						DefaultSeriesType = ChartTypes.Line,
						Height = altura,
						Width = largura,
						ClassName = "chart"
					})
					.SetTitle(new Title { Text = tituloGrafico })
					.SetXAxis(new XAxis { Categories = categories.ToArray(), Title = new XAxisTitle() { Text = tituloX } })
					.SetYAxis(new YAxis
					{
						Title = new YAxisTitle { Text = tituloY }
					})
					.SetTooltip(new Tooltip
					{
						Formatter = @"function() {
										return '<b>'+ this.series.name +'</b><br/>'+
									this.x +':00 : '+ this.y +'%';
								}"
					})/*SetTooltip(new Tooltip
					{
						HeaderFormat = "<span style=\"font-size:10px\">{point.x}</span><table>",
						PointFormat = "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td>" +
									  "<td style=\"padding:0\"><b>{point.y} </b></td></tr>",
						FooterFormat = "</table>",
						Shared = true,
						UseHTML = true
					})*/
					.SetSeries(dados.ToArray());

				return chart;

			}
			catch (Exception ex)
			{
				//Log.AppLog().ErrorException("\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name, ex);
				Log.AppLog().Error(ex, "\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name);
			}

			return null;

		}

		/// <summary>
		/// Gera grafico combinado, barra e linha
		/// </summary>
		/// <param name="titulo">Titulo do gráfico</param>
		/// <param name="categoria">Barras do grafico </param>
		/// <param name="series">Lista com os dados</param>
		/// <returns></returns>
		public static Highcharts CombineGraph(string titulo, List<string> categoria, List<Series> series, string idgrafico, int? largura = 320, int? altura = 300, string tituloX = "", string tituloY = "", bool porcentagem = true)
		{

			var valorMax = series.Select(a => new { Max = a.Data.ArrayData.Max() }).ToList();
			var valorMin = series.Select(a => new { Min = a.Data.ArrayData.Min() }).ToList();
			double maiorValor = valorMax.Where(x => x.Max != null).Count() == 0 ? 0 : (double)valorMax.Max(d => d.Max);
			double menorValor = valorMin.Where(x => x.Min != null).Count() == 0 ? 0 : (double)valorMin.Min(d => d.Min);

			maiorValor = maiorValor + 0.03;

			Highcharts chart = new Highcharts(idgrafico)
				.InitChart(largura == 0 && altura == 0 ? new Chart
				{
					ZoomType = ZoomTypes.Xy,
					Height = null,
					Width = null
				}
				:
				new Chart
				{
					ZoomType = ZoomTypes.Xy,
					Height = altura,
					Width = largura
				}
				)
				.SetTitle(new Title { Text = titulo })
				.SetXAxis(new XAxis { Categories = categoria.ToArray() })
				.SetYAxis(new[]
				{
					new YAxis
					{
						Labels = new YAxisLabels
						{
							Formatter = porcentagem == true ? "function() { return this.value +'%'; }" : "function() { return this.value; }",
							Style = "color: '#000000'"
						},
						Title = new YAxisTitle
						{
							Text = string.Empty,
							Style = "color: '#ffffff'"
						},
						Max = maiorValor,
						Min = menorValor
					},
					new YAxis
					{
						Labels = new YAxisLabels
						{
							Formatter = porcentagem == true ? "function() { return this.value +'%'; }" : "function() { return this.value; }",
							Style = "color: '#ffffff'"
						},
						Title = new YAxisTitle
						{
							Text = string.Empty,
							Style = "color: '#ffffff'"
						},
						Max = maiorValor,
						Min = menorValor,
						Opposite = false
					}
				})
				.SetTooltip(new Tooltip
				{
					Formatter = "function() { return ''+ this.x +': '+ this.y + (this.series.name == 'Rainfall' ? ' mm' : '°%'); }"
				})
				.SetPlotOptions(new PlotOptions
				{
					Line = new PlotOptionsLine
					{
						DataLabels = new PlotOptionsLineDataLabels
						{
							Enabled = true,
							Style = "color: '#000000'",
							//Formatter = "function() { return ''+ this.x +': '+ this.y + (this.series.name == 'Rainfall' ? ' mm' : '%'); }",
							Formatter = porcentagem == true ? "function() { return this.y + (this.series.name == 'Rainfall' ? ' ' : '%'); }" : "function() { return this.y; }",
							Y = -15
						},
						EnableMouseTracking = false
					},
					Column = new PlotOptionsColumn
					{
						DataLabels = new PlotOptionsColumnDataLabels
						{
							Enabled = true,
							Rotation = -89,
							Formatter = porcentagem == true ? "function() { return this.y + (this.series.name == 'Rainfall' ? ' ' : '%'); }" : "function() { return this.y; }",
							Y = -15
						},
						EnableMouseTracking = false
					}
				})
				.SetLegend(new Legend
				{
					Layout = Layouts.Horizontal,
					Align = HorizontalAligns.Left,
					X = 200,
					VerticalAlign = VerticalAligns.Top,
					Y = 15,
					Floating = true,
					BackgroundColor = new BackColorOrGradient(ColorTranslator.FromHtml("#FFFFFF"))
				})
				.SetSeries(series.ToArray())
				.SetExporting(new Exporting {
					Enabled = true,
					//Scale = 10,
					SourceWidth = 1024,
					//SourceHeight = 800,
					Filename = idgrafico,
					Type = "image/png",
					Url = "http://export.highcharts.com",
					Width = 1300
				});

			return chart;
		}

		/// <summary>
		/// Gera grafico combinado, barra e linha, com drilldown
		/// </summary>
		/// <param name="titulo">Titulo do gráfico</param>
		/// <param name="categoria">Barras do grafico </param>
		/// <param name="series">Lista com os dados</param>
		/// <param name="idgrafico"></param>
		/// <param name="largura"></param>
		/// <param name="altura"></param>
		/// <param name="tituloX"></param>
		/// <param name="tituloY"></param>
		/// <param name="max"></param>
		/// <param name="min"></param>
		/// <returns></returns>
		public static Highcharts CombineGraph2(string titulo, List<string> categoria, Series[] series, string idgrafico, int? largura = null, int? altura = null, string tituloX = "", string tituloY = "", double? max = null, double? min = null)
		{

			Highcharts chart = new Highcharts(idgrafico)
				.InitChart(
					new Chart
					{
						DefaultSeriesType = ChartTypes.Column,
						ZoomType = ZoomTypes.Xy,
						Height = altura,
						Width = largura
					}
				)
				.SetTitle(new Title {Text = titulo})
				.SetXAxis(new XAxis {Categories = categoria.ToArray()})
				.SetYAxis(new[]
				{
					new YAxis
					{
						Labels = new YAxisLabels
						{
							Formatter = "function() { return this.value; }",
							Style = "color: '#000000'"
						},
						Title = new YAxisTitle
						{
							Text = string.Empty,
							Style = "color: '#ffffff'"
						},
						Max = max,
						Min = min
					},
					new YAxis
					{
						Labels = new YAxisLabels
						{
							Formatter = "function() { return this.value; }",
							Style = "color: '#ffffff'"
						},
						Title = new YAxisTitle
						{
							Text = string.Empty,
							Style = "color: '#ffffff'"
						},
						Max = max,
						Min = min,
						Opposite = false,
						StartOnTick = false
					}
				})
				.SetTooltip(new Tooltip
				{
					Formatter = "function() { return '' + this.x + ': ' + this.y; }"
				})
				.SetPlotOptions(new PlotOptions
				{
					Line = new PlotOptionsLine
					{
						DataLabels = new PlotOptionsLineDataLabels
						{
							Enabled = true,
							Style = "color: '#000000'",
							Y = -15
						},
						EnableMouseTracking = false
					},
					Column = new PlotOptionsColumn
					{
						Cursor = Cursors.Pointer,
						
						DataLabels = new PlotOptionsColumnDataLabels
						{
							Enabled = true,
							Rotation = -89,
							Y = -15,
							Style = "fontWeight: 'bold'"
						},
						EnableMouseTracking = true
					}
				})
				.SetLegend(new Legend
				{
					Layout = Layouts.Horizontal,
					Align = HorizontalAligns.Left,
					X = 200,
					VerticalAlign = VerticalAligns.Top,
					Y = 15,
					Floating = true,
					BackgroundColor = new BackColorOrGradient(ColorTranslator.FromHtml("#FFFFFF"))
				})
				.SetSeries(series)
				/*.AddJavascripFunction(
					"ColumnPointClick",
					@"
					var drilldown = this.drilldown;
						if (drilldown) { // drill down
							setChart(drilldown.name, drilldown.categories, drilldown.data.data, drilldown.color);
						} else { // restore
							setChart(name, categorias, dados);
							//voltaChart();
						}"
				//@"$('#grafico2').toggle();$('#grafico2').css('width', '100%');"
				)
				.AddJavascripFunction(
					"setChart",
					"	var chart = " + idgrafico + ";" + 
					"	chart.xAxis[0].setCategories(categories);" +
					"	while(chart.series.length > 0) chart.series[0].remove(true);" +
					"	chart.addSeries({" +
					"		 name: name," +
					"		 data: data," +
					"		 color: color || 'white'" +
					"	});"+
					"	chart.yAxis[0].update({ max: null, min: null })",
					"name", "categories", "data", "color"
				)
				.AddJavascripFunction(
					"voltaChart",
					"	" + idgrafico + ".drillUp();"
				)
				.AddJavascripVariable("colors", "Highcharts.getOptions().colors")
				.AddJavascripVariable("name", "'{0}'".FormatWith(titulo))
				.AddJavascripVariable("categorias", JsonSerializer.Serialize(categoria.ToArray()))
				.AddJavascripVariable("dados", JsonSerializer.Serialize(series[0]))*/;
			

			return chart;
		}

		public static Highcharts StackedColumn(string titulo, List<string> categoria, List<Series> series, string idChart = "chart", int? largura = null, int? altura = null)
		{
			Highcharts chart = new Highcharts(idChart)
				.InitChart(new Chart { 
					DefaultSeriesType = ChartTypes.Column,
					Width = largura,
					Height = altura
				})
				.SetTitle(new Title { Text = titulo })
				.SetXAxis(new XAxis { Categories = categoria.ToArray() })
				.SetYAxis(new YAxis
				{
					Min = 0,
					Title = new YAxisTitle { Text = string.Empty },
					StackLabels = new YAxisStackLabels
					{
						Enabled = true,
						Style = "fontWeight: 'bold', color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'"
					}
				})
				.SetLegend(new Legend
				{
					Layout = Layouts.Horizontal,
					Align = HorizontalAligns.Right,
					VerticalAlign = VerticalAligns.Top,
					X = -100,
					Y = 20,
					Floating = true,
					BackgroundColor = new BackColorOrGradient(ColorTranslator.FromHtml("#FFFFFF")),
					BorderColor = ColorTranslator.FromHtml("#CCC"),
					//BorderWidth = 1,
					Shadow = false
				})
				//.SetTooltip(new Tooltip { Formatter = "TooltipFormatter" })
				.SetPlotOptions(new PlotOptions
				{
					Column = new PlotOptionsColumn
					{
						Stacking = Stackings.Normal,
						DataLabels = new PlotOptionsColumnDataLabels
						{
							Enabled = true,
							Color = Color.White
						}
					}
				})
				.SetSeries(series.ToArray())
				.SetExporting(new Exporting
				{
					Enabled = true,
					//Scale = 10,
					SourceWidth = 1024,
					//SourceHeight = 800,
					Filename = idChart,
					Type = "image/png",
					Url = "http://export.highcharts.com",
					Width = 1300
				});

			return chart;
		}

	}
}