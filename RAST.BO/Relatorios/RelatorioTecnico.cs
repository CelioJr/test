﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.Rastreabilidade;

namespace RAST.BO.Relatorios
{
	public class RelatorioTecnico
	{

		public static int ObterProducaoModelo(DateTime DatInicio, DateTime DatFim, string codModelo)
		{
			return daoRelatorioTecnico.ObterProducaoModelo(DatInicio, DatFim, codModelo);
		}

		public static int ObterProducaoLinha(DateTime DatInicio, DateTime DatFim, int codLinha)
		{

			//var posto = daoPassagem.ObterPrimeiroPostoLeitura(codLinha);
			var qtde = daoRelatorioTecnico.ObterProducaoLinha(DatInicio, DatFim, codLinha);
			//var qtde = daoPassagem.ObterQtdLeiturasPosto(DatInicio, DatFim, codLinha, posto);
			return qtde;
		}

		public static int ObterProducaoLinha(DateTime DatInicio, DateTime DatFim, int codLinha, int turno)
		{

			var posto = daoPassagem.ObterPrimeiroPostoLeitura(codLinha);

			var horario = RAST.BO.Rastreabilidade.Geral.ObterHorarioPorID(turno == 2 ? 3 : 1);
			DatInicio = DateTime.Parse(DatInicio.ToString("dd/MM/yyyy") + " " + horario.HoraInicio);
			DatFim = DateTime.Parse(DatFim.ToString("dd/MM/yyyy") + " " + horario.HoraFim);

			var qtdLeitura = daoPassagem.ObterQtdLeiturasPosto(DatInicio, DatFim, codLinha, posto);
			
			return qtdLeitura;

		}

		public static int ObterProducaoFase(DateTime DatInicio, DateTime DatFim, string Fabrica)
		{
			return daoRelatorioTecnico.ObterProducaoFase(DatInicio, DatFim, Fabrica);
		}

		public static int ObterProducaoFamilia(DateTime DatInicio, DateTime DatFim, string Fabrica, string Familia)
		{
			return daoRelatorioTecnico.ObterProducaoFamilia(DatInicio, DatFim, Fabrica, Familia);
		}

		public static int ObterProdutosPendentes(DateTime DatInicio, DateTime DatFim, string codModelo)
		{
			return daoRelatorioTecnico.ObterProdutosPendentes(DatInicio, DatFim, codModelo);
		}

		public static int ObterProdutosPendentes(DateTime DatInicio, DateTime DatFim, int codLinha, string codModelo = "") 
		{
			return daoRelatorioTecnico.ObterProdutosPendentes(DatInicio, DatFim, codLinha, codModelo);
		}

		public static int ObterPendentesFase(DateTime DatInicio, DateTime DatFim, string Fase, string codModelo = "")
		{
			return daoRelatorioTecnico.ObterPendentesFase(DatInicio, DatFim, Fase, codModelo);
		}
	}
}
