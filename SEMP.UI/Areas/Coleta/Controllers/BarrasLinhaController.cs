﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.ApontaProd;
using SEMP.Model.DTO;

namespace SEMP.Areas.Coleta.Controllers
{
    public class BarrasLinhaController : Controller
    {
        //
        // GET: /Coleta/BarrasLinha/

        public ActionResult Index()
        {

	        var linhas = BarrasLinhaBO.ObterLinhas();

            return View(linhas);
        }

        //
        // GET: /Coleta/BarrasLinha/Edit/5

	    public ActionResult Edit(string codLinha)
	    {
		    if (!String.IsNullOrEmpty(codLinha))
		    {
				var linha = BarrasLinhaBO.ObterLinha(codLinha);
				return View(linha);
			}
		    else
		    {
			    return RedirectToAction("BarrasLinha");
		    }
		}

        //
        // POST: /Coleta/BarrasLinha/Edit/5

        [HttpPost]
        public ActionResult Edit(BarrasLinhaDTO linha)
        {
            try
            {
				BarrasLinhaBO.AtualizaLinha(linha);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

    }
}
