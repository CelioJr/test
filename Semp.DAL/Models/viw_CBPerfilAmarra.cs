using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class viw_CBPerfilAmarra
    {
        public int CodLinha { get; set; }
        public int NumPosto { get; set; }
        public string CodModelo { get; set; }
        public int CodTipoAmarra { get; set; }
        public string CodItem { get; set; }
        public string Mascara { get; set; }
        public string DesTipoAmarra { get; set; }
        public string DesModelo { get; set; }
        public string DesItem { get; set; }
        public int Sequencia { get; set; }

        public int? flgPlacaPainel;
    }
}
