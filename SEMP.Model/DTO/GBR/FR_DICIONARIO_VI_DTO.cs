using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DICIONARIO_VI_DTO
    {
        public string TABELA { get; set; }
        public string CAMPO { get; set; }
        public string DESCRICAO { get; set; }
        public string TIPO { get; set; }
        public int PK { get; set; }
        public int FK { get; set; }
        public short TAMANHO { get; set; }
        public byte PRECISAO { get; set; }
        public Nullable<int> NULO { get; set; }
    }
}
