namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_rp_predecessora
    {
        [Key]
        public long id_rp_predecessora { get; set; }

        public long? id_rotapassoPai { get; set; }

        public long id_rotapassoFilho { get; set; }

        public short? is_aceitaSeNC { get; set; }

        public short? is_aceitaSeC { get; set; }

        public short? is_espelho { get; set; }

        public virtual dw_rotapasso dw_rotapasso { get; set; }

        public virtual dw_rotapasso dw_rotapasso1 { get; set; }
    }
}
