namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_cfgscrpimp
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_cfgscrpimp()
        {
            dw_folha = new HashSet<dw_folha>();
            dw_folhaemb = new HashSet<dw_folhaemb>();
        }

        [Key]
        public long id_cfgscrpimp { get; set; }

        [StringLength(30)]
        public string cd_scrp { get; set; }

        [StringLength(100)]
        public string ds_scrp { get; set; }

        [Column(TypeName = "text")]
        public string scriptimpressao { get; set; }

        public int? revisao { get; set; }

        public short? stativo { get; set; }

        public long id_cfg { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folha> dw_folha { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhaemb> dw_folhaemb { get; set; }

        public virtual om_cfg om_cfg { get; set; }
    }
}
