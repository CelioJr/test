using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEMP.Model.DTO
{
	public partial class CBTAItemDTO
	{
		[Key, Column(Order = 1)]
		public string CodModelo { get; set; }
		[Key, Column(Order = 2)]
		public string NumSerieModelo { get; set; }
		[Key, Column(Order = 3)]
		public string CodModeloPCI { get; set; }
		[Key, Column(Order = 4)]
        public string NumSeriePCI { get; set; }
		[Key, Column(Order = 5)]
        public string Fase { get; set; }
		[Key, Column(Order = 6)]
        public string SubFase { get; set; }
        public string CodProcesso { get; set; }
        public int Ordem { get; set; }
        public Nullable<System.DateTime> DatEntrada { get; set; }
        public Nullable<System.DateTime> DatSaida { get; set; }
        public Nullable<int> TempoCorrido { get; set; }
        public Nullable<int> TempoAtravessa { get; set; }
        public Nullable<int> QtdDefeito { get; set; }
        public Nullable<int> TempoTecnico { get; set; }
        public Nullable<int> CodLinha { get; set; }
		public bool? FlgTempoMedio { get; set; }

		public string DesModelo { get; set; }
		public string DesPCI { get; set; }
		public string Classe { get; set; }

		public int Incompleto { get; set; }
	}
}
