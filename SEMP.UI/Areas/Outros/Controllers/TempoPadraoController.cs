﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.PM;
using SEMP.Model.DTO;

namespace SEMP.Areas.Outros.Controllers
{
	public class TempoPadraoController : Controller
	{
		//
		// GET: /Outros/TempoPadrao/
		public ActionResult Index()
		{
			return View();
		}

		//
		// GET: /Outros/TempoPadrao/Details/5
		public ActionResult Lista(string Modelo, string flgAtivo)
		{
			var dados = TempoPadrao.ObterModelos();

			if (!String.IsNullOrEmpty(Modelo))
			{
				dados = dados.Where(x => x.CodModelo.Equals(Modelo)).ToList();

				dados.AddRange(TempoPadrao.ObterItensModelo(Modelo));

			}

			if (flgAtivo != null && flgAtivo.Equals("S"))
			{
				dados = dados.Where(x => x.flgAtivo == "S").ToList();
			}
			else if (flgAtivo != null && flgAtivo.Equals("N"))
			{
				dados = dados.Where(x => x.flgAtivo == "N").ToList();
			}

			return View(dados);
		}

		//
		// GET: /Outros/TempoPadrao/Create
		public ActionResult Create()
		{
			return View();
		}

		//
		// POST: /Outros/TempoPadrao/Create
		[HttpPost]
		public ActionResult Create(FormCollection tempoPadrao)
		{
			try
			{

				if (String.IsNullOrEmpty(tempoPadrao["TempoLp"]))
				{
					tempoPadrao["TempoLp"] = "0";
				}
				if (String.IsNullOrEmpty(tempoPadrao["TempoPadrao"]))
				{
					tempoPadrao["TempoPadrao"] = "0";
				}

				var t = new TempoPadraoDTO()
				{
					CodModelo = tempoPadrao["CodModelo"],
					CodPai = tempoPadrao["CodPai"],
					DesModelo = tempoPadrao["DesModelo"],
					TempoLp = decimal.Parse(tempoPadrao["TempoLp"]),
					TempoPadrao = decimal.Parse(tempoPadrao["TempoPadrao"]),
					Usuario = tempoPadrao["Usuario"]
				};

				TempoPadrao.GravarTempoPadrao(t);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}

		//
		// GET: /Outros/TempoPadrao/Edit/5
		public ActionResult Edit(string codModelo)
		{
			var dado = TempoPadrao.ObterTempoPadrao(codModelo);

			return View(dado);
		}

		//
		// POST: /Outros/TempoPadrao/Edit/5
		[HttpPost]
		public ActionResult Edit(TempoPadraoDTO tempo)
		{
			try
			{
				TempoPadrao.GravarTempoPadrao(tempo);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}

		//
		// GET: /Outros/TempoPadrao/Delete/5
		public ActionResult Delete(string codModelo)
		{
			var dado = TempoPadrao.ObterTempoPadrao(codModelo);

			return View(dado);
		}

		//
		// POST: /Outros/TempoPadrao/Delete/5
		[HttpPost]
		public ActionResult Delete(TempoPadraoDTO tempo)
		{
			try
			{
				TempoPadrao.RemoverTempoPadrao(tempo.CodModelo);

				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}
		}
	}
}
