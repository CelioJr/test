namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_VOLUME")]
    public partial class ALC_VOLUME
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ALC_VOLUME()
        {
            ALC_LOT_LIST = new HashSet<ALC_LOT_LIST>();
            ALC_OQC_INSPECTION = new HashSet<ALC_OQC_INSPECTION>();
            ALC_PRODUCTION_COUNTER = new HashSet<ALC_PRODUCTION_COUNTER>();
        }

        [Key]
        [StringLength(25)]
        public string ID_VOL { get; set; }

        public DateTime? DATE_CREATE { get; set; }

        public DateTime? DATE_FINISH { get; set; }

        [MaxLength(255)]
        public byte[] orderVol { get; set; }

        public int? QUANTITY { get; set; }

        [StringLength(20)]
        public string STATUS { get; set; }

        public int? QTY_PROD { get; set; }

        [StringLength(20)]
        public string ORDER_VOL { get; set; }

        public int? PRODUCTS_QTY { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_LOT_LIST> ALC_LOT_LIST { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_OQC_INSPECTION> ALC_OQC_INSPECTION { get; set; }

        public virtual ALC_ORDER ALC_ORDER { get; set; }

        public virtual ALC_ORDER ALC_ORDER1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_PRODUCTION_COUNTER> ALC_PRODUCTION_COUNTER { get; set; }
    }
}
