using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_PlanoMap : EntityTypeConfiguration<tbl_Plano>
    {
        public tbl_PlanoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodPlano, t.rowguid });

            // Properties
            this.Property(t => t.CodPlano)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NomRespons)
                .IsFixedLength()
                .HasMaxLength(12)
                .IsUnicode(false);

            this.Property(t => t.CodStatus)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.DesAprovacao)
                .IsFixedLength()
                .HasMaxLength(18)
                .IsUnicode(false);

            this.Property(t => t.Usuario)
                .IsFixedLength()
                .HasMaxLength(25)
                .IsUnicode(false);

            this.Property(t => t.CodFase)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.CodFab)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.FlgBreakdown)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.NomUsuario)
                .IsFixedLength()
                .HasMaxLength(25)
                .IsUnicode(false);

            this.Property(t => t.Supplier)
                .HasMaxLength(30)
                .IsUnicode(false);

            this.Property(t => t.CodODM)
                .HasMaxLength(30)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_Plano");
            this.Property(t => t.CodPlano).HasColumnName("CodPlano");
            this.Property(t => t.NomRespons).HasColumnName("NomRespons");
            this.Property(t => t.CodStatus).HasColumnName("CodStatus");
            this.Property(t => t.DesAprovacao).HasColumnName("DesAprovacao");
            this.Property(t => t.DatElab).HasColumnName("DatElab");
            this.Property(t => t.DatInativo).HasColumnName("DatInativo");
            this.Property(t => t.Usuario).HasColumnName("Usuario");
            this.Property(t => t.DatDigitacao).HasColumnName("DatDigitacao");
            this.Property(t => t.DatEDP).HasColumnName("DatEDP");
            this.Property(t => t.DatPCPM).HasColumnName("DatPCPM");
            this.Property(t => t.DatGEI).HasColumnName("DatGEI");
            this.Property(t => t.DatGMM).HasColumnName("DatGMM");
            this.Property(t => t.DatGEF).HasColumnName("DatGEF");
            this.Property(t => t.DatPreEDP).HasColumnName("DatPreEDP");
            this.Property(t => t.DatPreGEI).HasColumnName("DatPreGEI");
            this.Property(t => t.CodFase).HasColumnName("CodFase");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.FlgBreakdown).HasColumnName("FlgBreakdown");
            this.Property(t => t.DatBreakdown).HasColumnName("DatBreakdown");
            this.Property(t => t.NomUsuario).HasColumnName("NomUsuario");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
            this.Property(t => t.Supplier).HasColumnName("Supplier");
            this.Property(t => t.CodODM).HasColumnName("CodODM");
        }
    }
}
