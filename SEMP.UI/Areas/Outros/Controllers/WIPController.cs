﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.ApontaProd;
using SEMP.Model.DTO;

namespace SEMP.Areas.Outros.Controllers
{
	public class WIPController : Controller
	{
		//
		// GET: /Coleta/WIP/

		public ActionResult Index(string local, string familia)
		{
			var wip = new List<WIPProducaoDTO>();
			var locais = new List<WIPProducaoDTO>();
			var familias = new List<string>();

			try
			{
				wip = WIP.ObterDadosWIP();

				if (!String.IsNullOrEmpty(local) && local != "TODOS") {
					wip = wip.Where(x => x.CodLocal == local).ToList();
				}

				if (!String.IsNullOrEmpty(familia))
				{
					wip = wip.Where(x => x.Familia == familia).ToList();
				}

				var estoqueTotal = wip.Sum(x => x.QtdEstoque) ?? 0;
				var valorTotal = wip.Sum(x => x.Valor) ?? 0;

				if (wip.Any())
				{
					locais = wip.GroupBy(g => new { g.CodLocal, g.Familia })
								.Select(x => new WIPProducaoDTO() { CodLocal = x.Key.CodLocal, QtdEstoque = (x.Sum(y => y.Valor) / valorTotal), Familia = x.Key.Familia, Valor = x.Sum(s => s.Valor) })
								.OrderBy(x => x.CodLocal).ThenBy(x => x.Familia).ToList();

				}

				familias.Add("TODOS");
				familias.AddRange(WIP.ObterFamiliasWIP());

			}
			catch (Exception ex)
			{
				ViewBag.erro = "Erro ao consultar: " + ex.Message;
			}

			ViewBag.locais = locais;
			ViewBag.familias = familias;
			return View(wip);
		}

		public string GerarDadosWIP()
		{

			try
			{
				WIP.GerarDadosWIP();
				return "OK";
			}
			catch (Exception ex)
			{
				return "Erro: " + ex.Message;
				throw;
			}

		}


	}
}
