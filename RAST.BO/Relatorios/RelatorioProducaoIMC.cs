﻿using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.Model.DTO;

namespace RAST.BO.Relatorios
{
	public class RelatorioProducaoIMC
	{
		public static List<RelatorioProducao> ObterResumo(DateTime DatInicio, DateTime DatFim, int Turno)
		{

			//Cria o objeto que vai receber os dados do resumo de produção
			List<RelatorioProducao> resumo = new List<RelatorioProducao>();

			try
			{
				// Seleciona as linhas que serão verificadas. Nesse caso as linhas de LCD
				List<CBLinhaDTO> linhas = daoResumoIMC.LinhasIMC("IMC", DatInicio);

				// Faz a operação para cada linha
				foreach (CBLinhaDTO linha in linhas)
				{
					// Seleciona o programa da linha atual no período
					var programaLinha = daoResumoIMC.ObterProgramaByLinha(linha.CodLinhaT1, Turno, DatInicio, DatFim);

					// Seleciona a produção da linha atual no período
					var producaoDia = daoResumoIMC.ObterProduzidoByLinha(linha.CodLinhaT1, Turno, DatInicio, DatFim);

					DateTime DataIni = DatInicio.AddDays(-(DatInicio.Day) + 1);
					DateTime DataFim = DatFim.AddDays(-(DatFim.Day) + 1);
					var CapacLinha = daoResumoIMC.ObterCapacidadeByLinha(linha.CodLinhaT1, Turno, DataIni, DataFim);

					// Atualiza para cada linha e cada programa
					foreach (var programa in programaLinha)
					{
						RelatorioProducao r = new RelatorioProducao();
						r.CodLinha = linha.CodLinhaT1;
						r.DesLinha = linha.DesLinha;

						r.CodModelo = programa.CodModelo;
						r.DesModelo = daoGeral.ObterDescricaoItem(programa.CodModelo);

						r.QtdPlano = (int)programa.QtdProdPM;

						if (producaoDia.Any())
						{
							var qtde = (from i in producaoDia
										where i.CodLinha == linha.CodLinhaT1 &&
												i.NumTurno == Turno &&
												i.CodPlaca == programa.CodModelo
										select i.QtdProduzida).FirstOrDefault();

							r.QtdRealizado = (int)qtde;
						}
						else
						{
							r.QtdRealizado = 0;
						}

						if (r.QtdRealizado > 0)
						{
							r.PercDiferenca = Math.Abs(Math.Round(((decimal)r.QtdDiferenca / (decimal)r.QtdRealizado) * (decimal)100, 2));
						}

						if (CapacLinha.Any())
						{
							var qtde = CapacLinha.Any(
								c => c.CodLinha == linha.CodLinhaT1 && c.NumTurno == Turno && c.CodModelo == programa.CodModelo) == true ?
								(from i in CapacLinha
								 where i.CodLinha == linha.CodLinhaT1 &&
										 i.NumTurno == Turno &&
										 i.CodModelo == programa.CodModelo
								 select i.QtdCapacLinha).FirstOrDefault().Value
										: 0;

							r.QtdCapacidade = (int)qtde;
						}

						resumo.Add(r);
					}

					foreach (ProdDiaPlacaDTO producao in producaoDia)
					{
						var existe = (from prog in programaLinha
									  where prog.CodLinha == producao.CodLinha &&
										  //prog.CodModelo == producao.CodModelo &&
											  prog.NumTurno == producao.NumTurno
									  select prog);

						if (!existe.Any())
						{

							RelatorioProducao r = new RelatorioProducao();
							r.CodLinha = linha.CodLinhaT1;
							r.DesLinha = linha.DesLinha;

							//r.CodModelo = producao.CodModelo;
							//r.DesModelo = daoGeral.ObterDescricaoItem(producao.CodModelo);

							r.QtdRealizado = (int)producao.QtdProduzida;
							r.QtdPlano = 0;

							if (r.QtdRealizado > 0)
							{
								r.PercDiferenca = (r.QtdDiferenca / r.QtdRealizado) * 100;
							}

							resumo.Add(r);
						}
					}

				}

				return resumo;
			}
			catch
			{
				return null;
			}
		}

		public static List<RelatorioProducao> ObterResumo(DateTime DatInicio, DateTime DatFim, int Turno, string setor)
		{

			//Cria o objeto que vai receber os dados do resumo de produção
			List<RelatorioProducao> resumo = new List<RelatorioProducao>();

			try
			{
				// Seleciona as linhas que serão verificadas.
				List<CBLinhaDTO> linhas = daoResumoIMC.LinhasIMCSetor(setor, DatInicio);

				// Faz a operação para cada linha
				foreach (CBLinhaDTO linha in linhas)
				{
					// Seleciona o programa da linha atual no período
					var programaLinha = daoResumoIMC.ObterProgramaByLinha(linha.CodLinhaT1, Turno, DatInicio, DatFim);

					// Seleciona a produção da linha atual no período
					var producaoDia = daoResumoIMC.ObterProduzidoByLinha(linha.CodLinhaT1, Turno, DatInicio, DatFim);

					DateTime DataIni = DatInicio.AddDays(-(DatInicio.Day) + 1);
					DateTime DataFim = DatFim.AddDays(-(DatFim.Day) + 1);
					var CapacLinha = daoResumoIMC.ObterCapacidadeByLinha(linha.CodLinhaT1, Turno, DataIni, DataFim);

					// Atualiza para cada linha e cada programa
					foreach (var programa in programaLinha)
					{
						RelatorioProducao r = new RelatorioProducao();
						r.CodLinha = linha.CodLinhaT1;
						r.DesLinha = linha.DesLinha;

						r.CodModelo = programa.CodModelo;
						r.DesModelo = daoGeral.ObterDescricaoItem(programa.CodModelo);

						r.QtdPlano = (int)programa.QtdProdPM;

						if (producaoDia.Any())
						{
							var qtde = (from i in producaoDia
										where i.CodLinha == linha.CodLinhaT1 &&
												i.NumTurno == Turno &&
												i.CodPlaca == programa.CodModelo
										select i.QtdProduzida).FirstOrDefault();

							r.QtdRealizado = (int)qtde;
						}
						else
						{
							r.QtdRealizado = 0;
						}

						if (r.QtdRealizado > 0)
						{
							r.PercDiferenca = Math.Abs(Math.Round(((decimal)r.QtdDiferenca / (decimal)r.QtdRealizado) * (decimal)100, 2));
						}

						if (CapacLinha.Any())
						{
							var qtde = CapacLinha.Any(
								c => c.CodLinha == linha.CodLinhaT1 && c.NumTurno == Turno && c.CodModelo == programa.CodModelo) == true ?
								(from i in CapacLinha
								 where i.CodLinha == linha.CodLinhaT1 &&
										 i.NumTurno == Turno &&
										 i.CodModelo == programa.CodModelo
								 select i.QtdCapacLinha).FirstOrDefault().Value
										: 0;

							r.QtdCapacidade = (int)qtde;
						}

						resumo.Add(r);
					}

					foreach (ProdDiaPlacaDTO producao in producaoDia)
					{
						var existe = (from prog in programaLinha
									  where prog.CodLinha == producao.CodLinha &&
										  //prog.CodModelo == producao.CodModelo &&
											  prog.NumTurno == producao.NumTurno
									  select prog);

						if (!existe.Any())
						{

							RelatorioProducao r = new RelatorioProducao();
							r.CodLinha = linha.CodLinhaT1;
							r.DesLinha = linha.DesLinha;

							//r.CodModelo = producao.CodModelo;
							//r.DesModelo = daoGeral.ObterDescricaoItem(producao.CodModelo);

							r.QtdRealizado = (int)producao.QtdProduzida;
							r.QtdPlano = 0;

							if (r.QtdRealizado > 0)
							{
								r.PercDiferenca = (r.QtdDiferenca / r.QtdRealizado) * 100;
							}

							resumo.Add(r);
						}
					}

				}

				return resumo;
			}
			catch
			{
				return null;
			}
		}

		public static List<CBLinhaDTO> ObterLinhasIMC(DateTime? datProducao)
		{
			return daoResumoIMC.LinhasIMC("IMC", datProducao);
		}

		public static List<CBLinhaDTO> ObterLinhasSetor(DateTime? datProducao, string setor)
		{
			return daoResumoIMC.LinhasIMC(setor, datProducao);
		}

		public static List<CBLinhaDTO> ObterLinhasSetorAll(DateTime? datProducao, string setor)
		{
			return daoResumoIMC.LinhasIMCSetor(setor, datProducao);
		}

		public static List<ParadasLinha> ObterParadas(DateTime DatInicio, DateTime DatFim, int Turno)
		{
			List<ParadasLinha> resumoparadas = new List<ParadasLinha>();

			var linhas = ObterLinhasIMC(DatInicio);

			var paradas = new List<ProdParadaDTO>();

			linhas.ForEach(linha => paradas.AddRange(daoParadas.ObterParadas(DatInicio, DatFim, Turno, linha.CodLinhaT1)));
			

			foreach (var p in paradas)
			{
				var paradasdep = daoParadas.ObterParadaDep(p.NumParada);

				ParadasLinha paradaslinha = new ParadasLinha();
				paradaslinha.CodParada = p.NumParada;
				paradaslinha.DesParada = p.DesObs;
				paradaslinha.CodLinha = p.CodLinha;
				paradaslinha.FlgPDL = 'S';
				paradaslinha.HoraInicio = p.HorInicio;
				paradaslinha.HoraFim = p.HorFim;
				paradaslinha.QtdAparelhos = (int)p.QtdNaoProduzida;
				paradaslinha.SetorResp = paradasdep.FirstOrDefault().CodDepto;

				if (!String.IsNullOrEmpty(p.HorInicio) && !String.IsNullOrEmpty(p.HorFim))
				{
					DateTime horainicio = DateTime.Parse(DatInicio.ToShortDateString() + " " + p.HorInicio);
					DateTime horafim = DateTime.Parse(DatInicio.ToShortDateString() + " " + p.HorFim);

					var totalhoras = horafim.Subtract(horainicio);
					paradaslinha.TotalHoras = totalhoras.Hours.ToString("00") + ":" + totalhoras.Minutes.ToString("00");
				}

				//paradaslinha.TotalHoras
				resumoparadas.Add(paradaslinha);

			}

			return resumoparadas;
		}

		public static List<DefeitosLinha> ObterDefeitos(DateTime DatInicio, DateTime DatFim, int Turno, string setor)
		{
			List<DefeitosLinha> defeitoslinha = new List<DefeitosLinha>();

			var defeitos = daoDefeitos.ObterDefeitos(DatInicio, DatFim, Turno, setor).Select(
			d => new{ d.CodLinha,
									  d.CodDefeito,
									  CodCausa = (d.DesAcao == "FALSA FALHA" || d.DesAcao == "DEFEITO NÃO ENCONTRADO") ? "WX" : d.CodCausa,
									  d.CodOrigem,
									  d.Posicao,
									  d.NumECB,
									  CodModelo = d.NumECB.Substring(0,6)
				}
			).GroupBy(
				g => new { g.CodLinha, g.CodDefeito, g.CodCausa, g.CodOrigem, g.CodModelo}).Select(
				i => new { i.Key.CodLinha, i.Key.CodDefeito, i.Key.CodCausa, i.Key.CodOrigem, QtdDefeito = i.Count(), i.Key.CodModelo }
			).ToList();

			var totais = defeitos.GroupBy(
				g => g.CodLinha).Select(
				i => new DefeitosLinha()
				{
					CodLinha = i.Key,
					CodCausa = "",
					DesCausa = "ÍNDICE DE DEFEITOS % =>",
					Disposicao = (((double)i.Sum(s => s.QtdDefeito) /   (double)((daoResumoIMC.ObterProduzidoLinhaTotal(daoGeral.ObterLinhaCliente(i.Key), Turno, DatInicio, DatFim).Count == 0 ) ? 0 : 
                                                                        daoResumoIMC.ObterProduzidoLinhaTotal(daoGeral.ObterLinhaCliente(i.Key), Turno, DatInicio, DatFim).FirstOrDefault().QtdProduzida)) * 100.00).ToString("0.00"),
					Origem = "Total",
					Quantidade = i.Sum(s => s.QtdDefeito)
				}
			).ToList();

			foreach (var d in defeitos)
			{
				DefeitosLinha defeito = new DefeitosLinha
				{
					CodDefeito = d.CodDefeito,
					CodLinha = d.CodLinha,
					CodCausa = d.CodCausa,
					DesCausa = daoDefeitos.ObterDesCausa(d.CodCausa),
					DesDefeito = daoDefeitos.ObterDesDefeito(d.CodDefeito),
					Origem = daoDefeitos.ObterDesOrigem(d.CodOrigem),
					Quantidade = d.QtdDefeito,
					CodModelo = d.CodModelo,
					DesModelo = daoGeral.ObterDescricaoItem(d.CodModelo)
				};
				var firstOrDefault = totais.FirstOrDefault(x => x.CodLinha == d.CodLinha);
				if (firstOrDefault != null)
					defeito.Taxa = Math.Round(((decimal)d.QtdDefeito / (decimal)(firstOrDefault.Quantidade)) * 100, 2);


				defeitoslinha.Add(defeito);

			}

			totais.ForEach(defeitoslinha.Add);

			return defeitoslinha;
		}

        public static List<IndiceDefeitoLinha> ObterIndiceDefeito(DateTime DatInicio, DateTime DatFim, int? Turno, string Familia, string setor)
        {

            var linhas = ObterLinhasSetorAll(DatInicio, setor);

            if (!String.IsNullOrEmpty(Familia))
            {
                if (Familia != "DVD")
                    linhas = linhas.Where(x => x.Familia == Familia).ToList();
                else
                    linhas = linhas.Where(x => x.Familia == Familia || x.Familia == "AUD").ToList();

            }
            List<IndiceDefeitoLinha> indice = new List<IndiceDefeitoLinha>();

            foreach (var linha in linhas)
            {
                var posto = daoPassagem.ObterPrimeiroPostoLeitura(linha.CodLinha);
                var qtdLeitura = daoPassagem.ObterQtdLeiturasPosto(DatInicio, DatFim, linha.CodLinha, posto);
                var qtdEmbalado = daoRelatorioTecnico.ObterProducaoLinha(DateTime.Parse(DatInicio.ToShortDateString()), DateTime.Parse(DatFim.ToShortDateString()), linha.CodLinha);
                var qtdDefeito = daoDefeitos.ObterQtdDefeitosLinha(DateTime.Parse(DatInicio.ToShortDateString()), DateTime.Parse(DatFim.ToShortDateString()), linha.CodLinha);
                var qtdFalsaFalha = daoDefeitos.ObterQtdDefeitosFFLinha(DateTime.Parse(DatInicio.ToShortDateString()), DateTime.Parse(DatFim.ToShortDateString()), linha.CodLinha);

                var indlinha = new IndiceDefeitoLinha()
                {
                    CodLinha = linha.CodLinha,
                    QtdProducao = qtdLeitura,
                    QtdEmbalado = qtdEmbalado,
                    QtdDefeito = qtdDefeito,
                    QtdFalsaFalha = qtdFalsaFalha
                };

                indice.Add(indlinha);
            }

            return indice;
        }

	}
}
