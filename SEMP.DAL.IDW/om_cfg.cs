namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_cfg
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_cfg()
        {
            om_cfgptdetcoleta = new HashSet<om_cfgptdetcoleta>();
            om_cfgscrpimp = new HashSet<om_cfgscrpimp>();
            om_cfgurl = new HashSet<om_cfgurl>();
        }

        [Key]
        public long id_cfg { get; set; }

        public short? is_nivelfeeder { get; set; }

        public long? id_resguialimenta { get; set; }

        public int? revisao { get; set; }

        public short? st_ativo { get; set; }

        public long? id_tpptInsersora { get; set; }

        public long id_usrimpprog { get; set; }

        public long id_ccdefault { get; set; }

        public long? id_produto { get; set; }

        public long? id_tpgtFabrica { get; set; }

        public long? id_ft_paramCorrente { get; set; }

        public long? id_ft_paramFluxoE { get; set; }

        public long? id_ft_paramFlusoS { get; set; }

        public decimal? seg_feedbacklogin { get; set; }

        public decimal? seg_autologout { get; set; }

        public decimal? qt_maxptcoletafull { get; set; }

        public decimal? qt_maxetapateste { get; set; }

        public long? id_tpgtlogsuper { get; set; }

        public decimal? seg_heartbeat { get; set; }

        public long? id_tpptPM { get; set; }

        public long? id_tpptPTF { get; set; }

        public long? id_tpptPTS { get; set; }

        public long? id_tpptPTSCD { get; set; }

        public long? id_tpptPREPRO { get; set; }

        public long? id_tpptPPASS { get; set; }

        public long id_cal { get; set; }

        public long id_peproNORMAL { get; set; }

        public long id_peproCTREPROC { get; set; }

        public long? id_usrgrpSupervisor { get; set; }

        public long? id_usrgrpOperador { get; set; }

        public short? is_logonobrigatorio { get; set; }

        public long? id_estLiberado { get; set; }

        public long? id_estExpedicao { get; set; }

        public long? id_algocor { get; set; }

        public long? id_estRefugo { get; set; }

        [StringLength(100)]
        public string ds_mensagemSobreTensao { get; set; }

        [StringLength(100)]
        public string ds_mensagemSubTensao { get; set; }

        public long? id_ft_paramTensao { get; set; }

        public DateTime? dt_revisao { get; set; }

        public DateTime? dt_stativo { get; set; }

        public long id_usrRevisao { get; set; }

        public long id_usrStativo { get; set; }

        public decimal? qt_maxsubetapas { get; set; }

        public long? id_estProducao { get; set; }

        [StringLength(256)]
        public string mascaraCB { get; set; }

        public long? id_estMP { get; set; }

        public long? id_rapMagazine { get; set; }

        [StringLength(255)]
        public string url_smshumanmobile { get; set; }

        public long? id_gtImpCic { get; set; }

        public long? id_tparada { get; set; }

        public short? is_compensaapont { get; set; }

        public short? tp_layoutplano { get; set; }

        public short? is_processaiacinsert { get; set; }

        public short? is_processaiacidw { get; set; }

        public DateTime? dthr_estliberado { get; set; }

        public long? id_empresa { get; set; }

        [StringLength(256)]
        public string mascaraQtd { get; set; }

        public long? id_peproREGULAGEM { get; set; }

        public long? id_estAlimentacao { get; set; }

        public long? id_estlocalorigalim { get; set; }

        public long? id_ft_paramFP { get; set; }

        public long? id_ft_paramEC { get; set; }

        public long? id_ft_paramTemp { get; set; }

        public long? id_regras_ns { get; set; }

        public long? id_regras_cb { get; set; }

        public short? is_ihmtrocaop { get; set; }

        [StringLength(256)]
        public string mascarafolha { get; set; }

        public short? is_contaCicloImprodutivoRegula { get; set; }

        public long? id_trefugo { get; set; }

        public long? id_cfgabc { get; set; }

        public short? is_requerTecnicoInicioCIP { get; set; }

        public short? is_requerTecnicoFimCIP { get; set; }

        public long? id_talerta { get; set; }

        public long? id_tparadacip { get; set; }

        public long? id_tritmo { get; set; }

        public long? id_tparadasemconexao { get; set; }

        [StringLength(256)]
        public string mascaracdprodutocf { get; set; }

        [StringLength(256)]
        public string mascaracdprodutocb { get; set; }

        [StringLength(256)]
        public string mascaraop { get; set; }

        [StringLength(256)]
        public string mascaracdprodutomp { get; set; }

        public short? is_imp_mapa_qt_unica { get; set; }

        public virtual dw_cal dw_cal { get; set; }

        public virtual dw_est dw_est { get; set; }

        public virtual dw_est dw_est1 { get; set; }

        public virtual dw_est dw_est2 { get; set; }

        public virtual dw_est dw_est3 { get; set; }

        public virtual dw_est dw_est4 { get; set; }

        public virtual dw_est dw_est5 { get; set; }

        public virtual dw_estlocal dw_estlocal { get; set; }

        public virtual dw_ft_param dw_ft_param { get; set; }

        public virtual dw_ft_param dw_ft_param1 { get; set; }

        public virtual dw_ft_param dw_ft_param2 { get; set; }

        public virtual dw_ft_param dw_ft_param3 { get; set; }

        public virtual dw_ft_param dw_ft_param4 { get; set; }

        public virtual dw_ft_param dw_ft_param5 { get; set; }

        public virtual dw_ft_param dw_ft_param6 { get; set; }

        public virtual dw_pepro dw_pepro { get; set; }

        public virtual dw_pepro dw_pepro1 { get; set; }

        public virtual dw_pepro dw_pepro2 { get; set; }

        public virtual dw_rap dw_rap { get; set; }

        public virtual dw_t_alerta dw_t_alerta { get; set; }

        public virtual dw_t_parada dw_t_parada { get; set; }

        public virtual dw_t_parada dw_t_parada1 { get; set; }

        public virtual dw_t_parada dw_t_parada2 { get; set; }

        public virtual dw_t_refugo dw_t_refugo { get; set; }

        public virtual dw_t_ritmo dw_t_ritmo { get; set; }

        public virtual om_algocor om_algocor { get; set; }

        public virtual om_cc om_cc { get; set; }

        public virtual om_cfgabc om_cfgabc { get; set; }

        public virtual om_empresa om_empresa { get; set; }

        public virtual om_gt om_gt { get; set; }

        public virtual om_produto om_produto { get; set; }

        public virtual om_regras_nscb om_regras_nscb { get; set; }

        public virtual om_regras_nscb om_regras_nscb1 { get; set; }

        public virtual om_resgui om_resgui { get; set; }

        public virtual om_tpgt om_tpgt { get; set; }

        public virtual om_tpgt om_tpgt1 { get; set; }

        public virtual om_tppt om_tppt { get; set; }

        public virtual om_tppt om_tppt1 { get; set; }

        public virtual om_tppt om_tppt2 { get; set; }

        public virtual om_tppt om_tppt3 { get; set; }

        public virtual om_tppt om_tppt4 { get; set; }

        public virtual om_tppt om_tppt5 { get; set; }

        public virtual om_tppt om_tppt6 { get; set; }

        public virtual om_usrgrp om_usrgrp { get; set; }

        public virtual om_usrgrp om_usrgrp1 { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        public virtual om_usr om_usr2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfgptdetcoleta> om_cfgptdetcoleta { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfgscrpimp> om_cfgscrpimp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfgurl> om_cfgurl { get; set; }
    }
}
