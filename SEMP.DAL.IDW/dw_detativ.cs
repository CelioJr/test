namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_detativ
    {
        [Key]
        public long id_detativ { get; set; }

        public int? ordem { get; set; }

        public short? tp_detativ { get; set; }

        [Column(TypeName = "text")]
        public string texto { get; set; }

        [Column(TypeName = "image")]
        public byte[] foto { get; set; }

        public long id_procativ { get; set; }

        public virtual dw_procativ dw_procativ { get; set; }
    }
}
