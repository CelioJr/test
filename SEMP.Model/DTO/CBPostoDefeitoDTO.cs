using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
	public class CBPostoDefeitoDTO
	{
		[Display(Name = "Linha")]
		public int CodLinha { get; set; }
		[Display(Name = "Posto")]
		public int NumPosto { get; set; }
		[Display(Name = "Defeito")]
		public string CodDefeito { get; set; }

		[Display(Name = "Descri��o")]
		public string DesLinha { get; set; }
		[Display(Name = "Descri��o")]
		public string DesPosto { get; set; }
		[Display(Name = "Descri��o")]
		public string DesDefeito { get; set; }
	}
}
