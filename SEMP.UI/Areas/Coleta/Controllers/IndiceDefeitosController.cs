﻿using DotNet.Highcharts;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web.Mvc;
using RAST.BO.Relatorios;
using RAST.BO.Rastreabilidade;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using SEMP.Models;
using SEMP.Model.DTO;

namespace SEMP.Areas.Coleta.Controllers
{
	public class IndiceDefeitosController : Controller
	{
		//
		// GET: /Coleta/IndiceDefeitos/

		public ActionResult Index()
		{
			return View();
		}

		public ActionResult Linha(string datInicio, string datFim, string fase, int? codLinha, int? turno, string codModelo, string flgGrafico = "S", string familia = "")
		{
			var dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			var dFim = dIni.AddDays(1).AddMinutes(-1);	

			if (!String.IsNullOrEmpty(datInicio) && !String.IsNullOrEmpty(datFim))
			{
				dIni = DateTime.Parse(datInicio);
				dFim = DateTime.Parse(datFim);
			}						

			var fabrica = (fase == "PLACA") ? "FEC" : fase;
			var placa = (fase == "PLACA") ? 1 : 0;
			
			int qtdProducao = 0;
			string desLinha = "";			
			List<object> indicesFalsaFalha = new List<object>();
			List<object> indicesGeral = new List<object>();
			List<string> diasMes = new List<string>();

			if (codLinha != null)
			{
				List<viw_CBDefeitoTurnoDTO> Defeitos = Defeito.ObterDefeitosRel(dIni, dFim, codLinha);

				qtdProducao += RelatorioTecnico.ObterProducaoLinha(dIni, dFim, (int)codLinha);

				desLinha = Geral.ObterLinha(codLinha).DesLinha;

				if(Defeitos.Count() == 0 || qtdProducao == 0)
					return View();				

				var indicesDefeito = Defeitos.Where(x => x.FlgRevisado == 1).GroupBy(x => x.DatEvento.Date).Select(x => new IndiceDefeito()
				{
					Dia = (x.Key).Date,
					QtdProduzida = (RelatorioTecnico.ObterProducaoLinha(x.Key, x.Key, (int)codLinha)),
					QtdDefeitos = x.Count(),
					QtdFalsaFalha = x.Count(y => y.CodCausa != null && y.CodCausa.StartsWith("WX"))
					//IndiceGeral = Math.Round(((float)x.Count() / (float)(RelatorioTecnico.ObterProducaoLinha(x.Key, x.Key, (int)codLinha))) * 100, 2),
					//IndiceFalsaFalha = Math.Round(((float)x.Count(y => y.CodCausa != null && y.CodCausa.StartsWith("WX")) / (float)(RelatorioTecnico.ObterProducaoLinha(x.Key, x.Key, (int)codLinha))) * 100, 2),
					//IndiceSemFalsaFalha = Math.Round((((float)x.Count() / (float)(RelatorioTecnico.ObterProducaoLinha(x.Key, x.Key, (int)codLinha))) * 100) - (((float)x.Count(y => y.CodCausa != null && y.CodCausa.StartsWith("WX")) / (float)(RelatorioTecnico.ObterProducaoLinha(x.Key, x.Key, (int)codLinha))) * 100), 2)					
				}
				).OrderBy(x => x.Dia).ToList();

				foreach (var item in indicesDefeito)
				{
					item.IndiceGeral = Math.Round((float)item.QtdDefeitos / (float)item.QtdProduzida * 100, 2);
					item.IndiceFalsaFalha = Math.Round((float)item.QtdFalsaFalha / (float)item.QtdProduzida * 100, 2);
					item.IndiceSemFalsaFalha = Math.Round((((float)item.QtdDefeitos / (float)item.QtdProduzida * 100) - (((float)item.QtdFalsaFalha) / (float)item.QtdProduzida) * 100), 2);
				}

				ViewBag.indiceDefeitos = indicesDefeito;

				int produzido = indicesDefeito.Sum(x => x.QtdProduzida);
				int qtdDefeitos = indicesDefeito.Sum(x => x.QtdDefeitos);
				double indiceGeral = 0;				
				int qtdFalsaFalha = indicesDefeito.Sum(x => x.QtdFalsaFalha);
				double indiceFalsaFalha = 0;

				ViewBag.produzido = produzido;
				ViewBag.qtdDefeitos = qtdDefeitos;
				if (produzido > 0)
					indiceGeral = Math.Round(((float)qtdDefeitos / (float)produzido) * 100, 2);
				ViewBag.indiceGeral = indiceGeral;
				ViewBag.qtdFalsaFalha = qtdFalsaFalha;
				if (produzido > 0)
					indiceFalsaFalha = Math.Round((((float)qtdFalsaFalha / (float)produzido) * 100), 2);
				ViewBag.indiceFalsaFalha = indiceFalsaFalha;

				indicesDefeito.ForEach(a => indicesFalsaFalha.Add(a.IndiceFalsaFalha));
				indicesDefeito.ForEach(a => indicesGeral.Add(a.IndiceGeral));
				indicesDefeito.ForEach(a => diasMes.Add(a.Dia.ToShortDateString()));

			}
			else
			{
				List<viw_CBDefeitoTurnoDTO> Defeitos = Defeito.ObterDefeitosRel(dIni, dFim, null, fabrica, null, 0, null);

				qtdProducao += RelatorioTecnico.ObterProducaoFase(dIni, dFim, fabrica);

				desLinha = "Todas as linhas";

				if (Defeitos.Count() == 0 || qtdProducao == 0)
					return View();
				

				var indicesDefeito = Defeitos.Where(x => x.FlgRevisado == 1).GroupBy(x => x.DatEvento.Date).Select(x => new IndiceDefeito()
				{
					Dia = (x.Key).Date,
					QtdProduzida = (RelatorioTecnico.ObterProducaoFase(x.Key, x.Key, fabrica)),
					QtdDefeitos = x.Count(),
					QtdFalsaFalha = x.Count(y => y.CodCausa != null && y.CodCausa.StartsWith("WX")),
					//IndiceGeral = Math.Round(((double)x.Count() / (double)(RelatorioTecnico.ObterProducaoFase(x.Key, x.Key, fabrica))) * 100, 2),
					//IndiceFalsaFalha = Math.Round(((double)x.Count(y => y.CodCausa != null && y.CodCausa.StartsWith("WX")) / (double)(RelatorioTecnico.ObterProducaoFase(x.Key, x.Key, fabrica))) * 100, 2),
					//IndiceSemFalsaFalha = Math.Round((((double)x.Count() / (double)(RelatorioTecnico.ObterProducaoFase(x.Key, x.Key, fabrica))) * 100) - (((float)x.Count(y => y.CodCausa != null && y.CodCausa.StartsWith("WX")) / (double)(RelatorioTecnico.ObterProducaoFase(x.Key, x.Key, fabrica))) * 100), 2),					
				}
				).OrderBy(x => x.Dia).ToList();

				foreach (var item in indicesDefeito)
				{
					item.IndiceGeral = Math.Round((float)item.QtdDefeitos / (float)item.QtdProduzida * 100, 2);
					item.IndiceFalsaFalha = Math.Round((float)item.QtdFalsaFalha / (float)item.QtdProduzida * 100, 2);
					item.IndiceSemFalsaFalha = Math.Round((((float)item.QtdDefeitos / (float)item.QtdProduzida * 100) - (((float)item.QtdFalsaFalha) / (float)item.QtdProduzida) * 100), 2);
				}

				ViewBag.indiceDefeitos = indicesDefeito;

				int produzido = indicesDefeito.Sum(x => x.QtdProduzida);
				int qtdDefeitos = indicesDefeito.Sum(x => x.QtdDefeitos);
				double indiceGeral = 0;
				int qtdFalsaFalha = indicesDefeito.Sum(x => x.QtdFalsaFalha);
				double indiceFalsaFalha = 0;

				ViewBag.produzido = produzido;
				ViewBag.qtdDefeitos = qtdDefeitos;
				if (produzido > 0)
					indiceGeral = Math.Round(((float)qtdDefeitos / (float)produzido) * 100, 2);
				ViewBag.indiceGeral = indiceGeral;
				ViewBag.qtdFalsaFalha = qtdFalsaFalha;
				if (produzido > 0)
					indiceFalsaFalha = Math.Round((((float)qtdFalsaFalha / (float)produzido) * 100), 2);
				ViewBag.indiceFalsaFalha = indiceFalsaFalha;

				indicesDefeito.ForEach(a => indicesFalsaFalha.Add(a.IndiceFalsaFalha));
				indicesDefeito.ForEach(a => indicesGeral.Add(a.IndiceGeral));
				indicesDefeito.ForEach(a => diasMes.Add(a.Dia.ToShortDateString()));
			}

			if (flgGrafico == "S")
			{
                List<Series> series = new List<Series>
                {
                    new Series
                    {
                        Name = "Índice Falsa Falha",
                        Type = ChartTypes.Column,
                        YAxis = "1",
                        Data = new Data(indicesFalsaFalha.ToArray())
                    },

                    new Series
                    {
                        Name = "Índice Geral",
                        Color = ColorTranslator.FromHtml("#89A54E"),
                        Data = new Data(indicesGeral.ToArray())
                    }
                };
                Highcharts charts = Grafico.CombineGraph("Gráfico de Defeito", diasMes, series, "Nome", null, 600);			

				ViewBag.Charts = charts;
			}
			
			ViewBag.deslinha = desLinha;			


			return View();
		}

		public ActionResult Familia(string datInicio, string datFim, string fase, int? codLinha, int? turno, string codModelo, string flgGrafico = "S", string familia = "")
		{

			var dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			var dFim = dIni.AddDays(1).AddMinutes(-1);

			if (!String.IsNullOrEmpty(datInicio) && !String.IsNullOrEmpty(datFim))
			{
				dIni = DateTime.Parse(datInicio);
				dFim = DateTime.Parse(datFim);
			}			

			var fabrica = (fase == "PLACA") ? "FEC" : fase;
			var placa = (fase == "PLACA") ? 1 : 0;

			int qtdProducao = 0;
			List<object> indicesFalsaFalha = new List<object>();
			List<object> indicesGeral = new List<object>();
			List<string> diasMes = new List<string>();

			if (!String.IsNullOrEmpty(familia))
			{
				List<viw_CBDefeitoTurnoDTO> Defeitos = Defeito.ObterDefeitosRel(dIni, dFim, null, fabrica, null, 0, familia);

				qtdProducao += RelatorioTecnico.ObterProducaoFamilia(dIni, dFim, fase, familia);

				if (Defeitos.Count() == 0 || qtdProducao == 0)
					return View();				

				var indicesDefeito = Defeitos.Where(x => x.FlgRevisado == 1).GroupBy(x => x.DatEvento.Date).Select(x => new IndiceDefeito()
				{
					Dia = (x.Key).Date,
					QtdProduzida = (RelatorioTecnico.ObterProducaoFamilia(x.Key, x.Key, fase, familia)),
					QtdDefeitos = x.Count(),
					QtdFalsaFalha = x.Count(y => y.CodCausa != null && y.CodCausa.StartsWith("WX")),
					//IndiceGeral = Math.Round(((double)x.Count() / (float)(RelatorioTecnico.ObterProducaoFamilia(x.Key, x.Key, fase, familia))) * 100, 2),
					//IndiceFalsaFalha = Math.Round(((double)x.Count(y => y.CodCausa != null &&  y.CodCausa.StartsWith("WX")) / (double)(RelatorioTecnico.ObterProducaoFamilia(x.Key, x.Key, fase, familia))) * 100, 2),
					//IndiceSemFalsaFalha = Math.Round((((double)x.Count() / (double)(RelatorioTecnico.ObterProducaoFamilia(x.Key, x.Key, fase, familia))) * 100) - (((double)x.Count(y => y.CodCausa != null && y.CodCausa.StartsWith("WX")) / (double)(RelatorioTecnico.ObterProducaoFamilia(x.Key, x.Key, fase, familia))) * 100), 2)					
				}
				).OrderBy(x => x.Dia).ToList();

				foreach (var item in indicesDefeito)
				{
					item.IndiceGeral = Math.Round((float)item.QtdDefeitos / (float)item.QtdProduzida * 100, 2);
					item.IndiceFalsaFalha = Math.Round((float)item.QtdFalsaFalha / (float)item.QtdProduzida * 100, 2);
					item.IndiceSemFalsaFalha = Math.Round((((float)item.QtdDefeitos / (float)item.QtdProduzida * 100) - (((float)item.QtdFalsaFalha) / (float)item.QtdProduzida) * 100), 2);
				}

				ViewBag.indiceDefeitos = indicesDefeito;

				int produzido = indicesDefeito.Sum(x => x.QtdProduzida);
				int qtdDefeitos = indicesDefeito.Sum(x => x.QtdDefeitos);
				double indiceGeral = 0;
				int qtdFalsaFalha = indicesDefeito.Sum(x => x.QtdFalsaFalha);
				double indiceFalsaFalha = 0;

				ViewBag.produzido = produzido;
				ViewBag.qtdDefeitos = qtdDefeitos;
				if (produzido > 0)
					indiceGeral = Math.Round(((float)qtdDefeitos / (float)produzido) * 100, 2);
				ViewBag.indiceGeral = indiceGeral;
				ViewBag.qtdFalsaFalha = qtdFalsaFalha;
				if (produzido > 0)
					indiceFalsaFalha = Math.Round((((float)qtdFalsaFalha / (float)produzido) * 100), 2);
				ViewBag.indiceFalsaFalha = indiceFalsaFalha;

				indicesDefeito.ForEach(a => indicesFalsaFalha.Add(a.IndiceFalsaFalha));
				indicesDefeito.ForEach(a => indicesGeral.Add(a.IndiceGeral));
				indicesDefeito.ForEach(a => diasMes.Add(a.Dia.ToShortDateString()));
			}

			if (flgGrafico == "S")
			{

                List<Series> series = new List<Series>
                {
                    new Series
                    {
                        Name = "Índice Falsa Falha",
                        Type = ChartTypes.Column,
                        YAxis = "1",
                        Data = new Data(indicesFalsaFalha.ToArray())
                    },

                    new Series
                    {
                        Name = "Índice Geral",
                        Color = ColorTranslator.FromHtml("#89A54E"),
                        Data = new Data(indicesGeral.ToArray())
                    }
                };
                Highcharts charts = Grafico.CombineGraph("Gráfico de Defeito", diasMes, series, "Nome", null, 600);

				ViewBag.Charts = charts;
			}

			ViewBag.Familia = familia;			

			return View();
		}
		

	}
}
