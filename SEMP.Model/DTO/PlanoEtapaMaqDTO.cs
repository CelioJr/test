using System;

namespace SEMP.Model.DTO
{
    public partial class PlanoEtapaMaqDTO
    {
        public string CodModelo { get; set; }
        public string NumRevisao { get; set; }
        public string NumEtapa { get; set; }
        public string NomMaq { get; set; }
        public string CodProcesso { get; set; }
        public Nullable<decimal> TempoPadrao { get; set; }
        public System.Guid rowguid { get; set; }
        public short FlgLeftRight { get; set; }
        public string CodInsersora { get; set; }
        public string CodFam { get; set; }
        public string CodBloco { get; set; }
    }
}
