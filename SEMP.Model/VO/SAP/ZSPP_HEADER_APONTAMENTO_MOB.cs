﻿using System.Collections.Generic;

namespace SEMP.Model.VO.SAP
{
	public class ZSPP_HEADER_APONTAMENTO_MOB
	{
		public ZSPP_HEADER_APONTAMENTO_MOB() {
			ITEM = new List<ZSPP_ITEM_APONTAMENTO_MOB>();
		}

		public string LOTE { get; set; }
		public string ORDEM { get; set; }
		public int QUANTIDADE { get; set; }
		public double PESO { get; set; }
		public int CODLINHA { get; set; }
		public int CODPOSTO { get; set; }
		public List<ZSPP_ITEM_APONTAMENTO_MOB> ITEM { get; set; }

	}
}
