namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_expcvs
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_expcvs()
        {
            dw_expcvspf = new HashSet<dw_expcvspf>();
        }

        [Key]
        public long id_expcvs { get; set; }

        [StringLength(30)]
        public string cd_expcvs { get; set; }

        [StringLength(100)]
        public string ds_expcvs { get; set; }

        public DateTime? dthr_iEntrada { get; set; }

        public DateTime? dthr_fEntrada { get; set; }

        [StringLength(256)]
        public string sku { get; set; }

        [StringLength(100)]
        public string complemento { get; set; }

        [StringLength(40)]
        public string nserieIncial { get; set; }

        [StringLength(40)]
        public string nserieFinal { get; set; }

        public long? id_usrSupervisor { get; set; }

        public long? id_usrOperador { get; set; }

        public long? id_pt { get; set; }

        public long? id_produto { get; set; }

        public long? id_ft_etapa { get; set; }

        public long? id_tdefeitoReprocesso { get; set; }

        public long? id_tdefeito { get; set; }

        public short? is_apenasComFalha { get; set; }

        public short? is_apenasComFalhaReprocesso { get; set; }

        public short? is_apenasSucessoReprocesso { get; set; }

        public DateTime? dthr_iReprocesso { get; set; }

        public DateTime? dthr_fReprocesso { get; set; }

        public long? id_tacao { get; set; }

        public decimal? correnteMinima { get; set; }

        public decimal? correnteMaxima { get; set; }

        public decimal? tensaoMinima { get; set; }

        public decimal? tensaoMaxima { get; set; }

        public short? st_fluxoEntrada { get; set; }

        public short? st_fluxoSaida { get; set; }

        public decimal? qt_totallinhas { get; set; }

        public decimal? qt_linhasporarquivo { get; set; }

        public short? tp_exportacao { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_expcvspf> dw_expcvspf { get; set; }

        public virtual dw_ft_etapa dw_ft_etapa { get; set; }

        public virtual om_produto om_produto { get; set; }

        public virtual om_pt om_pt { get; set; }

        public virtual dw_t_acao dw_t_acao { get; set; }

        public virtual dw_t_defeito dw_t_defeito { get; set; }

        public virtual dw_t_defeito dw_t_defeito1 { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }
    }
}
