using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_CONSULTA_AVANCADA_DTO
    {
        public int FRM_CODIGO { get; set; }
        public int CAV_CODIGO { get; set; }
        public string CAV_DESCRICAO { get; set; }
        public string CAV_TEXTO { get; set; }
        public virtual FR_FORMULARIO_DTO FR_FORMULARIO { get; set; }
    }
}
