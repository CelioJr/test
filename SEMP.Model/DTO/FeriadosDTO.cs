using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FeriadosDTO
    {
        public string Setor { get; set; }
        public System.DateTime DatFeriado { get; set; }
        public string DesFeriado { get; set; }
        public string DiaUtil { get; set; }
    }
}
