namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_folhaiac
    {
        [Key]
        public long id_folhaiac { get; set; }

        public long? id_folha { get; set; }

        public long? id_prg { get; set; }

        public decimal? qt_ativa { get; set; }

        public long id_produto { get; set; }

        public long? id_produtodois { get; set; }

        public decimal? qt_mpporciclo { get; set; }

        public virtual dw_folha dw_folha { get; set; }

        public virtual om_prg om_prg { get; set; }

        public virtual om_produto om_produto { get; set; }

        public virtual om_produto om_produto1 { get; set; }
    }
}
