﻿using SEMP.DAL.Models;
using SEMP.Model.DTO;
using System.Collections.Generic;
using System.Linq;

namespace SEMP.DAL.DAO.Materiais
{
	public class daoPlanoEDP
	{

		public static PlanoIACDTO ObterPlanoIAC(string codModelo, string numRevisao)
		{

			using (var contexto = new DTB_SIMContext())
			{

				var dados = (from p in contexto.tbl_PlanoIAC
							 from i in contexto.tbl_Item.Where(x => x.CodItem == p.CodModelo).DefaultIfEmpty()
							 where p.CodModelo == codModelo && p.NumRevisao == numRevisao
							 select new PlanoIACDTO()
							 {
								 CodModelo = p.CodModelo,
								 Amplitude = p.Amplitude,
								 CodProcesso = p.CodProcesso,
								 CodStatus = p.CodStatus,
								 DatAtivo = p.DatAtivo,
								 DatInativo = p.DatInativo,
								 DatStatus = p.DatStatus,
								 DatUltAlt = p.DatUltAlt,
								 FlgLeftRight = p.FlgLeftRight,
								 NomAprovacao = p.NomAprovacao,
								 NomDocLDC = p.NomDocLDC,
								 NomEmissor = p.NomEmissor,
								 NumRevisao = p.NumRevisao,
								 rowguid = p.rowguid,
								 DesModelo = i.DesItem
							 }).FirstOrDefault();


				return dados;

			}

		}
		public static List<PlanoIACDTO> ObterPlanosIAC(string codModelo, string numRevisao)
		{

			using (var contexto = new DTB_SIMContext())
			{

				var dados = (from p in contexto.tbl_PlanoIAC
							 from i in contexto.tbl_Item.Where(x => x.CodItem == p.CodModelo).DefaultIfEmpty()
							 where p.CodModelo == codModelo && (numRevisao == null || p.NumRevisao == numRevisao)
							 select new PlanoIACDTO()
							 {
								 CodModelo = p.CodModelo,
								 Amplitude = p.Amplitude,
								 CodProcesso = p.CodProcesso,
								 CodStatus = p.CodStatus,
								 DatAtivo = p.DatAtivo,
								 DatInativo = p.DatInativo,
								 DatStatus = p.DatStatus,
								 DatUltAlt = p.DatUltAlt,
								 FlgLeftRight = p.FlgLeftRight,
								 NomAprovacao = p.NomAprovacao,
								 NomDocLDC = p.NomDocLDC,
								 NomEmissor = p.NomEmissor,
								 NumRevisao = p.NumRevisao,
								 rowguid = p.rowguid,
								 DesModelo = i.DesItem
							 }).ToList();


				return dados;

			}

		}

		public static List<PlanoIACDTO> ObterPlanosIAC(string codModelo)
		{

			using (var contexto = new DTB_SIMContext())
			{
				return ObterPlanosIAC(codModelo, null);

			}

		}

		/// <summary>
		/// Busca os itens de uma ordem de alimentação a partir de uma revisão especificada
		/// </summary>
		/// <param name="codModelo"></param>
		/// <param name="numRevisao"></param>
		/// <returns></returns>
		public static List<PlanoIACItemDTO> ObterPlanoIACItem(string codModelo, string numRevisao)
		{

			using (var contexto = new DTB_SIMContext())
			{

				var dados = (from p in contexto.tbl_PlanoIACItem
							 from i in contexto.tbl_Item.Where(x => x.CodItem == p.CodItem).DefaultIfEmpty()
							 where p.CodModelo == codModelo && p.NumRevisao == numRevisao
							 select new PlanoIACItemDTO()
							 {
								 CodModelo = p.CodModelo,
								 CodProcesso = p.CodProcesso,
								 FlgLeftRight = p.FlgLeftRight,
								 NumRevisao = p.NumRevisao,
								 rowguid = p.rowguid,
								 CodItem = p.CodItem,
								 DesLeftRight = p.DesLeftRight,
								 NumEstacaoZ = p.NumEstacaoZ,
								 NumEtapa = p.NumEtapa,
								 NumPosicao = p.NumPosicao,
								 Polaridade = p.Polaridade,
								 DesItem = i.DesItem
							 }).ToList();


				return dados;

			}

		}

		/// <summary>
		/// Busca os itens de uma ordem de alimentação a partir da última revisão do modelo
		/// </summary>
		/// <param name="codModelo"></param>
		/// <returns></returns>
		public static List<PlanoIACItemDTO> ObterPlanoIACItem(string codModelo)
		{

			using (var contexto = new DTB_SIMContext())
			{
				var numRevisao = "";

				var planoIAC = contexto.tbl_PlanoIAC.Where(x => x.CodModelo == codModelo).ToList();

				if (planoIAC.Count() > 0)
				{
					numRevisao = planoIAC.Where(x => x.CodStatus.Trim() == "3").Max(x => x.NumRevisao);

				}

				var dados = ObterPlanoIACItem(codModelo, numRevisao);

				return dados;

			}

		}

		public static List<PlanoEtapaDTO> ObterPlanoEtapa(string codModelo, string numRevisao)
		{

			using (var contexto = new DTB_SIMContext())
			{

				var dados = contexto.tbl_PlanoEtapa.Where(x => x.CodModelo == codModelo && x.NumRevisao == numRevisao).ToList();

				var dto = new List<PlanoEtapaDTO>();

				AutoMapper.Mapper.Map(dados, dto);

				return dto;

			}


		}
	}
}
