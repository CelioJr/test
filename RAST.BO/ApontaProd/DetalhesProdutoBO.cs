﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.Apontaprod;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.VO.Rastreabilidade.Relatorios;

namespace RAST.BO.Relatorios
{
	public class DetalhesProdutoBO
	{
		public static LeiturasMF ObterLeituraModelo(String Codigo, String NumSerie)
		{
			return daoAcompanhamento.ObterLeiturasPorModelo(Codigo, NumSerie).FirstOrDefault();
		}

		public static List<LeiturasItem> ObterItensLeitura(String Codigo, String NumSerie)
		{
			var leituras = daoDetalhesProduto.ObterLeiturasItems(Codigo, NumSerie);

			if (leituras == null || !leituras.Any())
			{
				leituras = daoDetalhesProduto.ObterLeiturasAmarra(Codigo, NumSerie);
			}

			return leituras;
		}
	}
}
