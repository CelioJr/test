using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
	public partial class CapacMdoLinhaDTO
	{
		[Display(Name = "Linha")]
		public string CodLinha { get; set; }
		[Display(Name = "Descri��o")]
		public int? Quantidade { get; set; }
		[Display(Name = "Data In�cio")]
		public System.DateTime? DatInicio { get; set; }
		[Display(Name = "Data Fim")]
		public System.DateTime? DatFim { get; set; }
		[Display(Name = "Turno")]
		public int NumTurno { get; set; }

		[Display(Name = "Descri��o")]
		public string DesLinha { get; set; }

	}
}
