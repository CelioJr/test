namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_tags
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_tags()
        {
            om_regras_tags = new HashSet<om_regras_tags>();
        }

        [Key]
        public long id_tags { get; set; }

        [StringLength(60)]
        public string cd_tags { get; set; }

        [StringLength(100)]
        public string ds_tags { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_regras_tags> om_regras_tags { get; set; }
    }
}
