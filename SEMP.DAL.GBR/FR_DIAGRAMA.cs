namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_DIAGRAMA
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DGR_COD { get; set; }

        [StringLength(120)]
        public string DGR_NOME { get; set; }

        [StringLength(80)]
        public string DGR_AUTOR { get; set; }

        [StringLength(20)]
        public string DGR_VERSAO { get; set; }

        public int? DGR_TIPOTABELA { get; set; }

        public DateTime? DGR_DATACRIACAO { get; set; }

        public DateTime? DGR_ATUALIZACAO { get; set; }

        public string DGR_COMENTARIO { get; set; }

        public int? DGR_PADRAOFK { get; set; }

        public string DGR_LAYOUT { get; set; }
    }
}
