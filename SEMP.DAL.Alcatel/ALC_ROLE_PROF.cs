namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_ROLE_PROF")]
    public partial class ALC_ROLE_PROF
    {
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal id { get; set; }

        public int role { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? profile_id { get; set; }

        public virtual ALC_PROFILE ALC_PROFILE { get; set; }
    }
}
