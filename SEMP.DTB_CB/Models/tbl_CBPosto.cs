using System;
using System.Collections.Generic;

namespace SEMP.DTB_CB.Models
{
    public partial class tbl_CBPosto
    {
        public tbl_CBPosto()
        {
            this.tbl_CBPostoDefeito = new List<tbl_CBPostoDefeito>();
        }

        public int CodLinha { get; set; }
        public int NumPosto { get; set; }
        public int CodTipoPosto { get; set; }
        public string DesPosto { get; set; }
        public string NumIP { get; set; }
        public string CodDRT { get; set; }
        public string FlgTipoTerminal { get; set; }
        public string FlgAtivo { get; set; }
        public int NumSeq { get; set; }
        public Nullable<bool> flgObrigatorio { get; set; }
        public Nullable<bool> flgDRTObrig { get; set; }
        public string TipoAmarra { get; set; }
        //public string flgPedeAP { get; set; }
        //public string flgImprimeEtq { get; set; }
        //public Nullable<int> TempoLeitura { get; set; }
        //public Nullable<int> TipoEtiqueta { get; set; }
        public virtual tbl_CBLinha tbl_CBLinha { get; set; }
        public virtual tbl_CBTipoPosto tbl_CBTipoPosto { get; set; }
        public virtual ICollection<tbl_CBPostoDefeito> tbl_CBPostoDefeito { get; set; }
    }
}
