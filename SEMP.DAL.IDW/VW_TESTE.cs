namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class VW_TESTE
    {
        public long? ID_UP { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long ID_EVT { get; set; }

        [StringLength(255)]
        public string CB { get; set; }

        [StringLength(40)]
        public string CD_ACAO { get; set; }

        [StringLength(40)]
        public string CD_ALERTA { get; set; }

        [StringLength(40)]
        public string CD_CAUSA { get; set; }

        [StringLength(60)]
        public string CD_COMPONENTE { get; set; }

        [StringLength(40)]
        public string CD_CONSULTA { get; set; }

        [StringLength(4)]
        public string CD_ESTRUTURA { get; set; }

        [StringLength(20)]
        public string cd_feeder { get; set; }

        [StringLength(40)]
        public string CD_JUSTIFICATIVA { get; set; }

        [StringLength(40)]
        public string CD_MOLDE { get; set; }

        [StringLength(20)]
        public string cd_nozzle { get; set; }

        [StringLength(40)]
        public string CD_PARADA { get; set; }

        [StringLength(40)]
        public string CD_PRODUTO { get; set; }

        [StringLength(30)]
        public string CD_PRODUTOREDZ { get; set; }

        [StringLength(40)]
        public string CD_REFUGO { get; set; }

        [StringLength(40)]
        public string CD_TEC1 { get; set; }

        [StringLength(40)]
        public string CD_TEC2 { get; set; }

        [StringLength(40)]
        public string CD_TECRESP { get; set; }

        [StringLength(100)]
        public string depara { get; set; }

        public DateTime? DTHR_CADASTROBANCO { get; set; }

        public DateTime? DTHR_CADASTROSERVER { get; set; }

        public DateTime? DTHR_ENVIOPRCOLETOREVENTOS { get; set; }

        public DateTime? DTHR_EVENTO { get; set; }

        public DateTime? DTHR_EVENTOANTERIOR { get; set; }

        public DateTime? DTHR_IPROCESSASERVER { get; set; }

        public DateTime? DTHR_PROCESSASERVER { get; set; }

        public decimal? ENERGIACONSUMIDA { get; set; }

        [StringLength(512)]
        public string ERROCONSOL { get; set; }

        public decimal? FATORPOTENCIA { get; set; }

        public long? ID_PRCOLETOREVENTOS { get; set; }

        public short? IS_REGULAGEM { get; set; }

        [StringLength(40)]
        public string LOGIN { get; set; }

        public long? MILI_DURACAOEVENTO { get; set; }

        public long? ID_EVTINICIO { get; set; }

        public long? ID_MSICUP { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long ID_TPEVT { get; set; }

        public long? ID_UPIHM { get; set; }

        [StringLength(40)]
        public string NROP { get; set; }

        [StringLength(255)]
        public string ORIGEM { get; set; }

        public int? qt_erromontagem { get; set; }

        public decimal? QTDE { get; set; }

        public decimal? QTDE_CICLOS { get; set; }

        public int? SEQUENCIAL { get; set; }

        public int? ST_EVT { get; set; }

        public decimal? TEMPERATURA { get; set; }

        public int? tp_erromontagem { get; set; }
    }
}
