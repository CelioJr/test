namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_ACAO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FR_ACAO()
        {
            FR_ACAOCOMPONENTE = new HashSet<FR_ACAOCOMPONENTE>();
            FR_ACAOPARAMETRO = new HashSet<FR_ACAOPARAMETRO>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ACO_CODIGO { get; set; }

        [Required]
        [StringLength(30)]
        public string ACO_NOME { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_ACAOCOMPONENTE> FR_ACAOCOMPONENTE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_ACAOPARAMETRO> FR_ACAOPARAMETRO { get; set; }
    }
}
