using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEMP.DAL.Models
{    
    public partial class tbl_CBTempoModelo
    {
        public int Id { get; set; }
        public string CodIdentificador { get; set; }
        public System.DateTime DatVigenciaIni { get; set; }
        public System.DateTime DatVigenciaFim { get; set; }
        public string flgTipoProduto { get; set; }
        public string CodTempoMod { get; set; }
        public Nullable<decimal> ValTempoPadrao { get; set; }
        public decimal ValTempoMontagem { get; set; }
        public string flgAtivo { get; set; }
        public Nullable<decimal> ValPrepPlaca { get; set; }
        public Nullable<decimal> ValWakeUp { get; set; }
        public Nullable<decimal> ValRunIn { get; set; }
        public Nullable<decimal> ValTesteFinal { get; set; }
        public Nullable<decimal> ValDownload { get; set; }
        public Nullable<decimal> ValPrepKits { get; set; }
        public Nullable<decimal> ValEmbalagem { get; set; }
        public Nullable<decimal> ValSerigrafiaGab { get; set; }
        public Nullable<decimal> ValTampografiaFront { get; set; }
        public Nullable<decimal> ValTampografiaLCD { get; set; }
        public Nullable<decimal> ValTampografiaCover { get; set; }
        public Nullable<decimal> ValTampografiaBackCover { get; set; }
        public Nullable<decimal> ValMontagemGabinete { get; set; }
        public Nullable<decimal> ValMontagemFrontal { get; set; }
        public Nullable<decimal> ValMontagemFonte { get; set; }
        public Nullable<decimal> ValMontagemLCD { get; set; }
        public Nullable<decimal> ValPreTeste { get; set; }
        public Nullable<decimal> ValCalibGSensor { get; set; }       
    }
}
