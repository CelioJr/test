using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DATABASE_DTO
    {
        public FR_DATABASE_DTO()
        {
            this.FR_COMPILADOR_DATABASE = new List<FR_COMPILADOR_DATABASE_DTO>();
        }

        public string DBA_CODIGO { get; set; }
        public string DBA_DESCRICAO { get; set; }
        public virtual ICollection<FR_COMPILADOR_DATABASE_DTO> FR_COMPILADOR_DATABASE { get; set; }
    }
}
