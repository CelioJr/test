namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ZCSD_INTERFACE_REM_EMBARQUE
    {
        public int id { get; set; }

        [Required]
        [StringLength(16)]
        public string PACKNO { get; set; }

        [Required]
        [StringLength(10)]
        public string EXTROW { get; set; }

        [StringLength(12)]
        public string NRREMESSA { get; set; }

        [StringLength(8)]
        public string ITEMREMESSA { get; set; }

        [StringLength(10)]
        public string DATAREMESSA { get; set; }

        [StringLength(48)]
        public string MATERIAL { get; set; }

        [StringLength(8)]
        public string LOCALEXPEDICAO { get; set; }

        [StringLength(16)]
        public string QTDREMESSA { get; set; }

        [StringLength(16)]
        public string QTDCONFIRMADA { get; set; }

        [StringLength(16)]
        public string TRANSPORTADORA { get; set; }

        [StringLength(14)]
        public string DOCVENDA { get; set; }

        [StringLength(8)]
        public string ITEMDOCVENDA { get; set; }

        [StringLength(8)]
        public string LOCALORGTRANSP { get; set; }

        [StringLength(12)]
        public string TRANSPORTADORA1 { get; set; }

        [StringLength(10)]
        public string DATAEMBARQUE { get; set; }

        [StringLength(12)]
        public string NUMERO { get; set; }

        [StringLength(13)]
        public string SERIENF { get; set; }

        [StringLength(4)]
        public string NUMERONF { get; set; }

        [StringLength(12)]
        public string CONHECIMENTO { get; set; }

        [StringLength(12)]
        public string NUMEROFATURA { get; set; }

        [StringLength(8)]
        public string CODIGOOCORRENCIA { get; set; }

        [StringLength(10)]
        public string DATAEMBARQUE1 { get; set; }

        [StringLength(39)]
        public string OBJETORASTREIO { get; set; }

        [StringLength(8)]
        public string FABRICA { get; set; }

        [StringLength(6)]
        public string ITINERARIO { get; set; }

        [StringLength(10)]
        public string INICIOTRANSPORTE { get; set; }
    }
}
