namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_TAREFA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_TAREFA()
        {
            tbl_dados = new HashSet<tbl_dados>();
            tbl_historico = new HashSet<tbl_historico>();
            tbl_logs = new HashSet<tbl_logs>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(20)]
        public string NOME { get; set; }

        [Required]
        [StringLength(150)]
        public string DESCRICAO { get; set; }

        public int ID_CRITICIDADE { get; set; }

        [Required]
        [StringLength(50)]
        public string ACTION { get; set; }

        public int ID_TIPO { get; set; }

        [Required]
        [StringLength(150)]
        public string COMANDO { get; set; }

        [Required]
        [StringLength(300)]
        public string EMAILRESPONSAVEIS { get; set; }

        public int TENTATIVAS { get; set; }

        public bool ATIVO { get; set; }

        public bool DELETADO { get; set; }

        public virtual tbl_criticidade tbl_criticidade { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_dados> tbl_dados { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_historico> tbl_historico { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_logs> tbl_logs { get; set; }

        public virtual tbl_tipo tbl_tipo { get; set; }
    }
}
