﻿using System.Web;
using System.Web.Optimization;

namespace SEMP
{
	public class BundleConfig
	{
		public static void RegisterBundles(BundleCollection bundles)
		{
			#region Scripts

			bundles.Add(new ScriptBundle("~/bundles/site")
							.Include("~/Scripts/jquery-{version}.js",
							"~/Scripts/jquery-ui-{version}.js",
							"~/Scripts/bootstrap.js",
							"~/Scripts/menu.js", 
							"~/Scripts/sisap.js"
							));

			bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-{version}.js"));

			bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include("~/Scripts/jquery-ui-{version}.js"));

			bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include("~/Scripts/jquery.unobtrusive*","~/Scripts/jquery.validate*"));

			bundles.Add(new ScriptBundle("~/bundles/login").Include("~/Scripts/login.js"));

			bundles.Add(new ScriptBundle("~/bundles/scripts").Include("~/Scripts/menu.js", "~/Scripts/sisap.js"));

			bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-*"));

			bundles.Add(new ScriptBundle("~/bundles/freewall").Include("~/Scripts/freewall.js"));

			bundles.Add(new ScriptBundle("~/bundles/datatable").Include("~/bower_components/datatables/media/js/jquery.dataTables.js"));
			bundles.Add(new ScriptBundle("~/bundles/datatable_bootstrap")
								.Include("~/bower_components/datatables/media/js/jquery.dataTables.js",
										 "~/bower_components/datatables/media/js/dataTables.bootstrap.js"));

			//bundles.Add(new ScriptBundle("~/bundles/datatable2").Include("~/Scripts/jquery.dataTables.min.js"));

			bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include("~/Scripts/bootstrap.js"));

			bundles.Add(new ScriptBundle("~/bundles/excelexport").Include("~/Scripts/jquery.btechco.excelexport.js"));

			bundles.Add(new ScriptBundle("~/bundles/floatThead").Include("~/Scripts/jquery.floatThead.js"));

			bundles.Add(new ScriptBundle("~/bundles/timepicker").Include("~/Scripts/jquery.ui.timepicker.js"));

			bundles.Add(new ScriptBundle("~/bundles/maskedinput").Include("~/Scripts/jquery.maskedinput.js"));

			bundles.Add(new ScriptBundle("~/bundles/highcharts")
							.Include("~/Scripts/Highcharts-4.0.1/js/highcharts-all.js",
							"~/Scripts/Highcharts-4.0.1/js/highcharts-3d.js",
							"~/Scripts/Highcharts-4.0.1/js/modules/data.js")
							.Include("~/Scripts/Highcharts-4.0.1/js/regression.js"));
			bundles.Add(new ScriptBundle("~/bundles/highcharts2").Include("~/Scripts/Highcharts-4.0.1/js/highcharts.js", "~/Scripts/Highcharts-4.0.1/js/modules/exporting.js", "~/Scripts/Highcharts-4.0.1/js/modules/drilldown.js", "~/Scripts/Highcharts-4.0.1/js/highcharts-3d.js"));

			bundles.Add(new ScriptBundle("~/bundles/tablesorter").Include("~/Scripts/jquery.tablesorter.js"));

			bundles.Add(new ScriptBundle("~/bundles/knockout").Include("~/Scripts/knockout-{version}.js", "~/Scripts/knockout.mapping-latest.js"));

			#endregion

			#region Styles
			bundles.Add(new StyleBundle("~/Content/csssite")
						.Include(
						"~/Content/header-theme.css",
						"~/Content/header.css",
						"~/Content/themes/base/jquery-ui.css",
						"~/Content/themes/base/jquery.ui.theme.css",
						"~/Content/bootstrap.css", 
						"~/Content/bootstrap-theme.css"
						));

			//bundles.Add(new StyleBundle("~/Content/datatable").Include("~/Content/themes/base/jquery.dataTables.css"));
			bundles.Add(new StyleBundle("~/Content/datatable").Include("~/bower_components/datatables/media/css/jquery.dataTables.css"));
			bundles.Add(new StyleBundle("~/Content/datatable_bootstrap")
								.Include("~/bower_components/datatables/media/css/jquery.dataTables.css",
										 "~/bower_components/datatables/media/css/dataTables.bootstrap.css"));

			bundles.Add(new StyleBundle("~/Content/login").Include("~/Content/reset.css", "~/Content/login.css"));

			bundles.Add(new StyleBundle("~/Content/jqueryui").Include("~/Content/themes/base/jquery-ui.css",
						"~/Content/themes/base/jquery.ui.theme.css"));

			bundles.Add(new ScriptBundle("~/bundles/combogrid").Include("~/Scripts/jquery.ui.combogrid.js"));
			bundles.Add(new StyleBundle("~/Content/combogrid").Include("~/Content/themes/base/jquery.ui.combogrid.css"));

			bundles.Add(new StyleBundle("~/Content/timepicker").Include("~/Content/themes/base/jquery.ui.timepicker.css"));

			bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
						"~/Content/themes/base/jquery.ui.core.css",
						"~/Content/themes/base/jquery.ui.resizable.css",
						"~/Content/themes/base/jquery.ui.selectable.css",
						"~/Content/themes/base/jquery.ui.accordion.css",
						"~/Content/themes/base/jquery.ui.autocomplete.css",
						"~/Content/themes/base/jquery.ui.button.css",
						"~/Content/themes/base/jquery.ui.dialog.css",
						"~/Content/themes/base/jquery.ui.slider.css",
						"~/Content/themes/base/jquery.ui.tabs.css",
						"~/Content/themes/base/jquery.ui.datepicker.css",
						"~/Content/themes/base/jquery.ui.progressbar.css",
						"~/Content/themes/base/jquery.ui.theme.css"));

			bundles.Add(new StyleBundle("~/Content/freewall").Include("~/Content/wall/style.css", "~/Content/wall/metro-style.css"));
            bundles.Add(new StyleBundle("~/Content/itensFormulario").Include("~/Content/itensFormularios.css"));
			bundles.Add(new StyleBundle("~/Content/postoEmbalagem").Include("~/Content/postoEmbalagem.css"));
			bundles.Add(new StyleBundle("~/Content/painelProducao").Include("~/Content/painelProducao.css"));
			bundles.Add(new StyleBundle("~/Content/painelProducaoLinha").Include("~/Content/painelProducaoLinha.css"));
			bundles.Add(new StyleBundle("~/Content/bootstrap").Include("~/Content/bootstrap_sisap.css", "~/Content/bootstrap-theme.css"));

			bundles.Add(new StyleBundle("~/Content/bootstrap2").Include("~/Content/bootstrap.css", "~/Content/bootstrap-theme.css"));
			
			#endregion

		}
	}
}