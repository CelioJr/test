﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using RAST.BO.Rastreabilidade;
using SEMP.DAL.DAO.Embalagem;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model;
using SEMP.Model.DTO;
using SEMP.Model.VO;
using SEMP.Model.VO.Embalagem;

namespace RAST.BO.Embalagem
{
	 
	public class EmbalagemDat : Embalagem, IEmbalagem
	{
        private new void DesfazerOperacoes(CBEmbaladaDTO embalagem, List<CBAmarraDTO> listaAmarra)
        {
            DaoEmbalagem.DesfazerOperacoes(embalagem, listaAmarra);
        }

        public static new ClassesAuxiliaresEmbalagem.ResultadoEmbalagemProduto ExecProcedure_EmbalarProduto(CBEmbaladaDTO embalada)
        {
            var dadosEmbalada = DaoEmbalagem.Exec_Procedure_EmbalarProduto(embalada);

            return dadosEmbalada;
        }


        public override Mensagem Embalar_Produto (CBEmbaladaDTO embalagem, List<CBAmarraDTO> listaAmarra, Passagem passagem, CBPostoDTO posto, bool gravarBancoSemp = true)
		{

            return base.Embalar_Produto(embalagem, listaAmarra, passagem, posto, false);


		}

		public override Mensagem ValidarLeitura(int tipoAmarra, string leitura, string numAp, string numSerie, string numKitProvisorio,
			CBLinhaDTO linha)
		{
			return new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "Lime", DesMensagem = "LEITURA OK. LEIA PRÓXIMO." };
		}

		public override Mensagem ValidarProduto(Passagem passagem, string codDrt, string numAp, string leitura, string modelo, string codFab,
			string codigoCinescopio, CBLinhaDTO linha)
		{
			return passagem.ValidaPassagem(leitura, codDrt, passagem.posto.CodLinha, passagem.posto.NumPosto, numAp);

		}

		public new string AbrirCaixaColetiva(int idLinha, string codModelo, string codFab, string umAP)
		{
			return base.AbrirCaixaColetiva(idLinha, codModelo, codFab, umAP);
		}

		public new Mensagem FecharCaixaColetiva(string numLote, string codDrt, string justificativa)
		{
			return base.FecharCaixaColetiva(numLote, codDrt, justificativa);
		}

		public new Mensagem FecharLote(string numLote, string codDrt, string justificativa)
		{
			var retorno = new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "LIME", DesMensagem = "LEITURA OK. LEIA PRÓXIMO." }; // Defeito.ObterMensagem(Constantes.RAST_OK);
			var resultado = DaoEmbalagem.FecharLote(numLote, codDrt, justificativa);

			if (resultado) return retorno;

			retorno = Defeito.ObterMensagem(Constantes.RAST_LOTE_NAOFINALIZADO);

			return retorno;
		}

		public new List<CBEmbaladaDTO> ProdutosDeLoteEmAberto(string numAp, string codModelo, int codLinha, string codFab)
		{
			return base.ProdutosDeLoteEmAberto(numAp, codModelo, codLinha, codFab);
		}

		public new List<CBEmbaladaDTO> RecuperarItensEmbaladosDaCaixaColetivaAberta(string numAp, string codModelo, int codLinha, string codFab)
		{
			return base.RecuperarItensEmbaladosDaCaixaColetivaAberta(numAp, codModelo, codLinha, codFab);
		}

		public override Mensagem ImprimirEtiquetasIndividuais(string numAp, string leitura, string codModelo, CBPostoDTO posto, double PesoCaixa)
		{
			return base.ImprimirEtiquetasIndividuais(numAp, leitura, codModelo, posto, PesoCaixa);
		}

		public override Mensagem ImprimirCaixaColetiva(string numeroCaixaColetiva, CBLinhaDTO linha, CBPostoDTO posto)
		{
			return base.ImprimirCaixaColetiva(numeroCaixaColetiva, linha, posto);
		}

		public new int QtdeCaixasColetivasNoLote(string numLote)
		{
			return base.QtdeCaixasColetivasNoLote(numLote);
		}

		public new bool AlterarTamanhoCaixaColetiva(int codLinha, int tamanho)
		{
			return base.AlterarTamanhoCaixaColetiva(codLinha, tamanho);
		}

		public new string RecuperarLoteAberto(string numAp, string codModelo, int codLinha, string codFab)
		{
			return base.RecuperarLoteAberto(numAp, codModelo, codLinha, codFab);
		}
		public new string RecuperarLoteAberto(string codModelo, int codLinha, string codFab)
		{

			return DaoEmbalagem.RecuperarNumeroLoteAberto(codModelo, codLinha, codFab);
		}

		public new virtual string RecuperarNumeroCaixaColetivaAberta(string numAp, int codLinha, string codFab)
		{
			return base.RecuperarNumeroCaixaColetivaAberta(numAp, codLinha, codFab);
		}
		public new virtual string RecuperarNumeroCaixaColetivaAberta(string numAp, string modelo, int codLinha, string codFab)
		{
			return base.RecuperarNumeroCaixaColetivaAberta(numAp, modelo, codLinha, codFab);
		}

		public new string ObterTotalProduzidoCaixaColetiva(string codFabrica, string numAp, int codLinha)
		{
			string numCC = RecuperarNumeroCaixaColetivaAberta(numAp, codLinha, codFabrica);
			int qtdeEmb = 0;

			if (!string.IsNullOrEmpty(numCC))
				qtdeEmb = DaoEmbalagem.QuantidadeItensEmbaladosCaixaColetiva(numCC);

			//string qtdePadrao = Geral.ObterQuantidadeCaixaColetivaPorLinha(codLinha).ToString();

			//return (qtdeEmb.ToString() + "/" + qtdePadrao);
			return qtdeEmb.ToString();
			//return base.ObterTotalProduzidoCaixaColetiva(codFabrica, numAp, codLinha);
		}

		public new string ObterTotalProduzidoCaixaColetiva(string codFabrica, string numAp, int codLinha, string codModelo)
		{
			string numCC = DaoEmbalagem.RecuperarNumeroCaixaColetivaAberta(numAp, codLinha, codFabrica, codModelo);
			int qtdeEmb = 0;

			if (!string.IsNullOrEmpty(numCC))
				qtdeEmb = DaoEmbalagem.QuantidadeItensEmbaladosCaixaColetiva(numCC);

			//string qtdePadrao = Geral.ObterQuantidadeCaixaColetivaPorLinha(codLinha).ToString();

			//return (qtdeEmb.ToString() + "/" + qtdePadrao);
			return qtdeEmb.ToString();
		}
	


	}
}
