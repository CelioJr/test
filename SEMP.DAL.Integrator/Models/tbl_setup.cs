namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_setup
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string NOME { get; set; }

        [Required]
        [StringLength(50)]
        public string VALOR { get; set; }
    }
}
