using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_SupervisorMap : EntityTypeConfiguration<tbl_Supervisor>
    {
        public tbl_SupervisorMap()
        {
            // Primary Key
            this.HasKey(t => t.CodDRT);

            // Properties
            this.Property(t => t.CodDRT)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8);

            this.Property(t => t.CodSeguranca)
                .IsRequired()
                .HasMaxLength(12);

            // Table & Column Mappings
            this.ToTable("tbl_Supervisor");
            this.Property(t => t.CodDRT).HasColumnName("CodDRT");
            this.Property(t => t.CodSeguranca).HasColumnName("CodSeguranca");
        }
    }
}
