namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_ANYTHING")]
    public partial class ALC_ANYTHING
    {
        [Key]
        public int id_anything { get; set; }

        [StringLength(50)]
        public string description { get; set; }

        [StringLength(50)]
        public string key_value { get; set; }

        public int? next_value { get; set; }
    }
}
