using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBPlanoAcaoTipoCausaMap : EntityTypeConfiguration<tbl_CBPlanoAcaoTipoCausa>
    {
        public tbl_CBPlanoAcaoTipoCausaMap()
        {
            // Primary Key
            this.HasKey(t => t.CodTipoCausa);

            this.Property(t => t.DesTipoCausa)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("tbl_CBPlanoAcaoTipoCausa");
            this.Property(t => t.CodTipoCausa).HasColumnName("CodTipoCausa");
            this.Property(t => t.DesTipoCausa).HasColumnName("DesTipoCausa");
        }
    }
}
