using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ProgMesResumoDTO
    {
        public string CodFab { get; set; }
        public System.DateTime DatProducao { get; set; }
        public string CodModelo { get; set; }
        public Nullable<int> QtdProdPm { get; set; }
        public string Usuario { get; set; }
        public System.Guid rowguid { get; set; }
        public string flgCobertura { get; set; }
        public Nullable<int> NumPrioridade { get; set; }
        public string Observacao { get; set; }
        public string FlagTP { get; set; }
    }
}
