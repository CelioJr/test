﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.DTO
{
	public class RelReposicaoDTO
	{
		public DateTime Dia{ get; set; }
		public int Romaneio { get; set; }
		public string Modelo { get; set; }
		public string Item { get; set; }
		public  decimal? QtdEstoque { get; set; }
		public string Status { get; set; }
		public DateTime? DatAprovacao { get; set; }
		public DateTime? DatRecebimento { get; set; }
		public DateTime? DatPagamento { get; set; }
		public string NumAP { get; set; }
		public  decimal? QtdItens { get; set; }		
		public  TimeSpan? TempoReposicao { get; set; }
		public TimeSpan? TempoAprovacao { get; set; }
		public TimeSpan? TempoRecebimento { get; set; }
	}
}
