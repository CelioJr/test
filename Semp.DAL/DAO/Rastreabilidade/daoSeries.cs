﻿using SEMP.DAL.Models;
using SEMP.Model.DTO;
using SEMP.Model.VO.Rastreabilidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.DAO.Rastreabilidade
{
	public class daoSeries
	{
		public string CodModelo { get; set; }
		private static List<EstruturaModelo> Estrutura { get; set; }

		public static List<EstruturaModelo> ObterEstrutura (string codModelo)
		{

			Estrutura = new List<EstruturaModelo>();

			ObterEstrutura(codModelo, 0);

			Estrutura = Estrutura.OrderBy(x => x.Indice).ToList();

			return Estrutura;

		}

		public static void ObterEstrutura(string codModelo, int indice)
		{
			using (var contexto = new DTB_CBContext())
			{
				var indice2 = indice;

				if (indice <= 0)
				{
					var pai = (from pi in contexto.tbl_PlanoItem
							   join item in contexto.tbl_Item on pi.CodModelo equals item.CodItem
							   join filho in contexto.tbl_Item on pi.CodItem equals filho.CodItem
							   join pcp in contexto.tbl_ItemPCP on pi.CodItem equals pcp.CodItem
							   where pi.CodItem == codModelo && (pi.DatFim == null || pi.DatFim >= DateTime.Now)
							   select new { pi.CodModelo, pi.CodItem, pcp.MovEstoque, DesModelo = item.DesItem.Trim(), DesItem = filho.DesItem.Trim() }
								).ToList();

					if (pai.Count() > 0)
					{
						foreach (var item in pai)
						{
							var fantasma = (item.MovEstoque == "N");
							var est = new EstruturaModelo(item.CodModelo, item.DesModelo, item.CodItem, item.DesItem, fantasma, indice);
							Estrutura.Add(est);

							indice2 = indice - 1;
							ObterEstrutura(item.CodModelo, indice2);
						}

					}
                    else
                    {
                        var desModelo = daoGeral.ObterDescricaoItem(codModelo);
                        Estrutura.Add(new EstruturaModelo(codModelo, desModelo, codModelo, desModelo, false, indice));
                    }

				}

				if (indice >= 0)
				{

					var filhos = (from pi in contexto.tbl_PlanoItem
								  join item in contexto.tbl_Item on pi.CodItem equals item.CodItem
								  join modelo in contexto.tbl_Item on pi.CodModelo equals modelo.CodItem
								  join pcp in contexto.tbl_ItemPCP on pi.CodItem equals pcp.CodItem
								  where pi.CodModelo == codModelo && (pi.DatFim == null || pi.DatFim >= DateTime.Now) && (item.CodFam == "031-03" || item.CodFam == "031-02") &&
										  (pcp.CodProcesso.StartsWith("01") ||
										  pcp.CodProcesso.StartsWith("02") ||
										  pcp.CodProcesso.StartsWith("03")
										  )
								  select new { pi.CodModelo, pi.CodItem, pcp.MovEstoque, DesModelo = modelo.DesItem.Trim(), DesItem = item.DesItem.Trim() }
								).ToList();
					if (filhos.Count() > 0)
					{
						foreach (var filho in filhos)
						{
							var fantasma = (filho.MovEstoque == "N");
							var est = new EstruturaModelo(filho.CodModelo, filho.DesModelo, filho.CodItem, filho.DesItem, fantasma, indice);

							Estrutura.Add(est);

							indice2 = indice + 1;
							ObterEstrutura(filho.CodItem, indice2);
						}
					}
				}

			}

		}

		public static List<CBEmbaladaDTO> ObterListaEmbalagem(DateTime datInicio, DateTime datFim, string codModelo) {

			var estrutura = ObterEstrutura(codModelo);
			var modelos = estrutura.Select(x => x.CodFilho).ToList();

			var produtoFinal = estrutura.OrderBy(x => x.Indice).FirstOrDefault();

			modelos.Remove(produtoFinal.CodModelo);

			var embalagens = new List<CBEmbaladaDTO>();

			using (var contexto = new DTB_CBContext())
			{
				var series = (from e in contexto.tbl_CBEmbalada
							  let emb = contexto.tbl_CBEmbalada
												  .Where(x => x.DatReferencia >= datInicio && x.DatReferencia <= datFim && modelos.Contains(x.CodModelo))
												  .Select(y => y.NumECB).Distinct().ToList()
							  where emb.Contains(e.NumECB)
							  select e).ToList();

                var resultado = series.Select(x => new CBEmbaladaDTO()
				{
					NumECB = x.NumECB,
					CodFab = x.CodFab,
					NumAP = x.NumAP,
					CodLinha = x.CodLinha,
					CodModelo = x.CodModelo,
					DatLeitura = x.DatLeitura
				}).ToList();

                var amarra = (from a in contexto.tbl_CBAmarra
                              let emb = contexto.tbl_CBEmbalada
                                                 .Where(x => x.DatReferencia >= datInicio && x.DatReferencia <= datFim && modelos.Contains(x.CodModelo))
                                                 .Select(y => y.NumECB).Distinct().ToList()
                              where emb.Contains(a.NumECB)
                              select a).ToList()
                              .Select(x => new CBEmbaladaDTO() {
                                NumECB = x.NumECB,
                                CodFab = x.CodFab,
                                NumAP = x.NumAP,
                                CodLinha = x.CodLinha.Value,
                                CodModelo = x.CodModelo,
                                DatLeitura = x.DatAmarra.Value
                              }).ToList();

                resultado.AddRange(amarra);


                return resultado;

				/*var emb = (from e in contexto.tbl_CBEmbalada
						   where e.DatReferencia >= datInicio && e.DatReferencia <= datFim && modelos.Contains(e.CodModelo)
						   group e by new { e.NumECB } into grupo
						   select new { NumECB = grupo.Key }).ToList();

				var series2= contexto.tbl_CBEmbalada.Where(x => emb.Contains(x.CodModelo)).ToList();*/
			}
			

		}

        public static List<CBEmbaladaDTO> ObterListaEmbalagemAP(string codFab, string numAP)
        {

            var embalagens = new List<CBEmbaladaDTO>();

            using (var contexto = new DTB_CBContext())
            {
                var series = (from e in contexto.tbl_CBEmbalada
                              let emb = contexto.tbl_CBEmbalada
                                                  .Where(x => x.CodFab == codFab && x.NumAP == numAP)
                                                  .Select(y => y.NumECB).Distinct().ToList()
                              where emb.Contains(e.NumECB)
                              select e).ToList();

                return series.Select(x => new CBEmbaladaDTO()
                {
                    NumECB = x.NumECB,
                    CodFab = x.CodFab,
                    NumAP = x.NumAP,
                    CodLinha = x.CodLinha,
                    CodModelo = x.CodModelo,
                    DatLeitura = x.DatLeitura
                }).ToList();

                /*var emb = (from e in contexto.tbl_CBEmbalada
						   where e.DatReferencia >= datInicio && e.DatReferencia <= datFim && modelos.Contains(e.CodModelo)
						   group e by new { e.NumECB } into grupo
						   select new { NumECB = grupo.Key }).ToList();

				var series2= contexto.tbl_CBEmbalada.Where(x => emb.Contains(x.CodModelo)).ToList();*/
            }


        }


    }

}
