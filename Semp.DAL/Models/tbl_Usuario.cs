using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_Usuario
    {
        public string CodFab { get; set; }
        public string NomUsuario { get; set; }
        public string NomDepto { get; set; }
        public string FullName { get; set; }
        public string FlgAtivo { get; set; }
        public string Email { get; set; }
        public string NomDeptoAD { get; set; }
        public string Cpf { get; set; }
    }
}
