﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
	public class LeiturasItem
	{
		[Display(Name = "Item")]
		public String CodItem { get; set; }
		[Display(Name = "Descrição")]
		public String DesItem { get; set; }
		[Display(Name = "Num. Série")]
		public String NumSerie { get; set; }
		[Display(Name = "Data Leitura")]
		public DateTime? DataLeitura { get; set; }
	}
}
