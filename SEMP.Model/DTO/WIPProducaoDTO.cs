using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
    public partial class WIPProducaoDTO
    {
		[Display(Name="Local")]
        public string CodLocal { get; set; }
		[Display(Name = "Item")]
        public string CodItem { get; set; }
		[Display(Name = "Fam�lia")]
        public string Familia { get; set; }

		[Display(Name = "Estoque")]
		[DisplayFormat(DataFormatString = "{0:n2}")]
        public Nullable<decimal> QtdEstoque { get; set; }

		[DisplayFormat(DataFormatString = "{0:n2}")]
        public Nullable<decimal> Valor { get; set; }

		[Display(Name = "Descri��o")]
		public string DesItem { get; set; }
    }
}
