﻿using SAP.Middleware.Connector;
using SEMP.Model.DTO;
using SEMP.Model.VO.SAP;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace SEMP.SAPConnector
{
	public class SAPFunctions
	{

		public static RfcDestination ConnectSAP()
		{
#if DEBUG
			var SapRfcDestination = RfcDestinationManager.GetDestination("QAS");
#else
			var SapRfcDestination = RfcDestinationManager.GetDestination("PRD");
#endif

			return SapRfcDestination;

		}

		public static bool TestaConn()
		{

			try
			{
				var conexao = ConnectSAP();

				conexao.Ping();

				return !conexao.IsShutDown;
			}
			catch
			{

				return false;
			}

		}

		/// <summary>
		/// Obter tabela de dados do SAP
		/// </summary>
		/// <param name="nomTabela">Nome da Tabela</param>
		/// <param name="campos">Quais os campos que serão trazidos. Se em branco retorna todos os dados</param>
		/// <param name="filtroCampo">Campo a ser filtrada a tabela. Se em branco retorna todos os dados</param>
		/// <param name="filtroValor">Valor a ser filtrado se o campo for informado</param>
		/// <param name="maxResult">Limitador de resultado. [-1] retorna todos os registros </param>
		/// <returns>Retorna uma String com os dados tabulados com "|"</returns>
		public static string ObterDados(string nomTabela, string campos, string filtroCampo, string filtroValor, int maxResult = -1)
		{

			try
			{
				var SapRfcDestination = ConnectSAP();

				RfcSessionManager.BeginContext(SapRfcDestination);

				var funcInvHdrs = SapRfcDestination.Repository.CreateFunction("RFC_READ_TABLE");

				funcInvHdrs.SetValue("query_table", nomTabela);
				funcInvHdrs.SetValue("delimiter", "|");

				if (maxResult > -1)
				{
					funcInvHdrs.SetValue("ROWCOUNT", maxResult);
				}

				if (!String.IsNullOrEmpty(filtroCampo))
				{
					var opcoes = funcInvHdrs.GetTable("OPTIONS");
					opcoes.Append();
					opcoes.SetValue("TEXT", filtroCampo + " EQ '" + filtroValor + "'");
				}

				if (!String.IsNullOrEmpty(campos))
				{
					var fieldsTable = funcInvHdrs.GetTable("FIELDS");
					var allFields = campos.Split('|');
					foreach (var item in allFields)
					{
						fieldsTable.Append();
						fieldsTable.SetValue("FIELDNAME", item);
					}
				}

				funcInvHdrs.Invoke(SapRfcDestination);

				IRfcTable tblInvHdr = funcInvHdrs.GetTable("DATA");

				var dados = tblInvHdr.ToString();
				dados = dados.Replace("TABLE  [STRUCTURE TAB512 { FIELD WA=", "");
				dados = dados.Replace("[STRUCTURE TAB512 { FIELD WA=", "");
				dados = dados.Replace("}]", "\n");

				RfcSessionManager.EndContext(SapRfcDestination);

				return dados.TrimStart();
			}
			catch (RfcCommunicationException comex)
			{
				Console.WriteLine("Erro: " + comex.Message);
				return "Erro: " + comex.Message;
			}
			catch (RfcLogonException ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return "Erro: " + ex.Message;
			}
			catch (RfcAbapRuntimeException ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return "Erro: " + ex.Message;
			}
			catch (RfcAbapBaseException ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return "Erro: " + ex.Message;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return "Erro: " + ex.Message;
			}
		}

		/// <summary>
		/// Obter tabela de dados do SAP
		/// </summary>
		/// <param name="nomTabela">Nome da Tabela</param>
		/// <param name="campos">Quais os campos que serão trazidos. Se em branco retorna todos os dados. Delimitador : |</param>
		/// <param name="filtroCampo">Campo a ser filtrada a tabela. Se em branco retorna todos os dados</param>
		/// <param name="filtroValor">Valor a ser filtrado se o campo for informado</param>
		/// <param name="maxResult">Limitador de resultado. [-1] retorna todos os registros </param>
		/// <returns>Retorna um DataTable com os dados organizados</returns>
		public static DataTable ObterTabela(string nomTabela, string campos, string query, int maxResult = -1)
		{
			try
			{
				var conexao = ConnectSAP();

				RfcSessionManager.BeginContext(conexao);

				RfcRepository repo = conexao.Repository;

				IRfcFunction testfn = repo.CreateFunction("RFC_READ_TABLE");

				testfn.SetValue("query_table", nomTabela);
				testfn.SetValue("delimiter", "|");

				if (maxResult > -1)
				{
					testfn.SetValue("ROWCOUNT", maxResult);
				}

				if (!String.IsNullOrEmpty(query))
				{
					var opcoes = testfn.GetTable("OPTIONS");
					string[] separador = new string[] { " AND " };
					if (query.Contains(" AND "))
					{
						var listaOpcoes = query.Split(separador, StringSplitOptions.RemoveEmptyEntries);
						for (var i = 0; i < listaOpcoes.Count(); i++)
						{
							var texto = listaOpcoes[i];
							if (i < listaOpcoes.Count() - 1)
							{
								texto = texto + " AND ";
							}

							opcoes.Append();
							opcoes.SetValue("TEXT", texto);
						}
					}
					else
					{
						opcoes.Append();
						opcoes.SetValue("TEXT", query);
					}
				}

				if (!String.IsNullOrEmpty(campos))
				{
					var fieldsTable = testfn.GetTable("FIELDS");
					var allFields = campos.Split('|');
					foreach (var item in allFields)
					{
						fieldsTable.Append();
						fieldsTable.SetValue("FIELDNAME", item);
					}
				}

				testfn.Invoke(conexao);

				var resultado = testfn.ToDataTable();

				RfcSessionManager.EndContext(conexao);

				return resultado;
			}
			catch (RfcCommunicationException comex)
			{
				Console.WriteLine("Erro: " + comex.Message);
				return null;
			}
			catch (RfcLogonException ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return null;
			}
			catch (RfcAbapRuntimeException ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return null;
			}
			catch (RfcAbapBaseException ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return null;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return null;
			}

		}

		/// <summary>
		/// Obter tabela de dados do SAP
		/// </summary>
		/// <param name="queryTable">Nome da Tabela</param>
		/// <param name="delimiter">Delimitador de resultado</param>
		/// <param name="noData">Caractere a ser usado quando resultado for vazio</param>
		/// <param name="rowSkips">Quantidade de registros a serem pulados (para paginação)</param>
		/// <param name="rowCount">Limitador de resultado. [0] retorna todos os registros </param>
		/// <param name="options">Opções a serem usadas para filtrar a tabela. Se em branco retorna todos os dados</param>
		/// <param name="fields">Quais os campos que serão trazidos. Se em branco retorna todos os dados. </param>
		/// <returns>Retorna um DataTable com os dados organizados</returns>
		public static DataTable ObterTabela(string queryTable, string delimiter, string noData, int rowSkips, int rowCount, string options, string fields)
		{
			try
			{
				var conexao = ConnectSAP();

				RfcSessionManager.BeginContext(conexao);

				RfcRepository repo = conexao.Repository;

				IRfcFunction testfn = repo.CreateFunction("RFC_READ_TABLE");

				testfn.SetValue("QUERY_TABLE", queryTable);
				testfn.SetValue("DELIMITER", delimiter);
				testfn.SetValue("NO_DATA", noData);
				testfn.SetValue("ROWSKIPS", rowSkips);
				testfn.SetValue("ROWCOUNT", rowCount);

				var separador = new string[] { "\r\n" };

				if (!String.IsNullOrEmpty(options))
				{
					var opcoes = testfn.GetTable("OPTIONS");
					var linhas = options.Split(separador, StringSplitOptions.RemoveEmptyEntries);

					foreach (var item in linhas)
					{
						opcoes.Append();
						opcoes.SetValue("TEXT", item);
					}
				}

				if (!String.IsNullOrEmpty(fields))
				{
					var fieldsTable = testfn.GetTable("FIELDS");
					var allFields = fields.Split(separador, StringSplitOptions.RemoveEmptyEntries);
					foreach (var item in allFields)
					{
						fieldsTable.Append();
						fieldsTable.SetValue("FIELDNAME", item);
					}
				}

				testfn.Invoke(conexao);

				var resultado = testfn.ToDataTable();

				RfcSessionManager.EndContext(conexao);

				return resultado;
			}
			catch (Exception ex)
			{
				throw new Exception($"Erro ao consultar: {ex.Message}", ex);
			}

		}

		public static RetornoPadrao ZFPP_GOODSMVT_CREATE(ZTPP_RASTREAB zTPP_RASTREAB)
		{

			try
			{
				var SapRfcDestination = ConnectSAP();

				RfcSessionManager.BeginContext(SapRfcDestination);

				var funcApont = SapRfcDestination.Repository.CreateFunction("ZFPP_GOODSMVT_CREATE_RFC");

				var opcoes = funcApont.GetTable("I_GOODSMVT");
				opcoes.Append();
				opcoes.SetValue("WERKS", zTPP_RASTREAB.WERKS);
				opcoes.Append();
				opcoes.SetValue("AUFNR", zTPP_RASTREAB.AUFNR);
				opcoes.Append();
				opcoes.SetValue("MATNR1", zTPP_RASTREAB.MATNR1);
				opcoes.Append();
				opcoes.SetValue("EAN13", zTPP_RASTREAB.EAN13);
				opcoes.Append();
				opcoes.SetValue("SERIAL1", zTPP_RASTREAB.SERIAL1);
				opcoes.Append();
				opcoes.SetValue("CREDATE1", zTPP_RASTREAB.CREDATE1.TrataDataSAP());
				opcoes.Append();
				opcoes.SetValue("MATNR2", zTPP_RASTREAB.MATNR2);
				opcoes.Append();
				opcoes.SetValue("SERIAL2", zTPP_RASTREAB.SERIAL2);
				opcoes.Append();
				opcoes.SetValue("CREDATE2", zTPP_RASTREAB.CREDATE2.TrataDataSAP());
				opcoes.Append();
				opcoes.SetValue("CRETIME2", zTPP_RASTREAB.CRETIME2);
				opcoes.Append();
				opcoes.SetValue("CRETIME1", zTPP_RASTREAB.CRETIME1);
				opcoes.Append();
				opcoes.SetValue("ARBPL", zTPP_RASTREAB.ARBPL);
				opcoes.Append();
				opcoes.SetValue("CODLINHA", zTPP_RASTREAB.CODLINHA);
				opcoes.Append();
				opcoes.SetValue("CODPOSTO", zTPP_RASTREAB.CODPOSTO);
				opcoes.Append();
				opcoes.SetValue("DRT", zTPP_RASTREAB.DRT);
				opcoes.Append();
				opcoes.SetValue("TPAMAR", zTPP_RASTREAB.TPAMAR);
				opcoes.Append();
				opcoes.SetValue("GESME", zTPP_RASTREAB.GESME);
				opcoes.Append();
				opcoes.SetValue("STATCONF", zTPP_RASTREAB.STATCONF);
				opcoes.Append();
				opcoes.SetValue("DTCONF", zTPP_RASTREAB.DTCONF.TrataDataSAP());
				opcoes.Append();
				opcoes.SetValue("MBLNR", zTPP_RASTREAB.MBLNR);
				opcoes.Append();
				opcoes.SetValue("STATVD", zTPP_RASTREAB.STATVD);
				opcoes.Append();
				opcoes.SetValue("STBCKFLUSH", zTPP_RASTREAB.STBCKFLUSH);
				opcoes.Append();
				opcoes.SetValue("DTBCKFLUSH", zTPP_RASTREAB.DTBCKFLUSH.TrataDataSAP());
				opcoes.Append();
				opcoes.SetValue("USERNAME", zTPP_RASTREAB.USERNAME);
				opcoes.Append();
				opcoes.SetValue("SISAP", zTPP_RASTREAB.SISAP);

				funcApont.Invoke(SapRfcDestination);

				IRfcTable tblInvHdr = funcApont.GetTable("I_GOODSMVT");

				var dados = tblInvHdr.ToString();
				/*dados = dados.Replace("TABLE  [STRUCTURE TAB512 { FIELD WA=", "");
				dados = dados.Replace("[STRUCTURE TAB512 { FIELD WA=", "");
				dados = dados.Replace("}]", "\n");*/

				RfcSessionManager.EndContext(SapRfcDestination);

				return new RetornoPadrao(dados.TrimStart());

			}
			catch (RfcCommunicationException comex)
			{
				Console.WriteLine("Erro: " + comex.Message);
				return new RetornoPadrao("Erro: " + comex.Message, false);
			}
			catch (RfcLogonException ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return new RetornoPadrao("Erro: " + ex.Message, false);
			}
			catch (RfcAbapRuntimeException ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return new RetornoPadrao("Erro: " + ex.Message, false);
			}
			catch (RfcAbapBaseException ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return new RetornoPadrao("Erro: " + ex.Message, false);
			}
			catch (Exception ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return new RetornoPadrao("Erro: " + ex.Message, false);
			}


		}

		public static RetornoPadrao ZFPP_APONTAMENTO_PCI_SISAP(ZSPP_APONTAMENTO_SISAP zSPP_APONTAMENTO_SISAP)
		{

			try
			{
				var SapRfcDestination = ConnectSAP();

				RfcSessionManager.BeginContext(SapRfcDestination);
				var repo = SapRfcDestination.Repository;

				var funcApont = repo.CreateFunction("ZFPP_APONTAMENTO_PCI_SISAP");

				IRfcTable tabela = funcApont.GetTable("SET_APONTAMENTO");
				tabela.Insert();

				IRfcTable tabRow = tabela.GetTable("INTTROW");

				foreach (var item in zSPP_APONTAMENTO_SISAP.INTTROW)
				{
					RfcStructureMetadata am = repo.GetStructureMetadata("ZSPP_APONTAMENTO_PCI");
					IRfcStructure estItens = am.CreateStructure();

					estItens.SetValue("EXTROW", item.EXTROW);
					estItens.SetValue("AUFNR", item.AUFNR);
					estItens.SetValue("YIELD", item.YIELD);
					estItens.SetValue("MEINS", item.MEINS.Trim());

					tabRow.Append(estItens);
				}

				tabela.SetValue("PACKNO", zSPP_APONTAMENTO_SISAP.PACKNO);
				tabela.SetValue("INTTROW", tabRow);

				funcApont.Invoke(SapRfcDestination);

				RfcSessionManager.EndContext(SapRfcDestination);
				SapRfcDestination = null;

				return new RetornoPadrao("Sucesso!");

			}
			catch (RfcCommunicationException comex)
			{
				Console.WriteLine("Erro: " + comex.Message);
				return new RetornoPadrao("Erro: " + comex.Message, false);
			}
			catch (RfcLogonException ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return new RetornoPadrao("Erro: " + ex.Message, false);
			}
			catch (RfcAbapRuntimeException ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return new RetornoPadrao("Erro: " + ex.Message, false);
			}
			catch (RfcAbapBaseException ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return new RetornoPadrao("Erro: " + ex.Message, false);
			}
			catch (Exception ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return new RetornoPadrao("Erro: " + ex.Message, false);
			}
		}

		public static List<ZTPP_APONPCI> ObterApontamentoPacote(string PACKNO)
		{

			var dados = ObterTabela("ZTPP_APONPCI", "", "PACKNO EQ '" + PACKNO + "'");
			if (dados != null && dados.Rows.Count > 0)
			{
				var lista = dados.AsEnumerable();
				var resultado = lista
									.Select(x => new ZTPP_APONPCI()
									{
										ID = x.Field<string>("ID"),
										MBLNR = x.Field<string>("MBLNR"),
										GJAHR = x.Field<double>("GJAHR"),
										ZEILE = x.Field<int>("ZEILE"),
										WERKS = x.Field<string>("WERKS"),
										AUFART = x.Field<string>("AUFART"),
										AUFNR = x.Field<string>("AUFNR"),
										MATNR = x.Field<string>("MATNR"),
										ARBPL = x.Field<string>("ARBPL"),
										YIELD = x.Field<double>("YIELD"),
										MEINS = x.Field<string>("MEINS"),
										NUCONF = x.Field<double>("NUCONF"),
										DTCONF = TrataDataSAP(x.Field<string>("DTCONF")),
										HRCONF = TrataHoraSAP(x.Field<string>("HRCONF")),
										PACKNO = x.Field<string>("PACKNO"),
										DTRECEB = TrataDataSAP(x.Field<string>("DTRECEB")),
										HRRECEB = TrataHoraSAP(x.Field<string>("HRRECEB"))
									})
									.ToList();

				return resultado;
			}
			return null;

		}

		public static List<ZTPP_APONPCI> Obter_ZTPP_APONPCI(DateTime datInicio, DateTime datFim, string numOP, string codModelo)
		{
			var query = $"DTRECEB GE '{datInicio.ToString("yyyyMMdd")}' AND DTRECEB LE '{datFim.ToString("yyyyMMdd")}'";
			if (!String.IsNullOrEmpty(numOP))
			{
				query += $" AND AUFNR EQ '{numOP}'";
			}
			if (!String.IsNullOrEmpty(codModelo))
			{
				query += $" AND MATNR EQ '{codModelo}'";
			}

			var dados = ObterTabela("ZTPP_APONPCI", "", query);
			if (dados != null && dados.Rows.Count > 0)
			{
				var lista = dados.AsEnumerable();
				var resultado = lista
									.Select(x => new ZTPP_APONPCI()
									{
										ID = x.Field<string>("ID"),
										MBLNR = x.Field<string>("MBLNR"),
										GJAHR = x.Field<double>("GJAHR"),
										ZEILE = x.Field<int>("ZEILE"),
										WERKS = x.Field<string>("WERKS"),
										AUFART = x.Field<string>("AUFART"),
										AUFNR = x.Field<string>("AUFNR"),
										MATNR = x.Field<string>("MATNR"),
										ARBPL = x.Field<string>("ARBPL"),
										YIELD = x.Field<double>("YIELD"),
										MEINS = x.Field<string>("MEINS"),
										NUCONF = x.Field<double>("NUCONF"),
										DTCONF = TrataDataSAP(x.Field<string>("DTCONF")),
										HRCONF = TrataHoraSAP(x.Field<string>("HRCONF")),
										PACKNO = x.Field<string>("PACKNO"),
										DTRECEB = TrataDataSAP(x.Field<string>("DTRECEB")),
										HRRECEB = TrataHoraSAP(x.Field<string>("HRRECEB"))
									})
									.ToList();

				return resultado;
			}
			return null;

		}

		public static List<ZTPP_OPMAO> Obter_ZTPP_OPMAO()
		{

			try
			{
				var SapRfcDestination = ConnectSAP();

				RfcSessionManager.BeginContext(SapRfcDestination);

				var funcInvHdrs = SapRfcDestination.Repository.CreateFunction("RFC_READ_TABLE");

				funcInvHdrs.SetValue("query_table", "ZTPP_OPMAO");
				funcInvHdrs.SetValue("delimiter", "|");

				funcInvHdrs.Invoke(SapRfcDestination);

				var returnTable = funcInvHdrs.GetTable("DATA");

				var tabela = new List<ZTPP_OPMAO>();

				foreach (var item in returnTable)
				{
					var estrutura = item.GetValue("WA").ToString().Split('|');

					var dados = new ZTPP_OPMAO();

					dados.MANDT = estrutura[0].ToString();
					dados.ID = int.Parse(estrutura[1]);
					dados.WERKS = estrutura[2];
					dados.AUFNR = estrutura[3];
					dados.TYPE = estrutura[4];
					dados.MATNR = estrutura[5];
					dados.ARBPL = estrutura[6];
					dados.OQTY = Decimal.Parse(estrutura[7].Replace('.', ','));
					dados.WEMNG = Decimal.Parse(estrutura[8].Replace('.', ','));
					dados.DTCV = TrataDataSAP(estrutura[9]);
					dados.DTIN = TrataDataSAP(estrutura[10]);
					dados.DTFM = TrataDataSAP(estrutura[11]);
					dados.DTREF = TrataDataSAP(estrutura[12]);
					dados.DTMOD = TrataDataSAP(estrutura[13]);
					dados.SYSTEM_STATUS = estrutura[14];
					dados.STAT1 = estrutura[15];
					dados.DTPI = TrataDataSAP(estrutura[16]);
					dados.STAT2 = estrutura[17];
					dados.DTSISAP = TrataDataSAP(estrutura[18]);
					dados.PACKNO = estrutura[19];


					tabela.Add(dados);

				}

				RfcSessionManager.EndContext(SapRfcDestination);

				return tabela;
			}
			catch (RfcCommunicationException comex)
			{
				Console.WriteLine("Erro: " + comex.Message);
				return null;
			}
			catch (RfcLogonException ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return null;
			}
			catch (RfcAbapRuntimeException ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return null;
			}
			catch (RfcAbapBaseException ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return null;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return null;
			}
		}

		public static List<ZTPP_OPMAO> Obter_ZTPP_OPMAO(string datInicio, string datFim)
		{

			try
			{
				var dados = ObterTabela("ZTPP_OPMAO", "", "DTREF >= '" + datInicio + "' AND DTREF <= '" + datFim + "'");

				var resultado = new List<ZTPP_OPMAO>();

				if (dados != null && dados.Rows.Count > 0)
				{
					var lista = dados.AsEnumerable().ToList();
					resultado = new List<ZTPP_OPMAO>();
					foreach (var item in lista)
					{
						var reg = new ZTPP_OPMAO();
						reg.MANDT = item.Field<string>(0);
						reg.ID = item.Field<int>(1);
						reg.WERKS = item.Field<string>(2);
						reg.AUFNR = item.Field<string>(3);
						reg.TYPE = item.Field<string>(4);
						reg.MATNR = item.Field<string>(5);
						reg.ARBPL = item.Field<string>(6);
						reg.OQTY = (Decimal)item.Field<double>(7);//.Replace('.', ',')),
						reg.WEMNG = (Decimal)item.Field<double>(8);//.Replace('.', ',')),
						reg.DTCV = TrataDataSAP(item.Field<string>(9));
						reg.DTIN = TrataDataSAP(item.Field<string>(10));
						reg.DTFM = TrataDataSAP(item.Field<string>(11));
						reg.DTREF = TrataDataSAP(item.Field<string>(12));
						reg.DTMOD = TrataDataSAP(item.Field<string>(13));
						reg.SYSTEM_STATUS = item.Field<string>(14);
						reg.STAT1 = item.Field<string>(15);
						reg.DTPI = TrataDataSAP(item.Field<string>(16));
						reg.STAT2 = item.Field<string>(17);
						reg.DTSISAP = TrataDataSAP(item.Field<string>(18));
						reg.PACKNO = item.Field<string>(19);

						resultado.Add(reg);

					}

				}

				return resultado;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return null;
			}
		}

		public static ZTPP_OPMAO Obter_ZTPP_OPMAO(string numOP)
		{

			try
			{
				var conexao = ConnectSAP();

				RfcSessionManager.BeginContext(conexao);
				RfcRepository repo = conexao.Repository;
				IRfcFunction testfn = repo.CreateFunction("RFC_READ_TABLE");

				testfn.SetValue("query_table", "ZTPP_OPMAO");
				testfn.SetValue("delimiter", "|");

				var opcoes = testfn.GetTable("OPTIONS");
				opcoes.Append();
				opcoes.SetValue("TEXT", $"AUFNR = '{numOP}'");

				testfn.Invoke(conexao);

				var tabela = testfn.GetTable("DATA");
				var resultado = new ZTPP_OPMAO();

				var item = tabela.GetValue("WA").ToString().Split('|');

				resultado.MANDT = item[0];
				resultado.ID = int.Parse(item[1]);
				resultado.WERKS = item.GetValue(2).ToString();
				resultado.AUFNR = item.GetValue(3).ToString();
				resultado.TYPE = item.GetValue(4).ToString();
				resultado.MATNR = item.GetValue(5).ToString();
				resultado.ARBPL = item.GetValue(6).ToString();
				resultado.OQTY = Decimal.Parse(item.GetValue(7).ToString().Replace('.', ','));
				resultado.WEMNG = Decimal.Parse(item.GetValue(8).ToString().Replace('.', ','));
				resultado.DTCV = TrataDataSAP(item.GetValue(9).ToString());
				resultado.DTIN = TrataDataSAP(item.GetValue(10).ToString());
				resultado.DTFM = TrataDataSAP(item.GetValue(11).ToString());
				resultado.DTREF = TrataDataSAP(item.GetValue(12).ToString());
				resultado.DTMOD = TrataDataSAP(item.GetValue(13).ToString());
				resultado.SYSTEM_STATUS = item.GetValue(14).ToString();
				resultado.STAT1 = item.GetValue(15).ToString();
				resultado.DTPI = TrataDataSAP(item.GetValue(16).ToString());
				resultado.STAT2 = item.GetValue(17).ToString();
				resultado.DTSISAP = TrataDataSAP(item.GetValue(18).ToString());
				resultado.PACKNO = item.GetValue(19).ToString();

				RfcSessionManager.EndContext(conexao);

				return resultado;



			}
			catch (Exception ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return null;
			}
		}

		public static List<RESB> ObterRESB(string numOP)
		{
			var resultado = new List<RESB>();

			var colunas = "AUFNR|WERKS|RSPOS|MATNR|BAUGR|BDMNG|MEINS|POSTP|SORTF|DUMPS|MATKL";
			var dados = ObterTabela("RESB", colunas, $"AUFNR EQ '{numOP}'");

			if (dados != null)
			{
				var lista = dados.AsEnumerable().ToList();

				foreach (var item in lista)
				{
					var reg = new RESB();
					reg.aufnr = item.Field<string>("aufnr");
					reg.werks = item.Field<string>("werks");
					reg.rspos = item.Field<double>("rspos");
					reg.matnr = item.Field<string>("matnr");
					reg.baugr = item.Field<string>("baugr");
					reg.bdmng = (decimal)(item.Field<double>("bdmng"));
					reg.meins = item.Field<string>("meins");
					reg.postp = item.Field<string>("postp");
					reg.sortf = item.Field<string>("sortf");
					reg.dumps = item.Field<string>("dumps");
					reg.matkl = item.Field<string>("matkl");

					resultado.Add(reg);
				}

			}

			return resultado;

		}

		public static RetornoPadrao BAPI_PRODORD_GET_DETAIL(ref BAPI_PRODORD_GET_DETAIL ordem)
		{

			try
			{
				var SapRfcDestination = ConnectSAP();

				RfcSessionManager.BeginContext(SapRfcDestination);
				var repo = SapRfcDestination.Repository;

				var funcApont = repo.CreateFunction("BAPI_PRODORD_GET_DETAIL");

				funcApont.SetValue("NUMBER", ordem.NUMBER);

				IRfcStructure tabela = funcApont.GetStructure("ORDER_OBJECTS");

				tabela.SetValue("HEADER", ordem.ORDER_OBJECTS.HEADER);
				tabela.SetValue("COMPONENTS", ordem.ORDER_OBJECTS.COMPONENTS);
				tabela.SetValue("OPERATIONS", ordem.ORDER_OBJECTS.OPERATIONS);
				tabela.SetValue("POSITIONS", ordem.ORDER_OBJECTS.POSITIONS);
				tabela.SetValue("PROD_REL_TOOLS", ordem.ORDER_OBJECTS.PROD_REL_TOOLS);
				tabela.SetValue("SEQUENCES", ordem.ORDER_OBJECTS.SEQUENCES);
				tabela.SetValue("SUBOPERATIONS", ordem.ORDER_OBJECTS.SUBOPERATIONS);
				tabela.SetValue("TRIGGER_POINTS", ordem.ORDER_OBJECTS.TRIGGER_POINTS);

				funcApont.Invoke(SapRfcDestination);

				var componentes = funcApont.GetTable("COMPONENT");
				var header = funcApont.GetTable("HEADER");
				var operation = funcApont.GetTable("OPERATION");
				var position = funcApont.GetTable("POSITION");

				foreach (var item in componentes)
				{
					var component = new BAPI_ORDER_COMPONENT();

					component.BACKFLUSH = item.GetValue("BACKFLUSH").ToString();
					component.BASE_UOM = item.GetValue("BASE_UOM").ToString();
					component.BASE_UOM_ISO = item.GetValue("BASE_UOM_ISO").ToString();
					component.BATCH = item.GetValue("BATCH").ToString();
					component.COMMITED_QUANTITY = Decimal.Parse(item.GetValue("COMMITED_QUANTITY").ToString());
					component.DELETION_INDICATOR = item.GetValue("DELETION_INDICATOR").ToString();
					component.ENTRY_QUANTITY = Decimal.Parse(item.GetValue("ENTRY_QUANTITY").ToString());
					component.ENTRY_UOM = item.GetValue("ENTRY_UOM").ToString();
					component.ENTRY_UOM_ISO = item.GetValue("ENTRY_UOM_ISO").ToString();
					component.ITEM_CATEGORY = item.GetValue("ITEM_CATEGORY").ToString();
					component.ITEM_NUMBER = item.GetValue("ITEM_NUMBER").ToString();
					component.MATERIAL = item.GetValue("MATERIAL").ToString();
					component.MATERIAL_DESCRIPTION = item.GetValue("MATERIAL_DESCRIPTION").ToString();
					component.MATERIAL_EXTERNAL = item.GetValue("MATERIAL_EXTERNAL").ToString();
					component.MATERIAL_GUID = item.GetValue("MATERIAL_GUID").ToString();
					component.MATERIAL_LONG = item.GetValue("MATERIAL_LONG").ToString();
					component.MATERIAL_VERSION = item.GetValue("MATERIAL_VERSION").ToString();
					component.MOVEMENT_TYPE = item.GetValue("MOVEMENT_TYPE").ToString();
					component.OPERATION = item.GetValue("OPERATION").ToString();
					component.ORDER_NUMBER = item.GetValue("ORDER_NUMBER").ToString();
					component.PROD_PLANT = item.GetValue("PROD_PLANT").ToString();
					component.PURCHASE_REQ_ITEM = int.Parse(item.GetValue("PURCHASE_REQ_ITEM").ToString());
					component.PURCHASE_REQ_NO = item.GetValue("PURCHASE_REQ_NO").ToString();
					component.REQ_DATE = TrataDataSAP(item.GetValue("REQ_DATE").ToString());
					component.REQ_QUAN = Decimal.Parse(item.GetValue("REQ_QUAN").ToString());
					component.REQ_SEGMENT = item.GetValue("REQ_SEGMENT").ToString();
					component.RESERVATION_ITEM = int.Parse(item.GetValue("RESERVATION_ITEM").ToString());
					component.RESERVATION_NUMBER = int.Parse(item.GetValue("RESERVATION_NUMBER").ToString());
					component.RESERVATION_TYPE = item.GetValue("RESERVATION_TYPE").ToString();
					component.SEQUENCE = item.GetValue("SEQUENCE").ToString();
					component.SHORTAGE = Decimal.Parse(item.GetValue("SHORTAGE").ToString());
					component.SPECIAL_STOCK = item.GetValue("SPECIAL_STOCK").ToString();
					component.STOCK_SEGMENT = item.GetValue("STOCK_SEGMENT").ToString();
					component.STORAGE_LOCATION = item.GetValue("STORAGE_LOCATION").ToString();
					component.SUPPLY_AREA = item.GetValue("SUPPLY_AREA").ToString();
					component.SYSTEM_STATUS = item.GetValue("SYSTEM_STATUS").ToString();
					component.VALUATION_SPEC_STOCK = item.GetValue("VALUATION_SPEC_STOCK").ToString();
					component.WITHDRAWN_QUANTITY = Decimal.Parse(item.GetValue("WITHDRAWN_QUANTITY").ToString());

					ordem.COMPONENT.Add(component);
				}

				foreach (var item in header)
				{
					var head = new BAPI_ORDER_HEADER1()
					{
						ACTUAL_FINISH_DATE = TrataDataSAP(item.GetValue("ACTUAL_FINISH_DATE").ToString()),
						ACTUAL_RELEASE_DATE = TrataDataSAP(item.GetValue("ACTUAL_RELEASE_DATE").ToString()),
						ACTUAL_START_DATE = TrataDataSAP(item.GetValue("ACTUAL_START_DATE").ToString()),
						ACTUAL_START_TIME = item.GetValue("ACTUAL_START_TIME").ToString(),
						BATCH = item.GetValue("BATCH").ToString(),
						COLLECTIVE_ORDER = item.GetValue("COLLECTIVE_ORDER").ToString(),
						CONFIRMED_QUANTITY = Decimal.Parse(item.GetValue("CONFIRMED_QUANTITY").ToString()),
						CONF_CNT = int.Parse(item.GetValue("CONF_CNT").ToString()),
						CONF_NO = int.Parse(item.GetValue("CONF_NO").ToString()),
						DATE_OF_EXPIRY = TrataDataSAP(item.GetValue("DATE_OF_EXPIRY").ToString()),
						DATE_OF_MANUFACTURE = TrataDataSAP(item.GetValue("DATE_OF_MANUFACTURE").ToString()),
						DELETION_FLAG = item.GetValue("DELETION_FLAG").ToString(),
						ENTERED_BY = item.GetValue("ENTERED_BY").ToString(),
						ENTER_DATE = TrataDataSAP(item.GetValue("ENTER_DATE").ToString()),
						EXPL_DATE = TrataDataSAP(item.GetValue("EXPL_DATE").ToString()),
						FINISH_DATE = TrataDataSAP(item.GetValue("FINISH_DATE").ToString()),
						FINISH_TIME = item.GetValue("FINISH_TIME").ToString(),
						INT_OBJ_NO = int.Parse(item.GetValue("INT_OBJ_NO").ToString()),
						LEADING_ORDER = item.GetValue("LEADING_ORDER").ToString(),
						MATERIAL = item.GetValue("MATERIAL").ToString(),
						MATERIAL_EXTERNAL = item.GetValue("MATERIAL_EXTERNAL").ToString(),
						MATERIAL_GUID = item.GetValue("MATERIAL_GUID").ToString(),
						MATERIAL_LONG = item.GetValue("MATERIAL_LONG").ToString(),
						MATERIAL_TEXT = item.GetValue("MATERIAL_TEXT").ToString(),
						MATERIAL_VERSION = item.GetValue("MATERIAL_VERSION").ToString(),
						MRP_CONTROLLER = item.GetValue("MRP_CONTROLLER").ToString(),
						ORDER_NUMBER = item.GetValue("ORDER_NUMBER").ToString(),
						ORDER_SEQ_NO = int.Parse(item.GetValue("ORDER_SEQ_NO").ToString()),
						ORDER_TYPE = item.GetValue("ORDER_TYPE").ToString(),
						PLAN_PLANT = item.GetValue("PLAN_PLANT").ToString(),
						PRIORITY = item.GetValue("PRIORITY").ToString(),
						PRODUCTION_FINISH_DATE = TrataDataSAP(item.GetValue("PRODUCTION_FINISH_DATE").ToString()),
						PRODUCTION_PLANT = item.GetValue("PRODUCTION_PLANT").ToString(),
						PRODUCTION_SCHEDULER = item.GetValue("PRODUCTION_SCHEDULER").ToString(),
						PRODUCTION_START_DATE = TrataDataSAP(item.GetValue("PRODUCTION_START_DATE").ToString()),
						PROD_SCHED_PROFILE = item.GetValue("PROD_SCHED_PROFILE").ToString(),
						RESERVATION_NUMBER = int.Parse(item.GetValue("RESERVATION_NUMBER").ToString()),
						ROUTING_NO = int.Parse(item.GetValue("ROUTING_NO").ToString()),
						SALES_ORDER = item.GetValue("SALES_ORDER").ToString(),
						SALES_ORDER_ITEM = int.Parse(item.GetValue("SALES_ORDER_ITEM").ToString()),
						SCHED_FIN_TIME = item.GetValue("SCHED_FIN_TIME").ToString(),
						SCHED_RELEASE_DATE = TrataDataSAP(item.GetValue("SCHED_RELEASE_DATE").ToString()),
						SCHED_START_TIME = item.GetValue("SCHED_START_TIME").ToString(),
						SCRAP = Decimal.Parse(item.GetValue("SCRAP").ToString()),
						START_DATE = TrataDataSAP(item.GetValue("START_DATE").ToString()),
						START_TIME = item.GetValue("START_TIME").ToString(),
						SYSTEM_STATUS = item.GetValue("SYSTEM_STATUS").ToString(),
						TARGET_QUANTITY = Decimal.Parse(item.GetValue("TARGET_QUANTITY").ToString()),
						UNIT = item.GetValue("UNIT").ToString(),
						UNIT_ISO = item.GetValue("UNIT_ISO").ToString(),
						WBS_ELEMENT = int.Parse(item.GetValue("WBS_ELEMENT").ToString())
					};
					ordem.HEADER.Add(head);

				}

				foreach (var item in operation)
				{
					var opera = new BAPI_ORDER_OPERATION1()
					{
						ROUTING_NO = int.Parse(item.GetValue("ROUTING_NO").ToString()),
						COUNTER = int.Parse(item.GetValue("COUNTER").ToString()),
						SEQUENCE_NO = item.GetValue("SEQUENCE_NO").ToString(),
						CONF_NO = int.Parse(item.GetValue("CONF_NO").ToString()),
						CONF_CNT = int.Parse(item.GetValue("CONF_CNT").ToString()),
						PURCHASE_REQ_NO = item.GetValue("PURCHASE_REQ_NO").ToString(),
						PURCHASE_REQ_ITEM = int.Parse(item.GetValue("PURCHASE_REQ_ITEM").ToString()),
						GROUP_COUNTER = item.GetValue("GROUP_COUNTER").ToString(),
						TASK_LIST_TYPE = item.GetValue("TASK_LIST_TYPE").ToString(),
						TASK_LIST_GROUP = item.GetValue("TASK_LIST_GROUP").ToString(),
						OPERATION_NUMBER = item.GetValue("OPERATION_NUMBER").ToString(),
						OPR_CNTRL_KEY = item.GetValue("OPR_CNTRL_KEY").ToString(),
						PROD_PLANT = item.GetValue("PROD_PLANT").ToString(),
						DESCRIPTION = item.GetValue("DESCRIPTION").ToString(),
						DESCRIPTION2 = item.GetValue("DESCRIPTION2").ToString(),
						STANDARD_VALUE_KEY = item.GetValue("STANDARD_VALUE_KEY").ToString(),
						ACTIVITY_TYPE_1 = item.GetValue("ACTIVITY_TYPE_1").ToString(),
						ACTIVITY_TYPE_2 = item.GetValue("ACTIVITY_TYPE_2").ToString(),
						ACTIVITY_TYPE_3 = item.GetValue("ACTIVITY_TYPE_3").ToString(),
						ACTIVITY_TYPE_4 = item.GetValue("ACTIVITY_TYPE_4").ToString(),
						ACTIVITY_TYPE_5 = item.GetValue("ACTIVITY_TYPE_5").ToString(),
						ACTIVITY_TYPE_6 = item.GetValue("ACTIVITY_TYPE_6").ToString(),
						UNIT = item.GetValue("UNIT").ToString(),
						UNIT_ISO = item.GetValue("UNIT_ISO").ToString(),
						QUANTITY = Decimal.Parse(item.GetValue("QUANTITY").ToString()),
						SCRAP = Decimal.Parse(item.GetValue("SCRAP").ToString()),
						EARL_SCHED_START_DATE_EXEC = TrataDataSAP(item.GetValue("EARL_SCHED_START_DATE_EXEC").ToString()),
						EARL_SCHED_START_TIME_EXEC = item.GetValue("EARL_SCHED_START_TIME_EXEC").ToString(),
						EARL_SCHED_START_DATE_PROC = TrataDataSAP(item.GetValue("EARL_SCHED_START_DATE_PROC").ToString()),
						EARL_SCHED_START_TIME_PROC = item.GetValue("EARL_SCHED_START_TIME_PROC").ToString(),
						EARL_SCHED_START_DATE_TEARD = TrataDataSAP(item.GetValue("EARL_SCHED_START_DATE_TEARD").ToString()),
						EARL_SCHED_START_TIME_TEARD = item.GetValue("EARL_SCHED_START_TIME_TEARD").ToString(),
						EARL_SCHED_FIN_DATE_EXEC = TrataDataSAP(item.GetValue("EARL_SCHED_FIN_DATE_EXEC").ToString()),
						EARL_SCHED_FIN_TIME_EXEC = item.GetValue("EARL_SCHED_FIN_TIME_EXEC").ToString(),
						LATE_SCHED_START_DATE_EXEC = TrataDataSAP(item.GetValue("LATE_SCHED_START_DATE_EXEC").ToString()),
						LATE_SCHED_START_TIME_EXEC = item.GetValue("LATE_SCHED_START_TIME_EXEC").ToString(),
						LATE_SCHED_START_DATE_PROC = TrataDataSAP(item.GetValue("LATE_SCHED_START_DATE_PROC").ToString()),
						LATE_SCHED_START_TIME_PROC = item.GetValue("LATE_SCHED_START_TIME_PROC").ToString(),
						LATE_SCHED_START_DATE_TEARD = TrataDataSAP(item.GetValue("LATE_SCHED_START_DATE_TEARD").ToString()),
						LATE_SCHED_START_TIME_TEARD = item.GetValue("LATE_SCHED_START_TIME_TEARD").ToString(),
						LATE_SCHED_FIN_DATE_EXEC = TrataDataSAP(item.GetValue("LATE_SCHED_FIN_DATE_EXEC").ToString()),
						LATE_SCHED_FIN_TIME_EXEC = item.GetValue("LATE_SCHED_FIN_TIME_EXEC").ToString(),
						WORK_CENTER = item.GetValue("WORK_CENTER").ToString(),
						WORK_CENTER_TEXT = item.GetValue("WORK_CENTER_TEXT").ToString(),
						SYSTEM_STATUS = item.GetValue("SYSTEM_STATUS").ToString(),
						SUBOPERATION = item.GetValue("SUBOPERATION").ToString()
					};

					ordem.OPERATION.Add(opera);
				}

				foreach (var item in position)
				{
					var opera = new BAPI_ORDER_ITEM();

					opera.ORDER_NUMBER = item.GetValue("ORDER_NUMBER").ToString();
					opera.ORDER_ITEM_NUMBER = item.GetValue("ORDER_ITEM_NUMBER").ToString();
					opera.SALES_ORDER = item.GetValue("SALES_ORDER").ToString();
					opera.SALES_ORDER_ITEM = item.GetValue("SALES_ORDER_ITEM").ToString();
					opera.SCRAP = Decimal.Parse(item.GetValue("SCRAP").ToString());
					opera.QUANTITY = Decimal.Parse(item.GetValue("QUANTITY").ToString());
					var qtd = item.GetValue("DELIVERED_QUANTITY").ToString();
					if (!String.IsNullOrEmpty(qtd))
					{
						opera.DELIVERED_QUANTITY = Decimal.Parse(qtd);
					}
					opera.BASE_UNIT = item.GetValue("BASE_UNIT").ToString();
					opera.BASE_UNIT_ISO = item.GetValue("BASE_UNIT_ISO").ToString();
					opera.MATERIAL = item.GetValue("MATERIAL").ToString();
					opera.ACTUAL_DELIVERY_DATE = TrataDataSAP(item.GetValue("ACTUAL_DELIVERY_DATE").ToString());
					opera.PLANNED_DELIVERY_DATE = TrataDataSAP(item.GetValue("PLANNED_DELIVERY_DATE").ToString());
					opera.PLAN_PLANT = item.GetValue("PLAN_PLANT").ToString();
					opera.STORAGE_LOCATION = item.GetValue("STORAGE_LOCATION").ToString();
					opera.DELIVERY_COMPL = item.GetValue("DELIVERY_COMPL").ToString();
					opera.PRODUCTION_VERSION = item.GetValue("PRODUCTION_VERSION").ToString();
					opera.PROD_PLANT = item.GetValue("PROD_PLANT").ToString();
					opera.ORDER_TYPE = item.GetValue("ORDER_TYPE").ToString();
					opera.FINISH_DATE = TrataDataSAP(item.GetValue("FINISH_DATE").ToString());
					opera.PRODUCTION_FINISH_DATE = TrataDataSAP(item.GetValue("PRODUCTION_FINISH_DATE").ToString());
					opera.BATCH = item.GetValue("BATCH").ToString();
					opera.DELETION_FLAG = item.GetValue("DELETION_FLAG").ToString();
					opera.MRP_AREA = item.GetValue("MRP_AREA").ToString();
					opera.MATERIAL_TEXT = item.GetValue("MATERIAL_TEXT").ToString();
					opera.MATERIAL_EXTERNAL = item.GetValue("MATERIAL_EXTERNAL").ToString();
					opera.MATERIAL_GUID = item.GetValue("MATERIAL_GUID").ToString();
					opera.MATERIAL_VERSION = item.GetValue("MATERIAL_VERSION").ToString();
					opera.STOCK_SEGMENT = item.GetValue("STOCK_SEGMENT").ToString();
					opera.MATERIAL_LONG = item.GetValue("MATERIAL_LONG").ToString();

					ordem.POSITION.Add(opera);
				}

				RfcSessionManager.EndContext(SapRfcDestination);

				SapRfcDestination = null;
				return new RetornoPadrao("Sucesso!");

			}
			catch (RfcCommunicationException comex)
			{
				Console.WriteLine("Erro: " + comex.Message);
				return new RetornoPadrao("Erro: " + comex.Message, false);
			}
			catch (RfcLogonException ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return new RetornoPadrao("Erro: " + ex.Message, false);
			}
			catch (RfcAbapRuntimeException ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return new RetornoPadrao("Erro: " + ex.Message, false);
			}
			catch (RfcAbapBaseException ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return new RetornoPadrao("Erro: " + ex.Message, false);
			}
			catch (Exception ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return new RetornoPadrao("Erro: " + ex.Message, false);
			}

		}

		public static RetornoPadrao ObterLTAP(ref List<LTAP> tabela, string numOrdem, string numUD)
		{

			var colunas = "NLPLA|MATNR|VLENR|QDATU|QZEIT|WERKS|WDATU|VISTM|VLTYP|QNAME|MAKTX";
			var query = $"LGNUM EQ '101'";

			if (!String.IsNullOrEmpty(numOrdem))
			{
				query += $" AND NLPLA EQ '{numOrdem}'";
			}

			if (!String.IsNullOrEmpty(numUD))
			{
				query += $" AND VLENR EQ '{numUD.PadLeft(20, '0')}'";
			}

			try
			{
				var dados = ObterTabela("LTAP", colunas, query);

				if (dados != null)
				{
					tabela = new List<LTAP>();
					var lista = dados.AsEnumerable().ToList();

					foreach (var item in lista)
					{
						var reg = new LTAP();
						reg.NLPLA = item.Field<string>("NLPLA");
						reg.MATNR = item.Field<string>("MATNR").TrimStart('0');
						reg.VLENR = item.Field<string>("VLENR").TrimStart('0');
						reg.QDATU = TrataDataSAP(item.Field<string>("QDATU"));
						reg.QZEIT = TrataHoraSAP(item.Field<string>("QZEIT"));
						reg.WERKS = item.Field<string>("WERKS");
						reg.WDATU = TrataDataSAP(item.Field<string>("WDATU"));
						reg.VISTM = item.Field<double>("VISTM");
						reg.VLTYP = item.Field<string>("VLTYP");
						reg.QNAME = item.Field<string>("QNAME");
						reg.MAKTX = item.Field<string>("MAKTX");

						tabela.Add(reg);
					}

				}

				return new RetornoPadrao("", true);
			}
			catch (Exception ex)
			{
				return new RetornoPadrao($"Erro ao consultar: {ex.Message}", false);
			}
		}

		public static List<EstoquePCIDTO> ObterEstoquePCI() 
		{
			var retorno = new List<EstoquePCIDTO>();

			var camposMARD = "MATNR\r\nWERKS\r\nLGORT\r\nPSTAT\r\nLABST";
			var opcoesMARD = "(LGORT EQ 'IA01' OR LGORT EQ 'IA02' OR \r\n" + //"LABST NE 0 AND (LGORT EQ 'IA01' OR LGORT EQ 'IA02' OR \r\n" +
							 "LGORT EQ 'IM01' OR LGORT EQ 'IM02' OR LGORT EQ 'IM03' OR \r\n" +
							 "LGORT EQ 'MF01' OR LGORT EQ 'MF02')";

			var camposMARA = "MATNR";
			var opcoesMARA = "(MATKL EQ '10-416' OR MATKL EQ '80-090' OR MATKL EQ '10-414' OR\r\n " +
							 " MATKL EQ '10-410' OR MATKL EQ '10-411' OR MATKL EQ '10-415')\r\n" +
							 " AND MTART EQ 'ZSEM'\r\n";

			var pcisMARA = ObterTabela("MARA", "|", "", 0, 0, opcoesMARA, camposMARA);

			var modelos = "";
			for (var i = 0; i < pcisMARA.Rows.Count; i++)
			{
				if (i < 2){
					modelos = $"AND (MATNR EQ '{pcisMARA.Rows[i]["MATNR"].ToString()}'";
					i++;
					if (i < pcisMARA.Rows.Count)
					{
						modelos += $" OR MATNR EQ '{pcisMARA.Rows[i]["MATNR"].ToString()}'\r\n";
					}
				}
				else
				{
					modelos += $"OR MATNR EQ '{pcisMARA.Rows[i]["MATNR"].ToString()}' ";
					i++;
					if (i < pcisMARA.Rows.Count)
					{
						modelos += $"OR MATNR EQ '{pcisMARA.Rows[i]["MATNR"].ToString()}'\r\n";
					}
				}
				
			}
			modelos += ")";

			var estoquePCI = ObterTabela("MARD", "|", "", 0, 0, opcoesMARD + "\r\n"+ modelos, camposMARD);

			var dados = estoquePCI.AsEnumerable().ToList();
			foreach (var item in dados)
			{
				var linha = new EstoquePCIDTO();
				linha.CodFab = item["WERKS"].ToString();
				linha.CodItem = item.Field<string>("MATNR");
				linha.QtdEstoque = decimal.Parse(item["LABST"].ToString());
				linha.CodLocal = item["LGORT"].ToString();
				linha.DatAtualizacao = DateTime.Now;

				retorno.Add(linha);
			}

			return retorno;
		
		}

		public static RetornoPadrao ZFMPP_APONTAMENTO_OP_MOB(ZSPP_HEADER_APONTAMENTO_MOB dados) 
		{
			try
			{

				var dest = ConnectSAP();
				RfcSessionManager.BeginContext(dest);

				var repo = dest.Repository;

				var funcApont = repo.CreateFunction("ZFMPP_APONTAMENTO_OP_MOB");

				IRfcTable tabela = funcApont.GetTable("SET_APONTAMENTO");
				tabela.Insert();

				tabela.SetValue("LOTE", dados.LOTE);
				tabela.SetValue("ORDEM", dados.ORDEM.PadLeft(12, '0'));
				tabela.SetValue("QUANTIDADE", dados.QUANTIDADE);
				tabela.SetValue("PESO", dados.PESO);
				tabela.SetValue("CODLINHA", dados.CODLINHA);
				tabela.SetValue("CODPOSTO", dados.CODPOSTO);

				IRfcTable tabRow = tabela.GetTable("ITEM");

				foreach (var item in dados.ITEM)
				{
					RfcStructureMetadata am = repo.GetStructureMetadata("ZSPP_ITEM_APONTAMENTO_MOB");
					IRfcStructure estItens = am.CreateStructure();

					estItens.SetValue("NECOD1", item.NECOD1);
					estItens.SetValue("NECOD2", item.NECOD2);
					estItens.SetValue("SERIAL", item.SERIAL);
					estItens.SetValue("IMEICOD1", item.IMEICOD1.PadLeft(18, '0'));
					estItens.SetValue("IMEICOD2", item.IMEICOD2.PadLeft(18, '0'));
					estItens.SetValue("DATA_REAL_PROD", item.DATA_REAL_PROD.TrataDataSAP());
					estItens.SetValue("HORA_REAL_PROD", item.HORA_REAL_PROD);
					estItens.SetValue("CPF", item.CPF.PadLeft(15, '0'));
					estItens.SetValue("PESO", item.PESO);

					tabRow.Append(estItens);
				}

				funcApont.Invoke(dest);

				IRfcTable tblResult = funcApont.GetTable("GET_APONTAMENTO");

				var dt = tblResult.ToDataTable("GET_APONTAMENTO");

				RfcSessionManager.EndContext(dest);

				dest = null;

				if (dt != null && dt.Rows.Count > 0){
					return new RetornoPadrao($"Erro ao processar! {dt.Rows[0]["DESLOG"].ToString()}", false);
				}

				return new RetornoPadrao($"Sucesso!");
				//return new RetornoPadrao($"Sucesso!{funcApont.ToString()}");
			}
			catch (Exception ex)
			{
				Console.WriteLine("Erro: " + ex.Message);
				return new RetornoPadrao("Erro: " + ex.Message, false);
			}
			

		}

		public static DateTime TrataDataSAP(string Data)
		{
			try
			{

				var retorno = "";

				if (!String.IsNullOrEmpty(Data) && Data != "00000000")
				{
					if (Data.Length == 8)
					{
						retorno = String.Format("{0}/{1}/{2}", Data.Substring(6, 2), Data.Substring(4, 2), Data.Substring(0, 4));
					}
					else
					{
						var bPassou = false;
						bPassou = DateTime.TryParse(retorno, out DateTime data);

						if (!bPassou)
						{
							System.Globalization.CultureInfo culture;
							System.Globalization.DateTimeStyles styles;
							culture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
							styles = System.Globalization.DateTimeStyles.None;
							DateTime.TryParse(Data, culture, styles, out data);

						}

						return data;

					}

					return DateTime.Parse(retorno);
				}
				else
				{
					return new DateTime();
				}
			}
			catch
			{

				return new DateTime();
			}

		}

		public static String TrataHoraSAP(string Hora)
		{
			try
			{
				return $"{Hora.Substring(0, 2)}:{Hora.Substring(2, 2)}:{Hora.Substring(4, 2)}";
			}
			catch
			{
				return "00:00";
			}


		}
	}

	public class RetornoPadrao
	{

		public bool Status { get; set; }
		public string Mensagem { get; set; }

		public RetornoPadrao()
		{
			Status = false;
			Mensagem = "";
		}

		/// <summary>
		/// Retorno padrão para sucesso
		/// </summary>
		/// <param name="mensagem">Mensagem de retorno para o usuário</param>
		public RetornoPadrao(string mensagem)
		{
			Status = true;
			Mensagem = mensagem;
		}

		/// <summary>
		/// Retorno padrão para consultas
		/// </summary>
		/// <param name="status">Verdadeiro ou Falso</param>
		/// <param name="mensagem">Mensagem de retorno</param>
		public RetornoPadrao(string mensagem, bool status)
		{
			Status = status;
			Mensagem = mensagem;
		}

	}

}