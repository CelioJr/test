namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolprmo
    {
        [Key]
        public long id_consolprmo { get; set; }

        public long id_consolpr { get; set; }

        public long id_consolmo { get; set; }

        public long? pcs_auto_producaobruta { get; set; }

        public long? pcs_auto_producaorefugada { get; set; }

        public long? pcs_manu_producaobruta { get; set; }

        public long? pcs_manu_producaorefugada { get; set; }

        public virtual dw_consolmo dw_consolmo { get; set; }

        public virtual dw_consolpr dw_consolpr { get; set; }
    }
}
