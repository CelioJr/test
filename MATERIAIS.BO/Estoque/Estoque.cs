﻿using SEMP.DAL.DAO.Materiais;
using SEMP.Model.DTO;
using SEMP.Model.VO.Materiais;
using System.Collections.Generic;

namespace MATERIAIS.BO.Estoque
{
	public class Estoque
	{

		public static List<EstoquePCIDTO> ObterEstoquePCI() 
		{
			return daoEstoque.ObterEstoquePCI();
		}


		public static bool GravarEstoquePCI(List<EstoquePCIDTO> dados)
		{
			return daoEstoque.GravarEstoquePCI(dados);

		}

		public static List<SaldoPlacasResumo> ObterResumoSaldoPlacas(string agrupa, string codModelo, string codPlaca) {
			return daoEstoque.ObterResumoSaldoPlacas(agrupa, codModelo, codPlaca);
		}
	}
}
