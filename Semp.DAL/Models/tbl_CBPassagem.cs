using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
	/// <summary>
	/// Classe responsável por espelhar a tabela tbl_CBPassagem do banco de dados.
	/// </summary>
	public partial class tbl_CBPassagem
	{
		public string NumECB { get; set; }
		public int CodLinha { get; set; }
		public int NumPosto { get; set; }
		public System.DateTime DatEvento { get; set; }
		public string CodDRT { get; set; }
		public string CodFab { get; set; }
		public string NumAP { get; set; }
	}
}
