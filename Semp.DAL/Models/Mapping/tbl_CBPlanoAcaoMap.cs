using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
	public class tbl_CBPlanoAcaoMap : EntityTypeConfiguration<tbl_CBPlanoAcao>
	{
		public tbl_CBPlanoAcaoMap()
		{
			// Primary Key
			this.HasKey(t => t.CodPlano);

			// Properties
			this.Property(t => t.CodDRT)
				.HasMaxLength(50);

			this.Property(t => t.DesPlano)
				.HasMaxLength(300);

			// Table & Column Mappings
			this.ToTable("tbl_CBPlanoAcao");
			this.Property(t => t.CodPlano).HasColumnName("CodPlano");
			this.Property(t => t.CodLinha).HasColumnName("CodLinha");
			this.Property(t => t.NumPosto).HasColumnName("NumPosto");
			this.Property(t => t.DatPlano).HasColumnName("DatPlano");
			this.Property(t => t.CodDRT).HasColumnName("CodDRT");
			this.Property(t => t.DesPlano).HasColumnName("DesPlano");
			this.Property(t => t.FlgStatus).HasColumnName("FlgStatus");
			this.Property(t => t.DatConclusao).HasColumnName("DatConclusao");
			this.Property(t => t.Hora).HasColumnName("Hora");

		}
	}
}
