﻿using SEMP.SAPConnector;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SEMP.Testes
{
	public partial class frmSAPRFCReadTable : Form
	{
		public frmSAPRFCReadTable()
		{
			InitializeComponent();
		}

		private void btnConsultar_Click(object sender, EventArgs e)
		{
			if(!String.IsNullOrEmpty(cmbExistentes.Text)){

				var dados = SAPFunctions.ObterEstoquePCI();

				grdDados.DataSource = dados;

				return;

			}

			if (String.IsNullOrEmpty(txtQueryTable.Text)) return;
			if (String.IsNullOrEmpty(txtRowCount.Text))
			{
				txtRowCount.Text = "500";
			}
			if (String.IsNullOrEmpty(txtRowSkips.Text))
			{
				txtRowSkips.Text = "0";
			}
			if (String.IsNullOrEmpty(txtDelimiter.Text))
			{
				txtDelimiter.Text = "|";
			}
			try
			{
				var dados = SAPFunctions.ObterTabela(txtQueryTable.Text, txtDelimiter.Text, txtNoData.Text, int.Parse(txtRowSkips.Text), int.Parse(txtRowCount.Text), txtOptions.Text, txtFields.Text);

				grdDados.DataSource = dados;
			}
			catch (Exception ex)
			{

				MessageBox.Show($"Erro ao consultar: {ex.Message}");
			}
			

		}

		private void frmSAPRFCReadTable_Load(object sender, EventArgs e)
		{
			var ping = SAPFunctions.TestaConn();

			chkPing.Checked = ping;
		}

		private void grdDados_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			txtQtdRegistros.Text = grdDados.Rows.Count.ToString();
		}
	}
}
