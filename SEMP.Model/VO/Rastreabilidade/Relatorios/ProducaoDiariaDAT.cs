﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
	public class ProducaoDiariaDAT
	{
		public int? Id { get; set; }
		public int? CodigoLinha { get; set; }
		public string DescricaoLinha { get; set; }
		public int? CodigoItem { get; set; }
		public string DescricaoItem { get; set; }
		public int? Programado { get; set; }
		public int? Embalado { get; set; }
		public int? InspecaoCQ { get; set; }
		public int? InspecaoOBA { get; set; }
		public int? Aprovado { get; set; }
		public int? Diferenca { get; set; }
		public int? Faturado { get; set; }
	}
}
