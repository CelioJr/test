using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_rpaitemMap : EntityTypeConfiguration<tbl_rpaitem>
    {
        public tbl_rpaitemMap()
        {
            // Primary Key
            this.HasKey(t => new { t.NumRPA, t.CodModelo, t.NumSerie });

            // Properties
            this.Property(t => t.NumRPA)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumSerie)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.NomUsuSaida)
                .HasMaxLength(50)
                .IsUnicode(false);

            this.Property(t => t.NumRPE)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NomUsuRec)
                .HasMaxLength(50)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_rpaitem");
            this.Property(t => t.NumRPA).HasColumnName("NumRPA");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.NumSerie).HasColumnName("NumSerie");
            this.Property(t => t.DatMov).HasColumnName("DatMov");
            this.Property(t => t.NomUsuSaida).HasColumnName("NomUsuSaida");
            this.Property(t => t.NumRPE).HasColumnName("NumRPE");
            this.Property(t => t.DatRet).HasColumnName("DatRet");
            this.Property(t => t.NomUsuRec).HasColumnName("NomUsuRec");
        }
    }
}
