using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_RelacaoIMC_IACMap : EntityTypeConfiguration<tbl_RelacaoIMC_IAC>
    {
        public tbl_RelacaoIMC_IACMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodModelo, t.CodItem });

            // Properties
            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6);

            this.Property(t => t.CodItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6);

            // Table & Column Mappings
            this.ToTable("tbl_RelacaoIMC_IAC");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
        }
    }
}
