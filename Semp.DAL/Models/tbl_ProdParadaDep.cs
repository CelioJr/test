using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_ProdParadaDep
    {
        public string CodFab { get; set; }
        public string NumParada { get; set; }
        public string CodDepto { get; set; }
        public string CodFabDepto { get; set; }
        public string TipItem { get; set; }
        public string CodGrupo { get; set; }
        public Nullable<System.DateTime> DatEnvio { get; set; }
        public System.Guid rowguid { get; set; }
    }
}
