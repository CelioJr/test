using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBHistoricoRetPostoMap : EntityTypeConfiguration<tbl_CBHistoricoRetPosto>
    {
        public tbl_CBHistoricoRetPostoMap()
        {
            // Primary Key
            this.HasKey(t => new {t.codLinha, t.numPosto, t.DatAtivacao });

            // Properties
            this.Property(t => t.numPosto).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("tbl_CBHistoricoRetPosto");
            this.Property(t => t.codLinha).HasColumnName("codLinha");
            this.Property(t => t.numPosto).HasColumnName("numPosto");
            this.Property(t => t.DatAtivacao).HasColumnName("DatAtivacao");
            this.Property(t => t.CodOpeAtivacao).HasColumnName("CodOpeAtivacao");
            this.Property(t => t.DatDesativacao).HasColumnName("DatDesativacao");
            this.Property(t => t.CodOpeDesativacao).HasColumnName("CodOpeDesativacao");            


        }
    }
}
