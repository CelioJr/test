using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_FabricaMap : EntityTypeConfiguration<tbl_Fabrica>
    {
        public tbl_FabricaMap()
        {
            // Primary Key
            this.HasKey(t => t.CodFab);

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NomFab)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.Sigla)
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.SiglaEDP)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.NumCGC)
                .IsFixedLength()
                .HasMaxLength(18)
                .IsUnicode(false);

            this.Property(t => t.CodLocalFat)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.NumApolice)
                .IsFixedLength()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.RazaoSocial)
                .HasMaxLength(60)
                .IsUnicode(false);

            this.Property(t => t.EndFabrica)
                .HasMaxLength(80)
                .IsUnicode(false);

            this.Property(t => t.NumSUF)
                .IsFixedLength()
                .HasMaxLength(12)
                .IsUnicode(false);

            this.Property(t => t.NumCEX)
                .IsFixedLength()
                .HasMaxLength(15)
                .IsUnicode(false);

            this.Property(t => t.NomAtend)
                .HasMaxLength(50)
                .IsUnicode(false);

            this.Property(t => t.NumInscEst)
                .HasMaxLength(14)
                .IsUnicode(false);

            this.Property(t => t.DTB_Operation)
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.CodMatriz)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_Fabrica");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.NomFab).HasColumnName("NomFab");
            this.Property(t => t.Sigla).HasColumnName("Sigla");
            this.Property(t => t.SiglaEDP).HasColumnName("SiglaEDP");
            this.Property(t => t.NumCGC).HasColumnName("NumCGC");
            this.Property(t => t.CodLocalFat).HasColumnName("CodLocalFat");
            this.Property(t => t.NumApolice).HasColumnName("NumApolice");
            this.Property(t => t.RazaoSocial).HasColumnName("RazaoSocial");
            this.Property(t => t.EndFabrica).HasColumnName("EndFabrica");
            this.Property(t => t.NumSUF).HasColumnName("NumSUF");
            this.Property(t => t.NumCEX).HasColumnName("NumCEX");
            this.Property(t => t.NomAtend).HasColumnName("NomAtend");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
            this.Property(t => t.NumInscEst).HasColumnName("NumInscEst");
            this.Property(t => t.DTB_Operation).HasColumnName("DTB_Operation");
            this.Property(t => t.CodMatriz).HasColumnName("CodMatriz");
        }
    }
}
