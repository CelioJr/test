namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_calavu
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_calavu()
        {
            pp_cp_hora = new HashSet<pp_cp_hora>();
            pp_cp_turno = new HashSet<pp_cp_turno>();
        }

        [Key]
        public long id_calavu { get; set; }

        public long id_cal { get; set; }

        public DateTime? dt_avulso { get; set; }

        public decimal? hrinicial { get; set; }

        public decimal? hrFinal { get; set; }

        [StringLength(40)]
        public string hrInicialGUI { get; set; }

        [StringLength(40)]
        public string hrFinalGUI { get; set; }

        public decimal? seg_tempocalendario { get; set; }

        public decimal? seg_tempocalsempeso { get; set; }

        public decimal? seg_toleranciapre { get; set; }

        public decimal? seg_toleranciapos { get; set; }

        public long? id_calsem { get; set; }

        public short? tp_dtreferencia { get; set; }

        public short? is_calsemdesativado { get; set; }

        public short? is_inicioturno { get; set; }

        public short? is_fimturno { get; set; }

        public long id_turno { get; set; }

        public short? tp_calavu { get; set; }

        public virtual dw_cal dw_cal { get; set; }

        public virtual dw_calsem dw_calsem { get; set; }

        public virtual dw_turno dw_turno { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cp_hora> pp_cp_hora { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cp_turno> pp_cp_turno { get; set; }
    }
}
