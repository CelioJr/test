namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_CAD_POSTOS
    {
        [Key]
        public int COD_POSTO { get; set; }

        [StringLength(20)]
        public string NOME { get; set; }

        [StringLength(100)]
        public string CAMINHO_SAV { get; set; }

        [StringLength(100)]
        public string CAMINHO_BACKUP { get; set; }

        public DateTime? DATA { get; set; }

        public bool? DESATIVADO { get; set; }

        public bool? METODO2 { get; set; }

        public int? COD_LINHA { get; set; }
    }
}
