using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_CAD_ETIQUETA_STRING_DTO
    {
        public int COD_ETIQUETA { get; set; }
        public string DESCRICAO { get; set; }
        public string STRING { get; set; }
        public Nullable<int> COD_SKU { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
        public string TIPO { get; set; }
        public Nullable<int> COD_MODELO { get; set; }
    }
}
