namespace SEMP.Model.DTO
{
	using System.ComponentModel.DataAnnotations;

	public partial class ZSSD_GET_PARCEIRO_DTO
	{
        public int id { get; set; }

        [Required]
        [StringLength(16)]
        public string PACKNO { get; set; }

        [StringLength(10)]
        public string EXTROW { get; set; }

        [StringLength(10)]
        public string CLIENTE { get; set; }

        [StringLength(4)]
        public string ORGANIZACAOVENDAS { get; set; }

        [StringLength(2)]
        public string CANALDISTRIBUICAO { get; set; }

        [StringLength(2)]
        public string SETORATIVIDADE { get; set; }

        [StringLength(2)]
        public string FUNCAOPARCEIRO { get; set; }

        [StringLength(10)]
        public string CLIENTEPARCEIRO { get; set; }

        [StringLength(10)]
        public string FORNECEDOR { get; set; }
    }
}
