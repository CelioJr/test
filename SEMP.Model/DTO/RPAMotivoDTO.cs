using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class RPAMotivoDTO
    {
        public string CodMotivo { get; set; }
        public string DesMotivo { get; set; }
        public string CodLocalOri { get; set; }
        public string CodLocalDest { get; set; }
        public System.Guid rowguid { get; set; }
    }
}
