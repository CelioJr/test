﻿var verificaLogado;
var carregando = "<div class='progress'><div class='progress-bar progress-bar-striped active' role='progressbar' aria-valuenow='100' aria-valuemin='0' aria-valuemax='100' style='width: 100%'></div></div>";


$(document).ready(function () {
	// Evento de clique do elemento: ul#menu li.parent > a
	$('ul#menu li.parent > a').click(function () {
		// Expande ou retrai o elemento ul.sub-menu dentro do elemento pai (ul#menu li.parent)
		$('ul.sub-menu', $(this).parent()).slideToggle('fast', function () {
			// Depois de expandir ou retrair, troca a classe 'aberto' do <a> clicado
			$(this).toggleClass('aberto');
		});
		return false;
	});

	setInterval('VerificaLogado();', 360000);
	//verificaLogado = setInterval('VerificaLogado();', 2000);

	setTimeout("esconder()", 5000);

	$("#balaoMensagem").toggle();
	$("#balaoMensagem").fadeToggle(1500);
});

function esconder() {
	$("#balaoMensagem").fadeToggle();
}

function LimpaGrid(gridName) {
	$(gridName).find("tbody").html("<div style='position: absolute; left: 45%; top: 50%;'><img src='/Images/carregando.gif' /></div>");
}

function LimpaGrid2(gridName) {
	$(gridName).find("tbody").html("<tr><td></td></tr>");
}

function VerificaLogin() {
	$.ajax({
		type: "GET",
		url: "/Login/UsuarioCracha/",
		success: function (data) {
			if (data == "False") {
				document.location.href = "/";
			}
		}
	});
}

function VerificaLogado() {
	console.log("Verificou login");

	if (document.location.href.indexOf("Painel") > -1 || document.location.href.indexOf("painel") > -1) { clearInterval(verificaLogado); return; }

	$.ajax({
		type: "GET",
		url: "/Login/Logado/",
		success: function (data) {
			if (data == "False") {
				var url = encodeURIComponent(document.location.href.replace("http://stavm25", "").replace("http://localhost:30117", ""));
				document.location.href = "/?returnUrl=" + url;
			}
		}
	});
}

function showtime() {
	var now = new Date();
	var hours = now.getHours();
	var minutes = now.getMinutes();
	var seconds = now.getSeconds();
	var timeValue = "" + (hours);
	timeValue += ((minutes < 10) ? ":0" : ":") + minutes
	timeValue += ((seconds < 10) ? ":0" : ":") + seconds

	return timeValue;
}

function showdate() {
	var now = new Date();
	var year = now.getFullYear();
	var month = now.getMonth() + 1;
	var day = now.getDate();
	var dateValue = "";
	dateValue += ((day < 10) ? "0" : "") + day;
	dateValue += ((month < 10) ? "/0" : "/") + month;
	dateValue += '/' + year;

	return dateValue;
}

function padLeft(nr, n, str) {
	return Array(n - String(nr).length + 1).join(str || '0') + nr;
}

function searchAndHighlight(searchTerm, selector) {
	if (searchTerm) {
		//var wholeWordOnly = new RegExp("\\g"+searchTerm+"\\g","ig"); //matches whole word only
		//var anyCharacter = new RegExp("\\g["+searchTerm+"]\\g","ig"); //matches any word with any of search chars characters
		var selector = selector || "body";                             //use body as selector if none provided
		var searchTermRegEx = new RegExp(searchTerm, "ig");
		var matches = $(selector).text().match(searchTermRegEx);
		if (matches) {
			$('.highlighted').removeClass('highlighted');     //Remove old search highlights
			$(selector).html($(selector).html()
				.replace(searchTermRegEx, "<span class='highlighted'>" + searchTerm + "</span>"));
			if ($('.highlighted:first').length) {             //if match found, scroll to where the first one appears
				//$(window).scrollTop($('.highlighted:first').position().top+75);
			}
			return true;
		}
	}
	return false;
}

function getFormattedDate(date) {
	var year = date.getFullYear();
	var month = (1 + date.getMonth()).toString();
	month = month.length > 1 ? month : '0' + month;
	var day = date.getDate().toString();
	day = day.length > 1 ? day : '0' + day;
	return day + '/' + month + '/' + year;
}

function getFormattedTime(date) {
	var hours = date.getHours().toString();
	hours = hours.length > 1 ? hours : '0' + hours;
	var minutes = date.getMinutes().toString();
	minutes = minutes.length > 1 ? minutes : '0' + minutes;
	var seconds = date.getSeconds().toString();
	seconds = seconds.length > 1 ? seconds : '0' + seconds;
	
	return hours + ':' + minutes + ':' + seconds;
}

function formatJSONDate(date) {

	var data = new Date(parseInt(date.replace('/Date(', '').replace(')/', ''), 10));

	return getFormattedDate(data);

}

function FullScreen() {
	if ((document.fullScreenElement && document.fullScreenElement !== null) ||
		(!document.mozFullScreen && !document.webkitIsFullScreen)) {
		if (document.documentElement.requestFullScreen) {
			document.documentElement.requestFullScreen();
		} else if (document.documentElement.mozRequestFullScreen) {
			document.documentElement.mozRequestFullScreen();
		} else if (document.documentElement.webkitRequestFullScreen) {
			document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
		}
	} else {
		if (document.cancelFullScreen) {
			document.cancelFullScreen();
		} else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if (document.webkitCancelFullScreen) {
			document.webkitCancelFullScreen();
		}
	}
}

// jquery extend function
$.extend(
{
	redirectPost: function (location, args) {
		var form = $('<form></form>');
		form.attr("method", "post");
		form.attr("action", location);

		$.each(args, function (key, value) {
			var field = $('<input></input>');

			field.attr("type", "hidden");
			field.attr("name", key);
			field.attr("value", value);

			form.append(field);
		});
		$(form).appendTo('body').submit();
	}
});

$.fn.scale = function (x) {
	if (!$(this).filter(':visible').length && x != 1) return $(this);
	if (!$(this).parent().hasClass('scaleContainer')) {
		$(this).wrap($('<div class="scaleContainer">').css('position', 'relative'));
		$(this).data({
			'originalWidth': $(this).width(),
			'originalHeight': $(this).height()
		});
	}
	$(this).css({
		'transform': 'scale(' + x + ')',
		'-ms-transform': 'scale(' + x + ')',
		'-moz-transform': 'scale(' + x + ')',
		'-webkit-transform': 'scale(' + x + ')',
		'transform-origin': 'right bottom',
		'-ms-transform-origin': 'right bottom',
		'-moz-transform-origin': 'right bottom',
		'-webkit-transform-origin': 'right bottom',
		'position': 'absolute',
		'bottom': '0',
		'right': '0',
	});
	if (x == 1)
		$(this).unwrap().css('position', 'static'); else
		$(this).parent()
			.width($(this).data('originalWidth') * x)
			.height($(this).data('originalHeight') * x);
	return $(this);
};

function FiltraTabela(tabela, coluna, dados) {
	$("#" + tabela + " tbody tr").show();

	var nth = "#" + tabela + " tbody td:nth-child(" + (coluna + 1).toString() + ")";

	$(nth).each(function () {
		if ($(this).text().toUpperCase().indexOf(dados.toUpperCase()) < 0) {
			$(this).parent().hide();
		}
	});
}

function FiltraTabelaMulti(tabela, coluna, dados) {

	if (dados.length == 0) {
		$("#" + tabela + " tbody tr").show();
		return;
	}
	$("#" + tabela + " tbody tr").hide();

	$(dados).each(function (key, value) {
		console.log(key + " - " + value.text);
		var nth = "#" + tabela + " tbody td:nth-child(" + (coluna + 1).toString() + ")";

		$(nth).each(function () {
			if ($(this).text().toUpperCase().indexOf(value.text.toUpperCase()) >= 0) {
				$(this).parent().show();
			}
		});
	});
	return;
}

function alterafundo() { }
function voltafundo() { }