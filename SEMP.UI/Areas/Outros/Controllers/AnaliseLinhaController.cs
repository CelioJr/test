﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using RAST.BO.ApontaProd;
using SEMP.Model.VO.Rastreabilidade.Relatorios;

namespace SEMP.Areas.Outros.Controllers
{
	[Authorize]
	public class AnaliseLinhaController : Controller
	{
		//
		// GET: /Outros/AnaliseLinha/
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult Lista(string datIni, string datFim, int codLinha)
		{

			try
			{
				var dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
				var dFim = dIni.AddDays(1).AddMinutes(-1);

				if (!String.IsNullOrEmpty(datIni))
				{
					dIni = DateTime.Parse(datIni);
				}
				if (!String.IsNullOrEmpty(datFim))
				{
					dFim = DateTime.Parse(datFim);
				}

				var dados = ProducaoHoraAHoraBO.ObterProducaoDataHora(dIni, dFim, codLinha, 0, 0,0);

				return View(dados);
			}
			catch (Exception)
			{

				return View();
			}

			
		}

		public ActionResult Teste()
		{
			return View();
		}

		public JsonResult ObterProducao(string datIni, string datFim, int codLinha, string codModelo)
		{
			var dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			var dFim = dIni.AddDays(1).AddMinutes(-1);

			if (!String.IsNullOrEmpty(datIni))
			{
				dIni = DateTime.Parse(datIni);
			}
			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}

			try
			{
				var dados = ProducaoHoraAHoraBO.ObterProducaoData(dIni, dFim, codLinha, codModelo);

				return Json(dados, JsonRequestBehavior.AllowGet);
			}
			catch (Exception)
			{
				return Json(new RetornoGraficoGargalo(), JsonRequestBehavior.AllowGet);
				
			}
		}

		public JsonResult ObterDrillDown(string datIni, string datFim, int codLinha, int numPosto, string codModelo)
		{
			var dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			var dFim = dIni.AddDays(1).AddMinutes(-1);

			if (!String.IsNullOrEmpty(datIni))
			{
				dIni = DateTime.Parse(datIni);
			}
			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}

			var dados = ProducaoHoraAHoraBO.ObterProducaoHora(dIni, dFim, codLinha, numPosto, codModelo);

			return Json(dados, JsonRequestBehavior.AllowGet);
		}

		protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
		{
			return new JsonResult()
			{
				Data = data,
				ContentType = contentType,
				ContentEncoding = contentEncoding,
				JsonRequestBehavior = behavior,
				MaxJsonLength = Int32.MaxValue
			};
		}

		public JsonResult ObterProducao2(string datIni, string datFim, int codLinha, string codModelo)
		{
			var dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			var dFim = dIni.AddDays(1).AddMinutes(-1);

			if (!String.IsNullOrEmpty(datIni))
			{
				dIni = DateTime.Parse(datIni);
			}
			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}

			try
			{
				var dados = ProducaoHoraAHoraBO.ObterProducaoData2(dIni, dFim, codLinha, codModelo);

				return Json(dados, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json(new RetornoGraficoGargalo2(){dados = new List<DadosGraficoA>(){new DadosGraficoA() {name = ex.Message}}}, JsonRequestBehavior.AllowGet);

			}
		}

	}
}