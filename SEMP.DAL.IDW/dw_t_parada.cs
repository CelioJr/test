namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_t_parada
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_t_parada()
        {
            dw_consolpa = new HashSet<dw_consolpa>();
            dw_consolpalog = new HashSet<dw_consolpalog>();
            dw_t_parada1 = new HashSet<dw_t_parada>();
            ms_pt_coleta = new HashSet<ms_pt_coleta>();
            om_cfg = new HashSet<om_cfg>();
            om_cfg1 = new HashSet<om_cfg>();
            om_cfg2 = new HashSet<om_cfg>();
        }

        [Key]
        public long id_tparada { get; set; }

        [StringLength(30)]
        public string cd_tparada { get; set; }

        public int? revisao { get; set; }

        [StringLength(100)]
        public string ds_tparada { get; set; }

        public DateTime? dt_revisao { get; set; }

        public short? st_ativo { get; set; }

        public long id_tppt { get; set; }

        public DateTime? dt_stativo { get; set; }

        public long id_usrstativo { get; set; }

        public long id_usrrevisao { get; set; }

        public short? is_FDS { get; set; }

        public short? is_MDO { get; set; }

        public short? is_MTBF { get; set; }

        public short? is_MTTR { get; set; }

        public short? is_PA { get; set; }

        public short? is_PAO { get; set; }

        public short? is_PP { get; set; }

        public short? is_PREV { get; set; }

        public short? is_PTP { get; set; }

        public short? is_SCP { get; set; }

        public short? is_pesa { get; set; }

        public short? is_regulagem { get; set; }

        public short? is_requer_acao { get; set; }

        public short? is_requer_causa { get; set; }

        public short? is_requer_just { get; set; }

        public decimal? qt_tec { get; set; }

        public long? id_area { get; set; }

        public short? is_default { get; set; }

        public short? is_sem_op { get; set; }

        public short? is_sem_conexao { get; set; }

        public short? is_sem_evento { get; set; }

        public short? is_permitecorrecao { get; set; }

        public short? is_entradarap { get; set; }

        public short? is_saidarap { get; set; }

        public long? id_tparadaextra { get; set; }

        public decimal? seg_extrapolacao { get; set; }

        public decimal? seg_timeoutalerta { get; set; }

        public long? id_talerta { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolpa> dw_consolpa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolpalog> dw_consolpalog { get; set; }

        public virtual dw_t_alerta dw_t_alerta { get; set; }

        public virtual om_tppt om_tppt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_t_parada> dw_t_parada1 { get; set; }

        public virtual dw_t_parada dw_t_parada2 { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_pt_coleta> ms_pt_coleta { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg2 { get; set; }
    }
}
