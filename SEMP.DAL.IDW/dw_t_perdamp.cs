namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_t_perdamp
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_t_perdamp()
        {
            dw_consolpemp = new HashSet<dw_consolpemp>();
            dw_consolperdamplog = new HashSet<dw_consolperdamplog>();
        }

        [Key]
        public long id_tperdamp { get; set; }

        [Required]
        [StringLength(40)]
        public string ds_perdamp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolpemp> dw_consolpemp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolperdamplog> dw_consolperdamplog { get; set; }
    }
}
