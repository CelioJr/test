namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_folhaemb
    {
        [Key]
        public long id_folhaemb { get; set; }

        public long? id_folha { get; set; }

        public long? id_produto { get; set; }

        public decimal? qt_na_embalagem { get; set; }

        public short? is_emb_default { get; set; }

        public long? id_cfgscrpimp { get; set; }

        public virtual dw_folha dw_folha { get; set; }

        public virtual om_cfgscrpimp om_cfgscrpimp { get; set; }

        public virtual om_produto om_produto { get; set; }
    }
}
