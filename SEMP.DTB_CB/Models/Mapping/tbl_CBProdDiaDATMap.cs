using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DTB_CB.Models.Mapping
{
    public class tbl_CBProdDiaDATMap : EntityTypeConfiguration<tbl_CBProdDiaDAT>
    {
        public tbl_CBProdDiaDATMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.CodLinha, t.CodModelo, t.DatProducao, t.CodCin });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.CodLinha)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8);

            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6);

            this.Property(t => t.CodCin)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6);

            this.Property(t => t.FlagTP)
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("tbl_CBProdDiaDAT");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.DatProducao).HasColumnName("DatProducao");
            this.Property(t => t.CodCin).HasColumnName("CodCin");
            this.Property(t => t.QtdProgramada).HasColumnName("QtdProgramada");
            this.Property(t => t.QtdProduzida).HasColumnName("QtdProduzida");
            this.Property(t => t.QtdFaturado).HasColumnName("QtdFaturado");
            this.Property(t => t.FlagTP).HasColumnName("FlagTP");
        }
    }
}
