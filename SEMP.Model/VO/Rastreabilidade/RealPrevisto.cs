﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade
{
    public class RealPrevisto
    {
        public int QuantidadeReal { get; set; }
        public int QuantidadePrevista { get; set; }
    }
}
