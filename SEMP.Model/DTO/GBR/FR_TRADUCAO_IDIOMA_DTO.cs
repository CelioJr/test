using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_TRADUCAO_IDIOMA_DTO
    {
        public int TRA_CODIGO { get; set; }
        public int IDI_CODIGO { get; set; }
        public string TRI_TEXTO { get; set; }
        public string TRI_SITUACAO { get; set; }
        public string TRI_HASH { get; set; }
        public virtual FR_IDIOMA_DTO FR_IDIOMA { get; set; }
        public virtual FR_TRADUCAO_DTO FR_TRADUCAO { get; set; }
    }
}
