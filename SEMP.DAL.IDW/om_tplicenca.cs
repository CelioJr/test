namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_tplicenca
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_tplicenca()
        {
            om_licenca = new HashSet<om_licenca>();
        }

        [Key]
        public long id_tplicenca { get; set; }

        [StringLength(100)]
        public string ds_licenca { get; set; }

        [StringLength(512)]
        public string chave_verificacao { get; set; }

        public long? id_tppt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_licenca> om_licenca { get; set; }

        public virtual om_tppt om_tppt { get; set; }
    }
}
