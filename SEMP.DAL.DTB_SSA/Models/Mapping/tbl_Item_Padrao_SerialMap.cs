using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.DTB_SSA.Models.Mapping
{
    public class tbl_Item_Padrao_SerialMap : EntityTypeConfiguration<tbl_Item_Padrao_Serial>
    {
        public tbl_Item_Padrao_SerialMap()
        {
            // Primary Key
            this.HasKey(t => new { t.NE, t.dispositivo_id });

            // Properties
            this.Property(t => t.NE)
                .IsRequired()
                .HasMaxLength(13);

            this.Property(t => t.dispositivo_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ExpressaoRegular)
                .IsRequired()
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("tbl_Item_Padrao_Serial");
            this.Property(t => t.NE).HasColumnName("NE");
            this.Property(t => t.dispositivo_id).HasColumnName("dispositivo_id");
            this.Property(t => t.tamMax).HasColumnName("tamMax");
            this.Property(t => t.tamMin).HasColumnName("tamMin");
            this.Property(t => t.utilizarRange).HasColumnName("utilizarRange");
            this.Property(t => t.ExpressaoRegular).HasColumnName("ExpressaoRegular");
            this.Property(t => t.CodOpeCriacao).HasColumnName("CodOpeCriacao");
            this.Property(t => t.DatCriacao).HasColumnName("DatCriacao");
            this.Property(t => t.CodOpeAlt).HasColumnName("CodOpeAlt");
            this.Property(t => t.DatAlt).HasColumnName("DatAlt");
        }
    }
}
