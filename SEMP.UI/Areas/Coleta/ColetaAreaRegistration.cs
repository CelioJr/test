﻿using System.Web.Mvc;

namespace SEMP.Areas.Coleta
{
	public class ColetaAreaRegistration : AreaRegistration
	{
		public override string AreaName
		{
			get
			{
				return "Coleta";
			}
		}

		public override void RegisterArea(AreaRegistrationContext context)
		{
			context.MapRoute(
				"Coleta_default",
				"Coleta/{controller}/{action}/{id}",
				new {controle = "Home",  action = "Index", id = UrlParameter.Optional }
			);
		}
	}
}
