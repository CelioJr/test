using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBOBATipoStatusMap : EntityTypeConfiguration<tbl_CBOBATipoStatus>
    {
		public tbl_CBOBATipoStatusMap()
		{
			// Primary Key
			this.HasKey(t => new { t.CodStatus });

			// Properties
			this.Property(t => t.CodStatus)
				.IsRequired()
				.HasMaxLength(2)
                .IsUnicode(false);

			this.Property(t => t.DesStatus)
				.IsFixedLength()
				.HasMaxLength(50)
                .IsUnicode(false);

			this.Property(t => t.DesCor)
				.IsFixedLength()
				.HasMaxLength(20)
                .IsUnicode(false);

			// Table & Column Mappings
			this.ToTable("tbl_CBOBATipoStatus");
			this.Property(t => t.CodStatus).HasColumnName("CodStatus");
			this.Property(t => t.DesStatus).HasColumnName("DesStatus");
			this.Property(t => t.DesCor).HasColumnName("DesCor");
		}
	}
}
