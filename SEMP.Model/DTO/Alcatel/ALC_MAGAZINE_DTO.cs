using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_MAGAZINE_DTO
    {
        public ALC_MAGAZINE_DTO()
        {
            this.ALC_ENGINE = new List<ALC_ENGINE_DTO>();
        }

        public string ID_MAG { get; set; }
        public Nullable<System.DateTime> DT_CREATION { get; set; }
        public Nullable<int> QTY_PROD { get; set; }
        public Nullable<int> QUANTITY { get; set; }
        public string STATUS { get; set; }
        public string OP { get; set; }
        public virtual ICollection<ALC_ENGINE_DTO> ALC_ENGINE { get; set; }
        public virtual ALC_ORDER_DTO ALC_ORDER { get; set; }
    }
}
