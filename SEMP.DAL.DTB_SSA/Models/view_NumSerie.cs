namespace SEMP.DAL.DTB_SSA.Models
{
    using System;

    public partial class view_NumSerie
    {
        public string DesSerie { get; set; }
        public int CodOF { get; set; }
        public string TipoMaquina { get; set; }
        public string CodModelo { get; set; }
        public string DesTitulo { get; set; }
        public string DesConfig { get; set; }
        public string DesModelo { get; set; }
        public string PartNumber { get; set; }
        public string Etiqueta { get; set; }
        public string PrecisaConfig { get; set; }
        public string PrecisaEAN { get; set; }
        public string PrecisaNomFantasia { get; set; }
        public int? codOfMae { get; set; }
        public string codSO { get; set; }
        public DateTime? datSepAlmox { get; set; }
        public string OpeSepAlmox { get; set; }
        public DateTime? dat1 { get; set; }
        public int? codope1 { get; set; }
        public string OpeSubEstoque { get; set; }
        public DateTime? dat2 { get; set; }
        public int? codope2 { get; set; }
        public string OpeMontagem { get; set; }
        public DateTime? dat3 { get; set; }
        public int? codope3 { get; set; }
        public string OpeRunIn { get; set; }
        public DateTime? dat4 { get; set; }
        public int? codope4 { get; set; }
        public string OpeReparo { get; set; }
        public DateTime? dat5 { get; set; }
        public int? codope5 { get; set; }
        public string OpeLiberacao { get; set; }
        public DateTime? dat6 { get; set; }
        public int? codOpe6 { get; set; }
        public string OpeEmbalagem { get; set; }
        public DateTime? datAprovacaoCQ { get; set; }
        public int? CodOpeAprovacaoCQ { get; set; }
        public string OpeAprovacaoCQ { get; set; }
        public DateTime? dat7 { get; set; }
        public int? codope7 { get; set; }
        public string OpeEstoque { get; set; }
        public byte codAtual { get; set; }
        public DateTime? dat10 { get; set; }
        public int? codope10 { get; set; }
        public string OpeDat10 { get; set; }
        public bool vinil { get; set; }
        public DateTime? datDownload { get; set; }
        public int? codOpeDownload { get; set; }
        public string OpeDownload { get; set; }
        public string CODEAN { get; set; }
        public string descliente { get; set; }
        public int numQtde { get; set; }
        public string LicencaWindows { get; set; }
        public DateTime? datEmbalPosto1 { get; set; }
        public byte? LinhaMontagem { get; set; }
        public string Tipo_Linha { get; set; }
        public string NomSO { get; set; }
        public string ExpressaoHCL { get; set; }
        public string LicencaSoftware { get; set; }
        public int? imagem_id { get; set; }
        public string ModeloVPE { get; set; }
        public string NumSerieToshiba { get; set; }
        public DateTime? datWakeUp { get; set; }
        public int? codOpeWakeUp { get; set; }
        public string OpeWakeUp { get; set; }
        public DateTime? datLiberacao { get; set; }
        public int? codOpeLiberacao { get; set; }
    }
}
