using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_PROFILE_INSTANCE_DTO
    {
        public string PRI_CODIGO { get; set; }
        public string PRI_DESCRIPTION { get; set; }
        public System.DateTime PRI_DATE { get; set; }
        public int USR_CODIGO { get; set; }
        public string SIS_CODIGO { get; set; }
    }
}
