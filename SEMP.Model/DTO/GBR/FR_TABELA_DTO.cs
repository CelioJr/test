using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_TABELA_DTO
    {
        public FR_TABELA_DTO()
        {
            this.FR_CAMPO = new List<FR_CAMPO_DTO>();
        }

        public string TAB_NOME { get; set; }
        public string TAB_DESCRICAO { get; set; }
        public virtual ICollection<FR_CAMPO_DTO> FR_CAMPO { get; set; }
    }
}
