namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_INTERVAL")]
    public partial class ALC_INTERVAL
    {
        [Key]
        public int ID_INTERVAL { get; set; }

        public int? INTERVAL_HOUR { get; set; }

        public int? QTY_MINUTES { get; set; }

        public int? COD_LINHA { get; set; }
    }
}
