using System;
using System.Collections.Generic;

namespace SEMP.Model.VO.Mobilidade
{
    public partial class ResumoProducaoAlcatel
    {
        public int? COD_SKU { get; set; }
		public string SKU { get; set; }
		public DateTime DATA { get; set; }
		public int? QTD_PLAN { get; set; }
		public int? QTD_PROD { get; set; }
		public int CodLinha { get; set; }
		public string DesLinha { get; set; }
	}
}
