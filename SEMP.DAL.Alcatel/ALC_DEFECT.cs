namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_DEFECT")]
    public partial class ALC_DEFECT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ALC_DEFECT()
        {
            ALC_OQC_INSPECTION = new HashSet<ALC_OQC_INSPECTION>();
            ALC_RMB_REGISTER = new HashSet<ALC_RMB_REGISTER>();
            ALC_REPAIR = new HashSet<ALC_REPAIR>();
        }

        [Key]
        [StringLength(10)]
        public string COD_DEFECT { get; set; }

        [StringLength(100)]
        public string DEFECT { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_OQC_INSPECTION> ALC_OQC_INSPECTION { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_RMB_REGISTER> ALC_RMB_REGISTER { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_REPAIR> ALC_REPAIR { get; set; }
    }
}
