namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_LOG_PRODUCT")]
    public partial class ALC_LOG_PRODUCT
    {
        [Key]
        [StringLength(30)]
        public string serial { get; set; }

        [StringLength(30)]
        public string imei1 { get; set; }

        [StringLength(30)]
        public string imei2 { get; set; }

        [StringLength(50)]
        public string suid { get; set; }

        public DateTime? system_date { get; set; }

        public string sd_receiopt { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? file_date { get; set; }
    }
}
