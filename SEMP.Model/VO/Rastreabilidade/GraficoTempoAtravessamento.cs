﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade
{
	public class GraficoTempoAtravessamento
	{
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public string CodPlaca { get; set; }
		public string DesPlaca { get; set; }
		public DateTime Data { get; set; }

		public decimal TempoPadrao { get; set; }
		public decimal TempoAtravessamento { get; set; }

		public decimal TempoPadraoIAC { get; set; }
		public decimal TempoPadraoIMC { get; set; }
		public decimal TempoPadraoFEC { get; set; }
		public decimal TempoIAC { get; set; }
		public decimal TempoIMC { get; set; }
		public decimal TempoFEC { get; set; }

	}
}
