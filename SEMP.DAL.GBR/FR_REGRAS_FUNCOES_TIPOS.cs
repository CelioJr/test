namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_REGRAS_FUNCOES_TIPOS
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FTP_COD { get; set; }

        [Required]
        [StringLength(50)]
        public string FTP_NOME { get; set; }
    }
}
