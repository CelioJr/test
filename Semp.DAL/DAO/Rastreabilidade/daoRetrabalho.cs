﻿using System;
using System.Collections.Generic;
using System.Linq;
using SEMP.DAL.Models;
using SEMP.Model.DTO;
using SEMP.DAL.DTB_SSA.Models;

namespace SEMP.DAL.DAO.Rastreabilidade
{
    public class daoRetrabalho
	{
        //static daoRetrabalho()
        //{
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<tbl_CBRetrabalho, CBRetrabalhoDTO>();
        //        cfg.CreateMap<tbl_HistoricoReimpressao, HistoricoReimpressaoDTO>();
        //    });
        //}

        #region Consultas

        public static List<CBRetrabalhoDTO> ObterRetrabalhos()
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{				
				var retrabalhos = (from p in dtb_CB.tbl_CBRetrabalho select p).ToList();

				List<CBRetrabalhoDTO> retrabalhosDTO = new List<CBRetrabalhoDTO>();

				AutoMapper.Mapper.Map(retrabalhos, retrabalhosDTO);

				return retrabalhosDTO;
			}
		}

		public static int ObterUltimoCodRetrabalho()
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var codRetrabalho = dtb_CB.tbl_CBRetrabalho.Max(x => x.CodRetrabalho);

				return codRetrabalho;
			}
		}	
		
		public static int ObterTotalRetrabalhoInf()
		{
			using (DtbSsaContext dtb_SSA = new DtbSsaContext())
			{
				var count = dtb_SSA.tbl_HistoricoReimpressao.Where(x => x.DatImpressao == DateTime.Today).Select(x => x.DesSerie).Distinct().Count();

				return count;
			}
		}


        public static List<SIMRelatorioRetrabalhoDTO> ObterRetrabalhoSimPorData(DateTime datIni, DateTime datFim)
        {
            using (DTB_SIMContext dtb_SIM = new DTB_SIMContext())
            {
                datFim = datFim.AddDays(1);

                var retrabalhoSim = (from p in dtb_SIM.tbl_Retrabalho
                                     //join m in dtb_SIM.tbl_Item on p.NS.Substring(0,6) equals m.CodItem
                                     from m in dtb_SIM.tbl_Item.Where(x => x.CodItem == p.NS.Substring(0, 6)).DefaultIfEmpty()
                                     where p.DatLeitura >= datIni && p.DatLeitura <= datFim
                                     select new SIMRelatorioRetrabalhoDTO
                                     {
                                         NS = p.NS,
                                         CodItem = m.CodItem.Substring(0,6),
                                         DesItem = m.DesItem,
                                         DatLeitura = p.DatLeitura
                                     }

                                     ).ToList();

                return retrabalhoSim;

            }

        }

		public static List<HistoricoReimpressaoDTO> ObterRetrabalhoInfoPorData(DateTime datIni, DateTime datFim)
		{
			using (DtbSsaContext dtb_SSA = new DtbSsaContext())
			{
				/*var retrabalho = (from p in dtb_SSA.tbl_HistoricoReimpressao
									where p.DatImpressao >= datIni && p.DatImpressao <= datFim
									select p).ToList();*/

				var retrabalho = (from p in dtb_SSA.tbl_HistoricoReimpressao
									join i in dtb_SSA.TblHistorico on p.DesSerie equals i.desSerie
									join l in dtb_SSA.Tbl_Of on i.codOF equals l.codOF									
									where p.DatImpressao >= datIni && p.DatImpressao <= datFim
									select new 
									{p.DesSerie, 
									p.DatImpressao,
									l.codModelo
									}).ToList();

				var retrabalhosDistinct = retrabalho.OrderByDescending(x => x.DatImpressao).Select(p => p.DesSerie).Distinct().ToList();

				List<tbl_HistoricoReimpressao> retrabalhos = new List<tbl_HistoricoReimpressao>();				

				foreach (var item in retrabalhosDistinct)
				{
					var data = retrabalho.Where(x => x.DesSerie == item).OrderByDescending(x => x.DatImpressao).Select(x => x.DatImpressao).FirstOrDefault();
					var modelo = retrabalho.Where(x => x.DesSerie == item).OrderByDescending(x => x.DatImpressao).Select(x => x.codModelo).FirstOrDefault();
					tbl_HistoricoReimpressao historicoReimpressao = new tbl_HistoricoReimpressao();
					historicoReimpressao.DesSerie = item;
					historicoReimpressao.DatImpressao = data;					
					retrabalhos.Add(historicoReimpressao);
				}
								

				List<HistoricoReimpressaoDTO> retrabalhoDTO = new List<HistoricoReimpressaoDTO>();

				AutoMapper.Mapper.Map(retrabalhos, retrabalhoDTO);

				foreach (var item in retrabalhoDTO)
				{
					item.CodModelo = retrabalho.Where(x => x.DesSerie == item.DesSerie).Select(x => x.codModelo).FirstOrDefault();
				}
				
				return retrabalhoDTO;				
			}
		}

		public static List<HistoricoReimpressaoDTO> ObterAPReimpressao(List<HistoricoReimpressaoDTO> historico)
		{
			using (DtbSsaContext dtb_SSA = new DtbSsaContext())
			{
				foreach (var item in historico)
				{
					var codOF = (from p in dtb_SSA.TblHistorico
									where p.desSerie == item.DesSerie
									select p.codOF).FirstOrDefault();
					
					item.CodModelo = (from p in dtb_SSA.Tbl_Of
										where p.codOF == codOF
										select p.codModelo).FirstOrDefault();
				}

				return historico;
			}
		}


        // LISTA RETRABALHO

        public static List<CBRelatorioRetrabalhoDTO> ListarRetrabalho(DateTime datIni, DateTime datFim)
        {
            using (DTB_CBContext dtb_CB = new DTB_CBContext())

            {
                datFim = datFim.AddDays(1);
                var retrabalho = (from p in dtb_CB.tbl_CBPassagem
                                  join i in dtb_CB.tbl_CBLinha on p.CodLinha equals i.CodLinha
                                  join l in dtb_CB.tbl_Item on p.NumECB.Substring(0, 6) equals l.CodItem                                                                    
                                  join posto in dtb_CB.tbl_CBPosto on new { p.CodLinha, p.NumPosto } equals new { posto.CodLinha, posto.NumPosto }
                                  
                                  where p.DatEvento >= datIni 
                                     && p.DatEvento <= datFim 
                                     //&& p.CodLinha != 40 && p.NumPosto == 14
                                     //&& p.CodLinha != 40 && p.CodLinha != 34 
                                     && i.Setor == "FEC"
                                     && posto.CodTipoPosto == 36

                                  select new { 
                                     NumeroSerie = p.NumECB,
                                     Modelo = l.DesItem,
                                     Linha = i.DesLinha,
                                     Data = p.DatEvento
                                  
                                  }).ToList();

                List<CBRelatorioRetrabalhoDTO> listaRetrabalhoDTO = new List<CBRelatorioRetrabalhoDTO>();

                foreach (var item in retrabalho)
                {
                    CBRelatorioRetrabalhoDTO listaExibe = new CBRelatorioRetrabalhoDTO();
                    listaExibe.NumECB = item.NumeroSerie;
                    listaExibe.Modelo = item.Modelo;
                    listaExibe.Linha = item.Linha;
                    listaExibe.DatEvento = item.Data;

                    listaRetrabalhoDTO.Add(listaExibe);
               
                }

              return listaRetrabalhoDTO;

            }

        }

         //TOTAL RETRABALHADO

         public static int TotalCount (DateTime dateIni, DateTime dateFim) 
         {
             using (DTB_CBContext dtb_CB = new DTB_CBContext())
             {
                 //var total = dtb_CB.tbl_CBPassagem.Where(x => x.DatEvento >= dateIni && x.DatEvento <= dateFim 
                 //                                           && x.NumPosto == 14 && x.CodLinha != 40 && x.CodLinha != 34 ).Count();

                 var total = (from p in dtb_CB.tbl_CBPassagem
                              join posto in dtb_CB.tbl_CBPosto on new { p.CodLinha, p.NumPosto } equals new { posto.CodLinha, posto.NumPosto }
                                where p.DatEvento >= dateIni
                                && p.DatEvento <= dateFim
                                && posto.CodTipoPosto == 36
                               select p).Count(); 
                 
                 return total;
             }
         }

		#endregion	


     

		#region Gravações

		/*public static Boolean GravarRetrabalho(CBRetrabalhoDTO retrabalhoDTO)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				
			}
		}*/

		#endregion			
	}
}
