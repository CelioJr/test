namespace SEMP.Model.DTO
{
    public class viw_BarrasProducaoPlacaDTO
    {
        public string CodFab { get; set; }
        public string CodLinha { get; set; }
        public System.DateTime DatReferencia { get; set; }
        public System.DateTime DatLeitura { get; set; }
        public string CodOperador { get; set; }
        public string CodApontador { get; set; }
        public string CodModelo { get; set; }
        public string NumSerieModelo { get; set; }
        public string NomEstacao { get; set; }
        public string NumAP { get; set; }
    }
}
