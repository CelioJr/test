using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DEP_REGRA_TABELA_DTO
    {
        public int REG_COD { get; set; }
        public string TAB_NOME { get; set; }
    }
}
