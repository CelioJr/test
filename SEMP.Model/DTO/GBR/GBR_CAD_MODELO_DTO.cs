using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_CAD_MODELO_DTO
    {
        public int COD_MODELO { get; set; }
        public string SHORTCODE { get; set; }
        public string MODELO { get; set; }
        public string ANATEL { get; set; }
        public string NOME_MODELO { get; set; }
        public string PCBA { get; set; }
        public string ORIGEM { get; set; }
        public string BLUETOOTH_QD_ID { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
        public Nullable<int> POS_IMEI1 { get; set; }
        public Nullable<int> POS_IMEI2 { get; set; }
        public Nullable<int> POS_SKU { get; set; }
        public string PI_MAIN { get; set; }
        public string PI_SUB1 { get; set; }
        public string PI_SUB2 { get; set; }
        public Nullable<int> DONOTLINE { get; set; }
        public Nullable<bool> ATIVO { get; set; }
        public Nullable<bool> IMPRESSAO_SMT_LENTA { get; set; }
        public Nullable<bool> ATIVAR_TAC { get; set; }
        public Nullable<int> NO_DIGITOS_IMEI { get; set; }
    }
}
