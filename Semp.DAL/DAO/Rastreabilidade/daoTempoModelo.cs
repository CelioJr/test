﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.Model.DTO;
using SEMP.DAL.Models;
using System.Data.Entity;

namespace SEMP.DAL.DAO.Rastreabilidade
{
    public class daoTempoModelo
    {

        public static List<CBTempoModeloDTO> ListaTempoModelo()
        {
            using (var db = new DTB_CBContext())
            {                
                return db.CBTempoModeloDTO.ToList();
            }
        }

        public static CBTempoModeloDTO DetalheItem(int id)
        {
            using (var db = new DTB_CBContext())
            {
                var obj = db.CBTempoModeloDTO.Find(id);
                obj.flgTipoProduto = obj.flgTipoProduto.Trim();

                return obj;
            }
        }

        public static bool Criar(CBTempoModeloDTO tempoModelo)
        {
            using (var db = new DTB_CBContext())
            {
                try
                {
                    db.CBTempoModeloDTO.Add(tempoModelo);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }


        public static bool Editar(CBTempoModeloDTO tempoModelo)
        {
            using (var db = new DTB_CBContext())
            {
                try
                {
                    db.Entry(tempoModelo).State = EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static bool Deletar(int id)
        {
            using (var db = new DTB_CBContext())
            {
                try
                {
                    CBTempoModeloDTO tbl_cbtempomodelo = db.CBTempoModeloDTO.Find(id);
                    db.CBTempoModeloDTO.Remove(tbl_cbtempomodelo);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
    }
}
