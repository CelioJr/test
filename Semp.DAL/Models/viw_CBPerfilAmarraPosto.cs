using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class viw_CBPerfilAmarraPosto
    {
        public int CodLinha { get; set; }
        public int NumPosto { get; set; }
        public int CodTipoAmarra { get; set; }
        public string DesTipoAmarra { get; set; }
        public string CodFam { get; set; }
    }
}
