using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_FAILURE_DTO
    {
        public ALC_FAILURE_DTO()
        {
            this.ALC_DOWNTIME = new List<ALC_DOWNTIME_DTO>();
        }

        public string ID_FAILURE { get; set; }
        public string DESCRIPTION { get; set; }
        public virtual ICollection<ALC_DOWNTIME_DTO> ALC_DOWNTIME { get; set; }
    }
}
