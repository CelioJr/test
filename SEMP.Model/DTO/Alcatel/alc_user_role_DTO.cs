using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class alc_user_role_DTO
    {
        public int id_role { get; set; }
        public string authority { get; set; }
        public Nullable<int> id_user { get; set; }
    }
}
