namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ZSPP_OP_PCI_LOG
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ZSPP_OP_PCI_LOG()
        {
            ZSPP_APONTAMENTO_PCI_LOG = new HashSet<ZSPP_APONTAMENTO_PCI_LOG>();
        }

        [Key]
        [StringLength(10)]
        public string EXTROW { get; set; }

        [StringLength(12)]
        public string AUFNR { get; set; }

        public decimal? YIELD { get; set; }

        [StringLength(3)]
        public string MEINS { get; set; }

        [StringLength(10)]
        public string MBLNR { get; set; }

        public DateTime? LOGDATE { get; set; }

        public TimeSpan? LOGTIME { get; set; }

        [StringLength(12)]
        public string USERNAME { get; set; }

        [StringLength(1)]
        public string TYPE { get; set; }

        [StringLength(40)]
        public string MATNR { get; set; }

        [StringLength(255)]
        public string MESSAGE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ZSPP_APONTAMENTO_PCI_LOG> ZSPP_APONTAMENTO_PCI_LOG { get; set; }
    }
}
