using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBLinhaDefeitoRelatorio
    {
        public int CodLinha { get; set; }
        public System.DateTime DatProducao { get; set; }
        public string CodDefeito { get; set; }
    }
}
