namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_REGRAS_TIPOS
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TIP_COD { get; set; }

        [StringLength(20)]
        public string TIP_NOME { get; set; }

        public int? TIP_VISIVEL { get; set; }

        [StringLength(200)]
        public string TIP_EQUIVALENTE { get; set; }

        public int? TIP_ASPAS { get; set; }

        public int? TIP_DEFAULT { get; set; }

        public int? TIP_CATEGORIA { get; set; }

        public int? TIP_SUPER { get; set; }

        [StringLength(30)]
        public string TIP_NOME_INTERNO { get; set; }

        public int? TIP_TAM_OBRIGATORIO { get; set; }

        public int? TIP_VISIVEL_PARAM_ENTRADA { get; set; }

        public int? TIP_VISIVEL_VAR { get; set; }

        public int? TIP_VISIVEL_CONST { get; set; }

        public int? TIP_VISIVEL_PARAM_SAIDA { get; set; }
    }
}
