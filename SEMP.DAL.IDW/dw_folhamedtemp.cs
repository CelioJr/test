namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_folhamedtemp
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_folhamedtemp()
        {
            dw_folhamedtemhor = new HashSet<dw_folhamedtemhor>();
        }

        [Key]
        public long id_folhamedtemp { get; set; }

        public long id_folha { get; set; }

        public short tp_armazenamento { get; set; }

        public decimal qt_armazenamento { get; set; }

        public decimal seg_intervalo_leitura { get; set; }

        public virtual dw_folha dw_folha { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhamedtemhor> dw_folhamedtemhor { get; set; }
    }
}
