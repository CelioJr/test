using System;

namespace SEMP.DAL.Models
{
    public partial class tbl_PlanoIAC
    {
        public string CodModelo { get; set; }
        public string NumRevisao { get; set; }
        public string CodProcesso { get; set; }
        public string CodStatus { get; set; }
        public DateTime? DatStatus { get; set; }
        public DateTime? DatAtivo { get; set; }
        public DateTime? DatInativo { get; set; }
        public DateTime? DatUltAlt { get; set; }
        public string NomAprovacao { get; set; }
        public string NomEmissor { get; set; }
        public string NomDocLDC { get; set; }
        public string Amplitude { get; set; }
        public System.Guid rowguid { get; set; }
        public short FlgLeftRight { get; set; }
    }
}
