using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_CAD_SKU_DTO
    {
        public GBR_CAD_SKU_DTO()
        {
            this.GBR_CADASTRO_DE_PATH_FT = new List<GBR_CADASTRO_DE_PATH_FT_DTO>();
        }

        public int COD_SKU { get; set; }
        public string SKU { get; set; }
        public Nullable<int> COD_MODELO { get; set; }
        public string CLIENTE { get; set; }
        public string COR { get; set; }
        public string SAPCODE { get; set; }
        public string EAN { get; set; }
        public string PACK_STRING { get; set; }
        public string PALLET_STRING { get; set; }
        public Nullable<decimal> PESO_MIN { get; set; }
        public Nullable<decimal> PESO_MAX { get; set; }
        public Nullable<bool> PESO_ATIVO { get; set; }
        public Nullable<int> TIME_OUT { get; set; }
        public Nullable<decimal> PESO_MAX_COLETIVO { get; set; }
        public Nullable<decimal> PESO_MIN_COLETIVO { get; set; }
        public Nullable<bool> PESO_COLETIVO_ATIVO { get; set; }
        public Nullable<bool> CONFIRMAR_CAMPOS_ADICIONAIS { get; set; }
        public Nullable<bool> CONFIRMAR_2D_CX_COL { get; set; }
        public string PRODUCTCODES { get; set; }
        public string IMPORTATION_LABEL { get; set; }
        public Nullable<int> QTD_PRODUTO_COLETIVO { get; set; }
        public Nullable<int> QTD_PRODUTO_PALLET { get; set; }
        public Nullable<bool> ATIVO { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
        public Nullable<bool> BALANCA_RAPIDA { get; set; }
        public string PATH_SAP_FILE_COLETIVA { get; set; }
        public string PATH_SAP_FILE_PALLET { get; set; }
        public Nullable<int> QTD_COLMEIA { get; set; }
        public string PATH_COLMEIA { get; set; }
        public Nullable<int> TOTAL_COLMEIA { get; set; }
        public string PCBA { get; set; }
        public string BOM { get; set; }
        public Nullable<int> QTD_CHECK_CQ { get; set; }
        public Nullable<bool> CHK_CQ_PALLET { get; set; }
        public Nullable<bool> CONFIRMAR_IMEI_PALLET { get; set; }
        public Nullable<bool> CHK_SERIAL_CX_INDIVIDUAL { get; set; }
        public Nullable<int> DELAY_CX_INDIVIDUAL { get; set; }
        public Nullable<bool> CHK_IMEI_AP_COLETIVA { get; set; }
        public Nullable<int> QTD_PRODUTO_VOLUME { get; set; }
        public string PATH_SAP_FILE_VOLUME { get; set; }
        public string VOLUME_STRING { get; set; }
        public Nullable<bool> IGNORAR { get; set; }
        public Nullable<bool> RETIRA_ESPACO_PALLET { get; set; }
        public string PATH_REMESSA { get; set; }
        public Nullable<bool> VERIFICAR_SKU_SAV { get; set; }
        public Nullable<bool> CONTADORES_ATIVOS { get; set; }
        public Nullable<int> QTD_COLETIVA_LIN { get; set; }
        public Nullable<int> QTD_COLETIVA_COL { get; set; }
        public Nullable<int> QTD_COLETIVA_BARCODE { get; set; }
        public Nullable<int> SLCB_PORTA { get; set; }
        public string SLCB_HOSTNAME { get; set; }
        public Nullable<int> SLCB_TIMEOUT { get; set; }
        public Nullable<int> SLCB_ORDEM_IMEI { get; set; }
        public Nullable<int> SLCB_ORDEM_SKU { get; set; }
        public Nullable<int> SLCB_ORDEM_EAN { get; set; }
        public Nullable<int> SLCB_ORDEM_SAP { get; set; }
        public string SLCB_PATHFILE_ERROS { get; set; }
        public Nullable<bool> SLCB_PESO_ATIVO { get; set; }
        public virtual ICollection<GBR_CADASTRO_DE_PATH_FT_DTO> GBR_CADASTRO_DE_PATH_FT { get; set; }
    }
}
