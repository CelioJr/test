﻿using System;

namespace SEMP.Model.VO.SAP
{
	public class BAPI_ORDER_HEADER1
	{
		public String ORDER_NUMBER { get; set; }
		public String PRODUCTION_PLANT { get; set; }
		public String MRP_CONTROLLER { get; set; }
		public String PRODUCTION_SCHEDULER { get; set; }
		public String MATERIAL { get; set; }
		public DateTime EXPL_DATE { get; set; }
		public int ROUTING_NO { get; set; }
		public int RESERVATION_NUMBER { get; set; }
		public DateTime SCHED_RELEASE_DATE { get; set; }
		public DateTime ACTUAL_RELEASE_DATE { get; set; }
		public DateTime FINISH_DATE { get; set; }
		public DateTime START_DATE { get; set; }
		public DateTime PRODUCTION_FINISH_DATE { get; set; }
		public DateTime PRODUCTION_START_DATE { get; set; }
		public DateTime ACTUAL_START_DATE { get; set; }
		public DateTime ACTUAL_FINISH_DATE { get; set; }
		public Decimal SCRAP { get; set; }
		public Decimal TARGET_QUANTITY { get; set; }
		public String UNIT { get; set; }
		public String UNIT_ISO { get; set; }
		public String PRIORITY { get; set; }
		public String ORDER_TYPE { get; set; }
		public String ENTERED_BY { get; set; }
		public DateTime ENTER_DATE { get; set; }
		public String DELETION_FLAG { get; set; }
		public int WBS_ELEMENT { get; set; }
		public int CONF_NO { get; set; }
		public int CONF_CNT { get; set; }
		public int INT_OBJ_NO { get; set; }
		public String SCHED_FIN_TIME { get; set; }
		public String SCHED_START_TIME { get; set; }
		public String COLLECTIVE_ORDER { get; set; }
		public int ORDER_SEQ_NO { get; set; }
		public String FINISH_TIME { get; set; }
		public String START_TIME { get; set; }
		public String ACTUAL_START_TIME { get; set; }
		public String LEADING_ORDER { get; set; }
		public String SALES_ORDER { get; set; }
		public int SALES_ORDER_ITEM { get; set; }
		public String PROD_SCHED_PROFILE { get; set; }
		public String MATERIAL_TEXT { get; set; }
		public String SYSTEM_STATUS { get; set; }
		public Decimal CONFIRMED_QUANTITY { get; set; }
		public String PLAN_PLANT { get; set; }
		public String BATCH { get; set; }
		public String MATERIAL_EXTERNAL { get; set; }
		public String MATERIAL_GUID { get; set; }
		public String MATERIAL_VERSION { get; set; }
		public DateTime DATE_OF_EXPIRY { get; set; }
		public DateTime DATE_OF_MANUFACTURE { get; set; }
		public String MATERIAL_LONG { get; set; }

	}
}
