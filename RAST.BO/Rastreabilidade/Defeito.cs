﻿using System;
using System.Collections.Generic;
using System.Linq;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.VO;
using SEMP.BO;
using SEMP.Model.DTO;
using SEMP.Model;
using SEMP.Model.VO.CNQ;
using SEMP.Model.VO.Rastreabilidade.Relatorios;

namespace RAST.BO.Rastreabilidade
{
	public class Defeito
	{
		#region Manutenções
		public static Mensagem AddDefeitoRastreamento(string numECB, string codErro, string codDRT, int codLinha, int numPosto)
		{
			Mensagem erro = new Mensagem();

			var mensagem = daoMensagem.ObterMensagensByCodigo(codErro);

			erro.CodMensagem = codErro;
			erro.DesMensagem = mensagem.DesMensagem;
			erro.DesCor = mensagem.DesCor;
			erro.DesSom = mensagem.DesSom;
			erro.DatStatus = Util.GetDate();

			CBDefeitoRastreamentoDTO defeito = new CBDefeitoRastreamentoDTO();

			defeito.NumECB = numECB;
			defeito.CodDefeito = codErro;
			defeito.CodDRT = codDRT;
			defeito.CodLinha = codLinha;
			defeito.NumPosto = numPosto;
			defeito.DatEvento = Util.GetDate();

			string inserir = daoMensagem.InsertDefeitoRastreamento(defeito);

			if (!inserir.Equals(String.Empty))
			{
				erro.CodMensagem = Constantes.DB_ERRO;
				erro.DesMensagem = "Erro ao gravar defeito de rastreamento: " + inserir + "\n" + erro.DesMensagem;
			}

			return erro;

		}

		public static String ObterDesDefeito(string codDefeito)
		{
			return daoDefeitos.ObterDesDefeito(codDefeito);
		}

		public static Mensagem GravarPosicao(string NumSerie, string Posicao)
		{
			Mensagem retorno = new Mensagem();

			CBDefeitoDTO defeitoDTO = daoDefeitos.ObterDefeitoAberto(NumSerie);

			if (defeitoDTO != null)
			{
				defeitoDTO.Posicao = Posicao;
				bool resultado = daoDefeitos.GravarPosicao(defeitoDTO);
				if(resultado == true)
					retorno = Defeito.ObterMensagem(Constantes.RAST_OK);
				else
					retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
			}
			else
			{
				retorno = Defeito.ObterMensagem(Constantes.RAST_REGISTRO_NAO_ENCONTRADO);
			}

			return retorno;
		}

		public static Mensagem GravarDefeito(int CodLinha, int NumPosto, string NumSerie, string CodDefeito, string DrtUsuario, string NumSeriePai)
		{
			Mensagem retorno = new Mensagem();

			try
			{

				CBDefeitoDTO defeito = new CBDefeitoDTO();

				defeito.CodLinha = CodLinha;
				defeito.NumPosto = NumPosto;
				defeito.NumECB = NumSerie;
				defeito.CodDefeito = CodDefeito;
				defeito.FlgRevisado = Constantes.DEF_ABERTO;
				defeito.CodDRTTest = DrtUsuario;
				defeito.DatEvento = SEMP.BO.Util.GetDate();
				defeito.NumSeriePai = NumSeriePai;


				Boolean gravar = daoDefeitos.InsereDefeito(defeito);
				if (gravar)
				{
				    daoDefeitos.ApagarPassagemInformatica(CodLinha, NumPosto, NumSerie);
					retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);
				}
				else
				{
					retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				}

			}
			catch (Exception ex)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				retorno.StackErro = ex.InnerException.Message;
			}

			return retorno;
		}

		public static Mensagem GravarDefeito(int CodLinha, int NumPosto, string NumSerie, DateTime DatEvento, string CodDefeito, string DrtUsuario, string NumSeriePai)
		{
			Mensagem retorno = new Mensagem();

			try
			{

				CBDefeitoDTO defeito = new CBDefeitoDTO();

				defeito.CodLinha = CodLinha;
				defeito.NumPosto = NumPosto;
				defeito.NumECB = NumSerie;
				defeito.CodDefeito = CodDefeito;
				defeito.FlgRevisado = Constantes.DEF_ABERTO;
				defeito.CodDRTTest = DrtUsuario;
				defeito.DatEvento = DatEvento;
				defeito.NumSeriePai = NumSeriePai;


				Boolean gravar = daoDefeitos.InsereDefeito(defeito);
				if (gravar)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);
				}
				else
				{
					retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				}

			}
			catch (Exception ex)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				retorno.StackErro = ex.InnerException.Message;
			}

			return retorno;
		}
		#endregion

		public static Mensagem ObterMensagem(string codMensagem)
		{
			return daoMensagem.ObterMensagem(codMensagem);
		}

		public static List<viw_CBDefeitoTurnoDTO> ObterDefeitos(DateTime DatInicio, DateTime DatFim) {

			return daoDefeitos.ObterDefeitos(DatInicio, DatFim);
		}

		public static viw_CBDefeitoTurnoDTO ObterRegistroDefeito(string numECB, string codDefeito, int codLinha, int numPosto, DateTime datEvento)
		{
			return daoDefeitos.ObterRegistroDefeito(numECB, codDefeito, codLinha, numPosto, datEvento);
		}

		public static List<viw_CBDefeitoTurnoDTO> ObterRegistroDefeitoCompleto(string numECB, string codDefeito, int codLinha, int numPosto, DateTime datEvento)
		{
			var dado = ObterRegistroDefeito(numECB, codDefeito, codLinha, numPosto, datEvento);

			var filhos = daoDefeitos.ObterDefeitosSerie(dado.NumSeriePai);

			var resultado = new List<viw_CBDefeitoTurnoDTO>();

			resultado.Add(dado);

			if (filhos.Any()) { 
				resultado.AddRange(filhos);
			}

			return resultado;

		}

		public static List<viw_CBDefeitoTurnoDTO> ObterDefeitosRel(DateTime datInicio, DateTime datFim, int? codLinha = null, string fabrica = "", string codDRT = "", int flgPlaca = 0, string familia = "")
		{
			var defeitos = daoDefeitos.ObterDefeitosRel(datInicio, datFim, codLinha, fabrica, codDRT, flgPlaca, familia);

			return defeitos;
		}

		public static List<viw_CBDefeitoTurnoDTO> ObterDefeitosRelRef(DateTime datInicio, DateTime datFim, int? codLinha = null, string fabrica = "", string codDRT = "")
		{
			var defeitos = daoDefeitos.ObterDefeitosRelRef(datInicio, datFim, codLinha, fabrica, codDRT);

			return defeitos;
		}

		private static List<viw_CBDefeitoTurnoDTO> AtualizarQtde(List<viw_CBDefeitoTurnoDTO> defeitos, DateTime DatInicio, DateTime DatFim, String CodDRT, int? CodLinha, String Fabrica)
		{
			if (DatInicio.Month != DatFim.Month && DatInicio.Year != DatFim.Year) return defeitos;

			var codDefeito = "";
			var qtdDefeito = 0;
			foreach (var defeito in defeitos.OrderBy(x => x.CodDefeito))
			{
				if (codDefeito != defeito.CodDefeito)
				{
					codDefeito = defeito.CodDefeito;

					if (!String.IsNullOrEmpty(CodDRT))
					{
						qtdDefeito = daoDefeitos.ObterQtdeDefeitos(DatInicio, DatFim, defeito.CodDefeito, CodDRT);
					}
					else if (CodLinha != null)
					{
						qtdDefeito = daoDefeitos.ObterQtdeDefeitos(DatInicio, DatFim, defeito.CodDefeito, (int)CodLinha);
					}
					else
					{
						qtdDefeito = daoDefeitos.ObterQtdeDefeitosFabrica(DatInicio, DatFim, defeito.CodDefeito, Fabrica);
					}
				}

				defeito.QtdMes = qtdDefeito;
			}

			return defeitos;

		}

		public static Boolean VerificaDefeitoAtivo(string CodDefeito)
		{
			return daoDefeitos.VerificaDefeitoAtivo(CodDefeito);
		}

		#region Depreciado
		public static List<viw_CBDefeitoTurnoDTO> ObterDefeitosLinhaPlaca(DateTime DatIni, DateTime DatFim, int CodLinha)
		{
			var defeitos = daoDefeitos.ObterDefeitosRel(DatIni, DatFim, CodLinha, flgPlaca: 1);

			DateTime inicio = DatIni.AddDays(-DatIni.Day + 1);
			DateTime fim = inicio.AddMonths(1).AddDays(-1);

			defeitos = AtualizarQtde(defeitos, inicio, fim, "", CodLinha, "");

			return defeitos;
		}

		public static List<viw_CBDefeitoTurnoDTO> ObterDefeitosFabricaPlaca(DateTime datIni, DateTime datFim, string fabrica)
		{
			var defeitos = daoDefeitos.ObterDefeitosRel(datIni, datFim, fabrica: fabrica, flgPlaca: 1);

			DateTime inicio = datIni.AddDays(-datIni.Day + 1);
			DateTime fim = inicio.AddMonths(1).AddDays(-1);

			defeitos = AtualizarQtde(defeitos, inicio, fim, "", null, fabrica);

			return defeitos;
		}

		private static List<viw_CBDefeitoTurnoDTO> AtualizarDescricoes(List<viw_CBDefeitoTurnoDTO> defeitos, DateTime DatInicio, DateTime DatFim, String CodDRT, int? CodLinha, String Fabrica)
		{
			string codDefeito = ""; string desDefeito = "";
			string codModelo = ""; string desModelo = "";
			int codLinha = 0; string desLinha = "";

			foreach (var defeito in defeitos.OrderBy(x => x.CodDefeito))
			{
				if (codDefeito != defeito.CodDefeito.Trim())
				{
					desDefeito = daoDefeitos.ObterDesDefeito(defeito.CodDefeito);
					codDefeito = defeito.CodDefeito;
				}

				if (!String.IsNullOrEmpty(defeito.CodCausa))
				{
					defeito.DesCausa = daoDefeitos.ObterDesCausa(defeito.CodCausa);
				}

				if (!String.IsNullOrEmpty(defeito.CodOrigem))
				{
					defeito.DesOrigem = daoDefeitos.ObterDesOrigem(defeito.CodOrigem.Trim().PadLeft(2, '0'));
				}

				if (codModelo != defeito.NumECB.Substring(0, 6))
				{
					desModelo = daoGeral.ObterDescResumidaItem(defeito.NumECB.Substring(0, 6));
					codModelo = defeito.NumECB.Substring(0, 6);
				}

				if (codLinha != defeito.CodLinha)
				{
					desLinha = daoGeral.ObterLinha(defeito.CodLinha).DesLinha;
					codLinha = defeito.CodLinha;
				}

				defeito.DesDefeito = desDefeito;
				defeito.DesModelo = desModelo;
				defeito.DesLinha = desLinha;
				defeito.DesPosto = daoGeral.ObterPosto(defeito.CodLinha, defeito.NumPosto).DesPosto;

				if (!String.IsNullOrEmpty(CodDRT))
				{
					defeito.QtdMes = daoDefeitos.ObterQtdeDefeitos(DatInicio, DatFim, defeito.CodDefeito, CodDRT);
				}
				else if (CodLinha != null)
				{
					defeito.QtdMes = daoDefeitos.ObterQtdeDefeitos(DatInicio, DatFim, defeito.CodDefeito, (int)CodLinha);
				}
				else
				{
					defeito.QtdMes = daoDefeitos.ObterQtdeDefeitosFabrica(DatInicio, DatFim, defeito.CodDefeito, Fabrica);
				}

			}

			return defeitos;

		}

		public static List<viw_CBDefeitoTurnoDTO> ObterDefeitosTecnico(DateTime DatIni, DateTime DatFim, string CodDRT)
		{
			var defeitos = daoDefeitos.ObterDefeitosRel(DatIni, DatFim, null, "", CodDRT);

			//altera o período para o mês inteiro para consulta das quantidades do mês
			DateTime inicio = DatIni.AddDays(-DatIni.Day + 1);
			DateTime fim = inicio.AddMonths(1).AddDays(-1);

			defeitos = AtualizarQtde(defeitos, inicio, fim, CodDRT, null, "");

			return defeitos;
		}

		public static List<viw_CBDefeitoTurnoDTO> ObterDefeitosLinha(DateTime DatIni, DateTime DatFim, int CodLinha)
		{
			var defeitos = daoDefeitos.ObterDefeitosRel(DatIni, DatFim, CodLinha);

			//altera o período para o mês inteiro para consulta das quantidades do mês
			DateTime inicio = DatIni.AddDays(-DatIni.Day + 1);
			DateTime fim = inicio.AddMonths(1).AddDays(-1);

			defeitos = AtualizarQtde(defeitos, inicio, fim, "", CodLinha, "");

			return defeitos;
		}
		
		public static List<viw_CBDefeitoTurnoDTO> ObterDefeitosFabrica(DateTime DatIni, DateTime DatFim, string Fabrica)
		{
			var defeitos = daoDefeitos.ObterDefeitosRel(DatIni, DatFim, fabrica: Fabrica);

			DateTime inicio = DatIni.AddDays(-DatIni.Day + 1);
			DateTime fim = inicio.AddMonths(1).AddDays(-1);

			defeitos = AtualizarQtde(defeitos, inicio, fim, "", null, Fabrica);

			return defeitos;
		}
		#endregion

		/// <summary>
		/// Método responsável por retornar lista de defeitos cadastrados.
		/// </summary>
		/// <returns></returns>
		public static List<CBCadDefeitoDTO> ObterDefeitosCadastrados()
		{
			return daoDefeitos.ObterDefeitosCadastrados();
		}

		#region Cadastros

		public static List<CBCadDefeitoDTO> ObterListaDefeitos()
		{
			return daoDefeitos.ObterListaDefeitos();
		}

		public static CBCadDefeitoDTO ObterCadDefeito(string codDefeito)
		{
			return daoDefeitos.ObterCadDefeito(codDefeito);
		}

		public static void GravarDefeito(CBCadDefeitoDTO defeito)
		{
			daoDefeitos.GravarDefeito(defeito);

		}

		public static void RemoverDefeito(CBCadDefeitoDTO defeito)
		{
			daoDefeitos.RemoverDefeito(defeito);
		}

		public static string ObterUltimoDefeito()
		{
			return daoDefeitos.ObterUltimoDefeito();
		}

		#endregion

		#region Posto-Defeito

		public static List<CBPostoDefeitoDTO> ObterPostosDefeito(int? codLinha = null, int? numPosto = null)
		{
			return daoDefeitos.ObterPostosDefeito(codLinha, numPosto);
		}

		public static CBPostoDefeitoDTO ObterPostoDefeito(int codLinha, int numPosto, string codDefeito)
		{
			return daoDefeitos.ObterPostoDefeito(codLinha, numPosto, codDefeito);
		}

		public static void GravarPostoDefeito(CBPostoDefeitoDTO postodefeito)
		{
			daoDefeitos.GravarPostoDefeito(postodefeito);
		}

		public static void RemoverPostoDefeito(CBPostoDefeitoDTO postodefeito)
		{
			daoDefeitos.RemoverPostoDefeito(postodefeito);
		}

		public static Boolean VerificaPostoDefeito(int CodLinha, int NumPosto, string CodDefeito)
		{
			return daoDefeitos.VerificaPostoDefeito(CodLinha, NumPosto, CodDefeito);
		}

		#endregion

		#region CNQ

		/// <summary>
		/// Método que acessa a camada de dados para acessar os principais defeitos para o relatório do CNQ
		/// </summary>
		/// <param name="DatInicio"></param>
		/// <param name="DatFim"></param>
		/// <param name="Tipo"></param>
		/// <param name="Fabrica"></param>
		/// <param name="Familia"></param>
		/// <returns></returns>
		public static List<PrincipaisDefeitos> ObterDefeitosCNQ(DateTime DatInicio, DateTime DatFim, string Tipo,
			string Fabrica, string Familia, string Modelo)
		{
			var defeitos = daoDefeitos.ObterDefeitosCNQ(DatInicio, DatFim, Tipo, Fabrica, Familia, Modelo);

			defeitos.ForEach(x => x.QtdProducao = daoRelatorioTecnico.ObterProducaoModelo(DatInicio, DatFim, x.CodModelo));

			return defeitos;
		}

		public static List<ResumoDefeito> ObterResumoCNQ(DateTime DatInicio, DateTime DatFim, string Fabrica)
		{
			return daoDefeitos.ObterResumoCNQ(DatInicio, DatFim, Fabrica);
		}


		public static List<ResumoDefeito> ObterResumoCNQ(DateTime DatInicio, DateTime DatFim, string Fabrica, string Familia)
		{
			return daoDefeitos.ObterResumoCNQ(DatInicio, DatFim, Fabrica, Familia);
		}

		#endregion

		#region Causa
		public static List<CBCadCausaDTO> ObterCausas()
		{
			return daoDefeitos.ObterCausas();
		}

		public static CBCadCausaDTO ObterCausa(string codCausa)
		{
			return daoDefeitos.ObterCausa(codCausa);
		}

		public static string ObterDesCausa(string codCausa)
		{
			return daoDefeitos.ObterDesCausa(codCausa);
		}

		public static void GravarCausa(CBCadCausaDTO causa)
		{
			daoDefeitos.GravarCausa(causa);

		}

		public static void RemoverCausa(CBCadCausaDTO causa)
		{
			daoDefeitos.RemoverCausa(causa);
		}

		public static int ObterUltimaCausa()
		{
			return daoDefeitos.ObterUltimaCausa();
		}
		#endregion

		#region Origem
		public static List<CBCadOrigemDTO> ObterOrigens()
		{
			return daoDefeitos.ObterOrigens();
		}

		public static CBCadOrigemDTO ObterOrigem(string codOrigem)
		{
			return daoDefeitos.ObterOrigem(codOrigem);
		}

		public static string ObterDesOrigem(string codOrigem)
		{
			return daoDefeitos.ObterDesOrigem(codOrigem);
		}

		public static void GravarOrigem(CBCadOrigemDTO origem)
		{
			daoDefeitos.GravarOrigem(origem);

		}

		public static void RemoverOrigem(CBCadOrigemDTO origem)
		{
			daoDefeitos.RemoverOrigem(origem);
		}

		public static int ObterUltimaOrigem()
		{
			return daoDefeitos.ObterUltimaOrigem();
		}
		#endregion

		#region SubDefeito

		public static List<CBCadSubDefeitoDTO> ObterSubDefeitos()
		{
			return daoDefeitos.ObterSubDefeitos();
		}

		public static Boolean GravarSubDefeito(CBCadSubDefeitoDTO defeito)
		{
			return daoDefeitos.GravarSubDefeito(defeito);
		}

		public static int ObterIndiceSubDefeito(string CodDefeito)
		{
			return daoDefeitos.ObterIndiceSubDefeito(CodDefeito);
		}

		public static CBCadSubDefeitoDTO ObterCadSubDefeito(string codDefeito, string CodSubDefeito)
		{
			return daoDefeitos.ObterCadSubDefeito(codDefeito, CodSubDefeito);
		}

		public static List<CBCadSubDefeitoDTO> ObterSubDefeitos(string CodDefeito)
		{
			return daoDefeitos.ObterSubDefeitos(CodDefeito);
		}

		public static int? ObterSubDefeitoSelecionado(string NumECB)
		{
			return ObterSubDefeitoSelecionado(NumECB);
		}

		#endregion
	}
}
