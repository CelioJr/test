namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_REGRAS_TRIGGERS
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(100)]
        public string TAB_NOME { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TAB_EVENTO { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int REG_COD { get; set; }

        public string TAB_PARAMS { get; set; }

        public string TAB_SCRIPT { get; set; }

        public DateTime? TAB_COMPILACAO { get; set; }
    }
}
