using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class LOT_VIEW_DTO
    {
        public Nullable<long> ID { get; set; }
        public int ID_LIST { get; set; }
        public Nullable<System.DateTime> dtPallet { get; set; }
        public string PACKING { get; set; }
        public Nullable<int> LINE { get; set; }
        public string ID_LOTE { get; set; }
        public string ORDER_ID { get; set; }
        public Nullable<int> RESP_PALLET { get; set; }
        public string order_idorder { get; set; }
        public string fullSku { get; set; }
    }
}
