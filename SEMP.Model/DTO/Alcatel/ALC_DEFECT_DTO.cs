using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_DEFECT_DTO
    {
        public ALC_DEFECT_DTO()
        {
            this.ALC_OQC_INSPECTION = new List<ALC_OQC_INSPECTION_DTO>();
            this.ALC_RMB_REGISTER = new List<ALC_RMB_REGISTER_DTO>();
            this.ALC_REPAIR = new List<ALC_REPAIR_DTO>();
        }

        public string COD_DEFECT { get; set; }
        public string DEFECT { get; set; }
        public virtual ICollection<ALC_OQC_INSPECTION_DTO> ALC_OQC_INSPECTION { get; set; }
        public virtual ICollection<ALC_RMB_REGISTER_DTO> ALC_RMB_REGISTER { get; set; }
        public virtual ICollection<ALC_REPAIR_DTO> ALC_REPAIR { get; set; }
    }
}
