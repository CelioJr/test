﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.Rastreabilidade;
using Highsoft.Web.Mvc.Charts;

namespace SEMP.UIColeta.Controllers
{
	[Authorize]
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			var linhasIAC = Geral.ObterLinhaPorFabrica("IAC");
			var linhasIMC = Geral.ObterLinhaPorFabrica("IMC");
			var linhasFEC = Geral.ObterLinhaPorFabrica("FEC");
			var Velocidades = Geral.ObterVelocidades() ?? new List<Model.VO.Rastreabilidade.VelocidadeLinha>() { new Model.VO.Rastreabilidade.VelocidadeLinha() { CodLinha = 1, QtdProduzido = 0, Velocidade = 0 } };

			DateTime dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			DateTime dFim = dIni.AddDays(1).AddMinutes(-1);

			var resumoLinhas = Geral.ObterResumoEmbaladoLinha(dIni, dFim);


			ViewBag.linhasIAC = linhasIAC != null ? linhasIAC.Where(x => x.FlgAtivo == "1").ToList() : new List<Model.DTO.CBLinhaDTO>();
			ViewBag.linhasIMC = linhasIMC != null ? linhasIMC.Where(x => x.FlgAtivo == "1").ToList() : new List<Model.DTO.CBLinhaDTO>();
			ViewBag.linhasFEC = linhasFEC != null ? linhasFEC.Where(x => x.FlgAtivo == "1").ToList() : new List<Model.DTO.CBLinhaDTO>();
			ViewBag.resumoLinhas = resumoLinhas;
			ViewBag.velocidades = Velocidades;

			return View();
		}

		[HttpPost]
		public ActionResult Index(int CodLinha, int NumPosto){

			Session["CodLinha"] = CodLinha;
			Session["NumPosto"] = NumPosto;

			return RedirectToAction("Index", "Posto");

		}

		public ActionResult Tasks(){
			return View();
		}


		public JsonResult ObterVelocidades() {

			var dados = Geral.ObterVelocidades();
			return Json(dados, JsonRequestBehavior.AllowGet);
		
		}
	}
}