using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GPANotaDTO
    {
        public string CodSerie { get; set; }
        public string NumNota { get; set; }
        public string CodModelo { get; set; }
        public Nullable<System.DateTime> DatEntrada { get; set; }
        public string CodLocal { get; set; }
        public Nullable<decimal> QtdNota { get; set; }
        public Nullable<decimal> QtdRecebido { get; set; }
        public string NomUsuario { get; set; }
    }
}
