using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class tbl_CNQMaoObra
    {
        public string CodMaoObra { get; set; }
        public string DesMaoObraP { get; set; }
        public string DesMaoObraE { get; set; }
    }
}
