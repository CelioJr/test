﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
	public class ProducaoDataHora
	{
		[Key]
		[Column(Order = 1)] 
		public int CodLinha { get; set; }
		public String DesLinha { get; set; }
		[Key]
		[Column(Order = 2)] 
		public int NumPosto { get; set; }
		public String DesPosto { get; set; }
		public int SeqPosto { get; set; }
		[Key]
		[Column(Order = 3)] 
		public DateTime Data { get; set; }
		[Key]
		[Column(Order = 4)] 
		public int Hora { get; set; }
		public double QtdMeta { get; set; }
		public int QtdProduzido { get; set; }
		public int QtdDefeitos { get; set; }
		public int QtdAcumulado { get; set; }
		public int QtdProduzidoAcum { get; set; }
		public double QtdMetaAcum { get; set; }

		public double IndDefeito
		{
			get
			{
				if (QtdProduzido > 0 && QtdDefeitos > 0)
				{
					var indice = Math.Round(((double)QtdDefeitos / QtdProduzido) * 100.0, 2);
					return indice;
				}
				else
				{
					return 0;
				}
			}
		}

		public double IndDefeitoAcum
		{
			get
			{
				if (QtdProduzidoAcum > 0 && QtdAcumulado > 0)
				{
					var indice = Math.Round(((double)QtdAcumulado / QtdProduzidoAcum) * 100.0, 2);
					return indice;
				}
				else
				{
					return 0;
				}
			}
		}

		public System.Drawing.Color Cor { get; set; }
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }

		public decimal? AparelhosMinuto { get; set; }

	}
}
