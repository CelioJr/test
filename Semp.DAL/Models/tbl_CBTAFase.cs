using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBTAFase
    {
        public string CodModelo { get; set; }
        public System.DateTime DatReferencia { get; set; }
        public string NumSerieModelo { get; set; }
        public string NumSeriePCI { get; set; }
        public string Fase { get; set; }
        public int Ordem { get; set; }
        public Nullable<int> TempoAtravessa { get; set; }
        public Nullable<int> TempoCorrido { get; set; }
        public Nullable<int> QtdDefeitos { get; set; }
        public Nullable<int> TempoTecnico { get; set; }
    }
}
