﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.PM;
using SEMP.Model.VO.PM;

namespace SEMP.Areas.Outros.Controllers
{
	public class ProducaoDiariaController : Controller
	{
		//
		// GET: /Outros/ProducaoDiaria/
		public ActionResult Index()
		{
			return View();
		}

		//
		// GET: /Outros/ProducaoDiaria/Details/5
		public ActionResult Lista(string datInicio, string fase)
		{
			var programa = new List<ProgramaDiario>();

			try
			{
				var data = DateTime.Parse(datInicio);
				if (fase.Equals("F"))
				{
					programa = ProducaoDiaria.ObterProgramaDiario(data);
					programa.AddRange(ProducaoDiaria.ObterProgramaDiarioMobilidadePA(data));
					programa.AddRange(ProducaoDiaria.ObterProgramaDiarioDATTeste(data));

				}
				else
				{
					programa = ProducaoDiaria.ObterProgramaDiarioPlaca(data, fase);
					programa.AddRange(ProducaoDiaria.ObterProgramaDiarioDATTeste(data));
				}
		}
			catch (Exception ex)
			{
				ViewBag.mensagem = $"Erro: {ex.Message}";
				if (programa == null || !programa.Any())
				{
					programa = new List<ProgramaDiario>();
				}
			}


			return View(programa);
		}


	}
}
