namespace SEMP.Model.DTO
{
    public class CBPassagem_HistDTO
    {
        public string NumECB { get; set; }
        public int CodLinha { get; set; }
        public int NumPosto { get; set; }
        public System.DateTime DatEvento { get; set; }
        public string CodDRT { get; set; }

        //outras propriedades
        public string NomeOperador { get; set; }
        public string DescricaoLinha { get; set; }
        public string DescricaoPosto { get; set; }
    }
}
