namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class te_lei
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public te_lei()
        {
            te_tarifas = new HashSet<te_tarifas>();
        }

        [Key]
        public long id_lei { get; set; }

        [StringLength(60)]
        public string cd_lei { get; set; }

        [StringLength(40)]
        public string ds_lei { get; set; }

        public DateTime? dt_ivigencia { get; set; }

        public DateTime? dt_fvigencia { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<te_tarifas> te_tarifas { get; set; }
    }
}
