using System;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
    public class CBLoteDTO
    {
        
        public string NumLote { get; set; }
        public Nullable<int> Qtde { get; set; }
        
        [Display(Name = "Data de Abertura")]
        public Nullable<System.DateTime> DatAbertura { get; set; }
        
        [Display(Name = "Data de Fechamento")]
        public Nullable<System.DateTime> DatFechamento { get; set; }

		public Nullable<int> Status { get; set; }
        public Nullable<System.DateTime> DatTransferencia { get; set; }
        public Nullable<System.DateTime> DatRecebimento { get; set; }              
        public string CodDRTFechamento { get; set; }
        
        [Display(Name = "Justificativa do Fechamento")]
        public string JustFechamento { get; set; }
        public string CodDRTRecebimento { get; set; }
        public string CodModelo { get; set; }
        public string CodFab { get; set; }
        public string NumAP { get; set; }
        public Nullable<int> TamLote { get; set; }
		public string NumLotePai { get; set; }
        public int? CodLinha { get; set; }
        public int? NumPosto { get; set; }
		public int? TamAmostras { get; set; }


		public string DatRecebimentoString { get; set; }
		public int QuantidadeCaixaPendente { get; set; }
		public int QuantidadeCaixaLote { get; set; }

		public string Setor { get; set; }
		public string DesModelo { get; set; }
		public string DesLinha { get; set; }

		public int QtdRec { get; set; }
    }
}
