namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_cfgptdetcoleta
    {
        [Key]
        public long id_cfgptdetcoleta { get; set; }

        public long id_cfg { get; set; }

        public long id_pt { get; set; }

        public virtual om_cfg om_cfg { get; set; }

        public virtual om_pt om_pt { get; set; }
    }
}
