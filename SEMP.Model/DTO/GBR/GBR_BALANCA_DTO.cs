using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_BALANCA_DTO
    {
        public int COD_BALANCA { get; set; }
        public Nullable<decimal> PESO { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
        public string IP { get; set; }
        public Nullable<System.DateTime> DATA_LIDA { get; set; }
        public Nullable<bool> F_PROCESSADO { get; set; }
    }
}
