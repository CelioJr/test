namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_jobdetlog
    {
        [Key]
        public long id_jobdetlog { get; set; }

        public DateTime? dthr_iexecucao { get; set; }

        public DateTime? dthr_fexecucao { get; set; }

        public short? st_execucao { get; set; }

        [StringLength(100)]
        public string ds_execucao { get; set; }

        public long id_joblog { get; set; }

        public long id_jobdet { get; set; }

        [StringLength(255)]
        public string url_origem { get; set; }

        public virtual om_jobdet om_jobdet { get; set; }

        public virtual om_joblog om_joblog { get; set; }
    }
}
