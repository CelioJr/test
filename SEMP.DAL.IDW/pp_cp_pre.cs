namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_cp_pre
    {
        [Key]
        public long id_cppre { get; set; }

        public long id_cpPredecessora { get; set; }

        public long id_cp { get; set; }

        public virtual pp_cp pp_cp { get; set; }

        public virtual pp_cp pp_cp1 { get; set; }
    }
}
