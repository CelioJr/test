namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class v_aux_temp
    {
        [StringLength(30)]
        public string cd_pt { get; set; }

        [StringLength(30)]
        public string cd_produto { get; set; }

        [StringLength(60)]
        public string cd_componente { get; set; }

        [Key]
        [Column(Order = 0)]
        public decimal qt_auto_perdamp { get; set; }

        public int? qt_erromontagem { get; set; }

        [Key]
        [Column(Order = 1)]
        public DateTime dthr_perdamp { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id_evt { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id_rap { get; set; }

        [StringLength(30)]
        public string cd_rap { get; set; }

        [Key]
        [Column(Order = 4)]
        public decimal total { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id_consolpemp { get; set; }

        [Key]
        [Column(Order = 6)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id_consolpemplog { get; set; }

        [StringLength(20)]
        public string cd_feeder { get; set; }
    }
}
