using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GpaLocalAreaDTO
    {
        public string CodLocal { get; set; }
        public string CodArea { get; set; }
    }
}
