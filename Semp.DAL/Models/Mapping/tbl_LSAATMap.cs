using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_LSAATMap : EntityTypeConfiguration<tbl_LSAAT>
    {
        public tbl_LSAATMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.NumAT, t.DatReferencia, t.DatEmissao, t.DatAlimentacao, t.CodProcesso, t.CodStatus });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NumAT)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.Sequencia)
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodModelo)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumCGC)
                .IsFixedLength()
                .HasMaxLength(18)
                .IsUnicode(false);

            this.Property(t => t.CodProcesso)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.CodStatus)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.TipoAT)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.Observacao)
                .HasMaxLength(40)
                .IsUnicode(false);

            this.Property(t => t.NumFAT)
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.NomAceite)
                .HasMaxLength(15)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_LSAAT");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.NumAT).HasColumnName("NumAT");
            this.Property(t => t.DatReferencia).HasColumnName("DatReferencia");
            this.Property(t => t.Sequencia).HasColumnName("Sequencia");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.QtdLoteAT).HasColumnName("QtdLoteAT");
            this.Property(t => t.DatEmissao).HasColumnName("DatEmissao");
            this.Property(t => t.DatAlimentacao).HasColumnName("DatAlimentacao");
            this.Property(t => t.NumCGC).HasColumnName("NumCGC");
            this.Property(t => t.CodProcesso).HasColumnName("CodProcesso");
            this.Property(t => t.CodStatus).HasColumnName("CodStatus");
            this.Property(t => t.QtdProduzida).HasColumnName("QtdProduzida");
            this.Property(t => t.TipoAT).HasColumnName("TipoAT");
            this.Property(t => t.Observacao).HasColumnName("Observacao");
            this.Property(t => t.DatInicioProd).HasColumnName("DatInicioProd");
            this.Property(t => t.NumFAT).HasColumnName("NumFAT");
            this.Property(t => t.DatLiberacao).HasColumnName("DatLiberacao");
            this.Property(t => t.DatAceite).HasColumnName("DatAceite");
            this.Property(t => t.DatFechamento).HasColumnName("DatFechamento");
            this.Property(t => t.DatProgInicio).HasColumnName("DatProgInicio");
            this.Property(t => t.DatProgFinal).HasColumnName("DatProgFinal");
            this.Property(t => t.NomAceite).HasColumnName("NomAceite");
        }
    }
}
