using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ProgMesVPEDTO
    {
        public string CodFab { get; set; }
        public System.DateTime DatProducao { get; set; }
        public string CodLinha { get; set; }
        public string CodModelo { get; set; }
        public Nullable<int> QtdProdPm { get; set; }
        public string Usuario { get; set; }
        public System.Guid rowguid { get; set; }
        public System.Guid rowguid8 { get; set; }
    }
}
