namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ms_detectorusr
    {
        [Key]
        public long id_detectorusr { get; set; }

        public long id_detector { get; set; }

        public long id_usr { get; set; }

        public virtual ms_detector ms_detector { get; set; }

        public virtual ms_detector ms_detector1 { get; set; }

        public virtual ms_usr ms_usr { get; set; }

        public virtual ms_usr ms_usr1 { get; set; }
    }
}
