using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class RPEDTO
    {
        public string CodFab { get; set; }
        public string NumRPE { get; set; }
        public string NumRPA { get; set; }
        public Nullable<System.DateTime> DatAprovRPE { get; set; }
        public Nullable<decimal> QtdAprovRPE { get; set; }
        public string FlgProdConf { get; set; }
        public string Usuario { get; set; }
        public string NomRespSetor { get; set; }
        public Nullable<System.DateTime> DatRespSetor { get; set; }
        public string NomRespGQ { get; set; }
        public Nullable<System.DateTime> DatRespGQ { get; set; }
        public string ObsRPE { get; set; }
        public string FlgClasse { get; set; }
        public System.Guid rowguid { get; set; }
    }
}
