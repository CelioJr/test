using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_PPB_DTO
    {
        public ALC_PPB_DTO()
        {
            this.ALC_DOWNTIME = new List<ALC_DOWNTIME_DTO>();
            this.ALC_ORDER = new List<ALC_ORDER_DTO>();
            this.ALC_PROD_PLAN = new List<ALC_PROD_PLAN_DTO>();
        }

        public string ppb { get; set; }
        public Nullable<bool> BATTERY { get; set; }
        public Nullable<bool> CHARGER { get; set; }
        public Nullable<bool> MEMORY { get; set; }
        public Nullable<bool> SD_CARD { get; set; }
        public virtual ICollection<ALC_DOWNTIME_DTO> ALC_DOWNTIME { get; set; }
        public virtual ICollection<ALC_ORDER_DTO> ALC_ORDER { get; set; }
        public virtual ICollection<ALC_PROD_PLAN_DTO> ALC_PROD_PLAN { get; set; }
    }
}
