﻿using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_Retrabalho
    {

        public string NS { get; set; }
        public Nullable<int> CodLote { get; set; }
        public Nullable<System.DateTime> DatLeitura { get; set; }

    }

}
