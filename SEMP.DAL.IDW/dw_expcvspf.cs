namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_expcvspf
    {
        [Key]
        public long id_expcfvpf { get; set; }

        public long? id_expcvs { get; set; }

        public long? id_progrp { get; set; }

        public virtual dw_expcvs dw_expcvs { get; set; }

        public virtual om_progrp om_progrp { get; set; }
    }
}
