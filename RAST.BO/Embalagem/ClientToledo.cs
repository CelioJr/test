﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;

namespace RAST.BO.Embalagem
{
    public class ClientBalanca
    {
        public static string RecuperarPeso (string IP_Servidor)
        {
            // Data buffer for incoming data.
            byte[] bytes = new byte[1024];
            string strPeso = string.Empty;

            // Connect to a remote device.
            try {
                // Establish the remote endpoint for the socket.
                // This example uses port 11000 on the local computer.
#pragma warning disable CS0618 // O tipo ou membro é obsoleto
                IPHostEntry ipHostInfo = Dns.Resolve(IP_Servidor);
#pragma warning restore CS0618 // O tipo ou membro é obsoleto
                IPAddress ipAddress = ipHostInfo.AddressList[0];
                IPEndPoint remoteEP = new IPEndPoint(ipAddress,8888);

                // Create a TCP/IP  socket.
                Socket sender = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp );

                // Connect the socket to the remote endpoint. Catch any errors.
                try {
                    sender.Connect(remoteEP);

                    //Console.WriteLine("Socket connected to {0}", sender.RemoteEndPoint.ToString());

                    // Encode the data string into a byte array.
                    byte[] msg = Encoding.ASCII.GetBytes("Peso<EOF>");

                    // Send the data through the socket.
                    int bytesSent = sender.Send(msg);

                    // Receive the response from the remote device.
                    int bytesRec = sender.Receive(bytes);
                    //Console.WriteLine("Echoed test = {0}", Encoding.ASCII.GetString(bytes,0,bytesRec));
                    strPeso = Encoding.ASCII.GetString(bytes, 0, bytesRec);

                    // Release the socket.
                    sender.Shutdown(SocketShutdown.Both);
                    sender.Close();
                
                } catch (ArgumentNullException ane) {
                    throw new Exception(ane.Message); // Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
                } catch (SocketException se) {
                    throw new Exception(se.Message); //Console.WriteLine("SocketException : {0}",se.ToString());
                } catch (Exception e) {
                    throw new Exception(e.Message);  //Console.WriteLine("Unexpected exception : {0}", e.ToString());
                }

            } catch (Exception e) {
                //Console.WriteLine( e.ToString());
                throw new Exception(e.Message);
            }

            return strPeso;
        }
    }
}