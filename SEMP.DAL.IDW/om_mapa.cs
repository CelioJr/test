namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_mapa
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_mapa()
        {
            om_alim = new HashSet<om_alim>();
            om_mapapa = new HashSet<om_mapapa>();
        }

        [Key]
        public long id_mapa { get; set; }

        [StringLength(30)]
        public string cd_mapa { get; set; }

        public int? revisao { get; set; }

        [StringLength(256)]
        public string ds_mapa { get; set; }

        public DateTime? dt_revisao { get; set; }

        public DateTime? dt_stativo { get; set; }

        public long id_produto { get; set; }

        public short? st_ativo { get; set; }

        public long id_pt { get; set; }

        public long? id_prg { get; set; }

        public long id_usrstativo { get; set; }

        public long id_usrrevisao { get; set; }

        public long? id_folha { get; set; }

        public virtual dw_folha dw_folha { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_alim> om_alim { get; set; }

        public virtual om_prg om_prg { get; set; }

        public virtual om_produto om_produto { get; set; }

        public virtual om_pt om_pt { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_mapapa> om_mapapa { get; set; }
    }
}
