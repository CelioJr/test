namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_MOD_POSTOS
    {
        [Key]
        public int COD_MOD_POSTOS { get; set; }

        public int? COD_POSTOS { get; set; }

        public int? COD_SKU { get; set; }
    }
}
