namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_DOWNTIME")]
    public partial class ALC_DOWNTIME
    {
        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal ID_DOWNTIME { get; set; }

        public DateTime? DT_ENTRY { get; set; }

        [StringLength(255)]
        public string FAILURE { get; set; }

        public int? HOUR_TARGET { get; set; }

        [StringLength(20)]
        public string INTERVAL { get; set; }

        public DateTime? LOST_MINUTES { get; set; }

        public int? PRODUCED { get; set; }

        public int? COD_LINE { get; set; }

        [StringLength(5)]
        public string PPB { get; set; }

        public int? COD_SKU { get; set; }

        public int? DEPARTMENT { get; set; }

        public int? MODEL { get; set; }

        [StringLength(25)]
        public string SHIFT { get; set; }

        public DateTime? DT_CREATED { get; set; }

        [StringLength(5)]
        public string ID_FAILURE { get; set; }

        [StringLength(255)]
        public string OBSERVATION { get; set; }

        [StringLength(20)]
        public string ID_OP { get; set; }

        public virtual ALC_DEPARTMENT ALC_DEPARTMENT { get; set; }

        public virtual ALC_PPB ALC_PPB { get; set; }

        public virtual ALC_DOWNTIME ALC_DOWNTIME1 { get; set; }

        public virtual ALC_DOWNTIME ALC_DOWNTIME2 { get; set; }

        public virtual ALC_ORDER ALC_ORDER { get; set; }

        public virtual ALC_FAILURE ALC_FAILURE { get; set; }
    }
}
