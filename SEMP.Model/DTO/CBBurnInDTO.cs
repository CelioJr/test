using System;

namespace SEMP.Model.DTO
{
    public class CBBurnInDTO
    {
        public string NumECB { get; set; }
        public Nullable<int> CodLinha { get; set; }
        public Nullable<int> NumPosto { get; set; }
        public System.DateTime DatEntrada { get; set; }
        public Nullable<System.DateTime> DatDefeito { get; set; }
        public Nullable<System.DateTime> DatSaida { get; set; }
        public string CodDRT { get; set; }
        public string CodCarro { get; set; }
        public string CodItem { get; set; }
        public Nullable<System.DateTime> DatBurnIn { get; set; }
        public Nullable<System.DateTime> DatEncerrado { get; set; }
    }
}
