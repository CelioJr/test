using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_SHIFT_DTO
    {
        public int ID_SHIFT { get; set; }
        public string DESCRIPTION { get; set; }
    }
}
