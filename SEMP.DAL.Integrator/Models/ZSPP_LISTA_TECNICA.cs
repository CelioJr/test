namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ZSPP_LISTA_TECNICA
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(16)]
        public string PACKNO { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        public string EXTROW { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(12)]
        public string AUFNR { get; set; }

        [StringLength(4)]
        public string WERKS { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(40)]
        public string MATNR { get; set; }

        public decimal? BDMNG { get; set; }

        [StringLength(3)]
        public string MEINS { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(10)]
        public string SORTF { get; set; }

        [StringLength(9)]
        public string MATKL { get; set; }

        public virtual ZSPP_ORDEM_PRODUCAO ZSPP_ORDEM_PRODUCAO { get; set; }
    }
}
