using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CnqOrigemMap : EntityTypeConfiguration<tbl_CnqOrigem>
    {
        public tbl_CnqOrigemMap()
        {
            // Primary Key
            this.HasKey(t => t.CodOrigem);

            // Properties
            this.Property(t => t.CodOrigem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.DesOrigem)
                .HasMaxLength(30)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_CnqOrigem");
            this.Property(t => t.CodOrigem).HasColumnName("CodOrigem");
            this.Property(t => t.DesOrigem).HasColumnName("DesOrigem");
        }
    }
}
