using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_TempoPadrao
    {
        public string CodModelo { get; set; }
        public Nullable<decimal> TempoPadrao { get; set; }
        public Nullable<decimal> TempoLp { get; set; }
        public string CodPai { get; set; }
        public string Usuario { get; set; }
        public System.Guid rowguid { get; set; }
    }
}
