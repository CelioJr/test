﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.Models
{
    public class tbl_CBPesoEmbalagem
    {
        public string Modelo { get; set; }
        public string PesoMin { get; set; }
        public string PesoMax { get; set; }

    }
}
