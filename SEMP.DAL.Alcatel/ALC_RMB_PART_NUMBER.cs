namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_RMB_PART_NUMBER")]
    public partial class ALC_RMB_PART_NUMBER
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ALC_RMB_PART_NUMBER()
        {
            ALC_RMB_COST = new HashSet<ALC_RMB_COST>();
            ALC_RMB_REGISTER = new HashSet<ALC_RMB_REGISTER>();
        }

        [Key]
        [StringLength(50)]
        public string SAP_CODE { get; set; }

        [StringLength(255)]
        public string DESCRIPTION { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_RMB_COST> ALC_RMB_COST { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_RMB_REGISTER> ALC_RMB_REGISTER { get; set; }
    }
}
