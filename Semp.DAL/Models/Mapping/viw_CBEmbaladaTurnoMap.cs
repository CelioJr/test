using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class viw_CBEmbaladaTurnoMap : EntityTypeConfiguration<viw_CBEmbaladaTurno>
    {
        public viw_CBEmbaladaTurnoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.NumECB, t.DatLeitura, t.CodLinha });

            // Properties
            this.Property(t => t.NumECB)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumLote)
                .HasMaxLength(18)
                .IsUnicode(false);

            this.Property(t => t.CodModelo)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodLinhaFec)
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.NumAP)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodOperador)
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.CodTestador)
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.CodFab)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NumCC)
                .HasMaxLength(18)
                .IsUnicode(false);

            this.Property(t => t.CodLinhaT1)
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("viw_CBEmbaladaTurno");
            this.Property(t => t.NumECB).HasColumnName("NumECB");
            this.Property(t => t.DatLeitura).HasColumnName("DatLeitura");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.NumLote).HasColumnName("NumLote");
            this.Property(t => t.DatReferencia).HasColumnName("DatReferencia");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.CodLinhaFec).HasColumnName("CodLinhaFec");
            this.Property(t => t.NumAP).HasColumnName("NumAP");
            this.Property(t => t.CodOperador).HasColumnName("CodOperador");
            this.Property(t => t.CodTestador).HasColumnName("CodTestador");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.NumPosto).HasColumnName("NumPosto");
            this.Property(t => t.NumCC).HasColumnName("NumCC");
            this.Property(t => t.Turno).HasColumnName("Turno");
            this.Property(t => t.CodLinhaT1).HasColumnName("CodLinhaT1");
        }
    }
}
