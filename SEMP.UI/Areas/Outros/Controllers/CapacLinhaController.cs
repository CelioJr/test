﻿using Microsoft.Ajax.Utilities;
using RAST.BO.PM;
using RAST.BO.Rastreabilidade;
using SEMP.Model.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEMP.Areas.Outros.Controllers
{
	public class CapacLinhaController : Controller
	{


		//
		// GET: /Outros/CapacLinha/
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult Lista(string dataProd, string codLinha, string codModelo)
		{
			var data = DateTime.Now.AddDays(-DateTime.Now.Day).AddDays(1).Date;

			if (!string.IsNullOrWhiteSpace(dataProd))
			{
				data = DateTime.Parse(dataProd);
			}
			else
			{
				data = DateTime.Parse(data.Date.ToShortDateString());
			}

			var capacs = CapacLinha.ObterCapacidades(data, codLinha, codModelo);

			foreach (var item in capacs)
			{
				var cadModelo = ModeloBO.ObterModelo(item.CodModelo);
				if (cadModelo != null){
					item.PostoEntrada = cadModelo.PostoEntrada;
				}
			}

			return View(capacs);
		}

		//
		// GET: /Outros/CapacLinha/Create
		public ActionResult Create()
		{
			return View();
		}

		//
		// POST: /Outros/CapacLinha/Create
		[HttpPost]
		public ActionResult Edit(CapacLinhaDTO capac, string PostoEntrada)
		{
			try
			{
				if (!CapacLinha.VerificaSeLinhaExiste(capac.CodLinha))
				{
					var json = new
					{
						message = "Código de linha inválido.",
						retorno = false
					};
					return Json(json, JsonRequestBehavior.AllowGet);
				}
				else if (!CapacLinha.VerificaSeModeloExiste(capac.CodModelo))
				{
					var json = new
					{
						message = "Código de modelo inválido.",
						retorno = false
					};
					return Json(json, JsonRequestBehavior.AllowGet);
				}
				else
				{
					CapacLinha.Gravar(capac);

					if (!String.IsNullOrEmpty(PostoEntrada))
					{
						var cadModelo = ModeloBO.ObterModelo(capac.CodModelo);
						if (cadModelo != null)
						{
							cadModelo.PostoEntrada = Convert.ToBoolean(PostoEntrada);
						}
						else
						{
							cadModelo = new CBModeloDTO()
							{
								CodModelo = capac.CodModelo,
								PostoEntrada = Convert.ToBoolean(PostoEntrada)
							};
						}
						ModeloBO.AtualizaModelo(cadModelo);
					}

					var json = new
					{
						message = "Capacidade inserida com sucesso.",
						retorno = true
					};
					return Json(json, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception ex)
			{

				var mensagem = ex.InnerException != null ? ex.InnerException.InnerException != null ? ex.InnerException.InnerException.Message : ex.InnerException.Message : ex.Message;

				var json = new
				{
					message = "Não foi possível gravar. (" + mensagem + ")",
					retorno = false
				};
				return Json(json, JsonRequestBehavior.AllowGet);
			}
		}

		//
		// GET: /Outros/CapacLinha/Edit/5
		public ActionResult Edit(string dataProd, string codLinha, string codModelo)
		{
			DateTime data = DateTime.Now.Date;

			if (!string.IsNullOrWhiteSpace(dataProd))
			{
				data = DateTime.Parse(dataProd);
			}
			if (!String.IsNullOrEmpty(codLinha))
			{
				var origem = CapacLinha.ObterCapacidade(data, codLinha, codModelo);

				var cadModelo = ModeloBO.ObterModelo(codModelo);
				if (cadModelo != null){
					ViewBag.postoEntrada = cadModelo.PostoEntrada;
				}

				return View(origem);
			}

			return View();
		}

		//
		// POST: /Coleta/Origem/Edit/5
		[HttpPost]
		public ActionResult EditOld(CapacLinhaDTO capac)
		{
			try
			{
				CapacLinha.Gravar(capac);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}

		//
		// GET: /Outros/CapacLinha/Delete/5
		public ActionResult Delete(string dataProd, string codLinha, string codModelo)
		{
			DateTime data = DateTime.Now.Date;

			if (!string.IsNullOrWhiteSpace(dataProd))
			{
				data = DateTime.Parse(dataProd);
			}

			var origem = CapacLinha.ObterCapacidade(data, codLinha, codModelo);

			return View(origem);
		}

		//
		// POST: /Outros/CapacLinha/Delete/5
		[HttpPost]
		public ActionResult Delete(CapacLinhaDTO capac)
		{
			try
			{
				CapacLinha.Remover(capac);

				return RedirectToAction("Index");
			}
			catch
			{
				return RedirectToAction("Index");
			}
		}

		/// <summary>
		/// Rotina para clonar um mês para o mês atual
		/// </summary>
		/// <param name="mesRef"></param>
		/// <returns></returns>
		public string ClonarMes(string mesRef, string mesDest)
		{

			var data = DateTime.Parse(mesRef);
			var dataDest = DateTime.Parse(mesDest);

			var clonar = CapacLinha.ClonarMes(data, dataDest);

			return clonar.ToString();
		}

	}
}
