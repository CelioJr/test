using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_VERSAO_DTO
    {
        public int VER_CODIGO { get; set; }
        public string VER_TIPO { get; set; }
        public string SIS_CODIGO { get; set; }
        public Nullable<int> OBJ_CODIGO { get; set; }
        public string VER_DESCRICAO { get; set; }
        public byte[] VER_CONTEUDO { get; set; }
        public System.DateTime VER_DATA_HORA { get; set; }
        public string VER_VERSAO { get; set; }
        public int USR_CODIGO { get; set; }
    }
}
