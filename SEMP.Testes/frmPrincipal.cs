﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SEMP.Testes
{
	public partial class frmPrincipal : Form
	{
		public frmPrincipal()
		{
			InitializeComponent();
		}

		private void btnRFCReadTable_Click(object sender, EventArgs e)
		{
			var f = new frmSAPRFCReadTable();
			f.Show();
			f.Left = this.Left;
			f.Top = this.Top + this.Height;
		}

		private void frmPrincipal_Load(object sender, EventArgs e)
		{
			this.Top = 0;
			this.Left = 0;
			this.Width = Screen.AllScreens[0].WorkingArea.Width;
		}

		private void btnTestesGerais_Click(object sender, EventArgs e)
		{
			var f = new frmGeral();

			f.Show();
		}

		private void btnApontamentoMOB_Click(object sender, EventArgs e)
		{
			var f = new frmApontamentoMOB();
			f.Show();
		}
	}
}
