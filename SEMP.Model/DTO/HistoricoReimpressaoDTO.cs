﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.DTO
{
	public class HistoricoReimpressaoDTO
	{
		public string DesSerie { get; set; }
		public System.DateTime DatImpressao { get; set; }
		public Nullable<int> CodOpe { get; set; }
		public string TipoEtiqueta { get; set; }
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
	}
}
