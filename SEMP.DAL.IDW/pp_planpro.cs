namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_planpro
    {
        [Key]
        public long id_planpro { get; set; }

        public long? pcs_producaoestoque { get; set; }

        public long? pcs_producaobruta { get; set; }

        public long? pcs_producaoliquida { get; set; }

        public long id_plano { get; set; }

        public long? id_produto { get; set; }
    }
}
