namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_prg
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_prg()
        {
            dw_folhaiac = new HashSet<dw_folhaiac>();
            om_mapa = new HashSet<om_mapa>();
            om_prgpos = new HashSet<om_prgpos>();
        }

        [Key]
        public long id_prg { get; set; }

        [StringLength(30)]
        public string cd_prg { get; set; }

        public int? revisao { get; set; }

        [StringLength(256)]
        public string ds_prg { get; set; }

        public DateTime? dt_revisao { get; set; }

        public DateTime? dt_stativo { get; set; }

        public short? st_ativo { get; set; }

        public long id_pt { get; set; }

        public long? id_produto { get; set; }

        public short? is_ConformeEstruturaProduto { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhaiac> dw_folhaiac { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_mapa> om_mapa { get; set; }

        public virtual om_produto om_produto { get; set; }

        public virtual om_pt om_pt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_prgpos> om_prgpos { get; set; }
    }
}
