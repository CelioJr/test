using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
    public partial class MensagemPainelDTO
    {
		[Display(Name = "C�digo")]
        public long CodMensagem { get; set; }
		[Display(Name = "Destino")]
        public string Destino { get; set; }
		[Display(Name = "Mensagem")]
        public string Mensagem { get; set; }
		[Display(Name = "Data")]
        public Nullable<System.DateTime> Data { get; set; }
		[Display(Name = "Status")]
        public string flgAtivo { get; set; }
		[Display(Name = "Usu�rio")]
        public string nomusuario { get; set; }
    }
}
