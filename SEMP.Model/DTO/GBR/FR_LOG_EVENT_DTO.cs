using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_LOG_EVENT_DTO
    {
        public int LOG_ID { get; set; }
        public Nullable<System.DateTime> LOG_DATA { get; set; }
        public string LOG_HORA { get; set; }
        public Nullable<int> LOG_CODFORM { get; set; }
        public string LOG_DESCFORM { get; set; }
        public string LOG_OPERACAO { get; set; }
        public string LOG_USUARIO { get; set; }
        public string LOG_SISTEMA { get; set; }
        public string LOG_CHAVE { get; set; }
        public string LOG_CHAVECONT { get; set; }
        public string LOG_CONTEUDO { get; set; }
        public string LOG_IP { get; set; }
    }
}
