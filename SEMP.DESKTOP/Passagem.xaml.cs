﻿using RAST.BO.Embalagem;
using RAST.BO.Rastreabilidade;
using SEMP.Model;
using SEMP.Model.DTO;
using SEMP.Model.VO;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SISAP
{
    /// <summary>
    /// Interaction logic for Passagem.xaml
    /// </summary>
    public partial class Passagem : Window
    {

        private Thread thread;
        public SerialPort serialPort = new SerialPort();
        RetornoLogin login = null;

        public string Modelo { get; set; }
        public string NumAP { get; set; }
        public string CodFab { get; set; }
        public int CodLinha { get; set; }
        public int NumPosto { get; set; }

        private bool validouSerial = false;
        private string barcode = string.Empty;

        BrushConverter bc = new BrushConverter();
        Brush corCinza = null;
        Posto posto = null;

        RAST.BO.Rastreabilidade.Passagem passagem = null;
        List<viw_CBPerfilAmarraDTO> perfil = null;

        public Passagem(RetornoLogin login)
        {
            InitializeComponent();

            txtLeitura.Focus();
            
            serialPort.DataReceived += new SerialDataReceivedEventHandler(serialPort_DataReceived);

            thread = new Thread(new ThreadStart(AsynchronousSocketListener.StartListening));
            thread.Start();

            this.login = login;

            corCinza = (Brush)bc.ConvertFrom("#FFD3D3D3");
            corCinza.Freeze();

            SetConfiguracaoPosto();
            SetConfigPorta();
        }

        private void btn_Sair(object sender, RoutedEventArgs e)
        {
            serialPort.Close();
            serialPort.Dispose();
            Close();
        }

        private void btnLogout(object sender, RoutedEventArgs e)
        {
            new MainWindow().Show();
            Close();
        }

        private void btn_configurarPosto(object sender, RoutedEventArgs e)
        {
            new Configuracao.ConfigurarPosto(this).ShowDialog();
        }

        private void btn_configurarBalanca(object sender, RoutedEventArgs e)
        {
            serialPort.Close();
            new Configuracao.ConfigurarBalanca(this).ShowDialog();
        }

        private void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                List<string> listaDados = new List<string>();
                try
                {
                    String message;
                    message = serialPort.ReadLine();
                    message = message.Trim().TrimEnd();

                    string t = string.Empty;
                    foreach (byte a in message.ToCharArray())
                    {
                        if (Convert.ToChar(a).Equals(' ') || Char.IsNumber(Convert.ToChar(a)) || Char.IsSeparator(Convert.ToChar(a)) || Char.IsPunctuation(Convert.ToChar(a)))
                            t += Convert.ToChar(a);
                    }

                    string[] valores = t.Split(' ');
                    foreach (var x in valores)
                    {
                        if (!string.IsNullOrEmpty(x.Trim()))
                            listaDados.Add(x);
                    }

                    AsynchronousSocketListener.ValorOficial = listaDados[2];
                    
                    if (validouSerial)
                    {
                        
                        var peso = Embalagem.RecuperarPesoEmbalagem(Modelo);

                        if (peso == null)
                        {
                            canMsg.Dispatcher.Invoke(new Action(() => { canMsg.Background = new SolidColorBrush(Colors.Red); }), null);
                            lbMsg.Dispatcher.Invoke(new Action(() => { lbMsg.Content = "ERRO AO RECUPERAR O PESO DO MODELO"; }), null);    
                        }
                        else
                        {
                            txtPeso.Dispatcher.Invoke(new Action(() => { txtPeso.Text = listaDados[2]; }), null);

                            double pesoLido = double.Parse(listaDados[2]);
                            double pesoMin = double.Parse(peso.PesoMin);
                            double pesoMax = double.Parse(peso.PesoMax);

                            if (pesoLido < pesoMin || pesoLido > pesoMax)
                            {
                                canMsg.Dispatcher.Invoke(new Action(() => { canMsg.Background = new SolidColorBrush(Colors.Red); }), null);
                                lbMsg.Dispatcher.Invoke(new Action(() => { lbMsg.Content = "PESO DO PRODUTO INVÁLIDO."; }), null);

                                validouSerial = false;
                                txtPeso.Dispatcher.Invoke(new Action(() => { txtPeso.Text = ""; }), null);
                                txtLeitura.Dispatcher.Invoke(new Action(() => { txtLeitura.IsEnabled = true; txtLeitura.Text = string.Empty; }), null);    
                            }
                            else
                            {
                                EmbalagemTablet em = new EmbalagemTablet();

                                lbMsg.Dispatcher.Invoke(new Action(() => { lbMsg.Content = "ENVIANDO..."; }), null);

                                var retornoImp = em.ImprimirEtiquetasIndividuais(NumAP, barcode, Modelo.ToString(), Geral.ObterPosto(CodLinha, NumPosto), pesoLido);

                                validouSerial = false;
                                txtPeso.Dispatcher.Invoke(new Action(() => { txtPeso.Text = ""; }), null);
                                txtLeitura.Dispatcher.Invoke(new Action(() => { txtLeitura.IsEnabled = true; txtLeitura.Text = string.Empty; txtLeitura.Focus();  }), null);    

                                if (retornoImp.CodMensagem != "R0001")
                                {
                                    canMsg.Dispatcher.Invoke(new Action(() => { canMsg.Background = new SolidColorBrush(Colors.Red); }), null);
                                    lbMsg.Dispatcher.Invoke(new Action(() => { lbMsg.Content = "ERRO AO IMPRIMIR ETIQUETA."; }), null);                
                                }
                                else
                                {
                                    var retornoGravar = passagem.GravaPassagemProcedure(barcode, login.CodDRT, CodLinha, NumPosto, CodFab, NumAP);

                                    if (retornoGravar.CodMensagem == "R0001")
                                    {
                                        SetarTotalProduzido();
                                        SetarUltimasLeituras();
                                        canMsg.Dispatcher.Invoke(new Action(() => { canMsg.Background = new SolidColorBrush(Colors.Green); }), null);
                                        lbMsg.Dispatcher.Invoke(new Action(() => { lbMsg.Content = "LEITURA OK. LEIA O PRÓXIMO"; }), null);
                                    }
                                    else
                                    {
                                        canMsg.Dispatcher.Invoke(new Action(() => { canMsg.Background = new SolidColorBrush(Colors.Red); }), null);
                                        lbMsg.Dispatcher.Invoke(new Action(() => { lbMsg.Content = retornoGravar.DesMensagem; }), null);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        canMsg.Dispatcher.Invoke(new Action(() => { canMsg.Background = new SolidColorBrush(Colors.Red); }), null);
                        lbMsg.Dispatcher.Invoke(new Action(() => { lbMsg.Content = "LEIA PRIMEIRO UM NUMERO DE SERIE."; }), null);
                    }

                }
                catch (TimeoutException t)
                {
                    MessageBox.Show("Erro "+t.Message);
                }

            }
            catch (System.TimeoutException a)
            {
                MessageBox.Show(a.Message.ToString());
            }
        }

        public void SetConfigPorta()
        {
            try
            {
                string porta = SEMP.DESKTOP.Configuracao.Config.porta;
                string velocidade = SEMP.DESKTOP.Configuracao.Config.velocidade;
                string bitDados = SEMP.DESKTOP.Configuracao.Config.bitDados;
                string paridade = SEMP.DESKTOP.Configuracao.Config.paridade;
                string bitParada = SEMP.DESKTOP.Configuracao.Config.bitParada;
                string fluxo = SEMP.DESKTOP.Configuracao.Config.fluxo;

                if (!string.IsNullOrWhiteSpace(porta))
                {

                    serialPort.PortName = porta;

                    switch (paridade)
                    {
                        case "odd":
                            serialPort.Parity = Parity.Odd;
                            break;
                        case "Even":
                            serialPort.Parity = Parity.Even;
                            break;
                        case "Mark":
                            serialPort.Parity = Parity.Mark;
                            break;
                        case "Space":
                            serialPort.Parity = Parity.Space;
                            break;
                        default:
                            serialPort.Parity = Parity.None;
                            break;
                    }

                    switch (bitParada)
                    {
                        case "1":
                            serialPort.StopBits = StopBits.One;
                            break;
                        case "1.5":
                            serialPort.StopBits = StopBits.OnePointFive;
                            break;
                        case "2":
                            serialPort.StopBits = StopBits.Two;
                            break;
                        default:
                            serialPort.StopBits = StopBits.None;
                            break;
                    }

                    switch (fluxo)
                    {
                        case "XON/XOFF":
                            serialPort.Handshake = Handshake.XOnXOff;
                            break;
                        case "RTS/CTS":
                            serialPort.RtsEnable = true;
                            break;
                        case "DSR/DTR":
                            serialPort.DtrEnable = true;
                            break;
                        default:
                            serialPort.Handshake = Handshake.None;
                            break;
                    }

                    if (velocidade == null)
                    {
                        MessageBox.Show("Velocidade Invalida");
                    }
                    else
                    {
                        serialPort.BaudRate = Int32.Parse(velocidade);
                    }

                    if (bitDados == null)
                    {
                        MessageBox.Show("Bit de Dados Invalido");
                    }
                    else
                    {
                        serialPort.DataBits = Int32.Parse(bitDados);
                    }


                    if (!serialPort.IsOpen)
                    {
                        txtPeso.Clear();
                        serialPort.Open();
                        MessageBox.Show("Porta Conectada", "Balança Toledo");
                    }
                    else
                    {
                        MessageBox.Show("Porta não Conectada", "Balança Toledo");
                    }
                }
                else
                {
                    new Configuracao.ConfigurarBalanca(this).ShowDialog();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"SISAP");
            }
        }

        private void txtLeitura_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && !string.IsNullOrWhiteSpace(txtLeitura.Text))
            {
                Mensagem retorno = null;

                if (passagem != null)
                {
                    if (ValidarMascara())
                    {
                        retorno = passagem.ValidaPassagem(txtLeitura.Text, login.CodDRT, passagem.posto.CodLinha, passagem.posto.NumPosto, NumAP);

                        if (retorno.CodMensagem != "R0001")
                        {
                            if (retorno.CodMensagem == "R0003")
                            {
                                canMsg.Background = new SolidColorBrush(Colors.Yellow);
                            }
                            else
                            {
                                canMsg.Background = new SolidColorBrush(Colors.Red);
                            }
                            txtLeitura.Text = string.Empty;
                            lbMsg.Content = retorno.DesMensagem;
                        }
                        else if (passagem.posto.CodTipoPosto == Constantes.POSTO_AMARRA_NOTEBOOK ||
                                 passagem.posto.CodTipoPosto == Constantes.POSTO_AMARRA_TABLET ||
                                 passagem.posto.CodTipoPosto == Constantes.POSTO_PASSAGEM_TABLET 
                                || passagem.posto.CodTipoPosto == Constantes.POSTO_AMARRA_LOTE_TABLET_IMPRESSAO_ETIQUETA_PESO 
                            )
                        {
                            if (!Amarracao.VerificaSeProdutoExisteInformatica(txtLeitura.Text, NumAP))
                            {
                                validouSerial = false;
                                txtPeso.Dispatcher.Invoke(new Action(() => { txtPeso.Text = ""; }), null);
                                txtLeitura.Dispatcher.Invoke(new Action(() => { txtLeitura.IsEnabled = true; txtLeitura.Text = string.Empty; txtLeitura.Focus(); }), null);    

                                lbMsg.Content = "NÚMERO DE SÉRIE NÃO PERTENCE A AP: " + NumAP + ".";
                            }
                            else if (!Amarracao.ValidarSeProdutoInformaticaPassouSeparacao(txtLeitura.Text))
                            {
                                validouSerial = false;
                                txtPeso.Dispatcher.Invoke(new Action(() => { txtPeso.Text = ""; }), null);
                                txtLeitura.Dispatcher.Invoke(new Action(() => { txtLeitura.IsEnabled = true; txtLeitura.Text = string.Empty; txtLeitura.Focus(); }), null);    

                                lbMsg.Content = "PRODUTO NÃO PASSOU NA SEPARAÇÃO";
                            }
                            else
                            {
                                validouSerial = true;
                                barcode = txtLeitura.Text;
                                canMsg.Background = corCinza;
                                txtLeitura.IsEnabled = false;
                                lbMsg.Content = "AGUARDANDO LEITURA DO PESO.";
                            }

                            
                        }
                        else
                        {
                            validouSerial = true;
                            barcode = txtLeitura.Text;
                            canMsg.Background = corCinza;
                            txtLeitura.IsEnabled = false;
                            lbMsg.Content = "AGUARDANDO LEITURA DO PESO.";
                        }
                    }
                    else
                    {
                        txtLeitura.Text = string.Empty;
                        canMsg.Background = new SolidColorBrush(Colors.Red);
                        lbMsg.Content = "SERIAL INVÁLIDO.";
                    }
                }
                else
                {
                    txtLeitura.Text = string.Empty;
                    canMsg.Background = new SolidColorBrush(Colors.Red);
                    lbMsg.Content = "POSTO NÃO CONFIGURADO CORRETAMENTE.";
                }
            }
        }

        private bool ValidarNumSerieEPeso()
        {
            return true;
        }

        private void SetConfiguracaoPosto()
        {
            //lbEstacao.Content = System.Configuration.ConfigurationSettings.AppSettings["linha"] + " | " + System.Configuration.ConfigurationSettings.AppSettings["posto"];
            //lbAP.Content = System.Configuration.ConfigurationSettings.AppSettings["numAP"];
        }

        public void SetarPosto()
        {
            perfil = RAST.BO.Rastreabilidade.Amarracao.GetPerfilPosto(CodLinha, NumPosto, Modelo, CodFab, NumAP);

            posto = new Posto(CodLinha, NumPosto);
            canMsg.Background = corCinza;
            lbMsg.Content = "AGUARDANDO ENVIO.";
        }

        public void SetarPassagem()
        {
            passagem = new RAST.BO.Rastreabilidade.Passagem(posto.posto);
        }

        public void SetarTotalProduzido()
        {
            var total = Geral.ObterTotalAprovadas(CodLinha, NumPosto);
            lbTotalProduzido.Dispatcher.Invoke(new Action(() => { lbTotalProduzido.Content = string.Format("Total Produzido: {0}", total.ToString()); }), null);
        }

        public void SetarUltimasLeituras()
        {
            var lista = RAST.BO.Rastreabilidade.Amarracao.HistoricoPostoPassagem(CodLinha, NumPosto);

            dgUltimasLeituras.Dispatcher.Invoke(new Action(() => { dgUltimasLeituras.ItemsSource = lista.Take(5).ToList(); }), null);
        }

        public bool ValidarMascara()
        {

            if (!Regex.IsMatch(txtLeitura.Text, perfil[0].Mascara))
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        private void Window_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                validouSerial = false;
                txtPeso.Dispatcher.Invoke(new Action(() => { txtPeso.Text = ""; }), null);
                txtLeitura.Dispatcher.Invoke(new Action(() => { txtLeitura.IsEnabled = true; txtLeitura.Text = string.Empty; txtLeitura.Focus(); }), null);    

                SetarTotalProduzido();
                SetarUltimasLeituras();
                //canMsg.Dispatcher.Invoke(new Action(() => { canMsg.Background = new SolidColorBrush(Colors.Green); }), null);
                lbMsg.Dispatcher.Invoke(new Action(() => { lbMsg.Content = "LEIA UM PRODUTO"; }), null);
            }
        }

    }
}
