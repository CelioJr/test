﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEMP.Areas.Coleta.Controllers
{
	/// <summary>
	/// Classe que controla as Action da funcionalidade Setup
	/// </summary>
	[Authorize]
	public class SetupController : Controller
	{
		/// <summary>
		/// Método que controla a pagina incial do Setup.
		/// </summary>
		/// <returns>Retorna a View Index.cshtml</returns>
		[Authorize]
		public ActionResult Index()
		{
			return View();
		}

		/// <summary>
		/// Método que faz o Setup do Posto.
		/// </summary>
		/// <param name="fabrica">Recebe a fabrica do setup.</param>
		/// <param name="codLinha">Recebe o código da linha do setup.</param>
		/// <param name="numPosto">Recebe o número do posto do setup.</param>
		/// <returns>Retorna a View Index.cshtml</returns>
		[HttpPost]
		public ActionResult SetarPosto(string fabrica, string codLinha, string numPosto)
		{
			try
			{

				HttpCookie cookie = new HttpCookie("Posto");

				cookie.Expires = DateTime.Now.AddYears(1);
				cookie.Values.Add("Fabrica", fabrica);
				cookie.Values.Add("codLinha", codLinha);
				cookie.Values.Add("numPosto", numPosto);

				this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);

				return RedirectToAction("Index", "Home");
			}
			catch
			{
				return RedirectToAction("Index", "Setup");
			}
		}

		/// <summary>
		/// Método que faz o Setup do Posto.
		/// </summary>
		/// <param name="fabrica">Recebe a fabrica do setup.</param>
		/// <param name="codLinha">Recebe o código da linha do setup.</param>
		/// <param name="numPosto">Recebe o número do posto do setup.</param>
		/// <returns>Retorna a View Index.cshtml</returns>
		public ActionResult SetPosto(string fabrica, string codLinha, string numPosto)
		{
			try
			{

				HttpCookie cookie = new HttpCookie("Posto");

				cookie.Expires = DateTime.Now.AddYears(1);
				cookie.Values.Add("Fabrica", fabrica);
				cookie.Values.Add("codLinha", codLinha);
				cookie.Values.Add("numPosto", numPosto);

				this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);

				return RedirectToAction("Index", "Home");
			}
			catch
			{
				return RedirectToAction("Index", "Setup");
			}
		}

		public ActionResult Contingencia(){

			return View();

		}
	}
}
