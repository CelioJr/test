﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RAST.BO.Rastreabilidade;
using SEMP.BO;
using SEMP.DAL.DAO.PM;
using SEMP.DAL.Models;
using SEMP.Model.DTO;
using SEMP.Model.VO.PM;

namespace RAST.BO.PM
{
	public class ParadaDeLinha
	{

		#region Parada de Linha

		public static List<ProdParadaDTO> ObterParadas(DateTime datInicio, DateTime datFim, string linha, string codFab,
			string codModelo, string tipoParada, string status, string codDepto)
		{
			var dados = daoParadaDeLinha.ObterParadas(datInicio, datFim, linha, codFab, codModelo, tipoParada, status, codDepto);

			foreach (var parada in dados)
			{
				var deptos = daoParadaDeLinha.ObterListaParadaDep(parada.CodFab, parada.NumParada);

				parada.Deptos = String.Join("; ", deptos.Select(x => x.CodDepto).ToList());
			}

			return dados;
		}

		public static ProdParadaDTO ObterParada(string codFab, string numParada)
		{
			return daoParadaDeLinha.ObterParada(codFab, numParada);
		}

		public static string GravarParada(ProdParadaDTO parada)
		{
			var numparada = parada.NumParada;

			if (String.IsNullOrEmpty(parada.NumParada))
			{
				numparada = daoParadaDeLinha.ObterProximaParada();
				parada.NumParada = numparada;
			}

			if (!daoParadaDeLinha.GravarParada(parada))
			{
				return "";
			}

			return numparada;
		}

		public static void RemoverParada(string codFab, string numParada)
		{
			daoParadaDeLinha.RemoverParada(codFab, numParada);
		}

		public static List<ResumoHorasParadas> ObterResumoHorasParadas(int codFab, DateTime dataInicio, DateTime dataFim)
		{
			return daoParadaDeLinha.ObterResumoHorasParadas(codFab, dataInicio, dataFim);
		}

		public static List<ResumoDetalhesHorasParadas> ObterDetalhesResumoHorasParadas(int codFab, DateTime dataInicio, DateTime dataFim)
		{
			return daoParadaDeLinha.ObterDetalhesResumoHorasParadas(codFab, dataInicio, dataFim);
		}

		public static List<ProdParadaDTO> ObterResumoMensal(DateTime datInicio, DateTime datFim, string codFab, string codDepto)
		{
			return daoParadaDeLinha.ObterResumoMensal(datInicio, datFim, codFab, codDepto);
		}

		public static string GetDescricaoMes(string mes)
		{
			switch (mes)
			{
				case "01":
					mes = "Janeiro";
					break;
				case "02":
					mes = "Fevereiro";
					break;
				case "03":
					mes = "Março";
					break;
				case "04":
					mes = "Abril";
					break;
				case "05":
					mes = "Maio";
					break;
				case "06":
					mes = "Junho";
					break;
				case "07":
					mes = "Julho";
					break;
				case "08":
					mes = "Agosto";
					break;
				case "09":
					mes = "Setembro";
					break;
				case "10":
					mes = "Outubro";
					break;
				case "11":
					mes = "Novembro";
					break;
				case "12":
					mes = "Dezembro";
					break;
				default:
					break;
			}

			return mes;
		}

		#endregion

		#region Departamento de Paradas
		/// <summary>
		/// Rotina para buscar no banco de dados os registros cadastrados com base no filtro informado
		/// </summary>
		/// <param name="codFab"></param>
		/// <param name="tipoParada"></param>
		/// <returns></returns>
		public static List<ProdParadaDepDTO> ObterListaParadaDep(string codFab, string numParada)
		{
			return daoParadaDeLinha.ObterListaParadaDep(codFab, numParada);
		}

		/// <summary>
		/// Rotina para buscar no banco de dados um determinado registro
		/// </summary>
		/// <param name="codLinha"></param>
		/// <returns></returns>
		public static ProdParadaDepDTO ObterParadaDep(string codFab, string numParada, string codDepto)
		{
			return daoParadaDeLinha.ObterParadaDep(codFab, numParada, codDepto);
		}

		/// <summary>
		/// Rotina de manuntenção de dados, para inclusão e alteração
		/// </summary>
		/// <param name="linha"></param>
		public static void GravarParadaDep(ProdParadaDepDTO parada)
		{
			daoParadaDeLinha.GravarParadaDep(parada);
		}

		/// <summary>
		/// Rotina para excluir um registro do banco de dados
		/// </summary>
		/// <param name="codLinha"></param>
		/// <param name="numTurno"></param>
		public static void RemoverParadaDep(string numParada, string codFab, string codDepto)
		{
			daoParadaDeLinha.RemoverParadaDep(numParada, codFab, codDepto);
		}
		#endregion

		#region Itens de parada por departamento
		/// <summary>
		/// Rotina para buscar no banco de dados um determinado registro
		/// </summary>
		/// <param name="codLinha"></param>
		/// <returns></returns>
		public static ProdParadaDepItemDTO ObterParadaDepItem(string codFab, string numParada, string codDepto)
		{
			return daoParadaDeLinha.ObterParadaDepItem(codFab, numParada, codDepto);
		}

		/// <summary>
		/// Rotina de manuntenção de dados, para inclusão e alteração
		/// </summary>
		/// <param name="linha"></param>
		public static void GravarParadaDepItem(ProdParadaDepItemDTO parada)
		{
			daoParadaDeLinha.GravarParadaDepItem(parada);
		}

		/// <summary>
		/// Rotina para excluir um registro do banco de dados
		/// </summary>
		/// <param name="codLinha"></param>
		/// <param name="numTurno"></param>
		public static void RemoverParadaDepItem(string numParada, string codFab, string codDepto)
		{
			daoParadaDeLinha.RemoverParadaDepItem(numParada, codFab, codDepto);
		}
		#endregion

		#region Motivos de Paradas

		public static List<ParadaDTO> ObterMotivos()
		{
			return daoParadaDeLinha.ObterMotivos();

		}

		public static List<ParadaDTO> ObterMotivos(string codFab, string tipoParada)
		{
			if (String.IsNullOrEmpty(codFab) && String.IsNullOrEmpty(tipoParada))
			{
				return daoParadaDeLinha.ObterMotivos();
			}

			if (!String.IsNullOrEmpty(codFab) && String.IsNullOrEmpty(tipoParada))
			{
				return daoParadaDeLinha.ObterMotivos().Where(x => x.CodFab.Equals(codFab)).ToList();
			}

			if (String.IsNullOrEmpty(codFab) && !String.IsNullOrEmpty(tipoParada))
			{
				return daoParadaDeLinha.ObterMotivos().Where(x => x.TipParada.Equals(tipoParada)).ToList();
			}

			return daoParadaDeLinha.ObterMotivos(codFab, tipoParada);

		}

		public static ParadaDTO ObterMotivo(string codParada, string codFab)
		{
			return daoParadaDeLinha.ObterMotivo(codParada, codFab);
		}

		public static void GravarMotivo(ParadaDTO parada)
		{
			daoParadaDeLinha.GravarMotivo(parada);
		}

		public static void RemoverMotivo(string codParada, string codFab)
		{
			daoParadaDeLinha.RemoverMotivo(codParada, codFab);
		}

		#endregion

		#region Amplitude para Emails
		public static List<ProdAmplitudeDTO> ObterListaAmplitude()
		{
			return daoParadaDeLinha.ObterListaAmplitude();
		}

		public static List<ProdAmplitudeDTO> ObterListaAmplitude(string depto)
		{
			return daoParadaDeLinha.ObterListaAmplitude(depto);
		}

		public static List<ProdAmplitudeDTO> ObterListaAmplitudeParada(string codFab, string numParada)
		{
			return daoParadaDeLinha.ObterListaAmplitudeParada(codFab, numParada);
		}

		public static ProdAmplitudeDTO ObterAmplitude(string codFab, string codDepto, string nomUsuario)
		{
			return daoParadaDeLinha.ObterAmplitude(codFab, codDepto, nomUsuario);
		}

		public static string ObterDesGrupo(string codFab, string codDepto)
		{
			return daoParadaDeLinha.ObterDesGrupo(codFab, codDepto);
		}

		public static void GravarAmplitude(ProdAmplitudeDTO dado)
		{
			daoParadaDeLinha.GravarAmplitude(dado);
		}

		public static void RemoverAmplitude(string codFab, string codDepto, string nomUsuario)
		{
			daoParadaDeLinha.RemoverAmplitude(codFab, codDepto, nomUsuario);
		}
		#endregion


		#region Envio de e-mails

		public static string EnviarEmailDepto(string codFab, string numParada)
		{
			try
			{

				// busca a lista com os usuários para o envio do email
				var emails = ObterListaAmplitudeParada(codFab, numParada);

				// Obtem os destinatários que irão em cópia
				var copia = ObterListaAmplitude("COPIA");
				var copiaS = String.Join("; ", copia.Select(x => x.NomEmail.Trim()));

				// obtem os dados da parada
				var parada = ObterParada(codFab, numParada);
				parada.StatusEmail = "*";

				// obtem os departamentos da parada
				var deptos = ObterListaParadaDep(codFab, numParada);

				// separa os departamentos cujo retorno da parada é obrigatório
				var deptoO = deptos.Where(x => x.TipItem.Equals("O")).Select(y => y.CodDepto).ToList();
				var deptoI = deptos.Where(x => x.TipItem.Equals("I")).Select(y => y.CodDepto).ToList();

				// seleciona os emails dos responsáveis pela parada
				var obrigatorios = String.Join("; ", emails.Where(x => deptoO.Contains(x.CodDepto)).Select(x => x.NomEmail.Trim()));
				var informacao = String.Join("; ", emails.Where(x => deptoI.Contains(x.CodDepto)).Select(x => x.NomEmail.Trim()));

				// define o conteúdo do email
				var mensagem = $"Ocorrência de Parada de Linha de acordo com os parâmetros Abaixo: \n" +
								$"Nº Parada     : {numParada}  -  Data Abertura: {parada.DatParada.ToShortDateString()} \n" +
								$"MODELO        : {parada.CodModelo}  {parada.DesModelo} \n" +
								$"N.E.          : {parada.CodItem}  {parada.DesItem} \n" +
								$"LINHA         : {parada.CodLinha} - {parada.DesLinha} \n" +
								$"SETOR         : {parada.DesDepLinha} \n" +
								$"MOTIVO        : {parada.CodParada} - {parada.DesParada} \n" +
								$"HORARIO DE INICIO DA PARADA : {parada.HorInicio} \n" +
								$"DETALHES DA PARADA: {parada.DesObs} \n" +
								$"--------------------------------------------------------------------\n";

				var msgO = $"{mensagem} RETORNO OBRIGATÓRIO, FAVOR JUSTIFICAR MOTIVO DA PARADA\n";

				// dispara e-mail para os responsáveis
				// obrigatorios
				var emailenviado = Geral.EnviarEmail("paradadelinha@semptcl.com.br", "Parada de Linha", obrigatorios, "Parada de linha", msgO, copiaS, true);

				if (emailenviado != "") return $"E-mail obrigatório não enviado: {emailenviado}";

				GravarParada(parada);

				//atualiza os dados no banco de dados
				foreach (var depto in deptos)
				{
					depto.DatEnvio = DateTime.Parse(Util.GetDate().ToShortDateString());

					if (depto.TipItem.Equals("O"))
					{
						var deptoitem = ObterParadaDepItem(codFab, numParada, depto.CodDepto);

						if (deptoitem == null)
						{
							var item = new ProdParadaDepItemDTO
							{
								CodFab = depto.CodFab,
								CodDepto = depto.CodDepto,
								CodFabDepto = depto.CodFabDepto,
								NumParada = depto.NumParada
							};

							GravarParadaDepItem(item);
						}
					}

					GravarParadaDep(depto);
				}

				//informativos
				if (String.IsNullOrEmpty(copiaS))
				{
					var msgI = $"{mensagem} Email Apenas Informativo, não é necessário responder\n";

					var emailenviado2 = Geral.EnviarEmail("paradadelinha@semptcl.com.br", "Parada de Linha", informacao,
						"Parada de linha", msgI, copiaS, true);

					if (emailenviado2 != "") return $"E-mail informativo não enviado: {emailenviado2}";
				}

			}
			catch (Exception ex)
			{

				Geral.EnviarEmail("paradadelinha@semptcl.com.br", "Erro no envio de e-mail: Parada de Linha", "thiago.raheem@semptcl.com.br", "Parada de linha", ex.Message, "", true);

			}

			return "";

		}

		public static string EnviarEmailResposta(string codFab, string numParada, string codDepto)
		{
			// busca a lista com os usuários para o envio do email
			var emails = ObterListaAmplitudeParada(codFab, numParada);

			// Obtem os destinatários que irão em cópia
			var copia = ObterListaAmplitude("COPIA");
			var copiaS = String.Join("; ", copia.Select(x => x.NomEmail.Trim()));

			// obtem os dados da parada
			var parada = ObterParada(codFab, numParada);

			// seleciona os emails dos responsáveis pela parada
			var enderecos = String.Join("; ", emails.Select(x => x.NomEmail));

			var resposta = daoParadaDeLinha.ObterParadaDepItem(codFab, numParada, codDepto);

			// define o conteúdo do email
			var msg = $"Ocorrência de Parada de Linha de acordo com os parâmetros Abaixo: \n" +
							$"Nº Parada     : {numParada} \n" +
							$"Data Parada   : {parada.DatParada.ToShortDateString()} \n" +
							$"MODELO        : {parada.CodModelo}  {parada.DesModelo} \n" +
							$"N.E.          : {parada.CodItem}  {parada.DesItem} \n" +
							$"LINHA         : {parada.CodLinha} - {parada.DesLinha} \n" +
							$"SETOR         : {parada.DesDepLinha} \n" +
							$"MOTIVO        : {parada.CodParada} - {parada.DesParada} \n" +
							$"HORARIO DE INICIO DA PARADA : {parada.HorInicio} \n" +
							$"HORARIO DO FINAL DA PARADA: {parada.HorFim} \n" +
							$"--------------------------------------------------------------------\n" +
							$"CAUSA DA PARADA : {resposta.DesCausa} \n" +
							$"AÇÃO CORRETIVA  : {resposta.DesAcao} \n" +
							$"RESPONSAVEL     : {resposta.NomResponsavel} \n";

			/*var msg = String.Format(mensagem, numParada, parada.DatParada.ToShortDateString(), parada.CodModelo, parada.DesModelo,
										parada.CodItem, parada.DesItem, parada.CodLinha, parada.DesLinha, parada.DesDepLinha, parada.CodParada, parada.DesParada,
										parada.HorInicio, parada.HorFim, resposta.DesCausa, resposta.DesAcao, resposta.NomResponsavel);*/

			// dispara e-mail para os responsáveis
			var emailenviado = Geral.EnviarEmail("paradadelinha@semptcl.com.br", "Parada de Linha", enderecos, "Parada de linha", msg, copiaS);

			if (emailenviado)
			{
				var depitem = daoParadaDeLinha.ObterListaParadaDepItem(codFab, numParada);
				foreach (var item in depitem)
				{
					item.DatEnvio = Util.GetDate();
					daoParadaDeLinha.GravarParadaDepItem(item);
				}
				return "True";
			}

			return "False";

		}

		#endregion
	}
}
