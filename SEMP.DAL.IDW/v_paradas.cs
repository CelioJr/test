﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.IDW
{
	public partial class v_paradas
	{
		[Key]
		[Column(Order = 1)]
		public string maquina { get; set; }
		[Key]
		[Column(Order = 2)]
		public string codigo { get; set; }
		public string descricao { get; set; }
		[Key]
		[Column(Order = 3)]
		public Nullable<DateTime> inicio { get; set; }
		public Nullable<DateTime> fim { get; set; }
		public Nullable<decimal> tempoComPeso { get; set; }
		public Nullable<decimal> tempoSemPeso { get; set; }

	}
}
