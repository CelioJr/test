using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_GpaMovimentacaoMap : EntityTypeConfiguration<tbl_GpaMovimentacao>
    {
        public tbl_GpaMovimentacaoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodItem, t.DocInterno1, t.DocInterno2, t.CodArea, t.CodLocal, t.DataInicial, t.DataMovimentacao, t.CodDRT });

            // Properties
            this.Property(t => t.CodItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.DocInterno1)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.DocInterno2)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.CodArea)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.CodLocal)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.TipDocto1)
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.DocInterno3)
                .IsFixedLength()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.CodDRT)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.Operacao)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.CodBloqueio)
                .HasMaxLength(2)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_GpaMovimentacao");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.DocInterno1).HasColumnName("DocInterno1");
            this.Property(t => t.DocInterno2).HasColumnName("DocInterno2");
            this.Property(t => t.CodArea).HasColumnName("CodArea");
            this.Property(t => t.CodLocal).HasColumnName("CodLocal");
            this.Property(t => t.DataInicial).HasColumnName("DataInicial");
            this.Property(t => t.TipDocto1).HasColumnName("TipDocto1");
            this.Property(t => t.DocInterno3).HasColumnName("DocInterno3");
            this.Property(t => t.DataMovimentacao).HasColumnName("DataMovimentacao");
            this.Property(t => t.DataFinal).HasColumnName("DataFinal");
            this.Property(t => t.CodDRT).HasColumnName("CodDRT");
            this.Property(t => t.Operacao).HasColumnName("Operacao");
            this.Property(t => t.CodBloqueio).HasColumnName("CodBloqueio");
        }
    }
}
