namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_necimplog
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pp_necimplog()
        {
            pp_necimpurllog = new HashSet<pp_necimpurllog>();
        }

        [Key]
        public long id_necimplog { get; set; }

        public DateTime? dthr_iimportacao { get; set; }

        public long id_usr { get; set; }

        public DateTime? dthr_fimportacao { get; set; }

        public long id_necimp { get; set; }

        public int? mes_referencia { get; set; }

        public int? ano_referencia { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual pp_necimp pp_necimp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_necimpurllog> pp_necimpurllog { get; set; }
    }
}
