namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.TEST_TABLEs")]
    public partial class TEST_TABLEs
    {
        public int id { get; set; }

        [StringLength(255)]
        public string title { get; set; }

        public string description { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime createdAt { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime updatedAt { get; set; }
    }
}
