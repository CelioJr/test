using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBLinhaObservacaoMap : EntityTypeConfiguration<tbl_CBLinhaObservacao>
    {
        public tbl_CBLinhaObservacaoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodLinha, t.DatProducao });

            // Properties
            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Observacao)
                .HasMaxLength(500);

            this.Property(t => t.NomUsuario)
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("tbl_CBLinhaObservacao");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.DatProducao).HasColumnName("DatProducao");
            this.Property(t => t.Observacao).HasColumnName("Observacao");
            this.Property(t => t.NomUsuario).HasColumnName("NomUsuario");
            this.Property(t => t.DatModificacao).HasColumnName("DatModificacao");
        }
    }
}
