using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DEP_REGRA_REGRA_DTO
    {
        public int REG_COD { get; set; }
        public int REG_COD_REFERENCIA { get; set; }
    }
}
