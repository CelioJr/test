using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_CAD_LINHAS_DTO
    {
        public GBR_CAD_LINHAS_DTO()
        {
            this.alc_lot_list = new List<ALC_LOT_LIST_DTO>();
        }

        public int COD_LINHAS { get; set; }
        public string NOME_LINHA { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
        public virtual ICollection<ALC_LOT_LIST_DTO> alc_lot_list { get; set; }
    }
}
