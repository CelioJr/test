namespace SEMP.DAL.DTB_SSA.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_ItemEstoque
    {
        public tbl_ItemEstoque()
        {
            this.tbl_Key_Component_d = new HashSet<tbl_Key_Component_d>();
            this.tbl_Key_Component_STI = new HashSet<tbl_Key_Component_STI>();
        }
    
        public string NE { get; set; }
        public string descricao { get; set; }
        public string unidade { get; set; }
        public string familia_id { get; set; }
        public decimal? custo_medio { get; set; }
        public string flgKitEmbalagem { get; set; }
        public int? dispositivo_id { get; set; }
        public string hcl_tg { get; set; }
        public string hcl_f { get; set; }
        public string hcl_p { get; set; }
        public int? codItem { get; set; }
        public string flgKitAcessorio { get; set; }
        public string flgCOA { get; set; }
        public int? periodo_frequencia { get; set; }
        public string flgColetaExped { get; set; }
        public int? QtdePalete { get; set; }
        public string flgVirtual { get; set; }
        public int? RespDispositivo { get; set; }
    
        public virtual ICollection<tbl_Key_Component_d> tbl_Key_Component_d { get; set; }
        public virtual ICollection<tbl_Key_Component_STI> tbl_Key_Component_STI { get; set; }
    }
}
