﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.Models;

namespace SEMP.DAL.DAO.Rastreabilidade
{
	public class daoRelatorioTecnico
	{

		public static int ObterProducaoModelo(DateTime DatInicio, DateTime DatFim, string codModelo)
		{

			using (DTB_CBContext dtbCB = new DTB_CBContext())
			{

				var producao = dtbCB.tbl_CBEmbalada.Where(x => x.CodModelo == codModelo && x.DatReferencia >= DatInicio && x.DatReferencia <= DatFim).ToList();

				return producao.Count();

			}
		}

		public static int ObterProducaoLinha(DateTime DatInicio, DateTime DatFim, int codLinha)
		{

			using (DTB_CBContext dtbCB = new DTB_CBContext())
			{

				var producao = dtbCB.tbl_CBEmbalada.Where(x => x.CodLinha == codLinha && x.DatReferencia >= DatInicio && x.DatReferencia <= DatFim).ToList();

				return producao.Count();

			}
		}

		public static int ObterProducaoFase(DateTime DatInicio, DateTime DatFim, string Fabrica)
		{

			using (DTB_CBContext dtbCB = new DTB_CBContext())
			{

				var producao = (from e in dtbCB.tbl_CBEmbalada
								join l in dtbCB.tbl_CBLinha on e.CodLinha equals l.CodLinha
								where e.DatReferencia >= DatInicio && e.DatReferencia <= DatFim && l.Setor == Fabrica
								select e).ToList();

				return producao.Count();

			}
		}

		public static int ObterProducaoFamilia(DateTime DatInicio, DateTime DatFim, string Fabrica, string Familia)
		{

			using (DTB_CBContext dtbCB = new DTB_CBContext())
			{

				var producao = (from e in dtbCB.tbl_CBEmbalada
								join l in dtbCB.tbl_CBLinha on e.CodLinha equals l.CodLinha
								where e.DatReferencia >= DatInicio && e.DatReferencia <= DatFim && l.Setor == Fabrica && l.Familia == Familia
								select e).ToList();

				return producao.Count();

			}
		}

		public static int ObterProdutosPendentes(DateTime DatInicio, DateTime DatFim, string codModelo)
		{

			using (DTB_CBContext dtbCB = new DTB_CBContext())
			{

				var producao = dtbCB.viw_CBDefeitoTurno.Where(x =>
					x.NumECB.StartsWith(codModelo) &&
					x.DatReferencia >= DatInicio &&
					x.DatReferencia <= DatFim &&
					x.FlgRevisado == 0 &&
					String.IsNullOrEmpty(x.NumSeriePai)).ToList();

				return producao.Count();

			}
		}

		public static int ObterProdutosPendentes(DateTime DatInicio, DateTime DatFim, int codLinha, string codModelo = "")
		{

			using (DTB_CBContext dtbCB = new DTB_CBContext())
			{

				var producao = dtbCB.viw_CBDefeitoTurno.Where(x =>
					x.CodLinha == codLinha &&
					x.DatReferencia >= DatInicio &&
					x.DatReferencia <= DatFim &&
					x.FlgRevisado == 0 &&
					String.IsNullOrEmpty(x.NumSeriePai)).ToList();

				if (!String.IsNullOrEmpty(codModelo))
				{
					producao = producao.Where(x => x.NumECB.StartsWith(codModelo)).ToList();
				}

				return producao.Count();

			}
		}

		public static int ObterPendentesFase(DateTime DatInicio, DateTime DatFim, string Fabrica, string codModelo = "")
		{

			using (var dtbCB = new DTB_CBContext())
			{

				var producao = (from e in dtbCB.viw_CBDefeitoTurno
								join l in dtbCB.tbl_CBLinha on e.CodLinha equals l.CodLinha
								where e.DatReferencia >= DatInicio && e.DatReferencia <= DatFim && l.Setor == Fabrica &&
									String.IsNullOrEmpty(e.NumSeriePai) &&
									e.FlgRevisado == 0
								select e).ToList();

				if (!String.IsNullOrEmpty(codModelo))
				{
					producao = producao.Where(x => x.NumECB.StartsWith(codModelo)).ToList();
				}
				return producao.Count();

			}
		}
	}
}
