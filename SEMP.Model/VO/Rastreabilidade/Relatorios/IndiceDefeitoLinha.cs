﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
	public class IndiceDefeitoLinha
	{
		public enum Origem { Producao, Embalado }

		public int CodLinha { get; set; }
		public int QtdProducao { get; set; }
		public int QtdEmbalado { get; set; }
		public int QtdDefeito { get; set; }
		public int QtdFalsaFalha { get; set; }
		public int QtdRAC { get; set; }

        public int QtDefeitoReal { get;set; }

		public IndiceDefeitoLinha(){
			QtdProducao = 0;
			QtdDefeito = 0;
			QtdEmbalado = 0;
			QtdFalsaFalha = 0;
			QtdRAC = 0;
		}

		public decimal IndTotal(Origem Origem)
		{
			decimal resultado = 0;
			
			if (Origem == Origem.Producao && QtdProducao > 0) {
				resultado = Math.Round((decimal)QtdDefeito / (decimal)QtdProducao * 100, 2);
			}
			else if (QtdEmbalado > 0) {
				resultado = Math.Round((decimal)QtdDefeito / (decimal)QtdEmbalado * 100, 2);
			}

			return resultado;
		}
		public decimal IndReal(Origem Origem)
		{
			var Qtd = QtdDefeito - QtdFalsaFalha - QtdRAC;
			decimal resultado = 0;

			if (Origem == Origem.Producao && QtdProducao > 0) {
				resultado = Math.Round((decimal)Qtd / (decimal)QtdProducao * 100, 2);
			}
			else if (QtdEmbalado > 0) {
				resultado = Math.Round( (decimal)Qtd / (decimal)QtdEmbalado * 100, 2);
			}
			return resultado;
		}
		public decimal IndFalsaFalha(Origem Origem)
		{
			decimal resultado = 0;

			if (Origem == Origem.Producao && QtdProducao > 0) {
				resultado = Math.Round((decimal)QtdFalsaFalha / (decimal)QtdProducao * 100, 2);
			}
			else if (QtdEmbalado > 0) {
				resultado = Math.Round((decimal)QtdFalsaFalha / (decimal)QtdEmbalado * 100, 2);
			}
			return resultado;
		}
		public decimal IndRAC(Origem Origem)
		{
			decimal resultado = 0;

			if (Origem == Origem.Producao && QtdProducao > 0) {
				resultado = Math.Round((decimal)QtdRAC / (decimal)QtdProducao * 100, 2);
			}
			else if (QtdEmbalado > 0) {
				resultado = Math.Round((decimal)QtdRAC / (decimal)QtdEmbalado * 100, 2);
			}
			return resultado;
		}
	}
}
