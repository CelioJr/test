namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ms_msicup
    {
        [Key]
        public long id_msicup { get; set; }

        public long? id_ic { get; set; }

        public long? id_ms { get; set; }

        public long? id_up { get; set; }

        [StringLength(100)]
        public string url_conexao { get; set; }

        public int? tp_conexao { get; set; }

        [StringLength(100)]
        public string url_auxiliar { get; set; }

        public short? is_ativo { get; set; }

        public virtual ms_ic ms_ic { get; set; }

        public virtual ms_ms ms_ms { get; set; }

        public virtual ms_up ms_up { get; set; }
    }
}
