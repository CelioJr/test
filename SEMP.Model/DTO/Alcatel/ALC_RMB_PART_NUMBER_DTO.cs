using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_RMB_PART_NUMBER_DTO
    {
        public ALC_RMB_PART_NUMBER_DTO()
        {
            this.ALC_RMB_COST = new List<ALC_RMB_COST_DTO>();
            this.ALC_RMB_REGISTER = new List<ALC_RMB_REGISTER_DTO>();
        }

        public string SAP_CODE { get; set; }
        public string DESCRIPTION { get; set; }
        public virtual ICollection<ALC_RMB_COST_DTO> ALC_RMB_COST { get; set; }
        public virtual ICollection<ALC_RMB_REGISTER_DTO> ALC_RMB_REGISTER { get; set; }
    }
}
