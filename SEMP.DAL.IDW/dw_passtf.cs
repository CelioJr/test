namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_passtf
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_passtf()
        {
            dw_passtfse = new HashSet<dw_passtfse>();
        }

        [Key]
        public long id_passtf { get; set; }

        public long id_passagem { get; set; }

        public short? st_etapa { get; set; }

        public DateTime? dthr_ietapa { get; set; }

        public int? ms_dthrietapa { get; set; }

        public DateTime? dthr_fetapa { get; set; }

        public int? ms_dthrfetapa { get; set; }

        public long id_ft_etapa { get; set; }

        public long? id_passcau { get; set; }

        public int? ordemEtapa { get; set; }

        public virtual dw_ft_etapa dw_ft_etapa { get; set; }

        public virtual dw_passagem dw_passagem { get; set; }

        public virtual dw_passcau dw_passcau { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_passtfse> dw_passtfse { get; set; }
    }
}
