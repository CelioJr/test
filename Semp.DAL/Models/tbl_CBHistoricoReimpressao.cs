using System;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBHistoricoReimpressao
    {
        public string DesSerie { get; set; }
        public System.DateTime DatImpressao { get; set; }
        public Nullable<int> CodOpe { get; set; }
    }
}
