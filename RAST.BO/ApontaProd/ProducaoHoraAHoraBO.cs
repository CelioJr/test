﻿using System;
using System.Collections.Generic;
using System.Linq;
using SEMP.DAL.DAO.Apontaprod;
using SEMP.DAL.DAO.Mobilidade;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.DTO;
using SEMP.Model.VO.Rastreabilidade;
using SEMP.Model.VO.Rastreabilidade.Relatorios;

namespace RAST.BO.ApontaProd
{
	public class ProducaoHoraAHoraBO
	{
		public static List<ProducaoHoraAHora> ObterProducaoHoraAHora(DateTime Data, String Fase)
		{
			if (Fase.Equals("IMC"))
			{
				var dados = daoProducaoHoraAHora.ObterProducaoHoraAHoraIMC(Data);

				dados = dados.Where(x => x.CodLinha.StartsWith("111")).ToList();

				return dados;
			}
			else if (Fase.Equals("IAC"))
			{
				var dados = daoProducaoHoraAHora.ObterProducaoHoraAHoraIMC(Data);

				dados = dados.Where(x => !x.CodLinha.StartsWith("111")).ToList();

				return dados;
			}
			else if (Fase.Equals("MOB"))
			{
				var dados = daoAlcatel.ObterProducaoHoraAHora(Data);

				return dados;
			}
			else
			{
				return daoProducaoHoraAHora.ObterProducaoHoraAHora(Data);
			}

		}

		public static List<ProducaoLinhaMinuto> ObterProducaoLinhaMinuto(DateTime datReferencia, string fase = "FEC")
		{

			try
			{
				if (fase == "FEC")
				{
					return daoAcompanhamento.ObterLeiturasSimples(datReferencia);
				}
				else if (fase == "MOB")
				{
					return daoAlcatel.ObterLeiturasSimples(datReferencia);
				}
				else
				{
					return daoAcompanhamento.ObterLeiturasSimplesIMC(datReferencia, fase);
				}

			}
			catch (Exception)
			{
				return new List<ProducaoLinhaMinuto>();
			}

		}

		public static List<ProducaoLinhaMinuto> ObterProducaoLinhaMinutoFase(DateTime datReferencia, string fase = "FEC")
		{

			try
			{
				return daoAcompanhamento.ObterLeiturasSimplesFase(datReferencia, fase);

			}
			catch (Exception)
			{
				return new List<ProducaoLinhaMinuto>();
			}

		}

		public static List<ProducaoDataHora> ObterProducaoDataHora(DateTime datIni, DateTime datFim, int codLinha, int numPosto, int horaIni, int horaFim)
		{
			var retorno = daoAcompanhamento.ObterProducaoDataHora(datIni, datFim, codLinha, horaIni, horaFim);

			if (numPosto != 0)
			{
				retorno = retorno.Where(x => x.NumPosto == numPosto).ToList();
			}

			return retorno;
		}

		public static RetornoGraficoGargalo ObterProducaoData(DateTime datIni, DateTime datFim, int codLinha, string codModelo = "")
		{
			var producao = daoAcompanhamento.ObterProducaoData(datIni, datFim, codLinha);

			if (!String.IsNullOrEmpty(codModelo))
			{
				producao = producao.Where(x => x.CodModelo == codModelo).ToList();
			}
			
			var menorValor = producao.GroupBy(x => x.CodModelo).Select(y => new
			{
				CodModelo = y.Key,
				MenorValor = y.Min(x => x.QtdProduzido)
			}).ToList();

			menorValor.ForEach(x =>
			{
				var firstOrDefault = producao.FirstOrDefault(y => y.CodModelo == x.CodModelo && y.QtdProduzido == x.MenorValor);
				if (firstOrDefault != null)
					firstOrDefault.FlgGargalo = 2;
			});

			var max = producao.Max(x => x.QtdProduzido);
			var maxMeta = producao.Max(x => x.QtdMeta);
			var min = producao.Min(y => y.QtdProduzido);
			var minMeta = producao.Min(x => x.QtdMeta);

			if (maxMeta > max) max = (int)maxMeta;
			if (minMeta < min) min = (int)minMeta;

			var retorno = new RetornoGraficoGargalo()
			{
				dados = producao.Select(x => new DadosGraficoA() { codmodelo = x.CodModelo, drilldown = x.NumPosto.ToString(), name = x.DesPosto, y = x.QtdProduzido, color = x.Cor }).ToList(),
				metas = producao.Select(x => new DadosGraficoA() { codmodelo = x.CodModelo, name = x.DesPosto, y = (double)x.QtdMeta, color = "Green" }).ToList(),
				plano = producao.Select(x => new DadosGraficoA() { codmodelo = x.CodModelo, name = x.DesPosto, y = (double)x.QtdPlano, color = "Orange" }).ToList(),
				/*menor = producao.Select(x =>
				{
					var orDefault = menorValor.FirstOrDefault(p => p.CodModelo == x.CodModelo);
					return orDefault != null ? new DadosGraficoA() { name = x.DesPosto, y = orDefault.MenorValor, color = "Red" } : null;
				}).ToList(),*/
				modelos = producao.Where(x => x.SeqPosto == producao.Max(y => y.SeqPosto)).Select(x => new DadosGraficoA() { codmodelo = x.CodModelo, name = x.CodModelo + '-' + x.DesModelo.Trim(), y = x.QtdProduzido, color = "Black" }).Distinct().ToList(),
				limite = new Limite() { max = max, min = min }
			};

			return retorno;
		}

		public static RetornoGraficoGargalo2 ObterProducaoData2(DateTime datIni, DateTime datFim, int codLinha, string codModelo = "")
		{
			var producao = daoAcompanhamento.ObterProducaoData(datIni, datFim, codLinha);
			
			if (!String.IsNullOrEmpty(codModelo))
			{
				producao = producao.Where(x => x.CodModelo == codModelo).ToList();
			}

			var menorValor = producao.GroupBy(x => x.CodModelo).Select(y => new
			{
				CodModelo = y.Key,
				MenorValor = y.Min(x => x.QtdProduzido)
			}).ToList();

			menorValor.ForEach(x =>
			{
				var firstOrDefault = producao.FirstOrDefault(y => y.CodModelo == x.CodModelo && y.QtdProduzido == x.MenorValor);
				if (firstOrDefault != null && firstOrDefault.FlgGargalo != 1)
					firstOrDefault.FlgGargalo = 2;
			});

			var modelos = producao.GroupBy(m => m.CodModelo).Select(x => x.Key);

			var categorias = producao.GroupBy(x => new {x.SeqPosto, x.CodModelo}).Select(x =>
			new
			{
				DesPosto = "Sequência: " + x.Key.SeqPosto, 
				 SeqPosto = x.Key.SeqPosto, 
				 CodModelo = x.Key.CodModelo,
				 QtdPostos = x.Count(),
				 QtdCapac = x.Sum(y => y.QtdMeta)
			}).Distinct().ToList();

			var dados = new List<DadosGraficoB>();

			foreach (var modelo in modelos)
			{
				foreach (var linha in producao.Where(m => m.CodModelo == modelo))
				{
					var qtdes = new List<int>();

					foreach (var categoria in categorias.Where(c => c.CodModelo == modelo))
					{
						qtdes.Add((categoria.SeqPosto == linha.SeqPosto) ? linha.QtdProduzido : 0);

					}

					dados.Add(new DadosGraficoB()
					{
						codmodelo = linha.CodModelo,
						name = linha.DesPosto,
						stack = "Produção",
						data = qtdes.ToArray()
					});
				}

				dados.Add(new DadosGraficoB()
				{
					codmodelo = modelo,
					name = "Capacidade",
					stack = "Capacidade",
					color = "green",
					data = categorias.Where(c => c.CodModelo == modelo).Select(x => (int)x.QtdCapac).ToArray()
				});
			}

			var maxpostos = categorias.Max(x => x.QtdCapac);
			maxpostos += (maxpostos * 0.001m);

			var total = new List<DadosGraficoA>();

			total.AddRange(producao.GroupBy(m => m.CodModelo).Select(x => new DadosGraficoA()
			{
				codmodelo = x.Key,
				y = x.Sum(c => c.QtdProduzido),
				color = "Blue",
				name = "Produção"
			}));

			total.AddRange(producao.GroupBy(m => m.CodModelo).Select(x => new DadosGraficoA()
			{
				codmodelo = x.Key,
				y = x.Sum(c => (double)c.QtdMeta),
				color = "Green",
				name = "Capacidade"
			}));


			var retorno = new RetornoGraficoGargalo2()
			{
				Categorias = categorias.Select(x => new Categorias() {CodModelo = x.CodModelo, DesPosto = x.DesPosto}).ToList(),
				agregado = dados,
				dados = producao.Select(x => new DadosGraficoA()
				{
					codmodelo = x.CodModelo, 
					drilldown = x.NumPosto.ToString(), 
					name = x.DesPosto, 
					y = x.QtdProduzido, 
					color = x.Cor
				}).ToList(),
				plano = producao.GroupBy(x => new {x.CodModelo, x.DesModelo}).Select(x => new DadosGraficoA()
				{
					codmodelo = x.Key.CodModelo, 
					name = x.Key.CodModelo + '-' + x.Key.DesModelo.Trim(), 
					y = x.Max(z => (double)z.QtdPlano), 
					color = "Black"
				}).Distinct().ToList(),
				modelos = producao.GroupBy(x => new {x.CodModelo, x.DesModelo}).Select(x => new DadosGraficoA()
				{
					codmodelo = x.Key.CodModelo, 
					name = x.Key.CodModelo + '-' + x.Key.DesModelo.Trim(), 
					y = x.Max(z => z.QtdProduzido), 
					color = "Black",
					drilldown = x.Max(z => z.QtdPlano).ToString()
				}).Distinct().ToList(),
				menor = total,
				limite = new Limite(){max = (double)maxpostos, min = 0}
			};

			return retorno;
		}

		public static RetornoGraficoGargalo ObterProducaoHora(DateTime datIni, DateTime datFim, int codLinha, int numPosto,
			string codModelo = "")
		{
			// pegar os valores diários
			var diario = ObterProducaoDataHora(datIni, datFim, codLinha, numPosto, 0, 0);

			var dia = datIni == datFim;

			var metasDiario =
				dia ?
				diario//.Where(h => h.Hora > 6)
					.GroupBy(x => x.Hora )
					.Select(m => new { Hora = m.Key.ToString("00") + ":00", QtdMeta = m.Sum(x => x.QtdMeta) }) :
				diario//.Where(h => h.Hora > 6)
					.GroupBy(x => x.Data )
					.Select(m => new { Hora = m.Key.ToShortDateString(), QtdMeta = m.Sum(x => x.QtdMeta) });

			var dados =
				dia ?
					diario.GroupBy(x => new {x.Hora})
						.Select(
							x => new DadosGraficoA() {color = "Aquamarine", name = x.Key.Hora.ToString("00") + ":00", y = x.Sum(m => m.QtdProduzido)})
						.ToList() :
					diario.GroupBy(x => new { x.Data })
						.Select(
							x => new DadosGraficoA() { color = "Aquamarine", name = x.Key.Data.ToShortDateString(), y = x.Sum(m => m.QtdProduzido) })
						.ToList();

			var metas = metasDiario
				.Select(x => new DadosGraficoA() {color = "green", name = x.Hora, y = x.QtdMeta})
				.ToList();

			var max = 0.0;
			var min = 0.0;

			if (dados.Count > 0)
			{
				max = dados.Max(x => x.y);
				var maxMeta = metas.Max(x => x.y);
				min = dados.Min(y => y.y);
				var minMeta = metas.Min(x => x.y);

				if (maxMeta > max) max = (int) maxMeta;
				if (minMeta < min) min = (int) minMeta;
			}
			var retorno = new RetornoGraficoGargalo()
			{
				dados = dados,
				metas = metas,
				limite = new Limite(){max = max, min = min}
			};

			return retorno;

		}

		public static List<CBTempoMedioLinha> ObterTempoMedioLinha(DateTime datIni, DateTime datFim, int codLinha, string horaInicio, string horaFim)
		{
			return daoProducaoHoraAHora.ObterCBTempoMedioLinha(datIni, datFim, codLinha, horaInicio, horaFim);

		}

		public static List<CBTempoMedioLinha> ObterTempoMedioModelo(DateTime datIni, DateTime datFim, string codModelo, int codLinha, string horaInicio, string horaFim)
		{
			return daoProducaoHoraAHora.ObterCBTempoMedioModelo(datIni, datFim, codModelo, codLinha, horaInicio, horaFim);

		}

		public static void GravarTempo(int codLinha, int numPosto, string codModelo, decimal tempoMedio)
		{
			daoProducaoHoraAHora.GravarTempo(codLinha, numPosto, codModelo, tempoMedio);

		}

		#region Perfil de Modelo por Posto
		public static List<CBLinhaPostoPerfilDTO> ObterPerfilLinhaPosto(int codLinha)
		{
			return daoAcompanhamento.ObterPerfilLinhaPosto(codLinha);
		}

		public static List<CBLinhaPostoPerfilDTO> ObterPerfilLinhaPosto(int? codLinha, string codModelo)
		{
			return daoAcompanhamento.ObterPerfilLinhaPosto(codLinha, codModelo);
		}

		public static CBLinhaPostoPerfilDTO ObterPerfilLinhaPosto(int codLinha, int numPosto, string codModelo)
		{
			return daoAcompanhamento.ObterPerfilLinhaPosto(codLinha, numPosto, codModelo);
		}

		public static void GravarPerfilLinhaPosto(CBLinhaPostoPerfilDTO dado)
		{
			daoAcompanhamento.GravarPerfilLinhaPosto(dado);
		}

		public static void RemoverPerfilLinhaPosto(CBLinhaPostoPerfilDTO dado)
		{
			daoAcompanhamento.RemoverPerfilLinhaPosto(dado);
		}
		#endregion

		public static List<string> ObterHorarios()
		{
			var horarios = new List<string>();

			for (var i = 0; i < 23; i++)
			{
				horarios.Add(String.Format("{0}:00", i));

			}

			return horarios;
		}
	}
}
