using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBHorarioMap : EntityTypeConfiguration<tbl_CBHorario>
    {
        public tbl_CBHorarioMap()
        {
            // Primary Key
            this.HasKey(t => t.idHorario);

            // Properties
            this.Property(t => t.HoraInicio)
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.HoraFim)
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.flgViraDia)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_CBHorario");
            this.Property(t => t.idHorario).HasColumnName("idHorario");
            this.Property(t => t.Turno).HasColumnName("Turno");
            this.Property(t => t.HoraInicio).HasColumnName("HoraInicio");
            this.Property(t => t.HoraFim).HasColumnName("HoraFim");
            this.Property(t => t.flgViraDia).HasColumnName("flgViraDia");
        }
    }
}
