using System;

namespace SEMP.Model.DTO
{
    public class RIFuncionarioDTO
    {
        public string CodFab { get; set; }
        public string CodDRT { get; set; }
        public string FlgTipo { get; set; }
        public Nullable<short> FlgControleAcesso { get; set; }
        public string NomUsuario { get; set; }
        public string NomFuncionario { get; set; }
        public string NomLogin { get; set; }
        public string CodCargo { get; set; }
        public string CodSetor { get; set; }
        public string CodSecao { get; set; }
        public string CodDepto { get; set; }
        public string CodCBO { get; set; }
        public Nullable<System.DateTime> DatAdm { get; set; }
        public Nullable<System.DateTime> DatNas { get; set; }
        public string NumCTPS { get; set; }
        public string UF_CTPS { get; set; }
        public string Serie_CTPS { get; set; }
        public string NumPIS { get; set; }
        public string NumCPF { get; set; }
        public string FlgAtivo { get; set; }
        public string FlgSexo { get; set; }
        public string Endereco { get; set; }
        public string Numero { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string NumCEP { get; set; }
        public string UF { get; set; }
        public string NumTelefone { get; set; }
        public string NomEmpresa { get; set; }
        public string NomMae { get; set; }
        public string NomPai { get; set; }
        public string FlgECivil { get; set; }
        public string DesNatural { get; set; }
        public string DesNacional { get; set; }
        public string FlgAutoriza { get; set; }
        public Nullable<short> FlgEscola { get; set; }
        public Nullable<short> FlgLivre { get; set; }
        public Nullable<short> FlgIncentivo { get; set; }
        public string NumRamal { get; set; }
        public string NumComplemento { get; set; }
        public string NumRG { get; set; }
        public Nullable<System.DateTime> DatExpedicao { get; set; }
        public string NomExpedidor { get; set; }
        public string UFNacto { get; set; }
        public string NumCracha { get; set; }
        public string CodContabil { get; set; }
        public Nullable<System.DateTime> DatDemissao { get; set; }
        public string flgTurno { get; set; }
        public string CodPagamento { get; set; }
        public string TituloEleitor { get; set; }
        public string ZonaTitulo { get; set; }
        public string SecaoTitulo { get; set; }
        public string CodRegra { get; set; }
        public string CargaHoraDiaria { get; set; }
        public string CargaHoraMensal { get; set; }
        public string StatusProvisorio { get; set; }
        public string FlgExtra { get; set; }
        public string NumNIT { get; set; }
        public string TipBeneficio { get; set; }
        public string TipRevezamento { get; set; }
        public string CodDRTCript { get; set; }
        public string flgGerarEspelho { get; set; }
        public Nullable<decimal> ValSalario { get; set; }
        public Nullable<System.DateTime> DatFim1Periodo { get; set; }
        public Nullable<System.DateTime> DatFim2Periodo { get; set; }
        public string CodProcesso { get; set; }
        public string Rota { get; set; }
        public string PontoReferencia { get; set; }
        public string flgSempar { get; set; }
        public string Agencia { get; set; }
        public string NumConta { get; set; }
    }
}
