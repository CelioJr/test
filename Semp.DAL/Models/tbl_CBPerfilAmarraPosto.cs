using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBPerfilAmarraPosto
    {
        public int CodLinha { get; set; }
        public int NumPosto { get; set; }
        public int CodTipoAmarra { get; set; }
        public Nullable<int> Sequencia { get; set; }
    }
}
