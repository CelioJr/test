using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_REPAIR_ACTION_DTO
    {
        public ALC_REPAIR_ACTION_DTO()
        {
            this.ALC_REPAIR = new List<ALC_REPAIR_DTO>();
        }

        public int ID_ACTION { get; set; }
        public string DESCRIPTION { get; set; }
        public virtual ICollection<ALC_REPAIR_DTO> ALC_REPAIR { get; set; }
    }
}
