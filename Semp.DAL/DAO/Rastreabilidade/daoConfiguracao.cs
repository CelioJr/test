﻿using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.VO;
using SEMP.DAL.Models;
using SEMP.Model.DTO;

namespace SEMP.DAL.DAO.Rastreabilidade
{
	public class daoConfiguracao
	{
        //static daoConfiguracao() {
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<tbl_CBLinha, CBLinhaDTO>();
        //        cfg.CreateMap<tbl_CBPosto, CBPostoDTO>();
        //        cfg.CreateMap<tbl_CBTipoPosto, CBTipoPostoDTO>();
        //        cfg.CreateMap<tbl_CBHorario, CBHorarioDTO>();
        //        cfg.CreateMap<tbl_CBTipoAmarra, CBTipoAmarraDTO>();
        //        cfg.CreateMap<tbl_CBPerfilAmarraPosto, CBPerfilAmarraPostoDTO>();
        //        cfg.CreateMap<tbl_CBLoteConfig, CBLoteConfigDTO>();
        //        cfg.CreateMap<tbl_CBHorarioLinha, CBHorarioLinhaDTO>();
        //        cfg.CreateMap<tbl_Item, ItemDTO>();
        //        cfg.CreateMap<tbl_CBMaquina, CBMaquinaDTO>();
        //    });

        //}


		#region Validações
		
		public static Boolean VerificaLinha(int CodLinha)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var linha = dtb_CB.tbl_CBLinha.Where(x => x.CodLinha == CodLinha).FirstOrDefault();

				if (linha == null)
					return false;
				else
					return true;
			}
		}

		public static Boolean VerificaLinhaCliente(string CodLinhaCliente)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var linha = dtb_CB.tbl_CBLinha.Where(x => x.CodLinhaT1 == CodLinhaCliente).FirstOrDefault();

				if (linha == null)
					return false;
				else
					return true;
			}
		}

		public static Boolean VerificaLinhaClienteSIM(string CodLinhaCliente)
		{
			using (DTB_SIMContext dtb_SIM = new DTB_SIMContext())
			{
				var linha = dtb_SIM.tbl_BarrasLinha.Where(x => x.CodLinha == CodLinhaCliente || x.CodLinhaRel == CodLinhaCliente).FirstOrDefault();

				if (linha == null)
					return false;
				else
					return true;
			}
		}

		public static Boolean VerificaPosto(int CodLinha, int NumPosto)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var posto = dtb_CB.tbl_CBPosto.Where(x => x.CodLinha == CodLinha && x.NumPosto == NumPosto).FirstOrDefault();

				if (posto == null)
					return false;
				else
					return true;
			}
		}

		public static Boolean VerificaHorario(string HorarioInicio, string HorarioFim)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var horario = dtb_CB.tbl_CBHorario.Where(x => x.HoraInicio == HorarioInicio && x.HoraFim == HorarioFim).FirstOrDefault();

				if (horario == null)
					return false;
				else
					return true;
			}
		}

		public static Boolean VerificaLoteConfig(int CodLinha)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var config = dtb_CB.tbl_CBLoteConfig.Where(x => x.idLinha == CodLinha).FirstOrDefault();

				if (config == null)
					return false;
				else
					return true;
			}
		}

		public static Boolean VerificaHorarioLinha(int IdHorario, int CodLinha, int Turno)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var config = dtb_CB.tbl_CBHorarioLinha.Where(x => x.idHorario == IdHorario && x.CodLinha == CodLinha && x.Turno == Turno).FirstOrDefault();

				if (config == null)
					return false;
				else
					return true;
			}
		}

        public static Boolean VerificaMaquina(tbl_CBMaquina Maquina)
        {
            using (DTB_CBContext dtb_CB = new DTB_CBContext())
            {
                var config = dtb_CB.tbl_CBMaquina.Where(x => x.CodLinha == Maquina.CodLinha && x.CodMaquina == Maquina.CodMaquina).FirstOrDefault();

                if (config == null)
                    return false;
                else
                    return true;
            }
        }


	#endregion

		#region Consultas
		public static List<CBLinhaDTO> ObterLinhas()
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var linhas = (from p in dtb_CB.tbl_CBLinha
								let qtdPostos = dtb_CB.tbl_CBPosto.Count(x => x.CodLinha == p.CodLinha)
								select new CBLinhaDTO () {
									CodLinha = p.CodLinha,
									DesLinha = p.DesLinha.Trim(),
									DesObs = p.DesObs,
									Setor = p.Setor.Trim(),
									Familia = p.Familia,
									CodLinhaT1 = p.CodLinhaT1,
									CodLinhaT2 = p.CodLinhaT2,
									CodLinhaT3 = p.CodLinhaT3,
									CodPlaca = p.CodPlaca,
									CodModelo = p.CodModelo,
									CodAuxiliar = p.CodAuxiliar,
									CodFam = p.CodFam,
									CodLinhaOrigem = p.CodLinhaOrigem,
									SetorOrigem = p.SetorOrigem,
									FlgAtivo = p.flgAtivo,
									QtdPostos = qtdPostos
								});

				var p2 = linhas.ToList();

				
				return p2;
			}
		}

        public static string ObterDesLinha(int codLinha)
        {
            using (DTB_CBContext dtb_CB = new DTB_CBContext())
            {
                var desLinhas = (from p in dtb_CB.tbl_CBLinha where p.CodLinha == codLinha select p.DesLinha).FirstOrDefault();

                if (String.IsNullOrEmpty(desLinhas))
                {
                    return desLinhas = "Pendente descrição de linha";
                }
                
                return desLinhas;

            }
                        
        }

        public static string ObterDesPosto(int codTipoPosto)
        {
            using (DTB_CBContext dtb_CB = new DTB_CBContext())
            {
                var desTipoPosto = (from a in dtb_CB.tbl_CBTipoPosto where a.CodTipoPosto == codTipoPosto select a.DesTipoPosto).FirstOrDefault();

                if (String.IsNullOrEmpty(desTipoPosto))
                {
                    return desTipoPosto = "Informação pendente!";
                }
                
                return desTipoPosto;
            }

        }

		public static CBLinhaDTO ObterLinha(int CodLinha)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var linhas = (from p in dtb_CB.tbl_CBLinha								
							  where /*p.CodLinhaT1 != null &&*/ p.CodLinha == CodLinha
							  select p);

				var p2 = linhas.FirstOrDefault();

				CBLinhaDTO cBLinhaDTO = new CBLinhaDTO();

				AutoMapper.Mapper.Map(p2, cBLinhaDTO);

				return cBLinhaDTO;
			}
		}

        public static CBMaquinaDTO ObterMaquina(int CodLinha, string CodMaquina)
        {
            using (DTB_CBContext dtb_CB = new DTB_CBContext())
            {
                var maquinas = (from p in dtb_CB.tbl_CBMaquina
                                where p.CodLinha == CodLinha && p.CodMaquina != null && p.CodMaquina == CodMaquina
                              select p);

                var maquina = maquinas.FirstOrDefault();

                CBMaquinaDTO cBMaquinaDTO = new CBMaquinaDTO();

                AutoMapper.Mapper.Map(maquina, cBMaquinaDTO);

                return cBMaquinaDTO;
            }
        }

		public static List<CBPostoDTO> ObterPostos(int CodLinha)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var postos = (from p in dtb_CB.tbl_CBPosto
								join tp in dtb_CB.tbl_CBTipoPosto on p.CodTipoPosto equals tp.CodTipoPosto
							  where p.CodLinha == CodLinha
							  select new { p, tp.DesTipoPosto, tp.DesCor });

				var p2 = postos.Select(x => new CBPostoDTO() { 
					CodDRT = x.p.CodDRT,
					CodLinha = x.p.CodLinha,
					CodTipoPosto = x.p.CodTipoPosto,
					DesCor = x.DesCor,
					DesPosto = x.p.DesPosto,
					DesTipoPosto = x.DesTipoPosto,
					FlgAtivo = x.p.FlgAtivo,
					flgDRTObrig = x.p.flgDRTObrig,
					FlgGargalo = x.p.FlgGargalo,
					flgImprimeEtq = x.p.flgImprimeEtq,
					flgObrigatorio = x.p.flgObrigatorio,
					flgPedeAP = x.p.flgPedeAP,
					FlgTipoTerminal = x.p.FlgTipoTerminal,
					flgValidaOrigem = x.p.flgValidaOrigem,
					MontarLote = x.p.MontarLote,
					NumIP = x.p.NumIP,
					NumIPBalanca = x.p.NumIPBalanca,
					NumPosto = x.p.NumPosto,
					NumSeq = x.p.NumSeq,
					PostoBloqueado = x.p.PostoBloqueado,
					TempoBloqueio = x.p.TempoBloqueio ?? 0,
					TempoCiclo = x.p.TempoCiclo,
					TempoLeitura = x.p.TempoLeitura,
					TipoAmarra = x.p.TipoAmarra,
					TipoEtiqueta = x.p.TipoEtiqueta
				}).ToList();


				return p2;
			}
		}

		public static CBPostoDTO ObterPosto(int CodLinha, int NumPosto)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var postos = (from p in dtb_CB.tbl_CBPosto
							  where p.CodLinha == CodLinha && p.NumPosto == NumPosto
							  select p);

				var p2 = postos.FirstOrDefault();

				CBPostoDTO cBPostoDTO = new CBPostoDTO();

				AutoMapper.Mapper.Map(p2, cBPostoDTO);

				return cBPostoDTO;
			}
		}

        public static int ObterTipoPostoTablet(int codLinha, int tipoPosto)
        {
            using (DTB_CBContext dtb_CB = new DTB_CBContext())
            {
                var posto = (from p in dtb_CB.tbl_CBPosto where codLinha == p.CodLinha 
                                 && tipoPosto == p.CodTipoPosto select p.NumPosto).FirstOrDefault();

                return posto;
               
            }

        }

		public static List<CBTipoPostoDTO> ObterTiposPosto()
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var postos = (from p in dtb_CB.tbl_CBTipoPosto							  
							  select p);

				var p2 = postos.ToList();

				List<CBTipoPostoDTO> cBPostoDTO = new List<CBTipoPostoDTO>();

				AutoMapper.Mapper.Map(p2, cBPostoDTO);

				return cBPostoDTO;
			}
		}

		public static List<CBTipoPostoDTO> ObterTipoPosto()
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var postos = (from p in dtb_CB.tbl_CBPosto							 
							  select p);

				var p2 = postos.ToList();

				List<CBTipoPostoDTO> cBTipoPostoDTO = new List<CBTipoPostoDTO>();

				AutoMapper.Mapper.Map(p2, cBTipoPostoDTO);

				return cBTipoPostoDTO;
			}
		}

		public static List<CBHorarioDTO> ObterHorarios()
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var horarios = (from p in dtb_CB.tbl_CBHorario							  
							  select p);

				var p2 = horarios.ToList();

				List<CBHorarioDTO> cBHorarioDTO = new List<CBHorarioDTO>();

				AutoMapper.Mapper.Map(p2, cBHorarioDTO);

				return cBHorarioDTO;
			}
		}

		public static CBHorarioDTO ObterHorario(int idHorario)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var horario = (from p in dtb_CB.tbl_CBHorario
							  where p.idHorario == idHorario
							  select p);

				var p2 = horario.FirstOrDefault();

				CBHorarioDTO cBHorarioDTO = new CBHorarioDTO();

				AutoMapper.Mapper.Map(p2, cBHorarioDTO);

				return cBHorarioDTO;
			}
		}

		public static List<CBTipoAmarraDTO> ObterTiposAmarra()
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var tipos = (from p in dtb_CB.tbl_CBTipoAmarra
								where p.CodTipoAmarra != 6
								select p);

				var p2 = tipos.ToList();

				List<CBTipoAmarraDTO> cBTipoAmarraDTO = new List<CBTipoAmarraDTO>();

				AutoMapper.Mapper.Map(p2, cBTipoAmarraDTO);

				return cBTipoAmarraDTO;
			}
		}

		public static List<CBPerfilAmarraPostoDTO> ObterTipoAmarra(int CodLinha, int NumPosto)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var perfil = (from p in dtb_CB.tbl_CBPerfilAmarraPosto
									where p.CodLinha == CodLinha && p.NumPosto == NumPosto
								select p);

				var p2 = perfil.OrderBy(a => a.Sequencia).ToList();

				List<CBPerfilAmarraPostoDTO> cBPerfilAmarraPostoDTO = new List<CBPerfilAmarraPostoDTO>();

				AutoMapper.Mapper.Map(p2, cBPerfilAmarraPostoDTO);

				return cBPerfilAmarraPostoDTO;
			}
		}

		public static CBTipoAmarraDTO ObterTipoAmarra(int CodTipoAmarra)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var tipo = (from p in dtb_CB.tbl_CBTipoAmarra
							   where p.CodTipoAmarra == CodTipoAmarra
							   select p);

				var p2 = tipo.FirstOrDefault();

				CBTipoAmarraDTO cBTipoAmarraDTO = new CBTipoAmarraDTO();

				AutoMapper.Mapper.Map(p2, cBTipoAmarraDTO);

				return cBTipoAmarraDTO;
			}
		}

		public static List<CBPerfilMascaraDTO> ObterPerfilMascara()
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var perfil = (from p in dtb_CB.tbl_CBPerfilMascara
								join i in dtb_CB.tbl_Item on p.CodItem equals i.CodItem
							 select new { p.CodItem, p.Mascara, i.DesItem });

				var p2 = perfil.ToList()
								.Select(x => new CBPerfilMascaraDTO() { 
									CodItem = x.CodItem,
									Mascara = x.Mascara,
									DesItem = x.DesItem
								}).ToList();


				return p2;
			}
		}

		public static int? ObterTurno(int IdHorario)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var turno = (from p in dtb_CB.tbl_CBHorario
							  where p.idHorario == IdHorario
							  select p.Turno);
				
				return turno.FirstOrDefault();				
			}
		}

		public static int ObterTamanhoLote(int IdLinha)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var tamLote = (from p in dtb_CB.tbl_CBLoteConfig
							 where p.idLinha == IdLinha
							 select p.Qtde);

				if(tamLote == null)
					return 0;
				else 
					return (int)tamLote.FirstOrDefault();
			}
		}

        public static CBLoteConfigDTO ObterLoteConfig(int IdLinha)
        {
            using (DTB_CBContext dtb_CB = new DTB_CBContext())
            {
                var lote = (from p in dtb_CB.tbl_CBLoteConfig
                               where p.idLinha == IdLinha
                               select p).FirstOrDefault();

                CBLoteConfigDTO cBLoteConfigDTO = new CBLoteConfigDTO();

                AutoMapper.Mapper.Map(lote, cBLoteConfigDTO);

                return cBLoteConfigDTO;
            }
        }

		public static CBHorarioLinhaDTO ObterDatInicioHorarioLinha(int CodLinha)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var horarioLinha = (from p in dtb_CB.tbl_CBHorarioLinha
									where p.CodLinha == CodLinha
									select p).FirstOrDefault();
				
				CBHorarioLinhaDTO cBHorarioLinhaDTO = new CBHorarioLinhaDTO();

				AutoMapper.Mapper.Map(horarioLinha, cBHorarioLinhaDTO);

				return cBHorarioLinhaDTO;
			}
		}

		public static List<CBHorarioDTO> ObterHorarioLinha(int CodLinha)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{

				var horarios = (
					from p in dtb_CB.tbl_CBHorario
					join i in dtb_CB.tbl_CBHorarioLinha on p.idHorario equals i.idHorario
					where i.CodLinha == CodLinha
					select p
					);

				var p2 = horarios.ToList();

				List<CBHorarioDTO> cBHorarioDTO = new List<CBHorarioDTO>();

				AutoMapper.Mapper.Map(p2, cBHorarioDTO);

				return cBHorarioDTO;
			}
		}

		public static Boolean VerificaMascara(string CodItem)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var mascara = (from p in dtb_CB.tbl_CBPerfilMascara
								where p.CodItem == CodItem
								select p).FirstOrDefault();

				if(mascara == null)
					return false;
				else
					return true;

			}
		}

		public static String ObterMascara(string CodItem)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var mascara = (from p in dtb_CB.tbl_CBPerfilMascara
							   where p.CodItem == CodItem
							   select p.Mascara).FirstOrDefault();

				if(mascara == null)
					return "";
				else
					return mascara.ToString();

			}
		}

		public static List<CBHorarioIntervaloDTO> ObterHorarioIntervalo(int codLinha)
		{
			using (var dtb_CB = new DTB_CBContext())
			{

				var horarios = (
					from p in dtb_CB.tbl_CBHorarioIntervalo
					where p.CodLinha == codLinha
					select new CBHorarioIntervaloDTO()
					{
						CodLinha = p.CodLinha,
						HoraFim = p.HoraFim,
						HoraInicio = p.HoraInicio,
						TempoIntervalo = p.TempoIntervalo,
						TipoIntervalo = p.TipoIntervalo,
						Turno = p.Turno
					}).ToList();

				return horarios;
			}
		}

	    public static ItemDTO ObterDescricaoNE(String coditem)
	    {
            var itemDTO = new ItemDTO();
	        
	        using (DTB_CBContext dtb_SIM = new DTB_CBContext())
	        {
                var item = (from p in dtb_SIM.tbl_Item
                            where p.CodItem == coditem
                                select p).FirstOrDefault();

                AutoMapper.Mapper.Map(item, itemDTO);

                return itemDTO;

	        }
            
	    }

		public static List<CBEtiquetaDTO> ObterEtiquetas(string linha)
		{

			List<CBEtiquetaDTO> lista = null;

			
			
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{

			
				if (!string.IsNullOrWhiteSpace(linha))
				{
					var intlinha = Convert.ToInt32(linha);
					 lista = (from p in dtb_CB.tbl_CBEtiqueta
							  where p.CodLinha == intlinha
							
						select new CBEtiquetaDTO
						{
						CodLinha =  p.CodLinha,
						CaminhoEtiqueta = p.CaminhoEtiqueta,
						DescricaoEtiqueta = p.DescricaoEtiqueta,
						NomeEtiqueta = p.NomeEtiqueta,
						NomeImpressora = p.NomeImpressora,
						NumIP_Impressora = p.NumIP_Impressora,
						NumPosto = p.NumPosto,
						Porta_Impressora = p.Porta_Impressora,
						flgAtivo = p.flgAtivo

						}).ToList();


				}
				else
				{
					lista = (from p in dtb_CB.tbl_CBEtiqueta
								 select new CBEtiquetaDTO
								 {
									 CodLinha = p.CodLinha,
									 CaminhoEtiqueta = p.CaminhoEtiqueta,
									 DescricaoEtiqueta = p.DescricaoEtiqueta,
									 NomeEtiqueta = p.NomeEtiqueta,
									 NomeImpressora = p.NomeImpressora,
									 NumIP_Impressora = p.NumIP_Impressora,
									 NumPosto = p.NumPosto,
									 Porta_Impressora = p.Porta_Impressora,
									 flgAtivo = p.flgAtivo

								 }).ToList();
					
				}

			
			

				return lista;

			}


		}

        public static List<CBMaquinaDTO> ObterMaquinas()
        {
            using (DTB_CBContext dtb_CB = new DTB_CBContext())
            {
                var maquinas = (from m in dtb_CB.tbl_CBMaquina
                              where m.CodMaquina != null
                              select m);

                var m2 = maquinas.ToList();

                List<CBMaquinaDTO> cBMaquinaDTO = new List<CBMaquinaDTO>();

                AutoMapper.Mapper.Map(m2, cBMaquinaDTO);

                return cBMaquinaDTO;
            }
        }

		public static List<string> ObterFases()
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var perfil = (from p in dtb_CB.tbl_CBLinha
							  select p.Setor);

				return perfil.Distinct().ToList();
			}
		}


		#endregion

		#region Gravações

		public static Boolean InsereLinha(tbl_CBLinha linha)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				try
				{
					dtb_CB.tbl_CBLinha.Add(linha);
					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		public static Boolean InserePosto(tbl_CBPosto posto)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				try
				{
					dtb_CB.tbl_CBPosto.Add(posto);
					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		public static Boolean InsereLoteConfig(tbl_CBLoteConfig config)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				try
				{
					dtb_CB.tbl_CBLoteConfig.Add(config);
					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		public static Boolean InsereHorarioLinha(tbl_CBHorarioLinha horario)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				try
				{
					dtb_CB.tbl_CBHorarioLinha.Add(horario);
					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		public static Boolean InsereHorario(tbl_CBHorario horario)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				try
				{
					dtb_CB.tbl_CBHorario.Add(horario);
					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}		

		public static Boolean InsereMascaraEspecifica(tbl_CBPerfilMascara mascara)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				try
				{
					dtb_CB.tbl_CBPerfilMascara.Add(mascara);
					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		public static Boolean InserePerfilAmarraPosto(tbl_CBPerfilAmarraPosto perfil)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				try
				{
					dtb_CB.tbl_CBPerfilAmarraPosto.Add(perfil);
					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

        public static Boolean InsereMaquina(tbl_CBMaquina Maquina)
        {
            using (DTB_CBContext dtb_CB = new DTB_CBContext())
            {
                try
                {
                    dtb_CB.tbl_CBMaquina.Add(Maquina);
                    dtb_CB.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

		public static Boolean AtivaDesativaPosto(int codLinha, int numPosto, bool flgStatus)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				try
				{
					var posto = dtb_CB.tbl_CBPosto.FirstOrDefault(x => x.CodLinha == codLinha && x.NumPosto == numPosto);

					if (flgStatus)
					{
						posto.FlgAtivo = "S";
					}
					else
					{
						posto.FlgAtivo = "N";
					}

					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}

		}

		public static Boolean GravaIntervalo(int codLinha, int turno, string tipoIntervalo, string horaInicio, string horaFim, int tempoIntervalo)
		{

			using (var contexto = new DTB_CBContext())
			{
				var registro = contexto.tbl_CBHorarioIntervalo.FirstOrDefault(x => x.CodLinha == codLinha && x.TipoIntervalo == tipoIntervalo);

				if (registro != null)
				{
					registro.HoraFim = horaFim;
					registro.HoraInicio = horaInicio;
					registro.TempoIntervalo = tempoIntervalo;
					registro.Turno = turno;
				}
				else
				{
					var intervalo = new tbl_CBHorarioIntervalo()
					{
						CodLinha = codLinha,
						Turno = turno,
						TipoIntervalo = tipoIntervalo,
						HoraInicio = horaInicio,
						HoraFim = horaFim,
						TempoIntervalo = tempoIntervalo
					};
					contexto.tbl_CBHorarioIntervalo.Add(intervalo);
				}

				contexto.SaveChanges();

			}

			return true;

		}

		public static Boolean AlterarLinhaOrigem(int codLinha, string setor, int codLinhaOrigem)
		{

			using (var contexto = new DTB_CBContext())
			{
				var registro = contexto.tbl_CBLinha.FirstOrDefault(x => x.CodLinha == codLinha);

				if (registro != null)
				{
					registro.SetorOrigem = setor;
					registro.CodLinhaOrigem = codLinhaOrigem;
				}

				contexto.SaveChanges();

			}

			return true;

		}

		public static Boolean AlterarValidarOrigem(int codLinha, int numPosto, bool flgValidarOrigem)
		{

			using (var contexto = new DTB_CBContext())
			{
				var registro = contexto.tbl_CBPosto.FirstOrDefault(x => x.CodLinha == codLinha && x.NumPosto == numPosto);

				if (registro != null)
				{
					registro.flgValidaOrigem = flgValidarOrigem;
				}

				contexto.SaveChanges();

			}

			return true;

		}

		#region Remoção
		/* Remoção */

		public static Boolean RemoverPerfilAmarraPosto(tbl_CBPerfilAmarraPosto perfil)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				tbl_CBPerfilAmarraPosto instancia = new tbl_CBPerfilAmarraPosto();

				//instancia = dtb_CB.tbl_CBPerfilAmarraPosto.ToList(a => a.CodLinha == perfil.CodLinha && a.NumPosto == perfil.NumPosto);

				while(dtb_CB.tbl_CBPerfilAmarraPosto.FirstOrDefault(a => a.CodLinha == perfil.CodLinha && a.NumPosto == perfil.NumPosto) != null)
				{
					instancia = dtb_CB.tbl_CBPerfilAmarraPosto.FirstOrDefault(a => a.CodLinha == perfil.CodLinha && a.NumPosto == perfil.NumPosto);						
					try
					{
						dtb_CB.tbl_CBPerfilAmarraPosto.Remove(instancia);
						dtb_CB.SaveChanges();						
					}
					catch (Exception)
					{
						return false;
					}
				}

				return true;
			}
		}

		public static Boolean RemoverHorarioLinha(tbl_CBHorarioLinha horario)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				tbl_CBHorarioLinha instancia = new tbl_CBHorarioLinha();				

				while (dtb_CB.tbl_CBHorarioLinha.FirstOrDefault(a => a.idHorario == horario.idHorario && a.CodLinha == horario.CodLinha && a.Turno == horario.Turno) != null)
				{
					instancia = dtb_CB.tbl_CBHorarioLinha.FirstOrDefault(a => a.idHorario == horario.idHorario && a.CodLinha == horario.CodLinha && a.Turno == horario.Turno);
					try
					{
						dtb_CB.tbl_CBHorarioLinha.Remove(instancia);
						dtb_CB.SaveChanges();
					}
					catch (Exception)
					{
						return false;
					}
				}

				return true;
			}
		}

		public static Boolean RemoverIntervalo(int codLinha, string tipoIntervalo) {

			using (var contexto = new DTB_CBContext())
			{
				var dado = contexto.tbl_CBHorarioIntervalo.FirstOrDefault(x => x.CodLinha == codLinha && x.TipoIntervalo == tipoIntervalo);

				contexto.tbl_CBHorarioIntervalo.Remove(dado);
				contexto.SaveChanges();
				
				return true;
			}

		}

		#endregion

		#region Edição
		/* Edição */
		public static Boolean EditarLinha(tbl_CBLinha linha)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				tbl_CBLinha instancia = new tbl_CBLinha();

				instancia = dtb_CB.tbl_CBLinha.FirstOrDefault(a => a.CodLinha == linha.CodLinha);

				instancia.DesLinha = linha.DesLinha;
				instancia.DesObs = linha.DesObs;
				instancia.CodLinhaT1 = linha.CodLinhaT1;
				instancia.CodLinhaT2 = linha.CodLinhaT2;
				instancia.CodLinhaT3 = linha.CodLinhaT3;
				instancia.Familia = linha.Familia;
				instancia.Setor = linha.Setor;
				instancia.flgAtivo = linha.flgAtivo;
				instancia.CodLinhaOrigem = linha.CodLinhaOrigem;
				instancia.SetorOrigem = linha.SetorOrigem;

				try
				{					
					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		public static Boolean EditarPosto(tbl_CBPosto posto)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				tbl_CBPosto instancia = new tbl_CBPosto();

				instancia = dtb_CB.tbl_CBPosto.FirstOrDefault(a => a.CodLinha == posto.CodLinha && a.NumPosto == posto.NumPosto);

				instancia.CodTipoPosto = posto.CodTipoPosto;
				instancia.DesPosto = posto.DesPosto;
				instancia.FlgAtivo = posto.FlgAtivo;
				instancia.NumSeq = posto.NumSeq;
				instancia.flgObrigatorio = posto.flgObrigatorio;
				instancia.flgPedeAP = posto.flgPedeAP;
				instancia.flgImprimeEtq = posto.flgImprimeEtq;
				instancia.TempoLeitura = posto.TempoLeitura;
				instancia.TipoEtiqueta = posto.TipoEtiqueta;
				instancia.TempoCiclo = posto.TempoCiclo;
				instancia.FlgGargalo = posto.FlgGargalo;
                instancia.MontarLote = posto.MontarLote;
                instancia.flgValidaOrigem = posto.flgValidaOrigem;
				instancia.FlgTipoTerminal = posto.FlgTipoTerminal;

				try
				{
					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		public static Boolean EditarPosto(CBPostoDTO posto)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				tbl_CBPosto instancia = new tbl_CBPosto();

				instancia = dtb_CB.tbl_CBPosto.FirstOrDefault(a => a.CodLinha == posto.CodLinha && a.NumPosto == posto.NumPosto);

				AutoMapper.Mapper.Map(posto, instancia);

				try
				{
					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		public static Boolean EditarHorario(tbl_CBHorario horario)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				tbl_CBHorario instancia = new tbl_CBHorario();

				instancia = dtb_CB.tbl_CBHorario.FirstOrDefault(a => a.idHorario == horario.idHorario);

				instancia.idHorario = horario.idHorario;
				instancia.Turno = horario.Turno;
				instancia.HoraInicio = horario.HoraInicio;
				instancia.HoraFim = horario.HoraFim;
				instancia.flgViraDia = horario.flgViraDia;				

				try
				{
					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		public static Boolean EditarLoteConfig(tbl_CBLoteConfig config)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				tbl_CBLoteConfig instancia = dtb_CB.tbl_CBLoteConfig.FirstOrDefault(a => a.idLinha == config.idLinha);

				instancia.idLinha = config.idLinha;
				instancia.Qtde = config.Qtde;
				instancia.CodDRT = config.CodDRT;

				try
				{
					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		public static Boolean EditarMascara(tbl_CBPerfilMascara mascara)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				tbl_CBPerfilMascara instancia = new tbl_CBPerfilMascara();

				instancia = dtb_CB.tbl_CBPerfilMascara.FirstOrDefault(a => a.CodItem == mascara.CodItem);

				instancia.Mascara = mascara.Mascara;

				try
				{
					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

        public static Boolean EditarMaquina(tbl_CBMaquina Maquina)
        {
            using (DTB_CBContext dtb_CB = new DTB_CBContext())
            {
                tbl_CBMaquina instancia = dtb_CB.tbl_CBMaquina.FirstOrDefault(a => a.CodLinha == Maquina.CodLinha && a.CodMaquina == Maquina.CodMaquina);

                instancia.CodLinha = Maquina.CodLinha;
                instancia.CodMaquina = Maquina.CodMaquina;
                instancia.DesMaquina = Maquina.DesMaquina;

                try
                {
                    dtb_CB.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

		#endregion

		#endregion

		#region Tipo Amarração

		public static bool GravarTipoAmarra(CBTipoAmarraDTO dado)
		{
			try
			{

				using (var dtbCB = new DTB_CBContext())
				{
					var existe = dtbCB.tbl_CBTipoAmarra.FirstOrDefault(x => x.CodTipoAmarra == dado.CodTipoAmarra);

					if (existe != null)
					{
						existe.DesTipoAmarra = dado.DesTipoAmarra;
						existe.FlgMascaraPadrao = dado.FlgMascaraPadrao;
						existe.MascaraPadrao = dado.MascaraPadrao;
						existe.CodFam = dado.CodFam;
					}
					else
					{
						dtbCB.tbl_CBTipoAmarra.Add(new tbl_CBTipoAmarra()
						{
							CodTipoAmarra = dado.CodTipoAmarra,
							CodFam = dado.CodFam,
							DesTipoAmarra = dado.DesTipoAmarra,
							FlgMascaraPadrao = dado.FlgMascaraPadrao,
							MascaraPadrao = dado.MascaraPadrao
						});
					}

					dtbCB.SaveChanges();

					return true;
				}
			}
			catch (Exception)
			{
				return false;
			}

		}

		public static bool RemoverTipoAmarra(int codTipoAmarra)
		{
			try
			{

				using (var dtbCB = new DTB_CBContext())
				{
					var existe = dtbCB.tbl_CBTipoAmarra.FirstOrDefault(x => x.CodTipoAmarra == codTipoAmarra);

					if (existe != null)
					{
						dtbCB.tbl_CBTipoAmarra.Remove(existe);
					}
					else
					{
						return false;
					}

					dtbCB.SaveChanges();

					return true;
				}
			}
			catch (Exception)
			{
				return false;
			}

		}
		#endregion

	}
}
