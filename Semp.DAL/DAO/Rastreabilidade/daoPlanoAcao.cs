﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.Models;
using SEMP.Model.DTO;

namespace SEMP.DAL.DAO.Rastreabilidade
{
	public class daoPlanoAcao
	{

		#region Tipo de Causa

		public static List<CBPlanoAcaoTipoCausaDTO> ObterTipoCausaLista()
		{
			using (var dtbCB = new DTB_CBContext())
			{
				return dtbCB.tbl_CBPlanoAcaoTipoCausa.Select(tipo => new CBPlanoAcaoTipoCausaDTO()
				{
					CodTipoCausa = tipo.CodTipoCausa, 
					DesTipoCausa = tipo.DesTipoCausa
				}).ToList();
			}
		}

		public static CBPlanoAcaoTipoCausaDTO ObterTipoCausa(int codTipocausa)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				return dtbCB.tbl_CBPlanoAcaoTipoCausa.Select(tipo => new CBPlanoAcaoTipoCausaDTO()
				{
					CodTipoCausa = tipo.CodTipoCausa,
					DesTipoCausa = tipo.DesTipoCausa
				}).FirstOrDefault(x => x.CodTipoCausa == codTipocausa);
			}
		}

		public static void GravarTipoCausa(CBPlanoAcaoTipoCausaDTO dado)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				var existe = dtbCB.tbl_CBPlanoAcaoTipoCausa.FirstOrDefault(x => x.CodTipoCausa == dado.CodTipoCausa);

				if (existe != null)
				{
					existe.DesTipoCausa = dado.DesTipoCausa;
				}
				else
				{
					dtbCB.tbl_CBPlanoAcaoTipoCausa.Add(new tbl_CBPlanoAcaoTipoCausa() {DesTipoCausa = dado.DesTipoCausa});
				}

				dtbCB.SaveChanges();
			}
		}

		public static void RemoverTipoCausa(int codTipoCausa)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				var existe = dtbCB.tbl_CBPlanoAcaoTipoCausa.FirstOrDefault(x => x.CodTipoCausa == codTipoCausa);

				dtbCB.tbl_CBPlanoAcaoTipoCausa.Remove(existe);
				dtbCB.SaveChanges();
			}
		}

		#endregion


		#region Plano de ação

		public static List<CBPlanoAcaoDTO> ObterPlanoAcaoLista(DateTime datInicio, DateTime datFim, int? flgStatus)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				var resultado = (
					from dado in dtbCB.tbl_CBPlanoAcao
					join linha in dtbCB.tbl_CBLinha on dado.CodLinha equals linha.CodLinha
					from posto in dtbCB.tbl_CBPosto.Where(p => p.CodLinha == dado.CodLinha && p.NumPosto == dado.NumPosto).DefaultIfEmpty()
					where dado.DatPlano >= datInicio && dado.DatPlano <= datFim
					select new CBPlanoAcaoDTO()
					{
						CodPlano = dado.CodPlano,
						DesPlano = dado.DesPlano,
						CodDRT = dado.CodDRT,
						CodLinha = dado.CodLinha,
						DatConclusao = dado.DatConclusao,
						DatPlano = dado.DatPlano,
						FlgStatus = dado.FlgStatus,
						Hora = dado.Hora,
						NumPosto = dado.NumPosto,
						DesLinha = linha.DesLinha,
						DesPosto = posto.DesPosto
					});

				if (flgStatus != null)
				{
					resultado = resultado.Where(x => x.FlgStatus == flgStatus);
				}

				return resultado.ToList();
			}
		}

		public static CBPlanoAcaoDTO ObterPlanoAcao(int codPlano)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				var resultado = (from dado in dtbCB.tbl_CBPlanoAcao
								join linha in dtbCB.tbl_CBLinha on dado.CodLinha equals linha.CodLinha
								from posto in dtbCB.tbl_CBPosto.Where(p => p.CodLinha == dado.CodLinha && p.NumPosto == dado.NumPosto).DefaultIfEmpty()
								select new CBPlanoAcaoDTO()
								{
									CodPlano = dado.CodPlano,
									DesPlano = dado.DesPlano,
									CodDRT = dado.CodDRT,
									CodLinha = dado.CodLinha,
									DatConclusao = dado.DatConclusao,
									DatPlano = dado.DatPlano,
									FlgStatus = dado.FlgStatus,
									Hora = dado.Hora,
									NumPosto = dado.NumPosto,
									DesLinha = linha.DesLinha,
									DesPosto = posto.DesPosto
								}).FirstOrDefault(x => x.CodPlano == codPlano);

				return resultado;
			}
		}

		public static List<CBPlanoAcaoDTO> ObterPlanoAcao(int codLinha, DateTime datEvento)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				var resultado = (from dado in dtbCB.tbl_CBPlanoAcao
								 join linha in dtbCB.tbl_CBLinha on dado.CodLinha equals linha.CodLinha
								 from posto in dtbCB.tbl_CBPosto.Where(p => p.CodLinha == dado.CodLinha && p.NumPosto == dado.NumPosto).DefaultIfEmpty()
								 where dado.CodLinha == codLinha && dado.DatPlano == datEvento
								 select new CBPlanoAcaoDTO()
								 {
									 CodPlano = dado.CodPlano,
									 DesPlano = dado.DesPlano,
									 CodDRT = dado.CodDRT,
									 CodLinha = dado.CodLinha,
									 DatConclusao = dado.DatConclusao,
									 DatPlano = dado.DatPlano,
									 FlgStatus = dado.FlgStatus,
									 Hora = dado.Hora,
									 NumPosto = dado.NumPosto,
									 DesLinha = linha.DesLinha,
									 DesPosto = posto.DesPosto
								 }).ToList();

				return resultado;
			}
		}

		public static void GravarPlanoAcao(CBPlanoAcaoDTO dado)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				var existe = dtbCB.tbl_CBPlanoAcao.FirstOrDefault(x => x.CodPlano == dado.CodPlano);

				if (existe != null)
				{
					existe.DesPlano = dado.DesPlano;
					existe.CodDRT = dado.CodDRT;
					existe.DatConclusao = dado.DatConclusao;
					existe.DatPlano = dado.DatPlano;
					existe.FlgStatus = dado.FlgStatus;
					existe.Hora = dado.Hora;
				}
				else
				{
					dtbCB.tbl_CBPlanoAcao.Add(new tbl_CBPlanoAcao()
					{
						DesPlano = dado.DesPlano,
						CodDRT = dado.CodDRT,
						CodLinha = dado.CodLinha,
						DatConclusao = dado.DatConclusao,
						DatPlano = dado.DatPlano,
						FlgStatus = dado.FlgStatus,
						Hora = dado.Hora,
						NumPosto = dado.NumPosto
					});
				}

				dtbCB.SaveChanges();
			}
		}

		public static void RemoverPlanoAcao(int codPlano)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				var existe = dtbCB.tbl_CBPlanoAcao.FirstOrDefault(x => x.CodPlano == codPlano);

				dtbCB.tbl_CBPlanoAcao.Remove(existe);
				dtbCB.SaveChanges();
			}
		}

		#endregion

		#region Plano de ação - Causa

		public static List<CBPlanoAcaoCausaDTO> ObterPlanoAcaoCausaLista(int codPlano)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				var resultado = (
					from dado in dtbCB.tbl_CBPlanoAcaoCausa
					join causa in dtbCB.tbl_CBPlanoAcaoTipoCausa on dado.CodTipoCausa equals causa.CodTipoCausa
					where dado.CodPlano == codPlano
					select new CBPlanoAcaoCausaDTO()
					{
						CodPlano = dado.CodPlano,
						CodPlanoCausa = dado.CodPlanoCausa,
						DesCausa = dado.DesCausa,
						CodTipoCausa = dado.CodTipoCausa,
						DatInicio = dado.DatInicio,
						DatFim = dado.DatFim,
						FlgStatus = dado.FlgStatus,
						NomResponsavel = dado.NomResponsavel,
						DesTipoCausa = causa.DesTipoCausa
					});

				return resultado.ToList();
			}
		}

		public static CBPlanoAcaoCausaDTO ObterPlanoAcaoCausa(int codPlano, int codPlanoCausa)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				var resultado = (from dado in dtbCB.tbl_CBPlanoAcaoCausa
								 join causa in dtbCB.tbl_CBPlanoAcaoTipoCausa on dado.CodTipoCausa equals causa.CodTipoCausa
								 where dado.CodPlano == codPlano && dado.CodPlanoCausa == codPlanoCausa
								 select new CBPlanoAcaoCausaDTO()
								 {
									 CodPlano = dado.CodPlano,
									 CodPlanoCausa = dado.CodPlanoCausa,
									 DesCausa = dado.DesCausa,
									 CodTipoCausa = dado.CodTipoCausa,
									 DatInicio = dado.DatInicio,
									 DatFim = dado.DatFim,
									 FlgStatus = dado.FlgStatus,
									 NomResponsavel = dado.NomResponsavel,
									 DesTipoCausa = causa.DesTipoCausa
								 }).FirstOrDefault();

				return resultado;
			}
		}

		public static void GravarPlanoAcaoCausa(CBPlanoAcaoCausaDTO dado)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				var existe = dtbCB.tbl_CBPlanoAcaoCausa.FirstOrDefault(x => x.CodPlano == dado.CodPlano && x.CodPlanoCausa == dado.CodPlanoCausa);

				if (existe != null)
				{
					existe.CodPlano = dado.CodPlano;
					existe.CodPlanoCausa = dado.CodPlanoCausa;
					existe.DesCausa = dado.DesCausa;
					existe.CodTipoCausa = dado.CodTipoCausa;
					existe.DatInicio = dado.DatInicio;
					existe.DatFim = dado.DatFim;
					existe.FlgStatus = dado.FlgStatus;
					existe.NomResponsavel = dado.NomResponsavel;
				}
				else
				{
					dtbCB.tbl_CBPlanoAcaoCausa.Add(new tbl_CBPlanoAcaoCausa()
					{
						CodPlano = dado.CodPlano,
						CodPlanoCausa = dado.CodPlanoCausa,
						DesCausa = dado.DesCausa,
						CodTipoCausa = dado.CodTipoCausa,
						DatInicio = dado.DatInicio,
						DatFim = dado.DatFim,
						FlgStatus = dado.FlgStatus,
						NomResponsavel = dado.NomResponsavel
					});
				}

				dtbCB.SaveChanges();
			}
		}

		public static void RemoverPlanoAcaoCausa(int codPlano, int? codPlanoCausa)
		{
			using (var dtbCB = new DTB_CBContext())
			{

				var existe = codPlanoCausa != null ?
					dtbCB.tbl_CBPlanoAcaoCausa.FirstOrDefault(x => x.CodPlano == codPlano && x.CodPlanoCausa == codPlanoCausa) :
					dtbCB.tbl_CBPlanoAcaoCausa.FirstOrDefault(x => x.CodPlano == codPlano);

				dtbCB.tbl_CBPlanoAcaoCausa.Remove(existe);
				dtbCB.SaveChanges();
			}
		}

		#endregion

		#region Plano de ação - Acompanhamento

		public static List<CBPlanoAcaoAcompanhamentoDTO> ObterPlanoAcaoAcompanhamentoLista(int codPlano, int codPlanoCausa)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				var resultado = (
					from dado in dtbCB.tbl_CBPlanoAcaoAcompanhamento
					from causa in dtbCB.tbl_CBPlanoAcaoCausa.Where(x => x.CodPlano == dado.CodPlano && x.CodPlanoCausa == dado.CodPlanoCausa)
					where dado.CodPlano == codPlano && dado.CodPlanoCausa == codPlanoCausa
					select new CBPlanoAcaoAcompanhamentoDTO()
					{
						CodPlano = dado.CodPlano,
						CodPlanoCausa = dado.CodPlanoCausa,
						CodAcompanha = dado.CodAcompanha,
						DatAcompanha = dado.DatAcompanha,
						DesAcompanha = dado.DesAcompanha,
						DatInicio = dado.DatInicio,
						DatFim = dado.DatFim,
						FlgStatus = dado.FlgStatus,
						NomAcompanhante = dado.NomAcompanhante,
						Observacao = dado.Observacao,
						DesCausa = causa.DesCausa
					});

				return resultado.ToList();
			}
		}

		public static CBPlanoAcaoAcompanhamentoDTO ObterPlanoAcaoAcompanhamento(int codPlano, int codPlanoCausa, int codAcompanha)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				var resultado = (from dado in dtbCB.tbl_CBPlanoAcaoAcompanhamento
								 where dado.CodPlano == codPlano && dado.CodPlanoCausa == codPlanoCausa && dado.CodAcompanha == codAcompanha
								 select new CBPlanoAcaoAcompanhamentoDTO()
								 {
									 CodPlano = dado.CodPlano,
									 CodPlanoCausa = dado.CodPlanoCausa,
									 CodAcompanha = dado.CodAcompanha,
									 DatAcompanha = dado.DatAcompanha,
									 DesAcompanha = dado.DesAcompanha,
									 DatInicio = dado.DatInicio,
									 DatFim = dado.DatFim,
									 FlgStatus = dado.FlgStatus,
									 NomAcompanhante = dado.NomAcompanhante,
									 Observacao = dado.Observacao
								 }).FirstOrDefault();

				return resultado;
			}
		}

		public static void GravarPlanoAcaoAcompanhamento(CBPlanoAcaoAcompanhamentoDTO dado)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				var existe = dtbCB.tbl_CBPlanoAcaoAcompanhamento.FirstOrDefault(x => x.CodPlano == dado.CodPlano && x.CodPlanoCausa == dado.CodPlanoCausa && x.CodAcompanha == dado.CodAcompanha);

				if (existe != null)
				{
					existe.CodPlano = dado.CodPlano;
					existe.CodPlanoCausa = dado.CodPlanoCausa;
					existe.CodAcompanha = dado.CodAcompanha;
					existe.DatAcompanha = dado.DatAcompanha;
					existe.DesAcompanha = dado.DesAcompanha;
					existe.DatInicio = dado.DatInicio;
					existe.DatFim = dado.DatFim;
					existe.FlgStatus = dado.FlgStatus;
					existe.NomAcompanhante = dado.NomAcompanhante;
					existe.Observacao = dado.Observacao;
				}
				else
				{
					dtbCB.tbl_CBPlanoAcaoAcompanhamento.Add(new tbl_CBPlanoAcaoAcompanhamento()
					{
						CodPlano = dado.CodPlano,
						CodPlanoCausa = dado.CodPlanoCausa,
						//CodAcompanha = dado.CodAcompanha,
						DatAcompanha = dado.DatAcompanha,
						DesAcompanha = dado.DesAcompanha,
						DatInicio = dado.DatInicio,
						DatFim = dado.DatFim,
						FlgStatus = dado.FlgStatus,
						NomAcompanhante = dado.NomAcompanhante,
						Observacao = dado.Observacao
					});
				}

				dtbCB.SaveChanges();
			}
		}

		public static void RemoverPlanoAcaoAcompanhamento(int codPlano, int codPlanoCausa, int codAcompanha)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				var existe = dtbCB.tbl_CBPlanoAcaoAcompanhamento.FirstOrDefault(x => x.CodPlano == codPlano && x.CodPlanoCausa == codPlanoCausa && x.CodAcompanha == codAcompanha);

				dtbCB.tbl_CBPlanoAcaoAcompanhamento.Remove(existe);
				dtbCB.SaveChanges();
			}
		}

		#endregion

	}
}
