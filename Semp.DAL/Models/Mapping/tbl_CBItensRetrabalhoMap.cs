﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
	public class tbl_CBItensRetrabalhoMap : EntityTypeConfiguration<tbl_CBItensRetrabalho>
	{
		public tbl_CBItensRetrabalhoMap()
		{
			// Primary Key
			this.HasKey(t => new { t.CodRetrabalho, t.NumECB });

			// Properties
			this.Property(t => t.CodRetrabalho)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.NumECB)
				.IsRequired()
				.HasMaxLength(40);

			// Table & Column Mappings
			this.ToTable("tbl_CBItensRetrabalho");
			this.Property(t => t.CodRetrabalho).HasColumnName("CodRetrabalho");
			this.Property(t => t.NumECB).HasColumnName("NumECB");

			// Relationships
			this.HasRequired(t => t.tbl_CBRetrabalho)
				.WithMany(t => t.tbl_CBItensRetrabalho)
				.HasForeignKey(d => d.CodRetrabalho);

		}
	}
}
