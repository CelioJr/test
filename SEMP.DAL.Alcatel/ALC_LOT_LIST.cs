namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_LOT_LIST")]
    public partial class ALC_LOT_LIST
    {
        [Key]
        public int ID_LIST { get; set; }

        public DateTime? dtPallet { get; set; }

        [StringLength(255)]
        public string PACKING { get; set; }

        public int? LINE { get; set; }

        [StringLength(25)]
        public string ID_LOTE { get; set; }

        [StringLength(20)]
        public string ORDER_ID { get; set; }

        public int? RESP_PALLET { get; set; }

        [StringLength(15)]
        public string PACK_STRING { get; set; }

        [StringLength(10)]
        public string FAMILY { get; set; }

        public int? PRODUCTS_QTY { get; set; }

        public virtual ALC_SYS_USER ALC_SYS_USER { get; set; }

        public virtual ALC_VOLUME ALC_VOLUME { get; set; }

        public virtual ALC_ORDER ALC_ORDER { get; set; }
    }
}
