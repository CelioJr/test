namespace SEMP.Model.DTO
{
    public class CBPassagem_HDTO
    {
        public string NumECB { get; set; }
        public int CodLinha { get; set; }
        public int NumPosto { get; set; }
        public System.DateTime DatEvento { get; set; }
        public string CodDRT { get; set; }
    }
}
