namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ms_msihm
    {
        [Key]
        public long id_msihm { get; set; }

        public long id_ms { get; set; }

        public long id_ihm { get; set; }

        public DateTime? dthr_registro { get; set; }

        public virtual ms_ihm ms_ihm { get; set; }

        public virtual ms_ms ms_ms { get; set; }
    }
}
