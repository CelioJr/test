﻿using System;

namespace SEMP.Model.VO.SAP
{
	public class ZTPP_APONPCI
	{
		public string ID { get; set; }
		public string MBLNR { get; set; }
		public double GJAHR { get; set; }
		public int ZEILE { get; set; }
		public string WERKS { get; set; }
		public string AUFART { get; set; }
		public string AUFNR { get; set; }
		public string MATNR { get; set; }
		public string ARBPL { get; set; }
		public double YIELD { get; set; }
		public string MEINS { get; set; }
		public double NUCONF { get; set; }
		public DateTime? DTCONF { get; set; }
		public string HRCONF { get; set; }
		public string PACKNO { get; set; }
		public DateTime? DTRECEB { get; set; }
		public string HRRECEB { get; set; }

	}
}
