namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_MONTH_CONSUMPTION")]
    public partial class ALC_MONTH_CONSUMPTION
    {
        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal ID_MONTH_CONSUMPTION { get; set; }

        public double? CONDOMINIUM { get; set; }

        public DateTime? DT_CREATED { get; set; }

        public double? ENERGY_VLR { get; set; }

        [StringLength(12)]
        public string MONTH_CONSUMPTION { get; set; }

        public int? PEOPLE { get; set; }

        public double? RENTING { get; set; }

        public double? WATER_VLR { get; set; }

        public int? YEAR_CONSUMPTION { get; set; }
    }
}
