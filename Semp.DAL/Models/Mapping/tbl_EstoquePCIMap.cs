using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_EstoquePCIMap : EntityTypeConfiguration<tbl_EstoquePCI>
    {
        public tbl_EstoquePCIMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.CodItem, t.CodLocal });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(4)
                .IsUnicode(false);

            this.Property(t => t.CodItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodLocal)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(4)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_EstoquePCI");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.CodLocal).HasColumnName("CodLocal");
            this.Property(t => t.QtdEstoque).HasColumnName("QtdEstoque");
            this.Property(t => t.DatAtualizacao).HasColumnName("DatAtualizacao");
        }
    }
}
