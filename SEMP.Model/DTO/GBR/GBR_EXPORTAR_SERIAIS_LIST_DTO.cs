using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_EXPORTAR_SERIAIS_LIST_DTO
    {
        public int GBR_EXP_ID { get; set; }
        public string SERIAL { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
        public string USUARIO { get; set; }
        public string SKU { get; set; }
        public string CODETIQUETA { get; set; }
        public Nullable<int> COD_SKU { get; set; }
    }
}
