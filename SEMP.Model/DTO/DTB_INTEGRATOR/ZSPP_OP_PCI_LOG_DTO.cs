namespace SEMP.Model.DTO
{
	using System;
	using System.ComponentModel.DataAnnotations;

	public partial class ZSPP_OP_PCI_LOG_DTO
	{

        [Key]
        [StringLength(10)]
        public string EXTROW { get; set; }

        [StringLength(12)]
        public string AUFNR { get; set; }

        public decimal? YIELD { get; set; }

        [StringLength(3)]
        public string MEINS { get; set; }

        [StringLength(10)]
        public string MBLNR { get; set; }

        public DateTime? LOGDATE { get; set; }

        public TimeSpan? LOGTIME { get; set; }

        [StringLength(12)]
        public string USERNAME { get; set; }

        [StringLength(1)]
        public string TYPE { get; set; }

        [StringLength(40)]
        public string MATNR { get; set; }

        [StringLength(255)]
        public string MESSAGE { get; set; }

    }
}
