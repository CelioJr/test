using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_LIST_PALLET_DTO
    {
        public int COD_LIST_PALLET { get; set; }
        public string PACK_STRING { get; set; }
        public string PALLET_STRING { get; set; }
        public Nullable<int> COD_OP { get; set; }
        public Nullable<int> COD_SKU { get; set; }
        public Nullable<int> COD_MODELO { get; set; }
        public Nullable<int> COD_USUARIO { get; set; }
        public Nullable<bool> FECHADO { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
        public Nullable<bool> ETIQUETA_VALIDADA { get; set; }
        public Nullable<System.DateTime> ETIQUETA_DATA { get; set; }
        public Nullable<bool> CQ_VALIDADA { get; set; }
        public Nullable<decimal> PESO { get; set; }
        public Nullable<bool> REMESSA_IMPRESSA { get; set; }
        public Nullable<int> COD_REMESSA { get; set; }
    }
}
