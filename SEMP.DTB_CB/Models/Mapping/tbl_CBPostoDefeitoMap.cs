using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DTB_CB.Models.Mapping
{
    public class tbl_CBPostoDefeitoMap : EntityTypeConfiguration<tbl_CBPostoDefeito>
    {
        public tbl_CBPostoDefeitoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodLinha, t.NumPosto, t.CodDefeito });

            // Properties
            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumPosto)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodDefeito)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5);

            // Table & Column Mappings
            this.ToTable("tbl_CBPostoDefeito");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.NumPosto).HasColumnName("NumPosto");
            this.Property(t => t.CodDefeito).HasColumnName("CodDefeito");

            // Relationships
            this.HasRequired(t => t.tbl_CBPosto)
                .WithMany(t => t.tbl_CBPostoDefeito)
                .HasForeignKey(d => new { d.CodLinha, d.NumPosto });

        }
    }
}
