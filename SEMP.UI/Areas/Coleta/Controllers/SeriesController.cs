﻿using RAST.BO.Relatorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.ApontaProd;
using RAST.BO.Rastreabilidade;
using SEMP.Model.VO.Rastreabilidade.Relatorios;

namespace SEMP.Areas.Coleta.Controllers
{
    public class SeriesController : Controller
    {
        // GET: Coleta/Series
        public ActionResult Index()
        {
			return View();
        }

		public ActionResult Linhas(string datInicio)
		{

			var dIni = DateTime.Now;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);

			}

			var dados = Geral.ObterLinhaPorData(dIni);

			return View(dados);

		}

		public ActionResult Passagem(int codLinha, string datInicio, string exportar = "N")
		{

			if (!string.IsNullOrEmpty(exportar) && exportar != "N"){
				Response.AddHeader("content-disposition", "attachment; filename=Series.xls");
				Response.ContentType = "application/ms-excel";
			}

			var dIni = DateTime.Now;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			var dFim = dIni.AddDays(1).AddMinutes(-1);

			var dados = AuditorSerie.ObterPassagemLinha(dIni, dFim, codLinha);

			return View(dados);

		}


		#region Passagem de Séries entre Fases
		public ActionResult Fases() 
		{
			return View();
		
		}

		public ActionResult Embalagem(string datInicio, string datFim, string codModelo, string codFab, string numAP, string exportar = "N")
		{
			if (!string.IsNullOrEmpty(exportar) && exportar != "N")
			{
				Response.AddHeader("content-disposition", "attachment; filename=Series.xls");
				Response.ContentType = "application/ms-excel";
			}

            if (!String.IsNullOrEmpty(numAP))
            {
                var ap = Geral.ObterAP(codFab, numAP);
                if (ap != null)
                {
                    codModelo = ap.CodModelo;
                }
            }

			var estrutura = SeriesBO.ObterEstrutura(codModelo);

			var dIni = DateTime.Parse(datInicio);
			var dFim = DateTime.Parse(datFim);

			var embalagens = String.IsNullOrEmpty(numAP) ? SeriesBO.ObterListaEmbalagem(dIni, dFim, codModelo) : SeriesBO.ObterListaEmbalagemAP(codFab, numAP);

			ViewBag.Estrutura = estrutura;
			
			return View(embalagens);

		}
		#endregion

		#region Consultas

		public JsonResult ObterLinhas(string datInicio)
		{

			var dIni = DateTime.Now;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);

			}

			var dados = Geral.ObterLinhaPorData(dIni);
			var setores = dados.Select(x => x.Setor).Distinct().ToList();
			var retorno = setores.Select(x => new { Setor = x, Linhas = dados.Where(y => y.Setor == x).ToList() });

			return Json(retorno, JsonRequestBehavior.AllowGet);

		}

		public JsonResult Series(string datInicio, string datFim, int codLinha)
		{

			var dIni = DateTime.Now;

			if (!String.IsNullOrEmpty(datInicio)) {
				dIni = DateTime.Parse(datInicio);

			}

			var dFim = dIni.AddDays(1).AddMinutes(-1);

			var dados = AuditorSerie.ObterPassagemSerie(dIni, dFim, codLinha);

			return Json(dados, JsonRequestBehavior.AllowGet);
		
		}

		public JsonResult ObterEstrutura(string codModelo){

			var resultado = SeriesBO.ObterEstrutura(codModelo);

			return Json(resultado, JsonRequestBehavior.AllowGet);

		}

		#endregion
	}
}