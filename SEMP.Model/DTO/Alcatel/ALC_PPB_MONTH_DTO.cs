using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_PPB_MONTH_DTO
    {
        public decimal ID_PPB_MONTH { get; set; }
        public string FULL_SKU { get; set; }
        public Nullable<int> PPB_MONTH { get; set; }
        public Nullable<decimal> QTY { get; set; }
        public Nullable<int> PPB_YEAR { get; set; }
    }
}
