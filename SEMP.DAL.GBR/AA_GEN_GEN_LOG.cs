namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AA_GEN_GEN_LOG
    {
        [Key]
        public int GENERATOR { get; set; }

        public int? SESSION_ID { get; set; }
    }
}
