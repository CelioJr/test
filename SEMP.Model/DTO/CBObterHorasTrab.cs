﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.DTO
{
	public class CBObterHorasTrab
	{
		public int MinutosDia { get; set; }
		public int QtdMinReal { get; set; }
		public int TempoIntervalos { get; set; }
		public string Intervalo { get; set; }
	}
}
