using System;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using CodeFirstStoreFunctions;
using SEMP.DAL.Models.Mapping;
using SEMP.Model.DTO;
using SEMP.Model.VO.Rastreabilidade;
using SEMP.Model.VO.Rastreabilidade.Relatorios;

namespace SEMP.DAL.Models
{
	public partial class DTB_CBContext : DbContext
	{
		static DTB_CBContext()
		{
			Database.SetInitializer<DTB_CBContext>(null);            
		}

		#if DEBUG
		public DTB_CBContext()
			: base("Name=DTB_CBContextDEBUG")
		{
			this.Configuration.LazyLoadingEnabled = false;
		}
		#else
		public DTB_CBContext()
			: base("Name=DTB_CBContext")
		{
			this.Configuration.LazyLoadingEnabled = false;
			((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 360;
		}
		#endif
		public DbSet<tbl_CBAcao> tbl_CBAcao { get; set; }
		public DbSet<tbl_CBAcesso> tbl_CBAcesso { get; set; }
		public DbSet<tbl_CBAmarra> tbl_CBAmarra { get; set; }
		public DbSet<tbl_CBAmarra_Hist> tbl_CBAmarra_Hist { get; set; }
		public DbSet<tbl_CBBurnIn> tbl_CBBurnIn { get; set; }
		public DbSet<tbl_CBCadCausa> tbl_CBCadCausa { get; set; }
		public DbSet<tbl_CBCadDefeito> tbl_CBCadDefeito { get; set; }
		public DbSet<tbl_CBCadOrigem> tbl_CBCadOrigem { get; set; }
		public DbSet<tbl_CBCadSubDefeito> tbl_CBCadSubDefeito { get; set; }
		public DbSet<tbl_CBDefeito> tbl_CBDefeito { get; set; }
		public DbSet<tbl_CBDefeitoRastreamento> tbl_CBDefeitoRastreamento { get; set; }
		public DbSet<tbl_CBDeParaEtiqueta> tbl_CBDeParaEtiqueta { get; set; }
		public DbSet<tbl_CBDestinatarioEmail> tbl_CBDestinatarioEmail { get; set; }
		public DbSet<tbl_CBEmbalada> tbl_CBEmbalada { get; set; }
		public DbSet<tbl_CBEntradaIAC> tbl_CBEntradaIAC { get; set; }
		public DbSet<tbl_CBEtiqueta> tbl_CBEtiqueta { get; set; }
		public DbSet<tbl_CBFIFOExcecao> tbl_CBFIFOExcecao { get; set; }
		public DbSet<tbl_CBHistoricoReimpressao> tbl_CBHistoricoReimpressao { get; set; }
		public DbSet<tbl_CBHistoricoRetPosto> tbl_CBHistoricoRetPosto { get; set; }
		public DbSet<tbl_CBHorario> tbl_CBHorario { get; set; }
		public DbSet<tbl_CBHorarioIntervalo> tbl_CBHorarioIntervalo { get; set; }
		public DbSet<tbl_CBHorarioLinha> tbl_CBHorarioLinha { get; set; }
		public DbSet<tbl_CBIdentifica> tbl_CBIdentifica { get; set; }
		public DbSet<tbl_CBImpressaoLote> tbl_CBImpressaoLote { get; set; }
		public DbSet<tbl_CBItensRetrabalho> tbl_CBItensRetrabalho { get; set; }
		public DbSet<tbl_CBKitAcessorio> tbl_CBKitAcessorio { get; set; }
		public DbSet<tbl_CBLinha> tbl_CBLinha { get; set; }
		public DbSet<tbl_CBLinhaDefeitoRelatorio> tbl_CBLinhaDefeitoRelatorio { get; set; }
		public DbSet<tbl_CBLinhaObservacao> tbl_CBLinhaObservacao { get; set; }
		public DbSet<tbl_CBLinhaPostoPerfil> tbl_CBLinhaPostoPerfil { get; set; }
		public DbSet<tbl_CBLote> tbl_CBLote { get; set; }
		public DbSet<tbl_CBLoteConfig> tbl_CBLoteConfig { get; set; }
		public DbSet<tbl_CBMaquina> tbl_CBMaquina { get; set; }
		public DbSet<tbl_CBMensagens> tbl_CBMensagens { get; set; }
		public DbSet<tbl_CBModelo> tbl_CBModelo { get; set; }
		public DbSet<tbl_CBOBAInspecao> tbl_CBOBAInspecao { get; set; }
		public DbSet<tbl_CBOBATestes> tbl_CBOBATestes { get; set; }
		public DbSet<tbl_CBOBATipoStatus> tbl_CBOBATipoStatus { get; set; }
		public DbSet<tbl_CBOBARegistro> tbl_CBOBARegistro { get; set; }
		public DbSet<tbl_CBPadraoIndicador> tbl_CBPadraoIndicador { get; set; }
		public DbSet<tbl_CBPassagem> tbl_CBPassagem { get; set; }
		public DbSet<tbl_CBPassagem_H> tbl_CBPassagem_H { get; set; }
		public DbSet<tbl_CBPassagem_Hist> tbl_CBPassagem_Hist { get; set; }
		public DbSet<tbl_CBPerfilAmarra> tbl_CBPerfilAmarra { get; set; }
		public DbSet<tbl_CBPerfilAmarraPosto> tbl_CBPerfilAmarraPosto { get; set; }
		public DbSet<tbl_CBPerfilMascara> tbl_CBPerfilMascara { get; set; }
		public DbSet<tbl_CBPesoEmbalagem> tbl_CBPesoEmbalagem { get; set; }
		public DbSet<tbl_CBPlanoAcao> tbl_CBPlanoAcao { get; set; }
		public DbSet<tbl_CBPlanoAcaoAcompanhamento> tbl_CBPlanoAcaoAcompanhamento { get; set; }
		public DbSet<tbl_CBPlanoAcaoCausa> tbl_CBPlanoAcaoCausa { get; set; }
		public DbSet<tbl_CBPlanoAcaoTipoCausa> tbl_CBPlanoAcaoTipoCausa { get; set; }
		public DbSet<tbl_CBPosto> tbl_CBPosto { get; set; }
		public DbSet<tbl_CBPostoDefeito> tbl_CBPostoDefeito { get; set; }
		public DbSet<tbl_CBPostoSequencia> tbl_CBPostoSequencia { get; set; }
		public DbSet<tbl_CBQueimaInspecao> tbl_CBQueimaInspecao { get; set; }
		public DbSet<tbl_CBQueimaTestes> tbl_CBQueimaTestes { get; set; }
		public DbSet<tbl_CBQueimaTipoStatus> tbl_CBQueimaTipoStatus { get; set; }
		public DbSet<tbl_CBQueimaRegistro> tbl_CBQueimaRegistro { get; set; }
		public DbSet<tbl_CBRastLocal> tbl_CBRastLocal { get; set; }
		public DbSet<tbl_CBResumoIAC> tbl_CBResumoIAC { get; set; }
		public DbSet<tbl_CBRetrabalho> tbl_CBRetrabalho { get; set; }
		public DbSet<tbl_CBRotaRetrabalho> tbl_CBRotaRetrabalho { get; set; }
		public DbSet<tbl_CBTAFase> tbl_CBTAFase { get; set; }
		public DbSet<tbl_CBTAItem> tbl_CBTAItem { get; set; }
		public DbSet<tbl_CBTAMetas> tbl_CBTAMetas { get; set; }
		public DbSet<tbl_CBTAModelo> tbl_CBTAModelo { get; set; }
		public DbSet<tbl_CBTATempoItem> tbl_CBTATempoItem { get; set; }
		public DbSet<tbl_CBTipoAmarra> tbl_CBTipoAmarra { get; set; }
		public DbSet<tbl_CBTipoEtiqueta> tbl_CBTipoEtiqueta { get; set; }
		public DbSet<tbl_CBTipoPosto> tbl_CBTipoPosto { get; set; }
		public DbSet<tbl_CBTransfLote> tbl_CBTransfLote { get; set; }
		public DbSet<tbl_CBTrocaDRT> tbl_CBTrocaDRT { get; set; }
		public DbSet<tbl_CBTrocaItem> tbl_CBTrocaItem { get; set; }
		public DbSet<tbl_CBValidaModelo> tbl_CBValidaModelo { get; set; }
		public DbSet<tbl_Feriados> tbl_Feriados { get; set; }
		public DbSet<tbl_Item> tbl_Item { get; set; }
		public DbSet<tbl_ItemPCP> tbl_ItemPCP { get; set; }
		public DbSet<tbl_PlanoItem> tbl_PlanoItem { get; set; }
		public DbSet<tbl_RelacaoIMC_IAC> tbl_RelacaoIMC_IAC { get; set; }
		public DbSet<tbl_CBLoteItem> tbl_CBLoteItem { get; set; }
		public DbSet<tbl_ProgDiaJit> tbl_ProgDiaJit { get; set; }
		public DbSet<tbl_ProgDiaPlaca> tbl_ProgDiaPlaca { get; set; }
		public DbSet<tbl_RIFuncionario> tbl_RIFuncionario { get; set; }

		public DbSet<tbl_CBLoteStatus> tbl_CBLoteStatus { get; set; }		        

		public DbSet<viw_CBDefeitoTurno> viw_CBDefeitoTurno { get; set; }
		public DbSet<viw_CBDefeitoInspecaoIAC> viw_CBDefeitoInspecaoIAC { get; set; }
		public DbSet<viw_CBEmbaladaTurno> viw_CBEmbaladaTurno { get; set; }
		public DbSet<viw_CBPassagemTurno> viw_CBPassagemTurno { get; set; }
		public DbSet<viw_CBPerfilAmarra> viw_CBPerfilAmarra { get; set; }
		public DbSet<viw_CBPerfilAmarraPosto> viw_CBPerfilAmarraPosto { get; set; }
		public DbSet<viw_Item> viw_Item { get; set; }

		public DbSet<CBTempoModeloDTO> CBTempoModeloDTO { get; set; }
		public DbSet<ProducaoDataHora> CBBuscaProducaoHora { get; set; }
		public DbSet<ProducaoData> CBBuscaProducao { get; set; }
		public DbSet<CBTempoMedioLinha> CBTempoMedioLinha { get; set; }    
		public DbSet<DadosRecebLote> CBContagemRecebimentoLote { get; set; }
		public DbSet<CBTAItemDTO> CBTAItemDTO { get; set; }
        
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Configurations.Add(new tbl_CBAcessoMap());
			modelBuilder.Configurations.Add(new tbl_CBAcaoMap());
			modelBuilder.Configurations.Add(new tbl_CBAmarraMap());
			modelBuilder.Configurations.Add(new tbl_CBAmarra_HistMap());
			modelBuilder.Configurations.Add(new tbl_CBBurnInMap());
			modelBuilder.Configurations.Add(new tbl_CBCadCausaMap());
			modelBuilder.Configurations.Add(new tbl_CBCadDefeitoMap());
			modelBuilder.Configurations.Add(new tbl_CBCadOrigemMap());
			modelBuilder.Configurations.Add(new tbl_CBCadSubDefeitoMap());
			modelBuilder.Configurations.Add(new tbl_CBDefeitoMap());
			modelBuilder.Configurations.Add(new tbl_CBDefeitoRastreamentoMap());
			modelBuilder.Configurations.Add(new tbl_CBDeParaEtiquetaMap());
			modelBuilder.Configurations.Add(new tbl_CBDestinatarioEmailMap());
			modelBuilder.Configurations.Add(new tbl_CBEmbaladaMap());
			modelBuilder.Configurations.Add(new tbl_CBEntradaIACMap());
			modelBuilder.Configurations.Add(new tbl_CBEtiquetaMap());
			modelBuilder.Configurations.Add(new tbl_CBFIFOExcecaoMap());
			modelBuilder.Configurations.Add(new tbl_CBHistoricoReimpressaoMap());
			modelBuilder.Configurations.Add(new tbl_CBHistoricoRetPostoMap());
			modelBuilder.Configurations.Add(new tbl_CBHorarioMap());
			modelBuilder.Configurations.Add(new tbl_CBHorarioIntervaloMap());
			modelBuilder.Configurations.Add(new tbl_CBHorarioLinhaMap());
			modelBuilder.Configurations.Add(new tbl_CBIdentificaMap());
			modelBuilder.Configurations.Add(new tbl_CBImpressaoLoteMap());
			modelBuilder.Configurations.Add(new tbl_CBItensRetrabalhoMap());
			modelBuilder.Configurations.Add(new tbl_CBKitAcessorioMap());
			modelBuilder.Configurations.Add(new tbl_CBLinhaMap());
			modelBuilder.Configurations.Add(new tbl_CBLinhaDefeitoRelatorioMap());
			modelBuilder.Configurations.Add(new tbl_CBLinhaObservacaoMap());
			modelBuilder.Configurations.Add(new tbl_CBLinhaPostoPerfilMap());
			modelBuilder.Configurations.Add(new tbl_CBLoteMap());
			modelBuilder.Configurations.Add(new tbl_CBLoteConfigMap());
			modelBuilder.Configurations.Add(new tbl_CBMaquinaMap());
			modelBuilder.Configurations.Add(new tbl_CBMensagensMap());
			modelBuilder.Configurations.Add(new tbl_CBModeloMap());
			modelBuilder.Configurations.Add(new tbl_CBOBAInspecaoMap());
			modelBuilder.Configurations.Add(new tbl_CBOBARegistroMap());
			modelBuilder.Configurations.Add(new tbl_CBOBATestesMap());
			modelBuilder.Configurations.Add(new tbl_CBOBATipoStatusMap());
			modelBuilder.Configurations.Add(new tbl_CBPadraoIndicadorMap());
			modelBuilder.Configurations.Add(new tbl_CBPassagemMap());
			modelBuilder.Configurations.Add(new tbl_CBPassagem_HMap());
			modelBuilder.Configurations.Add(new tbl_CBPassagem_HistMap());
			modelBuilder.Configurations.Add(new tbl_CBPerfilAmarraMap());
			modelBuilder.Configurations.Add(new tbl_CBPerfilAmarraPostoMap());
			modelBuilder.Configurations.Add(new tbl_CBPerfilMascaraMap());
			modelBuilder.Configurations.Add(new tbl_CBPesoEmbalagemMap());
			modelBuilder.Configurations.Add(new tbl_CBPlanoAcaoMap());
			modelBuilder.Configurations.Add(new tbl_CBPlanoAcaoAcompanhamentoMap());
			modelBuilder.Configurations.Add(new tbl_CBPlanoAcaoCausaMap());
			modelBuilder.Configurations.Add(new tbl_CBPlanoAcaoTipoCausaMap());
			modelBuilder.Configurations.Add(new tbl_CBPostoMap());
			modelBuilder.Configurations.Add(new tbl_CBPostoDefeitoMap());
			modelBuilder.Configurations.Add(new tbl_CBPostoSequenciaMap());
			modelBuilder.Configurations.Add(new tbl_CBQueimaInspecaoMap());
			modelBuilder.Configurations.Add(new tbl_CBQueimaRegistroMap());
			modelBuilder.Configurations.Add(new tbl_CBQueimaTestesMap());
			modelBuilder.Configurations.Add(new tbl_CBQueimaTipoStatusMap());
			modelBuilder.Configurations.Add(new tbl_CBRastLocalMap());
			modelBuilder.Configurations.Add(new tbl_CBResumoIACMap());
			modelBuilder.Configurations.Add(new tbl_CBRetrabalhoMap());
			modelBuilder.Configurations.Add(new tbl_CBRotaRetrabalhoMap());
			modelBuilder.Configurations.Add(new tbl_CBTAFaseMap());
			modelBuilder.Configurations.Add(new tbl_CBTAItemMap());
			modelBuilder.Configurations.Add(new tbl_CBTAMetasMap());
			modelBuilder.Configurations.Add(new tbl_CBTAModeloMap());
			modelBuilder.Configurations.Add(new tbl_CBTATempoItemMap());
			modelBuilder.Configurations.Add(new tbl_CBTipoAmarraMap());
			modelBuilder.Configurations.Add(new tbl_CBTipoEtiquetaMap());
			modelBuilder.Configurations.Add(new tbl_CBTipoPostoMap());
			modelBuilder.Configurations.Add(new tbl_CBTransfLoteMap());
			modelBuilder.Configurations.Add(new tbl_CBTrocaDRTMap());
			modelBuilder.Configurations.Add(new tbl_CBTrocaItemMap());
			modelBuilder.Configurations.Add(new tbl_CBValidaModeloMap());
			modelBuilder.Configurations.Add(new tbl_FeriadosMap());
			modelBuilder.Configurations.Add(new tbl_ItemMap());
			modelBuilder.Configurations.Add(new tbl_ItemPCPMap());
			modelBuilder.Configurations.Add(new tbl_PlanoItemMap());
			modelBuilder.Configurations.Add(new tbl_ProgDiaJitMap());
			modelBuilder.Configurations.Add(new tbl_ProgDiaPlacaMap());
			modelBuilder.Configurations.Add(new tbl_RelacaoIMC_IACMap());
			modelBuilder.Configurations.Add(new tbl_RIFuncionarioMap());
			modelBuilder.Configurations.Add(new tbl_CBLoteItemMap());
			modelBuilder.Configurations.Add(new tbl_CBLoteStatusMap());
			modelBuilder.Configurations.Add(new viw_CBDefeitoTurnoMap());
			modelBuilder.Configurations.Add(new viw_CBEmbaladaTurnoMap());
			modelBuilder.Configurations.Add(new viw_CBPassagemTurnoMap());
			modelBuilder.Configurations.Add(new viw_CBPerfilAmarraMap());
			modelBuilder.Configurations.Add(new viw_CBPerfilAmarraPostoMap());
			modelBuilder.Configurations.Add(new viw_ItemMap());
			modelBuilder.Configurations.Add(new viw_CBDefeitoInspecaoIACMap());

			modelBuilder.Configurations.Add(new tbl_CBTempoModeloMap());
			modelBuilder.Conventions.Add(new FunctionsConvention<DTB_CBContext>("dbo"));
		}

		public ObjectResult<ProducaoDataHora> spc_CBBuscaProducaoHora(DateTime datInicio, DateTime datFim, int codLinha)
		{
			try
			{
				var dataIni = new ObjectParameter("DatIni", datInicio);
				var dataFim = new ObjectParameter("DatFim", datFim);
				var codlinha = new ObjectParameter("CodLinha", codLinha);

				return ((IObjectContextAdapter)this).ObjectContext.
							ExecuteFunction<ProducaoDataHora>("spc_CBBuscaProducaoHora", dataIni, dataFim, codlinha);
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}


		public ObjectResult<ProducaoData> spc_CBBuscaProducaoPeriodo(DateTime datInicio, DateTime datFim, int codLinha)
		{
			try
			{
				var dataIni = new ObjectParameter("DatIni", datInicio);
				var dataFim = new ObjectParameter("DatFim", datFim);
				var codlinha = new ObjectParameter("CodLinha", codLinha);

				return ((IObjectContextAdapter)this).ObjectContext.
							ExecuteFunction<ProducaoData>("spc_CBBuscaProducaoPeriodo", dataIni, dataFim, codlinha);
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}

		public ObjectResult<CBTempoMedioLinha> spc_CBTempoMedioLinha(DateTime datInicio, DateTime datFim, int codLinha, string horaInicio, string horaFim)
		{
			try
			{
				var dataIni = new ObjectParameter("DatInicio", datInicio);
				var dataFim = new ObjectParameter("DatFim", datFim);
				var codlinha = new ObjectParameter("CodLinha", codLinha);
				var horIni = new ObjectParameter("HoraIni", horaInicio);
				var horFim = new ObjectParameter("HoraFim", horaFim);

				return ((IObjectContextAdapter)this).ObjectContext.
							ExecuteFunction<CBTempoMedioLinha>("spc_CBTempoMedioLinha", dataIni, dataFim, codlinha, horIni, horFim);
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}

		public ObjectResult<String> spc_CBGravaRecebimentoLote(String numLote, int codLinha, int numPosto, string codDRT, int codLinhaDest)
		{
			try
			{
				var pNumLote = new ObjectParameter("NumLote", numLote);
				var pCodlinha = new ObjectParameter("CodLinha", codLinha);
				var pNumPosto = new ObjectParameter("NumPosto", numPosto);
				var pCodDRT = new ObjectParameter("CodDRT", codDRT);
				var pCodLinhaDest = new ObjectParameter("CodLinhaDest", codLinhaDest);

				return ((IObjectContextAdapter)this).ObjectContext.
							ExecuteFunction<String>("spc_CBGravaRecebimentoLote", pNumLote, pCodlinha, pNumPosto, pCodDRT, pCodLinhaDest);
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}

		public ObjectResult<CBTempoMedioLinha> spc_CBTempoMedioModelo(DateTime datInicio, DateTime datFim, string codModelo, int codLinha, string horaInicio, string horaFim)
		{
			try
			{
				var dataIni = new ObjectParameter("DatInicio", datInicio);
				var dataFim = new ObjectParameter("DatFim", datFim);
				var codmodelo = new ObjectParameter("CodModelo", codModelo);
				var codlinha = new ObjectParameter("CodLinha", codLinha);
				var horIni = new ObjectParameter("HoraIni", horaInicio);
				var horFim = new ObjectParameter("HoraFim", horaFim);

				return ((IObjectContextAdapter)this).ObjectContext.
							ExecuteFunction<CBTempoMedioLinha>("spc_CBTempoMedioModelo", dataIni, dataFim, codmodelo, codlinha, horIni, horFim);
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}

		public ObjectResult<int> spc_CBContaAmarraAP(int codLinha, int numPosto, string codFab, string numAP)
		{
			try
			{
				var pCodlinha = new ObjectParameter("CodLinha", codLinha);
				var pNumPosto = new ObjectParameter("NumPosto", numPosto);
				var pCodFab = new ObjectParameter("CodFab", codFab);
				var pNumAP = new ObjectParameter("NumAP", numAP);

				return ((IObjectContextAdapter)this).ObjectContext.
							ExecuteFunction<int>("spc_CBContaAmarraAP", pCodlinha, pNumPosto, pCodFab, pNumAP);
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}

		public ObjectResult<int> spc_CBContaTotalCCEmbalada(string numCC)
		{
			try
			{
				var pNumCC = new ObjectParameter("NumCC", numCC);

				return ((IObjectContextAdapter)this).ObjectContext.
							ExecuteFunction<int>("spc_CBContaTotalCCEmbalada", pNumCC);
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}

		public ObjectResult<DadosRecebLote> spc_CBContagemRecebimentoLote(int CodLinha)
		{
			try
			{
				var pCodLinha = new ObjectParameter("CodLinha", CodLinha);

				return ((IObjectContextAdapter)this).ObjectContext.
							ExecuteFunction<DadosRecebLote>("spc_CBContagemRecebimentoLote", pCodLinha);
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}

		public ObjectResult<CBTAItemDTO> spc_CBTAObterResumoItem(DateTime datInicio, DateTime datFim, string codModelo)
		{
			try
			{
				var dataIni = new ObjectParameter("DatInicio", datInicio);
				var dataFim = new ObjectParameter("DatFim", datFim);
				var codmodelo = new ObjectParameter("CodModelo", codModelo);

				return ((IObjectContextAdapter)this).ObjectContext.
							ExecuteFunction<CBTAItemDTO>("spc_CBTAObterResumoItem", dataIni, dataFim, codmodelo);
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}
	}

}
