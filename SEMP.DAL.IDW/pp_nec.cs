namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_nec
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pp_nec()
        {
            pp_cp = new HashSet<pp_cp>();
            pp_neccron = new HashSet<pp_neccron>();
            pp_planec = new HashSet<pp_planec>();
        }

        [Key]
        public long id_nec { get; set; }

        [StringLength(30)]
        public string cd_nec { get; set; }

        public int? revisao { get; set; }

        public DateTime? dt_revisao { get; set; }

        public DateTime? dt_stativo { get; set; }

        public short? st_ativo { get; set; }

        [StringLength(255)]
        public string url_origem { get; set; }

        public long? id_cliente { get; set; }

        public long? id_produto { get; set; }

        public int? hr_leadtime { get; set; }

        public long? id_usrrevisao { get; set; }

        public long? id_usrstativo { get; set; }

        public short? tp_nec { get; set; }

        [StringLength(255)]
        public string nr_doc { get; set; }

        public long? id_necimpurllog { get; set; }

        public long? id_pt { get; set; }

        public virtual om_produto om_produto { get; set; }

        public virtual om_pt om_pt { get; set; }

        public virtual om_pt om_pt1 { get; set; }

        public virtual om_pt om_pt2 { get; set; }

        public virtual om_pt om_pt3 { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        public virtual pp_cliente pp_cliente { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cp> pp_cp { get; set; }

        public virtual pp_necimpurllog pp_necimpurllog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_neccron> pp_neccron { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_planec> pp_planec { get; set; }
    }
}
