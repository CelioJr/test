namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_RMB_REGISTER")]
    public partial class ALC_RMB_REGISTER
    {
        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal ID_RMB { get; set; }

        public int? QUANTITY { get; set; }

        [StringLength(255)]
        public string SERIAL { get; set; }

        [StringLength(255)]
        public string RMB_STATUS { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? TOTAL_RMB { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? RMB_COST { get; set; }

        [StringLength(10)]
        public string DEFECT { get; set; }

        [StringLength(50)]
        public string PART_NUMBER { get; set; }

        [StringLength(255)]
        public string REASON { get; set; }

        public DateTime? CREATION_DATE { get; set; }

        public DateTime? EDITION_DATE { get; set; }

        public virtual ALC_DEFECT ALC_DEFECT { get; set; }

        public virtual ALC_REPAIR_REASON ALC_REPAIR_REASON { get; set; }

        public virtual ALC_RMB_COST ALC_RMB_COST { get; set; }

        public virtual ALC_RMB_PART_NUMBER ALC_RMB_PART_NUMBER { get; set; }
    }
}
