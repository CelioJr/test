using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_ACAOPARAMETRO_DTO
    {
        public int ACO_CODIGO { get; set; }
        public string ACP_NOME { get; set; }
        public int ACT_CODIGO { get; set; }
        public virtual FR_ACAO_DTO FR_ACAO { get; set; }
        public virtual FR_ACPTIPO_DTO FR_ACPTIPO { get; set; }
    }
}
