using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_PROFILE_SUMMARY_DTO
    {
        public string PRS_CODIGO { get; set; }
        public string PRI_CODIGO { get; set; }
        public string PRS_IDENTIFIER { get; set; }
        public string PRS_PARENT_IDENTIFIER { get; set; }
        public string PRS_DESCRIPTION { get; set; }
        public Nullable<long> PRS_HITS { get; set; }
        public Nullable<long> PRS_TOTAL { get; set; }
        public Nullable<long> PRS_CHILDREN_TOTAL { get; set; }
        public Nullable<long> PRS_MAX { get; set; }
        public Nullable<long> PRS_CHILDREN_MAX { get; set; }
        public Nullable<long> PRS_MIN { get; set; }
        public Nullable<long> PRS_CHILDREN_MIN { get; set; }
        public Nullable<long> PRS_AVG { get; set; }
        public Nullable<long> PRS_CHILDREN_AVG { get; set; }
        public string PRS_TYPE { get; set; }
        public Nullable<decimal> PRS_SHARED_TOTAL { get; set; }
    }
}
