using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBImpressaoLote
    {
        public string NumLote { get; set; }
        public int fase { get; set; }
        public string CodDrt { get; set; }
        public bool Status { get; set; }
        public System.DateTime DataImpressao { get; set; }
        public System.Guid id { get; set; }
    }
}
