using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_GPANotaItemMap : EntityTypeConfiguration<tbl_GPANotaItem>
    {
        public tbl_GPANotaItemMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodSerie, t.NumNota, t.CodModelo, t.NumSerie });

            // Properties
            this.Property(t => t.CodSerie)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NumNota)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumSerie)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.NomUsuario)
                .HasMaxLength(30)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_GPANotaItem");
            this.Property(t => t.CodSerie).HasColumnName("CodSerie");
            this.Property(t => t.NumNota).HasColumnName("NumNota");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.NumSerie).HasColumnName("NumSerie");
            this.Property(t => t.DatRecebimento).HasColumnName("DatRecebimento");
            this.Property(t => t.DatMovimentacao).HasColumnName("DatMovimentacao");
            this.Property(t => t.CodPallet).HasColumnName("CodPallet");
            this.Property(t => t.NomUsuario).HasColumnName("NomUsuario");
        }
    }
}
