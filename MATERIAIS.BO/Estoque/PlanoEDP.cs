﻿using SEMP.DAL.DAO.Materiais;
using SEMP.Model.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MATERIAIS.BO.Estoque
{
	public class PlanoEDP
	{

		public static PlanoIACDTO ObterPlanoIAC(string codModelo, string numRevisao)
		{
			return daoPlanoEDP.ObterPlanoIAC(codModelo, numRevisao);

		}

		public static List<PlanoIACDTO> ObterPlanosIAC(string codModelo, string numRevisao)
		{
			return daoPlanoEDP.ObterPlanosIAC(codModelo, numRevisao);

		}

		public static List<PlanoIACDTO> ObterPlanosIAC(string codModelo)
		{
			return daoPlanoEDP.ObterPlanosIAC(codModelo);

		}

		public static List<PlanoIACItemDTO> ObterPlanoIACItem(string codModelo, string numRevisao)
		{
			if (!string.IsNullOrEmpty(numRevisao))
			{
				return daoPlanoEDP.ObterPlanoIACItem(codModelo, numRevisao);
			}
			else
			{
				return daoPlanoEDP.ObterPlanoIACItem(codModelo);
			}

		}

		public static List<PlanoIACItemDTO> ObterPlanoIACItem(string codModelo)
		{
			return daoPlanoEDP.ObterPlanoIACItem(codModelo);

		}

		public static List<PlanoEtapaDTO> ObterPlanoEtapa(string codModelo, string numRevisao)
		{
			return daoPlanoEDP.ObterPlanoEtapa(codModelo, numRevisao);
		}

	}
}
