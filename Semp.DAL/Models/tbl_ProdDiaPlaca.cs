using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_ProdDiaPlaca
    {
        public string CodFab { get; set; }
        public string CodPlaca { get; set; }
        public System.DateTime DatProducao { get; set; }
        public string CodLinha { get; set; }
        public string CodProcesso { get; set; }
        public decimal? QtdProduzida { get; set; }
        public System.Guid rowguid { get; set; }
        public Nullable<decimal> QtdApontada { get; set; }
        public Nullable<decimal> QtdDivida { get; set; }
        public string FlagTP { get; set; }
        public int NumTurno { get; set; }
    }
}
