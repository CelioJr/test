namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_procarhom
    {
        [Key]
        public long id_procarhom { get; set; }

        public long? id_procedimento { get; set; }

        public short? tp_homologacao { get; set; }

        public long? id_cargo { get; set; }

        public virtual om_cargo om_cargo { get; set; }

        public virtual dw_procedimento dw_procedimento { get; set; }
    }
}
