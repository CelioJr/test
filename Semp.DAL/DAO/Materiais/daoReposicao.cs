﻿using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper.Internal;
using SEMP.DAL.Models;
using SEMP.Model.DTO;
using SEMP.Model.VO.Materiais;
using SEMP.Model;

namespace SEMP.DAL.DAO.Materiais
{
	public class daoReposicao
	{

		public static List<PainelReposicao> ObterDadosPainel()
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var retorno = new List<PainelReposicao>();

				var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
				var dFim = dIni.AddDays(1).AddMinutes(-1);
				dIni = dIni.AddDays(-dIni.Day + 1);

				var query = (from ent in dtbSim.tbl_EntMov
							 join entitem in dtbSim.tbl_EntMovItem on ent.NumRomaneio equals entitem.NumRomaneio
							 from entaprov in dtbSim.tbl_EntMovAprov.Where(x => x.NumRomaneio == ent.NumRomaneio).DefaultIfEmpty()
							 where ent.DatGerac >= dIni && ent.DatGerac <= dFim && ent.CodStatus != "C" && ent.CodOrigem == "803"
							 group new { ent, entitem } by new { ent.NumRomaneio, ent.DatGerac, ent.CodStatus, StatusAprov = entaprov.CodStatus} into grupo
							 select new
							 {
								 NumRomaneio = grupo.Key.NumRomaneio,
								 DatGeracao = grupo.Key.DatGerac,
								 Status = grupo.Key.CodStatus,
								 StatusAprov = grupo.Key.StatusAprov,
								 QtdItens = grupo.Count(),
								 QtdPagos = 0,
								 QtdCritico = grupo.Count(x => x.entitem.FlgCritico == "S"),
								 QtdDias = SqlFunctions.DateDiff("day", grupo.Key.DatGerac, SqlFunctions.GetDate()) ?? 0
							 }
					);

				retorno = query.ToList().Where(x => x.StatusAprov != "R").OrderByDescending(x => x.QtdCritico).ThenBy(x => x.DatGeracao).Select(x => new PainelReposicao()
				{
					NumRomaneio = x.NumRomaneio,
					DatGeracao = x.DatGeracao.ToShortDateString() + " " + x.DatGeracao.ToShortTimeString(),
					Status = (x.Status == "R") ? "RECEBIDO" :
								(x.StatusAprov == "A") ? "APROVADO" :
								(x.Status == "E") ? "GERADO" :
								(x.Status == "C") ? "CANCELADO" : "",
					StatusAprov = (x.StatusAprov == "C") ? "" :
									(x.StatusAprov == null) ? "AGUARDANDO" :
									(x.StatusAprov == "A") ? "APROVADO" : "REPROVADO",
					QtdItens = x.QtdItens,
					QtdPagos = x.QtdPagos,
					QtdDias = x.QtdDias,
					QtdCritico = x.QtdCritico
				}).ToList();


				var moviment = (from ent in dtbSim.tbl_EntMov
								join entitem in dtbSim.tbl_EntMovItem on ent.NumRomaneio equals entitem.NumRomaneio
								from mov in dtbSim.tbl_Movimentacao.Where(x =>
									x.CodItem == entitem.CodItem &&
									x.CodFab == ent.CodFab &&
									x.DocExterno.Substring(2, 6) == ent.NumRomaneio.ToString()).ToList()
								where mov.DatTra >= dIni && mov.DatTra <= dFim &&
										mov.CodLocal == "703" && mov.CodTra == "280" &&
										ent.DatGerac >= dIni && ent.DatGerac <= dFim && ent.CodStatus != "C"
								group entitem by ent.NumRomaneio
									into grupo
									select new
									{
										NumRomaneio = grupo.Key,
										QtdPagos = grupo.Count()
									});
									
				moviment.ToList().ForEach(x =>
				{
					var dado = retorno.FirstOrDefault(y => y.NumRomaneio == x.NumRomaneio);
					if (dado != null)
					{
						dado.QtdPagos = x.QtdPagos;
					}
				});
				
				return retorno.Where(x => x.QtdItens != x.QtdPagos).ToList();

			}

		}

		public static List<RelReposicaoDTO> ObterReposicao(DateTime datIni, DateTime datFim, string status, int fase)
		{
			using (var dtb_SIM = new DTB_SIMContext())
			{
				dtb_SIM.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

				string codFase = "803";
				string codEntreposto = "703";				

				if (fase == SEMP.Model.Constantes.FASE_MF)
				{
					codFase = "803";
					codEntreposto = "703";
				}
				else if (fase == SEMP.Model.Constantes.FASE_IMC)
				{
					codFase = "802";
					codEntreposto = "702";
				}
				
				datFim = datFim.AddDays(1);
				var datReferencia = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

				var consulta = (from movItem in dtb_SIM.tbl_EntMovItem
								join desc in dtb_SIM.tbl_Item on movItem.CodItem equals desc.CodItem
								join descModelo in dtb_SIM.tbl_Item on movItem.CodModelo equals descModelo.CodItem								
								from estoque in dtb_SIM.tbl_Estoque.Where(x => x.CodItem == movItem.CodItem && x.CodLocal == "007" && x.CodFab == Constantes.CodFab02).DefaultIfEmpty()
								join entMov in dtb_SIM.tbl_EntMov on movItem.NumRomaneio equals entMov.NumRomaneio
								from mov in
									dtb_SIM.tbl_Movimentacao.Where(
										  x => x.DocExterno.Substring(x.DocExterno.Length - 5, x.DocExterno.Length).Equals(movItem.NumRomaneio.ToString())
											   && x.CodItem == movItem.CodItem && x.CodTra.Equals("280") && x.CodLocal.Equals(codEntreposto)
											   && x.DatTra >= datIni.Date && x.DatTra <= datFim.Date).DefaultIfEmpty()
								from aprov in dtb_SIM.tbl_EntMovAprov.Where(x => x.NumRomaneio == movItem.NumRomaneio).DefaultIfEmpty()								  
								from entMovRec in dtb_SIM.tbl_EntMovRec.Where(x => x.NumRomaneio == movItem.NumRomaneio && x.CodItem == movItem.CodItem).DefaultIfEmpty()
								//from apitem in dtb_SIM.tbl_LSAAPItem.Where(x => x.CodItem == movItem.CodItem).DefaultIfEmpty()
								//from ap in dtb_SIM.tbl_LSAAP.Where(x => x.CodFab == apitem.CodFab && x.NumAP == apitem.NumAP).OrderBy(x => x.NumAP).FirstOrDefault()									
                                where entMov.DatGerac >= datIni.Date && entMov.DatGerac <= datFim.Date && entMov.CodOrigem == codFase && entMov.CodTipo == "R" 
								//&& estoque.CodLocal == "007" && estoque.CodFab == "02" 
								//&& ap.QtdLoteAP > ap.QtdProduzida && ap.DatReferencia == datReferencia &&
								//ap.CodFab == "02" && ap.CodProcesso == "03-00" && apitem.CodItem == movItem.CodItem &&								
								//(ap.CodStatus == "02" || ap.CodStatus == "03")
								select new RelReposicaoDTO
								{
									  Dia = entMov.DatGerac,
									  Romaneio = entMov.NumRomaneio,
									  Modelo = movItem.CodModelo.ToString() + " - " + descModelo.DesItem,
									  Item = movItem.CodItem.ToString() + " - " + desc.DesItem,
									  QtdEstoque = estoque.QtdEstoque,
									  Status = (entMov.CodStatus == "R" && aprov.DatAprov != null && mov.DatTra == null)
										  ? "RECEBIDO"
											: (aprov.CodStatus == "A" && aprov.DatAprov != null && mov.DatTra != null)
												? "PAGO"
												: (entMov.CodStatus == "E" && aprov.DatAprov == null)
												  ? "GERADO"
												  : (entMov.CodStatus == "E" && aprov.DatAprov != null)
													? "APROVADO"
													: (entMov.CodStatus == "C")
													  ? "CANCELADO" : "",
									  DatAprovacao = aprov.DatAprov,
									  DatRecebimento = entMovRec.DatReceb,
									  DatPagamento = mov.DatTra,
									  QtdItens	= movItem.QtdItem,
									  //NumAP = ap.NumAP
								}
					);
				
				var reposicoes = consulta.Distinct().OrderByDescending(x => x.Dia).ToList();


				if (!String.IsNullOrEmpty(status))
				{
					if (status.Substring(0, 1) == "*")
					{
						switch (status.Substring(1, status.Length - 1))
						{
							case("APROVACAO"):
								reposicoes = reposicoes.Where(x => x.Status == "GERADO").OrderBy(x => x.Dia).ToList();
								break;

							case("PAGAMENTO"):
								reposicoes = reposicoes.Where(x => x.DatPagamento == null && x.DatAprovacao != null).OrderBy(x => x.DatAprovacao).ToList();
								break;

							case ("RECEBIMENTO"):
								reposicoes = reposicoes.Where(x => x.DatRecebimento == null && x.DatAprovacao != null).OrderBy(x => x.DatAprovacao).ToList();
								break;
						}
					}
					else
					{
						reposicoes = reposicoes.Where(x => x.Status == status).ToList();	
					}					
				}
					

				return reposicoes;
			}
		}


		public static ConsultaApPagamento ConsultaAPPagamento(string CodItem)
		{
			using (var dtb_SIM = new DTB_SIMContext())
			{
				dtb_SIM.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

				var datReferencia = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

				var consulta = (from apitem in dtb_SIM.tbl_LSAAPItem
					join ap in dtb_SIM.tbl_LSAAP on new { apitem.CodFab, apitem.NumAP } equals new { ap.CodFab, ap.NumAP }
					join item in dtb_SIM.tbl_Item on apitem.CodItem equals item.CodItem
					join modelo in dtb_SIM.tbl_Item on ap.CodModelo equals modelo.CodItem
					where ap.QtdLoteAP > ap.QtdProduzida && ap.DatReferencia == datReferencia &&
						  (ap.CodFab == Constantes.CodFab02 || ap.CodFab == Constantes.CodFab20) && ap.CodProcesso == "03-00" && apitem.CodItem == CodItem &&
						  //apitem.QtdNecessaria > (apitem.QtdPaga??0)
						  (ap.CodStatus == "02" || ap.CodStatus == "03")
					select new ConsultaApPagamento
					{
						NumAP = ap.NumAP,
						CodItem = apitem.CodItem,
						DesItem = item.DesItem,
						CodFab = ap.CodFab,
						CodModelo = ap.CodModelo,
						DesModelo = modelo.DesItem,
						QtdNecessaria = apitem.QtdNecessaria,
						QtdProduzida = apitem.QtdProduzida,
						QtdPaga = apitem.QtdPaga
					}).ToList();

				ConsultaApPagamento numAp = consulta.OrderBy(x => x.NumAP).FirstOrDefault();

				return numAp;
			}
		}


		public static List<PainelGestao> ObterDadosGestao(int fase)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				dtbSim.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

				var retorno = new List<PainelGestao>();

				var dIni = new DateTime(2015, 07, 01);
				var dFim = DateTime.Now.AddDays(1).AddMinutes(-1);
				//var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
				//var dFim = dIni.AddDays(1).AddMinutes(-1);
				//dIni = dIni.AddDays(-dIni.Day + 1);
				

				#region recebimento
				var recebimentoQ = ObterPendRecebimento(dIni, dFim, fase)
								.GroupBy(x => x.NumRomaneio)
								.Select(y => new
								{
									NumRomaneio = y.Key,
									QtdItens = y.Count(),
									QtdProblema = y.Count(z => z.DifAtraso > 6),
									Data = y.Min(w => w.DatAprovacao),
									FlgCritico = y.Max(z => z.FlgCritico)
								})
								.ToList();

				var datReceb = recebimentoQ.Select(x => x.Data).Min();
				
					retorno.Add(new PainelGestao()
					{
						Categoria = PainelGestao.Tipo.Recebimento,
						Cor = recebimentoQ.Count(x => x.QtdProblema > 0) > 0 ? Color.Red.Name : Color.Green.Name,
						Quantidade = recebimentoQ.Sum(x => x.QtdItens),
						Setor = "Qualidade",
						DatUltimoRegistro = (recebimentoQ.Any() && datReceb != null) ? datReceb.Value.ToShortDateString() + " " + datReceb.Value.ToShortTimeString() : "",
						QtdCritico = recebimentoQ.Count(c => c.FlgCritico == "S")
					});

				#endregion

				#region aprovação
				var aprovacaoQ = ObterPendAprovacao(dIni, dFim, fase)
								.GroupBy(x => x.NumRomaneio)
								.Select(y => new
								{
									NumRomaneio = y.Key,
									QtdItens = y.Count(),
									QtdProblema = y.Count(z => z.DifAtraso > 6),
									Data = y.Min(w => w.DatGeracao),
									FlgCritico = y.Max(z => z.FlgCritico)
								})
								.ToList();

				if (aprovacaoQ.Any())
				{
					var datAprov = aprovacaoQ.Min(x => x.Data);

					retorno.Add(new PainelGestao()
					{
						Categoria = PainelGestao.Tipo.Aprovacao,
						Cor = aprovacaoQ.Count(x => x.QtdProblema > 0) > 0 ? Color.Red.Name : Color.Green.Name,
						Quantidade = aprovacaoQ.Sum(x => x.QtdItens),
						Setor = "Produção",
						DatUltimoRegistro = (aprovacaoQ.Any()) ? datAprov.ToShortDateString() + " " + datAprov.ToShortTimeString() : "",
						QtdCritico = recebimentoQ.Count(c => c.FlgCritico == "S")
					});
				}
				else
				{
					retorno.Add(new PainelGestao()
					{
						Categoria = PainelGestao.Tipo.Aprovacao,
						Cor = Color.Green.Name,
						Quantidade = 0,
						Setor = "Produção"
					});
				}

				#endregion

				#region pagamento

				var pagamentoQ = ObterPendPagamento(dIni, dFim, fase)
								.GroupBy(x => x.NumRomaneio)
								.Select(y => new
								{
									NumRomaneio = y.Key,
									QtdItens = y.Count(),
									QtdProblema = y.Count(z => z.DifAtraso > 6),
									Data = y.Min(w => w.DatAprovacao),
									FlgCritico = y.Max(z => z.FlgCritico)
								})
								.ToList();

				var datPagamento = pagamentoQ.Select(x => x.Data).Min();
					retorno.Add(new PainelGestao()
					{
						Categoria = PainelGestao.Tipo.Pagamento,
						Cor = pagamentoQ.Count(x => x.QtdProblema > 0) > 0 ? Color.Red.Name : Color.Green.Name,
						Quantidade = pagamentoQ.Sum(x => x.QtdItens),
						Setor = "Suprimentos",
						DatUltimoRegistro = (pagamentoQ.Any() && datPagamento != null) ? datPagamento.Value.ToShortDateString() + " " + datPagamento.Value.ToShortTimeString() : "",
						QtdCritico = recebimentoQ.Count(c => c.FlgCritico == "S")
					});

				#endregion

				#region laudo

				var laudoQ = ObterPendLaudo(dIni, dFim, fase)
								.GroupBy(x => x.NumRomaneio)
								.Select(y => new
								{
									NumRomaneio = y.Key,
									QtdItens = y.Count(),
									QtdProblema = y.Count(z => z.DifAtraso > 6),
									Data = y.Min(w => w.DatRecebimento),
									FlgCritico = y.Max(z => z.FlgCritico)
								})
								.ToList();

				var datLaudo = laudoQ.Select(x => x.Data).Min();
					retorno.Add(new PainelGestao()
					{
						Categoria = PainelGestao.Tipo.Laudo,
						Cor = laudoQ.Count(x => x.QtdProblema > 0) > 0 ? Color.Red.Name : Color.Green.Name,
						Quantidade = laudoQ.Sum(x => x.QtdItens),
						Setor = "Qualidade",
						DatUltimoRegistro = (laudoQ.Any() && datLaudo != null) ? datLaudo.Value.ToShortDateString() + " " + datLaudo.Value.ToShortTimeString() : "",
						QtdCritico = recebimentoQ.Count(c => c.FlgCritico == "S")
					});

				#endregion

				#region recebimento_producao

				var pagamentoQ2 = ObterPendPagamento2(dIni, dFim, fase)
					.GroupBy(x => x.NumRomaneio)
					.Select(y => new
					{
						NumRomaneio = y.Key,
						QtdItens = y.Count(),
						QtdProblema = y.Count(z => z.DifAtraso > 6),
						Data = y.Min(w => w.DatAprovacao),
						FlgCritico = y.Max(z => z.FlgCritico)
					})
					.ToList();

					var datPagamento2 = pagamentoQ2.Select(x => x.Data).Min();
					retorno.Add(new PainelGestao()
					{
						Categoria = PainelGestao.Tipo.RecebimentoProducao,
						Cor = pagamentoQ2.Count(x => x.QtdProblema > 0) > 0 ? Color.Red.Name : Color.Green.Name,
						Quantidade = pagamentoQ2.Sum(x => x.QtdItens),
						Setor = "Suprimentos",
						DatUltimoRegistro = (pagamentoQ2.Any() && datPagamento2 != null) ? datPagamento2.Value.ToShortDateString() + " " + datPagamento2.Value.ToShortTimeString() : "",
						QtdCritico = recebimentoQ.Count(c => c.FlgCritico == "S")
					});

				#endregion

				return retorno;

			}

		}

		public static List<Pendencias> ObterPendRecebimento(DateTime datInicio, DateTime datFim, int fase) 
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				dtbSim.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

				string codFase = "803";
				string codEntreposto = "703";

				if (fase == SEMP.Model.Constantes.FASE_MF)
				{
					codFase = "803";
					codEntreposto = "703";
				}
				else if (fase == SEMP.Model.Constantes.FASE_IMC)
				{
					codFase = "802";
					codEntreposto = "702";
				}

				var query = (from ent in dtbSim.tbl_EntMov
									join entitem in dtbSim.tbl_EntMovItem on ent.NumRomaneio equals entitem.NumRomaneio
									join entaprov in dtbSim.tbl_EntMovAprov on ent.NumRomaneio equals entaprov.NumRomaneio
									join item in dtbSim.tbl_Item on entitem.CodItem equals item.CodItem
									join modelo in dtbSim.tbl_Item on entitem.CodModelo equals modelo.CodItem
									from mov in dtbSim.tbl_Movimentacao.Where(x =>
										 x.CodItem == entitem.CodItem &&
										 x.CodFab == ent.CodFab &&
										 x.DatTra >= datInicio && x.DatTra <= datFim &&
										 x.DocExterno.Substring(2, 6) == ent.NumRomaneio.ToString() &&
										 x.CodLocal == codEntreposto && x.CodTra == "280").DefaultIfEmpty()
									from estoque in dtbSim.tbl_Estoque.Where(x => x.CodItem == entitem.CodItem && x.CodLocal == codFase /*&& x.CodFab == "02"*/).DefaultIfEmpty()
									let entrec = dtbSim.tbl_EntMovRec.Count(x => x.NumRomaneio == entitem.NumRomaneio && x.CodItem == entitem.CodItem && x.QtdRec == 0)
									where ent.DatGerac >= datInicio && 
											ent.DatGerac <= datFim && 
											ent.CodStatus != "C" && 
											ent.CodTipo == "R" && 
											ent.CodOrigem == codFase &&
											ent.CodStatus != "R" && 
											entaprov.CodStatus == "A" && 
											//(entitem.FlgNecReposicao == "S" || entitem.FlgNecReposicao == null) &&
											entrec == 0
											&& entitem.flgDevolvido != "S"
									orderby entaprov.DatAprov
									select new Pendencias
									{
										NumRomaneio = ent.NumRomaneio,
										CodItem = entitem.CodItem,
										DesItem = item.DesItem,
										Status = mov != null ? "PAGO" : "APROVADO",
										DatGeracao = ent.DatGerac,
										DatAprovacao = entaprov.DatAprov,
										DatPagamento = mov.DatTra,
										QtdItem = entitem.QtdItem,
										DifGeracao = SqlFunctions.DateDiff("hour", ent.DatGerac, SqlFunctions.GetDate()),
										DifAprov = SqlFunctions.DateDiff("hour", ent.DatGerac, entaprov.DatAprov),
										DifAtraso = SqlFunctions.DateDiff("hour", entaprov.DatAprov, SqlFunctions.GetDate()),
										QtdEstoque = estoque.QtdEstoque,
										CodModelo = entitem.CodModelo,
										DesModelo = modelo.DesItem,
										FlgCritico = entitem.FlgCritico
									});

				return query.ToList();

			}

		}

		public static List<Pendencias> ObterPendAprovacao(DateTime datInicio, DateTime datFim, int fase)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				string codFase = "803";			

				if (fase == SEMP.Model.Constantes.FASE_MF)
				{
					codFase = "803";
				}
				else if (fase == SEMP.Model.Constantes.FASE_IMC)
				{
					codFase = "802";
				}

				dtbSim.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

				var query = (from ent in dtbSim.tbl_EntMov
									join entitem in dtbSim.tbl_EntMovItem on ent.NumRomaneio equals entitem.NumRomaneio
									join item in dtbSim.tbl_Item on entitem.CodItem equals item.CodItem
									join modelo in dtbSim.tbl_Item on entitem.CodModelo equals modelo.CodItem
									from entaprov in dtbSim.tbl_EntMovAprov.Where(x => ent.NumRomaneio == x.NumRomaneio).DefaultIfEmpty()
									from estoque in dtbSim.tbl_Estoque.Where(x => x.CodItem == entitem.CodItem && x.CodLocal == codFase /*&& x.CodFab == "02"*/).DefaultIfEmpty()
									where ent.DatGerac >= datInicio && 
											ent.DatGerac <= datFim && 
											ent.CodStatus != "C" && 
											ent.CodTipo == "R" && ent.CodOrigem == codFase && 
											//(entitem.FlgNecReposicao == "S" || entitem.FlgNecReposicao == null) &&
											entaprov == null
									orderby ent.DatGerac
									select new Pendencias
									{
										NumRomaneio = ent.NumRomaneio,
										CodItem = entitem.CodItem,
										DesItem = item.DesItem,
										Status = "GERADO",
										DatGeracao = ent.DatGerac,
										DatAprovacao = entaprov.DatAprov,
										QtdItem = entitem.QtdItem,
										DifGeracao = SqlFunctions.DateDiff("hour", ent.DatGerac, SqlFunctions.GetDate()),
										DifAtraso = SqlFunctions.DateDiff("hour", ent.DatGerac, SqlFunctions.GetDate()),
										QtdEstoque = estoque.QtdEstoque,
										CodModelo = entitem.CodModelo,
										DesModelo = modelo.DesItem,
										FlgCritico = entitem.FlgCritico
									});

				return query.ToList();

			}

		}

		public static List<Pendencias> ObterPendPagamento(DateTime datInicio, DateTime datFim, int fase)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				dtbSim.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

				string codFase = "803";
				string codEntreposto = "703";

				if (fase == SEMP.Model.Constantes.FASE_MF)
				{
					codFase = "803";
					codEntreposto = "703";
				}
				else if (fase == SEMP.Model.Constantes.FASE_IMC)
				{
					codFase = "802";
					codEntreposto = "702";
				}

				var query = (from ent in dtbSim.tbl_EntMov
									join entitem in dtbSim.tbl_EntMovItem on ent.NumRomaneio equals entitem.NumRomaneio
									join entaprov in dtbSim.tbl_EntMovAprov on ent.NumRomaneio equals entaprov.NumRomaneio
									join item in dtbSim.tbl_Item on entitem.CodItem equals item.CodItem
									join modelo in dtbSim.tbl_Item on entitem.CodModelo equals modelo.CodItem
									from entrec in dtbSim.tbl_EntMovRec.Where(e => e.NumRomaneio == entitem.NumRomaneio && e.CodItem == entitem.CodItem).DefaultIfEmpty()
									from mov in dtbSim.tbl_Movimentacao.Where(x =>
										x.CodItem == entitem.CodItem &&
										x.CodFab == ent.CodFab &&
										x.DatTra >= datInicio && x.DatTra <= datFim &&
										x.DocExterno.Substring(2, 6) == ent.NumRomaneio.ToString() &&
										x.CodLocal == codEntreposto && x.CodTra == "280").DefaultIfEmpty()
									from estoque in dtbSim.tbl_Estoque.Where(x => x.CodItem == entitem.CodItem && x.CodLocal == "007" && x.CodFab == Constantes.CodFab02).DefaultIfEmpty()
									where ent.DatGerac >= datInicio && 
												ent.DatGerac <= datFim && 
												//ent.CodStatus != "C" && 
												ent.CodTipo == "R" && 
												ent.CodOrigem == codFase && 
												entaprov.CodStatus == "A" &&
												entitem.FlgNecReposicao == "S"  && //|| entitem.FlgNecReposicao == null) &&
												//!item.Origem.StartsWith("2") &&
                                                entitem.QtdItem < entrec.QtdPaga && mov == null
									orderby entaprov.DatAprov
									select new Pendencias
									{
										NumRomaneio = ent.NumRomaneio,
										CodItem = entitem.CodItem,
										DesItem = item.DesItem,
										Status = (entrec.QtdRec == entitem.QtdItem) ? "RECEBIDO" : "APROVADO",
										DatGeracao = ent.DatGerac,
										DatAprovacao = entaprov.DatAprov,
										DatRecebimento = entrec.DatReceb,
										DatPagamento = mov.DatTra,
										QtdItem = entitem.QtdItem,
										DifGeracao = SqlFunctions.DateDiff("hour", ent.DatGerac, SqlFunctions.GetDate()),
										DifAprov = SqlFunctions.DateDiff("hour", ent.DatGerac, entaprov.DatAprov),
										DifReceb = SqlFunctions.DateDiff("hour", entaprov.DatAprov, entrec.DatReceb),
										DifAtraso = SqlFunctions.DateDiff("hour", entaprov.DatAprov, SqlFunctions.GetDate()),
										QtdEstoque = estoque.QtdEstoque,
										CodModelo = entitem.CodModelo,
										DesModelo = modelo.DesItem,
										FlgCritico = entitem.FlgCritico
									});

				return query.ToList();

			}

		}

		public static List<Pendencias> ObterPendPagamento2(DateTime datInicio, DateTime datFim, int fase)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				dtbSim.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

				string codFase = "803";
				string codEntreposto = "703";

				if (fase == SEMP.Model.Constantes.FASE_MF)
				{
					codFase = "803";
					codEntreposto = "703";
				}
				else if (fase == SEMP.Model.Constantes.FASE_IMC)
				{
					codFase = "802";
					codEntreposto = "702";
				}

				var query = (from ent in dtbSim.tbl_EntMov
							 join entitem in dtbSim.tbl_EntMovItem on ent.NumRomaneio equals entitem.NumRomaneio
							 join entaprov in dtbSim.tbl_EntMovAprov on ent.NumRomaneio equals entaprov.NumRomaneio
							 join item in dtbSim.tbl_Item on entitem.CodItem equals item.CodItem
							 join modelo in dtbSim.tbl_Item on entitem.CodModelo equals modelo.CodItem
							 from entrec in dtbSim.tbl_EntMovRec.Where(e => e.NumRomaneio == entitem.NumRomaneio && e.CodItem == entitem.CodItem).DefaultIfEmpty()
							 from mov in dtbSim.tbl_EPIATMovimentacao.Where(x =>
								 x.CodItem == entitem.CodItem &&
								 x.CodFab == ent.CodFab &&
								 x.DatTra >= datInicio && x.DatTra <= datFim &&
								 x.DocExterno.Substring(2, 6) == ent.NumRomaneio.ToString() &&
								 x.CodLocal == codEntreposto && x.CodTra == "280" && x.QtdRecebida == null)
							 from estoque in dtbSim.tbl_Estoque.Where(x => x.CodItem == entitem.CodItem && x.CodLocal == "007" && x.CodFab == Constantes.CodFab02).DefaultIfEmpty()
							 where ent.DatGerac >= datInicio &&
										 ent.DatGerac <= datFim &&
										 ent.CodStatus != "C" &&
										 ent.CodTipo == "R" &&
										 ent.CodOrigem == codFase &&
										 entaprov.CodStatus == "A" &&
										 (entitem.FlgNecReposicao == "S") &&
										 !item.Origem.StartsWith("2")										 
							 orderby mov.DatTra
							 select new Pendencias
							 {
								 NumRomaneio = ent.NumRomaneio,
								 CodItem = entitem.CodItem,
								 DesItem = item.DesItem,
								 Status = (entrec.QtdRec == entitem.QtdItem) ? "RECEBIDO" : "APROVADO",
								 DatGeracao = ent.DatGerac,
								 DatAprovacao = entaprov.DatAprov,
								 DatRecebimento = entrec.DatReceb,
								 DatPagamento = mov.DatTra,
								 QtdItem = entitem.QtdItem,
								 DifGeracao = SqlFunctions.DateDiff("hour", ent.DatGerac, SqlFunctions.GetDate()),
								 DifAprov = SqlFunctions.DateDiff("hour", ent.DatGerac, entaprov.DatAprov),
								 DifReceb = SqlFunctions.DateDiff("hour", entaprov.DatAprov, entrec.DatReceb),
								 DifAtraso = SqlFunctions.DateDiff("hour", entaprov.DatAprov, SqlFunctions.GetDate()),
								 QtdEstoque = estoque.QtdEstoque,
								 CodModelo = entitem.CodModelo,
								 DesModelo = modelo.DesItem,
								 FlgCritico = entitem.FlgCritico
							 });

				return query.ToList();

			}

		}

		public static List<Pendencias> ObterPendLaudo(DateTime datInicio, DateTime datFim, int fase)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				dtbSim.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

				string codFase = "803";
				string codEntreposto = "703";

				if (fase == SEMP.Model.Constantes.FASE_MF)
				{
					codFase = "803";
					codEntreposto = "703";
				}
				else if (fase == SEMP.Model.Constantes.FASE_IMC)
				{
					codFase = "802";
					codEntreposto = "702";
				}

				var query = (from ent in dtbSim.tbl_EntMov
									join entitem in dtbSim.tbl_EntMovItem on ent.NumRomaneio equals entitem.NumRomaneio
									join entaprov in dtbSim.tbl_EntMovAprov on ent.NumRomaneio equals entaprov.NumRomaneio
									join item in dtbSim.tbl_Item on entitem.CodItem equals item.CodItem
									join modelo in dtbSim.tbl_Item on entitem.CodModelo equals modelo.CodItem
									join entrec in dtbSim.tbl_EntMovRec on new { entitem.NumRomaneio, entitem.CodItem } equals new { entrec.NumRomaneio, entrec.CodItem }
									from mov in dtbSim.tbl_Movimentacao.Where(x =>
										  x.CodItem == entitem.CodItem &&
										  x.CodFab == ent.CodFab &&
										  x.DocInterno2 == ("00000000" + ent.NumRomaneio.ToString()).Substring(ent.NumRomaneio.ToString().Length, 8) &&
										  (x.CodLocal == "012" || x.CodLocal == "002") && x.CodTra == "271").DefaultIfEmpty()
									from pgto in dtbSim.tbl_Movimentacao.Where(x =>
										 x.CodItem == entitem.CodItem &&
										 x.CodFab == ent.CodFab &&
										 x.DatTra >= datInicio && x.DatTra <= datFim &&
										 x.DocExterno.Substring(2, 6) == ent.NumRomaneio.ToString() &&
										 x.CodLocal == codEntreposto && x.CodTra == "280").DefaultIfEmpty()
									from estoque in dtbSim.tbl_Estoque.Where(x => x.CodItem == entitem.CodItem && x.CodLocal == "012" && x.CodFab == Constantes.CodFab02).DefaultIfEmpty()
									where ent.DatGerac >= datInicio && ent.DatGerac <= datFim && 
												ent.CodStatus != "C" && 
												ent.CodTipo == "R" && 
												ent.CodOrigem == codFase &&
												entaprov.CodStatus == "A" &&
												entrec.QtdRec > 0 &&
												//(entitem.FlgNecReposicao == "S" || entitem.FlgNecReposicao == null) &&
												mov == null
									orderby entrec.DatReceb
									select new Pendencias
									{
										NumRomaneio = ent.NumRomaneio,
										CodItem = entitem.CodItem,
										DesItem = item.DesItem,
										Status = (entrec.QtdRec == entitem.QtdItem) ? "RECEBIDO" : "APROVADO",
										DatGeracao = ent.DatGerac,
										DatAprovacao = entaprov.DatAprov,
										DatRecebimento = entrec.DatReceb,
										DatPagamento = pgto.DatTra,
										QtdItem = entitem.QtdItem,
										DifGeracao = SqlFunctions.DateDiff("hour", ent.DatGerac, SqlFunctions.GetDate()),
										DifAprov = SqlFunctions.DateDiff("hour", ent.DatGerac, entaprov.DatAprov),
										DifReceb = SqlFunctions.DateDiff("hour", entaprov.DatAprov, entrec.DatReceb),
										DifPagamento = SqlFunctions.DateDiff("hour", entrec.DatReceb, mov.DatTra),
										DifAtraso = SqlFunctions.DateDiff("hour", entrec.DatReceb, SqlFunctions.GetDate()),
										QtdEstoque = estoque.QtdEstoque,
										CodModelo = entitem.CodModelo,
										DesModelo = modelo.DesItem,
										FlgCritico = entitem.FlgCritico
									});

				return query.ToList();

			}
		}

		public static ConsultaRomaneio ObterRomaneio(int NumRomaneio)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var romaneio = (from entMovItem in dtbSim.tbl_EntMovItem
					from entMov in dtbSim.tbl_EntMov.Where(x => x.NumRomaneio == entMovItem.NumRomaneio).DefaultIfEmpty()
					from entMovAprov in dtbSim.tbl_EntMovAprov.Where(x => x.NumRomaneio == entMovItem.NumRomaneio).DefaultIfEmpty()
					from entMovRec in dtbSim.tbl_EntMovRec.Where(x => x.NumRomaneio == entMovItem.NumRomaneio).DefaultIfEmpty()
					from item in dtbSim.tbl_Item.Where(x => x.CodItem == entMovItem.CodItem).DefaultIfEmpty()
					from modelo in dtbSim.tbl_Item.Where(x => x.CodItem == entMovItem.CodModelo).DefaultIfEmpty()
					from causa in dtbSim.tbl_CnqCausa.Where(x => x.CodCausa == entMovItem.CodCausa).DefaultIfEmpty()
					from defeito in dtbSim.tbl_CnqDefeito.Where(x => x.CodDefeito == entMovItem.CodDefeito && x.flgAtivo == "S").DefaultIfEmpty()
					where entMovItem.NumRomaneio == NumRomaneio
					select new ConsultaRomaneio
					{
						NumRomaneio =  entMov.NumRomaneio,
						CodOrigem =  entMov.CodOrigem,
						CodFab = entMov.CodFab,
						CodTipo = (entMov.CodTipo == "R") ? "REJEITO" : (entMov.CodTipo == "D") ? "DEVOLUÇÃO" : entMov.CodTipo,
						NomUsuario = entMov.NomUsuario,
						NomMaquina = entMov.NomMaquina,
						CodStatus = entMov.CodStatus,
						DatGerac = entMov.DatGerac,
						QtdImpressa = entMov.QtdImpressa,
						DtUltimaImp = entMov.DtUltimaImp,
						UltImpUser = entMov.UltImpUser,
						CodItem = entMovItem.CodItem + " - " + item.DesItem,
						QtdItem = entMovItem.QtdItem,
						CodModelo = entMovItem.CodModelo + " - " + modelo.DesItem,
						CodDefeito = entMovItem.CodDefeito + " - " + defeito.DesDefeito,
						CodCausa = entMovItem.CodCausa + " - " + causa.DesCausa,
						OrigemDefeito = (entMovItem.OrigemDefeito == "F") ? "Fornecedor" : "Processo" ,
						DetalheDefeito = entMovItem.DetalheDefeito,
						NomResp = entMovItem.NomResp,
						flgDevolvido = (entMovItem.flgDevolvido == null) ? "" : entMovItem.flgDevolvido,
						DesDevMotivo = (entMovItem.DesDevMotivo == null) ? "" : entMovItem.DesDevMotivo,
						NumGrim = entMovItem.NumGrim,
						FlgCritico = (entMovItem.FlgCritico == "N") ? "Não" : "Sim",
						FlgNecReposicao = (entMovItem.FlgNecReposicao == "N") ? "Não" : "Sim",
						DatAprov = entMovAprov.DatAprov,
						UsuAprov = (entMovAprov.UsuAprov == null) ? "" : entMovAprov.UsuAprov,
						MaqAprov = entMovAprov.MaqAprov,
						CodStatusAprov = entMovAprov.CodStatus,
						CodItemRec = entMovItem.CodItem + " - " + item.DesItem,
						QtdRec = (entMovRec.QtdRec == null) ? 0 : entMovRec.QtdRec,
						DatReceb = entMovRec.DatReceb,
						NomUsuarioRec = (entMovRec.NomUsuario == null) ? "" : entMovRec.NomUsuario,
						NomMaquinaRec = entMovRec.NomMaquina,
						NumControle = entMovRec.NumControle,
						CodModeloRec = entMovItem.CodModelo + " - " + modelo.DesItem,
						CodDefeitoRec = entMovRec.CodDefeito,
						CodStatusRec = entMovRec.CodStatus,
						DocInt = entMovRec.DocInt,
						QtdLaudo = (entMovRec.QtdLaudo == null) ? 0 : entMovRec.QtdLaudo, 
						Ciente = entMovRec.Ciente,
						JustCiente = entMovRec.JustCiente,
						QtdPaga = (entMovRec.QtdPaga == null) ? 0 : entMovRec.QtdPaga
					}).FirstOrDefault();

				if (romaneio != null)
				{
					if (romaneio.DatGerac != null)
					{
						romaneio.DatGeracString = romaneio.DatGerac.ToString("dd/MM/yyyy hh:mm:ss");	
					}
					if (romaneio.DatAprov != null)
					{
						romaneio.DatAprovString = romaneio.DatAprov.Value.ToString("dd/MM/yyyy hh:mm:ss");
					}
					else if (romaneio.DatAprov == null)
					{
						romaneio.DatAprovString = "";
					}

					if (romaneio.DatReceb != null)
					{
						romaneio.DatRecebString = romaneio.DatReceb.Value.ToString("dd/MM/yyyy hh:mm:ss");
					}
					else if (romaneio.DatReceb == null)
					{
						romaneio.DatRecebString = "";
					}
				}
				
				

				return romaneio;
			}
		}


	}
}

