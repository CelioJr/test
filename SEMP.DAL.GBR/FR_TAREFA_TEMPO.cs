namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_TAREFA_TEMPO
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TRT_CODIGO { get; set; }

        public int TRF_CODIGO { get; set; }

        [Required]
        [StringLength(15)]
        public string TRT_TIPO { get; set; }

        public int TRT_VALOR { get; set; }

        public virtual FR_TAREFA FR_TAREFA { get; set; }
    }
}
