﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.ApontaProd;
using SEMP.Model.DTO;
using RAST.BO.Rastreabilidade;

namespace SEMP.Areas.Coleta.Controllers
{
	public class RastreiaController : Controller
	{
		//
		// GET: /Coleta/Rastreia/
		public ActionResult Index(string Codigo)
		{
			var rastreio = new List<CBRastreiaPCIDTO>();

			if (!String.IsNullOrEmpty(Codigo))
			{
				rastreio = RastreiaPCI.ObterRastreio(Codigo);
			}

			return View(rastreio);
		}

		public ActionResult Ficha(string Codigo)
		{
			var rastreio = new List<CBRastreiaPCIDTO>();

			if (!String.IsNullOrEmpty(Codigo))
			{
				rastreio = RastreiaPCI.ObterRastreio(Codigo);
			}

			return View(rastreio);
		}

		public ActionResult RastrearItem()
		{
			return View();
		}

		public JsonResult ObterDadosOS(string NumSerie)
		{
			var lista = DAT.ObterOSPorSerie(NumSerie.Substring(0,6), NumSerie)
							.Select(x => new {
								NumOS = x.NumOS,
								DatOS = x.DatOS.ToShortDateString(),
								CodItem = x.CodItem,
								DesItem = x.DesItem ?? "",
								DesDefExec = x.DesDefExec,
								DesDefPeca = x.DesDefPeca,
								Posicao = x.Posicao,
								Quantidade = x.Quantidade,
								DefReclamado = x.DefReclamado,
								DefConstatado = x.DefConstatado,
								NomTecnico = x.NomTecnico,
								NomStatus = x.NomStatus
							});

			return Json(lista, JsonRequestBehavior.AllowGet);
		}


	}
}
