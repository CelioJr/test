namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_ROMANIEO_DEBUG
    {
        [Key]
        public int COD_DEBUG { get; set; }

        [StringLength(50)]
        public string DANFE { get; set; }

        [StringLength(15)]
        public string COD { get; set; }

        [StringLength(15)]
        public string DOC { get; set; }

        public int? QTY { get; set; }

        public int? SERIE { get; set; }
    }
}
