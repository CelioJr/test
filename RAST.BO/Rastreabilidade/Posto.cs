﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.VO;
using SEMP.Model.VO.Rastreabilidade;
using SEMP.Model.DTO;

namespace RAST.BO.Rastreabilidade
{
    [Serializable]
	public class Posto
	{
		public DadosPosto posto = new DadosPosto();
		
		public Posto (string numIP){
			ObterPosto(numIP);
		}

		public Posto(int codLinha, int numPosto)
		{
			ObterPosto(codLinha, numPosto);
		}

		public Posto (DadosPosto p) {
			posto = p;
		}

		public void ObterPosto (string numIP) {
			
			var p = daoGeral.ObterPostoPorIP(numIP);

			AtribuiPosto(p);

		}

		public void ObterPosto(int codLinha, int numPosto)
		{

			var p = daoGeral.ObterPosto(codLinha, numPosto);

			AtribuiPosto(p);

		}

		public void AtribuiPosto(CBPostoDTO p) {

			posto.CodLinha = p.CodLinha;
			posto.NumPosto = p.NumPosto;
			posto.CodTipoPosto = p.CodTipoPosto;
			posto.DesPosto = p.DesPosto;
			posto.numIP = p.NumIP;
			posto.flgAtivo = p.FlgAtivo;
			posto.NumSeq = p.NumSeq;
			posto.flgObrigatorio = p.flgObrigatorio.ToString();
			posto.flgDRTObrig = p.flgDRTObrig.ToString();
			posto.TipoAmarra = p.TipoAmarra;
			posto.flgPedeAP = p.flgPedeAP;
			posto.flgImprimeEtq = p.flgImprimeEtq;
			posto.LinhaCliente = "";
			posto.Turno = 1;
			posto.QtdPrograma = 0;
			posto.DesAcao = p.DesAcao;
			posto.NumIPBalanca = p.NumIPBalanca;
			posto.PostoBloqueado = p.PostoBloqueado;
			posto.TempoBloqueio = p.TempoBloqueio;
			posto.flgValidaOrigem = p.flgValidaOrigem != null ? Convert.ToBoolean(p.flgValidaOrigem.ToString()) : false;
			posto.DesLinha = p.DesLinha;
			posto.Setor = p.Setor;

		}

		public PainelResumoProd ObterProducao () {
		
			PainelResumoProd producao = new PainelResumoProd(posto.CodLinha, posto.NumPosto);
			string data = DateTime.Now.ToShortDateString();
			DateTime datProducao = DateTime.Parse(data);

			//var Programa = 0;//daoGeral.ObterPrograma(posto.LinhaCliente, datProducao);

			/*if (Programa != null) {
				producao.QtdPrograma = (int)Programa.QtdProdPm;
			}*/

			return producao;

		}


	}
}
