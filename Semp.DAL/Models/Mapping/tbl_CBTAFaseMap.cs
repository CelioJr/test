using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBTAFaseMap : EntityTypeConfiguration<tbl_CBTAFase>
    {
        public tbl_CBTAFaseMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodModelo, t.DatReferencia, t.NumSerieModelo, t.NumSeriePCI, t.Fase, t.Ordem });

            // Properties
            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumSerieModelo)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.NumSeriePCI)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.Fase)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.Ordem)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("tbl_CBTAFase");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.DatReferencia).HasColumnName("DatReferencia");
            this.Property(t => t.NumSerieModelo).HasColumnName("NumSerieModelo");
            this.Property(t => t.NumSeriePCI).HasColumnName("NumSeriePCI");
            this.Property(t => t.Fase).HasColumnName("Fase");
            this.Property(t => t.Ordem).HasColumnName("Ordem");
            this.Property(t => t.TempoAtravessa).HasColumnName("TempoAtravessa");
            this.Property(t => t.TempoCorrido).HasColumnName("TempoCorrido");
            this.Property(t => t.QtdDefeitos).HasColumnName("QtdDefeitos");
            this.Property(t => t.TempoTecnico).HasColumnName("TempoTecnico");
        }
    }
}
