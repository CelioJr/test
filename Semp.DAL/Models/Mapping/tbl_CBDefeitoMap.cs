using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
	public class tbl_CBDefeitoMap : EntityTypeConfiguration<tbl_CBDefeito>
	{
		public tbl_CBDefeitoMap()
		{
			// Primary Key
			this.HasKey(t => new { t.NumECB, t.CodLinha, t.NumPosto, t.CodDefeito, t.DatEvento });

			// Properties
			this.Property(t => t.NumECB)
				.IsRequired()
				.HasMaxLength(40)
                .IsUnicode(false);

			this.Property(t => t.CodLinha)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.NumPosto)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.CodDefeito)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(5)
                .IsUnicode(false);

			this.Property(t => t.CodCausa)
				.IsFixedLength()
				.HasMaxLength(5)
                .IsUnicode(false);

			this.Property(t => t.Posicao)
				.HasMaxLength(20)
                .IsUnicode(false);

			this.Property(t => t.FlgTipo)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			this.Property(t => t.CodOrigem)
				.IsFixedLength()
				.HasMaxLength(2)
                .IsUnicode(false);

			this.Property(t => t.DesAcao)
				.HasMaxLength(200)
                .IsUnicode(false);

			this.Property(t => t.NomUsuario)
				.HasMaxLength(30)
                .IsUnicode(false);

			this.Property(t => t.NumIP)
				.HasMaxLength(15)
                .IsUnicode(false);

			this.Property(t => t.CodDRT)
				.HasMaxLength(8)
                .IsUnicode(false);

			this.Property(t => t.CodDRTRevisor)
				.HasMaxLength(8)
                .IsUnicode(false);

			this.Property(t => t.CodDefeitoTest)
				.IsFixedLength()
				.HasMaxLength(5)
                .IsUnicode(false);

			this.Property(t => t.ObsDefeito)
				.HasMaxLength(200)
                .IsUnicode(false);

			this.Property(t => t.DefIncorreto)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			this.Property(t => t.CodDRTTest)
				.HasMaxLength(8)
                .IsUnicode(false);

			this.Property(t => t.NomUsuarioTest)
				.HasMaxLength(30)
                .IsUnicode(false);

			this.Property(t => t.NumSeriePai)
				.HasMaxLength(20)
                .IsUnicode(false);

			this.Property(t => t.CodItem)
				.IsFixedLength()
				.HasMaxLength(6)
                .IsUnicode(false);

			// Table & Column Mappings
			this.ToTable("tbl_CBDefeito");
			this.Property(t => t.NumECB).HasColumnName("NumECB");
			this.Property(t => t.CodLinha).HasColumnName("CodLinha");
			this.Property(t => t.NumPosto).HasColumnName("NumPosto");
			this.Property(t => t.CodDefeito).HasColumnName("CodDefeito");
			this.Property(t => t.FlgRevisado).HasColumnName("FlgRevisado");
			this.Property(t => t.CodCausa).HasColumnName("CodCausa");
			this.Property(t => t.Posicao).HasColumnName("Posicao");
			this.Property(t => t.FlgTipo).HasColumnName("FlgTipo");
			this.Property(t => t.CodOrigem).HasColumnName("CodOrigem");
			this.Property(t => t.DesAcao).HasColumnName("DesAcao");
			this.Property(t => t.DatEvento).HasColumnName("DatEvento");
			this.Property(t => t.DatRevisado).HasColumnName("DatRevisado");
			this.Property(t => t.DatEntRevisao).HasColumnName("DatEntRevisao");
			this.Property(t => t.NomUsuario).HasColumnName("NomUsuario");
			this.Property(t => t.NumIP).HasColumnName("NumIP");
			this.Property(t => t.CodRma).HasColumnName("CodRma");
			this.Property(t => t.NumPostoRevisado).HasColumnName("NumPostoRevisado");
			this.Property(t => t.CodDRT).HasColumnName("CodDRT");
			this.Property(t => t.CodDRTRevisor).HasColumnName("CodDRTRevisor");
			this.Property(t => t.CodDefeitoTest).HasColumnName("CodDefeitoTest");
			this.Property(t => t.ObsDefeito).HasColumnName("ObsDefeito");
			this.Property(t => t.DefIncorreto).HasColumnName("DefIncorreto");
			this.Property(t => t.CodDRTTest).HasColumnName("CodDRTTest");
			this.Property(t => t.NomUsuarioTest).HasColumnName("NomUsuarioTest");
			this.Property(t => t.NumSeriePai).HasColumnName("NumSeriePai");
			this.Property(t => t.CodLinhaRevisado).HasColumnName("CodLinhaRevisado");
			this.Property(t => t.Romaneio).HasColumnName("Romaneio");
			this.Property(t => t.CodItem).HasColumnName("CodItem");
			this.Property(t => t.CodSubDefeito).HasColumnName("CodSubDefeito");

		}
	}
}
