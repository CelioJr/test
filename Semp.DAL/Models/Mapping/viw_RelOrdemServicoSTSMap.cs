using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class viw_RelOrdemServicoSTSMap : EntityTypeConfiguration<viw_RelOrdemServicoSTS>
    {
        public viw_RelOrdemServicoSTSMap()
        {
            // Primary Key
            this.HasKey(t => new { t.NumOS });

            // Properties
            this.Property(t => t.NumOS)
                .IsRequired()
				.IsFixedLength()
                .HasMaxLength(7);

            this.Property(t => t.CodModelo)
                .IsFixedLength()
                .HasMaxLength(6);

			this.Property(t => t.DesModelo)
				.HasMaxLength(40);

			this.Property(t => t.CodFam)
                .IsFixedLength()
                .HasMaxLength(6);

			this.Property(t => t.NomFam)
				.HasMaxLength(25);

			this.Property(t => t.CodEstado)
                .IsFixedLength()
                .HasMaxLength(2);

			this.Property(t => t.NomEstado)
				.HasMaxLength(20);

			this.Property(t => t.NumSerie)
				.HasMaxLength(20);

			this.Property(t => t.NumNotaFiscal)
				.IsFixedLength()
                .HasMaxLength(10);

			this.Property(t => t.CodItem)
				.IsFixedLength()
				.HasMaxLength(6);

			this.Property(t => t.DesItem)
				.HasMaxLength(40);

			this.Property(t => t.CodDefExec)
				.IsFixedLength()
				.HasMaxLength(4);

			this.Property(t => t.DesDefExec)
				.HasMaxLength(50);

			this.Property(t => t.CodDefPeca)
				.IsFixedLength()
				.HasMaxLength(4);

			this.Property(t => t.DesDefPeca)
				.HasMaxLength(50);

			this.Property(t => t.Posicao)
				.IsFixedLength()
				.HasMaxLength(8);

			this.Property(t => t.DefReclamado)
				.HasMaxLength(50);

			this.Property(t => t.DefConstatado)
				.HasMaxLength(50);

			this.Property(t => t.CodTecnico)
				.IsFixedLength()
				.HasMaxLength(2);

			this.Property(t => t.NomTecnico)
				.HasMaxLength(30);

			this.Property(t => t.FlgStatus)
				.IsFixedLength()
				.HasMaxLength(1);

			this.Property(t => t.NomStatus)
				.HasMaxLength(50);

			// Table & Column Mappings
			this.ToTable("viw_RelOrdemServicoSTS");
            this.Property(t => t.NumOS).HasColumnName("NumOS");
            this.Property(t => t.DatOS).HasColumnName("DatOS");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.DesModelo).HasColumnName("DesModelo");
            this.Property(t => t.CodFam).HasColumnName("CodFam");
            this.Property(t => t.NomFam).HasColumnName("NomFam");
            this.Property(t => t.CodEstado).HasColumnName("CodEstado");
            this.Property(t => t.NomEstado).HasColumnName("NomEstado");
            this.Property(t => t.NumSerie).HasColumnName("NumSerie");
            this.Property(t => t.NumNotaFiscal).HasColumnName("NumNotaFiscal");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.DesItem).HasColumnName("DesItem");
            this.Property(t => t.CodDefExec).HasColumnName("CodDefExec");
            this.Property(t => t.DesDefExec).HasColumnName("DesDefExec");
            this.Property(t => t.CodDefPeca).HasColumnName("CodDefPeca");
			this.Property(t => t.DesDefPeca).HasColumnName("DesDefPeca");
			this.Property(t => t.Posicao).HasColumnName("Posicao");
			this.Property(t => t.Quantidade).HasColumnName("Quantidade");
			this.Property(t => t.DefReclamado).HasColumnName("DefReclamado");
			this.Property(t => t.DefConstatado).HasColumnName("DefConstatado");
			this.Property(t => t.CodTecnico).HasColumnName("CodTecnico");
			this.Property(t => t.NomTecnico).HasColumnName("NomTecnico");
			this.Property(t => t.FlgStatus).HasColumnName("FlgStatus");
			this.Property(t => t.NomStatus).HasColumnName("NomStatus");
		}
    }
}
