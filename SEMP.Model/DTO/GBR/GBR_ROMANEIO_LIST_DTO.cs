using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_ROMANEIO_LIST_DTO
    {
        public int COD_LIST { get; set; }
        public string DOC { get; set; }
        public Nullable<int> SERIE { get; set; }
        public string COD { get; set; }
        public Nullable<int> QTD { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
    }
}
