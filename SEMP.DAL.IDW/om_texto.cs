namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_texto
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_texto()
        {
            om_obj = new HashSet<om_obj>();
        }

        [Key]
        public long id_texto { get; set; }

        [StringLength(100)]
        public string ds_texto { get; set; }

        [StringLength(100)]
        public string fonte { get; set; }

        public int? tamanhofonte { get; set; }

        [StringLength(30)]
        public string cd_texto { get; set; }

        public int? revisao { get; set; }

        public short? is_retangulo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_obj> om_obj { get; set; }
    }
}
