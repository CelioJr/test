using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
    public partial class CBModeloDTO
    {
        public string CodModelo { get; set; }
        public string Setor { get; set; }
        public string Fase { get; set; }
		[Display(Name = "Qtd. Caixa")]
        public Nullable<int> QtdCaixa { get; set; }
        public Nullable<int> QtdLote { get; set; }
        public Nullable<int> QtdAmostrasOBA { get; set; }
        public Nullable<bool> FlgControlaFifo { get; set; }
        public Nullable<decimal> TempoCiclo { get; set; }
        public string Mascara { get; set; }
        public Nullable<bool> PostoEntrada { get; set; }

        public String DesItem { get; set; }
	}
}
