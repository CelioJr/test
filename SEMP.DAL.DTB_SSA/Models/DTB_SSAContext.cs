﻿using System;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using SEMP.DAL.DTB_SSA.Models.Mapping;

namespace SEMP.DAL.DTB_SSA.Models
{
	public partial class DtbSsaContext : DbContext
	{

		static DtbSsaContext()
		{
			Database.SetInitializer<DtbSsaContext>(null);
		}

#if DEBUG
        public DtbSsaContext()
            : base("Name=DTB_SSAContextDEBUG")
        {
        }
#else
		public DtbSsaContext()
			: base("Name=DTB_SSAContext")
		{
		}
#endif

		public DbSet<tbl_Historico> TblHistorico { get; set; }
		public DbSet<tbl_HistoricoToshiba> TblHistoricoToshiba { get; set; }
		public DbSet<tbl_ItemEstoque> TblItemEstoque { get; set; }
		public DbSet<tbl_Key_Component_d> TblKeyComponentD { get; set; }
		public DbSet<tbl_Key_Component_STI> TblKeyComponentSti { get; set; }
		public DbSet<tbl_KitAcessorio> TblKitAcessorio { get; set; }
		public DbSet<tbl_MFG_Build_d> TblMfgBuildD { get; set; }
		public DbSet<tbl_VersaoEstrutura> TblVersaoEstrutura { get; set; }
		public DbSet<tbl_OF> Tbl_Of { get; set; }
		public DbSet<tbl_SGI_Imagem> TblSgiImagem { get; set; }
		public DbSet<tbl_Dispositivo> tbl_Dispositivo { get; set; }
		public DbSet<tbl_SGI_SO> tbl_SGI_SO { get; set; }
		public DbSet<tbl_DispositivoIntegrado> tbl_DispositivoIntegrado { get; set; }
		public DbSet<tbl_Item_Padrao_Serial> tbl_Item_Padrao_Serial { get; set; }
		public DbSet<tbl_HistoricoReimpressao> tbl_HistoricoReimpressao { get; set; }
		public DbSet<tbl_Modelo> tbl_Modelo { get; set; }
		public DbSet<tbl_Operador> tbl_Operador { get; set; }
		public DbSet<view_NumSerie> ViewNumSerie { get; set; }
		public DbSet<view_NumSerieEstrutura> ViewNumSerieEstrutura { get; set; }
		public DbSet<viw_STI_NE> ViwStiNe { get; set; }
		public DbSet<viw_STI_ModeloEtiqueta> viw_STI_ModeloEtiqueta { get; set; }
		public DbSet<tbl_Posto> tbl_Posto { get; set; }


		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Configurations.Add(new tbl_HistoricoMap());
			modelBuilder.Configurations.Add(new tbl_HistoricoToshibaMap());
			modelBuilder.Configurations.Add(new tbl_ItemEstoqueMap());
			modelBuilder.Configurations.Add(new tbl_Key_Component_STIMap());
			modelBuilder.Configurations.Add(new tbl_Key_Component_dMap());
			modelBuilder.Configurations.Add(new tbl_KitAcessorioMap());
			modelBuilder.Configurations.Add(new tbl_MFG_Build_dMap());
			modelBuilder.Configurations.Add(new tbl_VersaoEstruturaMap());
			modelBuilder.Configurations.Add(new tbl_SGI_ImagemMap());
			modelBuilder.Configurations.Add(new tbl_OFMap());
			modelBuilder.Configurations.Add(new tbl_DispositivoMap());
			modelBuilder.Configurations.Add(new tbl_SGI_SOMap());
			modelBuilder.Configurations.Add(new tbl_ModeloMap());
			modelBuilder.Configurations.Add(new tbl_OperadorMap());
			modelBuilder.Configurations.Add(new viw_STI_NEMap());
			modelBuilder.Configurations.Add(new view_NumSerieEstruturaMap());
			modelBuilder.Configurations.Add(new view_NumSerieMap());
			modelBuilder.Configurations.Add(new viw_STI_ModeloEtiquetaMap());
			modelBuilder.Configurations.Add(new tbl_PostoMap());
			modelBuilder.Configurations.Add(new tbl_DispositivoIntegradoMap());
			modelBuilder.Configurations.Add(new tbl_Item_Padrao_SerialMap());
			modelBuilder.Configurations.Add(new tbl_HistoricoReimpressaoMap());

		}

		public int spc_InsereComponent(string pcodFab, string pnumAP, string pcodItem, string pnumSerie, int pidDispositivo, string pcodModelo,
										string pnumECB, string pcodDRT)
		{
			try
			{
				var codFab = new ObjectParameter("CodFab", pcodFab);
				var numAP = new ObjectParameter("NumAP", pnumAP);
				var codItem = new ObjectParameter("CodItem", pcodItem);
				var numSerie = new ObjectParameter("NumSerie", pnumSerie);
				var idDispositivo = new ObjectParameter("IDDispositivo", pidDispositivo);
				var codModelo = new ObjectParameter("CodModelo", pcodModelo);
				var numECB = new ObjectParameter("NumECB", pnumECB);
				var codDRT = new ObjectParameter("CodDRT", pcodDRT);

				return ((IObjectContextAdapter)this).ObjectContext.
							ExecuteFunction("spc_InsereComponent", codFab, numAP, codItem, numSerie, idDispositivo, codModelo, numECB, codDRT);
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}
	}
}
