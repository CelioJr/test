namespace SEMP.Model.DTO
{
	using System;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;

	public partial class tbl_dados_DTO
	{
        public int ID { get; set; }

        public int ID_TAREFA { get; set; }

        public int ID_ERRO { get; set; }

        public int ID_TIPO { get; set; }

        [Column(TypeName = "xml")]
        [Required]
        public string CARGA_XML { get; set; }

        public DateTime DATAEXEC { get; set; }

        public bool STATUS { get; set; }
    }
}
