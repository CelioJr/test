namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_LIST_PACK
    {
        [Key]
        public int COD_LIST_PACK { get; set; }

        [StringLength(15)]
        public string PACK_STRING { get; set; }

        public int? COD_SKU { get; set; }

        public int? COD_MODELO { get; set; }

        [StringLength(15)]
        public string SERIAL { get; set; }

        public int? COD_USUARIO { get; set; }

        public bool? FECHADO { get; set; }

        public DateTime? DATA { get; set; }

        public bool? CONFERIDO_PACK { get; set; }

        [StringLength(15)]
        public string IP { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PESO { get; set; }

        [StringLength(5)]
        public string PPB { get; set; }

        [StringLength(20)]
        public string ORDEM { get; set; }
    }
}
