namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolpemp
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_consolpemp()
        {
            dw_consolpempoco = new HashSet<dw_consolpempoco>();
        }

        [Key]
        public long id_consolpemp { get; set; }

        public long id_consol { get; set; }

        public long id_tperdamp { get; set; }

        public decimal qt_auto_perdamp { get; set; }

        public decimal qt_manu_perdamp { get; set; }

        public virtual dw_consol dw_consol { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolpempoco> dw_consolpempoco { get; set; }

        public virtual dw_t_perdamp dw_t_perdamp { get; set; }
    }
}
