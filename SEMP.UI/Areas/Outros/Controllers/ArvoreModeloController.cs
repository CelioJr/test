﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.Relatorios;
using SEMP.Model.VO.Rastreabilidade.Relatorios;

namespace SEMP.Areas.Outros.Controllers
{
	public class ArvoreModeloController : Controller
	{
		//
		// GET: /Coleta/ArvoreModelo/

		public ActionResult Index(string codModelo)
		{

			var arvore = new List<ArvoreModelo>();

			if (!String.IsNullOrEmpty(codModelo)) {
				try
				{
					arvore = ArvoreModeloBO.ObterEstrutura(codModelo);	
				}
				catch (Exception ex)
				{
					ViewBag.erro = "Erro ao consultar: " + ex.Message;
				}

			}

			return View(arvore);

		}

	 }
}
