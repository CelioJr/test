﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.Models;
using SEMP.Model.DTO;
using SEMP.Model.VO.CNQ;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using System.Data.Entity.SqlServer;
using SEMP.Model;
using SEMP.DAL.DTB_SSA.Models;

namespace SEMP.DAL.DAO.Rastreabilidade
{
    public class daoDefeitos
    {
        //static daoDefeitos()
        //{
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<tbl_CBCadDefeito, CBCadDefeitoDTO>();
        //        cfg.CreateMap<CBCadDefeitoDTO, tbl_CBCadDefeito>();
        //        cfg.CreateMap<viw_CBDefeitoTurno, viw_CBDefeitoTurnoDTO>();
        //        cfg.CreateMap<tbl_CBDefeito, CBDefeitoDTO>();
        //        cfg.CreateMap<CBDefeitoDTO, tbl_CBDefeito>();
        //        cfg.CreateMap<tbl_CBLinhaDefeitoRelatorio, CBLinhaDefeitoRelatorioDTO>();
        //        cfg.CreateMap<CBLinhaDefeitoRelatorioDTO, tbl_CBLinhaDefeitoRelatorio>();
        //        cfg.CreateMap<tbl_CBPostoDefeito, CBPostoDefeitoDTO>();
        //        cfg.CreateMap<CBPostoDefeitoDTO, tbl_CBPostoDefeito>();
        //        cfg.CreateMap<tbl_CBCadCausa, CBCadCausaDTO>();
        //        cfg.CreateMap<CBCadCausaDTO, tbl_CBCadCausa>();
        //        cfg.CreateMap<tbl_CBCadOrigem, CBCadOrigemDTO>();
        //        cfg.CreateMap<CBCadOrigemDTO, tbl_CBCadOrigem>();
        //        cfg.CreateMap<tbl_CBCadSubDefeito, CBCadSubDefeitoDTO>();
        //        cfg.CreateMap<CBCadSubDefeitoDTO, tbl_CBCadSubDefeito>();
        //    });
        //}


        #region Consultas Defeito

        /// <summary>
        /// Obter o registro de um defeito apontado em um número de série
        /// </summary>
        /// <param name="numECB"></param>
        /// <param name="codDefeito"></param>
        /// <param name="datEvento"></param>
        /// <returns></returns>
        public static viw_CBDefeitoTurnoDTO ObterRegistroDefeito(string numECB, string codDefeito, int codLinha, int numPosto, DateTime datEvento)
        {
            using (var dtbCb = new DTB_CBContext())
            {
                var query = (from d in dtbCb.viw_CBDefeitoTurno
                             join l in dtbCb.tbl_CBLinha on d.CodLinha equals l.CodLinha
                             from def in dtbCb.tbl_CBCadDefeito.Where(x => d.CodDefeito.Replace("UM", "0").Replace("UI", "0").Replace("LM", "0").Replace("SF", "0") == x.CodDefeito).DefaultIfEmpty()
                             from linha in dtbCb.tbl_CBLinha.Where(x => d.CodLinha.Equals(x.CodLinha)).DefaultIfEmpty()
                             from posto in dtbCb.tbl_CBPosto.Where(x => d.CodLinha == x.CodLinha && d.NumPosto == x.NumPosto).DefaultIfEmpty()
                             from causa in dtbCb.tbl_CBCadCausa.Where(x => d.CodCausa == x.CodCausa).DefaultIfEmpty()
                             from origem in dtbCb.tbl_CBCadOrigem.Where(x => d.CodOrigem == x.CodOrigem).DefaultIfEmpty()
                             from modelo in dtbCb.tbl_Item.Where(x => d.NumECB.Substring(0, 6) == x.CodItem).DefaultIfEmpty()
                             from item in dtbCb.tbl_Item.Where(x => d.CodItem == x.CodItem).DefaultIfEmpty()
                             where (d.NumECB == numECB &&
                                    d.CodDefeito == codDefeito &&
                                    d.CodLinha == codLinha &&
                                    d.NumPosto == numPosto) &&
                                    (d.DatEvento.Day == datEvento.Day &&
                                        d.DatEvento.Month == datEvento.Month &&
                                        d.DatEvento.Year == datEvento.Year &&
                                        d.DatEvento.Minute == datEvento.Minute &&
                                        d.DatEvento.Hour == datEvento.Hour &&
                                        d.DatEvento.Second == datEvento.Second)
                             select new viw_CBDefeitoTurnoDTO()
                             {
                                 NumECB = d.NumECB,
                                 CodLinha = d.CodLinha,
                                 NumPosto = d.NumPosto,
                                 CodDefeito = d.CodDefeito,
                                 DatEvento = d.DatEvento,
                                 FlgRevisado = d.FlgRevisado,
                                 CodCausa = d.CodCausa,
                                 Posicao = d.Posicao,
                                 FlgTipo = d.FlgTipo,
                                 CodOrigem = d.CodOrigem,
                                 DesAcao = d.DesAcao,
                                 DatRevisado = d.DatRevisado,
                                 DatEntRevisao = d.DatEntRevisao,
                                 NomUsuario = d.NomUsuario,
                                 NumIP = d.NumIP,
                                 CodRma = d.CodRma,
                                 NumPostoRevisado = d.NumPostoRevisado,
                                 CodDRT = d.CodDRT,
                                 CodDRTRevisor = d.CodDRTRevisor,
                                 CodDefeitoTest = d.CodDefeitoTest,
                                 ObsDefeito = d.ObsDefeito,
                                 DefIncorreto = d.DefIncorreto,
                                 CodDRTTest = d.CodDRTTest,
                                 NomUsuarioTest = d.NomUsuarioTest,
                                 NumSeriePai = d.NumSeriePai,
                                 CodLinhaRevisado = d.CodLinhaRevisado,
                                 Romaneio = d.Romaneio,
                                 CodItem = d.CodItem,
                                 DatReferencia = d.DatReferencia,
                                 Turno = d.Turno,
                                 DesDefeito = def.DesDefeito,
                                 DesLinha = linha.DesLinha,
                                 DesPosto = posto.DesPosto,
                                 DesCausa = causa.DesCausa,
                                 DesOrigem = origem.DesOrigem,
                                 DesModelo = modelo.DesItem,
                                 DesPosicao = item.DesItem
                             });

                return query.FirstOrDefault();
            }
        }

        /// <summary>
        /// Método responsável por retornar lista de defeitos cadastrados.
        /// </summary>
        /// <returns></returns>
        public static List<CBCadDefeitoDTO> ObterDefeitosCadastrados()
        {

            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var defeitos = dtbCb.tbl_CBCadDefeito.ToList();

                List<CBCadDefeitoDTO> defeitosRel = new List<CBCadDefeitoDTO>();

                AutoMapper.Mapper.Map(defeitos, defeitosRel);

                return defeitosRel;
            }

        }

        public static List<viw_CBDefeitoTurnoDTO> ObterDefeitos(DateTime DatInicio, DateTime DatFim)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var defeitos = (from d in dtbCb.tbl_CBDefeito
                                join l in dtbCb.tbl_CBLinha on d.CodLinha equals l.CodLinha
                                from emb in dtbCb.tbl_CBEmbalada.Where(x => x.NumECB == d.NumECB && x.CodLinha == d.CodLinha).DefaultIfEmpty()
                                from i in dtbCb.tbl_Item.Where(x => x.CodItem == emb.CodModelo).DefaultIfEmpty()
                                from causa in dtbCb.tbl_CBCadCausa.Where(x => x.CodCausa == d.CodCausa).DefaultIfEmpty()
                                where d.DatEvento >= DatInicio && d.DatEvento <= DatFim
                                      && String.IsNullOrEmpty(d.NumSeriePai) //&& d.FlgRevisado != 2 && d.FlgRevisado != 4
                                select new viw_CBDefeitoTurnoDTO()
                                {
                                    NumECB = d.NumECB,
                                    CodLinha = d.CodLinha,
                                    NumPosto = d.NumPosto,
                                    CodDefeito = d.CodDefeito,
                                    DatEvento = d.DatEvento,
                                    FlgRevisado = d.FlgRevisado,
                                    CodCausa = d.CodCausa,
                                    Posicao = d.Posicao,
                                    FlgTipo = d.FlgTipo,
                                    CodOrigem = d.CodOrigem,
                                    DesAcao = d.DesAcao,
                                    DatRevisado = d.DatRevisado,
                                    DatEntRevisao = d.DatEntRevisao,
                                    NomUsuario = d.NomUsuario,
                                    NumIP = d.NumIP,
                                    CodRma = d.CodRma,
                                    NumPostoRevisado = d.NumPostoRevisado,
                                    CodDRT = d.CodDRT,
                                    CodDRTRevisor = d.CodDRTRevisor,
                                    CodDefeitoTest = d.CodDefeitoTest,
                                    ObsDefeito = d.ObsDefeito,
                                    DefIncorreto = d.DefIncorreto,
                                    CodDRTTest = d.CodDRTTest,
                                    NomUsuarioTest = d.NomUsuarioTest,
                                    NumSeriePai = d.NumSeriePai,
                                    CodLinhaRevisado = d.CodLinhaRevisado,
                                    Turno = 1,
                                    CodigoModelo = emb.CodModelo,
                                    Setor = l.Setor,
                                    DesModelo = i.DesItem,
                                    DesCausa = causa.DesCausa
                                });
                var retorno = defeitos.ToList();

                retorno.ForEach(x => x.DatReferencia = DateTime.Parse(x.DatEvento.ToShortDateString()));

                if (retorno.Any(x => String.IsNullOrEmpty(x.CodigoModelo)))
                {
                    var resumoEmbalagem = daoGeral.ObterResumoEmbalagem(DatInicio, DatFim);

                    retorno.Where(x => String.IsNullOrEmpty(x.CodigoModelo)).ToList().ForEach(x =>
                    {

                        var codigo = resumoEmbalagem.FirstOrDefault(y => y.NumECB == x.CodModelo);

                        if (codigo != null)
                        {
                            x.CodigoModelo = codigo.CodModelo;
                            var descricao = daoGeral.ObterDescricaoItem(codigo.CodModelo);
                            x.DesModelo = descricao;

                        }

                    });

                }

                return retorno;
            }
        }

        public static List<viw_CBDefeitoTurnoDTO> ObterDefeitos(DateTime DatInicio, DateTime DatFim, int Turno)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var defeitos = (from d in dtbCb.viw_CBDefeitoTurno
                                where d.DatReferencia >= DatInicio && d.DatReferencia <= DatFim && d.Turno == Turno
                                      && String.IsNullOrEmpty(d.NumSeriePai) && d.FlgRevisado < 2
                                select d).ToList();

                List<viw_CBDefeitoTurnoDTO> viewDTO = new List<viw_CBDefeitoTurnoDTO>();

                AutoMapper.Mapper.Map(defeitos, viewDTO);

                return viewDTO;
            }
        }

        public static List<viw_CBDefeitoTurnoDTO> ObterDefeitos(DateTime DatInicio, DateTime DatFim, int Turno, string Fabrica)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var defeitos = (from d in dtbCb.viw_CBDefeitoTurno
                                join l in dtbCb.tbl_CBLinha on d.CodLinha equals l.CodLinha
                                where d.DatReferencia >= DatInicio && d.DatReferencia <= DatFim && d.Turno == Turno &&
                                      l.Setor == Fabrica && d.FlgRevisado < 2
                                      && String.IsNullOrEmpty(d.NumSeriePai)
                                select d).ToList();

                List<viw_CBDefeitoTurnoDTO> viewDTO = new List<viw_CBDefeitoTurnoDTO>();

                AutoMapper.Mapper.Map(defeitos, viewDTO);

                return viewDTO;
            }
        }

        public static List<viw_CBDefeitoTurnoDTO> ObterDefeitos(DateTime DatInicio, DateTime DatFim, int Turno, string Fabrica, string Linha)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var defeitos = (from d in dtbCb.viw_CBDefeitoTurno
                                join l in dtbCb.tbl_CBLinha on d.CodLinha equals l.CodLinha
                                where d.DatReferencia >= DatInicio && d.DatReferencia <= DatFim && d.Turno == Turno &&
                                      l.CodLinhaT1.Trim() == Linha
                                      && String.IsNullOrEmpty(d.NumSeriePai) && d.FlgRevisado < 2
                                select d).ToList();

                List<viw_CBDefeitoTurnoDTO> viewDTO = new List<viw_CBDefeitoTurnoDTO>();

                AutoMapper.Mapper.Map(defeitos, viewDTO);

                return viewDTO;
            }
        }

        public static List<viw_CBDefeitoTurnoDTO> ObterDefeitos(DateTime DatInicio, DateTime DatFim, string CodDRT)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var defeitos = (from d in dtbCb.viw_CBDefeitoTurno
                                where d.DatReferencia >= DatInicio && d.DatReferencia <= DatFim
                                      && String.IsNullOrEmpty(d.NumSeriePai)
                                      && d.FlgRevisado < 2
                                      && d.CodDRT == CodDRT
                                select d).ToList();

                List<viw_CBDefeitoTurnoDTO> viewDTO = new List<viw_CBDefeitoTurnoDTO>();

                AutoMapper.Mapper.Map(defeitos, viewDTO);

                return viewDTO;
            }
        }

        public static List<viw_CBDefeitoTurnoDTO> ObterDefeitosRel(DateTime datInicio, DateTime datFim, int? codLinha = null, string fabrica = "", string codDRT = "", int flgPlaca = 0, string familia = "")
        {
            using (var dtbCb = new DTB_CBContext())
            using (var dtbSsa = new DtbSsaContext())
            using (var dtbSIM = new DTB_SIMContext())
            {
				var query = (from d in dtbCb.viw_CBDefeitoTurno
							 join l in dtbCb.tbl_CBLinha on d.CodLinha equals l.CodLinha
							 from def in dtbCb.tbl_CBCadDefeito.Where(x => d.CodDefeito.Replace("UM", "0").Replace("UI", "0").Replace("LM", "0").Replace("SF", "0") == x.CodDefeito).DefaultIfEmpty()
							 from posto in dtbCb.tbl_CBPosto.Where(x => d.CodLinha == x.CodLinha && d.NumPosto == x.NumPosto).DefaultIfEmpty()
							 from causa in dtbCb.tbl_CBCadCausa.Where(x => d.CodCausa == x.CodCausa).DefaultIfEmpty()
							 from origem in dtbCb.tbl_CBCadOrigem.Where(x => d.CodOrigem == x.CodOrigem).DefaultIfEmpty()
							 from modelo in dtbCb.tbl_Item.Where(x => d.NumECB.Substring(0, 6) == x.CodItem).DefaultIfEmpty()
							 from subDefeito in dtbCb.tbl_CBCadSubDefeito.Where(x => d.CodDefeito == x.CodDefeito && d.CodSubDefeito == x.CodSubDefeito).DefaultIfEmpty()
							 from item in dtbCb.tbl_Item.Where(x => d.CodItem == x.CodItem).DefaultIfEmpty()
							 let desIAC = (from a in dtbCb.tbl_CBPassagem
										   join b in dtbCb.tbl_CBLinha on a.CodLinha equals b.CodLinha
										   where a.NumECB == d.NumECB && b.Setor == "IAC" && !b.DesLinha.Contains("ICT") && !b.DesLinha.Contains("LOTE")
										   orderby a.DatEvento descending
										   select b).FirstOrDefault().DesLinha
							 let qtdEmbalado = (from e in dtbCb.tbl_CBEmbalada where e.NumECB == d.NumECB select new { e.NumECB }).Count()
                             where (d.DatReferencia >= datInicio && d.DatReferencia <= datFim
                                      && (d.FlgRevisado < 2 || d.FlgRevisado == 3))
                             select new
                             {
                                 NumECB = d.NumECB,
                                 CodLinha = d.CodLinha,
                                 NumPosto = d.NumPosto,
                                 CodDefeito = (d.CodSubDefeito == null || subDefeito.flgAtivo == "0") ? d.CodDefeito : d.CodDefeito + "." + d.CodSubDefeito,
                                 DatEvento = d.DatEvento,
                                 FlgRevisado = d.FlgRevisado,
                                 CodCausa = d.CodCausa,
                                 Posicao = d.Posicao,
                                 FlgTipo = d.FlgTipo,
                                 CodOrigem = d.CodOrigem,
                                 DesAcao = d.DesAcao,
                                 DatRevisado = d.DatRevisado,
                                 DatEntRevisao = d.DatEntRevisao,
                                 NomUsuario = d.NomUsuario,
                                 NumIP = d.NumIP,
                                 CodRma = d.CodRma,
                                 NumPostoRevisado = d.NumPostoRevisado,
                                 CodDRT = d.CodDRT,
                                 CodDRTRevisor = d.CodDRTRevisor,
                                 CodDefeitoTest = d.CodDefeitoTest,
                                 ObsDefeito = d.ObsDefeito,
                                 DefIncorreto = d.DefIncorreto,
                                 CodDRTTest = d.CodDRTTest,
                                 NomUsuarioTest = d.NomUsuarioTest,
                                 NumSeriePai = d.NumSeriePai,
                                 CodLinhaRevisado = d.CodLinhaRevisado,
                                 Romaneio = d.Romaneio,
                                 CodItem = d.CodItem,
                                 DatReferencia = d.DatReferencia,
                                 Turno = d.Turno,
                                 DesDefeito = (d.CodSubDefeito == null || subDefeito.flgAtivo == "0") ? def.DesDefeito : subDefeito.DesDefeito,
                                 DesLinha = l.DesLinha,
                                 DesPosto = posto.DesPosto,
                                 DesCausa = causa.DesCausa,
                                 DesOrigem = origem.DesOrigem,
                                 DesModelo = modelo.DesItem,
                                 DesItem = item.DesItem,
                                 Setor = l.Setor,
                                 Familia = l.Familia,
                                 DesUltimaLinhaIAC = desIAC ?? "",
								 qtdEmbalado
							 });

                if (codLinha != null)
                {
                    query = query.Where(x => x.CodLinha == codLinha);
                }

                if (!String.IsNullOrEmpty(familia))
                {
                    query = query.Where(x => x.Familia == familia);
                }

                if (!String.IsNullOrEmpty(fabrica))
                {
                    query = query.Where(x => x.Setor == fabrica);
                }

                if (flgPlaca == 0)
                {
                    query = query.Where(x => String.IsNullOrEmpty(x.NumSeriePai));
                }
                else
                {
                    query = query.Where(x => !String.IsNullOrEmpty(x.NumSeriePai) && (x.NumECB.Length == 12 || x.NumECB.Length == 13) && x.qtdEmbalado > 0);
                }

                if (!String.IsNullOrEmpty(codDRT))
                {
                    query = query.Where(x => x.CodDRT == codDRT);
                }

                var defeitos = new List<viw_CBDefeitoTurnoDTO>();

                foreach (var d in query.ToList())
                {
                    var modelo = d.DesModelo;

                    if (d.NumECB.Length == 9)
                    {
                        var codmodelo = (from p in dtbSsa.TblHistorico
                                         join l in dtbSsa.Tbl_Of on p.codOF equals l.codOF
                                         where p.desSerie == d.NumECB
                                         select l.codModelo).FirstOrDefault();

                        if (codmodelo != null)
                        {
                            var desmodelo = dtbSIM.tbl_Item.Select(x => new { x.CodItem, x.DesItem }).FirstOrDefault(p => p.CodItem == codmodelo);

                            modelo = desmodelo != null ? desmodelo.DesItem : "Modelo não encontrado";
                        }

                    }

                    defeitos.Add(new viw_CBDefeitoTurnoDTO
                    {
                        NumECB = d.NumECB,
                        CodLinha = d.CodLinha,
                        NumPosto = d.NumPosto,
                        CodDefeito = d.CodDefeito,
                        DatEvento = d.DatEvento,
                        FlgRevisado = d.FlgRevisado,
                        CodCausa = d.CodCausa,
                        Posicao = d.Posicao,
                        FlgTipo = d.FlgTipo,
                        CodOrigem = d.CodOrigem,
                        DesAcao = d.DesAcao,
                        DatRevisado = d.DatRevisado,
                        DatEntRevisao = d.DatEntRevisao,
                        NomUsuario = d.NomUsuario,
                        NumIP = d.NumIP,
                        CodRma = d.CodRma,
                        NumPostoRevisado = d.NumPostoRevisado,
                        CodDRT = d.CodDRT,
                        CodDRTRevisor = d.CodDRTRevisor,
                        CodDefeitoTest = d.CodDefeitoTest,
                        ObsDefeito = d.ObsDefeito,
                        DefIncorreto = d.DefIncorreto,
                        CodDRTTest = d.CodDRTTest,
                        NomUsuarioTest = d.NomUsuarioTest,
                        NumSeriePai = d.NumSeriePai,
                        CodLinhaRevisado = d.CodLinhaRevisado,
                        Romaneio = d.Romaneio,
                        CodItem = d.CodItem,
                        DatReferencia = d.DatReferencia,
                        Turno = d.Turno,
                        DesDefeito = d.DesDefeito,
                        DesLinha = d.DesLinha,
                        DesPosto = d.DesPosto,
                        DesCausa = d.DesCausa,
                        DesOrigem = d.DesOrigem,
                        //Obter Modelo da informatica, numero de série de 9 digitos
                        DesModelo = modelo,
                        DesPosicao = d.DesItem,
                        DesUltimaLinhaIAC = d.DesUltimaLinhaIAC,
                        Setor = d.Setor
                        //DesUltimaLinhaIAC = fabrica == "IAC" && d.DesLinha.Contains("ICT") ? GetUltimaLinhaIAC(d.NumECB) : null
                    });
                }

                return defeitos;
            }

        }

        private static string GetUltimaLinhaIAC(string numSerie)
        {
            using (var dtbCb = new DTB_CBContext())
            {
                string ultinhaLInha = (from a in dtbCb.tbl_CBPassagem
                                       join b in dtbCb.tbl_CBLinha on a.CodLinha equals b.CodLinha
                                       where a.NumECB == numSerie && b.Setor == "IAC" && !b.DesLinha.Contains("ICT") && !b.DesLinha.Contains("LOTE")
                                       orderby a.DatEvento descending
                                       select b).FirstOrDefault().DesLinha;
                return ultinhaLInha;
            }

        }

        public static List<viw_CBDefeitoTurnoDTO> ObterDefeitosRelRef(DateTime datInicio, DateTime datFim, int? codLinha = null, string fabrica = "", string codDRT = "")
        {
            using (var dtbCb = new DTB_CBContext())
            {
                var query = (from d in dtbCb.viw_CBDefeitoTurno
                             join l in dtbCb.tbl_CBLinha on d.CodLinha equals l.CodLinha
                             from pai in dtbCb.tbl_CBDefeito.Where(x => x.NumSeriePai == d.NumECB && x.DatEvento == d.DatEvento)
                             from def in dtbCb.tbl_CBCadDefeito.Where(x => d.CodDefeito.Replace("UM", "0").Replace("UI", "0").Replace("LM", "0").Replace("SF", "0") == x.CodDefeito).DefaultIfEmpty()
                             from linha in dtbCb.tbl_CBLinha.Where(x => d.CodLinha.Equals(x.CodLinha)).DefaultIfEmpty()
                             from posto in dtbCb.tbl_CBPosto.Where(x => d.CodLinha == x.CodLinha && d.NumPosto == x.NumPosto).DefaultIfEmpty()
                             from causa in dtbCb.tbl_CBCadCausa.Where(x => d.CodCausa == x.CodCausa).DefaultIfEmpty()
                             from origem in dtbCb.tbl_CBCadOrigem.Where(x => d.CodOrigem == x.CodOrigem).DefaultIfEmpty()
                             from modelo in dtbCb.tbl_Item.Where(x => d.NumECB.Substring(0, 6) == x.CodItem).DefaultIfEmpty()
                             from item in dtbCb.tbl_Item.Where(x => d.CodItem == x.CodItem).DefaultIfEmpty()
                             where (d.DatReferencia >= datInicio && d.DatReferencia <= datFim
                                      && (d.FlgRevisado < 2 || d.FlgRevisado == 3))
                             select new
                             {
                                 NumECB = d.NumECB,
                                 CodLinha = d.CodLinha,
                                 NumPosto = d.NumPosto,
                                 CodDefeito = d.CodDefeito,
                                 DatEvento = d.DatEvento,
                                 FlgRevisado = d.FlgRevisado,
                                 CodCausa = d.CodCausa,
                                 Posicao = d.Posicao,
                                 FlgTipo = d.FlgTipo,
                                 CodOrigem = d.CodOrigem,
                                 DesAcao = d.DesAcao,
                                 DatRevisado = d.DatRevisado,
                                 DatEntRevisao = d.DatEntRevisao,
                                 NomUsuario = d.NomUsuario,
                                 NumIP = d.NumIP,
                                 CodRma = d.CodRma,
                                 NumPostoRevisado = d.NumPostoRevisado,
                                 CodDRT = d.CodDRT,
                                 CodDRTRevisor = d.CodDRTRevisor,
                                 CodDefeitoTest = d.CodDefeitoTest,
                                 ObsDefeito = d.ObsDefeito,
                                 DefIncorreto = d.DefIncorreto,
                                 CodDRTTest = d.CodDRTTest,
                                 NomUsuarioTest = d.NomUsuarioTest,
                                 NumSeriePai = d.NumSeriePai,
                                 CodLinhaRevisado = d.CodLinhaRevisado,
                                 Romaneio = d.Romaneio,
                                 CodItem = d.CodItem,
                                 DatReferencia = d.DatReferencia,
                                 Turno = d.Turno,
                                 DesDefeito = def.DesDefeito,
                                 DesLinha = linha.DesLinha,
                                 DesPosto = posto.DesPosto,
                                 DesCausa = causa.DesCausa,
                                 DesOrigem = origem.DesOrigem,
                                 DesModelo = modelo.DesItem,
                                 DesItem = item.DesItem,
                                 Setor = l.Setor
                             });

                if (codLinha != null)
                {
                    query = query.Where(x => x.CodLinha == codLinha);
                }

                if (!String.IsNullOrEmpty(fabrica))
                {
                    query = query.Where(x => x.Setor == fabrica);
                }

                if (!String.IsNullOrEmpty(codDRT))
                {
                    query = query.Where(x => x.CodDRT == codDRT);
                }

                var defeitos = query.ToList().Select(d => new viw_CBDefeitoTurnoDTO()
                {
                    NumECB = d.NumECB,
                    CodLinha = d.CodLinha,
                    NumPosto = d.NumPosto,
                    CodDefeito = d.CodDefeito,
                    DatEvento = d.DatEvento,
                    FlgRevisado = d.FlgRevisado,
                    CodCausa = d.CodCausa,
                    Posicao = d.Posicao,
                    FlgTipo = d.FlgTipo,
                    CodOrigem = d.CodOrigem,
                    DesAcao = d.DesAcao,
                    DatRevisado = d.DatRevisado,
                    DatEntRevisao = d.DatEntRevisao,
                    NomUsuario = d.NomUsuario,
                    NumIP = d.NumIP,
                    CodRma = d.CodRma,
                    NumPostoRevisado = d.NumPostoRevisado,
                    CodDRT = d.CodDRT,
                    CodDRTRevisor = d.CodDRTRevisor,
                    CodDefeitoTest = d.CodDefeitoTest,
                    ObsDefeito = d.ObsDefeito,
                    DefIncorreto = d.DefIncorreto,
                    CodDRTTest = d.CodDRTTest,
                    NomUsuarioTest = d.NomUsuarioTest,
                    NumSeriePai = d.NumSeriePai,
                    CodLinhaRevisado = d.CodLinhaRevisado,
                    Romaneio = d.Romaneio,
                    CodItem = d.CodItem,
                    DatReferencia = d.DatReferencia,
                    Turno = d.Turno,
                    DesDefeito = d.DesDefeito,
                    DesLinha = d.DesLinha,
                    DesPosto = d.DesPosto,
                    DesCausa = d.DesCausa,
                    DesOrigem = d.DesOrigem,
                    DesModelo = d.DesModelo,
                    DesPosicao = d.DesItem
                }).ToList();

                return defeitos;
            }

        }

        public static List<viw_CBDefeitoTurnoDTO> ObterDefeitosLinha(DateTime DatInicio, DateTime DatFim, int CodLinha)
        {
            using (var dtbCb = new DTB_CBContext())
            {
                var query = (from d in dtbCb.viw_CBDefeitoTurno
                                 //from acao in dtbCb.tbl_CBAcao.Where(x => d.DesAcao == x.CodAcao.ToString()).DefaultIfEmpty()
                             join l in dtbCb.tbl_CBLinha on d.CodLinha equals l.CodLinha
                             join def in dtbCb.tbl_CBCadDefeito on d.CodDefeito.Replace("UM", "0").Replace("UI", "0").Replace("LM", "0").Replace("SF", "0") equals def.CodDefeito into def2
                             from leftDef in def2.DefaultIfEmpty()
                             join linha in dtbCb.tbl_CBLinha on d.CodLinha equals linha.CodLinha into linha2
                             from leftLinha in linha2.DefaultIfEmpty()
                             join posto in dtbCb.tbl_CBPosto on new { d.CodLinha, d.NumPosto } equals new { posto.CodLinha, posto.NumPosto } into posto2
                             from leftPosto in posto2.DefaultIfEmpty()
                             join causa in dtbCb.tbl_CBCadCausa on d.CodCausa equals causa.CodCausa into causa2
                             from leftCausa in causa2.DefaultIfEmpty()
                             join origem in dtbCb.tbl_CBCadOrigem on d.CodOrigem equals origem.CodOrigem into origem2
                             from leftOrigem in origem2.DefaultIfEmpty()
                             join item in dtbCb.tbl_Item on d.NumECB.Substring(0, 6) equals item.CodItem into item2
                             from leftItem in item2.DefaultIfEmpty()
                             where (d.DatReferencia >= DatInicio && d.DatReferencia <= DatFim
                                      && d.FlgRevisado < 2
                                      && String.IsNullOrEmpty(d.NumSeriePai)
                                      && d.CodLinha == CodLinha)

                             select new viw_CBDefeitoTurnoDTO()
                             {
                                 NumECB = d.NumECB,
                                 CodLinha = d.CodLinha,
                                 NumPosto = d.NumPosto,
                                 CodDefeito = d.CodDefeito,
                                 DatEvento = d.DatEvento,
                                 FlgRevisado = d.FlgRevisado,
                                 CodCausa = d.CodCausa,
                                 Posicao = d.Posicao,
                                 FlgTipo = d.FlgTipo,
                                 CodOrigem = d.CodOrigem,
                                 //DesAcao = acao.Descricao,
                                 DesAcao = d.DesAcao,
                                 DatRevisado = d.DatRevisado,
                                 DatEntRevisao = d.DatEntRevisao,
                                 NomUsuario = d.NomUsuario,
                                 NumIP = d.NumIP,
                                 CodRma = d.CodRma,
                                 NumPostoRevisado = d.NumPostoRevisado,
                                 CodDRT = d.CodDRT,
                                 CodDRTRevisor = d.CodDRTRevisor,
                                 CodDefeitoTest = d.CodDefeitoTest,
                                 ObsDefeito = d.ObsDefeito,
                                 DefIncorreto = d.DefIncorreto,
                                 CodDRTTest = d.CodDRTTest,
                                 NomUsuarioTest = d.NomUsuarioTest,
                                 NumSeriePai = d.NumSeriePai,
                                 CodLinhaRevisado = d.CodLinhaRevisado,
                                 DatReferencia = d.DatReferencia,
                                 Turno = d.Turno,
                                 DesDefeito = leftDef.DesDefeito,
                                 DesLinha = leftLinha.DesLinha,
                                 DesPosto = leftPosto.DesPosto,
                                 DesCausa = leftCausa.DesCausa,
                                 DesOrigem = leftOrigem.DesOrigem,
                                 DesModelo = leftItem.DesItem
                             });
                var defeitos = query.ToList();

                return defeitos;
            }
        }

        public static List<viw_CBDefeitoTurnoDTO> ObterDefeitosLinhaPlaca(DateTime DatInicio, DateTime DatFim, int CodLinha)
        {
            using (var dtbCb = new DTB_CBContext())
            {
                var query = (from d in dtbCb.viw_CBDefeitoTurno
                                 //join acao in dtbCb.tbl_CBAcao on d.DesAcao equals acao.CodAcao.ToString()
                             join l in dtbCb.tbl_CBLinha on d.CodLinha equals l.CodLinha
                             join def in dtbCb.tbl_CBCadDefeito on d.CodDefeito.Replace("UM", "0").Replace("UI", "0").Replace("LM", "0").Replace("SF", "0") equals def.CodDefeito into def2
                             from leftDef in def2.DefaultIfEmpty()
                             join linha in dtbCb.tbl_CBLinha on d.CodLinha equals linha.CodLinha into linha2
                             from leftLinha in linha2.DefaultIfEmpty()
                             join posto in dtbCb.tbl_CBPosto on new { d.CodLinha, d.NumPosto } equals new { posto.CodLinha, posto.NumPosto } into posto2
                             from leftPosto in posto2.DefaultIfEmpty()
                             join causa in dtbCb.tbl_CBCadCausa on d.CodCausa equals causa.CodCausa into causa2
                             from leftCausa in causa2.DefaultIfEmpty()
                             join origem in dtbCb.tbl_CBCadOrigem on d.CodOrigem equals origem.CodOrigem into origem2
                             from leftOrigem in origem2.DefaultIfEmpty()
                             join item in dtbCb.tbl_Item on d.NumECB.Substring(0, 6) equals item.CodItem into item2
                             from leftItem in item2.DefaultIfEmpty()
                             where (d.DatReferencia >= DatInicio && d.DatReferencia <= DatFim
                                   && !String.IsNullOrEmpty(d.NumSeriePai)
                                   && d.FlgRevisado < 2
                                   && d.CodLinha == CodLinha
                                   && d.NumECB.Length == 12)

                             select new viw_CBDefeitoTurnoDTO()
                             {
                                 NumECB = d.NumECB,
                                 CodLinha = d.CodLinha,
                                 NumPosto = d.NumPosto,
                                 CodDefeito = d.CodDefeito,
                                 DatEvento = d.DatEvento,
                                 FlgRevisado = d.FlgRevisado,
                                 CodCausa = d.CodCausa,
                                 Posicao = d.Posicao,
                                 FlgTipo = d.FlgTipo,
                                 CodOrigem = d.CodOrigem,
                                 //DesAcao = acao.Descricao,
                                 DesAcao = d.DesAcao,
                                 DatRevisado = d.DatRevisado,
                                 DatEntRevisao = d.DatEntRevisao,
                                 NomUsuario = d.NomUsuario,
                                 NumIP = d.NumIP,
                                 CodRma = d.CodRma,
                                 NumPostoRevisado = d.NumPostoRevisado,
                                 CodDRT = d.CodDRT,
                                 CodDRTRevisor = d.CodDRTRevisor,
                                 CodDefeitoTest = d.CodDefeitoTest,
                                 ObsDefeito = d.ObsDefeito,
                                 DefIncorreto = d.DefIncorreto,
                                 CodDRTTest = d.CodDRTTest,
                                 NomUsuarioTest = d.NomUsuarioTest,
                                 NumSeriePai = d.NumSeriePai,
                                 CodLinhaRevisado = d.CodLinhaRevisado,
                                 DatReferencia = d.DatReferencia,
                                 Turno = d.Turno,
                                 DesDefeito = leftDef.DesDefeito,
                                 DesLinha = leftLinha.DesLinha,
                                 DesPosto = leftPosto.DesPosto,
                                 DesCausa = leftCausa.DesCausa,
                                 DesOrigem = leftOrigem.DesOrigem,
                                 DesModelo = leftItem.DesItem
                             });
                var defeitos = query.ToList();

                return defeitos;
            }
        }

        public static List<viw_CBDefeitoTurnoDTO> ObterDefeitosFabrica(DateTime DatInicio, DateTime DatFim, string Fabrica)
        {
            using (var dtbCb = new DTB_CBContext())
            {
                var query = (from d in dtbCb.viw_CBDefeitoTurno
                             join l in dtbCb.tbl_CBLinha on d.CodLinha equals l.CodLinha
                             join def in dtbCb.tbl_CBCadDefeito on d.CodDefeito.Replace("UM", "0").Replace("UI", "0").Replace("LM", "0").Replace("SF", "0") equals def.CodDefeito into def2
                             from leftDef in def2.DefaultIfEmpty()
                             join linha in dtbCb.tbl_CBLinha on d.CodLinha equals linha.CodLinha into linha2
                             from leftLinha in linha2.DefaultIfEmpty()
                             join posto in dtbCb.tbl_CBPosto on new { d.CodLinha, d.NumPosto } equals new { posto.CodLinha, posto.NumPosto } into posto2
                             from leftPosto in posto2.DefaultIfEmpty()
                             join causa in dtbCb.tbl_CBCadCausa on d.CodCausa equals causa.CodCausa into causa2
                             from leftCausa in causa2.DefaultIfEmpty()
                             join origem in dtbCb.tbl_CBCadOrigem on d.CodOrigem equals origem.CodOrigem into origem2
                             from leftOrigem in origem2.DefaultIfEmpty()
                             join item in dtbCb.tbl_Item on d.NumECB.Substring(0, 6) equals item.CodItem into item2
                             from leftItem in item2.DefaultIfEmpty()
                             where (d.DatReferencia >= DatInicio
                                   && d.DatReferencia <= DatFim
                                   && l.Setor == Fabrica
                                   && d.FlgRevisado < 2
                                   && String.IsNullOrEmpty(d.NumSeriePai))

                             select new viw_CBDefeitoTurnoDTO()
                             {
                                 NumECB = d.NumECB,
                                 CodLinha = d.CodLinha,
                                 NumPosto = d.NumPosto,
                                 CodDefeito = d.CodDefeito,
                                 DatEvento = d.DatEvento,
                                 FlgRevisado = d.FlgRevisado,
                                 CodCausa = d.CodCausa,
                                 Posicao = d.Posicao,
                                 FlgTipo = d.FlgTipo,
                                 CodOrigem = d.CodOrigem,
                                 DesAcao = d.DesAcao,
                                 DatRevisado = d.DatRevisado,
                                 DatEntRevisao = d.DatEntRevisao,
                                 NomUsuario = d.NomUsuario,
                                 NumIP = d.NumIP,
                                 CodRma = d.CodRma,
                                 NumPostoRevisado = d.NumPostoRevisado,
                                 CodDRT = d.CodDRT,
                                 CodDRTRevisor = d.CodDRTRevisor,
                                 CodDefeitoTest = d.CodDefeitoTest,
                                 ObsDefeito = d.ObsDefeito,
                                 DefIncorreto = d.DefIncorreto,
                                 CodDRTTest = d.CodDRTTest,
                                 NomUsuarioTest = d.NomUsuarioTest,
                                 NumSeriePai = d.NumSeriePai,
                                 CodLinhaRevisado = d.CodLinhaRevisado,
                                 DatReferencia = d.DatReferencia,
                                 Turno = d.Turno,
                                 DesDefeito = leftDef.DesDefeito,
                                 DesLinha = leftLinha.DesLinha,
                                 DesPosto = leftPosto.DesPosto,
                                 DesCausa = leftCausa.DesCausa,
                                 DesOrigem = leftOrigem.DesOrigem,
                                 DesModelo = leftItem.DesItem
                             });
                var defeitos = query.ToList();

                return defeitos;
            }
        }

        public static List<viw_CBDefeitoTurnoDTO> ObterDefeitosFabricaPlaca(DateTime DatInicio, DateTime DatFim, string Fabrica)
        {
            using (var dtbCb = new DTB_CBContext())
            {
                var query = (from d in dtbCb.viw_CBDefeitoTurno
                             join l in dtbCb.tbl_CBLinha on d.CodLinha equals l.CodLinha
                             join def in dtbCb.tbl_CBCadDefeito on d.CodDefeito.Replace("UM", "0").Replace("UI", "0").Replace("LM", "0").Replace("SF", "0") equals def.CodDefeito into def2
                             from leftDef in def2.DefaultIfEmpty()
                             join linha in dtbCb.tbl_CBLinha on d.CodLinha equals linha.CodLinha into linha2
                             from leftLinha in linha2.DefaultIfEmpty()
                             join posto in dtbCb.tbl_CBPosto on new { d.CodLinha, d.NumPosto } equals new { posto.CodLinha, posto.NumPosto } into posto2
                             from leftPosto in posto2.DefaultIfEmpty()
                             join causa in dtbCb.tbl_CBCadCausa on d.CodCausa equals causa.CodCausa into causa2
                             from leftCausa in causa2.DefaultIfEmpty()
                             join origem in dtbCb.tbl_CBCadOrigem on d.CodOrigem equals origem.CodOrigem into origem2
                             from leftOrigem in origem2.DefaultIfEmpty()
                             join item in dtbCb.tbl_Item on d.NumECB.Substring(0, 6) equals item.CodItem into item2
                             from leftItem in item2.DefaultIfEmpty()

                             where (d.DatReferencia >= DatInicio
                                     && d.DatReferencia <= DatFim
                                     && d.FlgRevisado < 2
                                     && l.Setor == Fabrica
                                     && !String.IsNullOrEmpty(d.NumSeriePai)
                                     && d.NumECB.Length == 12)

                             select new viw_CBDefeitoTurnoDTO()
                             {
                                 NumECB = d.NumECB,
                                 CodLinha = d.CodLinha,
                                 NumPosto = d.NumPosto,
                                 CodDefeito = d.CodDefeito,
                                 DatEvento = d.DatEvento,
                                 FlgRevisado = d.FlgRevisado,
                                 CodCausa = d.CodCausa,
                                 Posicao = d.Posicao,
                                 FlgTipo = d.FlgTipo,
                                 CodOrigem = d.CodOrigem,
                                 DesAcao = d.DesAcao,
                                 DatRevisado = d.DatRevisado,
                                 DatEntRevisao = d.DatEntRevisao,
                                 NomUsuario = d.NomUsuario,
                                 NumIP = d.NumIP,
                                 CodRma = d.CodRma,
                                 NumPostoRevisado = d.NumPostoRevisado,
                                 CodDRT = d.CodDRT,
                                 CodDRTRevisor = d.CodDRTRevisor,
                                 CodDefeitoTest = d.CodDefeitoTest,
                                 ObsDefeito = d.ObsDefeito,
                                 DefIncorreto = d.DefIncorreto,
                                 CodDRTTest = d.CodDRTTest,
                                 NomUsuarioTest = d.NomUsuarioTest,
                                 NumSeriePai = d.NumSeriePai,
                                 CodLinhaRevisado = d.CodLinhaRevisado,
                                 DatReferencia = d.DatReferencia,
                                 Turno = d.Turno,
                                 DesDefeito = leftDef.DesDefeito,
                                 DesLinha = leftLinha.DesLinha,
                                 DesPosto = leftPosto.DesPosto,
                                 DesCausa = leftCausa.DesCausa,
                                 DesOrigem = leftOrigem.DesOrigem,
                                 DesModelo = leftItem.DesItem
                             });
                var defeitos = query.ToList();


                return defeitos;
            }
        }

        public static List<viw_CBDefeitoTurnoDTO> ObterDefeitosFamilia(DateTime DatInicio, DateTime DatFim, string Familia, string Fabrica)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var defeitos = (from d in dtbCb.viw_CBDefeitoTurno
                                join l in dtbCb.tbl_CBLinha on d.CodLinha equals l.CodLinha
                                where d.DatReferencia >= DatInicio && d.DatReferencia <= DatFim &&
                                      l.Setor == Fabrica && l.Familia == Familia && d.FlgRevisado < 2
                                      && String.IsNullOrEmpty(d.NumSeriePai)
                                select d).ToList();

                List<viw_CBDefeitoTurnoDTO> viewDTO = new List<viw_CBDefeitoTurnoDTO>();

                AutoMapper.Mapper.Map(defeitos, viewDTO);

                return viewDTO;
            }
        }

        public static List<viw_CBDefeitoTurnoDTO> ObterDefeitosSerie(String numECB)
        {
            using (var dtbCb = new DTB_CBContext())
            {
                var query = (from d in dtbCb.viw_CBDefeitoTurno
                             join l in dtbCb.tbl_CBLinha on d.CodLinha equals l.CodLinha
                             from def in dtbCb.tbl_CBCadDefeito.Where(x => d.CodDefeito.Replace("UM", "0").Replace("UI", "0").Replace("LM", "0").Replace("SF", "0") == x.CodDefeito).DefaultIfEmpty()
                             from linha in dtbCb.tbl_CBLinha.Where(x => d.CodLinha.Equals(x.CodLinha)).DefaultIfEmpty()
                             from posto in dtbCb.tbl_CBPosto.Where(x => d.CodLinha == x.CodLinha && d.NumPosto == x.NumPosto).DefaultIfEmpty()
                             from causa in dtbCb.tbl_CBCadCausa.Where(x => d.CodCausa == x.CodCausa).DefaultIfEmpty()
                             from origem in dtbCb.tbl_CBCadOrigem.Where(x => d.CodOrigem == x.CodOrigem).DefaultIfEmpty()
                             from modelo in dtbCb.tbl_Item.Where(x => d.NumECB.Substring(0, 6) == x.CodItem).DefaultIfEmpty()
                             from item in dtbCb.tbl_Item.Where(x => d.CodItem == x.CodItem).DefaultIfEmpty()
                             where (d.NumECB == numECB)
                             select new viw_CBDefeitoTurnoDTO()
                             {
                                 NumECB = d.NumECB,
                                 CodLinha = d.CodLinha,
                                 NumPosto = d.NumPosto,
                                 CodDefeito = d.CodDefeito,
                                 DatEvento = d.DatEvento,
                                 FlgRevisado = d.FlgRevisado,
                                 CodCausa = d.CodCausa,
                                 Posicao = d.Posicao,
                                 FlgTipo = d.FlgTipo,
                                 CodOrigem = d.CodOrigem,
                                 DesAcao = d.DesAcao,
                                 DatRevisado = d.DatRevisado,
                                 DatEntRevisao = d.DatEntRevisao,
                                 NomUsuario = d.NomUsuario,
                                 NumIP = d.NumIP,
                                 CodRma = d.CodRma,
                                 NumPostoRevisado = d.NumPostoRevisado,
                                 CodDRT = d.CodDRT,
                                 CodDRTRevisor = d.CodDRTRevisor,
                                 CodDefeitoTest = d.CodDefeitoTest,
                                 ObsDefeito = d.ObsDefeito,
                                 DefIncorreto = d.DefIncorreto,
                                 CodDRTTest = d.CodDRTTest,
                                 NomUsuarioTest = d.NomUsuarioTest,
                                 NumSeriePai = d.NumSeriePai,
                                 CodLinhaRevisado = d.CodLinhaRevisado,
                                 Romaneio = d.Romaneio,
                                 CodItem = d.CodItem,
                                 DatReferencia = d.DatReferencia,
                                 Turno = d.Turno,
                                 DesDefeito = def.DesDefeito,
                                 DesLinha = linha.DesLinha,
                                 DesPosto = posto.DesPosto,
                                 DesCausa = causa.DesCausa,
                                 DesOrigem = origem.DesOrigem,
                                 DesModelo = modelo.DesItem,
                                 DesPosicao = item.DesItem
                             });

                return query.ToList();
            }
        }

        public static string ObterDesDefeito(String CodDefeito)
        {
            string desdefeito = "";

            if (String.IsNullOrEmpty(CodDefeito))
            {
                return "";
            }

            if (CodDefeito.Trim().Length == 3)
                CodDefeito = "0" + CodDefeito;
            else if (CodDefeito.Trim().Length == 5)
                CodDefeito = "0" + CodDefeito.Substring(2, 3);

            {
                using (DTB_CBContext dtbCB = new DTB_CBContext())
                {
                    desdefeito = (from c in dtbCB.tbl_CBCadDefeito
                                  where c.CodDefeito == CodDefeito.ToString()
                                  select c.DesDefeito).FirstOrDefault();
                }
            }

            if (String.IsNullOrEmpty(desdefeito))
            {
                desdefeito = "";
            }

            return desdefeito;

        }

        public static Boolean VerificaDefeitoAtivo(string CodDefeito)
        {
            using (DTB_CBContext dtbCB = new DTB_CBContext())
            {
                var desdefeito = (from c in dtbCB.tbl_CBCadDefeito
                                  where c.CodDefeito == CodDefeito.ToString()
                                  select c).FirstOrDefault();

                if (desdefeito != null && desdefeito.flgAtivo == "1")
                    return true;
                else
                    return false;

            }
        }

        public static Boolean VerificaPostoDefeito(int CodLinha, int NumPosto, string CodDefeito)
        {
            using (DTB_CBContext dtbCB = new DTB_CBContext())
            {
                var verifica = (from c in dtbCB.tbl_CBPostoDefeito
                                where c.CodLinha == CodLinha && c.NumPosto == NumPosto && c.CodDefeito == CodDefeito
                                select c).FirstOrDefault();

                if (verifica != null)
                    return true;
                else
                    return false;
            }
        }

        public static string ObterDesTipoNC(String TipoNC)
        {
            using (DTB_SIMContext dtbSim = new DTB_SIMContext())
            {
                string desdefeito = (from c in dtbSim.tbl_CNQMaoObra
                                     where c.CodMaoObra == TipoNC
                                     select c.DesMaoObraP).FirstOrDefault();
                if (String.IsNullOrEmpty(desdefeito))
                {
                    desdefeito = "";
                }
                return desdefeito;
            }
        }

        public static CBDefeitoDTO ObterDefeitoAberto(string NumECB)
        {
            try
            {
                using (DTB_CBContext dtbCb = new DTB_CBContext())
                {
                    var defeito = dtbCb.tbl_CBDefeito.OrderByDescending(o => o.DatEvento).FirstOrDefault(p => p.NumECB == NumECB && p.FlgRevisado == 0);

                    var defeitoDTO = new CBDefeitoDTO();

                    AutoMapper.Mapper.Map(defeito, defeitoDTO);

                    return defeitoDTO;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Obter defeitos de uma placa
        /// </summary>
        /// <param name="NumECB"></param>
        /// <returns></returns>
        public static List<CBDefeitoDTO> ObterDefeitoPCI(string NumECB)
        {
            try
            {
                using (DTB_CBContext dtbCb = new DTB_CBContext())
                {
                    var defeito = dtbCb.tbl_CBDefeito.Where(p => p.NumECB == NumECB).OrderByDescending(o => o.DatEvento).ToList();

                    var defeitoDTO = new List<CBDefeitoDTO>();

                    AutoMapper.Mapper.Map(defeito, defeitoDTO);

                    return defeitoDTO;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static Boolean GravarPosicao(CBDefeitoDTO defeitoDTO)
        {
            try
            {
                using (DTB_CBContext dtbCb = new DTB_CBContext())
                {
                    tbl_CBDefeito defeito = dtbCb.tbl_CBDefeito.OrderByDescending(o => o.DatEvento).FirstOrDefault(x => x.NumECB == defeitoDTO.NumECB && x.FlgRevisado == 0);
                    defeito.Posicao = defeitoDTO.Posicao;

                    try
                    {
                        dtbCb.SaveChanges();
                        return true;
                    }
                    catch (Exception)
                    {
                        return false;
                    }

                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static Boolean InsereDefeito(CBDefeitoDTO defeito)
        {
            try
            {
                using (DTB_CBContext dtbCb = new DTB_CBContext())
                {
                    tbl_CBDefeito viewDTO = new tbl_CBDefeito();

                    AutoMapper.Mapper.Map(defeito, viewDTO);


                    dtbCb.tbl_CBDefeito.Add(viewDTO);
                    dtbCb.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static DateTime ObterDataDefeito(string NumECB, int CodLinha, int NumPosto)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                DateTime DatEvento = (from p in dtbCb.tbl_CBDefeito
                                      where NumECB == p.NumECB && p.FlgRevisado == 0 && CodLinha == p.CodLinha && NumPosto == p.NumPosto
                                      select p.DatEvento).FirstOrDefault();

                return DatEvento;
            }
        }

        public static Boolean CancelamentoDefeito(string NumECB, int CodLinha, int NumPosto)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var evento = (from registro in dtbCb.tbl_CBDefeito
                              where NumECB == registro.NumECB && CodLinha == registro.CodLinha &&
                                      NumPosto == registro.NumPosto && registro.FlgRevisado == 0
                              select registro).FirstOrDefault();

                if (evento != null)
                {
                    evento.FlgRevisado = Constantes.DEF_CANCELADO;

                    try
                    {
                        dtbCb.SaveChanges();
                        return true;
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                }

                return false;
            }
        }

        public static List<CBLinhaDefeitoRelatorioDTO> ObterDefeitosRelatorio(int CodLinha, DateTime DatProducao)
        {

            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var defeitos = dtbCb.tbl_CBLinhaDefeitoRelatorio.Where(x => x.CodLinha == CodLinha && x.DatProducao == DatProducao).ToList();

                List<CBLinhaDefeitoRelatorioDTO> defeitosRel = new List<CBLinhaDefeitoRelatorioDTO>();

                AutoMapper.Mapper.Map(defeitos, defeitosRel);

                return defeitosRel;
            }

        }

        public static List<CBLinhaDefeitoRelatorioDTO> ObterDefeitosRelatorio(DateTime DatProducao)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var defeitos = dtbCb.tbl_CBLinhaDefeitoRelatorio.Where(x => x.DatProducao == DatProducao);

                var defeito = defeitos.ToList();

                List<CBLinhaDefeitoRelatorioDTO> defeitosRel = new List<CBLinhaDefeitoRelatorioDTO>();

                AutoMapper.Mapper.Map(defeito, defeitosRel);

                return defeitosRel;
            }

        }

        public static void IncluirDefeitoRelatorio(int CodLinha, DateTime DatProducao, string CodDefeito)
        {
            try
            {
                tbl_CBLinhaDefeitoRelatorio defeito = new tbl_CBLinhaDefeitoRelatorio()
                {
                    CodDefeito = CodDefeito,
                    CodLinha = CodLinha,
                    DatProducao = DatProducao
                };

                using (DTB_CBContext dtbCb = new DTB_CBContext())
                {
                    if (!dtbCb.tbl_CBLinhaDefeitoRelatorio.Where(
                            x => x.CodDefeito == CodDefeito &&
                                x.CodLinha == CodLinha &&
                                x.DatProducao == DatProducao).ToList().Any())
                    {
                        dtbCb.tbl_CBLinhaDefeitoRelatorio.Add(defeito);
                        dtbCb.SaveChanges();
                    }
                }
            }
            catch (Exception)
            {

            }

        }

        public static void RemoveDefeitoRelatorio(int CodLinha, DateTime DatProducao, string CodDefeito)
        {
            try
            {
                using (DTB_CBContext dtbCb = new DTB_CBContext())
                {
                    var defeito = dtbCb.tbl_CBLinhaDefeitoRelatorio.FirstOrDefault(
                        x => x.CodDefeito == CodDefeito && x.CodLinha == CodLinha && x.DatProducao == DatProducao);

                    if (defeito != null)
                    {
                        dtbCb.tbl_CBLinhaDefeitoRelatorio.Remove(defeito);
                        dtbCb.SaveChanges();
                    }
                }
            }
            catch (Exception)
            {

            }

        }

        public static int ObterQtdeDefeitos(DateTime datInicio, DateTime datFim, string codDefeito)
        {

            using (var dtbCb = new DTB_CBContext())
            {
                var defeitos = (from d in dtbCb.viw_CBDefeitoTurno
                                where d.DatReferencia >= datInicio && d.DatReferencia <= datFim
                                      && String.IsNullOrEmpty(d.NumSeriePai) && d.FlgRevisado < 2
                                      && d.CodDefeito == codDefeito
                                group d by d.CodDefeito into g
                                select new { Qtde = g.Count() }).FirstOrDefault();

                if (defeitos != null)
                    return defeitos.Qtde;

                return 0;
            }
        }

        public static int ObterQtdeDefeitos(DateTime datInicio, DateTime datFim, string codDefeito, string CodDRT)
        {

            using (var dtbCb = new DTB_CBContext())
            {
                var defeitos = (from d in dtbCb.viw_CBDefeitoTurno
                                where d.DatReferencia >= datInicio && d.DatReferencia <= datFim
                                      && String.IsNullOrEmpty(d.NumSeriePai) && d.FlgRevisado < 2
                                      && d.CodDefeito == codDefeito
                                      && d.CodDRT == CodDRT
                                group d by d.CodDefeito into g
                                select new { Qtde = g.Count() }).FirstOrDefault();
                if (defeitos != null)
                    return defeitos.Qtde;

                return 0;
            }
        }

        public static int ObterQtdeDefeitos(DateTime datInicio, DateTime datFim, string codDefeito, int CodLinha)
        {

            using (var dtbCb = new DTB_CBContext())
            {
                var defeitos = (from d in dtbCb.viw_CBDefeitoTurno
                                where d.DatReferencia >= datInicio && d.DatReferencia <= datFim
                                      && String.IsNullOrEmpty(d.NumSeriePai) && d.FlgRevisado < 2
                                      && d.CodDefeito == codDefeito
                                      && d.CodLinha == CodLinha
                                group d by d.CodDefeito into g
                                select new { Qtde = g.Count() }).FirstOrDefault();

                return defeitos != null ? defeitos.Qtde : 0;
            }
        }

        public static int ObterQtdeDefeitosFabrica(DateTime DatInicio, DateTime DatFim, string CodDefeito, string Fabrica)
        {

            using (var dtbCb = new DTB_CBContext())
            {
                var defeitos = (from d in dtbCb.viw_CBDefeitoTurno
                                join l in dtbCb.tbl_CBLinha on d.CodLinha equals l.CodLinha
                                where d.DatReferencia >= DatInicio && d.DatReferencia <= DatFim
                                      && String.IsNullOrEmpty(d.NumSeriePai) && d.FlgRevisado < 2
                                      && d.CodDefeito == CodDefeito
                                      && l.Setor == Fabrica
                                group d by d.CodDefeito into g
                                select new { Qtde = g.Count() }).FirstOrDefault();

                return defeitos != null ? defeitos.Qtde : 0;
            }
        }

        public static int ObterQtdDefeitosLinha(DateTime DatInicio, DateTime DatFim, int CodLinha)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var defeitos = (from d in dtbCb.viw_CBDefeitoTurno
                                where d.DatReferencia >= DatInicio && d.DatReferencia <= DatFim
                                      && String.IsNullOrEmpty(d.NumSeriePai)
                                      && d.FlgRevisado < 2
                                      && d.CodLinha == CodLinha
                                select d).Count();

                return defeitos;
            }
        }

        public static int ObterQtdDefeitosFFLinha(DateTime DatInicio, DateTime DatFim, int CodLinha)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var defeitos = (from d in dtbCb.viw_CBDefeitoTurno
                                where d.DatReferencia >= DatInicio && d.DatReferencia <= DatFim
                                      && String.IsNullOrEmpty(d.NumSeriePai)
                                      && d.FlgRevisado < 2
                                      && d.CodLinha == CodLinha
                                      && (d.CodCausa == "WX00" || d.DesAcao == "2")
                                select d).Count();

                return defeitos;
            }
        }

        #endregion

        #region Cadastro Defeito

        public static List<CBCadDefeitoDTO> ObterListaDefeitos()
        {
            using (var dtbCb = new DTB_CBContext())
            {
                var defeitos = dtbCb.tbl_CBCadDefeito.ToList();

                var defeitosDTO = new List<CBCadDefeitoDTO>();

                AutoMapper.Mapper.Map(defeitos, defeitosDTO);

                return defeitosDTO;
            }
        }

        public static CBCadDefeitoDTO ObterCadDefeito(string codDefeito)
        {
            using (var dtbCb = new DTB_CBContext())
            {
                var defeitos = dtbCb.tbl_CBCadDefeito.FirstOrDefault(x => x.CodDefeito == codDefeito);

                var defeitosDTO = new CBCadDefeitoDTO();

                AutoMapper.Mapper.Map(defeitos, defeitosDTO);

                return defeitosDTO;
            }
        }

        public static void GravarDefeito(CBCadDefeitoDTO defeito)
        {

            using (var dtbCb = new DTB_CBContext())
            {
                var def = dtbCb.tbl_CBCadDefeito.FirstOrDefault(x => x.CodDefeito == defeito.CodDefeito);

                if (def != null)
                {
                    AutoMapper.Mapper.Map(defeito, def);

                    dtbCb.SaveChanges();
                }
                else
                {
                    var novodef = new tbl_CBCadDefeito();

                    AutoMapper.Mapper.Map(defeito, novodef);

                    dtbCb.tbl_CBCadDefeito.Add(novodef);
                    dtbCb.SaveChanges();
                }
            }
        }

        public static void RemoverDefeito(CBCadDefeitoDTO defeito)
        {

            using (var dtbCb = new DTB_CBContext())
            {
                var def = dtbCb.tbl_CBCadDefeito.FirstOrDefault(x => x.CodDefeito == defeito.CodDefeito);

                if (def == null) return;

                dtbCb.tbl_CBCadDefeito.Remove(def);

                dtbCb.SaveChanges();
            }
        }

        public static string ObterUltimoDefeito()
        {
            using (var dtbCb = new DTB_CBContext())
            {
                var defeito = dtbCb.tbl_CBCadDefeito.OrderByDescending(x => x.CodDefeito).FirstOrDefault();

                if (defeito != null)
                {
                    return defeito.CodDefeito;
                }
                else
                {
                    return "0001";
                }

            }

        }

        #endregion

        #region Posto-Defeito

        public static List<CBPostoDefeitoDTO> ObterPostosDefeito(int? codLinha = null, int? numPosto = null)
        {
            using (var dtbCb = new DTB_CBContext())
            {
                var postodefeito = (from p in dtbCb.tbl_CBPostoDefeito
                                    from linha in dtbCb.tbl_CBLinha.Where(x => x.CodLinha == p.CodLinha).DefaultIfEmpty()
                                    from posto in dtbCb.tbl_CBPosto.Where(x => x.CodLinha == p.CodLinha && x.NumPosto == p.NumPosto).DefaultIfEmpty()
                                    from def in dtbCb.tbl_CBCadDefeito.Where(x => x.CodDefeito == p.CodDefeito)
                                    select new CBPostoDefeitoDTO()
                                    {
                                        CodDefeito = p.CodDefeito,
                                        CodLinha = p.CodLinha,
                                        NumPosto = p.NumPosto,
                                        DesLinha = linha.DesLinha,
                                        DesPosto = posto.DesPosto,
                                        DesDefeito = def.DesDefeito
                                    });

                if (codLinha != null) postodefeito = postodefeito.Where(x => x.CodLinha == codLinha);
                if (numPosto != null) postodefeito = postodefeito.Where(x => x.NumPosto == numPosto);

                return postodefeito.ToList();
            }

        }

        public static CBPostoDefeitoDTO ObterPostoDefeito(int codLinha, int numPosto, string codDefeito)
        {
            using (var dtbCb = new DTB_CBContext())
            {
                var postodefeito = (from p in dtbCb.tbl_CBPostoDefeito
                                    from linha in dtbCb.tbl_CBLinha.Where(x => x.CodLinha == p.CodLinha).DefaultIfEmpty()
                                    from posto in dtbCb.tbl_CBPosto.Where(x => x.CodLinha == p.CodLinha && x.NumPosto == p.NumPosto).DefaultIfEmpty()
                                    from def in dtbCb.tbl_CBCadDefeito.Where(x => x.CodDefeito == p.CodDefeito)
                                    select new CBPostoDefeitoDTO()
                                    {
                                        CodDefeito = p.CodDefeito,
                                        CodLinha = p.CodLinha,
                                        NumPosto = p.NumPosto,
                                        DesLinha = linha.DesLinha,
                                        DesPosto = posto.DesPosto,
                                        DesDefeito = def.DesDefeito
                                    }).FirstOrDefault(x => x.CodLinha == codLinha && x.NumPosto == numPosto && x.CodDefeito == codDefeito);

                return postodefeito;
            }

        }

        public static void GravarPostoDefeito(CBPostoDefeitoDTO postodefeito)
        {
            using (var dtbCb = new DTB_CBContext())
            {
                var novodef = new tbl_CBPostoDefeito();

                AutoMapper.Mapper.Map(postodefeito, novodef);

                dtbCb.tbl_CBPostoDefeito.Add(novodef);
                dtbCb.SaveChanges();
            }
        }

        public static void RemoverPostoDefeito(CBPostoDefeitoDTO defeito)
        {

            using (var dtbCb = new DTB_CBContext())
            {
                var def = dtbCb.tbl_CBPostoDefeito.FirstOrDefault(x => x.CodDefeito == defeito.CodDefeito && x.CodLinha == defeito.CodLinha && x.NumPosto == defeito.NumPosto);

                if (def != null)
                {
                    dtbCb.tbl_CBPostoDefeito.Remove(def);

                    dtbCb.SaveChanges();
                }

            }
        }
        #endregion

        #region CNQ
        /// <summary>
        /// Método que consulta no banco de dados os principais defeitos para o relatório do CNQ
        /// </summary>
        /// <param name="DatInicio">Data início do filtro</param>
        /// <param name="DatFim">Data fim do filtro</param>
        /// <param name="Tipo">Tipo de Defeito (Máquina/Mão-de-Obra/Material)</param>
        /// <param name="Fabrica">Setor verificado</param>
        /// <param name="Familia">Família de produto a ser classificada</param>
        /// <returns>Lista de defeitos (Classe PrincipaisDefeitos)</returns>
        public static List<PrincipaisDefeitos> ObterDefeitosCNQ(DateTime DatInicio, DateTime DatFim, string Tipo, string Fabrica, string Familia, string Modelo)
        {
            using (var dtbCb = new DTB_CBContext())
            {
                dtbCb.Database.Log = Console.Write;

                var query = (from defeito in dtbCb.tbl_CBDefeito
                             join item in dtbCb.tbl_Item on defeito.NumECB.Substring(0, 6) equals item.CodItem
                             join linha in dtbCb.tbl_CBLinha on defeito.CodLinha equals linha.CodLinha
                             join causa in dtbCb.tbl_CBCadCausa on defeito.CodCausa equals causa.CodCausa into causa2
                             from leftCausa in causa2.DefaultIfEmpty()
                             join def in dtbCb.tbl_CBCadDefeito on
                                 defeito.CodDefeito.Replace("UM", "0").Replace("UI", "0").Replace("LM", "0").Replace("SF", "0") equals
                                 def.CodDefeito into def2
                             from leftDefeito in def2.DefaultIfEmpty()
                             join origem in dtbCb.tbl_CBCadOrigem on defeito.CodOrigem equals origem.CodOrigem into origem2
                             from leftOrigem in origem2.DefaultIfEmpty()
                             where (defeito.DatRevisado >= DatInicio && defeito.DatRevisado <= DatFim)
                                   && defeito.FlgTipo.Equals(Tipo)
                                   && defeito.FlgRevisado == 1
                                   && String.IsNullOrEmpty(defeito.NumSeriePai)
                             select new
                             {
                                 defeito.CodCausa,
                                 defeito.Posicao,
                                 item.CodItem,
                                 item.DesItem,
                                 leftCausa.DesCausa,
                                 leftDefeito.DesDefeito,
                                 leftOrigem.DesOrigem,
                                 linha.Setor,
                                 linha.CodLinhaT1
                             });

                if (!String.IsNullOrEmpty(Fabrica))
                {
                    query = query.Where(x => x.Setor.Equals(Fabrica));
                }

                if (!String.IsNullOrEmpty(Modelo))
                {
                    query = query.Where(x => x.CodItem.Equals(Modelo));
                }

                if (!String.IsNullOrEmpty(Familia))
                {
                    query = query.Where(x => x.CodLinhaT1.StartsWith(Familia));
                }

                var query2 = (from q in query
                              group q by new { q.CodCausa, q.Posicao, q.CodItem, q.DesItem, q.DesCausa, q.DesDefeito, q.DesOrigem } into grupo
                              select new PrincipaisDefeitos()
                              {
                                  CodModelo = grupo.Key.CodItem,
                                  DesModelo = grupo.Key.DesItem,
                                  Posicao = grupo.Key.Posicao,
                                  CodCausa = grupo.Key.CodCausa,
                                  DesCausa = grupo.Key.DesCausa,
                                  DesDefeito = grupo.Key.DesDefeito,
                                  DesOrigem = grupo.Key.DesOrigem,
                                  Qtde = grupo.Count()
                              });

                var resultado = query2.OrderByDescending(x => x.Qtde).Take(5).ToList();

                return resultado;
            }
        }


        public static List<ResumoDefeito> ObterResumoCNQ(DateTime DatInicio, DateTime DatFim, string Fabrica)
        {
            using (var dtbCb = new DTB_CBContext())
            {

                var resumo = (from defeito in dtbCb.tbl_CBDefeito
                              join linha in dtbCb.tbl_CBLinha on defeito.CodLinha equals linha.CodLinha
                              where defeito.DatEvento >= DatInicio
                                    && defeito.DatEvento <= DatFim
                                    && !String.IsNullOrEmpty(defeito.FlgTipo)
                                    && defeito.FlgRevisado != 0
                                    && defeito.FlgRevisado != 2
                                    && defeito.FlgRevisado != 4
                                    && linha.Setor.Equals(Fabrica)
                              group defeito by defeito.FlgTipo into grupo
                              select new ResumoDefeito()
                              {
                                  CodDefeito = grupo.Key,
                                  QtdDefeito = grupo.Count()
                              }).ToList();

                return resumo;
            }
        }

        public static List<ResumoDefeito> ObterResumoCNQ(DateTime DatInicio, DateTime DatFim, string Fabrica, string Familia)
        {
            using (var dtbCb = new DTB_CBContext())
            {

                var resumo = (from defeito in dtbCb.tbl_CBDefeito
                              join linha in dtbCb.tbl_CBLinha on defeito.CodLinha equals linha.CodLinha
                              where defeito.DatEvento >= DatInicio
                                    && defeito.DatEvento <= DatFim
                                    && !String.IsNullOrEmpty(defeito.FlgTipo)
                                    && defeito.FlgRevisado != 0
                                    && defeito.FlgRevisado != 2
                                    && defeito.FlgRevisado != 4
                                    && linha.Setor.Equals(Fabrica)
                                    && linha.Familia.Equals(Familia)
                              group defeito by defeito.FlgTipo into grupo
                              select new ResumoDefeito()
                              {
                                  CodDefeito = grupo.Key,
                                  QtdDefeito = grupo.Count()
                              }).ToList();

                return resumo;
            }
        }

        #endregion

        #region Causa
        public static string ObterDesCausa(String codCausa)
        {
            var causa = ObterCausa(codCausa);

            return causa != null ? causa.DesCausa : "";
        }

        public static List<CBCadCausaDTO> ObterCausas()
        {
            using (var dtbCb = new DTB_CBContext())
            {
                var causas = dtbCb.tbl_CBCadCausa.ToList();

                var causasDTO = new List<CBCadCausaDTO>();

                AutoMapper.Mapper.Map(causas, causasDTO);

                return causasDTO;
            }
        }

        public static CBCadCausaDTO ObterCausa(string codCausa)
        {
            using (var dtbCb = new DTB_CBContext())
            {
                var causas = dtbCb.tbl_CBCadCausa.FirstOrDefault(x => x.CodCausa == codCausa);

                var causasDTO = new CBCadCausaDTO();

                AutoMapper.Mapper.Map(causas, causasDTO);

                return causasDTO;
            }
        }

        public static void GravarCausa(CBCadCausaDTO causa)
        {

            using (var dtbCb = new DTB_CBContext())
            {
                var def = dtbCb.tbl_CBCadCausa.FirstOrDefault(x => x.CodCausa == causa.CodCausa);

                if (def != null)
                {
                    AutoMapper.Mapper.Map(causa, def);

                    dtbCb.SaveChanges();
                }
                else
                {
                    var novodef = new tbl_CBCadCausa();

                    AutoMapper.Mapper.Map(causa, novodef);

                    dtbCb.tbl_CBCadCausa.Add(novodef);
                    dtbCb.SaveChanges();
                }
            }
        }

        public static void RemoverCausa(CBCadCausaDTO causa)
        {

            using (var dtbCb = new DTB_CBContext())
            {
                var def = dtbCb.tbl_CBCadCausa.FirstOrDefault(x => x.CodCausa == causa.CodCausa);

                if (def == null) return;

                dtbCb.tbl_CBCadCausa.Remove(def);

                dtbCb.SaveChanges();
            }
        }

        public static int ObterUltimaCausa()
        {
            using (var dtbCb = new DTB_CBContext())
            {
                var defeito = dtbCb.tbl_CBCadCausa.OrderByDescending(x => x.CodCausa).FirstOrDefault(x => !x.CodCausa.StartsWith("WX"));

                if (defeito != null)
                {
                    return int.Parse(defeito.CodCausa.Replace("W", ""));
                }

                return 1;

            }

        }

        #endregion

        #region Origem
        public static CBCadOrigemDTO ObterOrigem(String CodOrigem)
        {
            if (!string.IsNullOrEmpty(CodOrigem))
            {
                using (var dtbCb = new DTB_CBContext())
                {
                    CodOrigem = CodOrigem.Trim();

                    var origem = (from c in dtbCb.tbl_CBCadOrigem
                                  where c.CodOrigem == CodOrigem
                                  select new CBCadOrigemDTO()
                                  {
                                      CodOrigem = c.CodOrigem,
                                      DesOrigem = c.DesOrigem,
                                      FlgAtivo = c.FlgAtivo
                                  }).FirstOrDefault();

                    return origem;
                }
            }

            return null;
        }

        public static string ObterDesOrigem(String CodOrigem)
        {
            if (!string.IsNullOrEmpty(CodOrigem))
            {
                using (var dtbCb = new DTB_CBContext())
                {
                    CodOrigem = CodOrigem.Trim();

                    string desorigem = (from c in dtbCb.tbl_CBCadOrigem
                                        where c.CodOrigem == CodOrigem
                                        select c.DesOrigem).FirstOrDefault();

                    if (String.IsNullOrEmpty(desorigem))
                    {
                        desorigem = "";
                    }

                    return desorigem;
                }
            }

            return "";
        }

        public static List<CBCadOrigemDTO> ObterOrigens()
        {
            using (var dtbCb = new DTB_CBContext())
            {
                var origens = dtbCb.tbl_CBCadOrigem.Select(x => new CBCadOrigemDTO() { CodOrigem = x.CodOrigem.Trim(), DesOrigem = x.DesOrigem }).ToList();

                origens = origens.OrderBy(x => int.Parse(x.CodOrigem)).ToList();

                return origens;
            }
        }

        public static void GravarOrigem(CBCadOrigemDTO origem)
        {

            using (var dtbCb = new DTB_CBContext())
            {
                var def = dtbCb.tbl_CBCadOrigem.FirstOrDefault(x => x.CodOrigem == origem.CodOrigem);

                if (def != null)
                {
                    def.DesOrigem = origem.DesOrigem;
                    def.FlgAtivo = origem.FlgAtivo;

                    dtbCb.SaveChanges();
                }
                else
                {
                    var novodef = new tbl_CBCadOrigem();

                    AutoMapper.Mapper.Map(origem, novodef);

                    dtbCb.tbl_CBCadOrigem.Add(novodef);
                    dtbCb.SaveChanges();
                }
            }
        }

        public static void RemoverOrigem(CBCadOrigemDTO origem)
        {

            using (var dtbCb = new DTB_CBContext())
            {
                var def = dtbCb.tbl_CBCadOrigem.FirstOrDefault(x => x.CodOrigem == origem.CodOrigem);

                if (def == null) return;

                dtbCb.tbl_CBCadOrigem.Remove(def);

                dtbCb.SaveChanges();
            }
        }

        public static int ObterUltimaOrigem()
        {
            using (var dtbCb = new DTB_CBContext())
            {
                var origens = dtbCb.tbl_CBCadOrigem.ToList();

                var origem = origens.OrderByDescending(x => int.Parse(x.CodOrigem)).FirstOrDefault();

                if (origem != null)
                {
                    return int.Parse(origem.CodOrigem);
                }

                return 1;

            }

        }

        #endregion

        #region SubDefeito

        public static List<CBCadSubDefeitoDTO> ObterSubDefeitos()
        {
            using (DTB_CBContext dtb_CB = new DTB_CBContext())
            {
                var subDefeitos = dtb_CB.tbl_CBCadSubDefeito.ToList();

                List<CBCadSubDefeitoDTO> subDefeitosDTO = new List<CBCadSubDefeitoDTO>();

                AutoMapper.Mapper.Map(subDefeitos, subDefeitosDTO);

                return subDefeitosDTO;
            }
        }

        public static Boolean GravarSubDefeito(CBCadSubDefeitoDTO defeito)
        {

            using (var dtbCb = new DTB_CBContext())
            {
                try
                {
                    var def = dtbCb.tbl_CBCadSubDefeito.FirstOrDefault(x => x.CodDefeito == defeito.CodDefeito && x.CodSubDefeito == defeito.CodSubDefeito);

                    if (def != null)
                    {
                        AutoMapper.Mapper.Map(defeito, def);

                        dtbCb.SaveChanges();

                        return true;
                    }
                    else
                    {
                        var novodef = new tbl_CBCadSubDefeito();

                        AutoMapper.Mapper.Map(defeito, novodef);

                        dtbCb.tbl_CBCadSubDefeito.Add(novodef);
                        dtbCb.SaveChanges();

                        return true;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static int ObterIndiceSubDefeito(string CodDefeito)
        {
            using (var dtb_CB = new DTB_CBContext())
            {

                var defeito = dtb_CB.tbl_CBCadSubDefeito.Where(x => x.CodDefeito == CodDefeito).ToList();

                if (defeito.Count() == 0)
                    return 0;

                var indice = dtb_CB.tbl_CBCadSubDefeito.Where(x => x.CodDefeito == CodDefeito).Max(x => x.CodSubDefeito);

                return indice;
            }
        }

        public static CBCadSubDefeitoDTO ObterCadSubDefeito(string codDefeito, string CodSubDefeito)
        {
            using (var dtbCb = new DTB_CBContext())
            {
                int indice = int.Parse(CodSubDefeito);
                var defeitos = dtbCb.tbl_CBCadSubDefeito.FirstOrDefault(x => x.CodDefeito == codDefeito && x.CodSubDefeito == indice);

                CBCadSubDefeitoDTO defeitosDTO = new CBCadSubDefeitoDTO();

                AutoMapper.Mapper.Map(defeitos, defeitosDTO);

                return defeitosDTO;
            }
        }

        public static List<CBCadSubDefeitoDTO> ObterSubDefeitos(string CodDefeito)
        {
            using (var dtb_CB = new DTB_CBContext())
            {
                var subDefeitos = dtb_CB.tbl_CBCadSubDefeito.Where(x => x.CodDefeito == CodDefeito && x.flgAtivo == "1").ToList();

                List<CBCadSubDefeitoDTO> subDefeitosDTO = new List<CBCadSubDefeitoDTO>();

                AutoMapper.Mapper.Map(subDefeitos, subDefeitosDTO);

                return subDefeitosDTO;
            }
        }

        public static int? ObterSubDefeitoSelecionado(string NumECB)
        {
            using (var dtb_CB = new DTB_CBContext())
            {
                var subDefeito = dtb_CB.tbl_CBDefeito.Where(x => x.NumECB == NumECB && x.FlgRevisado == 0).FirstOrDefault();

                if (subDefeito == null)
                    return 0;
                else if (subDefeito.CodSubDefeito == null)
                    return 0;
                else
                    return subDefeito.CodSubDefeito;
            }
        }

        #endregion

        #region INFORMATICA

        public static void ApagarPassagemInformatica(int CodLinha, int NumPosto, string NumSerie)
        {
            using (var contexto = new DTB_CBContext())
            {
                var listaTipoPostoParaApagar = new List<int>
                {
                    24,
                    25,
                    26,
                    32
                };

                var listaPostos = (from a in contexto.tbl_CBLinha
                                   join b in contexto.tbl_CBPosto on a.CodLinha equals b.CodLinha
                                   where a.CodLinha.Equals(CodLinha) && listaTipoPostoParaApagar.Contains(b.CodTipoPosto)
                                   select b.NumPosto).ToList();



                if (listaPostos.Count > 0)
                {
                    var listaDeletar =
                        contexto.tbl_CBPassagem.Where(a => a.NumECB == NumSerie && a.CodLinha == CodLinha && listaPostos.Contains(a.NumPosto))
                            .ToList();

                    contexto.tbl_CBPassagem_Hist.AddRange(listaDeletar.Select(s => new tbl_CBPassagem_Hist
                    {
                        CodDRT = s.CodDRT,
                        CodLinha = s.CodLinha,
                        DatEvento = s.DatEvento,
                        NumECB = s.NumECB,
                        NumPosto = s.NumPosto
                    }).ToList());

                    contexto.tbl_CBPassagem.RemoveRange(listaDeletar);

                    contexto.SaveChanges();

                }
            }
        }

        #endregion
    }
}
