using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_KcmNac
    {
        public string CodFab { get; set; }
        public System.DateTime Data { get; set; }
        public string CodItem { get; set; }
        public Nullable<decimal> QtdItem { get; set; }
        public Nullable<decimal> CustoA { get; set; }
        public Nullable<decimal> CustoB { get; set; }
        public System.Guid rowguid { get; set; }
    }
}
