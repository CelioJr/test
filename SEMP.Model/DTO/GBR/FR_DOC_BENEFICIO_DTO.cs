using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DOC_BENEFICIO_DTO
	{
        public string BEN_DESCRICAO { get; set; }
        public string BEN_VALOR { get; set; }
        public int DOC_CODIGO { get; set; }
        public virtual FR_DOC_PRINCIPAL_DTO FR_DOC_PRINCIPAL { get; set; }
    }
}
