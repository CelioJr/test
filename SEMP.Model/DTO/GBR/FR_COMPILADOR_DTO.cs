using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_COMPILADOR_DTO
    {
        public FR_COMPILADOR_DTO()
        {
            this.FR_COMPILADOR_DATABASE = new List<FR_COMPILADOR_DATABASE_DTO>();
        }

        public string CPL_DESCRITOR { get; set; }
        public string CPL_ESPECIFICACAO { get; set; }
        public virtual ICollection<FR_COMPILADOR_DATABASE_DTO> FR_COMPILADOR_DATABASE { get; set; }
    }
}
