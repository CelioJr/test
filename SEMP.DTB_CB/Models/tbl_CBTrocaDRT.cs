using System;
using System.Collections.Generic;

namespace SEMP.DTB_CB.Models
{
    public partial class tbl_CBTrocaDRT
    {
        public int CodLinha { get; set; }
        public Nullable<int> NumPosto { get; set; }
        public string CodDRT { get; set; }
        public Nullable<System.DateTime> DatTroca { get; set; }
        public string CodDRTAnt { get; set; }
        public virtual tbl_CBLinha tbl_CBLinha { get; set; }
    }
}
