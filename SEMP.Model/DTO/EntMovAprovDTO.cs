using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class EntMovAprovDTO
    {
        public int NumRomaneio { get; set; }
        public System.DateTime DatAprov { get; set; }
        public string UsuAprov { get; set; }
        public string MaqAprov { get; set; }
        public string CodStatus { get; set; }
    }
}
