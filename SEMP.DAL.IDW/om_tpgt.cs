namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_tpgt
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_tpgt()
        {
            om_cfg = new HashSet<om_cfg>();
            om_cfg1 = new HashSet<om_cfg>();
            om_gt = new HashSet<om_gt>();
        }

        [Key]
        public long id_tpgt { get; set; }

        [StringLength(30)]
        public string cd_tpgt { get; set; }

        public int? revisao { get; set; }

        [StringLength(100)]
        public string ds_tpgt { get; set; }

        public DateTime? dt_revisao { get; set; }

        public DateTime? dt_stativo { get; set; }

        public short? st_ativo { get; set; }

        public long id_usrrevisao { get; set; }

        public long id_usrstativo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_gt> om_gt { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }
    }
}
