namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_DOC_PRINCIPAL
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FR_DOC_PRINCIPAL()
        {
            FR_DOC_APROVACAO = new HashSet<FR_DOC_APROVACAO>();
            FR_DOC_ATOR = new HashSet<FR_DOC_ATOR>();
            FR_DOC_BENEFICIO = new HashSet<FR_DOC_BENEFICIO>();
            FR_DOC_CASO_USO = new HashSet<FR_DOC_CASO_USO>();
            FR_DOC_DIAGRAMA_CONTEXTO = new HashSet<FR_DOC_DIAGRAMA_CONTEXTO>();
            FR_DOC_FORMULARIO = new HashSet<FR_DOC_FORMULARIO>();
            FR_DOC_FORMULARIO_IMAGEM = new HashSet<FR_DOC_FORMULARIO_IMAGEM>();
            FR_DOC_INTERFACE = new HashSet<FR_DOC_INTERFACE>();
            FR_DOC_INTERPRETACAO_CAMPO = new HashSet<FR_DOC_INTERPRETACAO_CAMPO>();
            FR_DOC_LIMITE = new HashSet<FR_DOC_LIMITE>();
            FR_DOC_MATERIAL_REFERENCIA = new HashSet<FR_DOC_MATERIAL_REFERENCIA>();
            FR_DOC_MODO_OPERACAO = new HashSet<FR_DOC_MODO_OPERACAO>();
            FR_DOC_REGRAS_IMAGEM = new HashSet<FR_DOC_REGRAS_IMAGEM>();
            FR_DOC_RELATORIO = new HashSet<FR_DOC_RELATORIO>();
            FR_DOC_REQUISITO = new HashSet<FR_DOC_REQUISITO>();
            FR_DOC_RESTRICAO = new HashSet<FR_DOC_RESTRICAO>();
            FR_DOC_VERSAO = new HashSet<FR_DOC_VERSAO>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DOC_CODIGO { get; set; }

        [StringLength(500)]
        public string DOC_AUTORIA { get; set; }

        [StringLength(3)]
        public string SIS_CODIGO { get; set; }

        [StringLength(10)]
        public string DOC_DATA { get; set; }

        [StringLength(255)]
        public string DOC_EMPRESA { get; set; }

        [StringLength(255)]
        public string DOC_LOCAL { get; set; }

        public byte[] DOC_MODELO_DADOS { get; set; }

        public string DOC_OBJETIVO { get; set; }

        public string DOC_MISSAO { get; set; }

        public string DOC_COMPONENTES { get; set; }

        public string DOC_VISAO_GERAL { get; set; }

        public string DOC_CONVENCOES { get; set; }

        public string DOC_OBSERVACOES { get; set; }

        public string DOC_USUARIO_DESCRICAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_APROVACAO> FR_DOC_APROVACAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_ATOR> FR_DOC_ATOR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_BENEFICIO> FR_DOC_BENEFICIO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_CASO_USO> FR_DOC_CASO_USO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_DIAGRAMA_CONTEXTO> FR_DOC_DIAGRAMA_CONTEXTO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_FORMULARIO> FR_DOC_FORMULARIO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_FORMULARIO_IMAGEM> FR_DOC_FORMULARIO_IMAGEM { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_INTERFACE> FR_DOC_INTERFACE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_INTERPRETACAO_CAMPO> FR_DOC_INTERPRETACAO_CAMPO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_LIMITE> FR_DOC_LIMITE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_MATERIAL_REFERENCIA> FR_DOC_MATERIAL_REFERENCIA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_MODO_OPERACAO> FR_DOC_MODO_OPERACAO { get; set; }

        public virtual FR_SISTEMA FR_SISTEMA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_REGRAS_IMAGEM> FR_DOC_REGRAS_IMAGEM { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_RELATORIO> FR_DOC_RELATORIO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_REQUISITO> FR_DOC_REQUISITO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_RESTRICAO> FR_DOC_RESTRICAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_VERSAO> FR_DOC_VERSAO { get; set; }
    }
}
