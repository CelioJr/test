﻿using RAST.BO.PM;
using SEMP.BO;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.DTO;
using SEMP.Model.VO.Rastreabilidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAST.BO.Rastreabilidade
{
	public class TempoAtravessamentoBO
	{

		public static List<TempoAtravessamento> ObterAtravessamento(DateTime datInicio, DateTime datFim, string codModelo)
		{

			var dados = daoTempoAtravessamento.ObterAtravessamento(datInicio, datFim, codModelo);

			return dados;

		}

		public static List<TempoAtravessamentoResumo> ObterAtravessamentoResumo(DateTime datInicio, DateTime datFim, string codModelo)
		{

			var dados = daoTempoAtravessamento.ObterAtravessamentoResumo(datInicio, datFim, codModelo);

			return dados;

		}

		public static List<TempoAtravessamentoResumo> ObterAtravessamentoSerie(string numSerie)
		{

			var dados = daoTempoAtravessamento.ObterAtravessamentoSerie(numSerie);

			return dados;

		}

		public static List<TempoAtravessamentoModelo> ObterAtravessamentoSerieDados(string numSerie)
		{

			var dados = daoTempoAtravessamento.ObterAtravessamentoSerieDados(numSerie);

			return dados;

		}

		public static List<TempoAtravessamentoResumoAP> ObterAtravessamentoResumoAP(string flgDia, string flgDefeitos, string flgFaltaLeitura, string codFab, string numAP)
		{

			var dados = daoTempoAtravessamento.ObterAtravessamentoResumoAP(flgDia, flgDefeitos, flgFaltaLeitura, codFab, numAP);

			var ap = Geral.ObterAP(codFab, numAP);

			dados.ForEach(x =>
			{
				x.QtdLoteAP = ap.QtdLoteAP;
				x.QtdApontada = ap.QtdProduzida;
				x.DatAbertura = ap.DatAP;
				x.DatLiberacao = ap.DatLiberacao;
				x.DatSeparacao = ap.DatAceite;
				x.DatProducao = ap.DatInicioProd;
				x.DatEncerramento = ap.DatFechamento;
				x.TempoAP = ap.DatFechamento.HasValue ? ObterDifHoraSemLinha(ap.DatAP, ap.DatFechamento.Value) : 0;
			});

			return dados;

		}

		public static List<TempoAtravessamentoModelo> ObterAtravessamentoModelo(DateTime datInicio, DateTime datFim, string codModelo)
		{

			var dados = daoTempoAtravessamento.ObterAtravessamentoModelo(datInicio, datFim, codModelo);

			return dados;

		}

		public static List<TempoAtravessamentoResumo> ObterAtravessamentoMes(DateTime datInicio, DateTime datFim, string codModelo)
		{
			if (codModelo == null || codModelo == "")
			{
				return new List<TempoAtravessamentoResumo>();
			}

			datInicio = datInicio.AddDays(-datInicio.Day).AddDays(1);
			datFim = DateTime.Parse(DateTime.Now.ToShortDateString());

			var dados = new List<TempoAtravessamentoResumo>();

			while (datInicio <= datFim)
			{
				var dado = daoTempoAtravessamento.ObterAtravessamentoResumo(datInicio, datInicio, codModelo);
				if (dado != null && dado.Count() > 0)
				{
					dados.AddRange(dado);
				}

				datInicio = datInicio.AddDays(1);
			}

			return dados;

		}

		public static List<CBTAModeloDTO> ObterMediaMensal(DateTime datInicio, DateTime datFim, string codModelo)
		{
			if (codModelo == null || codModelo == "")
			{
				return new List<CBTAModeloDTO>();
			}

			var dados = daoTempoAtravessamento.ObterResumo(datInicio, datFim, codModelo);

			return dados;

		}

		public static GraficoTempoAtravessamento ObterAtravessamentoProduto(DateTime datInicio, string codModelo)
		{
			try
			{

				var datFim = datInicio.AddDays(1).AddMinutes(-1);

				var tempoAtravessamento = daoTempoAtravessamento.ObterAtravessamentoResumo(datInicio, datFim, codModelo);

				if (tempoAtravessamento.Any())
				{

					var tempoPadraoIAC = 0m;
					var tempoPadraoIMC = 0m;
					var tempoPadraoFEC = 0m;

					var tempoFEC = 0m;
					var tempoIMC = 0m;
					var tempoIAC = 0m;

					var registro = tempoAtravessamento.FirstOrDefault();

					var modeloIAC = Geral.ObterCodModeloIAC(registro.CodModelo);
					var modeloSMD = Geral.ObterCodModeloSMD(modeloIAC);

					tempoPadraoIAC = modeloSMD == null && modeloIAC == null ? 0 : TempoPadrao.ObterTempoPadraoIDW(modeloSMD ?? modeloIAC).TempoPadrao ?? 0;
					var t = TempoPadrao.ObterTempoPadrao(registro.CodModelo);
					tempoPadraoIMC = t != null ? (t.TempoPadrao ?? 0) * 60 : 0;
					tempoPadraoFEC = (TempoPadrao.ObterTempoPadrao(registro.CodModelo).TempoPadrao ?? 0) * 60;

					//var agrupado = 0;//tempoAtravessamento.GroupBy(x => x.).Select(y => new { Fase = y.Key, TempoMedio = (decimal?)y.Sum(z => z.TempoEntradaSaida) / y.Count() });

					/*if (agrupado.Any(x => x.Fase == "FEC")) { tempoFEC = (int)agrupado.FirstOrDefault(x => x.Fase == "FEC").TempoMedio.Value; }
					if (agrupado.Any(x => x.Fase == "IMC")) { tempoIMC = (int)agrupado.FirstOrDefault(x => x.Fase == "IMC").TempoMedio.Value; }
					if (agrupado.Any(x => x.Fase == "IAC")) { tempoIAC = (int)agrupado.FirstOrDefault(x => x.Fase == "IAC").TempoMedio.Value; }*/

					var dados = new GraficoTempoAtravessamento()
					{
						CodModelo = registro.CodModelo,
						DesModelo = daoGeral.ObterDescricaoItem(registro.CodModelo),
						CodPlaca = registro.CodModelo,
						DesPlaca = daoGeral.ObterDescricaoItem(registro.CodModelo),
						TempoPadrao = (int)(tempoPadraoIAC + tempoPadraoIMC + tempoPadraoFEC),
						//TempoAtravessamento = (int)(agrupado.Sum(x => x.TempoMedio) ?? 0),

						TempoFEC = tempoFEC,
						TempoIMC = tempoIMC,
						TempoIAC = tempoIAC,

						TempoPadraoFEC = (int)tempoPadraoFEC,
						TempoPadraoIMC = (int)tempoPadraoIMC,
						TempoPadraoIAC = (int)tempoPadraoIAC
					};

					return dados;
				}
				else
				{

					var tempoPadraoIAC = 0m;
					var tempoPadraoIMC = 0m;
					var tempoPadraoFEC = 0m;

					var dados = new GraficoTempoAtravessamento()
					{
						CodModelo = codModelo,
						DesModelo = daoGeral.ObterDescricaoItem(codModelo),
						TempoPadrao = (int)(tempoPadraoIAC + tempoPadraoIMC + tempoPadraoFEC),
						TempoAtravessamento = 0,

						TempoFEC = 0,
						TempoIMC = 0,
						TempoIAC = 0,

						TempoPadraoFEC = (int)tempoPadraoFEC,
						TempoPadraoIMC = (int)tempoPadraoIMC,
						TempoPadraoIAC = (int)tempoPadraoIAC
					};

					return dados;

				}

			}
			catch (Exception)
			{
				return new GraficoTempoAtravessamento();
			}
		}

		public static decimal ObterDifHora(DateTime datInicio, DateTime datFim, int codLinha)
		{

			if (datInicio == datFim) {
				var linha = Geral.ObterLinha(codLinha);

				if (linha.Setor == "IAC" && linha.Familia == "BOT"){
					datInicio = datInicio.AddMinutes(-12);
				}
				else if (linha.Setor == "IAC" && (linha.Familia == "AXI" || linha.Familia == "RAD"))
				{
					datInicio = datInicio.AddMinutes(-4);
				}

			}

			return daoTempoAtravessamento.ObterDifHora(datInicio, datFim, codLinha);

		}

		public static decimal ObterDifHoraSemLinha(DateTime datInicio, DateTime datFim)
		{

			return daoTempoAtravessamento.ObterDifHoraSemLinha(datInicio, datFim);

		}

		public static List<string> ObterAPsModeloData(string codModelo, DateTime datReferencia)
		{
			return daoTempoAtravessamento.ObterAPsModeloData(codModelo, datReferencia);

		}

		public static List<FeriadosDTO> ObterFeriados()
		{

			return daoTempoAtravessamento.ObterFeriados();
		}

		public static List<FeriadosDTO> ObterFeriados(DateTime datInicio, DateTime datFim)
		{

			return daoTempoAtravessamento.ObterFeriados(datInicio, datFim);
		}

		#region Nova Versão

		public static List<CBTAModeloDTO> ObterResumo(DateTime datInicio, DateTime datFim)
		{

			return daoTempoAtravessamento.ObterResumo(datInicio, datFim);

		}

		public static List<CBTAFaseDTO> ObterResumoFase(DateTime datInicio, DateTime datFim)
		{

			return daoTempoAtravessamento.ObterResumoFase(datInicio, datFim);

		}

		public static List<CBTAItemDTO> ObterResumoItem(string numECB)
		{

			return daoTempoAtravessamento.ObterResumoItem(numECB);

		}

		public static List<CBTAItemDTO> ObterResumoItem(DateTime datInicio, DateTime datFim, string codModelo)
		{

			return daoTempoAtravessamento.ObterResumoItem(datInicio, datFim, codModelo);

		}

		public static List<CBTAItemDTO> ObterResumoItem(DateTime datInicio, DateTime datFim, string codModelo, int menor, int maior, string flgTipoGrafico)
		{
			var dados = daoTempoAtravessamento.ObterResumoItem(datInicio, datFim, codModelo);

			var resumo = !String.IsNullOrEmpty(flgTipoGrafico) && flgTipoGrafico == "P" ?
							dados.Where(x => !x.SubFase.StartsWith("WIP")).GroupBy(x => x.NumSerieModelo).Select(x => new { NumSerieModelo = x.Key, TempoAtravessa = x.Sum(y => y.TempoAtravessa) }).ToList() :
							flgTipoGrafico == "W" ?
							dados.Where(x => x.SubFase.StartsWith("WIP")).GroupBy(x => x.NumSerieModelo).Select(x => new { NumSerieModelo = x.Key, TempoAtravessa = x.Sum(y => y.TempoAtravessa) }).ToList() :
							dados.GroupBy(x => x.NumSerieModelo).Select(x => new { NumSerieModelo = x.Key, TempoAtravessa = x.Sum(y => y.TempoAtravessa) }).ToList()
							;

			resumo = resumo.Where(x => x.TempoAtravessa >= menor && x.TempoAtravessa <= maior).ToList();

			dados = dados.Where(x => resumo.Select(y => y.NumSerieModelo).ToList().Contains(x.NumSerieModelo)).ToList();

			return dados;

		}

		public static List<CBTATempoItemDTO> ObterTempoItem(string numECB)
		{

			return daoTempoAtravessamento.ObterTempoItem(numECB);

		}

		public static List<CBTAItemDTO> ObterTempoItem(DateTime datInicio, DateTime datFim, string codModelo, string flgTipoGrafico, string flgFase)
		{
			// pega os dados do banco de dados
			var dados = daoTempoAtravessamento.ObterResumoItem(datInicio, datFim, codModelo);

			if (dados != null && dados.Count() > 0)
			{
				// filtra os dados conforme o parâmetro informado para o tipo de gráfico (P-produção ou W-WIP)
				var producao = !String.IsNullOrEmpty(flgTipoGrafico) && flgTipoGrafico == "P" ?
								dados.Where(x => !x.SubFase.StartsWith("WIP"))
									.GroupBy(x => new { x.CodModelo, x.NumSerieModelo })
									.Select(x => new
									{
										CodModelo = x.Key.CodModelo,
										NumSerieModelo = x.Key.NumSerieModelo,
										TempoAtravessa = x.Sum(y => y.TempoAtravessa)
									}).ToList()
									:
								flgTipoGrafico == "W" ?
								dados.Where(x => x.SubFase.StartsWith("WIP"))
									.GroupBy(x => new { x.CodModelo, x.NumSerieModelo })
									.Select(x => new
									{
										CodModelo = x.Key.CodModelo,
										NumSerieModelo = x.Key.NumSerieModelo,
										TempoAtravessa = x.Sum(y => y.TempoAtravessa)
									})
									:
									dados.GroupBy(x => new { x.CodModelo, x.NumSerieModelo })
									.Select(x => new
									{
										CodModelo = x.Key.CodModelo,
										NumSerieModelo = x.Key.NumSerieModelo,
										TempoAtravessa = x.Sum(y => y.TempoAtravessa)
									});

				if (!String.IsNullOrEmpty(flgFase))
				{
					dados = dados.Where(x => x.SubFase == flgFase).ToList();

				}

				// Agrupa os dados para encontrar os valores Média, Menor, Maior, Quantidade de Registros e o K
				var resumoProducao = producao.GroupBy(x => x.CodModelo)
											 .Select(x => new
											 {
												 CodModelo = x.Key,
												 k = Math.Round(Convert.ToDouble((x.Max(y => y.TempoAtravessa) - x.Min(y => y.TempoAtravessa)) / Math.Sqrt(x.Count())), 0)
											 })
											 .ToList();

				// de posse dos dados resumidos, encontra as classes para cada número de série
				var classesProducao = (from d in producao
									   join r in resumoProducao on d.CodModelo equals r.CodModelo
									   select new
									   {
										   d.NumSerieModelo,
										   Classes = Math.Ceiling(Convert.ToDouble(d.TempoAtravessa) / r.k)
									   }).ToList();

				foreach (var item in dados)
				{
					var classe = classesProducao.FirstOrDefault(x => x.NumSerieModelo == item.NumSerieModelo);
					if (classe != null){
						item.Classe = classe.Classes.ToString();
					}
				}

				return dados;
			}

			return null;

		}

		public static List<GraficoHistograma> ObterHistograma(DateTime datInicio, DateTime datFim, string codModelo, string flgTipoGrafico, string flgFase)
		{
			// pega os dados do banco de dados
			var dados = daoTempoAtravessamento.ObterResumoItem(datInicio, datFim, codModelo);

			if (dados != null && dados.Count() > 0)
			{

				// filtra os dados conforme o parâmetro informado para o tipo de gráfico (P-produção ou W-WIP)
				var producao = !String.IsNullOrEmpty(flgTipoGrafico) && flgTipoGrafico == "P" ?
								dados.Where(x => !x.SubFase.StartsWith("WIP"))
									.GroupBy(x => new { x.CodModelo, x.NumSerieModelo })
									.Select(x => new
									{
										CodModelo = x.Key.CodModelo,
										NumSerieModelo = x.Key.NumSerieModelo,
										TempoAtravessa = x.Sum(y => y.TempoAtravessa)
									}).ToList()
									:
								flgTipoGrafico == "W" ?
								dados.Where(x => x.SubFase.StartsWith("WIP"))
									.GroupBy(x => new { x.CodModelo, x.NumSerieModelo })
									.Select(x => new
									{
										CodModelo = x.Key.CodModelo,
										NumSerieModelo = x.Key.NumSerieModelo,
										TempoAtravessa = x.Sum(y => y.TempoAtravessa)
									})
									:
									dados.GroupBy(x => new { x.CodModelo, x.NumSerieModelo })
									.Select(x => new
									{
										CodModelo = x.Key.CodModelo,
										NumSerieModelo = x.Key.NumSerieModelo,
										TempoAtravessa = x.Sum(y => y.TempoAtravessa)
									});
			
				if(!String.IsNullOrEmpty(flgFase)){
					dados = dados.Where(x => x.SubFase == flgFase).ToList();

				}

				// Agrupa os dados para encontrar os valores Média, Menor, Maior, Quantidade de Registros e o K
				var resumoProducao = producao.GroupBy(x => x.CodModelo)
											 .Select(x => new
											 {
												 CodModelo = x.Key,
												 TempoAtravessa = x.Average(y => y.TempoAtravessa),
												 Menor = x.Min(y => y.TempoAtravessa),
												 Maior = x.Max(y => y.TempoAtravessa),
												 QtdRegistros = x.Count(),
												 k = Math.Round(Convert.ToDouble((x.Max(y => y.TempoAtravessa) - x.Min(y => y.TempoAtravessa)) / Math.Sqrt(x.Count())), 0)
											 })
											 .ToList();

				// de posse dos dados resumidos, encontra as classes para cada número de série
				var classesProducao = (from d in producao
									   join r in resumoProducao on d.CodModelo equals r.CodModelo
									   select new
									   {
										   d.CodModelo,
										   d.NumSerieModelo,
										   d.TempoAtravessa,
										   r.k,
										   r.Menor,
										   r.Maior,
										   Classes = Math.Ceiling(Convert.ToDouble(d.TempoAtravessa) / r.k)-1
									   }).ToList();

				// gera o resultado final contando a quantidade de aparelhos por classe
				var resultadoProducao = classesProducao.GroupBy(x => new { x.CodModelo, x.Classes, x.k })
										.Select(x => new
										{
											CodModelo = x.Key.CodModelo,
											Classes = x.Key.Classes,
											QtdProdutos = x.Count(),
											k = x.Key.k,
											DesClasse = String.Format("{0}-{1}", x.Key.Classes * x.Key.k, (x.Key.Classes * x.Key.k) + x.Key.k)
										})
										.OrderBy(x => x.Classes)
										.ToList();

				var resultadoFinal = resultadoProducao.Select(x => new GraficoHistograma()
				{
					id = (int)x.Classes,
					DesClasse = x.DesClasse,
					Valor = x.QtdProdutos
				}).OrderBy(x => x.id).ToList();


				return resultadoFinal;
			}


			return null;

		}

		public static List<GraficoHistograma> ObterHistogramaAtravessa(DateTime datInicio, DateTime datFim, string codModelo, string flgTipoGrafico, string flgFase)
		{
			// pega os dados do banco de dados
			var dados = daoTempoAtravessamento.ObterResumoItem(datInicio, datFim, codModelo);

			if (dados != null && dados.Count() > 0)
			{
				dados = dados.Where(x => x.Incompleto == 0 || x.QtdDefeito == 0).ToList();

				// filtra os dados conforme o parâmetro informado para o tipo de gráfico (P-produção ou W-WIP)
				var producao = !String.IsNullOrEmpty(flgTipoGrafico) && flgTipoGrafico == "P" ?
								dados.Where(x => !x.SubFase.StartsWith("WIP"))
									.GroupBy(x => new { x.CodModelo, x.NumSerieModelo })
									.Select(x => new
									{
										CodModelo = x.Key.CodModelo,
										NumSerieModelo = x.Key.NumSerieModelo,
										TempoAtravessa = x.Sum(y => y.TempoAtravessa)
									}).ToList()
									:
								flgTipoGrafico == "W" ?
								dados.Where(x => x.SubFase.StartsWith("WIP"))
									.GroupBy(x => new { x.CodModelo, x.NumSerieModelo })
									.Select(x => new
									{
										CodModelo = x.Key.CodModelo,
										NumSerieModelo = x.Key.NumSerieModelo,
										TempoAtravessa = x.Sum(y => y.TempoAtravessa)
									})
									:
									dados.GroupBy(x => new { x.CodModelo, x.NumSerieModelo })
									.Select(x => new
									{
										CodModelo = x.Key.CodModelo,
										NumSerieModelo = x.Key.NumSerieModelo,
										TempoAtravessa = x.Sum(y => y.TempoAtravessa)
									});

				if (!String.IsNullOrEmpty(flgFase))
				{
					dados = dados.Where(x => x.SubFase == flgFase).ToList();

				}

				// Agrupa os dados para encontrar os valores Média, Menor, Maior, Quantidade de Registros e o K
				var resumoProducao = producao.GroupBy(x => x.CodModelo)
											 .Select(x => new
											 {
												 CodModelo = x.Key,
												 TempoAtravessa = x.Average(y => y.TempoAtravessa),
												 Menor = x.Min(y => y.TempoAtravessa),
												 Maior = x.Max(y => y.TempoAtravessa),
												 QtdRegistros = x.Count(),
												 k = Math.Round(Convert.ToDouble((x.Max(y => y.TempoAtravessa) - x.Min(y => y.TempoAtravessa)) / Math.Sqrt(x.Count())), 0)
											 })
											 .ToList();

				// de posse dos dados resumidos, encontra as classes para cada número de série
				var classesProducao = (from d in producao
									   join r in resumoProducao on d.CodModelo equals r.CodModelo
									   select new
									   {
										   d.CodModelo,
										   d.NumSerieModelo,
										   d.TempoAtravessa,
										   r.k,
										   r.Menor,
										   r.Maior,
										   Classes = Math.Ceiling(Convert.ToDouble(d.TempoAtravessa) / r.k) - 1
									   }).ToList();

				// gera o resultado final contando a quantidade de aparelhos por classe
				var resultadoProducao = classesProducao.GroupBy(x => new { x.CodModelo, x.Classes, x.k })
										.Select(x => new
										{
											CodModelo = x.Key.CodModelo,
											Classes = x.Key.Classes,
											QtdProdutos = x.Count(),
											k = x.Key.k,
											DesClasse = String.Format("{0}-{1}", x.Key.Classes * x.Key.k, ((x.Key.Classes * x.Key.k) + x.Key.k)-1)
										})
										.OrderBy(x => x.Classes)
										.ToList();
				
				var maiorClasse = resultadoProducao.FirstOrDefault(y => y.QtdProdutos == resultadoProducao.Max(x => x.QtdProdutos)).Classes;
				
				var resultadoFinal = resultadoProducao.Select(x => new GraficoHistograma()
				{
					id = (int)x.Classes,
					DesClasse = x.DesClasse + ((x.Classes >= maiorClasse - 3 && x.Classes <= maiorClasse + 3) ? "*": ""),
					Valor = x.QtdProdutos
				}).OrderBy(x => x.id).ToList();


				return resultadoFinal;
			}


			return null;

		}

		public static List<CBTAModeloDTO> ObterResumoModelos(DateTime datInicio, DateTime datFim) 
		{
			return daoTempoAtravessamento.ObterResumoModelos(datInicio, datFim);
		}

		public static List<CBTAMetasDTO> ObterMetas(string codModelo)
		{
			return daoTempoAtravessamento.ObterMetas(codModelo);
		}

		#endregion

	}

}
