using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_SESSAO_VI_DTO
    {
        public int SES_CONEXAO { get; set; }
        public Nullable<System.DateTime> SES_DATAHORA_LOGIN { get; set; }
        public string SES_USUARIO { get; set; }
        public string SES_NOME_USUARIO { get; set; }
        public string SES_NOME_MAQUINA { get; set; }
        public string SES_END_IP { get; set; }
        public string SIS_CODIGO { get; set; }
    }
}
