﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAST.BO.Rastreabilidade
{
	class Mascara
	{

		public enum enumNivel
		{
			Forte,
			Generico
		}

		public class ExpressaoRegular
		{
			public String Pattern { get; set; }

			public Boolean Valida { get; set; } //Indica se a expressão encontrada foi valida para todas as amostras

			public enumNivel Nivel { get; set; } //nível da expressão

			public int QtdAmostrasAfetadas { get; set; }
		}
		
		
		public class Amostra
		{
			public String CodItem { get; set; }

			public String Leitura { get; set; }

			public List<ExpressaoRegular> lstExpressaoRegular { get; set; }

			/// <summary>
			/// True => se a amostra está sendo obtida atraves da importação do dtb_sim; False => se a amostra está sendo obtida de qualquer outro meio.
			/// </summary>
			public Boolean Importada { get; set; }

			public Boolean Valida { get; set; }

			public Amostra()
			{
				Valida = false;
			}
		}		

	}
}
