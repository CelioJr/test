using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_LSAAPMap : EntityTypeConfiguration<tbl_LSAAP>
    {
        public tbl_LSAAPMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.NumAP, t.DatEmissao, t.DatAP, t.CodProcesso, t.CodModelo, t.CodLocal });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NumAP)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodProcesso)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodCinescopio)
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.Sequencia)
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodRevisao)
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodRadial)
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodRevRadial)
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodAxial)
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodRevAxial)
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodLocal)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.CodStatus)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodLinha)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.Observacao)
                .HasMaxLength(40)
                .IsUnicode(false);

            this.Property(t => t.TipAP)
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.CodLocalAP)
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.NomAceite)
                .HasMaxLength(15)
                .IsUnicode(false);

            this.Property(t => t.FlgDivida)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.NumSerieINI)
                .HasMaxLength(13)
                .IsUnicode(false);

            this.Property(t => t.NumSerieFIN)
                .HasMaxLength(13)
                .IsUnicode(false);

            this.Property(t => t.CodSO)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.FlgToshiba)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.FlgTipoVenda)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_LSAAP");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.NumAP).HasColumnName("NumAP");
            this.Property(t => t.DatEmissao).HasColumnName("DatEmissao");
            this.Property(t => t.DatAP).HasColumnName("DatAP");
            this.Property(t => t.DatReferencia).HasColumnName("DatReferencia");
            this.Property(t => t.CodProcesso).HasColumnName("CodProcesso");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.CodCinescopio).HasColumnName("CodCinescopio");
            this.Property(t => t.QtdLoteAP).HasColumnName("QtdLoteAP");
            this.Property(t => t.QtdProduzida).HasColumnName("QtdProduzida");
            this.Property(t => t.Sequencia).HasColumnName("Sequencia");
            this.Property(t => t.CodRevisao).HasColumnName("CodRevisao");
            this.Property(t => t.CodRadial).HasColumnName("CodRadial");
            this.Property(t => t.CodRevRadial).HasColumnName("CodRevRadial");
            this.Property(t => t.CodAxial).HasColumnName("CodAxial");
            this.Property(t => t.CodRevAxial).HasColumnName("CodRevAxial");
            this.Property(t => t.CodLocal).HasColumnName("CodLocal");
            this.Property(t => t.CodStatus).HasColumnName("CodStatus");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.Observacao).HasColumnName("Observacao");
            this.Property(t => t.TipAP).HasColumnName("TipAP");
            this.Property(t => t.CodLocalAP).HasColumnName("CodLocalAP");
            this.Property(t => t.DatLiberacao).HasColumnName("DatLiberacao");
            this.Property(t => t.DatAceite).HasColumnName("DatAceite");
            this.Property(t => t.DatFechamento).HasColumnName("DatFechamento");
            this.Property(t => t.DatProgInicio).HasColumnName("DatProgInicio");
            this.Property(t => t.DatProgFinal).HasColumnName("DatProgFinal");
            this.Property(t => t.NomAceite).HasColumnName("NomAceite");
            this.Property(t => t.DatInicioProd).HasColumnName("DatInicioProd");
            this.Property(t => t.FlgDivida).HasColumnName("FlgDivida");
            this.Property(t => t.QtdEmbalado).HasColumnName("QtdEmbalado");
            this.Property(t => t.NumSerieINI).HasColumnName("NumSerieINI");
            this.Property(t => t.NumSerieFIN).HasColumnName("NumSerieFIN");
            this.Property(t => t.CodSO).HasColumnName("CodSO");
            this.Property(t => t.FlgToshiba).HasColumnName("FlgToshiba");
            this.Property(t => t.FlgTipoVenda).HasColumnName("FlgTipoVenda");
        }
    }
}
