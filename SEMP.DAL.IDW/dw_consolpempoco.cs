namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolpempoco
    {
        [Key]
        public long id_consolpempoco { get; set; }

        public long id_consolpemplog { get; set; }

        public long id_consolpemp { get; set; }

        public virtual dw_consolpemp dw_consolpemp { get; set; }

        public virtual dw_consolperdamplog dw_consolperdamplog { get; set; }
    }
}
