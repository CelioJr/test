﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.PM;
using SEMP.Model.VO.PM;

namespace SEMP.Areas.Outros.Controllers
{
	public class ProgramaDiaController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult Lista(string mesRef, string fase, string codlinha, string familia, string codModelo)
		{
			var dMes = DateTime.Parse(DateTime.Now.AddDays(-DateTime.Now.Day).AddDays(1).ToShortDateString());

			if (!String.IsNullOrEmpty(mesRef))
			{
				dMes = DateTime.Parse(mesRef);
			}
			
			var programa = ProgramaProducao.ObterProgramacao(dMes, fase, codlinha, familia);

			if (!String.IsNullOrEmpty(codModelo)) {
				programa = programa.Where(x => x.CodModelo == codModelo).ToList();

			}

			var modelos = programa.GroupBy(x => new {x.CodFab, x.CodModelo, x.DesModelo, x.CodLinha, x.DesLinha, x.NumTurno, x.Familia, x.QtdCapac, x.PlanoMes, x.FlagTP})
								  .Select(x => new ProgModelos()
												{
													CodFab = x.Key.CodFab,
													CodLinha = x.Key.CodLinha,
													CodModelo = x.Key.CodModelo,
													DesModelo = x.Key.DesModelo,
                                                    FlagTP = x.Key.FlagTP,
													DesLinha = x.Key.DesLinha,
													NumTurno = x.Key.NumTurno,
													QtdPrograma = x.Sum(y => y.QtdProducao ?? y.QtdProdPm),
													Familia = x.Key.Familia,
													QtdCapacidade = x.Key.QtdCapac,
													QtdPlanoMes = x.Key.PlanoMes
												});

			ViewBag.Programa = programa;
			ViewBag.Data = dMes;
			return View(modelos);
		}

		public ActionResult Meses(string mesRef, string fase, string codlinha, string familia, string codModelo)
		{
			var dMes = DateTime.Parse(DateTime.Now.AddDays(-DateTime.Now.Day).AddDays(1).ToShortDateString());

			if (!String.IsNullOrEmpty(mesRef))
			{
				dMes = DateTime.Parse(mesRef);
			}

			var meses = fase.Equals("F") ? ProgramaProducao.ObterMesesPrograma(dMes) : ProgramaProducao.ObterMesesProgramaPlaca(dMes);

			ViewBag.mesRef = dMes.ToShortDateString();
			ViewBag.fase = fase;
			ViewBag.codlinha = codlinha;
			ViewBag.familia = familia;
			ViewBag.codModelo = codModelo;

			return View(meses);
		}

		public ActionResult ListaXLS(string mesRef, string fase, string codlinha, string familia)
		{

			Response.AddHeader("content-disposition", "attachment; filename=ProgramaDia.xls");
			Response.ContentType = "application/ms-excel";

			var dMes = DateTime.Parse(DateTime.Now.AddDays(-DateTime.Now.Day).AddDays(1).ToShortDateString());

			if (!String.IsNullOrEmpty(mesRef))
			{
				dMes = DateTime.Parse(mesRef);
			}

			var programa = ProgramaProducao.ObterProgramacao(dMes, fase, codlinha, familia);
            var modelos = programa.GroupBy(x => new { x.CodFab, x.CodModelo, x.DesModelo, x.CodLinha, x.DesLinha, x.NumTurno, x.Familia, x.QtdCapac, x.PlanoMes, x.FlagTP }).Select(x => new ProgModelos()
			{
				CodFab = x.Key.CodFab,
				CodLinha = x.Key.CodLinha,
				CodModelo = x.Key.CodModelo,
				DesModelo = x.Key.DesModelo,
                FlagTP = x.Key.FlagTP,
				DesLinha = x.Key.DesLinha,
				NumTurno = x.Key.NumTurno,
				QtdPrograma = x.Sum(y => y.QtdProducao ?? y.QtdProdPm),
				Familia = x.Key.Familia,
				QtdCapacidade = x.Key.QtdCapac,
				QtdPlanoMes = x.Key.PlanoMes
			});

			ViewBag.Programa = programa;
			ViewBag.Data = dMes;
			return View(modelos);
		}

		public ActionResult ListaSAP(string mesRef)
		{

			Response.AddHeader("content-disposition", "attachment; filename=ExcelSAP.xls");
			Response.ContentType = "application/ms-excel";

			var dMes = DateTime.Parse(DateTime.Now.AddDays(-DateTime.Now.Day).AddDays(1).ToShortDateString());

			if (!String.IsNullOrEmpty(mesRef))
			{
				dMes = DateTime.Parse(mesRef);
			}

			var dataHoje = DateTime.Parse(DateTime.Now.ToShortDateString());

			var programa = ProgramaProducao.ObterProgramacaoSAP(dMes);
			programa.ForEach(x => { if (x.DatProducao < dataHoje) { x.QtdProdPm = 0; } });

			var progPCI = ProgramaProducao.ObterProgramacao(dMes, "M", "", "");
			if (progPCI != null && progPCI.Any()){
				progPCI = progPCI.Where(x => x.CodLinha.StartsWith("DAT")).ToList();

				progPCI.ForEach(x => { if (x.DatProducao < dataHoje) { x.QtdProdPm = 0; } });

				programa.AddRange(progPCI);

			}

			return View(programa);
		}
	}
}
