using System;

namespace SEMP.Model.DTO
{
    public class ProgDiaPlacaDTO
    {
        public string CodFab { get; set; }
        public string CodPlaca { get; set; }
        public System.DateTime DatProducao { get; set; }
        public string CodLinha { get; set; }
        public string CodProcesso { get; set; }
        public decimal QtdProdPM { get; set; }
        public System.Guid rowguid { get; set; }
        public string CodAparelho { get; set; }
        public string flgCobertura { get; set; }
        public Nullable<System.DateTime> DatEstudo { get; set; }
        public Nullable<System.DateTime> DatReferencia { get; set; }
        public string CodConjCin { get; set; }
        public string CodModelo { get; set; }
        public Nullable<decimal> QtdProducao { get; set; }
        public string Observacao { get; set; }
        public string FlagTP { get; set; }
        public int NumTurno { get; set; }
    }
}
