﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.Relatorios;
using SEMP.Model.VO.Rastreabilidade.Relatorios;

namespace SEMP.Areas.Coleta.Controllers
{
    public class DetalhesProdutoController : Controller
    {
        //
        // GET: /Coleta/DetalhesProduto/

        public ActionResult Index(String Codigo, String NumSerie)
        {
	        try
	        {
				if (!String.IsNullOrEmpty(Codigo) && !String.IsNullOrEmpty(NumSerie))
				{
					ViewBag.produto = DetalhesProdutoBO.ObterLeituraModelo(Codigo, NumSerie);
					ViewBag.itens = DetalhesProdutoBO.ObterItensLeitura(Codigo, NumSerie);
				}
	        }
	        catch (Exception)
	        {
		        ViewBag.produto = new LeiturasMF();
		        ViewBag.itens = new List<LeiturasItem>();
		        ViewBag.Erro = "Erro ao consultar";
	        }

	        return View();
        }
    }
}
