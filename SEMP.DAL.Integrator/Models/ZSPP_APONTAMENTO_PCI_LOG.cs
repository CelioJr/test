namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ZSPP_APONTAMENTO_PCI_LOG
    {
        [Key]
        [StringLength(16)]
        public string PACKNO { get; set; }

        [StringLength(10)]
        public string INTTROW { get; set; }

        public virtual ZSPP_OP_PCI_LOG ZSPP_OP_PCI_LOG { get; set; }
    }
}
