using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_EntMovRec
    {
        public int NumRomaneio { get; set; }
        public string CodItem { get; set; }
        public Nullable<decimal> QtdRec { get; set; }
        public System.DateTime DatReceb { get; set; }
        public string NomUsuario { get; set; }
        public string NomMaquina { get; set; }
        public Nullable<int> NumControle { get; set; }
        public string CodModelo { get; set; }
        public string CodDefeito { get; set; }
        public string CodStatus { get; set; }
        public string DocInt { get; set; }
        public Nullable<long> QtdLaudo { get; set; }
        public string Ciente { get; set; }
        public string JustCiente { get; set; }
        public Nullable<decimal> QtdPaga { get; set; }
    }
}
