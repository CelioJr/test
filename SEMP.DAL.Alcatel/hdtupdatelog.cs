namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.hdtupdatelog")]
    public partial class hdtupdatelog
    {
        public long id { get; set; }

        public int endNumber { get; set; }

        [StringLength(255)]
        public string hdtId { get; set; }

        [StringLength(255)]
        public string model { get; set; }

        public int startNumber { get; set; }

        public int tacNumber { get; set; }

        [StringLength(255)]
        public string type { get; set; }

        [StringLength(255)]
        public string userName { get; set; }
    }
}
