namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolvaritmo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_consolvaritmo()
        {
            dw_consolvaritmooco = new HashSet<dw_consolvaritmooco>();
        }

        [Key]
        public long id_consolvaritmo { get; set; }

        public long id_consol { get; set; }

        public long? id_tritmo { get; set; }

        public decimal? seg_auto_temporitmo { get; set; }

        public decimal? seg_manu_temporitmo { get; set; }

        public virtual dw_consol dw_consol { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolvaritmooco> dw_consolvaritmooco { get; set; }

        public virtual dw_t_ritmo dw_t_ritmo { get; set; }
    }
}
