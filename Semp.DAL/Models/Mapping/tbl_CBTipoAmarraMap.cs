using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
	public class tbl_CBTipoAmarraMap : EntityTypeConfiguration<tbl_CBTipoAmarra>
	{
		public tbl_CBTipoAmarraMap()
		{
			// Primary Key
			this.HasKey(t => new { t.CodTipoAmarra, t.CodFam });

			// Properties
			this.Property(t => t.CodTipoAmarra)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.DesTipoAmarra)
				.HasMaxLength(15)
                .IsUnicode(false);

			this.Property(t => t.CodFam)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(6)
                .IsUnicode(false);

			this.Property(t => t.FlgMascaraPadrao)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			this.Property(t => t.MascaraPadrao)
				.HasMaxLength(50)
                .IsUnicode(false);

			this.Property(t => t.CodProcesso)				
				.IsFixedLength()
				.HasMaxLength(6)
                .IsUnicode(false);


			// Table & Column Mappings
			this.ToTable("tbl_CBTipoAmarra");
			this.Property(t => t.CodTipoAmarra).HasColumnName("CodTipoAmarra");
			this.Property(t => t.DesTipoAmarra).HasColumnName("DesTipoAmarra");
			this.Property(t => t.CodFam).HasColumnName("CodFam");
			this.Property(t => t.FlgMascaraPadrao).HasColumnName("FlgMascaraPadrao");
			this.Property(t => t.MascaraPadrao).HasColumnName("MascaraPadrao");
			this.Property(t => t.FlgPlacaPainel).HasColumnName("FlgPlacaPainel");			this.Property(t => t.CodProcesso).HasColumnName("CodProcesso");
        }
    }}
