namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_CQTF_LIST_HIST
    {
        [Key]
        public int COD_HIST { get; set; }

        [StringLength(50)]
        public string ARQUIVO { get; set; }

        [StringLength(15)]
        public string IMEI { get; set; }

        public DateTime? DATA { get; set; }

        [StringLength(100)]
        public string CABECALHO { get; set; }

        [StringLength(20)]
        public string INPUT { get; set; }
    }
}
