using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.DTB_SSA.Models.Mapping
{
    public class tbl_ItemEstoqueMap : EntityTypeConfiguration<tbl_ItemEstoque>
    {
        public tbl_ItemEstoqueMap()
        {
            // Primary Key
            this.HasKey(t => t.NE);

            // Properties
            this.Property(t => t.NE)
                .IsRequired()
                .HasMaxLength(13);

            this.Property(t => t.descricao)
                .IsRequired()
                .HasMaxLength(90);

            this.Property(t => t.unidade)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.familia_id)
                .IsRequired()
                .HasMaxLength(6);

            this.Property(t => t.flgKitEmbalagem)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.hcl_tg)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.hcl_f)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.hcl_p)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.flgKitAcessorio)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.flgCOA)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.flgColetaExped)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.flgVirtual)
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("tbl_ItemEstoque");
            this.Property(t => t.NE).HasColumnName("NE");
            this.Property(t => t.descricao).HasColumnName("descricao");
            this.Property(t => t.unidade).HasColumnName("unidade");
            this.Property(t => t.familia_id).HasColumnName("familia_id");
            this.Property(t => t.custo_medio).HasColumnName("custo_medio");
            this.Property(t => t.flgKitEmbalagem).HasColumnName("flgKitEmbalagem");
            this.Property(t => t.dispositivo_id).HasColumnName("dispositivo_id");
            this.Property(t => t.hcl_tg).HasColumnName("hcl_tg");
            this.Property(t => t.hcl_f).HasColumnName("hcl_f");
            this.Property(t => t.hcl_p).HasColumnName("hcl_p");
            this.Property(t => t.codItem).HasColumnName("codItem");
            this.Property(t => t.flgKitAcessorio).HasColumnName("flgKitAcessorio");
            this.Property(t => t.flgCOA).HasColumnName("flgCOA");
            this.Property(t => t.periodo_frequencia).HasColumnName("periodo_frequencia");
            this.Property(t => t.flgColetaExped).HasColumnName("flgColetaExped");
            this.Property(t => t.QtdePalete).HasColumnName("QtdePalete");
            this.Property(t => t.flgVirtual).HasColumnName("flgVirtual");
            this.Property(t => t.RespDispositivo).HasColumnName("RespDispositivo");

        }
    }
}
