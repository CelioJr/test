using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CapacMdoLinhaMap : EntityTypeConfiguration<tbl_CapacMdoLinha>
    {
        public tbl_CapacMdoLinhaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodLinha, t.NumTurno });

            // Properties
            this.Property(t => t.CodLinha)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.NumTurno)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("tbl_CapacMdoLinha");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.Quantidade).HasColumnName("Quantidade");
            this.Property(t => t.DatInicio).HasColumnName("DatInicio");
            this.Property(t => t.DatFim).HasColumnName("DatFim");
            this.Property(t => t.NumTurno).HasColumnName("NumTurno");
        }
    }
}
