﻿using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public class tbl_CBEtiqueta
    {
        
        public int CodLinha { get; set; }
        public int NumPosto { get; set; }
        public string NomeEtiqueta { get; set; }
        public string DescricaoEtiqueta { get; set; }
        public string CaminhoEtiqueta { get; set; }
        public string NumIP_Impressora { get; set; }
        public string Porta_Impressora { get; set; }
        public Nullable<bool> flgAtivo { get; set; }
        public string NomeImpressora { get; set; }

         
    }
}
