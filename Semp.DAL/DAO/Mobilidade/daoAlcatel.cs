﻿using SEMP.Model.VO.Mobilidade;
using SEMP.Model.VO.PM;
using System;
using System.Collections.Generic;
using System.Linq;
using SEMP.DAL.Alcatel;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using System.Data.SqlClient;
using SEMP.Model.DTO;
using System.Data;

namespace SEMP.DAL.DAO.Mobilidade
{
	public class daoAlcatel
	{
		public static List<ResumoProducaoAlcatel> ObterResumoAlcatel(DateTime datInicio, DateTime datFim)
		{

			try
			{
				using (var contexto = new ALCATELContext())
				{
					var resultado = contexto.Database.SqlQuery<ResumoProducaoAlcatel>($"EXEC spc_ResumoProducao '{datInicio.ToString("MM/dd/yyyy")}', '{datFim.AddDays(1).AddMinutes(-1).ToString("MM/dd/yyyy")}'").ToList<ResumoProducaoAlcatel>();

					return resultado;
				}
			}
			catch (Exception)
			{

				return null;
			}



		}

		public static List<ResumoProducaoAlcatel> ObterResumoAlcatelEngine(DateTime datInicio, DateTime datFim)
		{

			try
			{
				using (var contexto = new ALCATELContext())
				{
					var resultado = contexto.Database.SqlQuery<ResumoProducaoAlcatel>($"EXEC spc_ResumoProducaoEngine '{datInicio.ToString("MM/dd/yyyy")}', '{datFim.AddDays(1).AddMinutes(-1).ToString("MM/dd/yyyy")}'").ToList<ResumoProducaoAlcatel>();

					return resultado;
				}
			}
			catch (Exception)
			{

				return null;
			}



		}

		public static List<ResumoProducaoAlcatel> ObterResumoAlcatelEngineFT(DateTime datInicio, DateTime datFim)
		{

			try
			{
				using (var contexto = new ALCATELContext())
				{

					var resumo = (from A in contexto.ALC_ENGINE
								  where A.DT_PASS >= datInicio && A.DT_PASS <= datFim
								  select new
								  {
									  sku_COD_SKU = 0,
									  fullSku = "FT ENGINE TOTAL",
									  A.DT_PASS
								  })
								  .ToList()
								  .GroupBy(B => new { B.sku_COD_SKU, B.fullSku, DATA = B.DT_PASS.Value.Date })
								  .Select(x => new ResumoProducaoAlcatel()
								  {
									  COD_SKU = x.Key.sku_COD_SKU,
									  SKU = x.Key.fullSku,
									  DATA = x.Key.DATA,
									  QTD_PROD = x.Count()
								  })
								  .ToList();

					return resumo;
				}
			}
			catch (Exception)
			{

				return null;
			}



		}

		public static List<ProducaoHoraAHora> ObterProducaoHoraAHora(DateTime Data, string CodLinha = null)
		{
			try
			{
				using (var contexto = new ALCATELContext())
				{
					if (CodLinha != null)
					{
						var producaohora = contexto.Database.SqlQuery<ProducaoHoraAHora>(
							"EXEC spc_BarrasProducaoHora @Data, @CodLinha",
							new SqlParameter("Data", Data),
							new SqlParameter("CodLinha", CodLinha)
						).ToList();

						return producaohora;
					}
					else
					{
						var producaohora = contexto.Database.SqlQuery<ProducaoHoraAHora>(
							"EXEC spc_BarrasProducaoHora @Data",
							new SqlParameter("Data", Data)
						).ToList();

						return producaohora;
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
				return null;
			}

		}

		/// <summary>
		/// Rotina para buscar as leituras de um período, retornando uma coleção mais simples de dados para depois fazer cálculos
		/// </summary>
		/// <param name="Datinicio"></param>
		/// <param name="DatFim"></param>
		/// <returns></returns>
		public static List<ProducaoLinhaMinuto> ObterLeiturasSimples(DateTime datReferencia)
		{
			using (var contexto = new ALCATELContext())
			{
				var datFim = datReferencia.AddDays(1).AddMinutes(-1);

				var retorno = (from p in contexto.ALC_LOT_LIST
							   //join l in contexto.tbl_Linha on p.CodLinha equals l.CodLinha
							   join ap in contexto.ALC_ORDER on p.ORDER_ID equals ap.idOrder
							   from prog in contexto.ALC_PROD_PLAN.Where(x => x.sku == ap.sku_COD_SKU && x.ini_date == datReferencia && x.line == p.LINE).DefaultIfEmpty()
							   where p.dtPallet >= datReferencia && p.dtPallet <= datFim
							   group new { p, prog } by new { Hora = p.dtPallet.Value.Hour, Minuto = p.dtPallet.Value.Minute, p.LINE, ap.fullSku, prog.planned} into grupo
							   select new
							   {
								   CodLinha = grupo.Key.LINE,
								   DatReferencia = datReferencia,
								   CodModelo = grupo.Key.fullSku,
								   Hora = grupo.Key.Hora,
								   Minuto = grupo.Key.Minuto,
								   QtdLeitura = grupo.Sum(x => x.p.PRODUCTS_QTY) ?? 0,
								   QtdProdPm = grupo.Key.planned
							   })
							   .ToList()
							   .GroupBy(x => new { x.CodLinha, x.DatReferencia, x.Hora, x.Minuto, x.CodModelo })
							   .Select(x => new ProducaoLinhaMinuto()
							   {
								   CodLinha = x.Key.CodLinha.ToString(),
								   DesLinha = "",
								   CodModelo = x.Key.CodModelo.ToString(),
								   DatReferencia = x.Key.DatReferencia,
								   Hora = x.Key.Hora,
								   Minuto = x.Key.Minuto,
								   QtdLeitura = x.Sum(y => y.QtdLeitura),
								   QtdMeta = (decimal)x.Average(y => y.QtdProdPm ?? 0)
							   }).OrderBy(x => x.Hora).ToList();

				var linhas = daoGBR.ObterLinhas();

				retorno.ForEach(x => {
					var desLinha = linhas.FirstOrDefault(y => y.COD_LINHAS.ToString() == x.CodLinha).NOME_LINHA;
					x.DesLinha = desLinha;
				});

				return retorno;

			}
		}

		public static List<BarrasPainelDTO> ObterPainelProducaoALCATEL(string codLinha)
		{
			using (var contexto = new ALCATELContext())
			{
				try
				{
					var resumo = contexto.Database.SqlQuery<BarrasPainelDTO>(
						"EXEC spc_BarrasPainel @CodLinha",
						new SqlParameter("CodLinha", codLinha)
						).ToList();

					return resumo;
				}
				catch (Exception)
				{
					return null;
				}


			}
		}

		public static List<ResumoProducao> ObterResumoMF(int codLinha)
		{
			using (ALCATELContext contexto = new ALCATELContext())
			{
				List<ResumoProducao> resumos = new List<ResumoProducao>();
				SqlConnection con = new SqlConnection(contexto.Database.Connection.ConnectionString);
				try
				{
					SqlParameter[] parameters =
					 {
						new SqlParameter("CodLinha", codLinha)
					 };

					con.Open();
					string cmd = "EXEC spc_BarrasPainel @CodLinha";
					SqlCommand oCom = new SqlCommand(cmd, con);
					oCom.Parameters.Clear();

					foreach (SqlParameter param in parameters)
						oCom.Parameters.Add(param);

					DataSet oDs = new DataSet();

					using (SqlDataAdapter oDa = new SqlDataAdapter())
					{
						oDa.SelectCommand = oCom;
						oDa.Fill(oDs);
					}
					if (oDs != null)
					{
						foreach (DataTable table in oDs.Tables)
						{
							if (table.Columns.Count == 6)
							{
								foreach (DataRow row in table.Rows)
								{
									ResumoProducao resumo = new ResumoProducao();
									resumo.CodModelo = row["CodModelo"].ToString();
									resumo.DesModelo = row["DesModelo"].ToString();
									resumo.QtdPlano = (string.IsNullOrEmpty(row["Plano"].ToString()) ? 0 : int.Parse(row["Plano"].ToString()));
									resumo.QtdRealizado = (string.IsNullOrEmpty(row["Embalado"].ToString()) ? 0 : int.Parse(row["Embalado"].ToString()));
									resumos.Add(resumo);
								}
							}
						}
					}

					return resumos;
				}
				catch (Exception ex)
				{
					throw new Exception(ex.Message, ex.InnerException);
				}
				finally
				{
					con.Close();
				}
			}
		}


		#region Produção Diária x Programa
		public static List<ProgramaDiario> ObterProdutos(DateTime data)
		{
			using (var contexto = new ALCATELContext())
			{

				var inicio = data.AddDays(-data.Day).AddDays(1);
				var fim = inicio.AddMonths(1).AddDays(-1);

				/*var dados = (from p in contexto.ALC_ORDER
							 from plan in contexto.ALC_PROD_PLAN.Where(x => x.ini_date >= inicio && x.ini_date <= fim && x.sku == p.sku_COD_SKU).DefaultIfEmpty()
								 //from sku in gbr.GBR_CAD_SKU.Where(g => g.COD_SKU == p.sku_COD_SKU).DefaultIfEmpty()
								 //from mod in gbr.GBR_CAD_MODELO.Where(m => m.COD_MODELO == sku.COD_MODELO).DefaultIfEmpty()
							 where p.dtStart >= inicio && p.dtStart <= fim && p.IS_ENGINE == false
							 group new { p, plan } by new { CodModelo = p.sku_COD_SKU, DesItem = p.fullSku } //, mod.NOME_MODELO }
							into grupo
							 select new ProgramaDiario()
							 {
								 CodModelo = grupo.Key.CodModelo.ToString(),
								 DesModelo = grupo.Key.DesItem, // + " - " + grupo.Key.NOME_MODELO??"",
								 CodFam = "MOB",
								 NomFam = "MOBILIDADE",
								 QtdPP = grupo.Sum(x => x.plan.planned) ?? 0,
								 QtdVPE = 0,
								 Ordem = 98
							 }).ToList();*/
				//var query = $"SELECT CodModelo = CONVERT(varchar, p.sku_COD_SKU), DesModelo = p.fullSku + ' - ' + mod.NOME_MODELO COLLATE SQL_Latin1_General_CP1_CI_AS, CodFam = 'MOB', NomFam = 'MOBILIDADE', " +
				var query = $"SELECT CodModeloMob = plann.sku, CodModelo = plann.full_sku, DesModelo = mod.NOME_MODELO COLLATE SQL_Latin1_General_CP1_CI_AS, CodFam = 'MOB', NomFam = 'MOBILIDADE', " +
							$" QtdPP = CONVERT(DECIMAL, ISNULL(SUM(plann.planned), 0)), QtdVPE = 0.0, Ordem = 98 " +
							$" FROM db_accessadmin.ALC_PROD_PLAN plann" +
							$" LEFT JOIN GBR.dbo.GBR_CAD_SKU sku ON sku.COD_SKU = plann.sku " +
							$" LEFT JOIN GBR.dbo.GBR_CAD_MODELO mod ON mod.COD_MODELO = sku.COD_MODELO " +
							$" WHERE plann.ini_date >= '{inicio.ToString("MM/dd/yyyy")}' AND plann.ini_date <= '{fim.ToString("MM/dd/yyyy")}' " +
							$" GROUP BY plann.sku, plann.full_sku, mod.NOME_MODELO";

				IEnumerable<ProgramaDiario> dados = contexto.Database.SqlQuery<ProgramaDiario>(query);

				return dados.ToList();
			}
		}

		public static List<ProgramaDiario> ObterProgramaDia(DateTime data)
		{
			using (var contexto = new ALCATELContext())
			using (var gbr = new GBR.GBRContext())
			{

				var inicio = data;
				var fim = inicio.AddDays(1).AddMinutes(-1);

				var dados = (from p in contexto.ALC_PROD_PLAN
							 where p.ini_date >= inicio && p.ini_date <= fim
							 group p by new { CodModelo = p.sku, DesItem = p.full_sku }
								 into grupo
							 select new ProgramaDiario()
							 {
								 CodModeloMob = grupo.Key.CodModelo,
								 CodModelo = grupo.Key.DesItem,
								 QtdPP = grupo.Sum(x => x.planned)
							 }).ToList();

				return dados;
			}
		}

		public static List<ProgramaDiario> ObterProgramaDia(DateTime datInicio, DateTime datFim)
		{
			using (var contexto = new ALCATELContext())
			{

				var inicio = datInicio;
				var fim = datFim.AddDays(1).AddMinutes(-1);

				var dados = (from p in contexto.ALC_PROD_PLAN
							 where p.ini_date >= inicio && p.ini_date <= fim
							 group p by new { CodModelo = p.sku, DesItem = p.full_sku }
								 into grupo
							 select new ProgramaDiario()
							 {
								 CodModeloMob = grupo.Key.CodModelo,
								 CodModelo = grupo.Key.DesItem,
								 QtdPP = grupo.Sum(x => x.planned)
							 }).ToList();

				return dados;
			}
		}

		public static List<ProgramaDiario> ObterProducaoDia(DateTime data)
		{
			using (var contexto = new ALCATELContext())
			{

				var datFim = data.AddDays(1).AddMinutes(-1);

				var resumo = (from A in contexto.ALC_LOT_LIST
							  join B in contexto.ALC_ORDER on A.ORDER_ID equals B.idOrder
							  from plan in contexto.ALC_PROD_PLAN.Where(C => B.fullSku == C.full_sku && C.line == B.line_COD_LINHAS && C.ini_date >= data && C.ini_date <= datFim).DefaultIfEmpty()
							  where A.dtPallet >= data && A.dtPallet <= datFim && B.IS_ENGINE == false
							  select new
							  {
								  B.sku_COD_SKU,
								  B.fullSku,
								  A.PRODUCTS_QTY
							  })
							  .ToList()
							  .GroupBy(B => new { B.sku_COD_SKU, B.PRODUCTS_QTY, B.fullSku })
							  .Select(x => new ProgramaDiario()
							  {
								  CodModeloMob = x.Key.sku_COD_SKU,
								  CodModelo = x.Key.fullSku,
								  QtdProdDia = x.Count() * x.Key.PRODUCTS_QTY
							  })
							  .ToList();

				return resumo;
			}
		}

		public static List<ProgramaDiario> ObterProducaoAcumulada(DateTime data)
		{
			using (var contexto = new ALCATELContext())
			{
				var inicio = data.AddDays(-data.Day).AddDays(1);
				var fim = inicio.AddMonths(1).AddDays(-1);

				var resumo = (from A in contexto.ALC_LOT_LIST
							  join B in contexto.ALC_ORDER on A.ORDER_ID equals B.idOrder
							  where A.dtPallet >= inicio && A.dtPallet <= fim
							  select new
							  {
								  B.sku_COD_SKU,
								  B.fullSku,
								  A.PRODUCTS_QTY
							  })
							  .ToList()
							  .GroupBy(B => new { B.sku_COD_SKU, B.PRODUCTS_QTY, B.fullSku })
							  .Select(x => new ProgramaDiario()
							  {
								  CodModeloMob = x.Key.sku_COD_SKU,
								  CodModelo = x.Key.fullSku,
								  QtdAcumulada = x.Sum(y => y.PRODUCTS_QTY),
								  QtdPP = 0
							  })
							  .ToList();

				return resumo;

			}
		}

		public static List<ProgramaDiario> ObterDefeitosDia(DateTime data, string fase = "PA")
		{
			using (var contexto = new ALCATELContext())
			{
				var datFim = data.AddDays(1).AddMinutes(-1);

				var defeitos = (from d in contexto.ALC_REPAIR
								where d.DT_ENTRY >= data && d.DT_ENTRY <= datFim && d.STEP == fase
								group d by d.COD_SKU into grupo
								select new ProgramaDiario()
								{
									CodModeloMob = grupo.Key,
									QtdDefeitos = grupo.Count()
								});
				var resultado = defeitos.ToList();
				return resultado;
			}

		}
		#endregion

		public static List<ALC_VOLUME_DTO> ObterLotes(DateTime datInicio, DateTime datFim) {

			using (var contexto = new ALCATELContext())
			{
				try
				{
					var dados = contexto.ALC_VOLUME.Where(x => x.DATE_CREATE >= datInicio && x.DATE_CREATE <= datFim)
									.Select(z => new ALC_VOLUME_DTO() { 
										DATE_CREATE	= z.DATE_CREATE,
										DATE_FINISH = z.DATE_FINISH,
										ID_VOL = z.ID_VOL,
										orderVol = z.orderVol,
										ORDER_VOL = z.ORDER_VOL,
										PRODUCTS_QTY = z.PRODUCTS_QTY,
										QTY_PROD = z.QTY_PROD,
										QUANTITY = z.QUANTITY,
										STATUS = z.STATUS
									})
									.ToList();
					return dados;
				}
				catch (Exception)
				{
					return null;
				}
			}

		}

		public static List<ALC_LOT_LIST_DTO> ObterLotesItens(string numLote)
		{

			using (var contexto = new ALCATELContext())
			{
				try
				{
					var dados = contexto.ALC_LOT_LIST.Where(x => x.ID_LOTE == numLote)
									.Select(z => new ALC_LOT_LIST_DTO()
									{
										dtPallet = z.dtPallet,
										ID_LIST = z.ID_LIST,
										ID_LOTE = z.ID_LOTE,
										LINE = z.LINE,
										ORDER_ID = z.ORDER_ID,
										PACKING = z.PACKING,
										FAMILY = z.FAMILY,
										PACK_STRING = z.PACK_STRING,
										PRODUCTS_QTY = z.PRODUCTS_QTY,
										RESP_PALLET = z.RESP_PALLET
									})
									.ToList();
					return dados;
				}
				catch (Exception)
				{
					return null;
				}
			}

		}


		public static ALC_LOT_LIST_DTO ObterLoteItem(string numGRP)
		{

			using (var contexto = new ALCATELContext())
			{
				try
				{
					var dados = contexto.ALC_LOT_LIST.Select(z => new ALC_LOT_LIST_DTO()
						{
							dtPallet = z.dtPallet,
							ID_LIST = z.ID_LIST,
							ID_LOTE = z.ID_LOTE,
							LINE = z.LINE,
							ORDER_ID = z.ORDER_ID,
							PACKING = z.PACKING,
							FAMILY = z.FAMILY,
							PACK_STRING = z.PACK_STRING,
							PRODUCTS_QTY = z.PRODUCTS_QTY,
							RESP_PALLET = z.RESP_PALLET
						}).FirstOrDefault(x => x.PACK_STRING == numGRP);
					return dados;
				}
				catch (Exception)
				{
					return null;
				}
			}

		}
	}
}
