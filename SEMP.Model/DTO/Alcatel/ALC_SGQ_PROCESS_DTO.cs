using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_SGQ_PROCESS_DTO
    {
        public ALC_SGQ_PROCESS_DTO()
        {
            this.ALC_SGQ_DOCUMENT = new List<ALC_SGQ_DOCUMENT_DTO>();
        }

        public int ID_PROCESS { get; set; }
        public string DESCRIPTION { get; set; }
        public virtual ICollection<ALC_SGQ_DOCUMENT_DTO> ALC_SGQ_DOCUMENT { get; set; }
    }
}
