using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class viw_CBPassagemTurno
    {
        public string NumECB { get; set; }
        public int CodLinha { get; set; }
        public int NumPosto { get; set; }
        public System.DateTime DatEvento { get; set; }
        public string CodDRT { get; set; }
        public Nullable<System.DateTime> DatReferencia { get; set; }
        public Nullable<int> Turno { get; set; }
    }
}
