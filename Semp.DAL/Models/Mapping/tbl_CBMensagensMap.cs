using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBMensagensMap : EntityTypeConfiguration<tbl_CBMensagens>
    {
        public tbl_CBMensagensMap()
        {
            // Primary Key
            this.HasKey(t => t.CodMensagem);

            // Properties
            this.Property(t => t.CodMensagem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5);

            this.Property(t => t.DesMensagem)
                .HasMaxLength(50);

            this.Property(t => t.DesCor)
                .HasMaxLength(15);

            this.Property(t => t.DesSom)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("tbl_CBMensagens");
            this.Property(t => t.CodMensagem).HasColumnName("CodMensagem");
            this.Property(t => t.DesMensagem).HasColumnName("DesMensagem");
            this.Property(t => t.DesCor).HasColumnName("DesCor");
            this.Property(t => t.DesSom).HasColumnName("DesSom");
        }
    }
}
