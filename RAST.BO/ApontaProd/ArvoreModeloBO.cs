﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.VO.Rastreabilidade.Relatorios;

namespace RAST.BO.Relatorios
{
	public class ArvoreModeloBO
	{

		public static List<ArvoreModelo> ObterEstrutura(string codModelo)
		{

			DateTime data = DateTime.Parse(DateTime.Now.ToShortDateString()).AddDays(-DateTime.Now.Day + 1);

			var arvore = new List<ArvoreModelo>();

			// Adicionar o modelo que foi informado
			ArvoreModelo modelo = new ArvoreModelo();
			modelo.CodModelo = codModelo;
			modelo.DesModelo = daoGeral.ObterDescricaoItem(codModelo);
			modelo.CodTipo = "P";
			modelo.QtdLoteAP = daoArvoreModelos.ObterProgramaModelo(codModelo, data);
			var producao = daoArvoreModelos.ObterProducaoModelo(codModelo, data);
			if (producao != null) {
				modelo.QtdEmbalado = producao.QtdProduzida;
				modelo.QtdProduzida = producao.QtdApontada;
			}
			else 
			{
				modelo.QtdEmbalado = 0;
				modelo.QtdProduzida = 0;
			}
			modelo.Nivel = 0;
			modelo.CodStatus = "";

			arvore.Add(modelo);
			
			// Adicionar as APs do modelo
			var apsmodelo = daoSaldoPlaca.ObterAPsMes(codModelo, data);
			apsmodelo.ForEach(x => arvore.Add(new ArvoreModelo()
			{
				CodFab = x.CodFab,
				NumAP = x.NumAP,
				CodModelo = x.CodModelo,
				QtdLoteAP = x.QtdLoteAP,
				QtdProduzida = x.QtdProduzida,
				QtdEmbalado = x.QtdEmbalado,
				CodStatus = x.CodStatus,
				CodTipo = "A",
				Observacao = x.Observacao,
				Nivel = 1,
				TipAP = x.TipAP
			}));

			arvore.AddRange(ObterEstruturaItem(codModelo, 0, data) ?? new List<ArvoreModelo>());

			return arvore;

		}

		public static List<ArvoreModelo> ObterEstruturaItem (String codModelo, int Nivel, DateTime data) {

			var arvore = new List<ArvoreModelo>();
			// Buscar os itens do modelo que são modelo
			var itens = daoArvoreModelos.ObterItensModelo(codModelo);

			if (itens.Any()) {
				Nivel++;

				// Buscar dados e inserir na árvore principal
				foreach (var item in itens)
				{
					// cria o item
					var itemarvore = new ArvoreModelo();
					itemarvore.CodModelo = item.CodItem;
					itemarvore.DesModelo = daoGeral.ObterDescricaoItem(item.CodItem);
					itemarvore.CodTipo = item.CodProcesso;
					itemarvore.CodStatus = "";
					itemarvore.Nivel = Nivel;
					itemarvore.QtdLoteAP = daoArvoreModelos.ObterProgramaPlaca(item.CodItem, data);
					var producaoplaca = daoArvoreModelos.ObterProducaoPlaca(item.CodItem, data);
					if (producaoplaca != null)
					{
						itemarvore.QtdProduzida = producaoplaca.QtdApontada;
						itemarvore.QtdEmbalado = producaoplaca.QtdProduzida;
					}
					else
					{
						itemarvore.QtdProduzida = 0;
						itemarvore.QtdEmbalado = 0;
					}

					arvore.Add(itemarvore);

					// verifica as APs do item
					var aps = daoSaldoPlaca.ObterAPsMes(item.CodItem, data);
					aps.ForEach(x => arvore.Add(new ArvoreModelo()
					{
						CodFab = x.CodFab,
						NumAP = x.NumAP,
						CodModelo = x.CodModelo,
						QtdLoteAP = x.QtdLoteAP,
						QtdProduzida = x.QtdProduzida,
						QtdEmbalado = x.QtdEmbalado,
						CodStatus = x.CodStatus,
						CodTipo = "A",
						Observacao = x.Observacao,
						Nivel = Nivel + 1,
						TipAP = x.TipAP
					}));

					var itensarvore = ObterEstruturaItem(item.CodItem, Nivel, data);

					if (itensarvore != null && itensarvore.Any()){
						arvore.AddRange(itensarvore);
					}
				}

				return arvore;
			}
			else
			{
				return null;
			}
		}

	}
}
