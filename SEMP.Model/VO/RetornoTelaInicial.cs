﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO
{
	public class RetornoTelaInicial
	{

		public decimal produzido { get; set; }
		public decimal defeitos { get; set; }
		public decimal qtdPlano { get; set; }
		public decimal indice { get; set; }

	}
}
