using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using SEMP.DTB_CB.Models.Mapping;

namespace SEMP.DTB_CB.Models
{
	public partial class DTB_CBContext : DbContext
	{
		static DTB_CBContext()
		{
			Database.SetInitializer<DTB_CBContext>(null);
		}

		public DTB_CBContext()
			: base("Name=DTB_CB_Desk_Context")
		{
		}

		public DbSet<dtproperty> dtproperties { get; set; }
		public DbSet<sysdiagram> sysdiagrams { get; set; }
		public DbSet<tbl_CBAmarra> tbl_CBAmarra { get; set; }
		public DbSet<tbl_CBBurnIn> tbl_CBBurnIn { get; set; }
		public DbSet<tbl_CBDefeito> tbl_CBDefeito { get; set; }
		public DbSet<tbl_CBDefeitoRastreamento> tbl_CBDefeitoRastreamento { get; set; }
		public DbSet<tbl_CBEmbalada> tbl_CBEmbalada { get; set; }
		public DbSet<tbl_CBIdentifica> tbl_CBIdentifica { get; set; }
		public DbSet<tbl_CBLinha> tbl_CBLinha { get; set; }       
		public DbSet<tbl_CBLinhaPostoPerfil> tbl_CBLinhaPostoPerfil { get; set; }
		public DbSet<tbl_CBLote> tbl_CBLote { get; set; }
		public DbSet<tbl_CBPassagem> tbl_CBPassagem { get; set; }       
		public DbSet<tbl_CBPassagem_Hist> tbl_CBPassagem_Hist { get; set; }
		public DbSet<tbl_CBPosto> tbl_CBPosto { get; set; }
		public DbSet<tbl_CBPostoDefeito> tbl_CBPostoDefeito { get; set; }
		public DbSet<tbl_CBProdDiaDAT> tbl_CBProdDiaDAT { get; set; }
		public DbSet<tbl_CBRastLocal> tbl_CBRastLocal { get; set; }
		public DbSet<tbl_CBTipoPosto> tbl_CBTipoPosto { get; set; }
		public DbSet<tbl_CBTrocaDRT> tbl_CBTrocaDRT { get; set; }
		
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Configurations.Add(new dtpropertyMap());
			modelBuilder.Configurations.Add(new sysdiagramMap());
			modelBuilder.Configurations.Add(new tbl_CBAmarraMap());
			modelBuilder.Configurations.Add(new tbl_CBBurnInMap());
			modelBuilder.Configurations.Add(new tbl_CBDefeitoMap());
			modelBuilder.Configurations.Add(new tbl_CBDefeitoRastreamentoMap());
			modelBuilder.Configurations.Add(new tbl_CBEmbaladaMap());
			modelBuilder.Configurations.Add(new tbl_CBIdentificaMap());
			modelBuilder.Configurations.Add(new tbl_CBLinhaMap());           
			modelBuilder.Configurations.Add(new tbl_CBLinhaPostoPerfilMap());
			modelBuilder.Configurations.Add(new tbl_CBLoteMap());         
			modelBuilder.Configurations.Add(new tbl_CBPassagemMap());
			modelBuilder.Configurations.Add(new tbl_CBPassagem_HistMap());
			modelBuilder.Configurations.Add(new tbl_CBPostoMap());
			modelBuilder.Configurations.Add(new tbl_CBPostoDefeitoMap());
			modelBuilder.Configurations.Add(new tbl_CBProdDiaDATMap());
			modelBuilder.Configurations.Add(new tbl_CBRastLocalMap());
			modelBuilder.Configurations.Add(new tbl_CBTipoPostoMap());
			modelBuilder.Configurations.Add(new tbl_CBTrocaDRTMap());
			
		}
	}
}
