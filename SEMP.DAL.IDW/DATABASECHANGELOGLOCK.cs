namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DATABASECHANGELOGLOCK")]
    public partial class DATABASECHANGELOGLOCK
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        public bool LOCKED { get; set; }

        public DateTime? LOCKGRANTED { get; set; }

        [StringLength(255)]
        public string LOCKEDBY { get; set; }
    }
}
