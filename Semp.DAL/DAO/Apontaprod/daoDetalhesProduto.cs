﻿using System;
using System.Collections.Generic;
using System.Linq;
using SEMP.DAL.Models;
using SEMP.Model.VO.Rastreabilidade.Relatorios;

namespace SEMP.DAL.DAO.Rastreabilidade
{
	public class daoDetalhesProduto
	{
		public static List<LeiturasItem> ObterLeiturasItems(String Codigo, String NumSerie)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				List<LeiturasItem> itens = (from i in dtbSim.tbl_BarrasProducaoItem
					join d in dtbSim.tbl_Item on i.CodItem equals d.CodItem
					where i.CodModelo == Codigo && i.NumSerieModelo == NumSerie
					select new LeiturasItem()
					{
						CodItem = i.CodItem,
						DesItem = d.DesItem,
						NumSerie = i.NumSerieItem,
						DataLeitura = i.DatLeitura
					}).ToList();

				return itens;
			}
		}

		public static List<LeiturasItem> ObterLeiturasAmarra(String Codigo, String NumSerie)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				List<LeiturasItem> itens = (from i in dtbCb.tbl_CBAmarra
					join item in dtbCb.tbl_Item on i.CodItem equals item.CodItem
					where i.CodModelo == Codigo && i.NumSerie.StartsWith(Codigo) && i.NumSerie.EndsWith(NumSerie)
					select new LeiturasItem()
					{
						CodItem = i.CodItem,
						NumSerie = i.NumECB,
						DataLeitura = i.DatAmarra,
						DesItem = item.DesItem
					}).ToList();

				return itens;
			}

		}
	}
}
