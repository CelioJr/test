using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBQueimaTestesMap : EntityTypeConfiguration<tbl_CBQueimaTestes>
    {
		public tbl_CBQueimaTestesMap()
		{
			// Primary Key
			this.HasKey(t => new { t.NumECB, t.CodLinha, t.NumPosto, t.CodFab, t.NumRPA, t.CodDefeito, t.CodSubDefeito, t.DatTeste });

			// Properties
			this.Property(t => t.NumECB)
				.IsRequired()
				.HasMaxLength(20)
                .IsUnicode(false);

			this.Property(t => t.CodLinha)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.NumPosto)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.CodFab)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(2)
                .IsUnicode(false);

			this.Property(t => t.NumRPA)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(6)
                .IsUnicode(false);

			this.Property(t => t.CodDefeito)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(5)
                .IsUnicode(false);

			this.Property(t => t.CodSubDefeito)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.FlgStatus)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			this.Property(t => t.Observacao)
				.HasMaxLength(200)
                .IsUnicode(false);

			// Table & Column Mappings
			this.ToTable("tbl_CBQueimaTestes");
			this.Property(t => t.NumECB).HasColumnName("NumECB");
			this.Property(t => t.CodLinha).HasColumnName("CodLinha");
			this.Property(t => t.NumPosto).HasColumnName("NumPosto");
			this.Property(t => t.CodFab).HasColumnName("CodFab");
			this.Property(t => t.NumRPA).HasColumnName("NumRPA");
			this.Property(t => t.CodDefeito).HasColumnName("CodDefeito");
			this.Property(t => t.CodSubDefeito).HasColumnName("CodSubDefeito");
			this.Property(t => t.FlgStatus).HasColumnName("FlgStatus");
			this.Property(t => t.Observacao).HasColumnName("Observacao");
			this.Property(t => t.DatTeste).HasColumnName("DatTeste");
		}
	}
}
