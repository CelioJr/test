using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_REGRAS_CATEGORIAS_DTO
    {
        public FR_REGRAS_CATEGORIAS_DTO()
        {
            this.FR_REGRAS = new List<FR_REGRAS_DTO>();
        }

        public int CAT_COD { get; set; }
        public string CAT_NOME { get; set; }
        public Nullable<int> CAT_SUPER { get; set; }
        public virtual ICollection<FR_REGRAS_DTO> FR_REGRAS { get; set; }
    }
}
