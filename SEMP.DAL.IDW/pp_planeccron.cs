namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_planeccron
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pp_planeccron()
        {
            pp_cpneccron = new HashSet<pp_cpneccron>();
        }

        [Key]
        public long id_planeccron { get; set; }

        public DateTime? dthr_necessaria { get; set; }

        public long? qt_necessaria { get; set; }

        public long id_produto { get; set; }

        public long id_plano { get; set; }

        public long id_neccron { get; set; }

        public long? qt_estoque { get; set; }

        public int? ordem { get; set; }

        public long? qt_acumprir { get; set; }

        public short? is_antecipacao { get; set; }

        public virtual om_produto om_produto { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cpneccron> pp_cpneccron { get; set; }

        public virtual pp_neccron pp_neccron { get; set; }

        public virtual pp_plano pp_plano { get; set; }
    }
}
