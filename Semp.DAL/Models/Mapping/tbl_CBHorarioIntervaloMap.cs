using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBHorarioIntervaloMap : EntityTypeConfiguration<tbl_CBHorarioIntervalo>
    {
        public tbl_CBHorarioIntervaloMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodLinha, t.Turno, t.TipoIntervalo, t.HoraInicio });

            // Properties
            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Turno)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TipoIntervalo)
                .IsRequired()
                .HasMaxLength(15)
                .IsUnicode(false);

            this.Property(t => t.HoraInicio)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.HoraFim)
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_CBHorarioIntervalo");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.Turno).HasColumnName("Turno");
            this.Property(t => t.TipoIntervalo).HasColumnName("TipoIntervalo");
            this.Property(t => t.HoraInicio).HasColumnName("HoraInicio");
            this.Property(t => t.HoraFim).HasColumnName("HoraFim");
            this.Property(t => t.TempoIntervalo).HasColumnName("TempoIntervalo");
        }
    }
}
