using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBMaquinaMap : EntityTypeConfiguration<tbl_CBMaquina>
    {
        public tbl_CBMaquinaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodLinha, t.CodMaquina });

            // Properties
            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodMaquina)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.DesMaquina)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("tbl_CBMaquina");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.CodMaquina).HasColumnName("CodMaquina");
            this.Property(t => t.DesMaquina).HasColumnName("DesMaquina");

            // Relationshipsms
            //this.HasRequired(t => t.tbl_CBLinha)
            //    .WithMany(t => t.tbl_CBMaquina)
            //    .HasForeignKey(d => d.CodLinha);

        }
    }
}
