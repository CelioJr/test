﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SEMP.BO;
using SEMP.Model.DTO;
using RAST.BO.Relatorios;

namespace SEMP.Areas.Outros.Controllers
{
    public class MensagemPainelController : Controller
    {
        // GET: Outros/MensagemPainel
        public ActionResult Index()
        {
            return View();
        }

        // GET: Outros/MensagemPainel/Details/5
        public ActionResult Lista()
        {
			var dados = PainelProducao.ObterMensagemPainel();

			return View(dados);
        }

        // GET: Outros/MensagemPainel/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Outros/MensagemPainel/Create
        [HttpPost]
        public ActionResult Create(MensagemPainelDTO dados)
        {
            try
            {
                PainelProducao.GravarMensagemPainel(dados);

				return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch
            {
				return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Outros/MensagemPainel/Edit/5
        public ActionResult Edit(long codMensagem)
        {

			var dados = PainelProducao.ObterMensagemPainel(codMensagem);

            return View(dados);
        }

        // POST: Outros/MensagemPainel/Edit/5
        [HttpPost]
		public ActionResult Edit(MensagemPainelDTO dados)
        {
			try
			{
				PainelProducao.GravarMensagemPainel(dados);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
        }

        // GET: Outros/MensagemPainel/Delete/5
		public ActionResult Delete(long codMensagem)
		{

			var dados = PainelProducao.ObterMensagemPainel(codMensagem);

			return View(dados);
		}

        // POST: Outros/MensagemPainel/Delete/5
        [HttpPost]
		public ActionResult Delete(MensagemPainelDTO dados)
        {
			try
			{
				PainelProducao.RemoverMensagemPainel(dados.CodMensagem);

				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}
        }
    }
}
