﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.DTO
{
	public class CadRetrabalhoDTO
	{
		public int CodRetrabalho { get; set; }
		public string Justificativa { get; set; }
		public System.DateTime DatEvento { get; set; }
		public string CodDRT { get; set; }
		public int Status { get; set; }
		public string CodFab { get; set; }
		public string NumAP { get; set; }
		public int Quantidade { get; set; }
		public List<CadRotaRetrabalhoDTO> Rota { get; set; }
		public List<CadItensRetrabalhoDTO> Itens { get; set; }
		public List<CadTrocaItemDTO> TrocaItem { get; set; }
		
	}

	public class CadRotaRetrabalhoDTO
	{
		public int CodLinha { get; set; }
		public int NumPosto { get; set; }
	}

	public class CadItensRetrabalhoDTO
	{
		public string NumECB { get; set; }		
	}

	public class CadTrocaItemDTO
	{
		public string CodItemAnterior { get; set; }
		public string CodItemNovo { get; set; }
	}

}
