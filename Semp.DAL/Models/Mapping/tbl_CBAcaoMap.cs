using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBAcaoMap : EntityTypeConfiguration<tbl_CBAcao>
    {
        public tbl_CBAcaoMap()
        {
            // Primary Key
            this.HasKey(t => t.CodAcao);

            // Properties
            this.Property(t => t.Descricao)
                .IsFixedLength()
                .HasMaxLength(80)
                .IsUnicode(false);

            this.Property(t => t.Tipo)
                .IsFixedLength()
                .HasMaxLength(20)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_CBAcao");
            this.Property(t => t.CodAcao).HasColumnName("CodAcao");
            this.Property(t => t.Descricao).HasColumnName("Descricao");
            this.Property(t => t.Tipo).HasColumnName("Tipo");
            this.Property(t => t.FlgAtivo).HasColumnName("FlgAtivo");
        }
    }
}
