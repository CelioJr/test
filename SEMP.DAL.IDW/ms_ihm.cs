namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ms_ihm
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ms_ihm()
        {
            ms_msihm = new HashSet<ms_msihm>();
            ms_upihm = new HashSet<ms_upihm>();
        }

        [Key]
        public long id_ihm { get; set; }

        public DateTime? dthr_heartbeat { get; set; }

        public DateTime? dthr_cadastro { get; set; }

        [StringLength(100)]
        public string url_conexao { get; set; }

        [StringLength(1)]
        public string is_evtpnd { get; set; }

        [StringLength(1)]
        public string is_evtciclofechado { get; set; }

        [StringLength(1)]
        public string is_evttrabalhando { get; set; }

        [StringLength(1)]
        public string is_evtparada { get; set; }

        [StringLength(1)]
        public string is_evtrefugo { get; set; }

        [StringLength(1)]
        public string is_evtalerta { get; set; }

        [StringLength(1)]
        public string is_evtlogin { get; set; }

        [StringLength(1)]
        public string is_upreg { get; set; }

        [StringLength(1)]
        public string is_evttodos { get; set; }

        public long id_usr { get; set; }

        [StringLength(100)]
        public string url_ip { get; set; }

        [StringLength(30)]
        public string cd_ihm { get; set; }

        [StringLength(100)]
        public string url_conexaoAlt { get; set; }

        public virtual ms_usr ms_usr { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_msihm> ms_msihm { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_upihm> ms_upihm { get; set; }
    }
}
