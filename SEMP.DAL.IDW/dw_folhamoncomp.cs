namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_folhamoncomp
    {
        [Key]
        public long id_folhamoncomp { get; set; }

        [StringLength(40)]
        public string ds_mon { get; set; }

        public long id_produtoFilho { get; set; }

        public int? ordem { get; set; }

        public short? is_aceitarAlter { get; set; }

        public long? id_folhamon { get; set; }

        public int? qt_amontar { get; set; }

        public short? tp_validacao { get; set; }

        public short? limite_min_cb { get; set; }

        public short? limite_max_cb { get; set; }

        public short? is_avaliarlimites { get; set; }

        public virtual dw_folhamon dw_folhamon { get; set; }

        public virtual om_produto om_produto { get; set; }
    }
}
