using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_REGRAS_DTO
    {
        public FR_REGRAS_DTO()
        {
            this.FR_DOC_REGRAS_IMAGEM = new List<FR_DOC_REGRAS_IMAGEM_DTO>();
            this.FR_TAREFA = new List<FR_TAREFA_DTO>();
        }

        public int REG_COD { get; set; }
        public string REG_NOME { get; set; }
        public string REG_DESCRICAO { get; set; }
        public string REG_PARAMS { get; set; }
        public string REG_VARIAVEIS { get; set; }
        public string REG_PARAMS_OUT { get; set; }
        public string REG_INTERFACE { get; set; }
        public string REG_SCRIPT { get; set; }
        public Nullable<System.DateTime> REG_DATA { get; set; }
        public Nullable<System.DateTime> REG_HORA { get; set; }
        public string REG_COMPILADA { get; set; }
        public Nullable<int> REG_DESTINO { get; set; }
        public string REG_HASH { get; set; }
        public Nullable<int> CAT_COD { get; set; }
        public Nullable<int> USR_CODIGO { get; set; }
        public virtual ICollection<FR_DOC_REGRAS_IMAGEM_DTO> FR_DOC_REGRAS_IMAGEM { get; set; }
        public virtual FR_REGRAS_CATEGORIAS_DTO FR_REGRAS_CATEGORIAS { get; set; }
        public virtual ICollection<FR_TAREFA_DTO> FR_TAREFA { get; set; }
    }
}
