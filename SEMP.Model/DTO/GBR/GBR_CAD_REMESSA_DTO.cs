using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_CAD_REMESSA_DTO
    {
        public int COD_REMESSA { get; set; }
        public string REMESSA { get; set; }
        public Nullable<int> COD_SKU { get; set; }
        public Nullable<System.DateTime> DATA_ABERTURA { get; set; }
        public Nullable<System.DateTime> DATA_FECHAMENTO { get; set; }
        public Nullable<bool> ATIVO { get; set; }
        public Nullable<bool> FORCADO { get; set; }
        public Nullable<int> NO_MAXIMO_SERIAL { get; set; }
        public Nullable<bool> SERIAL_ENGINES { get; set; }
        public string VARIANTE { get; set; }
        public string REMESSA_GAM { get; set; }
        public Nullable<int> DOCVENDA { get; set; }
    }
}
