namespace SEMP.DAL.IDW
{
	using System;
	using System.Data.Entity;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	public partial class IDWContext : DbContext
	{
		public IDWContext()
			: base("name=IDWContext")
		{
		}

		public virtual DbSet<DATABASECHANGELOG> DATABASECHANGELOG { get; set; }
		public virtual DbSet<DATABASECHANGELOGLOCK> DATABASECHANGELOGLOCK { get; set; }
		public virtual DbSet<dtproperties> dtproperties { get; set; }
		public virtual DbSet<dw_cal> dw_cal { get; set; }
		public virtual DbSet<dw_calavu> dw_calavu { get; set; }
		public virtual DbSet<dw_calpt> dw_calpt { get; set; }
		public virtual DbSet<dw_calsem> dw_calsem { get; set; }
		public virtual DbSet<dw_consol> dw_consol { get; set; }
		public virtual DbSet<dw_consol_param> dw_consol_param { get; set; }
		public virtual DbSet<dw_consol_parammed> dw_consol_parammed { get; set; }
		public virtual DbSet<dw_consolal> dw_consolal { get; set; }
		public virtual DbSet<dw_consolallog> dw_consolallog { get; set; }
		public virtual DbSet<dw_consolalmo> dw_consolalmo { get; set; }
		public virtual DbSet<dw_consolaloco> dw_consolaloco { get; set; }
		public virtual DbSet<dw_consolatlog> dw_consolatlog { get; set; }
		public virtual DbSet<dw_consolciplog> dw_consolciplog { get; set; }
		public virtual DbSet<dw_consolcipoco> dw_consolcipoco { get; set; }
		public virtual DbSet<dw_consoldef> dw_consoldef { get; set; }
		public virtual DbSet<dw_consolestlocalpro> dw_consolestlocalpro { get; set; }
		public virtual DbSet<dw_consolestlocalprotemp> dw_consolestlocalprotemp { get; set; }
		public virtual DbSet<dw_consolid> dw_consolid { get; set; }
		public virtual DbSet<dw_consollog> dw_consollog { get; set; }
		public virtual DbSet<dw_consolmedparamlog> dw_consolmedparamlog { get; set; }
		public virtual DbSet<dw_consolmo> dw_consolmo { get; set; }
		public virtual DbSet<dw_consolmolog> dw_consolmolog { get; set; }
		public virtual DbSet<dw_consolmooco> dw_consolmooco { get; set; }
		public virtual DbSet<dw_consolpa> dw_consolpa { get; set; }
		public virtual DbSet<dw_consolpa_param> dw_consolpa_param { get; set; }
		public virtual DbSet<dw_consolpalog> dw_consolpalog { get; set; }
		public virtual DbSet<dw_consolpalogtec> dw_consolpalogtec { get; set; }
		public virtual DbSet<dw_consolpamo> dw_consolpamo { get; set; }
		public virtual DbSet<dw_consolpaoco> dw_consolpaoco { get; set; }
		public virtual DbSet<dw_consolpemp> dw_consolpemp { get; set; }
		public virtual DbSet<dw_consolpempoco> dw_consolpempoco { get; set; }
		public virtual DbSet<dw_consolperdamplog> dw_consolperdamplog { get; set; }
		public virtual DbSet<dw_consolpr> dw_consolpr { get; set; }
		public virtual DbSet<dw_consolprmo> dw_consolprmo { get; set; }
		public virtual DbSet<dw_consolpt> dw_consolpt { get; set; }
		public virtual DbSet<dw_consolre> dw_consolre { get; set; }
		public virtual DbSet<dw_consolrelog> dw_consolrelog { get; set; }
		public virtual DbSet<dw_consolremo> dw_consolremo { get; set; }
		public virtual DbSet<dw_consolreoco> dw_consolreoco { get; set; }
		public virtual DbSet<dw_consolsplog> dw_consolsplog { get; set; }
		public virtual DbSet<dw_consolvaritmo> dw_consolvaritmo { get; set; }
		public virtual DbSet<dw_consolvaritmolog> dw_consolvaritmolog { get; set; }
		public virtual DbSet<dw_consolvaritmologcau> dw_consolvaritmologcau { get; set; }
		public virtual DbSet<dw_consolvaritmooco> dw_consolvaritmooco { get; set; }
		public virtual DbSet<dw_desalimpendcontag> dw_desalimpendcontag { get; set; }
		public virtual DbSet<dw_detativ> dw_detativ { get; set; }
		public virtual DbSet<dw_est> dw_est { get; set; }
		public virtual DbSet<dw_estlocal> dw_estlocal { get; set; }
		public virtual DbSet<dw_estlocalpro> dw_estlocalpro { get; set; }
		public virtual DbSet<dw_estmov> dw_estmov { get; set; }
		public virtual DbSet<dw_estpro> dw_estpro { get; set; }
		public virtual DbSet<dw_estsalma> dw_estsalma { get; set; }
		public virtual DbSet<dw_expcvs> dw_expcvs { get; set; }
		public virtual DbSet<dw_expcvspf> dw_expcvspf { get; set; }
		public virtual DbSet<dw_folha> dw_folha { get; set; }
		public virtual DbSet<dw_folhacic> dw_folhacic { get; set; }
		public virtual DbSet<dw_folhaemb> dw_folhaemb { get; set; }
		public virtual DbSet<dw_folhaiac> dw_folhaiac { get; set; }
		public virtual DbSet<dw_folhamedtemhor> dw_folhamedtemhor { get; set; }
		public virtual DbSet<dw_folhamedtemp> dw_folhamedtemp { get; set; }
		public virtual DbSet<dw_folhamedtemphorcfg> dw_folhamedtemphorcfg { get; set; }
		public virtual DbSet<dw_folhamon> dw_folhamon { get; set; }
		public virtual DbSet<dw_folhamoncomp> dw_folhamoncomp { get; set; }
		public virtual DbSet<dw_folhaoperacao> dw_folhaoperacao { get; set; }
		public virtual DbSet<dw_folharap> dw_folharap { get; set; }
		public virtual DbSet<dw_folharapcom> dw_folharapcom { get; set; }
		public virtual DbSet<dw_folhasetup> dw_folhasetup { get; set; }
		public virtual DbSet<dw_folhateste> dw_folhateste { get; set; }
		public virtual DbSet<dw_folhatv> dw_folhatv { get; set; }
		public virtual DbSet<dw_ft_etapa> dw_ft_etapa { get; set; }
		public virtual DbSet<dw_ft_grupo> dw_ft_grupo { get; set; }
		public virtual DbSet<dw_ft_param> dw_ft_param { get; set; }
		public virtual DbSet<dw_ft_sub> dw_ft_sub { get; set; }
		public virtual DbSet<dw_ft_subparam> dw_ft_subparam { get; set; }
		public virtual DbSet<dw_grpativ> dw_grpativ { get; set; }
		public virtual DbSet<dw_grupo_ferramenta> dw_grupo_ferramenta { get; set; }
		public virtual DbSet<dw_macrange> dw_macrange { get; set; }
		public virtual DbSet<dw_macuso> dw_macuso { get; set; }
		public virtual DbSet<dw_nserie> dw_nserie { get; set; }
		public virtual DbSet<dw_nserieobs> dw_nserieobs { get; set; }
		public virtual DbSet<dw_operacao> dw_operacao { get; set; }
		public virtual DbSet<dw_operacaocomp> dw_operacaocomp { get; set; }
		public virtual DbSet<dw_operacaomidia> dw_operacaomidia { get; set; }
		public virtual DbSet<dw_operacaopredecessora> dw_operacaopredecessora { get; set; }
		public virtual DbSet<dw_operacaorap> dw_operacaorap { get; set; }
		public virtual DbSet<dw_passagem> dw_passagem { get; set; }
		public virtual DbSet<dw_passcau> dw_passcau { get; set; }
		public virtual DbSet<dw_passdef> dw_passdef { get; set; }
		public virtual DbSet<dw_passmon> dw_passmon { get; set; }
		public virtual DbSet<dw_passtf> dw_passtf { get; set; }
		public virtual DbSet<dw_passtfse> dw_passtfse { get; set; }
		public virtual DbSet<dw_passtfsepm> dw_passtfsepm { get; set; }
		public virtual DbSet<dw_passtfsepm_hist> dw_passtfsepm_hist { get; set; }
		public virtual DbSet<dw_pepro> dw_pepro { get; set; }
		public virtual DbSet<dw_procarhom> dw_procarhom { get; set; }
		public virtual DbSet<dw_procativ> dw_procativ { get; set; }
		public virtual DbSet<dw_procedimento> dw_procedimento { get; set; }
		public virtual DbSet<dw_produtoconjugado> dw_produtoconjugado { get; set; }
		public virtual DbSet<dw_prorea> dw_prorea { get; set; }
		public virtual DbSet<dw_proreaativ> dw_proreaativ { get; set; }
		public virtual DbSet<dw_proreaativobs> dw_proreaativobs { get; set; }
		public virtual DbSet<dw_proreausr> dw_proreausr { get; set; }
		public virtual DbSet<dw_rap> dw_rap { get; set; }
		public virtual DbSet<dw_rap_grupo> dw_rap_grupo { get; set; }
		public virtual DbSet<dw_rota> dw_rota { get; set; }
		public virtual DbSet<dw_rotapasso> dw_rotapasso { get; set; }
		public virtual DbSet<dw_rotapasso_pt> dw_rotapasso_pt { get; set; }
		public virtual DbSet<dw_rp_predecessora> dw_rp_predecessora { get; set; }
		public virtual DbSet<dw_rt> dw_rt { get; set; }
		public virtual DbSet<dw_rtcic> dw_rtcic { get; set; }
		public virtual DbSet<dw_t_acao> dw_t_acao { get; set; }
		public virtual DbSet<dw_t_alerta> dw_t_alerta { get; set; }
		public virtual DbSet<dw_t_area> dw_t_area { get; set; }
		public virtual DbSet<dw_t_causa> dw_t_causa { get; set; }
		public virtual DbSet<dw_t_defeito> dw_t_defeito { get; set; }
		public virtual DbSet<dw_t_just> dw_t_just { get; set; }
		public virtual DbSet<dw_t_operacao> dw_t_operacao { get; set; }
		public virtual DbSet<dw_t_parada> dw_t_parada { get; set; }
		public virtual DbSet<dw_t_perdamp> dw_t_perdamp { get; set; }
		public virtual DbSet<dw_t_refugo> dw_t_refugo { get; set; }
		public virtual DbSet<dw_t_ritmo> dw_t_ritmo { get; set; }
		public virtual DbSet<dw_testesub> dw_testesub { get; set; }
		public virtual DbSet<dw_testesubetapa> dw_testesubetapa { get; set; }
		public virtual DbSet<dw_turno> dw_turno { get; set; }
		public virtual DbSet<dw_versaodb> dw_versaodb { get; set; }
		public virtual DbSet<ip_balanceamento> ip_balanceamento { get; set; }
		public virtual DbSet<ms_cck> ms_cck { get; set; }
		public virtual DbSet<ms_cfg> ms_cfg { get; set; }
		public virtual DbSet<ms_detector> ms_detector { get; set; }
		public virtual DbSet<ms_detectorusr> ms_detectorusr { get; set; }
		public virtual DbSet<ms_evt> ms_evt { get; set; }
		public virtual DbSet<ms_evtcep> ms_evtcep { get; set; }
		public virtual DbSet<ms_evtdefeito> ms_evtdefeito { get; set; }
		public virtual DbSet<ms_evtmontagem> ms_evtmontagem { get; set; }
		public virtual DbSet<ms_ic> ms_ic { get; set; }
		public virtual DbSet<ms_ihm> ms_ihm { get; set; }
		public virtual DbSet<ms_ind> ms_ind { get; set; }
		public virtual DbSet<ms_monitor> ms_monitor { get; set; }
		public virtual DbSet<ms_ms> ms_ms { get; set; }
		public virtual DbSet<ms_msicup> ms_msicup { get; set; }
		public virtual DbSet<ms_msihm> ms_msihm { get; set; }
		public virtual DbSet<ms_perfilandon> ms_perfilandon { get; set; }
		public virtual DbSet<ms_perfilregras> ms_perfilregras { get; set; }
		public virtual DbSet<ms_pt_coleta> ms_pt_coleta { get; set; }
		public virtual DbSet<ms_pt_coleta_login> ms_pt_coleta_login { get; set; }
		public virtual DbSet<ms_tpevt> ms_tpevt { get; set; }
		public virtual DbSet<ms_trigger> ms_trigger { get; set; }
		public virtual DbSet<ms_up> ms_up { get; set; }
		public virtual DbSet<ms_upihm> ms_upihm { get; set; }
		public virtual DbSet<ms_usr> ms_usr { get; set; }
		public virtual DbSet<om_algocor> om_algocor { get; set; }
		public virtual DbSet<om_alim> om_alim { get; set; }
		public virtual DbSet<om_alimrea> om_alimrea { get; set; }
		public virtual DbSet<om_cargo> om_cargo { get; set; }
		public virtual DbSet<om_cc> om_cc { get; set; }
		public virtual DbSet<om_cfg> om_cfg { get; set; }
		public virtual DbSet<om_cfgabc> om_cfgabc { get; set; }
		public virtual DbSet<om_cfgabclim> om_cfgabclim { get; set; }
		public virtual DbSet<om_cfgind> om_cfgind { get; set; }
		public virtual DbSet<om_cfgptdetcoleta> om_cfgptdetcoleta { get; set; }
		public virtual DbSet<om_cfgscrpimp> om_cfgscrpimp { get; set; }
		public virtual DbSet<om_cfgurl> om_cfgurl { get; set; }
		public virtual DbSet<om_clidiario> om_clidiario { get; set; }
		public virtual DbSet<om_clp> om_clp { get; set; }
		public virtual DbSet<om_contato> om_contato { get; set; }
		public virtual DbSet<om_empresa> om_empresa { get; set; }
		public virtual DbSet<om_for> om_for { get; set; }
		public virtual DbSet<om_garantia> om_garantia { get; set; }
		public virtual DbSet<om_garpro> om_garpro { get; set; }
		public virtual DbSet<om_grnts> om_grnts { get; set; }
		public virtual DbSet<om_gt> om_gt { get; set; }
		public virtual DbSet<om_homogt> om_homogt { get; set; }
		public virtual DbSet<om_homopt> om_homopt { get; set; }
		public virtual DbSet<om_im> om_im { get; set; }
		public virtual DbSet<om_img> om_img { get; set; }
		public virtual DbSet<om_ind> om_ind { get; set; }
		public virtual DbSet<om_indgt> om_indgt { get; set; }
		public virtual DbSet<om_indpt> om_indpt { get; set; }
		public virtual DbSet<om_indtppt> om_indtppt { get; set; }
		public virtual DbSet<om_job> om_job { get; set; }
		public virtual DbSet<om_job_recurso> om_job_recurso { get; set; }
		public virtual DbSet<om_jobdet> om_jobdet { get; set; }
		public virtual DbSet<om_jobdetlog> om_jobdetlog { get; set; }
		public virtual DbSet<om_joblog> om_joblog { get; set; }
		public virtual DbSet<om_licenca> om_licenca { get; set; }
		public virtual DbSet<om_licenca_pt> om_licenca_pt { get; set; }
		public virtual DbSet<om_licmodrec> om_licmodrec { get; set; }
		public virtual DbSet<om_mapa> om_mapa { get; set; }
		public virtual DbSet<om_mapapa> om_mapapa { get; set; }
		public virtual DbSet<om_modulo_recurso> om_modulo_recurso { get; set; }
		public virtual DbSet<om_obj> om_obj { get; set; }
		public virtual DbSet<om_pa> om_pa { get; set; }
		public virtual DbSet<om_papro> om_papro { get; set; }
		public virtual DbSet<om_prg> om_prg { get; set; }
		public virtual DbSet<om_prgpos> om_prgpos { get; set; }
		public virtual DbSet<om_proaltglo> om_proaltglo { get; set; }
		public virtual DbSet<om_procomest> om_procomest { get; set; }
		public virtual DbSet<om_produto> om_produto { get; set; }
		public virtual DbSet<om_progrp> om_progrp { get; set; }
		public virtual DbSet<om_promidia> om_promidia { get; set; }
		public virtual DbSet<om_propaihomo> om_propaihomo { get; set; }
		public virtual DbSet<om_proturno> om_proturno { get; set; }
		public virtual DbSet<om_pt> om_pt { get; set; }
		public virtual DbSet<om_ptcnc> om_ptcnc { get; set; }
		public virtual DbSet<om_ptcp> om_ptcp { get; set; }
		public virtual DbSet<om_regras_nscb> om_regras_nscb { get; set; }
		public virtual DbSet<om_regras_tags> om_regras_tags { get; set; }
		public virtual DbSet<om_resgui> om_resgui { get; set; }
		public virtual DbSet<om_tags> om_tags { get; set; }
		public virtual DbSet<om_texto> om_texto { get; set; }
		public virtual DbSet<om_tpgt> om_tpgt { get; set; }
		public virtual DbSet<om_tplicenca> om_tplicenca { get; set; }
		public virtual DbSet<om_tppt> om_tppt { get; set; }
		public virtual DbSet<om_unidmedida> om_unidmedida { get; set; }
		public virtual DbSet<om_usr> om_usr { get; set; }
		public virtual DbSet<om_usrgrp> om_usrgrp { get; set; }
		public virtual DbSet<om_webcam> om_webcam { get; set; }
		public virtual DbSet<pp_cliente> pp_cliente { get; set; }
		public virtual DbSet<pp_cm> pp_cm { get; set; }
		public virtual DbSet<pp_cmcom> pp_cmcom { get; set; }
		public virtual DbSet<pp_cp> pp_cp { get; set; }
		public virtual DbSet<pp_cp_data> pp_cp_data { get; set; }
		public virtual DbSet<pp_cp_hora> pp_cp_hora { get; set; }
		public virtual DbSet<pp_cp_pre> pp_cp_pre { get; set; }
		public virtual DbSet<pp_cp_turno> pp_cp_turno { get; set; }
		public virtual DbSet<pp_cpentsai> pp_cpentsai { get; set; }
		public virtual DbSet<pp_cpfaltamp> pp_cpfaltamp { get; set; }
		public virtual DbSet<pp_cpneccron> pp_cpneccron { get; set; }
		public virtual DbSet<pp_cpnserie> pp_cpnserie { get; set; }
		public virtual DbSet<pp_cpproduto> pp_cpproduto { get; set; }
		public virtual DbSet<pp_indisp> pp_indisp { get; set; }
		public virtual DbSet<pp_indisp_rappt> pp_indisp_rappt { get; set; }
		public virtual DbSet<pp_nec> pp_nec { get; set; }
		public virtual DbSet<pp_neccron> pp_neccron { get; set; }
		public virtual DbSet<pp_necimp> pp_necimp { get; set; }
		public virtual DbSet<pp_necimplog> pp_necimplog { get; set; }
		public virtual DbSet<pp_necimpurl> pp_necimpurl { get; set; }
		public virtual DbSet<pp_necimpurllog> pp_necimpurllog { get; set; }
		public virtual DbSet<pp_plancol> pp_plancol { get; set; }
		public virtual DbSet<pp_planec> pp_planec { get; set; }
		public virtual DbSet<pp_planeccron> pp_planeccron { get; set; }
		public virtual DbSet<pp_plano> pp_plano { get; set; }
		public virtual DbSet<pp_planpro> pp_planpro { get; set; }
		public virtual DbSet<pp_planptgt> pp_planptgt { get; set; }
		public virtual DbSet<pp_tpplano> pp_tpplano { get; set; }
		public virtual DbSet<te_concessionaria> te_concessionaria { get; set; }
		public virtual DbSet<te_lei> te_lei { get; set; }
		public virtual DbSet<te_tarifas> te_tarifas { get; set; }
		public virtual DbSet<te_tarifasemanal> te_tarifasemanal { get; set; }
		public virtual DbSet<te_tipo_consumidor> te_tipo_consumidor { get; set; }
		public virtual DbSet<tmp_Trace> tmp_Trace { get; set; }
		public virtual DbSet<tt_sap_con> tt_sap_con { get; set; }
		public virtual DbSet<tt_sap_estmppa> tt_sap_estmppa { get; set; }
		public virtual DbSet<tt_sap_saiexp> tt_sap_saiexp { get; set; }
		public virtual DbSet<tt_tmg_con> tt_tmg_con { get; set; }
		public virtual DbSet<tmp_dw_consolpa> tmp_dw_consolpa { get; set; }
		public virtual DbSet<dw_passtfsepm_view> dw_passtfsepm_view { get; set; }
		public virtual DbSet<eventos> eventos { get; set; }
		public virtual DbSet<eventos_pm_desal> eventos_pm_desal { get; set; }
		public virtual DbSet<ms_dthr> ms_dthr { get; set; }
		public virtual DbSet<v_aa> v_aa { get; set; }
		public virtual DbSet<v_aux_temp> v_aux_temp { get; set; }
		public virtual DbSet<v_estoqueprocesso> v_estoqueprocesso { get; set; }
		public virtual DbSet<v_estoqueprocesso_mov> v_estoqueprocesso_mov { get; set; }
		public virtual DbSet<v_perdacomponente> v_perdacomponente { get; set; }
		public virtual DbSet<v_paradas> v_paradas { get; set; }
		public virtual DbSet<v_producaohora> v_producaohora { get; set; }
		public virtual DbSet<v_producaoturno> v_producaoturno { get; set; }
		public virtual DbSet<v_perdacomponente_semiacabado> v_perdacomponente_semiacabado { get; set; }
		public virtual DbSet<VW_TESTE> VW_TESTE { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<DATABASECHANGELOG>()
				.Property(e => e.ID)
				.IsUnicode(false);

			modelBuilder.Entity<DATABASECHANGELOG>()
				.Property(e => e.AUTHOR)
				.IsUnicode(false);

			modelBuilder.Entity<DATABASECHANGELOG>()
				.Property(e => e.FILENAME)
				.IsUnicode(false);

			modelBuilder.Entity<DATABASECHANGELOG>()
				.Property(e => e.EXECTYPE)
				.IsUnicode(false);

			modelBuilder.Entity<DATABASECHANGELOG>()
				.Property(e => e.MD5SUM)
				.IsUnicode(false);

			modelBuilder.Entity<DATABASECHANGELOG>()
				.Property(e => e.DESCRIPTION)
				.IsUnicode(false);

			modelBuilder.Entity<DATABASECHANGELOG>()
				.Property(e => e.COMMENTS)
				.IsUnicode(false);

			modelBuilder.Entity<DATABASECHANGELOG>()
				.Property(e => e.TAG)
				.IsUnicode(false);

			modelBuilder.Entity<DATABASECHANGELOG>()
				.Property(e => e.LIQUIBASE)
				.IsUnicode(false);

			modelBuilder.Entity<DATABASECHANGELOGLOCK>()
				.Property(e => e.LOCKEDBY)
				.IsUnicode(false);

			modelBuilder.Entity<dtproperties>()
				.Property(e => e.property)
				.IsUnicode(false);

			modelBuilder.Entity<dtproperties>()
				.Property(e => e.value)
				.IsUnicode(false);

			modelBuilder.Entity<dw_cal>()
				.Property(e => e.cd_cal)
				.IsUnicode(false);

			modelBuilder.Entity<dw_cal>()
				.Property(e => e.ds_cal)
				.IsUnicode(false);

			modelBuilder.Entity<dw_cal>()
				.HasMany(e => e.dw_calavu)
				.WithRequired(e => e.dw_cal)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_cal>()
				.HasMany(e => e.dw_calpt)
				.WithRequired(e => e.dw_cal)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_cal>()
				.HasMany(e => e.dw_calsem)
				.WithRequired(e => e.dw_cal)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_cal>()
				.HasMany(e => e.dw_consolid)
				.WithRequired(e => e.dw_cal)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_cal>()
				.HasMany(e => e.dw_consolestlocalprotemp)
				.WithRequired(e => e.dw_cal)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_cal>()
				.HasMany(e => e.dw_consolestlocalpro)
				.WithRequired(e => e.dw_cal)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_cal>()
				.HasMany(e => e.om_cfg)
				.WithRequired(e => e.dw_cal)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_calavu>()
				.Property(e => e.hrinicial)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_calavu>()
				.Property(e => e.hrFinal)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_calavu>()
				.Property(e => e.hrInicialGUI)
				.IsUnicode(false);

			modelBuilder.Entity<dw_calavu>()
				.Property(e => e.hrFinalGUI)
				.IsUnicode(false);

			modelBuilder.Entity<dw_calavu>()
				.Property(e => e.seg_tempocalendario)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_calavu>()
				.Property(e => e.seg_tempocalsempeso)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_calavu>()
				.Property(e => e.seg_toleranciapre)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_calavu>()
				.Property(e => e.seg_toleranciapos)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_calsem>()
				.Property(e => e.diasemana)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_calsem>()
				.Property(e => e.hrInicial)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_calsem>()
				.Property(e => e.hrFinal)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_calsem>()
				.Property(e => e.hrInicialGUI)
				.IsUnicode(false);

			modelBuilder.Entity<dw_calsem>()
				.Property(e => e.hrFinalGUI)
				.IsUnicode(false);

			modelBuilder.Entity<dw_calsem>()
				.Property(e => e.seg_tempocalendario)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_calsem>()
				.Property(e => e.seg_toleranciapre)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_calsem>()
				.Property(e => e.seg_tempocalsempeso)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_calsem>()
				.Property(e => e.seg_toleranciapos)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempocalendario)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_ciclomedio)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_perdaciclo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempodisponivel)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempoprodutivas)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_ritmo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_temporefugadas)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempotrabalhado)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_cicloimprodutivo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_cicloprodutivo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempoparada_sp)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempoparada)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempoativo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempoativo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempoparada)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempoparada_sp)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tmpcicnormal)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_ciclopadrao)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_perdacav)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_cicloprodutivo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_boas)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_cicloimprodutivo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempotrabalhado)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_temporefugadas)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_ritmo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempoprodutivas)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempodisponivel)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_perdaciclo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_ciclomedio)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_ciclopadrao)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tmpcicnormal)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_perdacav)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_boas)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempocalendario)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempoalerta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempoalerta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempocalsempeso)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempocalsempeso)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_ocoparadacp)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_ocoparadasp)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempoparadaMTBF)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempoparadaMTTR)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempoparadaPAO)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempoparadaPA)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempoparadaPTP)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempoparadaSCP)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempoparadaMDO)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.g_peso_bruto)
				.HasPrecision(7, 3);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.g_pelo_liquido)
				.HasPrecision(7, 3);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempoparada_ab)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_cicloimprodutivo)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_cicloimprodutivo)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_cicloprodutivo)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_cicloprodutivo)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_cicloregulagem)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_cicloregulagem)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_ocoparada_cp)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_ocoparada_cp)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_ocoparada_sp)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_ocoparada_sp)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_ocoparadaFDS)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_ocoparadaIMPREV)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_ocoparadaMDO)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_ocoparadaMTBF)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_ocoparadaMTTR)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_ocoparadaPA)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_ocoparadaPAO)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_ocoparadaPP)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_ocoparadaPREV)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_ocoparadaPTP)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_ocoparadaREGULAGEM)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_ocoparadaSCP)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_ocoparadaFDS)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_ocoparadaIMPREV)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_ocoparadaMDO)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_ocoparadaMTBF)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_ocoparadaMTTR)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_ocoparadaPA)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_ocoparadaPAO)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_ocoparadaPP)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_ocoparadaPREV)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_ocoparadaPTP)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_ocoparadaREGULAGEM)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_ocoparadaSCP)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.SEG_AUTO_TEMPOPARADA_CP)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.SEG_MANU_TEMPOPARADA_CP)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempoparadaFDS)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempoparadaIMPREV)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempoparadaMDO)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempoparadaMTBF)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempoparadaMTTR)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempoparadaPA)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempoparadaPAO)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempoparadaPP)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempoparadaPREV)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempoparadaPTP)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempoparadaREGULAGEM)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempoparadaSCP)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempoparadaFDS)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempoparadaIMPREV)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempoparadaPP)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempoparadaPREV)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempoparadaREGULAGEM)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.SEG_AUTO_TEMPOPRODUTIVO)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.SEG_MANU_TEMPOPRODUTIVO)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.SEG_MANU_CICLOREGULAGEM)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.SEG_AUTO_CICLOREGULAGEM)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_perdamp)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_perdamp)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_cicloprodutivo_cta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_cicloimprodutivo_cta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_cicloprodutivo_cta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_cicloimprodutivo_cta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_cta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_cta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempoparada_cp_vr)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempoparada_sp_vr)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempoparada_cp_vr)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempoparada_sp_vr)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_ocoparada_cp_vr)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_ocoparada_sp_vr)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_ocoparada_cp_vr)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_ocoparada_sp_vr)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempoparada_default)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempoparada_sem_op)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempoparada_sem_evt)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_auto_tempoparada_sem_cnx)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempoparada_default)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempoparada_sem_op)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempoparada_sem_evt)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.seg_manu_tempoparada_sem_cnx)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_tempoparada_default)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_tempoparada_sem_op)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_tempoparada_sem_evt)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_tempoparada_sem_cnx)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_tempoparada_default)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_tempoparada_sem_op)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_tempoparada_sem_evt)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_tempoparada_sem_cnx)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_cicloprevisto)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_manu_cicloprevisto)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.g_auto_peso_bruto2)
				.HasPrecision(20, 3);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.g_auto_peso_liquido2)
				.HasPrecision(20, 3);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.g_manu_peso_bruto2)
				.HasPrecision(20, 3);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.g_manu_peso_liquido2)
				.HasPrecision(20, 3);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.g_auto_peso_bruto)
				.HasPrecision(20, 3);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.g_auto_peso_liquido)
				.HasPrecision(20, 3);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.g_manu_peso_bruto)
				.HasPrecision(20, 3);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.g_manu_peso_liquido)
				.HasPrecision(20, 3);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_cavativas)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.qt_auto_cavtotal)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.pcs_auto_producaoliquida)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consol>()
				.Property(e => e.pcs_manu_producaoliquida)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consol>()
				.HasMany(e => e.dw_consolal)
				.WithRequired(e => e.dw_consol)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consol>()
				.HasMany(e => e.dw_consollog)
				.WithRequired(e => e.dw_consol)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consol>()
				.HasMany(e => e.dw_consol_param)
				.WithRequired(e => e.dw_consol)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consol>()
				.HasMany(e => e.dw_consolmo)
				.WithRequired(e => e.dw_consol)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consol>()
				.HasMany(e => e.dw_consolpr)
				.WithRequired(e => e.dw_consol)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consol>()
				.HasMany(e => e.dw_consolvaritmo)
				.WithRequired(e => e.dw_consol)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consol>()
				.HasMany(e => e.dw_consolre)
				.WithRequired(e => e.dw_consol)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consol>()
				.HasMany(e => e.dw_consolpemp)
				.WithRequired(e => e.dw_consol)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consol>()
				.HasMany(e => e.dw_consolpa)
				.WithRequired(e => e.dw_consol)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consol>()
				.HasMany(e => e.dw_consoldef)
				.WithRequired(e => e.dw_consol)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consol>()
				.HasMany(e => e.dw_consolcipoco)
				.WithRequired(e => e.dw_consol)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consol_param>()
				.Property(e => e.vl_minimo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol_param>()
				.Property(e => e.vl_maximo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol_param>()
				.Property(e => e.vl_medio)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol_param>()
				.Property(e => e.vl_somado)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consol_param>()
				.Property(e => e.vl_monetario)
				.HasPrecision(20, 4);

			modelBuilder.Entity<dw_consolal>()
				.Property(e => e.seg_auto_tempoalerta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolal>()
				.Property(e => e.seg_manu_tempoalerta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolal>()
				.HasMany(e => e.dw_consolalmo)
				.WithRequired(e => e.dw_consolal)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consolal>()
				.HasMany(e => e.dw_consolaloco)
				.WithRequired(e => e.dw_consolal)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consolallog>()
				.Property(e => e.seg_auto_tempoalerta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolallog>()
				.Property(e => e.seg_manu_tempoalerta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolallog>()
				.Property(e => e.obs)
				.IsUnicode(false);

			modelBuilder.Entity<dw_consolalmo>()
				.Property(e => e.seg_auto_tempoalerta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolalmo>()
				.Property(e => e.seg_manu_tempoalerta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolatlog>()
				.Property(e => e.url_conexao)
				.IsUnicode(false);

			modelBuilder.Entity<dw_consoldef>()
				.Property(e => e.qt_defeitos)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolestlocalpro>()
				.Property(e => e.ano)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolestlocalpro>()
				.Property(e => e.mes)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolestlocalpro>()
				.Property(e => e.qt_entrada)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolestlocalpro>()
				.Property(e => e.qt_saida)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolestlocalpro>()
				.Property(e => e.qt_perda)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolestlocalpro>()
				.Property(e => e.qt_consumida)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolestlocalpro>()
				.Property(e => e.qt_ajuste)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolestlocalpro>()
				.Property(e => e.qt_total)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolestlocalprotemp>()
				.Property(e => e.qt)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolid>()
				.Property(e => e.ano)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolid>()
				.Property(e => e.mes)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolid>()
				.Property(e => e.ds_especializaApon)
				.IsUnicode(false);

			modelBuilder.Entity<dw_consolid>()
				.HasMany(e => e.dw_consol)
				.WithRequired(e => e.dw_consolid)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consolid>()
				.HasMany(e => e.dw_consolciplog)
				.WithOptional(e => e.dw_consolid)
				.HasForeignKey(e => e.id_consolid_inicio);

			modelBuilder.Entity<dw_consolid>()
				.HasMany(e => e.dw_consolciplog1)
				.WithOptional(e => e.dw_consolid1)
				.HasForeignKey(e => e.id_consolid_fim);

			modelBuilder.Entity<dw_consolid>()
				.HasMany(e => e.dw_consolpt)
				.WithOptional(e => e.dw_consolid)
				.HasForeignKey(e => e.id_consolid_turno);

			modelBuilder.Entity<dw_consolid>()
				.HasMany(e => e.dw_consolpt1)
				.WithOptional(e => e.dw_consolid1)
				.HasForeignKey(e => e.id_consolid_hora);

			modelBuilder.Entity<dw_consolid>()
				.HasMany(e => e.dw_consolpt2)
				.WithOptional(e => e.dw_consolid2)
				.HasForeignKey(e => e.id_consolid_mes);

			modelBuilder.Entity<dw_consolid>()
				.HasMany(e => e.dw_consolpt3)
				.WithOptional(e => e.dw_consolid3)
				.HasForeignKey(e => e.id_consolid_acu);

			modelBuilder.Entity<dw_consolid>()
				.HasMany(e => e.dw_passagem)
				.WithRequired(e => e.dw_consolid)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consollog>()
				.Property(e => e.seg_corr_tempoativo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consollog>()
				.Property(e => e.seg_corr_tempoparada)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consollog>()
				.Property(e => e.seg_corr_tempoparada_sp)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consollog>()
				.Property(e => e.ds_log)
				.IsUnicode(false);

			modelBuilder.Entity<dw_consollog>()
				.Property(e => e.seg_corr_cicloprodutivo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consollog>()
				.Property(e => e.seg_corr_cicloimprodutivo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consollog>()
				.Property(e => e.seg_corr_tempotrabalhado)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consollog>()
				.Property(e => e.seg_corr_temporefugadas)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consollog>()
				.Property(e => e.seg_corr_ritmo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consollog>()
				.Property(e => e.seg_corr_tempoprodutivas)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consollog>()
				.Property(e => e.seg_corr_tempodisponivel)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consollog>()
				.Property(e => e.seg_corr_perdaciclo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consollog>()
				.Property(e => e.seg_corr_ciclomedio)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consollog>()
				.Property(e => e.seg_corr_ciclopadrao)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consollog>()
				.Property(e => e.seg_corr_tmpcicnormal)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consollog>()
				.Property(e => e.seg_corr_perdacav)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consollog>()
				.Property(e => e.seg_corr_boas)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consollog>()
				.Property(e => e.seg_corr_tempocalendario)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consollog>()
				.Property(e => e.seg_corr_tempoalerta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consollog>()
				.Property(e => e.seg_corr_tempocalsempeso)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmedparamlog>()
				.Property(e => e.vlr_lido)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolmedparamlog>()
				.Property(e => e.vl_monetario)
				.HasPrecision(20, 4);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_auto_tempologin)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_manu_tempologin)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_auto_parada)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_auto_sp)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_auto_tempotrabalhado)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_auto_cicloprodutivo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_auto_cicloimprodutivo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_manu_cicloprodutivo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_manu_cicloimprodutivo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.QT_AUTO_CICLOIMPRODUTIVO)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.QT_MANU_CICLOIMPRODUTIVO)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.QT_AUTO_CICLOPRODUTIVO)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.QT_MANU_CICLOPRODUTIVO)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.QT_AUTO_CICLOREGULAGEM)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.QT_MANU_CICLOREGULAGEM)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.SEG_AUTO_CICLOREGULAGEM)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.SEG_MANU_CICLOREGULAGEM)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.SEG_AUTO_TEMPOPARADA_CP)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.SEG_MANU_TEMPOPARADA_CP)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.SEG_AUTO_TEMPOPARADA_SP)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.SEG_MANU_TEMPOPARADA_SP)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_auto_tempoparada_cp_vr)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_auto_tempoparada_sp_vr)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_manu_tempoparada_cp_vr)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_manu_tempoparada_sp_vr)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.qt_auto_ocoparada_cp_vr)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.qt_auto_ocoparada_sp_vr)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.qt_manu_ocoparada_cp_vr)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.qt_manu_ocoparada_sp_vr)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_auto_tempoparada_default)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_auto_tempoparada_sem_op)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_auto_tempoparada_sem_evt)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_auto_tempoparada_sem_cnx)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_manu_tempoparada_default)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_manu_tempoparada_sem_op)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_manu_tempoparada_sem_evt)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_manu_tempoparada_sem_cnx)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.qt_auto_tempoparada_default)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.qt_auto_tempoparada_sem_op)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.qt_auto_tempoparada_sem_evt)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.qt_auto_tempoparada_sem_cnx)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.qt_manu_tempoparada_default)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.qt_manu_tempoparada_sem_op)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.qt_manu_tempoparada_sem_evt)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.qt_manu_tempoparada_sem_cnx)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_auto_cicloprodutivo_cta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_auto_cicloimprodutivo_cta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_manu_cicloprodutivo_cta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_manu_cicloimprodutivo_cta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_auto_cta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.Property(e => e.seg_manu_cta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolmo>()
				.HasMany(e => e.dw_consolalmo)
				.WithRequired(e => e.dw_consolmo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consolmo>()
				.HasMany(e => e.dw_consolpamo)
				.WithRequired(e => e.dw_consolmo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consolmo>()
				.HasMany(e => e.dw_consolmooco)
				.WithRequired(e => e.dw_consolmo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consolmo>()
				.HasMany(e => e.dw_consolprmo)
				.WithRequired(e => e.dw_consolmo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consolmo>()
				.HasMany(e => e.dw_consolremo)
				.WithRequired(e => e.dw_consolmo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consolmolog>()
				.HasMany(e => e.dw_consolmooco)
				.WithRequired(e => e.dw_consolmolog)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.id_consolpa)
				.HasPrecision(18, 0);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.seg_auto_tempoparada_ab)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.qt_auto_ocoparada_cp)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.qt_auto_ocoparada_sp)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.qt_manu_ocoparada_cp)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.qt_manu_ocoparada_sp)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.seg_auto_tempoparada_cp)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.seg_auto_tempoparada_sp)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.seg_manu_tempoparada_cp)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.seg_manu_tempoparada_sp)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.seg_auto_tempoparada_cp_vr)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.seg_auto_tempoparada_sp_vr)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.seg_manu_tempoparada_cp_vr)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.seg_manu_tempoparada_sp_vr)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.qt_auto_ocoparada_cp_vr)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.qt_auto_ocoparada_sp_vr)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.qt_manu_ocoparada_cp_vr)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.qt_manu_ocoparada_sp_vr)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.seg_auto_tempoparada_default)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.seg_auto_tempoparada_sem_op)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.seg_auto_tempoparada_sem_evt)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.seg_auto_tempoparada_sem_cnx)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.seg_manu_tempoparada_default)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.seg_manu_tempoparada_sem_op)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.seg_manu_tempoparada_sem_evt)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.seg_manu_tempoparada_sem_cnx)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.qt_auto_tempoparada_default)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.qt_auto_tempoparada_sem_op)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.qt_auto_tempoparada_sem_evt)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.qt_auto_tempoparada_sem_cnx)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.qt_manu_tempoparada_default)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.qt_manu_tempoparada_sem_op)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.qt_manu_tempoparada_sem_evt)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.qt_manu_tempoparada_sem_cnx)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.seg_auto_cta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.seg_manu_cta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.pcs_auto_perdaparada_cp)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.pcs_auto_perdaparada_sp)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.pcs_manu_perdaparada_cp)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpa>()
				.Property(e => e.pcs_manu_perdaparada_sp)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpa_param>()
				.Property(e => e.vl_maximo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpa_param>()
				.Property(e => e.vl_somado)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpa_param>()
				.Property(e => e.qt_medicoes)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpa_param>()
				.Property(e => e.vl_medio)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpa_param>()
				.Property(e => e.vl_monetario)
				.HasPrecision(20, 4);

			modelBuilder.Entity<dw_consolpalog>()
				.Property(e => e.seg_auto_tempoparada)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpalog>()
				.Property(e => e.seg_auto_tempoparada_ab)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpalog>()
				.Property(e => e.seg_Auto_Tempoparada_Cp)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpalog>()
				.Property(e => e.seg_Auto_Tempoparada_sp)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpalog>()
				.Property(e => e.seg_manu_Tempoparada_Cp)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpalog>()
				.Property(e => e.seg_manu_Tempoparada_sp)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpalog>()
				.Property(e => e.seg_auto_cta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpalog>()
				.Property(e => e.seg_manu_cta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpalog>()
				.HasMany(e => e.dw_consolciplog)
				.WithOptional(e => e.dw_consolpalog)
				.HasForeignKey(e => e.id_consolpalogEntrada);

			modelBuilder.Entity<dw_consolpalog>()
				.HasMany(e => e.dw_consolciplog1)
				.WithOptional(e => e.dw_consolpalog1)
				.HasForeignKey(e => e.id_consolpalogSaida);

			modelBuilder.Entity<dw_consolpalog>()
				.HasMany(e => e.dw_consolpaoco)
				.WithRequired(e => e.dw_consolpalog)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.qt_auto_tempoparada_cp)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.qt_auto_tempoparada_sp)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.qt_manu_tempoparada_cp)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.qt_manu_tempoparada_sp)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.seg_auto_tempoparada_cp)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.seg_auto_tempoparada_sp)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.seg_manu_tempoparada_cp)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.seg_manu_tempoparada_sp)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.seg_auto_tempoparada_cp_vr)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.seg_auto_tempoparada_sp_vr)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.seg_manu_tempoparada_cp_vr)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.seg_manu_tempoparada_sp_vr)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.qt_auto_ocoparada_cp_vr)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.qt_auto_ocoparada_sp_vr)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.qt_manu_ocoparada_cp_vr)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.qt_manu_ocoparada_sp_vr)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.seg_auto_tempoparada_default)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.seg_auto_tempoparada_sem_op)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.seg_auto_tempoparada_sem_evt)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.seg_auto_tempoparada_sem_cnx)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.seg_manu_tempoparada_default)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.seg_manu_tempoparada_sem_op)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.seg_manu_tempoparada_sem_evt)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.seg_manu_tempoparada_sem_cnx)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.qt_auto_tempoparada_default)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.qt_auto_tempoparada_sem_op)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.qt_auto_tempoparada_sem_evt)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.qt_auto_tempoparada_sem_cnx)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.qt_manu_tempoparada_default)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.qt_manu_tempoparada_sem_op)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.qt_manu_tempoparada_sem_evt)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.qt_manu_tempoparada_sem_cnx)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.seg_auto_cta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpamo>()
				.Property(e => e.seg_manu_cta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpaoco>()
				.Property(e => e.origem)
				.IsUnicode(false);

			modelBuilder.Entity<dw_consolpaoco>()
				.Property(e => e.pcs_auto_perdaparada_cp)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpaoco>()
				.Property(e => e.pcs_auto_perdaparada_sp)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpaoco>()
				.Property(e => e.pcs_manu_perdaparada_cp)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpaoco>()
				.Property(e => e.pcs_manu_perdaparada_sp)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpemp>()
				.Property(e => e.qt_auto_perdamp)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpemp>()
				.Property(e => e.qt_manu_perdamp)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpemp>()
				.HasMany(e => e.dw_consolpempoco)
				.WithRequired(e => e.dw_consolpemp)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consolperdamplog>()
				.Property(e => e.qt_auto_perdamp)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolperdamplog>()
				.Property(e => e.qt_manu_perdamp)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolperdamplog>()
				.HasMany(e => e.dw_consolpempoco)
				.WithRequired(e => e.dw_consolperdamplog)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consolpr>()
				.Property(e => e.g_peso_bruto)
				.HasPrecision(7, 3);

			modelBuilder.Entity<dw_consolpr>()
				.Property(e => e.g_peso_liquido)
				.HasPrecision(7, 3);

			modelBuilder.Entity<dw_consolpr>()
				.Property(e => e.seg_auto_tempoparada)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpr>()
				.Property(e => e.seg_auto_tempoparada_sp)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpr>()
				.Property(e => e.g_auto_peso_bruto)
				.HasPrecision(20, 3);

			modelBuilder.Entity<dw_consolpr>()
				.Property(e => e.g_auto_peso_liquido)
				.HasPrecision(20, 3);

			modelBuilder.Entity<dw_consolpr>()
				.Property(e => e.g_manu_peso_bruto)
				.HasPrecision(20, 3);

			modelBuilder.Entity<dw_consolpr>()
				.Property(e => e.g_manu_peso_liquido)
				.HasPrecision(20, 3);

			modelBuilder.Entity<dw_consolpr>()
				.Property(e => e.pcs_Auto_Perdacavidades)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpr>()
				.Property(e => e.pcs_Auto_Perdaciclo)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpr>()
				.Property(e => e.pcs_Auto_Perdaparada_Sp)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpr>()
				.Property(e => e.pcs_Auto_Perdaparada_Cp)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpr>()
				.Property(e => e.pcs_auto_producaoprevista)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpr>()
				.Property(e => e.seg_auto_ritmo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolpr>()
				.Property(e => e.pcs_auto_producaoliquida)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpr>()
				.Property(e => e.pcs_manu_producaoliquida)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_consolpr>()
				.HasMany(e => e.dw_consolprmo)
				.WithRequired(e => e.dw_consolpr)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consolre>()
				.HasMany(e => e.dw_consolremo)
				.WithRequired(e => e.dw_consolre)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consolvaritmo>()
				.Property(e => e.seg_auto_temporitmo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolvaritmo>()
				.Property(e => e.seg_manu_temporitmo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_consolvaritmo>()
				.HasMany(e => e.dw_consolvaritmooco)
				.WithRequired(e => e.dw_consolvaritmo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consolvaritmolog>()
				.HasMany(e => e.dw_consolvaritmologcau)
				.WithRequired(e => e.dw_consolvaritmolog)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_consolvaritmolog>()
				.HasMany(e => e.dw_consolvaritmooco)
				.WithRequired(e => e.dw_consolvaritmolog)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_desalimpendcontag>()
				.Property(e => e.qt_desalim)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_detativ>()
				.Property(e => e.texto)
				.IsUnicode(false);

			modelBuilder.Entity<dw_est>()
				.Property(e => e.cd_est)
				.IsUnicode(false);

			modelBuilder.Entity<dw_est>()
				.Property(e => e.ds_est)
				.IsUnicode(false);

			modelBuilder.Entity<dw_est>()
				.Property(e => e.depara)
				.IsUnicode(false);

			modelBuilder.Entity<dw_est>()
				.HasMany(e => e.dw_estpro)
				.WithRequired(e => e.dw_est)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_est>()
				.HasMany(e => e.dw_rotapasso)
				.WithOptional(e => e.dw_est)
				.HasForeignKey(e => e.id_est);

			modelBuilder.Entity<dw_est>()
				.HasMany(e => e.dw_rotapasso1)
				.WithOptional(e => e.dw_est1)
				.HasForeignKey(e => e.id_estConsumir);

			modelBuilder.Entity<dw_est>()
				.HasMany(e => e.om_cfg)
				.WithOptional(e => e.dw_est)
				.HasForeignKey(e => e.id_estAlimentacao);

			modelBuilder.Entity<dw_est>()
				.HasMany(e => e.om_cfg1)
				.WithOptional(e => e.dw_est1)
				.HasForeignKey(e => e.id_estExpedicao);

			modelBuilder.Entity<dw_est>()
				.HasMany(e => e.om_cfg2)
				.WithOptional(e => e.dw_est2)
				.HasForeignKey(e => e.id_estLiberado);

			modelBuilder.Entity<dw_est>()
				.HasMany(e => e.om_cfg3)
				.WithOptional(e => e.dw_est3)
				.HasForeignKey(e => e.id_estMP);

			modelBuilder.Entity<dw_est>()
				.HasMany(e => e.om_cfg4)
				.WithOptional(e => e.dw_est4)
				.HasForeignKey(e => e.id_estProducao);

			modelBuilder.Entity<dw_est>()
				.HasMany(e => e.om_cfg5)
				.WithOptional(e => e.dw_est5)
				.HasForeignKey(e => e.id_estRefugo);

			modelBuilder.Entity<dw_estlocal>()
				.Property(e => e.cd_local)
				.IsUnicode(false);

			modelBuilder.Entity<dw_estlocal>()
				.Property(e => e.ds_local)
				.IsUnicode(false);

			modelBuilder.Entity<dw_estlocal>()
				.HasMany(e => e.om_cfg)
				.WithOptional(e => e.dw_estlocal)
				.HasForeignKey(e => e.id_estlocalorigalim);

			modelBuilder.Entity<dw_estlocalpro>()
				.Property(e => e.qt_entrada)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_estlocalpro>()
				.Property(e => e.qt_saida)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_estlocalpro>()
				.Property(e => e.qt_ajuste)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_estlocalpro>()
				.Property(e => e.qt_total)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_estlocalpro>()
				.HasMany(e => e.dw_consolestlocalpro)
				.WithRequired(e => e.dw_estlocalpro)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_estlocalpro>()
				.HasMany(e => e.dw_consolestlocalprotemp)
				.WithRequired(e => e.dw_estlocalpro)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_estmov>()
				.Property(e => e.qt_ajuste)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_estmov>()
				.Property(e => e.qt_entrada_ant)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_estmov>()
				.Property(e => e.qt_saida_ant)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_estmov>()
				.Property(e => e.qt_reservada_ant)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_estmov>()
				.Property(e => e.qt_ajuste_ant)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_estmov>()
				.Property(e => e.lancamento)
				.IsUnicode(false);

			modelBuilder.Entity<dw_estmov>()
				.Property(e => e.qt_total_ant)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_estmov>()
				.Property(e => e.qt_total)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_estpro>()
				.Property(e => e.qt_entrada)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_estpro>()
				.Property(e => e.qt_saida)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_estpro>()
				.Property(e => e.qt_reservada)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_estpro>()
				.Property(e => e.qt_ajuste)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_estpro>()
				.Property(e => e.qt_total)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_estpro>()
				.HasMany(e => e.dw_estmov)
				.WithRequired(e => e.dw_estpro)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_estsalma>()
				.Property(e => e.qt_saldofinalmes)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_expcvs>()
				.Property(e => e.cd_expcvs)
				.IsUnicode(false);

			modelBuilder.Entity<dw_expcvs>()
				.Property(e => e.ds_expcvs)
				.IsUnicode(false);

			modelBuilder.Entity<dw_expcvs>()
				.Property(e => e.sku)
				.IsUnicode(false);

			modelBuilder.Entity<dw_expcvs>()
				.Property(e => e.complemento)
				.IsUnicode(false);

			modelBuilder.Entity<dw_expcvs>()
				.Property(e => e.nserieIncial)
				.IsUnicode(false);

			modelBuilder.Entity<dw_expcvs>()
				.Property(e => e.nserieFinal)
				.IsUnicode(false);

			modelBuilder.Entity<dw_expcvs>()
				.Property(e => e.correnteMinima)
				.HasPrecision(10, 3);

			modelBuilder.Entity<dw_expcvs>()
				.Property(e => e.correnteMaxima)
				.HasPrecision(10, 3);

			modelBuilder.Entity<dw_expcvs>()
				.Property(e => e.tensaoMinima)
				.HasPrecision(10, 3);

			modelBuilder.Entity<dw_expcvs>()
				.Property(e => e.tensaoMaxima)
				.HasPrecision(10, 3);

			modelBuilder.Entity<dw_expcvs>()
				.Property(e => e.qt_totallinhas)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_expcvs>()
				.Property(e => e.qt_linhasporarquivo)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_folha>()
				.Property(e => e.cd_folha)
				.IsUnicode(false);

			modelBuilder.Entity<dw_folha>()
				.Property(e => e.ds_folha)
				.IsUnicode(false);

			modelBuilder.Entity<dw_folha>()
				.Property(e => e.seg_ciclopadrao)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_folha>()
				.Property(e => e.seg_ciclotimeout)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_folha>()
				.Property(e => e.seg_ciclominimo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_folha>()
				.Property(e => e.seg_logoutauto)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_folha>()
				.Property(e => e.seg_setup)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_folha>()
				.Property(e => e.qt_fatorcontagem)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_folha>()
				.HasMany(e => e.dw_consolid)
				.WithRequired(e => e.dw_folha)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_folha>()
				.HasMany(e => e.dw_folhamedtemp)
				.WithRequired(e => e.dw_folha)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_folha>()
				.HasMany(e => e.dw_folhaoperacao)
				.WithRequired(e => e.dw_folha)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_folha>()
				.HasMany(e => e.dw_folharap)
				.WithRequired(e => e.dw_folha)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_folha>()
				.HasMany(e => e.dw_folhasetup)
				.WithRequired(e => e.dw_folha)
				.HasForeignKey(e => e.id_folhasaindo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_folha>()
				.HasMany(e => e.dw_folhasetup1)
				.WithRequired(e => e.dw_folha1)
				.HasForeignKey(e => e.id_folhaentrando)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_folha>()
				.HasMany(e => e.dw_folhateste)
				.WithRequired(e => e.dw_folha)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_folha>()
				.HasMany(e => e.dw_folhatv)
				.WithRequired(e => e.dw_folha)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_folhacic>()
				.Property(e => e.seg_ciclopadrao)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_folhacic>()
				.Property(e => e.qt_fatorcontagem)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_folhaemb>()
				.Property(e => e.qt_na_embalagem)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_folhaiac>()
				.Property(e => e.qt_ativa)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_folhaiac>()
				.Property(e => e.qt_mpporciclo)
				.HasPrecision(14, 4);

			modelBuilder.Entity<dw_folhamedtemhor>()
				.Property(e => e.hrini)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_folhamedtemhor>()
				.Property(e => e.hriniGUI)
				.IsUnicode(false);

			modelBuilder.Entity<dw_folhamedtemhor>()
				.Property(e => e.hrfim)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_folhamedtemhor>()
				.Property(e => e.hrfimGUI)
				.IsUnicode(false);

			modelBuilder.Entity<dw_folhamedtemhor>()
				.Property(e => e.seg_intervalo_leitura)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_folhamedtemhor>()
				.HasMany(e => e.dw_folhamedtemphorcfg)
				.WithRequired(e => e.dw_folhamedtemhor)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_folhamedtemp>()
				.Property(e => e.qt_armazenamento)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_folhamedtemp>()
				.Property(e => e.seg_intervalo_leitura)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_folhamedtemp>()
				.HasMany(e => e.dw_folhamedtemhor)
				.WithRequired(e => e.dw_folhamedtemp)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_folhamedtemphorcfg>()
				.Property(e => e.ds_cfg)
				.IsUnicode(false);

			modelBuilder.Entity<dw_folhamedtemphorcfg>()
				.Property(e => e.lim_inf_temp)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_folhamedtemphorcfg>()
				.Property(e => e.lim_sup_temp)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_folhamedtemphorcfg>()
				.Property(e => e.cor_intervalo)
				.IsUnicode(false);

			modelBuilder.Entity<dw_folhamoncomp>()
				.Property(e => e.ds_mon)
				.IsUnicode(false);

			modelBuilder.Entity<dw_folharap>()
				.Property(e => e.qt_usada)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_folharap>()
				.Property(e => e.seg_tempopreparacao)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_folharap>()
				.HasMany(e => e.dw_folharapcom)
				.WithRequired(e => e.dw_folharap)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_folharapcom>()
				.Property(e => e.qt_total)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_folharapcom>()
				.Property(e => e.qt_ativa)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_folhasetup>()
				.Property(e => e.seg_setup)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_folhateste>()
				.Property(e => e.tensao_min)
				.HasPrecision(10, 3);

			modelBuilder.Entity<dw_folhateste>()
				.Property(e => e.tensao_nom)
				.HasPrecision(10, 3);

			modelBuilder.Entity<dw_folhateste>()
				.Property(e => e.tensao_max)
				.HasPrecision(10, 3);

			modelBuilder.Entity<dw_folhateste>()
				.HasMany(e => e.dw_testesub)
				.WithRequired(e => e.dw_folhateste)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_ft_etapa>()
				.Property(e => e.ds_etapa)
				.IsUnicode(false);

			modelBuilder.Entity<dw_ft_etapa>()
				.Property(e => e.ds_mensagemok)
				.IsUnicode(false);

			modelBuilder.Entity<dw_ft_etapa>()
				.Property(e => e.ds_mensagemnok)
				.IsUnicode(false);

			modelBuilder.Entity<dw_ft_etapa>()
				.Property(e => e.cd_etapa)
				.IsUnicode(false);

			modelBuilder.Entity<dw_ft_etapa>()
				.HasMany(e => e.dw_ft_sub)
				.WithRequired(e => e.dw_ft_etapa)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_ft_etapa>()
				.HasMany(e => e.dw_passtf)
				.WithRequired(e => e.dw_ft_etapa)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_ft_grupo>()
				.Property(e => e.cd_ftgrupo)
				.IsUnicode(false);

			modelBuilder.Entity<dw_ft_grupo>()
				.Property(e => e.ds_ftgrupo)
				.IsUnicode(false);

			modelBuilder.Entity<dw_ft_param>()
				.Property(e => e.ds_parametro)
				.IsUnicode(false);

			modelBuilder.Entity<dw_ft_param>()
				.Property(e => e.ds_valor1)
				.IsUnicode(false);

			modelBuilder.Entity<dw_ft_param>()
				.Property(e => e.ds_valor2)
				.IsUnicode(false);

			modelBuilder.Entity<dw_ft_param>()
				.Property(e => e.ds_valor3)
				.IsUnicode(false);

			modelBuilder.Entity<dw_ft_param>()
				.Property(e => e.ds_valor4)
				.IsUnicode(false);

			modelBuilder.Entity<dw_ft_param>()
				.HasMany(e => e.dw_consol_param)
				.WithRequired(e => e.dw_ft_param)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_ft_param>()
				.HasMany(e => e.dw_consolmedparamlog)
				.WithRequired(e => e.dw_ft_param)
				.HasForeignKey(e => e.id_ft_param)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_ft_param>()
				.HasMany(e => e.dw_consolmedparamlog1)
				.WithRequired(e => e.dw_ft_param1)
				.HasForeignKey(e => e.id_ft_param)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_ft_param>()
				.HasMany(e => e.dw_consolmedparamlog2)
				.WithRequired(e => e.dw_ft_param2)
				.HasForeignKey(e => e.id_ft_param)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_ft_param>()
				.HasMany(e => e.dw_ft_subparam)
				.WithRequired(e => e.dw_ft_param)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_ft_param>()
				.HasMany(e => e.om_cfg)
				.WithOptional(e => e.dw_ft_param)
				.HasForeignKey(e => e.id_ft_paramFP);

			modelBuilder.Entity<dw_ft_param>()
				.HasMany(e => e.om_cfg1)
				.WithOptional(e => e.dw_ft_param1)
				.HasForeignKey(e => e.id_ft_paramEC);

			modelBuilder.Entity<dw_ft_param>()
				.HasMany(e => e.om_cfg2)
				.WithOptional(e => e.dw_ft_param2)
				.HasForeignKey(e => e.id_ft_paramTemp);

			modelBuilder.Entity<dw_ft_param>()
				.HasMany(e => e.om_cfg3)
				.WithOptional(e => e.dw_ft_param3)
				.HasForeignKey(e => e.id_ft_paramFlusoS);

			modelBuilder.Entity<dw_ft_param>()
				.HasMany(e => e.om_cfg4)
				.WithOptional(e => e.dw_ft_param4)
				.HasForeignKey(e => e.id_ft_paramTensao);

			modelBuilder.Entity<dw_ft_param>()
				.HasMany(e => e.om_cfg5)
				.WithOptional(e => e.dw_ft_param5)
				.HasForeignKey(e => e.id_ft_paramCorrente);

			modelBuilder.Entity<dw_ft_param>()
				.HasMany(e => e.om_cfg6)
				.WithOptional(e => e.dw_ft_param6)
				.HasForeignKey(e => e.id_ft_paramFluxoE);

			modelBuilder.Entity<dw_ft_sub>()
				.Property(e => e.ds_sub)
				.IsUnicode(false);

			modelBuilder.Entity<dw_ft_sub>()
				.Property(e => e.y81)
				.HasPrecision(10, 3);

			modelBuilder.Entity<dw_ft_sub>()
				.Property(e => e.y82)
				.HasPrecision(10, 3);

			modelBuilder.Entity<dw_ft_sub>()
				.Property(e => e.y83)
				.HasPrecision(10, 3);

			modelBuilder.Entity<dw_ft_sub>()
				.Property(e => e.y84)
				.HasPrecision(10, 3);

			modelBuilder.Entity<dw_ft_sub>()
				.Property(e => e.vl_ft_param)
				.HasPrecision(10, 3);

			modelBuilder.Entity<dw_ft_sub>()
				.HasMany(e => e.dw_ft_subparam)
				.WithRequired(e => e.dw_ft_sub)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_ft_sub>()
				.HasMany(e => e.dw_passtfse)
				.WithRequired(e => e.dw_ft_sub)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_ft_sub>()
				.HasMany(e => e.dw_testesub)
				.WithRequired(e => e.dw_ft_sub)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_ft_subparam>()
				.HasMany(e => e.dw_testesubetapa)
				.WithRequired(e => e.dw_ft_subparam)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_grpativ>()
				.Property(e => e.cd_grpativ)
				.IsUnicode(false);

			modelBuilder.Entity<dw_grpativ>()
				.Property(e => e.ds_grpativ)
				.IsUnicode(false);

			modelBuilder.Entity<dw_grpativ>()
				.HasMany(e => e.dw_procativ)
				.WithRequired(e => e.dw_grpativ)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_grupo_ferramenta>()
				.Property(e => e.cd_grupo_ferramenta)
				.IsUnicode(false);

			modelBuilder.Entity<dw_grupo_ferramenta>()
				.Property(e => e.ds_grupo_ferramenta)
				.IsUnicode(false);

			modelBuilder.Entity<dw_grupo_ferramenta>()
				.HasMany(e => e.dw_rap_grupo)
				.WithRequired(e => e.dw_grupo_ferramenta)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_macrange>()
				.Property(e => e.cd_macinicial)
				.IsUnicode(false);

			modelBuilder.Entity<dw_macrange>()
				.Property(e => e.cd_macfinal)
				.IsUnicode(false);

			modelBuilder.Entity<dw_macrange>()
				.Property(e => e.cd_modelo)
				.IsUnicode(false);

			modelBuilder.Entity<dw_macrange>()
				.Property(e => e.cd_ultimomacusado)
				.IsUnicode(false);

			modelBuilder.Entity<dw_macrange>()
				.HasMany(e => e.dw_macrange1)
				.WithOptional(e => e.dw_macrange2)
				.HasForeignKey(e => e.id_macrangePAI);

			modelBuilder.Entity<dw_macrange>()
				.HasMany(e => e.dw_macuso)
				.WithRequired(e => e.dw_macrange)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_macuso>()
				.Property(e => e.cd_mac)
				.IsUnicode(false);

			modelBuilder.Entity<dw_nserie>()
				.Property(e => e.cb)
				.IsUnicode(false);

			modelBuilder.Entity<dw_nserie>()
				.Property(e => e.ns)
				.IsUnicode(false);

			modelBuilder.Entity<dw_nserie>()
				.HasMany(e => e.dw_nserieobs)
				.WithRequired(e => e.dw_nserie)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_nserie>()
				.HasMany(e => e.dw_passagem2)
				.WithRequired(e => e.dw_nserie2)
				.HasForeignKey(e => e.id_nserie)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_nserieobs>()
				.Property(e => e.ds_obs)
				.IsUnicode(false);

			modelBuilder.Entity<dw_operacao>()
				.Property(e => e.seg_ciclopadrao)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_operacao>()
				.Property(e => e.cd_operacao)
				.IsUnicode(false);

			modelBuilder.Entity<dw_operacao>()
				.Property(e => e.ds_operacao)
				.IsUnicode(false);

			modelBuilder.Entity<dw_operacao>()
				.Property(e => e.x)
				.HasPrecision(10, 3);

			modelBuilder.Entity<dw_operacao>()
				.Property(e => e.y)
				.HasPrecision(10, 3);

			modelBuilder.Entity<dw_operacao>()
				.HasMany(e => e.dw_folhaoperacao)
				.WithRequired(e => e.dw_operacao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_operacao>()
				.HasMany(e => e.dw_operacaopredecessora)
				.WithOptional(e => e.dw_operacao)
				.HasForeignKey(e => e.id_operacaoAnterior);

			modelBuilder.Entity<dw_operacao>()
				.HasMany(e => e.dw_operacaopredecessora1)
				.WithOptional(e => e.dw_operacao1)
				.HasForeignKey(e => e.id_operacao);

			modelBuilder.Entity<dw_operacaomidia>()
				.Property(e => e.ds_operacaomidia)
				.IsUnicode(false);

			modelBuilder.Entity<dw_passagem>()
				.Property(e => e.seg_ciclo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_passagem>()
				.Property(e => e.ds_diariobordo)
				.IsUnicode(false);

			modelBuilder.Entity<dw_passagem>()
				.HasMany(e => e.dw_nserie)
				.WithOptional(e => e.dw_passagem)
				.HasForeignKey(e => e.id_passagem);

			modelBuilder.Entity<dw_passagem>()
				.HasMany(e => e.dw_nserie1)
				.WithOptional(e => e.dw_passagem1)
				.HasForeignKey(e => e.id_passagemTF);

			modelBuilder.Entity<dw_passagem>()
				.HasMany(e => e.dw_passcau1)
				.WithRequired(e => e.dw_passagem1)
				.HasForeignKey(e => e.id_passagem)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_passagem>()
				.HasMany(e => e.dw_passdef)
				.WithRequired(e => e.dw_passagem)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_passagem>()
				.HasMany(e => e.dw_passmon)
				.WithRequired(e => e.dw_passagem)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_passagem>()
				.HasMany(e => e.dw_passtf)
				.WithRequired(e => e.dw_passagem)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_passcau>()
				.Property(e => e.ds_posicaomecanica)
				.IsUnicode(false);

			modelBuilder.Entity<dw_passcau>()
				.HasMany(e => e.dw_passagem)
				.WithOptional(e => e.dw_passcau)
				.HasForeignKey(e => e.id_passcau);

			modelBuilder.Entity<dw_passdef>()
				.Property(e => e.ds_posicaomecanica)
				.IsUnicode(false);

			modelBuilder.Entity<dw_passmon>()
				.Property(e => e.ds_mon)
				.IsUnicode(false);

			modelBuilder.Entity<dw_passmon>()
				.Property(e => e.cb)
				.IsUnicode(false);

			modelBuilder.Entity<dw_passtf>()
				.HasMany(e => e.dw_passtfse)
				.WithRequired(e => e.dw_passtf)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_passtfse>()
				.HasMany(e => e.dw_passtfsepm_hist)
				.WithRequired(e => e.dw_passtfse)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_passtfse>()
				.HasMany(e => e.dw_passtfsepm)
				.WithRequired(e => e.dw_passtfse)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_passtfsepm>()
				.Property(e => e.vlcorrente)
				.HasPrecision(10, 3);

			modelBuilder.Entity<dw_passtfsepm>()
				.Property(e => e.tensao)
				.HasPrecision(10, 3);

			modelBuilder.Entity<dw_passtfsepm>()
				.Property(e => e.st_fase)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<dw_passtfsepm_hist>()
				.Property(e => e.vlcorrente)
				.HasPrecision(10, 3);

			modelBuilder.Entity<dw_passtfsepm_hist>()
				.Property(e => e.tensao)
				.HasPrecision(10, 3);

			modelBuilder.Entity<dw_passtfsepm_hist>()
				.Property(e => e.st_fase)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<dw_pepro>()
				.Property(e => e.ds_pepro)
				.IsUnicode(false);

			modelBuilder.Entity<dw_pepro>()
				.HasMany(e => e.dw_consolid)
				.WithRequired(e => e.dw_pepro)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_pepro>()
				.HasMany(e => e.om_cfg)
				.WithRequired(e => e.dw_pepro)
				.HasForeignKey(e => e.id_peproNORMAL)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_pepro>()
				.HasMany(e => e.om_cfg1)
				.WithRequired(e => e.dw_pepro1)
				.HasForeignKey(e => e.id_peproCTREPROC)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_pepro>()
				.HasMany(e => e.om_cfg2)
				.WithOptional(e => e.dw_pepro2)
				.HasForeignKey(e => e.id_peproREGULAGEM);

			modelBuilder.Entity<dw_procativ>()
				.Property(e => e.ds_procativ)
				.IsUnicode(false);

			modelBuilder.Entity<dw_procativ>()
				.Property(e => e.seg_tempopadrao)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_procativ>()
				.HasMany(e => e.dw_detativ)
				.WithRequired(e => e.dw_procativ)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_procedimento>()
				.Property(e => e.cd_procedimento)
				.IsUnicode(false);

			modelBuilder.Entity<dw_procedimento>()
				.Property(e => e.ds_procedimento)
				.IsUnicode(false);

			modelBuilder.Entity<dw_procedimento>()
				.Property(e => e.obs)
				.IsUnicode(false);

			modelBuilder.Entity<dw_procedimento>()
				.HasMany(e => e.dw_folha)
				.WithOptional(e => e.dw_procedimento)
				.HasForeignKey(e => e.id_procedimentoMontagem);

			modelBuilder.Entity<dw_procedimento>()
				.HasMany(e => e.dw_procativ)
				.WithRequired(e => e.dw_procedimento)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_proreaativ>()
				.HasMany(e => e.dw_proreaativobs)
				.WithRequired(e => e.dw_proreaativ)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_proreaativobs>()
				.Property(e => e.ds_obs)
				.IsUnicode(false);

			modelBuilder.Entity<dw_rap>()
				.Property(e => e.cd_rap)
				.IsUnicode(false);

			modelBuilder.Entity<dw_rap>()
				.Property(e => e.ds_rap)
				.IsUnicode(false);

			modelBuilder.Entity<dw_rap>()
				.Property(e => e.qt_total)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_rap>()
				.Property(e => e.qt_alocada)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_rap>()
				.Property(e => e.depara)
				.IsUnicode(false);

			modelBuilder.Entity<dw_rap>()
				.Property(e => e.seg_tempoliberacao)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_rap>()
				.HasMany(e => e.dw_consolperdamplog)
				.WithRequired(e => e.dw_rap)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_rap>()
				.HasMany(e => e.dw_folharap)
				.WithRequired(e => e.dw_rap)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_rap>()
				.HasMany(e => e.dw_rap1)
				.WithOptional(e => e.dw_rap2)
				.HasForeignKey(e => e.id_rapTipo);

			modelBuilder.Entity<dw_rap>()
				.HasMany(e => e.dw_rap_grupo)
				.WithRequired(e => e.dw_rap)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_rap>()
				.HasMany(e => e.om_cfg)
				.WithOptional(e => e.dw_rap)
				.HasForeignKey(e => e.id_rapMagazine);

			modelBuilder.Entity<dw_rap>()
				.HasMany(e => e.om_prgpos)
				.WithOptional(e => e.dw_rap)
				.HasForeignKey(e => e.id_rapTipoMesa);

			modelBuilder.Entity<dw_rap>()
				.HasMany(e => e.om_prgpos1)
				.WithOptional(e => e.dw_rap1)
				.HasForeignKey(e => e.id_rapTipoFeeder);

			modelBuilder.Entity<dw_rota>()
				.Property(e => e.cd_rota)
				.IsUnicode(false);

			modelBuilder.Entity<dw_rota>()
				.Property(e => e.ds_rota)
				.IsUnicode(false);

			modelBuilder.Entity<dw_rota>()
				.Property(e => e.gridx)
				.HasPrecision(8, 4);

			modelBuilder.Entity<dw_rota>()
				.Property(e => e.gridy)
				.HasPrecision(8, 4);

			modelBuilder.Entity<dw_rota>()
				.Property(e => e.largura)
				.HasPrecision(8, 4);

			modelBuilder.Entity<dw_rota>()
				.Property(e => e.altura)
				.HasPrecision(8, 4);

			modelBuilder.Entity<dw_rota>()
				.Property(e => e.seg_setup)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_rota>()
				.HasMany(e => e.dw_rotapasso)
				.WithRequired(e => e.dw_rota)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_rotapasso>()
				.Property(e => e.obs)
				.IsUnicode(false);

			modelBuilder.Entity<dw_rotapasso>()
				.Property(e => e.qt_movimentacao)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_rotapasso>()
				.HasMany(e => e.dw_rotapasso1)
				.WithOptional(e => e.dw_rotapasso2)
				.HasForeignKey(e => e.id_rotapassosucessoraC);

			modelBuilder.Entity<dw_rotapasso>()
				.HasMany(e => e.dw_rotapasso11)
				.WithOptional(e => e.dw_rotapasso3)
				.HasForeignKey(e => e.id_rotapassosucessoNC);

			modelBuilder.Entity<dw_rotapasso>()
				.HasMany(e => e.dw_rotapasso_pt)
				.WithRequired(e => e.dw_rotapasso)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_rotapasso>()
				.HasMany(e => e.dw_rp_predecessora)
				.WithOptional(e => e.dw_rotapasso)
				.HasForeignKey(e => e.id_rotapassoPai);

			modelBuilder.Entity<dw_rotapasso>()
				.HasMany(e => e.dw_rp_predecessora1)
				.WithRequired(e => e.dw_rotapasso1)
				.HasForeignKey(e => e.id_rotapassoFilho)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_rt>()
				.Property(e => e.seg_paradaparcial)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_rt>()
				.Property(e => e.seg_ciclopadraominimo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_rt>()
				.Property(e => e.seg_ultimociclo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_rt>()
				.Property(e => e.ulttemperaturalida)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_rt>()
				.HasMany(e => e.dw_rtcic)
				.WithRequired(e => e.dw_rt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_rtcic>()
				.Property(e => e.seg_duracao)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_t_acao>()
				.Property(e => e.cd_tacao)
				.IsUnicode(false);

			modelBuilder.Entity<dw_t_acao>()
				.Property(e => e.ds_tacao)
				.IsUnicode(false);

			modelBuilder.Entity<dw_t_acao>()
				.HasMany(e => e.dw_passcau)
				.WithRequired(e => e.dw_t_acao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_t_alerta>()
				.Property(e => e.cd_talerta)
				.IsUnicode(false);

			modelBuilder.Entity<dw_t_alerta>()
				.Property(e => e.ds_talerta)
				.IsUnicode(false);

			modelBuilder.Entity<dw_t_alerta>()
				.HasMany(e => e.dw_consolal)
				.WithRequired(e => e.dw_t_alerta)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_t_area>()
				.Property(e => e.cd_area)
				.IsUnicode(false);

			modelBuilder.Entity<dw_t_area>()
				.Property(e => e.ds_area)
				.IsUnicode(false);

			modelBuilder.Entity<dw_t_area>()
				.Property(e => e.email)
				.IsUnicode(false);

			modelBuilder.Entity<dw_t_causa>()
				.Property(e => e.cd_tcausa)
				.IsUnicode(false);

			modelBuilder.Entity<dw_t_causa>()
				.Property(e => e.ds_tcausa)
				.IsUnicode(false);

			modelBuilder.Entity<dw_t_defeito>()
				.Property(e => e.cd_tdefeito)
				.IsUnicode(false);

			modelBuilder.Entity<dw_t_defeito>()
				.Property(e => e.ds_tdefeito)
				.IsUnicode(false);

			modelBuilder.Entity<dw_t_defeito>()
				.HasMany(e => e.dw_expcvs)
				.WithOptional(e => e.dw_t_defeito)
				.HasForeignKey(e => e.id_tdefeitoReprocesso);

			modelBuilder.Entity<dw_t_defeito>()
				.HasMany(e => e.dw_expcvs1)
				.WithOptional(e => e.dw_t_defeito1)
				.HasForeignKey(e => e.id_tdefeito);

			modelBuilder.Entity<dw_t_defeito>()
				.HasMany(e => e.dw_passcau)
				.WithRequired(e => e.dw_t_defeito)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_t_defeito>()
				.HasMany(e => e.dw_passdef)
				.WithRequired(e => e.dw_t_defeito)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_t_just>()
				.Property(e => e.cd_tjust)
				.IsUnicode(false);

			modelBuilder.Entity<dw_t_just>()
				.Property(e => e.ds_tjust)
				.IsUnicode(false);

			modelBuilder.Entity<dw_t_operacao>()
				.Property(e => e.ds_toperacao)
				.IsUnicode(false);

			modelBuilder.Entity<dw_t_operacao>()
				.HasMany(e => e.dw_operacao)
				.WithRequired(e => e.dw_t_operacao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_t_parada>()
				.Property(e => e.cd_tparada)
				.IsUnicode(false);

			modelBuilder.Entity<dw_t_parada>()
				.Property(e => e.ds_tparada)
				.IsUnicode(false);

			modelBuilder.Entity<dw_t_parada>()
				.Property(e => e.qt_tec)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_t_parada>()
				.Property(e => e.seg_extrapolacao)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_t_parada>()
				.Property(e => e.seg_timeoutalerta)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_t_parada>()
				.HasMany(e => e.dw_consolpa)
				.WithRequired(e => e.dw_t_parada)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_t_parada>()
				.HasMany(e => e.dw_consolpalog)
				.WithRequired(e => e.dw_t_parada)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_t_parada>()
				.HasMany(e => e.dw_t_parada1)
				.WithOptional(e => e.dw_t_parada2)
				.HasForeignKey(e => e.id_tparadaextra);

			modelBuilder.Entity<dw_t_parada>()
				.HasMany(e => e.om_cfg)
				.WithOptional(e => e.dw_t_parada)
				.HasForeignKey(e => e.id_tparadasemconexao);

			modelBuilder.Entity<dw_t_parada>()
				.HasMany(e => e.om_cfg1)
				.WithOptional(e => e.dw_t_parada1)
				.HasForeignKey(e => e.id_tparada);

			modelBuilder.Entity<dw_t_parada>()
				.HasMany(e => e.om_cfg2)
				.WithOptional(e => e.dw_t_parada2)
				.HasForeignKey(e => e.id_tparadacip);

			modelBuilder.Entity<dw_t_perdamp>()
				.Property(e => e.ds_perdamp)
				.IsUnicode(false);

			modelBuilder.Entity<dw_t_perdamp>()
				.HasMany(e => e.dw_consolpemp)
				.WithRequired(e => e.dw_t_perdamp)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_t_perdamp>()
				.HasMany(e => e.dw_consolperdamplog)
				.WithRequired(e => e.dw_t_perdamp)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_t_refugo>()
				.Property(e => e.cd_trefugo)
				.IsUnicode(false);

			modelBuilder.Entity<dw_t_refugo>()
				.Property(e => e.ds_trefugo)
				.IsUnicode(false);

			modelBuilder.Entity<dw_t_refugo>()
				.HasMany(e => e.dw_consolre)
				.WithRequired(e => e.dw_t_refugo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_t_ritmo>()
				.Property(e => e.cd_tritmo)
				.IsUnicode(false);

			modelBuilder.Entity<dw_t_ritmo>()
				.Property(e => e.ds_tritmo)
				.IsUnicode(false);

			modelBuilder.Entity<dw_t_ritmo>()
				.HasMany(e => e.dw_consolvaritmologcau)
				.WithRequired(e => e.dw_t_ritmo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_testesub>()
				.Property(e => e.qt_med_seg)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_testesub>()
				.Property(e => e.qt_med_seg_pos_falha)
				.HasPrecision(10, 4);

			modelBuilder.Entity<dw_testesubetapa>()
				.Property(e => e.meta)
				.HasPrecision(10, 3);

			modelBuilder.Entity<dw_testesubetapa>()
				.Property(e => e.minimo)
				.HasPrecision(10, 3);

			modelBuilder.Entity<dw_testesubetapa>()
				.Property(e => e.maximo)
				.HasPrecision(10, 3);

			modelBuilder.Entity<dw_turno>()
				.Property(e => e.cd_turno)
				.IsUnicode(false);

			modelBuilder.Entity<dw_turno>()
				.Property(e => e.ds_turno)
				.IsUnicode(false);

			modelBuilder.Entity<dw_turno>()
				.Property(e => e.cor)
				.IsUnicode(false);

			modelBuilder.Entity<dw_turno>()
				.HasMany(e => e.dw_calavu)
				.WithRequired(e => e.dw_turno)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_turno>()
				.HasMany(e => e.dw_calsem)
				.WithRequired(e => e.dw_turno)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_turno>()
				.HasMany(e => e.dw_consolestlocalpro)
				.WithRequired(e => e.dw_turno)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_turno>()
				.HasMany(e => e.dw_consolestlocalprotemp)
				.WithRequired(e => e.dw_turno)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_turno>()
				.HasMany(e => e.dw_consolid)
				.WithRequired(e => e.dw_turno)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_turno>()
				.HasMany(e => e.dw_rt)
				.WithRequired(e => e.dw_turno)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<dw_turno>()
				.HasMany(e => e.om_proturno)
				.WithRequired(e => e.dw_turno)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ip_balanceamento>()
				.Property(e => e.cd_balanceamento)
				.IsUnicode(false);

			modelBuilder.Entity<ip_balanceamento>()
				.Property(e => e.pcs_producaoPlanejadaDiaria)
				.HasPrecision(14, 4);

			modelBuilder.Entity<ip_balanceamento>()
				.Property(e => e.seg_tempoDIsponivelDiario)
				.HasPrecision(20, 10);

			modelBuilder.Entity<ip_balanceamento>()
				.Property(e => e.seg_taktTime)
				.HasPrecision(20, 10);

			modelBuilder.Entity<ip_balanceamento>()
				.Property(e => e.ind_fadiga)
				.HasPrecision(6, 3);

			modelBuilder.Entity<ip_balanceamento>()
				.Property(e => e.ds_balanceamento)
				.IsUnicode(false);

			modelBuilder.Entity<ip_balanceamento>()
				.Property(e => e.comentarioRevisao)
				.IsUnicode(false);

			modelBuilder.Entity<ms_detector>()
				.Property(e => e.cd_detector)
				.IsUnicode(false);

			modelBuilder.Entity<ms_detector>()
				.Property(e => e.ds_detector)
				.IsUnicode(false);

			modelBuilder.Entity<ms_detector>()
				.HasMany(e => e.ms_detectorusr)
				.WithRequired(e => e.ms_detector)
				.HasForeignKey(e => e.id_detector)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ms_detector>()
				.HasMany(e => e.ms_detectorusr1)
				.WithRequired(e => e.ms_detector1)
				.HasForeignKey(e => e.id_detector)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ms_detector>()
				.HasMany(e => e.ms_trigger)
				.WithRequired(e => e.ms_detector)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.temperatura)
				.HasPrecision(20, 10);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.login)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.qtde_ciclos)
				.HasPrecision(10, 4);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.cd_parada)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.cd_refugo)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.cd_consulta)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.cd_acao)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.cd_causa)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.cd_justificativa)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.cd_alerta)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.cd_tec1)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.cd_tec2)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.cd_tecresp)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.cd_produtoRedz)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.cd_produto)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.nrop)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.cd_molde)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.cd_estrutura)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.qtde)
				.HasPrecision(10, 4);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.origem)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.erroconsol)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.cd_componente)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.cd_feeder)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.cd_nozzle)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.depara)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.fatorpotencia)
				.HasPrecision(4, 3);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.energiaconsumida)
				.HasPrecision(9, 3);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.cb)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.cd_area)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.cd_defeito)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.producaoLiquida)
				.HasPrecision(14, 4);

			modelBuilder.Entity<ms_evt>()
				.Property(e => e.producaoRefugada)
				.HasPrecision(14, 4);

			modelBuilder.Entity<ms_evt>()
				.HasMany(e => e.ms_evt1)
				.WithOptional(e => e.ms_evt2)
				.HasForeignKey(e => e.id_evtInicio);

			modelBuilder.Entity<ms_evt>()
				.HasMany(e => e.ms_evtcep)
				.WithRequired(e => e.ms_evt)
				.HasForeignKey(e => e.fk)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ms_evt>()
				.HasMany(e => e.ms_evtdefeito)
				.WithRequired(e => e.ms_evt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ms_evt>()
				.HasMany(e => e.ms_evtmontagem)
				.WithRequired(e => e.ms_evt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ms_evt>()
				.HasMany(e => e.ms_up)
				.WithOptional(e => e.ms_evt)
				.HasForeignKey(e => e.id_evtInicioCiclo);

			modelBuilder.Entity<ms_evt>()
				.HasMany(e => e.ms_up1)
				.WithOptional(e => e.ms_evt1)
				.HasForeignKey(e => e.id_evtInicioParada);

			modelBuilder.Entity<ms_evtcep>()
				.Property(e => e.energiaconsumida)
				.HasPrecision(9, 3);

			modelBuilder.Entity<ms_evtcep>()
				.Property(e => e.fatorpotencia)
				.HasPrecision(4, 3);

			modelBuilder.Entity<ms_evtcep>()
				.Property(e => e.tensao)
				.HasPrecision(10, 3);

			modelBuilder.Entity<ms_evtcep>()
				.Property(e => e.tensao1)
				.HasPrecision(10, 3);

			modelBuilder.Entity<ms_evtcep>()
				.Property(e => e.tensao2)
				.HasPrecision(10, 3);

			modelBuilder.Entity<ms_evtcep>()
				.Property(e => e.tensao3)
				.HasPrecision(10, 3);

			modelBuilder.Entity<ms_evtcep>()
				.Property(e => e.corrente)
				.HasPrecision(10, 3);

			modelBuilder.Entity<ms_evtcep>()
				.Property(e => e.corrente1)
				.HasPrecision(10, 3);

			modelBuilder.Entity<ms_evtcep>()
				.Property(e => e.corrente2)
				.HasPrecision(10, 3);

			modelBuilder.Entity<ms_evtcep>()
				.Property(e => e.corrente3)
				.HasPrecision(10, 3);

			modelBuilder.Entity<ms_evtcep>()
				.Property(e => e.temperatura)
				.HasPrecision(10, 3);

			modelBuilder.Entity<ms_evtcep>()
				.Property(e => e.tensao1e2)
				.HasPrecision(10, 3);

			modelBuilder.Entity<ms_evtcep>()
				.Property(e => e.tensao1e3)
				.HasPrecision(10, 3);

			modelBuilder.Entity<ms_evtcep>()
				.Property(e => e.tensao2e3)
				.HasPrecision(10, 3);

			modelBuilder.Entity<ms_evtdefeito>()
				.Property(e => e.cd_defeito)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evtdefeito>()
				.Property(e => e.cd_acao)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evtdefeito>()
				.Property(e => e.cd_causa)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evtdefeito>()
				.Property(e => e.cd_justificativa)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evtdefeito>()
				.Property(e => e.cd_area)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evtmontagem>()
				.Property(e => e.cb)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evtmontagem>()
				.Property(e => e.dsMontagem)
				.IsUnicode(false);

			modelBuilder.Entity<ms_evtmontagem>()
				.Property(e => e.cd_produto)
				.IsUnicode(false);

			modelBuilder.Entity<ms_ic>()
				.Property(e => e.cd_ic)
				.IsUnicode(false);

			modelBuilder.Entity<ms_ic>()
				.Property(e => e.ds_ic)
				.IsUnicode(false);

			modelBuilder.Entity<ms_ic>()
				.Property(e => e.url_conexao)
				.IsUnicode(false);

			modelBuilder.Entity<ms_ic>()
				.Property(e => e.firmware)
				.IsUnicode(false);

			modelBuilder.Entity<ms_ihm>()
				.Property(e => e.url_conexao)
				.IsUnicode(false);

			modelBuilder.Entity<ms_ihm>()
				.Property(e => e.is_evtpnd)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ms_ihm>()
				.Property(e => e.is_evtciclofechado)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ms_ihm>()
				.Property(e => e.is_evttrabalhando)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ms_ihm>()
				.Property(e => e.is_evtparada)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ms_ihm>()
				.Property(e => e.is_evtrefugo)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ms_ihm>()
				.Property(e => e.is_evtalerta)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ms_ihm>()
				.Property(e => e.is_evtlogin)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ms_ihm>()
				.Property(e => e.is_upreg)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ms_ihm>()
				.Property(e => e.is_evttodos)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ms_ihm>()
				.Property(e => e.url_ip)
				.IsUnicode(false);

			modelBuilder.Entity<ms_ihm>()
				.Property(e => e.cd_ihm)
				.IsUnicode(false);

			modelBuilder.Entity<ms_ihm>()
				.Property(e => e.url_conexaoAlt)
				.IsUnicode(false);

			modelBuilder.Entity<ms_ind>()
				.Property(e => e.ds_ind)
				.IsUnicode(false);

			modelBuilder.Entity<ms_ind>()
				.Property(e => e.ds_calculo)
				.IsUnicode(false);

			modelBuilder.Entity<ms_ind>()
				.Property(e => e.ds_unidade)
				.IsUnicode(false);

			modelBuilder.Entity<ms_monitor>()
				.Property(e => e.ds_item_monitorado)
				.IsUnicode(false);

			modelBuilder.Entity<ms_ms>()
				.Property(e => e.cd_ms)
				.IsUnicode(false);

			modelBuilder.Entity<ms_ms>()
				.Property(e => e.ds_ms)
				.IsUnicode(false);

			modelBuilder.Entity<ms_ms>()
				.Property(e => e.url_conexao)
				.IsUnicode(false);

			modelBuilder.Entity<ms_ms>()
				.HasMany(e => e.ms_msihm)
				.WithRequired(e => e.ms_ms)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ms_msicup>()
				.Property(e => e.url_conexao)
				.IsUnicode(false);

			modelBuilder.Entity<ms_msicup>()
				.Property(e => e.url_auxiliar)
				.IsUnicode(false);

			modelBuilder.Entity<ms_perfilandon>()
				.Property(e => e.cd_perfilandon)
				.IsUnicode(false);

			modelBuilder.Entity<ms_perfilandon>()
				.Property(e => e.ds_perfilandon)
				.IsUnicode(false);

			modelBuilder.Entity<ms_perfilregras>()
				.Property(e => e.seg_tempoauto)
				.HasPrecision(20, 10);

			modelBuilder.Entity<ms_perfilregras>()
				.Property(e => e.seg_tempobaixa)
				.HasPrecision(20, 10);

			modelBuilder.Entity<ms_perfilregras>()
				.Property(e => e.vl_motivo)
				.IsUnicode(false);

			modelBuilder.Entity<ms_perfilregras>()
				.Property(e => e.seg_tolerancia)
				.HasPrecision(20, 10);

			modelBuilder.Entity<ms_perfilregras>()
				.Property(e => e.url_conexaoUP)
				.IsUnicode(false);

			modelBuilder.Entity<ms_pt_coleta>()
				.Property(e => e.nrop)
				.IsUnicode(false);

			modelBuilder.Entity<ms_pt_coleta>()
				.HasMany(e => e.ms_pt_coleta_login)
				.WithRequired(e => e.ms_pt_coleta)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ms_tpevt>()
				.Property(e => e.ds_tpevt)
				.IsUnicode(false);

			modelBuilder.Entity<ms_tpevt>()
				.HasMany(e => e.ms_trigger)
				.WithRequired(e => e.ms_tpevt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ms_trigger>()
				.Property(e => e.vl_ind)
				.HasPrecision(6, 3);

			modelBuilder.Entity<ms_up>()
				.Property(e => e.cd_up)
				.IsUnicode(false);

			modelBuilder.Entity<ms_up>()
				.Property(e => e.ds_up)
				.IsUnicode(false);

			modelBuilder.Entity<ms_up>()
				.Property(e => e.is_Licenciada)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ms_up>()
				.Property(e => e.iduppdba)
				.IsUnicode(false);

			modelBuilder.Entity<ms_up>()
				.Property(e => e.cd_bc)
				.IsUnicode(false);

			modelBuilder.Entity<ms_up>()
				.Property(e => e.nrop)
				.IsUnicode(false);

			modelBuilder.Entity<ms_up>()
				.HasMany(e => e.ms_upihm)
				.WithRequired(e => e.ms_up)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ms_upihm>()
				.Property(e => e.url_conexao)
				.IsUnicode(false);

			modelBuilder.Entity<ms_usr>()
				.Property(e => e.cd_usr)
				.IsUnicode(false);

			modelBuilder.Entity<ms_usr>()
				.Property(e => e.nm_usr)
				.IsUnicode(false);

			modelBuilder.Entity<ms_usr>()
				.Property(e => e.login)
				.IsUnicode(false);

			modelBuilder.Entity<ms_usr>()
				.Property(e => e.senha)
				.IsUnicode(false);

			modelBuilder.Entity<ms_usr>()
				.HasMany(e => e.ms_cfg)
				.WithRequired(e => e.ms_usr)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ms_usr>()
				.HasMany(e => e.ms_detectorusr)
				.WithRequired(e => e.ms_usr)
				.HasForeignKey(e => e.id_usr)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ms_usr>()
				.HasMany(e => e.ms_detectorusr1)
				.WithRequired(e => e.ms_usr1)
				.HasForeignKey(e => e.id_usr)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ms_usr>()
				.HasMany(e => e.ms_ic)
				.WithRequired(e => e.ms_usr)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ms_usr>()
				.HasMany(e => e.ms_ihm)
				.WithRequired(e => e.ms_usr)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ms_usr>()
				.HasMany(e => e.ms_ms)
				.WithRequired(e => e.ms_usr)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ms_usr>()
				.HasMany(e => e.ms_perfilandon)
				.WithOptional(e => e.ms_usr)
				.HasForeignKey(e => e.id_usrRevisao);

			modelBuilder.Entity<ms_usr>()
				.HasMany(e => e.ms_perfilandon1)
				.WithOptional(e => e.ms_usr1)
				.HasForeignKey(e => e.id_usrStAtivo);

			modelBuilder.Entity<om_algocor>()
				.Property(e => e.ds_algocor)
				.IsUnicode(false);

			modelBuilder.Entity<om_algocor>()
				.HasMany(e => e.om_tppt)
				.WithRequired(e => e.om_algocor)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_alim>()
				.Property(e => e.cd_alim)
				.IsUnicode(false);

			modelBuilder.Entity<om_alim>()
				.Property(e => e.ds_alim)
				.IsUnicode(false);

			modelBuilder.Entity<om_alim>()
				.HasMany(e => e.om_alimrea)
				.WithRequired(e => e.om_alim)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_alim>()
				.HasMany(e => e.om_pt)
				.WithOptional(e => e.om_alim)
				.HasForeignKey(e => e.id_alim);

			modelBuilder.Entity<om_alim>()
				.HasMany(e => e.om_pt1)
				.WithOptional(e => e.om_alim1)
				.HasForeignKey(e => e.id_alimcorrente);

			modelBuilder.Entity<om_alim>()
				.HasMany(e => e.om_pt2)
				.WithOptional(e => e.om_alim2)
				.HasForeignKey(e => e.id_alimpre);

			modelBuilder.Entity<om_alimrea>()
				.Property(e => e.cd_lido)
				.IsUnicode(false);

			modelBuilder.Entity<om_alimrea>()
				.Property(e => e.qt_alimentada)
				.HasPrecision(10, 4);

			modelBuilder.Entity<om_alimrea>()
				.Property(e => e.cb_rap)
				.IsUnicode(false);

			modelBuilder.Entity<om_cargo>()
				.Property(e => e.cd_cargo)
				.IsUnicode(false);

			modelBuilder.Entity<om_cargo>()
				.Property(e => e.ds_cargo)
				.IsUnicode(false);

			modelBuilder.Entity<om_cargo>()
				.HasMany(e => e.om_usr2)
				.WithOptional(e => e.om_cargo2)
				.HasForeignKey(e => e.id_cargo);

			modelBuilder.Entity<om_cc>()
				.Property(e => e.cd_cc)
				.IsUnicode(false);

			modelBuilder.Entity<om_cc>()
				.Property(e => e.ds_cc)
				.IsUnicode(false);

			modelBuilder.Entity<om_cc>()
				.HasMany(e => e.om_cfg)
				.WithRequired(e => e.om_cc)
				.HasForeignKey(e => e.id_ccdefault)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_cc>()
				.HasMany(e => e.om_usr2)
				.WithOptional(e => e.om_cc2)
				.HasForeignKey(e => e.id_cc);

			modelBuilder.Entity<om_cfg>()
				.Property(e => e.seg_feedbacklogin)
				.HasPrecision(20, 10);

			modelBuilder.Entity<om_cfg>()
				.Property(e => e.seg_autologout)
				.HasPrecision(20, 10);

			modelBuilder.Entity<om_cfg>()
				.Property(e => e.qt_maxptcoletafull)
				.HasPrecision(10, 4);

			modelBuilder.Entity<om_cfg>()
				.Property(e => e.qt_maxetapateste)
				.HasPrecision(10, 4);

			modelBuilder.Entity<om_cfg>()
				.Property(e => e.seg_heartbeat)
				.HasPrecision(20, 10);

			modelBuilder.Entity<om_cfg>()
				.Property(e => e.ds_mensagemSobreTensao)
				.IsUnicode(false);

			modelBuilder.Entity<om_cfg>()
				.Property(e => e.ds_mensagemSubTensao)
				.IsUnicode(false);

			modelBuilder.Entity<om_cfg>()
				.Property(e => e.qt_maxsubetapas)
				.HasPrecision(10, 4);

			modelBuilder.Entity<om_cfg>()
				.Property(e => e.mascaraCB)
				.IsUnicode(false);

			modelBuilder.Entity<om_cfg>()
				.Property(e => e.url_smshumanmobile)
				.IsUnicode(false);

			modelBuilder.Entity<om_cfg>()
				.Property(e => e.mascaraQtd)
				.IsUnicode(false);

			modelBuilder.Entity<om_cfg>()
				.Property(e => e.mascarafolha)
				.IsUnicode(false);

			modelBuilder.Entity<om_cfg>()
				.Property(e => e.mascaracdprodutocf)
				.IsUnicode(false);

			modelBuilder.Entity<om_cfg>()
				.Property(e => e.mascaracdprodutocb)
				.IsUnicode(false);

			modelBuilder.Entity<om_cfg>()
				.Property(e => e.mascaraop)
				.IsUnicode(false);

			modelBuilder.Entity<om_cfg>()
				.Property(e => e.mascaracdprodutomp)
				.IsUnicode(false);

			modelBuilder.Entity<om_cfg>()
				.HasMany(e => e.om_cfgptdetcoleta)
				.WithRequired(e => e.om_cfg)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_cfg>()
				.HasMany(e => e.om_cfgscrpimp)
				.WithRequired(e => e.om_cfg)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_cfgabc>()
				.Property(e => e.cd_cfgabc)
				.IsUnicode(false);

			modelBuilder.Entity<om_cfgabc>()
				.Property(e => e.ds_cfgabc)
				.IsUnicode(false);

			modelBuilder.Entity<om_cfgabc>()
				.Property(e => e.obs)
				.IsUnicode(false);

			modelBuilder.Entity<om_cfgabc>()
				.HasMany(e => e.om_cfgabclim)
				.WithRequired(e => e.om_cfgabc)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_cfgabclim>()
				.Property(e => e.limite_inf_classe)
				.HasPrecision(7, 3);

			modelBuilder.Entity<om_cfgabclim>()
				.Property(e => e.limite_sup_classe)
				.HasPrecision(7, 3);

			modelBuilder.Entity<om_cfgabclim>()
				.Property(e => e.limite_inf)
				.HasPrecision(7, 3);

			modelBuilder.Entity<om_cfgabclim>()
				.Property(e => e.limite_sup)
				.HasPrecision(7, 3);

			modelBuilder.Entity<om_cfgabclim>()
				.Property(e => e.limite_inf_meta)
				.HasPrecision(7, 3);

			modelBuilder.Entity<om_cfgabclim>()
				.Property(e => e.limite_sup_meta)
				.HasPrecision(7, 3);

			modelBuilder.Entity<om_cfgabclim>()
				.Property(e => e.corClasse)
				.IsUnicode(false);

			modelBuilder.Entity<om_cfgind>()
				.Property(e => e.ind_inferior)
				.HasPrecision(6, 3);

			modelBuilder.Entity<om_cfgind>()
				.Property(e => e.ind_superior)
				.HasPrecision(6, 3);

			modelBuilder.Entity<om_cfgind>()
				.Property(e => e.ind_meta)
				.HasPrecision(6, 3);

			modelBuilder.Entity<om_cfgscrpimp>()
				.Property(e => e.cd_scrp)
				.IsUnicode(false);

			modelBuilder.Entity<om_cfgscrpimp>()
				.Property(e => e.ds_scrp)
				.IsUnicode(false);

			modelBuilder.Entity<om_cfgscrpimp>()
				.Property(e => e.scriptimpressao)
				.IsUnicode(false);

			modelBuilder.Entity<om_cfgurl>()
				.Property(e => e.url_conexao)
				.IsUnicode(false);

			modelBuilder.Entity<om_cfgurl>()
				.Property(e => e.ds_url)
				.IsUnicode(false);

			modelBuilder.Entity<om_clidiario>()
				.Property(e => e.ds_diario)
				.IsUnicode(false);

			modelBuilder.Entity<om_clp>()
				.Property(e => e.cd_clp)
				.IsUnicode(false);

			modelBuilder.Entity<om_clp>()
				.Property(e => e.ds_clp)
				.IsUnicode(false);

			modelBuilder.Entity<om_clp>()
				.Property(e => e.ds_curta)
				.IsUnicode(false);

			modelBuilder.Entity<om_contato>()
				.Property(e => e.nm_contato)
				.IsUnicode(false);

			modelBuilder.Entity<om_contato>()
				.Property(e => e.obs)
				.IsUnicode(false);

			modelBuilder.Entity<om_contato>()
				.Property(e => e.telefone1)
				.IsUnicode(false);

			modelBuilder.Entity<om_contato>()
				.Property(e => e.telefone2)
				.IsUnicode(false);

			modelBuilder.Entity<om_contato>()
				.Property(e => e.telefone3)
				.IsUnicode(false);

			modelBuilder.Entity<om_contato>()
				.Property(e => e.email1)
				.IsUnicode(false);

			modelBuilder.Entity<om_contato>()
				.Property(e => e.email2)
				.IsUnicode(false);

			modelBuilder.Entity<om_contato>()
				.Property(e => e.email3)
				.IsUnicode(false);

			modelBuilder.Entity<om_empresa>()
				.Property(e => e.cd_empresa)
				.IsUnicode(false);

			modelBuilder.Entity<om_empresa>()
				.Property(e => e.ds_empresa)
				.IsUnicode(false);

			modelBuilder.Entity<om_empresa>()
				.Property(e => e.endereco)
				.IsUnicode(false);

			modelBuilder.Entity<om_empresa>()
				.Property(e => e.cidade)
				.IsUnicode(false);

			modelBuilder.Entity<om_empresa>()
				.Property(e => e.estado)
				.IsUnicode(false);

			modelBuilder.Entity<om_empresa>()
				.Property(e => e.pais)
				.IsUnicode(false);

			modelBuilder.Entity<om_empresa>()
				.Property(e => e.chave_verificacao)
				.IsUnicode(false);

			modelBuilder.Entity<om_empresa>()
				.HasMany(e => e.om_licenca)
				.WithRequired(e => e.om_empresa)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_for>()
				.Property(e => e.cd_for)
				.IsUnicode(false);

			modelBuilder.Entity<om_for>()
				.Property(e => e.nm_fornecedor)
				.IsUnicode(false);

			modelBuilder.Entity<om_garantia>()
				.Property(e => e.cd_garantia)
				.IsUnicode(false);

			modelBuilder.Entity<om_garantia>()
				.Property(e => e.ds_garantia)
				.IsUnicode(false);

			modelBuilder.Entity<om_garantia>()
				.Property(e => e.doc_cliente)
				.IsUnicode(false);

			modelBuilder.Entity<om_garantia>()
				.Property(e => e.vl_licencas)
				.IsUnicode(false);

			modelBuilder.Entity<om_garantia>()
				.Property(e => e.vl_contratomanu)
				.IsUnicode(false);

			modelBuilder.Entity<om_garantia>()
				.HasMany(e => e.om_garpro)
				.WithRequired(e => e.om_garantia)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_garpro>()
				.Property(e => e.ns)
				.IsUnicode(false);

			modelBuilder.Entity<om_garpro>()
				.Property(e => e.qt_licencasvendidas)
				.HasPrecision(10, 4);

			modelBuilder.Entity<om_garpro>()
				.Property(e => e.qt_licencascontratomanu)
				.HasPrecision(10, 4);

			modelBuilder.Entity<om_gt>()
				.Property(e => e.cd_gt)
				.IsUnicode(false);

			modelBuilder.Entity<om_gt>()
				.Property(e => e.ds_gt)
				.IsUnicode(false);

			modelBuilder.Entity<om_gt>()
				.Property(e => e.ds_curta)
				.IsUnicode(false);

			modelBuilder.Entity<om_gt>()
				.Property(e => e.largura)
				.HasPrecision(8, 4);

			modelBuilder.Entity<om_gt>()
				.Property(e => e.altura)
				.HasPrecision(8, 4);

			modelBuilder.Entity<om_gt>()
				.Property(e => e.gridx)
				.HasPrecision(8, 4);

			modelBuilder.Entity<om_gt>()
				.Property(e => e.gridy)
				.HasPrecision(8, 4);

			modelBuilder.Entity<om_gt>()
				.Property(e => e.seg_x)
				.HasPrecision(20, 10);

			modelBuilder.Entity<om_gt>()
				.Property(e => e.seg_y)
				.HasPrecision(20, 10);

			modelBuilder.Entity<om_gt>()
				.Property(e => e.seg_z)
				.HasPrecision(20, 10);

			modelBuilder.Entity<om_gt>()
				.Property(e => e.tensao_min)
				.HasPrecision(10, 3);

			modelBuilder.Entity<om_gt>()
				.Property(e => e.tensao_nom)
				.HasPrecision(10, 3);

			modelBuilder.Entity<om_gt>()
				.Property(e => e.tensao_max)
				.HasPrecision(10, 3);

			modelBuilder.Entity<om_gt>()
				.Property(e => e.depara)
				.IsUnicode(false);

			modelBuilder.Entity<om_gt>()
				.Property(e => e.ind_oee)
				.HasPrecision(6, 3);

			modelBuilder.Entity<om_gt>()
				.Property(e => e.cor)
				.IsUnicode(false);

			modelBuilder.Entity<om_gt>()
				.Property(e => e.cb)
				.IsUnicode(false);

			modelBuilder.Entity<om_gt>()
				.HasMany(e => e.dw_folha)
				.WithRequired(e => e.om_gt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_gt>()
				.HasMany(e => e.dw_folhateste)
				.WithRequired(e => e.om_gt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_gt>()
				.HasMany(e => e.dw_macrange)
				.WithOptional(e => e.om_gt)
				.WillCascadeOnDelete();

			modelBuilder.Entity<om_gt>()
				.HasMany(e => e.dw_operacao)
				.WithOptional(e => e.om_gt)
				.HasForeignKey(e => e.id_gtFase);

			modelBuilder.Entity<om_gt>()
				.HasMany(e => e.dw_rota)
				.WithRequired(e => e.om_gt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_gt>()
				.HasMany(e => e.om_cfg)
				.WithOptional(e => e.om_gt)
				.HasForeignKey(e => e.id_gtImpCic);

			modelBuilder.Entity<om_gt>()
				.HasMany(e => e.om_gt1)
				.WithOptional(e => e.om_gt2)
				.HasForeignKey(e => e.id_gtfase);

			modelBuilder.Entity<om_gt>()
				.HasMany(e => e.om_homogt)
				.WithRequired(e => e.om_gt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_gt>()
				.HasMany(e => e.om_indgt)
				.WithRequired(e => e.om_gt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_gt>()
				.HasMany(e => e.om_obj)
				.WithRequired(e => e.om_gt)
				.HasForeignKey(e => e.id_gt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_gt>()
				.HasMany(e => e.om_obj1)
				.WithOptional(e => e.om_gt1)
				.HasForeignKey(e => e.id_gtfilho);

			modelBuilder.Entity<om_gt>()
				.HasMany(e => e.om_usr2)
				.WithOptional(e => e.om_gt2)
				.HasForeignKey(e => e.id_gt);

			modelBuilder.Entity<om_im>()
				.Property(e => e.msg)
				.IsUnicode(false);

			modelBuilder.Entity<om_img>()
				.Property(e => e.cd_img)
				.IsUnicode(false);

			modelBuilder.Entity<om_img>()
				.Property(e => e.ds_img)
				.IsUnicode(false);

			modelBuilder.Entity<om_img>()
				.Property(e => e.url_img)
				.IsUnicode(false);

			modelBuilder.Entity<om_img>()
				.HasMany(e => e.om_gt)
				.WithRequired(e => e.om_img)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_ind>()
				.Property(e => e.cd_ind)
				.IsUnicode(false);

			modelBuilder.Entity<om_ind>()
				.Property(e => e.ds_ind)
				.IsUnicode(false);

			modelBuilder.Entity<om_ind>()
				.Property(e => e.ds_curta)
				.IsUnicode(false);

			modelBuilder.Entity<om_ind>()
				.HasMany(e => e.om_cfgabclim)
				.WithRequired(e => e.om_ind)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_ind>()
				.HasMany(e => e.om_indgt)
				.WithRequired(e => e.om_ind)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_ind>()
				.HasMany(e => e.om_indpt)
				.WithRequired(e => e.om_ind)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_ind>()
				.HasMany(e => e.om_indtppt)
				.WithRequired(e => e.om_ind)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_indgt>()
				.Property(e => e.num_inf)
				.HasPrecision(6, 3);

			modelBuilder.Entity<om_indgt>()
				.Property(e => e.num_superior)
				.HasPrecision(6, 3);

			modelBuilder.Entity<om_indgt>()
				.Property(e => e.num_meta)
				.HasPrecision(6, 3);

			modelBuilder.Entity<om_indpt>()
				.Property(e => e.ind_inferior)
				.HasPrecision(6, 3);

			modelBuilder.Entity<om_indpt>()
				.Property(e => e.ind_superior)
				.HasPrecision(6, 3);

			modelBuilder.Entity<om_indpt>()
				.Property(e => e.ind_meta)
				.HasPrecision(6, 3);

			modelBuilder.Entity<om_indtppt>()
				.Property(e => e.ind_inferior)
				.HasPrecision(6, 3);

			modelBuilder.Entity<om_indtppt>()
				.Property(e => e.ind_superior)
				.HasPrecision(6, 3);

			modelBuilder.Entity<om_indtppt>()
				.Property(e => e.ind_meta)
				.HasPrecision(6, 3);

			modelBuilder.Entity<om_job>()
				.Property(e => e.cd_job)
				.IsUnicode(false);

			modelBuilder.Entity<om_job>()
				.Property(e => e.ds_job)
				.IsUnicode(false);

			modelBuilder.Entity<om_job>()
				.Property(e => e.padrao_schedule)
				.IsUnicode(false);

			modelBuilder.Entity<om_job>()
				.HasMany(e => e.om_jobdet)
				.WithRequired(e => e.om_job)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_job>()
				.HasMany(e => e.om_joblog)
				.WithRequired(e => e.om_job)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_job_recurso>()
				.Property(e => e.ds_job_recurso)
				.IsUnicode(false);

			modelBuilder.Entity<om_job_recurso>()
				.HasMany(e => e.om_jobdet)
				.WithRequired(e => e.om_job_recurso)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_jobdet>()
				.Property(e => e.url_origem)
				.IsUnicode(false);

			modelBuilder.Entity<om_jobdet>()
				.HasMany(e => e.om_jobdetlog)
				.WithRequired(e => e.om_jobdet)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_jobdetlog>()
				.Property(e => e.ds_execucao)
				.IsUnicode(false);

			modelBuilder.Entity<om_jobdetlog>()
				.Property(e => e.url_origem)
				.IsUnicode(false);

			modelBuilder.Entity<om_joblog>()
				.Property(e => e.ds_execucao)
				.IsUnicode(false);

			modelBuilder.Entity<om_joblog>()
				.HasMany(e => e.om_jobdetlog)
				.WithRequired(e => e.om_joblog)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_licenca>()
				.Property(e => e.chave_verificacao)
				.IsUnicode(false);

			modelBuilder.Entity<om_licenca>()
				.Property(e => e.documento)
				.IsUnicode(false);

			modelBuilder.Entity<om_licenca>()
				.Property(e => e.chave_verificacao_empresa)
				.IsUnicode(false);

			modelBuilder.Entity<om_licenca>()
				.HasMany(e => e.om_licenca_pt)
				.WithRequired(e => e.om_licenca)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_licenca>()
				.HasMany(e => e.om_licmodrec)
				.WithRequired(e => e.om_licenca)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_licmodrec>()
				.Property(e => e.chave_verificacao_licenca)
				.IsUnicode(false);

			modelBuilder.Entity<om_licmodrec>()
				.Property(e => e.chave_verificacao)
				.IsUnicode(false);

			modelBuilder.Entity<om_mapa>()
				.Property(e => e.cd_mapa)
				.IsUnicode(false);

			modelBuilder.Entity<om_mapa>()
				.Property(e => e.ds_mapa)
				.IsUnicode(false);

			modelBuilder.Entity<om_mapa>()
				.HasMany(e => e.om_alim)
				.WithRequired(e => e.om_mapa)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_mapa>()
				.HasMany(e => e.om_mapapa)
				.WithRequired(e => e.om_mapa)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_mapapa>()
				.Property(e => e.qt_usada)
				.HasPrecision(14, 4);

			modelBuilder.Entity<om_mapapa>()
				.Property(e => e.seg_frequenciaRealimentacao)
				.HasPrecision(20, 10);

			modelBuilder.Entity<om_mapapa>()
				.HasMany(e => e.om_alimrea)
				.WithRequired(e => e.om_mapapa)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_modulo_recurso>()
				.Property(e => e.ds_modulo_recurso)
				.IsUnicode(false);

			modelBuilder.Entity<om_modulo_recurso>()
				.HasMany(e => e.om_licmodrec)
				.WithRequired(e => e.om_modulo_recurso)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_obj>()
				.Property(e => e.x)
				.HasPrecision(8, 4);

			modelBuilder.Entity<om_obj>()
				.Property(e => e.y)
				.HasPrecision(8, 4);

			modelBuilder.Entity<om_obj>()
				.Property(e => e.x2)
				.HasPrecision(8, 4);

			modelBuilder.Entity<om_obj>()
				.Property(e => e.y2)
				.HasPrecision(8, 4);

			modelBuilder.Entity<om_obj>()
				.Property(e => e.corFrente)
				.IsUnicode(false);

			modelBuilder.Entity<om_obj>()
				.Property(e => e.corFundo)
				.IsUnicode(false);

			modelBuilder.Entity<om_obj>()
				.HasMany(e => e.om_obj1)
				.WithOptional(e => e.om_obj2)
				.HasForeignKey(e => e.id_objdestino);

			modelBuilder.Entity<om_obj>()
				.HasMany(e => e.om_obj11)
				.WithOptional(e => e.om_obj3)
				.HasForeignKey(e => e.id_objorigem);

			modelBuilder.Entity<om_pa>()
				.Property(e => e.cd_pa)
				.IsUnicode(false);

			modelBuilder.Entity<om_pa>()
				.Property(e => e.ds_pa)
				.IsUnicode(false);

			modelBuilder.Entity<om_pa>()
				.Property(e => e.desvio)
				.IsUnicode(false);

			modelBuilder.Entity<om_pa>()
				.Property(e => e.depara)
				.IsUnicode(false);

			modelBuilder.Entity<om_pa>()
				.HasMany(e => e.om_mapapa)
				.WithRequired(e => e.om_pa)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_pa>()
				.HasMany(e => e.om_prgpos)
				.WithRequired(e => e.om_pa)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_papro>()
				.Property(e => e.qt_atual)
				.HasPrecision(14, 4);

			modelBuilder.Entity<om_prg>()
				.Property(e => e.cd_prg)
				.IsUnicode(false);

			modelBuilder.Entity<om_prg>()
				.Property(e => e.ds_prg)
				.IsUnicode(false);

			modelBuilder.Entity<om_prg>()
				.HasMany(e => e.om_prgpos)
				.WithRequired(e => e.om_prg)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_prgpos>()
				.Property(e => e.scantrack)
				.IsUnicode(false);

			modelBuilder.Entity<om_prgpos>()
				.Property(e => e.feedertrack)
				.IsUnicode(false);

			modelBuilder.Entity<om_prgpos>()
				.Property(e => e.pocket)
				.IsUnicode(false);

			modelBuilder.Entity<om_prgpos>()
				.Property(e => e.name)
				.IsUnicode(false);

			modelBuilder.Entity<om_prgpos>()
				.Property(e => e.feedertable)
				.IsUnicode(false);

			modelBuilder.Entity<om_prgpos>()
				.Property(e => e.posicao)
				.HasPrecision(8, 4);

			modelBuilder.Entity<om_prgpos>()
				.Property(e => e.qt_usada)
				.HasPrecision(14, 4);

			modelBuilder.Entity<om_proaltglo>()
				.Property(e => e.posicao)
				.IsUnicode(false);

			modelBuilder.Entity<om_proaltglo>()
				.Property(e => e.qt)
				.HasPrecision(14, 4);

			modelBuilder.Entity<om_procomest>()
				.Property(e => e.qt_usada)
				.HasPrecision(10, 4);

			modelBuilder.Entity<om_produto>()
				.Property(e => e.cd_produto)
				.IsUnicode(false);

			modelBuilder.Entity<om_produto>()
				.Property(e => e.ds_produto)
				.IsUnicode(false);

			modelBuilder.Entity<om_produto>()
				.Property(e => e.min_valposalim)
				.HasPrecision(8, 4);

			modelBuilder.Entity<om_produto>()
				.Property(e => e.ds_complemento)
				.IsUnicode(false);

			modelBuilder.Entity<om_produto>()
				.Property(e => e.depara)
				.IsUnicode(false);

			modelBuilder.Entity<om_produto>()
				.Property(e => e.ds_curta)
				.IsUnicode(false);

			modelBuilder.Entity<om_produto>()
				.Property(e => e.vl_custounit)
				.HasPrecision(20, 10);

			modelBuilder.Entity<om_produto>()
				.Property(e => e.qt_loteminprod)
				.HasPrecision(14, 4);

			modelBuilder.Entity<om_produto>()
				.Property(e => e.ind_perdaproducao)
				.HasPrecision(14, 4);

			modelBuilder.Entity<om_produto>()
				.Property(e => e.g_peso_bruto)
				.HasPrecision(20, 3);

			modelBuilder.Entity<om_produto>()
				.Property(e => e.g_peso_liquido)
				.HasPrecision(20, 3);

			modelBuilder.Entity<om_produto>()
				.Property(e => e.cd_sap)
				.IsUnicode(false);

			modelBuilder.Entity<om_produto>()
				.Property(e => e.cd_caixa)
				.IsUnicode(false);

			modelBuilder.Entity<om_produto>()
				.Property(e => e.cd_modelo)
				.IsUnicode(false);

			modelBuilder.Entity<om_produto>()
				.Property(e => e.cd_partnumber)
				.IsUnicode(false);

			modelBuilder.Entity<om_produto>()
				.Property(e => e.ind_defeito)
				.HasPrecision(14, 4);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.dw_consolperdamplog)
				.WithRequired(e => e.om_produto)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.dw_consolpr)
				.WithRequired(e => e.om_produto)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.dw_estpro)
				.WithRequired(e => e.om_produto)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.dw_estsalma)
				.WithRequired(e => e.om_produto)
				.HasForeignKey(e => e.id_produto)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.dw_estsalma1)
				.WithRequired(e => e.om_produto1)
				.HasForeignKey(e => e.id_produto)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.dw_folhaiac)
				.WithRequired(e => e.om_produto)
				.HasForeignKey(e => e.id_produto)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.dw_folhaiac1)
				.WithOptional(e => e.om_produto1)
				.HasForeignKey(e => e.id_produtodois);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.dw_folhamon)
				.WithRequired(e => e.om_produto)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.dw_folhamoncomp)
				.WithRequired(e => e.om_produto)
				.HasForeignKey(e => e.id_produtoFilho)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.dw_folhateste)
				.WithRequired(e => e.om_produto)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.dw_operacao)
				.WithRequired(e => e.om_produto)
				.HasForeignKey(e => e.id_produtoAcabado)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.dw_operacao1)
				.WithRequired(e => e.om_produto1)
				.HasForeignKey(e => e.id_produtoSemiacabado)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.dw_passmon)
				.WithRequired(e => e.om_produto)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.dw_produtoconjugado)
				.WithRequired(e => e.om_produto)
				.HasForeignKey(e => e.id_produtopai)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.dw_produtoconjugado1)
				.WithRequired(e => e.om_produto1)
				.HasForeignKey(e => e.id_produtofilho)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.dw_rota)
				.WithRequired(e => e.om_produto)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.ip_balanceamento)
				.WithOptional(e => e.om_produto)
				.HasForeignKey(e => e.id_produtoAcabado);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.ip_balanceamento1)
				.WithOptional(e => e.om_produto1)
				.HasForeignKey(e => e.id_produtoSemiacabado);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.om_garpro)
				.WithRequired(e => e.om_produto)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.om_mapa)
				.WithRequired(e => e.om_produto)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.om_mapapa)
				.WithRequired(e => e.om_produto)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.om_prgpos)
				.WithRequired(e => e.om_produto)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.om_proaltglo)
				.WithRequired(e => e.om_produto)
				.HasForeignKey(e => e.id_produtoMae)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.om_proaltglo1)
				.WithRequired(e => e.om_produto1)
				.HasForeignKey(e => e.id_produtoFilho)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.om_proaltglo2)
				.WithOptional(e => e.om_produto2)
				.HasForeignKey(e => e.id_produtosemiacabado);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.om_proaltglo3)
				.WithOptional(e => e.om_produto3)
				.HasForeignKey(e => e.id_produtosemiacabado);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.om_procomest)
				.WithOptional(e => e.om_produto)
				.HasForeignKey(e => e.id_produtoMP);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.om_procomest1)
				.WithRequired(e => e.om_produto1)
				.HasForeignKey(e => e.id_produto)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.om_produto1)
				.WithOptional(e => e.om_produto2)
				.HasForeignKey(e => e.id_produtoAgru);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.om_promidia)
				.WithRequired(e => e.om_produto)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.om_propaihomo)
				.WithOptional(e => e.om_produto)
				.HasForeignKey(e => e.id_produtopai);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.om_propaihomo1)
				.WithOptional(e => e.om_produto1)
				.HasForeignKey(e => e.id_produtopai);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.om_proturno)
				.WithRequired(e => e.om_produto)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.pp_cmcom)
				.WithOptional(e => e.om_produto)
				.HasForeignKey(e => e.id_produtosemiacabado);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.pp_cmcom1)
				.WithRequired(e => e.om_produto1)
				.HasForeignKey(e => e.id_produtofinal)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.pp_cmcom2)
				.WithOptional(e => e.om_produto2)
				.HasForeignKey(e => e.id_produtoSai);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.pp_cmcom3)
				.WithOptional(e => e.om_produto3)
				.HasForeignKey(e => e.id_produtoEntra);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.pp_cmcom4)
				.WithOptional(e => e.om_produto4)
				.HasForeignKey(e => e.id_produto);

			modelBuilder.Entity<om_produto>()
				.HasMany(e => e.pp_planeccron)
				.WithRequired(e => e.om_produto)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_progrp>()
				.Property(e => e.cd_progrp)
				.IsUnicode(false);

			modelBuilder.Entity<om_progrp>()
				.Property(e => e.ds_progrp)
				.IsUnicode(false);

			modelBuilder.Entity<om_promidia>()
				.Property(e => e.ds_promidia)
				.IsUnicode(false);

			modelBuilder.Entity<om_promidia>()
				.Property(e => e.extensaoArquivo)
				.IsUnicode(false);

			modelBuilder.Entity<om_pt>()
				.Property(e => e.cd_pt)
				.IsUnicode(false);

			modelBuilder.Entity<om_pt>()
				.Property(e => e.url_conexao)
				.IsUnicode(false);

			modelBuilder.Entity<om_pt>()
				.Property(e => e.ds_pt)
				.IsUnicode(false);

			modelBuilder.Entity<om_pt>()
				.Property(e => e.ds_curta)
				.IsUnicode(false);

			modelBuilder.Entity<om_pt>()
				.Property(e => e.depara)
				.IsUnicode(false);

			modelBuilder.Entity<om_pt>()
				.Property(e => e.estagio)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<om_pt>()
				.Property(e => e.ind_oee)
				.HasPrecision(6, 3);

			modelBuilder.Entity<om_pt>()
				.Property(e => e.seg_auto_tempoParadaNoCiclo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<om_pt>()
				.Property(e => e.ds_sessao)
				.IsUnicode(false);

			modelBuilder.Entity<om_pt>()
				.Property(e => e.perc_varitmo)
				.HasPrecision(7, 3);

			modelBuilder.Entity<om_pt>()
				.Property(e => e.url_impressoracb)
				.IsUnicode(false);

			modelBuilder.Entity<om_pt>()
				.Property(e => e.url_impressoradoc)
				.IsUnicode(false);

			modelBuilder.Entity<om_pt>()
				.HasMany(e => e.dw_calpt)
				.WithRequired(e => e.om_pt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_pt>()
				.HasMany(e => e.dw_consolid)
				.WithRequired(e => e.om_pt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_pt>()
				.HasMany(e => e.dw_consolpalog)
				.WithRequired(e => e.om_pt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_pt>()
				.HasMany(e => e.dw_consolpt)
				.WithRequired(e => e.om_pt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_pt>()
				.HasMany(e => e.dw_consolsplog)
				.WithRequired(e => e.om_pt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_pt>()
				.HasMany(e => e.dw_nserieobs)
				.WithRequired(e => e.om_pt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_pt>()
				.HasMany(e => e.dw_passagem)
				.WithRequired(e => e.om_pt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_pt>()
				.HasMany(e => e.dw_rotapasso_pt)
				.WithRequired(e => e.om_pt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_pt>()
				.HasMany(e => e.dw_rt)
				.WithRequired(e => e.om_pt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_pt>()
				.HasOptional(e => e.ms_pt_coleta)
				.WithRequired(e => e.om_pt);

			modelBuilder.Entity<om_pt>()
				.HasMany(e => e.om_cfgptdetcoleta)
				.WithRequired(e => e.om_pt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_pt>()
				.HasMany(e => e.om_homopt)
				.WithRequired(e => e.om_pt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_pt>()
				.HasMany(e => e.om_indpt)
				.WithRequired(e => e.om_pt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_pt>()
				.HasMany(e => e.om_licenca_pt)
				.WithRequired(e => e.om_pt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_pt>()
				.HasMany(e => e.om_mapa)
				.WithRequired(e => e.om_pt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_pt>()
				.HasMany(e => e.om_pa)
				.WithRequired(e => e.om_pt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_pt>()
				.HasMany(e => e.om_prg)
				.WithRequired(e => e.om_pt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_pt>()
				.HasMany(e => e.om_ptcnc)
				.WithOptional(e => e.om_pt)
				.HasForeignKey(e => e.id_pt);

			modelBuilder.Entity<om_pt>()
				.HasMany(e => e.om_ptcnc1)
				.WithRequired(e => e.om_pt1)
				.HasForeignKey(e => e.id_ptFilho)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_pt>()
				.HasMany(e => e.pp_cp1)
				.WithOptional(e => e.om_pt1)
				.HasForeignKey(e => e.id_pt);

			modelBuilder.Entity<om_pt>()
				.HasMany(e => e.pp_nec)
				.WithOptional(e => e.om_pt)
				.HasForeignKey(e => e.id_pt);

			modelBuilder.Entity<om_pt>()
				.HasMany(e => e.pp_nec1)
				.WithOptional(e => e.om_pt1)
				.HasForeignKey(e => e.id_pt);

			modelBuilder.Entity<om_pt>()
				.HasMany(e => e.pp_nec2)
				.WithOptional(e => e.om_pt2)
				.HasForeignKey(e => e.id_pt);

			modelBuilder.Entity<om_pt>()
				.HasMany(e => e.pp_nec3)
				.WithOptional(e => e.om_pt3)
				.HasForeignKey(e => e.id_pt);

			modelBuilder.Entity<om_regras_nscb>()
				.Property(e => e.cd_regras_nscb)
				.IsUnicode(false);

			modelBuilder.Entity<om_regras_nscb>()
				.Property(e => e.ds_regras_nscb)
				.IsUnicode(false);

			modelBuilder.Entity<om_regras_nscb>()
				.HasMany(e => e.om_cfg)
				.WithOptional(e => e.om_regras_nscb)
				.HasForeignKey(e => e.id_regras_ns);

			modelBuilder.Entity<om_regras_nscb>()
				.HasMany(e => e.om_cfg1)
				.WithOptional(e => e.om_regras_nscb1)
				.HasForeignKey(e => e.id_regras_cb);

			modelBuilder.Entity<om_regras_tags>()
				.Property(e => e.complemento)
				.IsUnicode(false);

			modelBuilder.Entity<om_resgui>()
				.Property(e => e.cd_resgui)
				.IsUnicode(false);

			modelBuilder.Entity<om_resgui>()
				.Property(e => e.ds_resgui)
				.IsUnicode(false);

			modelBuilder.Entity<om_resgui>()
				.HasMany(e => e.om_cfg)
				.WithOptional(e => e.om_resgui)
				.HasForeignKey(e => e.id_resguialimenta);

			modelBuilder.Entity<om_resgui>()
				.HasMany(e => e.om_grnts)
				.WithRequired(e => e.om_resgui)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_resgui>()
				.HasMany(e => e.om_resgui1)
				.WithOptional(e => e.om_resgui2)
				.HasForeignKey(e => e.id_resguipai);

			modelBuilder.Entity<om_tags>()
				.Property(e => e.cd_tags)
				.IsUnicode(false);

			modelBuilder.Entity<om_tags>()
				.Property(e => e.ds_tags)
				.IsUnicode(false);

			modelBuilder.Entity<om_texto>()
				.Property(e => e.ds_texto)
				.IsUnicode(false);

			modelBuilder.Entity<om_texto>()
				.Property(e => e.fonte)
				.IsUnicode(false);

			modelBuilder.Entity<om_texto>()
				.Property(e => e.cd_texto)
				.IsUnicode(false);

			modelBuilder.Entity<om_tpgt>()
				.Property(e => e.cd_tpgt)
				.IsUnicode(false);

			modelBuilder.Entity<om_tpgt>()
				.Property(e => e.ds_tpgt)
				.IsUnicode(false);

			modelBuilder.Entity<om_tpgt>()
				.HasMany(e => e.om_cfg)
				.WithOptional(e => e.om_tpgt)
				.HasForeignKey(e => e.id_tpgtFabrica);

			modelBuilder.Entity<om_tpgt>()
				.HasMany(e => e.om_cfg1)
				.WithOptional(e => e.om_tpgt1)
				.HasForeignKey(e => e.id_tpgtlogsuper);

			modelBuilder.Entity<om_tpgt>()
				.HasMany(e => e.om_gt)
				.WithRequired(e => e.om_tpgt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_tplicenca>()
				.Property(e => e.ds_licenca)
				.IsUnicode(false);

			modelBuilder.Entity<om_tplicenca>()
				.Property(e => e.chave_verificacao)
				.IsUnicode(false);

			modelBuilder.Entity<om_tplicenca>()
				.HasMany(e => e.om_licenca)
				.WithRequired(e => e.om_tplicenca)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_tppt>()
				.Property(e => e.cd_tppt)
				.IsUnicode(false);

			modelBuilder.Entity<om_tppt>()
				.Property(e => e.ds_tppt)
				.IsUnicode(false);

			modelBuilder.Entity<om_tppt>()
				.HasMany(e => e.dw_folha)
				.WithRequired(e => e.om_tppt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_tppt>()
				.HasMany(e => e.dw_t_acao)
				.WithRequired(e => e.om_tppt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_tppt>()
				.HasMany(e => e.dw_t_alerta)
				.WithRequired(e => e.om_tppt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_tppt>()
				.HasMany(e => e.dw_t_defeito)
				.WithRequired(e => e.om_tppt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_tppt>()
				.HasMany(e => e.dw_t_parada)
				.WithRequired(e => e.om_tppt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_tppt>()
				.HasMany(e => e.dw_t_refugo)
				.WithRequired(e => e.om_tppt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_tppt>()
				.HasMany(e => e.dw_t_ritmo)
				.WithRequired(e => e.om_tppt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_tppt>()
				.HasMany(e => e.om_cfg)
				.WithOptional(e => e.om_tppt)
				.HasForeignKey(e => e.id_tpptInsersora);

			modelBuilder.Entity<om_tppt>()
				.HasMany(e => e.om_cfg1)
				.WithOptional(e => e.om_tppt1)
				.HasForeignKey(e => e.id_tpptPM);

			modelBuilder.Entity<om_tppt>()
				.HasMany(e => e.om_cfg2)
				.WithOptional(e => e.om_tppt2)
				.HasForeignKey(e => e.id_tpptPTF);

			modelBuilder.Entity<om_tppt>()
				.HasMany(e => e.om_cfg3)
				.WithOptional(e => e.om_tppt3)
				.HasForeignKey(e => e.id_tpptPTS);

			modelBuilder.Entity<om_tppt>()
				.HasMany(e => e.om_cfg4)
				.WithOptional(e => e.om_tppt4)
				.HasForeignKey(e => e.id_tpptPTSCD);

			modelBuilder.Entity<om_tppt>()
				.HasMany(e => e.om_cfg5)
				.WithOptional(e => e.om_tppt5)
				.HasForeignKey(e => e.id_tpptPREPRO);

			modelBuilder.Entity<om_tppt>()
				.HasMany(e => e.om_cfg6)
				.WithOptional(e => e.om_tppt6)
				.HasForeignKey(e => e.id_tpptPPASS);

			modelBuilder.Entity<om_tppt>()
				.HasMany(e => e.om_indtppt)
				.WithRequired(e => e.om_tppt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_tppt>()
				.HasMany(e => e.om_pt)
				.WithRequired(e => e.om_tppt)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_unidmedida>()
				.Property(e => e.cd_unidmedida)
				.IsUnicode(false);

			modelBuilder.Entity<om_unidmedida>()
				.Property(e => e.ds_unidmedida)
				.IsUnicode(false);

			modelBuilder.Entity<om_usr>()
				.Property(e => e.cd_usr)
				.IsUnicode(false);

			modelBuilder.Entity<om_usr>()
				.Property(e => e.login)
				.IsUnicode(false);

			modelBuilder.Entity<om_usr>()
				.Property(e => e.senha)
				.IsUnicode(false);

			modelBuilder.Entity<om_usr>()
				.Property(e => e.ds_nome)
				.IsUnicode(false);

			modelBuilder.Entity<om_usr>()
				.Property(e => e.ds_apelido)
				.IsUnicode(false);

			modelBuilder.Entity<om_usr>()
				.Property(e => e.url_sms)
				.IsUnicode(false);

			modelBuilder.Entity<om_usr>()
				.Property(e => e.url_email)
				.IsUnicode(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_cal)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_cal1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_consolciplog)
				.WithOptional(e => e.om_usr)
				.HasForeignKey(e => e.id_usrEntrada);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_consolciplog1)
				.WithOptional(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrSaida);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_consollog)
				.WithRequired(e => e.om_usr)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_consolmo)
				.WithRequired(e => e.om_usr)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_consolmolog)
				.WithRequired(e => e.om_usr)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_est)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_est1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_estlocal)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_estlocal1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_estmov)
				.WithRequired(e => e.om_usr)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_expcvs)
				.WithOptional(e => e.om_usr)
				.HasForeignKey(e => e.id_usrSupervisor);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_expcvs1)
				.WithOptional(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrOperador);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_folha)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_folha1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_ft_etapa)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_ft_etapa1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_ft_grupo)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrStativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_ft_grupo1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrRevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_grpativ)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrRevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_grpativ1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_grupo_ferramenta)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_grupo_ferramenta1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_nserieobs)
				.WithRequired(e => e.om_usr)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_operacao)
				.WithOptional(e => e.om_usr)
				.HasForeignKey(e => e.id_usrStativo);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_operacao1)
				.WithOptional(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrRevisao);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_passagem)
				.WithOptional(e => e.om_usr)
				.HasForeignKey(e => e.id_usroperador);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_passagem1)
				.WithOptional(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrsupervisor);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_procedimento)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrRevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_procedimento1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_rap)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrRevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_rap1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrStativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_rota)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_rota1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_t_acao)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_t_acao1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_t_alerta)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_t_alerta1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_t_defeito)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_t_defeito1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_t_parada)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_t_parada1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_t_refugo)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_t_refugo1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_t_ritmo)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrStativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_t_ritmo1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_turno)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.dw_turno1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.ip_balanceamento)
				.WithOptional(e => e.om_usr)
				.HasForeignKey(e => e.id_usrAutorizou);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.ip_balanceamento1)
				.WithOptional(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrRevisao);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.ip_balanceamento2)
				.WithOptional(e => e.om_usr2)
				.HasForeignKey(e => e.id_usrstAtivo);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.ms_detector)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrRevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.ms_detector1)
				.WithOptional(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.ms_pt_coleta)
				.WithOptional(e => e.om_usr)
				.HasForeignKey(e => e.id_usrTecnicoUm);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.ms_pt_coleta1)
				.WithOptional(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrTecnicoDois);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.ms_pt_coleta2)
				.WithOptional(e => e.om_usr2)
				.HasForeignKey(e => e.id_usrTecnicoResp);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_alim)
				.WithOptional(e => e.om_usr)
				.HasForeignKey(e => e.id_usrstativo);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_alimrea)
				.WithRequired(e => e.om_usr)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_cargo)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrRevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_cargo1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_cc)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_cc1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_cfg)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrimpprog)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_cfg1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrRevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_cfg2)
				.WithRequired(e => e.om_usr2)
				.HasForeignKey(e => e.id_usrStativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_cfgabc)
				.WithOptional(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_cfgabc1)
				.WithOptional(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_clp)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_clp1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_for)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_for1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_gt)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_gt1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_homogt)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usr)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_homogt1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrhomologado)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_homopt)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usr)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_homopt1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrhomologado)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_im)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrsrc)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_im1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrtrg)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_img)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_img1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_job)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_job1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_mapa)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_mapa1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_pa)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_pa1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_produto)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_produto1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_progrp)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_progrp1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_pt)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_pt1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_regras_nscb)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_regras_nscb1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_tpgt)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_tpgt1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_tppt)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_tppt1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_unidmedida)
				.WithOptional(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_unidmedida1)
				.WithOptional(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_usr1)
				.WithOptional(e => e.om_usr2)
				.HasForeignKey(e => e.id_usrrevisao);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.om_usr11)
				.WithOptional(e => e.om_usr3)
				.HasForeignKey(e => e.id_usrstativo);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.pp_cliente)
				.WithOptional(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.pp_cliente1)
				.WithOptional(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.pp_cm)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.pp_cm1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.pp_cp)
				.WithOptional(e => e.om_usr)
				.HasForeignKey(e => e.id_usrRevisao);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.pp_cp1)
				.WithOptional(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.pp_indisp)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.pp_indisp1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.pp_nec)
				.WithOptional(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.pp_nec1)
				.WithOptional(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.pp_necimp)
				.WithRequired(e => e.om_usr)
				.HasForeignKey(e => e.id_usrRevisao)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.pp_necimp1)
				.WithRequired(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrStativo)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.pp_necimplog)
				.WithRequired(e => e.om_usr)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.pp_plano)
				.WithOptional(e => e.om_usr)
				.HasForeignKey(e => e.id_usrRevisao);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.pp_plano1)
				.WithOptional(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.te_concessionaria)
				.WithOptional(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.te_concessionaria1)
				.WithOptional(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.te_tipo_consumidor)
				.WithOptional(e => e.om_usr)
				.HasForeignKey(e => e.id_usrrevisao);

			modelBuilder.Entity<om_usr>()
				.HasMany(e => e.te_tipo_consumidor1)
				.WithOptional(e => e.om_usr1)
				.HasForeignKey(e => e.id_usrstativo);

			modelBuilder.Entity<om_usrgrp>()
				.Property(e => e.cd_usrgrp)
				.IsUnicode(false);

			modelBuilder.Entity<om_usrgrp>()
				.Property(e => e.ds_usr_grp)
				.IsUnicode(false);

			modelBuilder.Entity<om_usrgrp>()
				.HasMany(e => e.ms_detector)
				.WithRequired(e => e.om_usrgrp)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usrgrp>()
				.HasMany(e => e.om_cfg)
				.WithOptional(e => e.om_usrgrp)
				.HasForeignKey(e => e.id_usrgrpSupervisor);

			modelBuilder.Entity<om_usrgrp>()
				.HasMany(e => e.om_cfg1)
				.WithOptional(e => e.om_usrgrp1)
				.HasForeignKey(e => e.id_usrgrpOperador);

			modelBuilder.Entity<om_usrgrp>()
				.HasMany(e => e.om_grnts)
				.WithRequired(e => e.om_usrgrp)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_usrgrp>()
				.HasMany(e => e.om_usr)
				.WithRequired(e => e.om_usrgrp)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<om_webcam>()
				.Property(e => e.url)
				.IsUnicode(false);

			modelBuilder.Entity<om_webcam>()
				.Property(e => e.cd_webcam)
				.IsUnicode(false);

			modelBuilder.Entity<om_webcam>()
				.Property(e => e.ds_webcam)
				.IsUnicode(false);

			modelBuilder.Entity<om_webcam>()
				.Property(e => e.ds_curta)
				.IsUnicode(false);

			modelBuilder.Entity<pp_cliente>()
				.Property(e => e.cd_cliente)
				.IsUnicode(false);

			modelBuilder.Entity<pp_cliente>()
				.Property(e => e.nm_cliente)
				.IsUnicode(false);

			modelBuilder.Entity<pp_cliente>()
				.Property(e => e.cnpj_cpf)
				.IsUnicode(false);

			modelBuilder.Entity<pp_cliente>()
				.Property(e => e.endereco)
				.IsUnicode(false);

			modelBuilder.Entity<pp_cliente>()
				.Property(e => e.cidade)
				.IsUnicode(false);

			modelBuilder.Entity<pp_cliente>()
				.Property(e => e.estado)
				.IsUnicode(false);

			modelBuilder.Entity<pp_cliente>()
				.Property(e => e.pais)
				.IsUnicode(false);

			modelBuilder.Entity<pp_cliente>()
				.Property(e => e.telefoneUm)
				.IsUnicode(false);

			modelBuilder.Entity<pp_cliente>()
				.Property(e => e.telefoneDois)
				.IsUnicode(false);

			modelBuilder.Entity<pp_cliente>()
				.Property(e => e.telefoneTres)
				.IsUnicode(false);

			modelBuilder.Entity<pp_cliente>()
				.Property(e => e.contato)
				.IsUnicode(false);

			modelBuilder.Entity<pp_cliente>()
				.Property(e => e.url_site)
				.IsUnicode(false);

			modelBuilder.Entity<pp_cliente>()
				.Property(e => e.depara)
				.IsUnicode(false);

			modelBuilder.Entity<pp_cliente>()
				.HasMany(e => e.om_clidiario)
				.WithRequired(e => e.pp_cliente)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<pp_cliente>()
				.HasMany(e => e.om_contato)
				.WithRequired(e => e.pp_cliente)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<pp_cm>()
				.Property(e => e.cd_cm)
				.IsUnicode(false);

			modelBuilder.Entity<pp_cm>()
				.Property(e => e.ds_cm)
				.IsUnicode(false);

			modelBuilder.Entity<pp_cm>()
				.HasMany(e => e.pp_cmcom)
				.WithRequired(e => e.pp_cm)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<pp_cmcom>()
				.Property(e => e.qt_sai)
				.HasPrecision(14, 4);

			modelBuilder.Entity<pp_cmcom>()
				.Property(e => e.qt_entra)
				.HasPrecision(14, 4);

			modelBuilder.Entity<pp_cmcom>()
				.Property(e => e.obs)
				.IsUnicode(false);

			modelBuilder.Entity<pp_cmcom>()
				.Property(e => e.posicao)
				.IsUnicode(false);

			modelBuilder.Entity<pp_cp>()
				.Property(e => e.cd_cp)
				.IsUnicode(false);

			modelBuilder.Entity<pp_cp>()
				.HasMany(e => e.om_pt)
				.WithOptional(e => e.pp_cp)
				.HasForeignKey(e => e.id_cp);

			modelBuilder.Entity<pp_cp>()
				.HasMany(e => e.pp_cp_pre)
				.WithRequired(e => e.pp_cp)
				.HasForeignKey(e => e.id_cpPredecessora)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<pp_cp>()
				.HasMany(e => e.pp_cp_pre1)
				.WithRequired(e => e.pp_cp1)
				.HasForeignKey(e => e.id_cp)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<pp_cp>()
				.HasMany(e => e.pp_cpneccron)
				.WithRequired(e => e.pp_cp)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<pp_cp>()
				.HasMany(e => e.pp_cpproduto)
				.WithRequired(e => e.pp_cp)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<pp_cp_data>()
				.HasMany(e => e.pp_cp_turno)
				.WithRequired(e => e.pp_cp_data)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<pp_cp_turno>()
				.HasMany(e => e.pp_cp_hora)
				.WithRequired(e => e.pp_cp_turno)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<pp_cpfaltamp>()
				.Property(e => e.qtde)
				.HasPrecision(10, 4);

			modelBuilder.Entity<pp_cpproduto>()
				.Property(e => e.nr_doc)
				.IsUnicode(false);

			modelBuilder.Entity<pp_cpproduto>()
				.Property(e => e.pcs_producaorefugada)
				.HasPrecision(14, 4);

			modelBuilder.Entity<pp_cpproduto>()
				.HasMany(e => e.pp_cp_data)
				.WithRequired(e => e.pp_cpproduto)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<pp_indisp>()
				.Property(e => e.cd_indisp)
				.IsUnicode(false);

			modelBuilder.Entity<pp_indisp>()
				.Property(e => e.ds_indisp)
				.IsUnicode(false);

			modelBuilder.Entity<pp_indisp>()
				.HasMany(e => e.pp_indisp_rappt)
				.WithRequired(e => e.pp_indisp)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<pp_indisp_rappt>()
				.Property(e => e.ds_indisp_rappt)
				.IsUnicode(false);

			modelBuilder.Entity<pp_indisp_rappt>()
				.Property(e => e.qt_indisp)
				.HasPrecision(10, 4);

			modelBuilder.Entity<pp_nec>()
				.Property(e => e.cd_nec)
				.IsUnicode(false);

			modelBuilder.Entity<pp_nec>()
				.Property(e => e.url_origem)
				.IsUnicode(false);

			modelBuilder.Entity<pp_nec>()
				.Property(e => e.nr_doc)
				.IsUnicode(false);

			modelBuilder.Entity<pp_nec>()
				.HasMany(e => e.pp_neccron)
				.WithRequired(e => e.pp_nec)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<pp_nec>()
				.HasMany(e => e.pp_planec)
				.WithRequired(e => e.pp_nec)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<pp_neccron>()
				.HasMany(e => e.pp_planeccron)
				.WithRequired(e => e.pp_neccron)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<pp_necimp>()
				.Property(e => e.cd_necimp)
				.IsUnicode(false);

			modelBuilder.Entity<pp_necimp>()
				.Property(e => e.ds_necimp)
				.IsUnicode(false);

			modelBuilder.Entity<pp_necimp>()
				.HasMany(e => e.pp_necimplog)
				.WithRequired(e => e.pp_necimp)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<pp_necimp>()
				.HasMany(e => e.pp_necimpurl)
				.WithRequired(e => e.pp_necimp)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<pp_necimplog>()
				.HasMany(e => e.pp_necimpurllog)
				.WithRequired(e => e.pp_necimplog)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<pp_necimpurl>()
				.Property(e => e.url_fonte)
				.IsUnicode(false);

			modelBuilder.Entity<pp_necimpurl>()
				.Property(e => e.mascara)
				.IsUnicode(false);

			modelBuilder.Entity<pp_necimpurl>()
				.Property(e => e.aba)
				.IsUnicode(false);

			modelBuilder.Entity<pp_necimpurl>()
				.HasMany(e => e.pp_necimpurllog)
				.WithRequired(e => e.pp_necimpurl)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<pp_necimpurllog>()
				.Property(e => e.ds_erro)
				.IsUnicode(false);

			modelBuilder.Entity<pp_necimpurllog>()
				.Property(e => e.url_arquivo)
				.IsUnicode(false);

			modelBuilder.Entity<pp_necimpurllog>()
				.Property(e => e.aba)
				.IsUnicode(false);

			modelBuilder.Entity<pp_plano>()
				.Property(e => e.cd_plano)
				.IsUnicode(false);

			modelBuilder.Entity<pp_plano>()
				.Property(e => e.ds_plano)
				.IsUnicode(false);

			modelBuilder.Entity<pp_plano>()
				.Property(e => e.ind_oee)
				.HasPrecision(6, 3);

			modelBuilder.Entity<pp_plano>()
				.Property(e => e.is_simular)
				.HasPrecision(1, 0);

			modelBuilder.Entity<pp_plano>()
				.HasMany(e => e.pp_cp)
				.WithRequired(e => e.pp_plano)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<pp_plano>()
				.HasMany(e => e.pp_plancol)
				.WithRequired(e => e.pp_plano)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<pp_plano>()
				.HasMany(e => e.pp_planec)
				.WithRequired(e => e.pp_plano)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<pp_plano>()
				.HasMany(e => e.pp_planeccron)
				.WithRequired(e => e.pp_plano)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<pp_plano>()
				.HasMany(e => e.pp_planptgt)
				.WithRequired(e => e.pp_plano)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<pp_planptgt>()
				.Property(e => e.linha)
				.HasPrecision(8, 4);

			modelBuilder.Entity<pp_planptgt>()
				.Property(e => e.coluna)
				.HasPrecision(8, 4);

			modelBuilder.Entity<pp_planptgt>()
				.Property(e => e.aba)
				.IsUnicode(false);

			modelBuilder.Entity<pp_tpplano>()
				.Property(e => e.ds_tpplano)
				.IsUnicode(false);

			modelBuilder.Entity<pp_tpplano>()
				.Property(e => e.ind_oee)
				.HasPrecision(6, 3);

			modelBuilder.Entity<pp_tpplano>()
				.HasMany(e => e.pp_plano)
				.WithRequired(e => e.pp_tpplano)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<te_concessionaria>()
				.Property(e => e.cd_concessionaria)
				.IsUnicode(false);

			modelBuilder.Entity<te_concessionaria>()
				.Property(e => e.ds_concessionaria)
				.IsUnicode(false);

			modelBuilder.Entity<te_lei>()
				.Property(e => e.cd_lei)
				.IsUnicode(false);

			modelBuilder.Entity<te_lei>()
				.Property(e => e.ds_lei)
				.IsUnicode(false);

			modelBuilder.Entity<te_tarifasemanal>()
				.Property(e => e.hrInicio)
				.HasPrecision(20, 10);

			modelBuilder.Entity<te_tarifasemanal>()
				.Property(e => e.hrFim)
				.HasPrecision(20, 10);

			modelBuilder.Entity<te_tarifasemanal>()
				.Property(e => e.vl_tarifademanda)
				.HasPrecision(20, 4);

			modelBuilder.Entity<te_tarifasemanal>()
				.Property(e => e.vl_tarifaconsumo)
				.HasPrecision(20, 4);

			modelBuilder.Entity<te_tipo_consumidor>()
				.Property(e => e.cd_tipo_consumidor)
				.IsUnicode(false);

			modelBuilder.Entity<te_tipo_consumidor>()
				.Property(e => e.ds_tipo_consumidor)
				.IsUnicode(false);

			modelBuilder.Entity<tt_sap_con>()
				.Property(e => e.deposito)
				.IsUnicode(false);

			modelBuilder.Entity<tt_sap_con>()
				.Property(e => e.conhecimento)
				.IsUnicode(false);

			modelBuilder.Entity<tt_sap_con>()
				.Property(e => e.centro)
				.IsUnicode(false);

			modelBuilder.Entity<tt_sap_con>()
				.Property(e => e.ds_erro)
				.IsUnicode(false);

			modelBuilder.Entity<tt_sap_estmppa>()
				.Property(e => e.deposito)
				.IsUnicode(false);

			modelBuilder.Entity<tt_sap_estmppa>()
				.Property(e => e.globalcode)
				.IsUnicode(false);

			modelBuilder.Entity<tt_sap_estmppa>()
				.Property(e => e.qt_estoque)
				.HasPrecision(10, 4);

			modelBuilder.Entity<tt_sap_estmppa>()
				.Property(e => e.centro)
				.IsUnicode(false);

			modelBuilder.Entity<tt_sap_estmppa>()
				.Property(e => e.ds_erro)
				.IsUnicode(false);

			modelBuilder.Entity<tt_sap_saiexp>()
				.Property(e => e.deposito)
				.IsUnicode(false);

			modelBuilder.Entity<tt_sap_saiexp>()
				.Property(e => e.centro)
				.IsUnicode(false);

			modelBuilder.Entity<tt_sap_saiexp>()
				.Property(e => e.globalcode)
				.IsUnicode(false);

			modelBuilder.Entity<tt_sap_saiexp>()
				.Property(e => e.qt_saida)
				.HasPrecision(10, 4);

			modelBuilder.Entity<tt_sap_saiexp>()
				.Property(e => e.ds_erro)
				.IsUnicode(false);

			modelBuilder.Entity<tt_tmg_con>()
				.Property(e => e.conhecimento)
				.IsUnicode(false);

			modelBuilder.Entity<tt_tmg_con>()
				.Property(e => e.globalcode)
				.IsUnicode(false);

			modelBuilder.Entity<tt_tmg_con>()
				.Property(e => e.qt_material)
				.HasPrecision(10, 4);

			modelBuilder.Entity<tt_tmg_con>()
				.Property(e => e.ds_erro)
				.IsUnicode(false);

			modelBuilder.Entity<tt_tmg_con>()
				.Property(e => e.centro)
				.IsUnicode(false);

			modelBuilder.Entity<tmp_dw_consolpa>()
				.Property(e => e.seg_auto_tempoparada)
				.HasPrecision(20, 10);

			modelBuilder.Entity<tmp_dw_consolpa>()
				.Property(e => e.seg_manu_tempoparada)
				.HasPrecision(20, 10);

			modelBuilder.Entity<tmp_dw_consolpa>()
				.Property(e => e.qt_auto_ocorrencias)
				.HasPrecision(10, 4);

			modelBuilder.Entity<tmp_dw_consolpa>()
				.Property(e => e.qt_manu_ocorrencias)
				.HasPrecision(10, 4);

			modelBuilder.Entity<tmp_dw_consolpa>()
				.Property(e => e.seg_auto_tempoparada_ab)
				.HasPrecision(20, 10);

			modelBuilder.Entity<dw_passtfsepm_view>()
				.Property(e => e.vlcorrente)
				.HasPrecision(10, 3);

			modelBuilder.Entity<dw_passtfsepm_view>()
				.Property(e => e.tensao)
				.HasPrecision(10, 3);

			modelBuilder.Entity<dw_passtfsepm_view>()
				.Property(e => e.st_fase)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.ds_tpevt)
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.cd_up)
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.temperatura)
				.HasPrecision(20, 10);

			modelBuilder.Entity<eventos>()
				.Property(e => e.login)
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.qtde_ciclos)
				.HasPrecision(10, 4);

			modelBuilder.Entity<eventos>()
				.Property(e => e.cd_parada)
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.cd_refugo)
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.cd_consulta)
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.cd_acao)
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.cd_causa)
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.cd_justificativa)
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.cd_alerta)
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.cd_tec1)
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.cd_tec2)
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.cd_tecresp)
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.cd_produtoRedz)
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.cd_produto)
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.nrop)
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.cd_molde)
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.cd_estrutura)
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.qtde)
				.HasPrecision(10, 4);

			modelBuilder.Entity<eventos>()
				.Property(e => e.origem)
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.erroconsol)
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.cd_componente)
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.cd_feeder)
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.cd_nozzle)
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.depara)
				.IsUnicode(false);

			modelBuilder.Entity<eventos>()
				.Property(e => e.fatorpotencia)
				.HasPrecision(4, 3);

			modelBuilder.Entity<eventos>()
				.Property(e => e.energiaconsumida)
				.HasPrecision(9, 3);

			modelBuilder.Entity<eventos>()
				.Property(e => e.cb)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.cd_up)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.ds_up)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.ds_tpevt)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.temperatura)
				.HasPrecision(20, 10);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.login)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.qtde_ciclos)
				.HasPrecision(10, 4);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.cd_parada)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.cd_refugo)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.cd_consulta)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.cd_acao)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.cd_causa)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.cd_justificativa)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.cd_alerta)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.cd_tec1)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.cd_tec2)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.cd_tecresp)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.cd_produtoRedz)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.cd_produto)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.nrop)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.cd_molde)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.cd_estrutura)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.qtde)
				.HasPrecision(10, 4);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.origem)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.erroconsol)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.cd_componente)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.cd_feeder)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.cd_nozzle)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.depara)
				.IsUnicode(false);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.fatorpotencia)
				.HasPrecision(4, 3);

			modelBuilder.Entity<eventos_pm_desal>()
				.Property(e => e.energiaconsumida)
				.HasPrecision(9, 3);

			modelBuilder.Entity<v_aa>()
				.Property(e => e.cd_folha)
				.IsUnicode(false);

			modelBuilder.Entity<v_aa>()
				.Property(e => e.cd_tppt)
				.IsUnicode(false);

			modelBuilder.Entity<v_aa>()
				.Property(e => e.nova)
				.IsUnicode(false);

			modelBuilder.Entity<v_aux_temp>()
				.Property(e => e.cd_pt)
				.IsUnicode(false);

			modelBuilder.Entity<v_aux_temp>()
				.Property(e => e.cd_produto)
				.IsUnicode(false);

			modelBuilder.Entity<v_aux_temp>()
				.Property(e => e.cd_componente)
				.IsUnicode(false);

			modelBuilder.Entity<v_aux_temp>()
				.Property(e => e.qt_auto_perdamp)
				.HasPrecision(14, 4);

			modelBuilder.Entity<v_aux_temp>()
				.Property(e => e.cd_rap)
				.IsUnicode(false);

			modelBuilder.Entity<v_aux_temp>()
				.Property(e => e.total)
				.HasPrecision(14, 4);

			modelBuilder.Entity<v_aux_temp>()
				.Property(e => e.cd_feeder)
				.IsUnicode(false);

			modelBuilder.Entity<v_estoqueprocesso>()
				.Property(e => e.cd_produto)
				.IsUnicode(false);

			modelBuilder.Entity<v_estoqueprocesso>()
				.Property(e => e.qt_total)
				.HasPrecision(14, 4);

			modelBuilder.Entity<v_estoqueprocesso>()
				.Property(e => e.qt_entrada)
				.HasPrecision(14, 4);

			modelBuilder.Entity<v_estoqueprocesso>()
				.Property(e => e.qt_saida)
				.HasPrecision(14, 4);

			modelBuilder.Entity<v_estoqueprocesso>()
				.Property(e => e.qt_ajuste)
				.HasPrecision(14, 4);

			modelBuilder.Entity<v_estoqueprocesso_mov>()
				.Property(e => e.cd_produto)
				.IsUnicode(false);

			modelBuilder.Entity<v_estoqueprocesso_mov>()
				.Property(e => e.qt_mov)
				.HasPrecision(14, 4);

			modelBuilder.Entity<v_estoqueprocesso_mov>()
				.Property(e => e.qt_total_atual)
				.HasPrecision(15, 4);

			modelBuilder.Entity<v_estoqueprocesso_mov>()
				.Property(e => e.qt_total_Ant)
				.HasPrecision(14, 4);

			modelBuilder.Entity<v_estoqueprocesso_mov>()
				.Property(e => e.qt_entrada_ant)
				.HasPrecision(14, 4);

			modelBuilder.Entity<v_estoqueprocesso_mov>()
				.Property(e => e.qt_saida_ant)
				.HasPrecision(14, 4);

			modelBuilder.Entity<v_estoqueprocesso_mov>()
				.Property(e => e.qt_ajuste_ant)
				.HasPrecision(14, 4);

			modelBuilder.Entity<v_perdacomponente>()
				.Property(e => e.maquina)
				.IsUnicode(false);

			modelBuilder.Entity<v_perdacomponente>()
				.Property(e => e.componente)
				.IsUnicode(false);

			modelBuilder.Entity<v_perdacomponente>()
				.Property(e => e.qtde_perdida)
				.HasPrecision(14, 4);

			modelBuilder.Entity<v_perdacomponente>()
				.Property(e => e.custounit)
				.HasPrecision(20, 10);

			modelBuilder.Entity<v_perdacomponente_semiacabado>()
				.Property(e => e.maquina)
				.IsUnicode(false);

			modelBuilder.Entity<v_perdacomponente_semiacabado>()
				.Property(e => e.cd_produto)
				.IsUnicode(false);

			modelBuilder.Entity<v_perdacomponente_semiacabado>()
				.Property(e => e.componente)
				.IsUnicode(false);

			modelBuilder.Entity<v_perdacomponente_semiacabado>()
				.Property(e => e.qtde_perdida)
				.HasPrecision(14, 4);

			modelBuilder.Entity<v_perdacomponente_semiacabado>()
				.Property(e => e.custo)
				.HasPrecision(20, 10);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.CB)
				.IsUnicode(false);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.CD_ACAO)
				.IsUnicode(false);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.CD_ALERTA)
				.IsUnicode(false);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.CD_CAUSA)
				.IsUnicode(false);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.CD_COMPONENTE)
				.IsUnicode(false);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.CD_CONSULTA)
				.IsUnicode(false);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.CD_ESTRUTURA)
				.IsUnicode(false);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.cd_feeder)
				.IsUnicode(false);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.CD_JUSTIFICATIVA)
				.IsUnicode(false);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.CD_MOLDE)
				.IsUnicode(false);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.cd_nozzle)
				.IsUnicode(false);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.CD_PARADA)
				.IsUnicode(false);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.CD_PRODUTO)
				.IsUnicode(false);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.CD_PRODUTOREDZ)
				.IsUnicode(false);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.CD_REFUGO)
				.IsUnicode(false);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.CD_TEC1)
				.IsUnicode(false);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.CD_TEC2)
				.IsUnicode(false);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.CD_TECRESP)
				.IsUnicode(false);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.depara)
				.IsUnicode(false);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.ENERGIACONSUMIDA)
				.HasPrecision(9, 3);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.ERROCONSOL)
				.IsUnicode(false);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.FATORPOTENCIA)
				.HasPrecision(4, 3);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.LOGIN)
				.IsUnicode(false);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.NROP)
				.IsUnicode(false);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.ORIGEM)
				.IsUnicode(false);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.QTDE)
				.HasPrecision(10, 4);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.QTDE_CICLOS)
				.HasPrecision(10, 4);

			modelBuilder.Entity<VW_TESTE>()
				.Property(e => e.TEMPERATURA)
				.HasPrecision(20, 10);
			
			modelBuilder.Entity<v_paradas>()
				.Property(t => t.maquina)
				.HasMaxLength(30)
				.IsUnicode(false);

			modelBuilder.Entity<v_paradas>()
				.Property(t => t.codigo)
				.HasMaxLength(30)
				.IsUnicode(false);

			modelBuilder.Entity<v_paradas>()
				.Property(t => t.descricao)
				.HasMaxLength(100)
				.IsUnicode(false);

			modelBuilder.Entity<v_producaohora>()
				.Property(t => t.cd_pt)
				.HasMaxLength(30)
				.IsUnicode(false);

			modelBuilder.Entity<v_producaohora>()
				.Property(t => t.cd_cp)
				.HasMaxLength(30)
				.IsUnicode(false);

			modelBuilder.Entity<v_producaohora>()
				.Property(t => t.cd_folha)
				.HasMaxLength(30)
				.IsUnicode(false);

			modelBuilder.Entity<v_producaoturno>()
				.Property(t => t.cd_turno)
				.HasMaxLength(30)
				.IsUnicode(false);

			modelBuilder.Entity<v_producaoturno>()
				.Property(t => t.cd_gt)
				.HasMaxLength(30)
				.IsUnicode(false);

			modelBuilder.Entity<v_producaoturno>()
				.Property(t => t.cd_pt)
				.HasMaxLength(30)
				.IsUnicode(false);

			modelBuilder.Entity<v_producaoturno>()
				.Property(t => t.cd_cp)
				.HasMaxLength(30)
				.IsUnicode(false);

			modelBuilder.Entity<v_producaoturno>()
				.Property(t => t.cd_folha)
				.HasMaxLength(30)
				.IsUnicode(false);
		}
	}
}
