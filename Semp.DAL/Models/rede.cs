namespace SEMP.DAL.Models
{
    using System.Collections.Generic;
    
    public partial class rede
    {
        public rede()
        {
            this.pcRede = new HashSet<pcRede>();
        }
    
        public decimal id { get; set; }
        public string vendorid { get; set; }
        public string deviceid { get; set; }
        public string subsysid { get; set; }
        public string revisao { get; set; }
        public string modelo { get; set; }
        public string fornecedor { get; set; }
        public string tipo { get; set; }
    
        public virtual ICollection<pcRede> pcRede { get; set; }
    }
}
