namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_FT_TEST
    {
        [Key]
        public int COD_FT { get; set; }

        [StringLength(15)]
        public string SERIAL { get; set; }

        public bool? APROVADO { get; set; }

        public DateTime? DATA { get; set; }

        [StringLength(100)]
        public string ARQUIVO { get; set; }
    }
}
