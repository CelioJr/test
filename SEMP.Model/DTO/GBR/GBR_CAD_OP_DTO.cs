using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_CAD_OP_DTO
    {
        public int COD_OP { get; set; }
        public string OP { get; set; }
        public Nullable<int> COD_SKU { get; set; }
        public Nullable<System.DateTime> DATA_ABERTURA { get; set; }
        public Nullable<System.DateTime> DATA_FECHAMENTO { get; set; }
        public Nullable<bool> ATIVO { get; set; }
        public Nullable<bool> FORCADO { get; set; }
        public Nullable<int> NO_MAXIMO_SERIAL { get; set; }
        public Nullable<bool> SERIAL_ENGINES { get; set; }
    }
}
