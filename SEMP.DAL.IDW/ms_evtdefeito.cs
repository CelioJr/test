namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ms_evtdefeito
    {
        [Key]
        public long id_evtdefeito { get; set; }

        [StringLength(40)]
        public string cd_defeito { get; set; }

        [StringLength(40)]
        public string cd_acao { get; set; }

        [StringLength(40)]
        public string cd_causa { get; set; }

        [StringLength(40)]
        public string cd_justificativa { get; set; }

        public long id_evt { get; set; }

        [StringLength(60)]
        public string cd_area { get; set; }

        public virtual ms_evt ms_evt { get; set; }
    }
}
