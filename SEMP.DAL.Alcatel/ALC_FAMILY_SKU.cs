namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_FAMILY_SKU")]
    public partial class ALC_FAMILY_SKU
    {
        [Key]
        [StringLength(10)]
        public string FAMILY_NAME { get; set; }
    }
}
