using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_LSAAP
    {
        public string CodFab { get; set; }
        public string NumAP { get; set; }
        public System.DateTime DatEmissao { get; set; }
        public System.DateTime DatAP { get; set; }
        public System.DateTime DatReferencia { get; set; }
        public string CodProcesso { get; set; }
        public string CodModelo { get; set; }
        public string CodCinescopio { get; set; }
        public Nullable<int> QtdLoteAP { get; set; }
        public Nullable<int> QtdProduzida { get; set; }
        public string Sequencia { get; set; }
        public string CodRevisao { get; set; }
        public string CodRadial { get; set; }
        public string CodRevRadial { get; set; }
        public string CodAxial { get; set; }
        public string CodRevAxial { get; set; }
        public string CodLocal { get; set; }
        public string CodStatus { get; set; }
        public string CodLinha { get; set; }
        public string Observacao { get; set; }
        public string TipAP { get; set; }
        public string CodLocalAP { get; set; }
        public Nullable<System.DateTime> DatLiberacao { get; set; }
        public Nullable<System.DateTime> DatAceite { get; set; }
        public Nullable<System.DateTime> DatFechamento { get; set; }
        public Nullable<System.DateTime> DatProgInicio { get; set; }
        public Nullable<System.DateTime> DatProgFinal { get; set; }
        public string NomAceite { get; set; }
        public Nullable<System.DateTime> DatInicioProd { get; set; }
        public string FlgDivida { get; set; }
        public Nullable<int> QtdEmbalado { get; set; }
        public string NumSerieINI { get; set; }
        public string NumSerieFIN { get; set; }
        public string CodSO { get; set; }
        public string FlgToshiba { get; set; }
        public string FlgTipoVenda { get; set; }
        public string NumAPDestino { get; set; }
		public string NumOP { get; set; }
	}
}
