namespace SEMP.Model.DTO
{
	using System;
	using System.ComponentModel.DataAnnotations;

	public partial class tbl_retorno_DTO
	{
        public int ID { get; set; }

        [Required]
        [StringLength(16)]
        public string PACKNO { get; set; }

        public int EXTROW { get; set; }

        public DateTime DATAC { get; set; }

        public int STATUS { get; set; }

        public string MSG { get; set; }
    }
}
