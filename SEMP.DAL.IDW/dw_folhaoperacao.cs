namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_folhaoperacao
    {
        [Key]
        public long id_folhaoperacao { get; set; }

        public long id_operacao { get; set; }

        public int? ordem { get; set; }

        public long id_folha { get; set; }

        public long? id_balanceamento { get; set; }

        public virtual dw_folha dw_folha { get; set; }

        public virtual ip_balanceamento ip_balanceamento { get; set; }

        public virtual dw_operacao dw_operacao { get; set; }
    }
}
