﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.DTB_SSA.Models.Mapping
{
	public class tbl_HistoricoReimpressaoMap : EntityTypeConfiguration<tbl_HistoricoReimpressao>
	{
		public tbl_HistoricoReimpressaoMap()
		{
			// Primary Key
			this.HasKey(t => new { t.DesSerie, t.DatImpressao });

			// Properties
			this.Property(t => t.DesSerie)
				.IsRequired()
				.HasMaxLength(39);

			this.Property(t => t.TipoEtiqueta)
				.IsFixedLength()
				.HasMaxLength(3);

			// Table & Column Mappings
			this.ToTable("tbl_HistoricoReimpressao");
			this.Property(t => t.DesSerie).HasColumnName("DesSerie");
			this.Property(t => t.DatImpressao).HasColumnName("DatImpressao");
			this.Property(t => t.CodOpe).HasColumnName("CodOpe");
			this.Property(t => t.TipoEtiqueta).HasColumnName("TipoEtiqueta");
		}
	}
}
