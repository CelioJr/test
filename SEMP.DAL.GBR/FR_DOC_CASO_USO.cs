namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_DOC_CASO_USO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FR_DOC_CASO_USO()
        {
            FR_DOC_CASO_USO_EXTENSAO = new HashSet<FR_DOC_CASO_USO_EXTENSAO>();
            FR_DOC_CASO_USO_EXTENSAO1 = new HashSet<FR_DOC_CASO_USO_EXTENSAO>();
            FR_DOC_CASO_USO_GENERALIZACAO = new HashSet<FR_DOC_CASO_USO_GENERALIZACAO>();
            FR_DOC_CASO_USO_GENERALIZACAO1 = new HashSet<FR_DOC_CASO_USO_GENERALIZACAO>();
            FR_DOC_CASO_USO_INCLUSAO = new HashSet<FR_DOC_CASO_USO_INCLUSAO>();
        }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DOC_CODIGO { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int USO_CODIGO { get; set; }

        [StringLength(255)]
        public string USO_NOME { get; set; }

        public string USO_DESCRICAO { get; set; }

        public string USO_REQUISITO { get; set; }

        public string USO_VALIDACAO { get; set; }

        public string USO_CENARIO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_CASO_USO_EXTENSAO> FR_DOC_CASO_USO_EXTENSAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_CASO_USO_EXTENSAO> FR_DOC_CASO_USO_EXTENSAO1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_CASO_USO_GENERALIZACAO> FR_DOC_CASO_USO_GENERALIZACAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_CASO_USO_GENERALIZACAO> FR_DOC_CASO_USO_GENERALIZACAO1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_CASO_USO_INCLUSAO> FR_DOC_CASO_USO_INCLUSAO { get; set; }

        public virtual FR_DOC_PRINCIPAL FR_DOC_PRINCIPAL { get; set; }
    }
}
