namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class eventos_pm_desal
    {
        [StringLength(30)]
        public string cd_up { get; set; }

        [StringLength(100)]
        public string ds_up { get; set; }

        [StringLength(100)]
        public string ds_tpevt { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id_evt { get; set; }

        public long? id_evtInicio { get; set; }

        public DateTime? dthr_evento { get; set; }

        public long? id_msicup { get; set; }

        public int? st_evt { get; set; }

        public DateTime? dthr_eventoAnterior { get; set; }

        public DateTime? dthr_cadastroBanco { get; set; }

        public DateTime? dthr_cadastroServer { get; set; }

        public DateTime? dthr_envioPrColetorEventos { get; set; }

        public long? id_prcoletoreventos { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id_tpevt { get; set; }

        public long? id_upihm { get; set; }

        public int? sequencial { get; set; }

        public decimal? temperatura { get; set; }

        [StringLength(40)]
        public string login { get; set; }

        public decimal? qtde_ciclos { get; set; }

        [StringLength(40)]
        public string cd_parada { get; set; }

        [StringLength(40)]
        public string cd_refugo { get; set; }

        [StringLength(40)]
        public string cd_consulta { get; set; }

        [StringLength(40)]
        public string cd_acao { get; set; }

        [StringLength(40)]
        public string cd_causa { get; set; }

        [StringLength(40)]
        public string cd_justificativa { get; set; }

        [StringLength(40)]
        public string cd_alerta { get; set; }

        [StringLength(40)]
        public string cd_tec1 { get; set; }

        [StringLength(40)]
        public string cd_tec2 { get; set; }

        [StringLength(40)]
        public string cd_tecresp { get; set; }

        [StringLength(30)]
        public string cd_produtoRedz { get; set; }

        [StringLength(40)]
        public string cd_produto { get; set; }

        [StringLength(40)]
        public string nrop { get; set; }

        [StringLength(40)]
        public string cd_molde { get; set; }

        [StringLength(4)]
        public string cd_estrutura { get; set; }

        public decimal? qtde { get; set; }

        [StringLength(255)]
        public string origem { get; set; }

        [StringLength(512)]
        public string erroconsol { get; set; }

        [StringLength(60)]
        public string cd_componente { get; set; }

        [StringLength(20)]
        public string cd_feeder { get; set; }

        [StringLength(20)]
        public string cd_nozzle { get; set; }

        public int? tp_erromontagem { get; set; }

        public int? qt_erromontagem { get; set; }

        public DateTime? dthr_processaServer { get; set; }

        public DateTime? dthr_iprocessaServer { get; set; }

        public short? is_regulagem { get; set; }

        [StringLength(100)]
        public string depara { get; set; }

        public long? mili_duracaoevento { get; set; }

        public decimal? fatorpotencia { get; set; }

        public decimal? energiaconsumida { get; set; }
    }
}
