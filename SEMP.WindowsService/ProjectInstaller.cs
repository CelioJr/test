﻿using log4net;
using System;
using System.ComponentModel;
using System.Reflection;
using System.ServiceProcess;

namespace SEMP.WindowsService
{
	[RunInstaller(true)]
	public partial class ProjectInstaller : System.Configuration.Install.Installer
	{
		private static ILog _logger = LogManager.GetLogger(typeof(ProjectInstaller));

		/// <summary>
		/// Prefix service display name
		/// </summary>
		private const String PREFIX_DISPLAY_NAME = "SISAP Integrator: ";

		public ProjectInstaller()
		{
			InitializeComponent();
			InitializeServices();
		}

		/// <summary>
		/// Start services
		/// Look for all Services in this assembly
		/// a Service NEED to inherits ServiceBase class
		/// </summary>
		private void InitializeServices()
		{
			_logger.Info("Iniciado instalação");
			try
			{
				// Look for all Services in this assembly
				// a Service NEED to inherits ServiceBase class

				// get all types
				Type[] types = Assembly.GetExecutingAssembly().GetTypes();

				// foreach class type
				for (int i = 0; i < types.Length; i++)
				{
					Type type = types[i];
					// verify if inherits a ServiceBase class
					if (type.IsSubclassOf(typeof(ServiceBase)))
					{
						// instance the class by reflection
						ServiceBase service = (ServiceBase)Activator.CreateInstance(type);

						// starts the service installer class
						ServiceInstaller serviceInstaller = new ServiceInstaller();

						if (string.IsNullOrWhiteSpace(service.ServiceName))
						{
							service.ServiceName = service.GetType().Name;
						}

						// set it's display name                       
						serviceInstaller.DisplayName = (PREFIX_DISPLAY_NAME + service.ServiceName);

						_logger.Info("Serviço type: " + service.GetType().Name);
						_logger.Info("Serviço encontrado: " + service.ToString());
						_logger.Info("Serviço nome: " + service.ServiceName);
						_logger.Info("Nome de exibição do serviço: " + serviceInstaller.DisplayName);

						// set it's name
						serviceInstaller.ServiceName = service.ServiceName;
						// set its start type
						serviceInstaller.StartType = ServiceStartMode.Automatic;


						// add installer
						Installers.Add(serviceInstaller);
					}
				}

				_logger.Info("Instalação concluida");
			}
			catch (Exception ex)
			{
				_logger.Error(ex);
			}
		}
	}
}
