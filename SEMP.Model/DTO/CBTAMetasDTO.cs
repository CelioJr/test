using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class CBTAMetasDTO
    {
        public string CodFase { get; set; }
        public string SubFase { get; set; }
        public Nullable<decimal> Meta { get; set; }
        public string CodModelo { get; set; }
        public int Ordem { get; set; }
    }
}
