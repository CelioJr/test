﻿using System.Data.Entity.Infrastructure.Interception;
using SEMP.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.Model.DTO;

namespace SEMP.DAL.DAO.Rastreabilidade
{
    public class daoProducaoHoraPosto
    {
        public static List<ProducaoHoraPostoDTO> ListaQtdPosto(DateTime dataIni, DateTime dataFim, int linha, int horaInicio, int horaFim , int? posto)
        {

            using (var dtbCB = new DTB_CBContext())
            {
                var lista = (from ph in dtbCB.tbl_CBPassagem
                             join b in dtbCB.tbl_CBPosto on ph.NumPosto equals b.NumPosto
                             where ph.CodLinha == linha && ph.DatEvento >= dataIni && ph.DatEvento <= dataFim && b.CodLinha == ph.CodLinha && (posto == null || b.NumPosto == posto.Value)
                             select new { CodLinha  = ph.CodLinha, 
                                          NumPosto = ph.NumPosto,
                                          DatEvento = ph.DatEvento,
                                          DescricaoPosto = b.DesPosto
                             }).ToList();

                var groupbyLista = (from a in lista
                                        group a by new {
                                            NumPosto = a.NumPosto, 
                                            CodLinha = a.CodLinha, 
                                            Dia = a.DatEvento.Day,
                                            Mes = a.DatEvento.Month,
                                            Ano = a.DatEvento.Year,
                                           Hora = a.DatEvento.Hour,
                                          
                                        } into g
                                        where g.Key.Hora >= horaInicio && g.Key.Hora <= horaFim
                                        orderby g.Key.Dia, g.Key.Hora , g.Key.NumPosto
                                        select new ProducaoHoraPostoDTO
                                        {
                                           
                                            dia = g.Key.Dia,
                                            ano = g.Key.Ano,
                                            mes = g.Key.Mes,
                                            linha = g.Key.CodLinha,
                                            hora = g.Key.Hora,
                                            posto = g.Key.NumPosto,
                                           // desPosto = g.Key.DesPosto,
                                            qtdProd = g.Count(),
                                            desPosto = lista.FirstOrDefault(x => x.CodLinha == g.Key.CodLinha && x.NumPosto == g.Key.NumPosto).DescricaoPosto
                                        }
                                        ).ToList(); //.ThenBy(x => x.posto)

                return groupbyLista;


            }
        }

        public static List<ProducaoHoraPostoDTO> ListaQtdPosto(DateTime dataIni, DateTime dataFim, int linha, int posto, int horaInicio, int horaFim)
        {

            using (var dtbCB = new DTB_CBContext())
            {
                var lista = (from ph in dtbCB.tbl_CBPassagem
                             join b in dtbCB.tbl_CBPosto on ph.NumPosto equals b.NumPosto
                             where ph.CodLinha == linha && ph.DatEvento >= dataIni && ph.DatEvento <= dataFim && b.CodLinha == ph.CodLinha && ph.NumPosto == posto
                             select new 
                             {
                                 CodLinha = ph.CodLinha,
                                 NumPosto = ph.NumPosto,
                                 DatEvento = ph.DatEvento,
                                 DescricaoPosto = b.DesPosto
                             }).ToList();

                var groupbyLista = (from a in lista
                                    group a by new
                                    {
                                        NumPosto = a.NumPosto,
                                        CodLinha = a.CodLinha,
                                        Dia = a.DatEvento.Day,
                                        Mes = a.DatEvento.Month,
                                        Ano = a.DatEvento.Year,
                                        Hora = a.DatEvento.Hour,

                                    } into g
                                    where g.Key.Hora >= horaInicio && g.Key.Hora <= horaFim
                                    orderby g.Key.Dia, g.Key.Hora, g.Key.NumPosto
                                    select new ProducaoHoraPostoDTO
                                    {

                                        dia = g.Key.Dia,
                                        ano = g.Key.Ano,
                                        mes = g.Key.Mes,
                                        linha = g.Key.CodLinha,
                                        hora = g.Key.Hora,
                                        posto = g.Key.NumPosto,
                                        qtdProd = g.Count(),
                                        desPosto = lista.FirstOrDefault(x => x.CodLinha == g.Key.CodLinha && x.NumPosto == g.Key.NumPosto).DescricaoPosto
                                    }
                                        ).ToList(); //.ThenBy(x => x.posto)

                return groupbyLista;


            }
        }
    }
}