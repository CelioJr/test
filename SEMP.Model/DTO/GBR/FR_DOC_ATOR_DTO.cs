using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DOC_ATOR_DTO
    {
        public FR_DOC_ATOR_DTO()
        {
            this.FR_DOC_CASO_USO_ATOR = new List<FR_DOC_CASO_USO_ATOR_DTO>();
            this.FR_DOC_HIPOTESE_TRABALHO = new List<FR_DOC_HIPOTESE_TRABALHO_DTO>();
        }

        public int ATO_CODIGO { get; set; }
        public string ATO_FREQUENCIA_USO { get; set; }
        public string ATO_NIVEL_INSTRUCAO { get; set; }
        public string ATO_NOME { get; set; }
        public string ATO_PROFICIENCIA_APLICACAO { get; set; }
        public string ATO_PROFICIENCIA_INFORMATICA { get; set; }
        public Nullable<int> ATO_USUARIO { get; set; }
        public int DOC_CODIGO { get; set; }
        public virtual FR_DOC_PRINCIPAL_DTO FR_DOC_PRINCIPAL { get; set; }
        public virtual ICollection<FR_DOC_CASO_USO_ATOR_DTO> FR_DOC_CASO_USO_ATOR { get; set; }
        public virtual ICollection<FR_DOC_HIPOTESE_TRABALHO_DTO> FR_DOC_HIPOTESE_TRABALHO { get; set; }
    }
}
