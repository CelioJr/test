using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBLinhaObservacao
    {
        public int CodLinha { get; set; }
        public System.DateTime DatProducao { get; set; }
        public string Observacao { get; set; }
        public string NomUsuario { get; set; }
        public Nullable<System.DateTime> DatModificacao { get; set; }
    }
}
