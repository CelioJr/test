namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_pai
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_pai()
        {
            tbl_status = new HashSet<tbl_status>();
        }

        [Key]
        public int id_pai { get; set; }

        [Required]
        [StringLength(50)]
        public string CODAPP { get; set; }

        [StringLength(100)]
        public string NAME { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_status> tbl_status { get; set; }
    }
}
