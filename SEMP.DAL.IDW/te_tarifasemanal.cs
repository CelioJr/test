namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class te_tarifasemanal
    {
        [Key]
        public long id_tarifasemanal { get; set; }

        public int? diaSemanaInicio { get; set; }

        public int? diaSemanaFIM { get; set; }

        public decimal? hrInicio { get; set; }

        public decimal? hrFim { get; set; }

        public long? id_tarifas { get; set; }

        public decimal? vl_tarifademanda { get; set; }

        public decimal? vl_tarifaconsumo { get; set; }

        public virtual te_tarifas te_tarifas { get; set; }
    }
}
