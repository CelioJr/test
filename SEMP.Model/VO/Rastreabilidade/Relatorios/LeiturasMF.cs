﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
	public class LeiturasMF
	{
		[Display(Name = "Data")]
		[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy hh:mm:ss}")]
		public DateTime Data { get; set; }
		[Display(Name = "Modelo")]
		public String CodModelo { get; set; }
		[Display(Name = "Descrição")]
		public String DesModelo { get; set; }
		[Display(Name = "Série Modelo")]
		public String NumSerieModelo { get; set; }
		[Display(Name = "Acessório")]
		public String CodAcessorio { get; set; }
		[Display(Name = "Descrição")]
		public String DesAcessorio { get; set; }
		[Display(Name = "Série Acessório")]
		public String NumSerieAcessorio { get; set; }
		[Display(Name = "Painel")]
		public String CodPainel { get; set; }
		[Display(Name = "Descrição")]
		public String DesPainel { get; set; }
		[Display(Name = "Série Painel")]
		public String NumSeriePainel { get; set; }
		[Display(Name = "Outro")]
		public String CodOutro { get; set; }
		[Display(Name = "Descrição")]
		public String DesOutro { get; set; }
		[Display(Name = "Série Outro")]
		public String NumSerieOutro { get; set; }
		[Display(Name = "Operador")]
		public String CodOperador { get; set; }
		[Display(Name = "Testador")]
		public String CodTestador { get; set; }
		[Display(Name = "Linha")]
		public String CodLinha { get; set; }
		[Display(Name = "Descrição")]
		public String DesLinha { get; set; }
		[Display(Name = "Turno")]
		public int NumTurno { get; set; }
		[Display(Name = "Data Produção")]
		[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
		public DateTime? DatReferencia { get; set; }
		[Display(Name = "Lote")]
		public String NumLote { get; set; }
		[Display(Name = "Fáb.")]
		public String CodFab { get; set; }
		[Display(Name = "AP")]
		public String NumAP { get; set; }
		[Display(Name = "Dig. Verificador")]
		public string DigVerificador { get; set; }
		[Display(Name = "Data Proc. SAP")]
		public DateTime? DatProcessadoSAP { get; set; }
		[Display(Name = "Ordem de Produção")]
		public String NumOP { get; set; }
	}
}
