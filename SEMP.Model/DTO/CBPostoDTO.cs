using System;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
    [Serializable]
	public class CBPostoDTO
	{
		[Display(Name = "Linha")]
		public int CodLinha { get; set; }
		[Display(Name = "Posto")]
		public int NumPosto { get; set; }
		[Display(Name = "Tipo Posto")]
		public int CodTipoPosto { get; set; }
		[Display(Name = "Descri��o")]
		public string DesPosto { get; set; }
		public string NumIP { get; set; }
		[Display(Name = "DRT Func.")]
		public string CodDRT { get; set; }
		public string FlgTipoTerminal { get; set; }
		public string FlgAtivo { get; set; }
		public int NumSeq { get; set; }
		public bool? flgObrigatorio { get; set; }
		public bool? flgDRTObrig { get; set; }
		public string TipoAmarra { get; set; }
		public string flgPedeAP { get; set; }
		public string flgImprimeEtq { get; set; }
		public int? TempoLeitura { get; set; }
		public int? TipoEtiqueta { get; set; }
		public decimal? TempoCiclo { get; set; }
		public string FlgGargalo { get; set; }

        public string NumIPBalanca { get; set; }

		[Display(Name = "Nome")]
		public string NomFuncionario { get; set; }

        public string PostoBloqueado { get; set; }

        public int? TempoBloqueio { get; set; }

        [Display(Name = "Validar Origem do Produto")]
        public bool? flgValidaOrigem { get; set; }

        [Display(Name = "Montar Lote")]
        public bool? MontarLote { get; set; }

		public Nullable<bool> flgBaixaEstoque { get; set; }

		public string DesCor { get; set; }
		public string DesAcao { get; set; }
		public string DesTipoPosto { get; set; }
		public string DesLinha { get; set; }
		public string Setor { get; set; }

	}
}
