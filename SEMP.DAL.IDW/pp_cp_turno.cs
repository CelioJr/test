namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_cp_turno
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pp_cp_turno()
        {
            pp_cp_hora = new HashSet<pp_cp_hora>();
        }

        [Key]
        public long id_cp_turno { get; set; }

        public long? pcs_producaoPrevista { get; set; }

        public long id_cp_data { get; set; }

        public long? id_turno { get; set; }

        public long? id_calavu { get; set; }

        public virtual dw_calavu dw_calavu { get; set; }

        public virtual dw_turno dw_turno { get; set; }

        public virtual pp_cp_data pp_cp_data { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cp_hora> pp_cp_hora { get; set; }
    }
}
