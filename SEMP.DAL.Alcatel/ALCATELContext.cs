namespace SEMP.DAL.Alcatel
{
	using System;
	using System.Data.Entity;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	public partial class ALCATELContext : DbContext
	{
		public ALCATELContext()
			: base("name=ALCATELContext")
		{
		}

		public virtual DbSet<ALC_ANYTHING> ALC_ANYTHING { get; set; }
		public virtual DbSet<alc_api_role> alc_api_role { get; set; }
		public virtual DbSet<alc_api_user> alc_api_user { get; set; }
		public virtual DbSet<ALC_COVER_LED> ALC_COVER_LED { get; set; }
		public virtual DbSet<ALC_CQTF> ALC_CQTF { get; set; }
		public virtual DbSet<ALC_DAY_CONSUMPTION> ALC_DAY_CONSUMPTION { get; set; }
		public virtual DbSet<ALC_DEFECT> ALC_DEFECT { get; set; }
		public virtual DbSet<ALC_DEPARTMENT> ALC_DEPARTMENT { get; set; }
		public virtual DbSet<ALC_DOWNTIME> ALC_DOWNTIME { get; set; }
		public virtual DbSet<ALC_ENGINE> ALC_ENGINE { get; set; }
		public virtual DbSet<ALC_FAILURE> ALC_FAILURE { get; set; }
		public virtual DbSet<ALC_FAMILY_SKU> ALC_FAMILY_SKU { get; set; }
		public virtual DbSet<ALC_FULL_SKU> ALC_FULL_SKU { get; set; }
		public virtual DbSet<ALC_HDT_PROD> ALC_HDT_PROD { get; set; }
		public virtual DbSet<ALC_HOUR_PALLET> ALC_HOUR_PALLET { get; set; }
		public virtual DbSet<ALC_INTERVAL> ALC_INTERVAL { get; set; }
		public virtual DbSet<ALC_INTERVAL_PROD> ALC_INTERVAL_PROD { get; set; }
		public virtual DbSet<ALC_INVENTORY> ALC_INVENTORY { get; set; }
		public virtual DbSet<ALC_LIST_ORDER> ALC_LIST_ORDER { get; set; }
		public virtual DbSet<ALC_LOG_PRODUCT> ALC_LOG_PRODUCT { get; set; }
		public virtual DbSet<ALC_LOT_LIST> ALC_LOT_LIST { get; set; }
		public virtual DbSet<ALC_LOTE_LIST> ALC_LOTE_LIST { get; set; }
		public virtual DbSet<ALC_MAGAZINE> ALC_MAGAZINE { get; set; }
		public virtual DbSet<ALC_MONTH_CONSUMPTION> ALC_MONTH_CONSUMPTION { get; set; }
		public virtual DbSet<ALC_OQC_INSPECTION> ALC_OQC_INSPECTION { get; set; }
		public virtual DbSet<ALC_OQC_TARGET> ALC_OQC_TARGET { get; set; }
		public virtual DbSet<ALC_ORDER> ALC_ORDER { get; set; }
		public virtual DbSet<ALC_POST_USER> ALC_POST_USER { get; set; }
		public virtual DbSet<ALC_PPB> ALC_PPB { get; set; }
		public virtual DbSet<ALC_PPB_MONTH> ALC_PPB_MONTH { get; set; }
		public virtual DbSet<ALC_PPB_TARGET> ALC_PPB_TARGET { get; set; }
		public virtual DbSet<ALC_PROD_PLAN> ALC_PROD_PLAN { get; set; }
		public virtual DbSet<ALC_PRODUCTION_COUNTER> ALC_PRODUCTION_COUNTER { get; set; }
		public virtual DbSet<ALC_PROFILE> ALC_PROFILE { get; set; }
		public virtual DbSet<ALC_REPAIR> ALC_REPAIR { get; set; }
		public virtual DbSet<ALC_REPAIR_ACTION> ALC_REPAIR_ACTION { get; set; }
		public virtual DbSet<ALC_REPAIR_ORIGIN> ALC_REPAIR_ORIGIN { get; set; }
		public virtual DbSet<ALC_REPAIR_REASON> ALC_REPAIR_REASON { get; set; }
		public virtual DbSet<ALC_RESPONSIBLE> ALC_RESPONSIBLE { get; set; }
		public virtual DbSet<ALC_RMB_COST> ALC_RMB_COST { get; set; }
		public virtual DbSet<ALC_RMB_PART_NUMBER> ALC_RMB_PART_NUMBER { get; set; }
		public virtual DbSet<ALC_RMB_REGISTER> ALC_RMB_REGISTER { get; set; }
		public virtual DbSet<ALC_ROLE_PROF> ALC_ROLE_PROF { get; set; }
		public virtual DbSet<ALC_ROLE_USER> ALC_ROLE_USER { get; set; }
		public virtual DbSet<ALC_SGQ_DOCUMENT> ALC_SGQ_DOCUMENT { get; set; }
		public virtual DbSet<ALC_SGQ_PROCESS> ALC_SGQ_PROCESS { get; set; }
		public virtual DbSet<ALC_SHIFT> ALC_SHIFT { get; set; }
		public virtual DbSet<ALC_SHIFT_PROD> ALC_SHIFT_PROD { get; set; }
		public virtual DbSet<ALC_SYS_FLAG> ALC_SYS_FLAG { get; set; }
		public virtual DbSet<ALC_SYS_LOG> ALC_SYS_LOG { get; set; }
		public virtual DbSet<ALC_SYS_USER> ALC_SYS_USER { get; set; }
		public virtual DbSet<ALC_TARGET_HOUR> ALC_TARGET_HOUR { get; set; }
		public virtual DbSet<alc_user_role> alc_user_role { get; set; }
		public virtual DbSet<ALC_VOLUME> ALC_VOLUME { get; set; }
		public virtual DbSet<hdtconfig> hdtconfig { get; set; }
		public virtual DbSet<hdtmodel> hdtmodel { get; set; }
		public virtual DbSet<hdtmodelrange> hdtmodelrange { get; set; }
		public virtual DbSet<hdtupdatelog> hdtupdatelog { get; set; }
		public virtual DbSet<PACKING_SERIAL_VIEW> PACKING_SERIAL_VIEW { get; set; }
		public virtual DbSet<TEST_ALCATEL> TEST_ALCATEL { get; set; }
		public virtual DbSet<TEST_TABLEs> TEST_TABLEs { get; set; }
		public virtual DbSet<usertable> usertable { get; set; }
		public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
		public virtual DbSet<ALC_HOUR_SHIFT> ALC_HOUR_SHIFT { get; set; }
		public virtual DbSet<ALC_LOT_LIST_BACK> ALC_LOT_LIST_BACK { get; set; }
		public virtual DbSet<ALC_REPAIR_BACK> ALC_REPAIR_BACK { get; set; }
		public virtual DbSet<LOT_VIEW> LOT_VIEW { get; set; }
		public virtual DbSet<USEFUL_MODEL> USEFUL_MODEL { get; set; }
		public virtual DbSet<tbl_ProdDia> tbl_ProdDia { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<ALC_ANYTHING>()
				.Property(e => e.description)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_ANYTHING>()
				.Property(e => e.key_value)
				.IsUnicode(false);

			modelBuilder.Entity<alc_api_role>()
				.Property(e => e.description)
				.IsUnicode(false);

			modelBuilder.Entity<alc_api_user>()
				.Property(e => e.password)
				.IsUnicode(false);

			modelBuilder.Entity<alc_api_user>()
				.Property(e => e.username)
				.IsUnicode(false);

			modelBuilder.Entity<alc_api_user>()
				.HasMany(e => e.alc_api_role)
				.WithMany(e => e.alc_api_user)
				.Map(m => m.ToTable("alc_api_user_roles").MapLeftKey("user_id").MapRightKey("roles_id"));

			modelBuilder.Entity<alc_api_user>()
				.HasMany(e => e.alc_api_role1)
				.WithMany(e => e.alc_api_user1)
				.Map(m => m.ToTable("alc_api_user_roles1").MapLeftKey("user_id").MapRightKey("roles_id"));

			modelBuilder.Entity<ALC_COVER_LED>()
				.Property(e => e.ID_COVER)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_COVER_LED>()
				.Property(e => e.OP)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_CQTF>()
				.Property(e => e.ID_CQTF)
				.HasPrecision(19, 0);

			modelBuilder.Entity<ALC_CQTF>()
				.Property(e => e.cqtfControl)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_CQTF>()
				.Property(e => e.remessa)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_DAY_CONSUMPTION>()
				.Property(e => e.id_consumption)
				.HasPrecision(19, 0);

			modelBuilder.Entity<ALC_DAY_CONSUMPTION>()
				.Property(e => e.energy)
				.HasPrecision(19, 0);

			modelBuilder.Entity<ALC_DAY_CONSUMPTION>()
				.Property(e => e.water)
				.HasPrecision(19, 0);

			modelBuilder.Entity<ALC_DAY_CONSUMPTION>()
				.Property(e => e.energyDiff)
				.HasPrecision(19, 0);

			modelBuilder.Entity<ALC_DAY_CONSUMPTION>()
				.Property(e => e.waterDiff)
				.HasPrecision(19, 0);

			modelBuilder.Entity<ALC_DEFECT>()
				.Property(e => e.COD_DEFECT)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_DEFECT>()
				.Property(e => e.DEFECT)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_DEFECT>()
				.HasMany(e => e.ALC_OQC_INSPECTION)
				.WithOptional(e => e.ALC_DEFECT)
				.HasForeignKey(e => e.defect);

			modelBuilder.Entity<ALC_DEFECT>()
				.HasMany(e => e.ALC_RMB_REGISTER)
				.WithOptional(e => e.ALC_DEFECT)
				.HasForeignKey(e => e.DEFECT);

			modelBuilder.Entity<ALC_DEFECT>()
				.HasMany(e => e.ALC_REPAIR)
				.WithRequired(e => e.ALC_DEFECT)
				.HasForeignKey(e => e.ID_DEFECT)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ALC_DEPARTMENT>()
				.Property(e => e.DESCRIPTION)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_DEPARTMENT>()
				.HasMany(e => e.ALC_DOWNTIME)
				.WithOptional(e => e.ALC_DEPARTMENT)
				.HasForeignKey(e => e.DEPARTMENT);

			modelBuilder.Entity<ALC_DOWNTIME>()
				.Property(e => e.ID_DOWNTIME)
				.HasPrecision(19, 0);

			modelBuilder.Entity<ALC_DOWNTIME>()
				.Property(e => e.FAILURE)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_DOWNTIME>()
				.Property(e => e.INTERVAL)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_DOWNTIME>()
				.Property(e => e.PPB)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_DOWNTIME>()
				.Property(e => e.SHIFT)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_DOWNTIME>()
				.Property(e => e.ID_FAILURE)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_DOWNTIME>()
				.Property(e => e.OBSERVATION)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_DOWNTIME>()
				.Property(e => e.ID_OP)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_DOWNTIME>()
				.HasOptional(e => e.ALC_DOWNTIME1)
				.WithRequired(e => e.ALC_DOWNTIME2);

			modelBuilder.Entity<ALC_ENGINE>()
				.Property(e => e.SERIAL)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_ENGINE>()
				.Property(e => e.ORDER_ENG)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_ENGINE>()
				.Property(e => e.OP_PA)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_ENGINE>()
				.Property(e => e.MAGAZINE)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_ENGINE>()
				.Property(e => e.BENCH)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_ENGINE>()
				.Property(e => e.JIG_SN)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_FAILURE>()
				.Property(e => e.ID_FAILURE)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_FAILURE>()
				.Property(e => e.DESCRIPTION)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_FAMILY_SKU>()
				.Property(e => e.FAMILY_NAME)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_FULL_SKU>()
				.Property(e => e.FULL_SKU)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_HDT_PROD>()
				.Property(e => e.HDT_NAME)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_HDT_PROD>()
				.Property(e => e.BEGIN_RANGE)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_HDT_PROD>()
				.Property(e => e.CURRENT_RANGE)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_HDT_PROD>()
				.Property(e => e.END_RANGE)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_HOUR_PALLET>()
				.Property(e => e.idHour)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_INVENTORY>()
				.Property(e => e.PACKAGE_CODE)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LIST_ORDER>()
				.Property(e => e.imei)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LIST_ORDER>()
				.Property(e => e.codorder)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LIST_ORDER>()
				.Property(e => e.fullsku)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LIST_ORDER>()
				.Property(e => e.lot)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LIST_ORDER>()
				.Property(e => e.packing)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LIST_ORDER>()
				.Property(e => e.pallet)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LIST_ORDER>()
				.Property(e => e.shipping)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LOG_PRODUCT>()
				.Property(e => e.serial)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LOG_PRODUCT>()
				.Property(e => e.imei1)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LOG_PRODUCT>()
				.Property(e => e.imei2)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LOG_PRODUCT>()
				.Property(e => e.suid)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LOG_PRODUCT>()
				.Property(e => e.sd_receiopt)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LOT_LIST>()
				.Property(e => e.PACKING)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LOT_LIST>()
				.Property(e => e.ID_LOTE)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LOT_LIST>()
				.Property(e => e.ORDER_ID)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LOT_LIST>()
				.Property(e => e.PACK_STRING)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LOT_LIST>()
				.Property(e => e.FAMILY)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LOTE_LIST>()
				.Property(e => e.PACKING)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LOTE_LIST>()
				.Property(e => e.ID_LOTE)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LOTE_LIST>()
				.Property(e => e.ORDER_ID)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_MAGAZINE>()
				.Property(e => e.ID_MAG)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_MAGAZINE>()
				.Property(e => e.STATUS)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_MAGAZINE>()
				.Property(e => e.OP)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_MAGAZINE>()
				.HasMany(e => e.ALC_ENGINE)
				.WithOptional(e => e.ALC_MAGAZINE)
				.HasForeignKey(e => e.MAGAZINE);

			modelBuilder.Entity<ALC_MONTH_CONSUMPTION>()
				.Property(e => e.ID_MONTH_CONSUMPTION)
				.HasPrecision(19, 0);

			modelBuilder.Entity<ALC_MONTH_CONSUMPTION>()
				.Property(e => e.MONTH_CONSUMPTION)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_OQC_INSPECTION>()
				.Property(e => e.box)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_OQC_INSPECTION>()
				.Property(e => e.observation)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_OQC_INSPECTION>()
				.Property(e => e.sku)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_OQC_INSPECTION>()
				.Property(e => e.defect)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_OQC_INSPECTION>()
				.Property(e => e.status)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_OQC_INSPECTION>()
				.Property(e => e.family)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_OQC_INSPECTION>()
				.Property(e => e.lot)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_OQC_TARGET>()
				.Property(e => e.sku)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_OQC_TARGET>()
				.HasMany(e => e.ALC_OQC_INSPECTION)
				.WithOptional(e => e.ALC_OQC_TARGET)
				.HasForeignKey(e => e.target);

			modelBuilder.Entity<ALC_ORDER>()
				.Property(e => e.idOrder)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_ORDER>()
				.Property(e => e.status)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_ORDER>()
				.Property(e => e.ppb_ppb)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_ORDER>()
				.Property(e => e.fullSku)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_ORDER>()
				.HasMany(e => e.ALC_COVER_LED)
				.WithOptional(e => e.ALC_ORDER)
				.HasForeignKey(e => e.OP);

			modelBuilder.Entity<ALC_ORDER>()
				.HasMany(e => e.ALC_DOWNTIME)
				.WithOptional(e => e.ALC_ORDER)
				.HasForeignKey(e => e.ID_OP);

			modelBuilder.Entity<ALC_ORDER>()
				.HasMany(e => e.ALC_ENGINE)
				.WithOptional(e => e.ALC_ORDER)
				.HasForeignKey(e => e.ORDER_ENG);

			modelBuilder.Entity<ALC_ORDER>()
				.HasMany(e => e.ALC_ENGINE1)
				.WithOptional(e => e.ALC_ORDER1)
				.HasForeignKey(e => e.OP_PA);

			modelBuilder.Entity<ALC_ORDER>()
				.HasMany(e => e.ALC_LOT_LIST)
				.WithOptional(e => e.ALC_ORDER)
				.HasForeignKey(e => e.ORDER_ID);

			modelBuilder.Entity<ALC_ORDER>()
				.HasMany(e => e.ALC_LOTE_LIST)
				.WithOptional(e => e.ALC_ORDER)
				.HasForeignKey(e => e.ORDER_ID);

			modelBuilder.Entity<ALC_ORDER>()
				.HasMany(e => e.ALC_MAGAZINE)
				.WithOptional(e => e.ALC_ORDER)
				.HasForeignKey(e => e.OP);

			modelBuilder.Entity<ALC_ORDER>()
				.HasMany(e => e.ALC_VOLUME)
				.WithOptional(e => e.ALC_ORDER)
				.HasForeignKey(e => e.ORDER_VOL);

			modelBuilder.Entity<ALC_ORDER>()
				.HasMany(e => e.ALC_PRODUCTION_COUNTER)
				.WithOptional(e => e.ALC_ORDER)
				.HasForeignKey(e => e.order_idOrder);

			modelBuilder.Entity<ALC_ORDER>()
				.HasMany(e => e.ALC_VOLUME1)
				.WithOptional(e => e.ALC_ORDER1)
				.HasForeignKey(e => e.ORDER_VOL);

			modelBuilder.Entity<ALC_ORDER>()
				.HasMany(e => e.ALC_PRODUCTION_COUNTER1)
				.WithOptional(e => e.ALC_ORDER1)
				.HasForeignKey(e => e.order_idOrder);

			modelBuilder.Entity<ALC_POST_USER>()
				.Property(e => e.description)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_POST_USER>()
				.HasMany(e => e.ALC_SYS_USER)
				.WithOptional(e => e.ALC_POST_USER)
				.HasForeignKey(e => e.USER_POST);

			modelBuilder.Entity<ALC_POST_USER>()
				.HasMany(e => e.ALC_SYS_USER1)
				.WithOptional(e => e.ALC_POST_USER1)
				.HasForeignKey(e => e.USER_POST);

			modelBuilder.Entity<ALC_POST_USER>()
				.HasMany(e => e.ALC_REPAIR)
				.WithRequired(e => e.ALC_POST_USER)
				.HasForeignKey(e => e.ID_POST)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ALC_PPB>()
				.Property(e => e.ppb)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_PPB>()
				.HasMany(e => e.ALC_ORDER)
				.WithOptional(e => e.ALC_PPB)
				.HasForeignKey(e => e.ppb_ppb);

			modelBuilder.Entity<ALC_PPB>()
				.HasMany(e => e.ALC_PROD_PLAN)
				.WithRequired(e => e.ALC_PPB)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ALC_PPB_MONTH>()
				.Property(e => e.ID_PPB_MONTH)
				.HasPrecision(19, 0);

			modelBuilder.Entity<ALC_PPB_MONTH>()
				.Property(e => e.FULL_SKU)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_PPB_MONTH>()
				.Property(e => e.QTY)
				.HasPrecision(19, 0);

			modelBuilder.Entity<ALC_PPB_TARGET>()
				.Property(e => e.TARGET_ID)
				.HasPrecision(19, 0);

			modelBuilder.Entity<ALC_PPB_TARGET>()
				.Property(e => e.MATERIAL)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_PROD_PLAN>()
				.Property(e => e.id_plan)
				.HasPrecision(19, 0);

			modelBuilder.Entity<ALC_PROD_PLAN>()
				.Property(e => e.full_sku)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_PROD_PLAN>()
				.Property(e => e.ppb)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_PRODUCTION_COUNTER>()
				.Property(e => e.order_idOrder)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_PRODUCTION_COUNTER>()
				.Property(e => e.packing)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_PRODUCTION_COUNTER>()
				.Property(e => e.volume)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_PRODUCTION_COUNTER>()
				.Property(e => e.ID_LOTE)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_PROFILE>()
				.Property(e => e.id)
				.HasPrecision(19, 0);

			modelBuilder.Entity<ALC_PROFILE>()
				.Property(e => e.avatar)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_PROFILE>()
				.Property(e => e.email)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_PROFILE>()
				.Property(e => e.firstName)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_PROFILE>()
				.Property(e => e.lastName)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_PROFILE>()
				.Property(e => e.password)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_PROFILE>()
				.Property(e => e.passwordResetToken)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_PROFILE>()
				.Property(e => e.profileType)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_PROFILE>()
				.Property(e => e.username)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_PROFILE>()
				.Property(e => e.uuid)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_PROFILE>()
				.HasMany(e => e.ALC_ROLE_PROF)
				.WithOptional(e => e.ALC_PROFILE)
				.HasForeignKey(e => e.profile_id);

			modelBuilder.Entity<ALC_REPAIR>()
				.Property(e => e.COMPONENT)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_REPAIR>()
				.Property(e => e.OBSERVATION)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_REPAIR>()
				.Property(e => e.STEP)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_REPAIR>()
				.Property(e => e.ID_DEFECT)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_REPAIR>()
				.Property(e => e.ID_REASON)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_REPAIR>()
				.Property(e => e.STATUS_ENG)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_REPAIR>()
				.Property(e => e.DURATION_TIME)
				.HasPrecision(19, 0);

			modelBuilder.Entity<ALC_REPAIR_ACTION>()
				.Property(e => e.DESCRIPTION)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_REPAIR_ORIGIN>()
				.Property(e => e.DESCRIPTION)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_REPAIR_REASON>()
				.Property(e => e.ID_REASON)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_REPAIR_REASON>()
				.Property(e => e.DESCRIPTION)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_REPAIR_REASON>()
				.HasMany(e => e.ALC_RMB_REGISTER)
				.WithOptional(e => e.ALC_REPAIR_REASON)
				.HasForeignKey(e => e.REASON);

			modelBuilder.Entity<ALC_RESPONSIBLE>()
				.Property(e => e.name)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_RMB_COST>()
				.Property(e => e.COST_ID)
				.HasPrecision(19, 0);

			modelBuilder.Entity<ALC_RMB_COST>()
				.Property(e => e.PART_NUMBER)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_RMB_COST>()
				.HasMany(e => e.ALC_RMB_REGISTER)
				.WithOptional(e => e.ALC_RMB_COST)
				.HasForeignKey(e => e.RMB_COST);

			modelBuilder.Entity<ALC_RMB_PART_NUMBER>()
				.Property(e => e.SAP_CODE)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_RMB_PART_NUMBER>()
				.Property(e => e.DESCRIPTION)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_RMB_PART_NUMBER>()
				.HasMany(e => e.ALC_RMB_COST)
				.WithOptional(e => e.ALC_RMB_PART_NUMBER)
				.HasForeignKey(e => e.PART_NUMBER);

			modelBuilder.Entity<ALC_RMB_PART_NUMBER>()
				.HasMany(e => e.ALC_RMB_REGISTER)
				.WithOptional(e => e.ALC_RMB_PART_NUMBER)
				.HasForeignKey(e => e.PART_NUMBER);

			modelBuilder.Entity<ALC_RMB_REGISTER>()
				.Property(e => e.ID_RMB)
				.HasPrecision(19, 0);

			modelBuilder.Entity<ALC_RMB_REGISTER>()
				.Property(e => e.SERIAL)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_RMB_REGISTER>()
				.Property(e => e.RMB_STATUS)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_RMB_REGISTER>()
				.Property(e => e.TOTAL_RMB)
				.HasPrecision(19, 0);

			modelBuilder.Entity<ALC_RMB_REGISTER>()
				.Property(e => e.RMB_COST)
				.HasPrecision(19, 0);

			modelBuilder.Entity<ALC_RMB_REGISTER>()
				.Property(e => e.DEFECT)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_RMB_REGISTER>()
				.Property(e => e.PART_NUMBER)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_RMB_REGISTER>()
				.Property(e => e.REASON)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_ROLE_PROF>()
				.Property(e => e.id)
				.HasPrecision(19, 0);

			modelBuilder.Entity<ALC_ROLE_PROF>()
				.Property(e => e.profile_id)
				.HasPrecision(19, 0);

			modelBuilder.Entity<ALC_ROLE_USER>()
				.Property(e => e.authority)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_ROLE_USER>()
				.HasMany(e => e.ALC_SYS_USER)
				.WithMany(e => e.ALC_ROLE_USER)
				.Map(m => m.ToTable("ALC_SYS_USER_ALC_ROLE_USER").MapLeftKey("roles_authority"));

			modelBuilder.Entity<ALC_SGQ_DOCUMENT>()
				.Property(e => e.ID_DOCUMENT)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_SGQ_DOCUMENT>()
				.Property(e => e.DESCRIPTION)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_SGQ_DOCUMENT>()
				.Property(e => e.LINK)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_SGQ_DOCUMENT>()
				.Property(e => e.DOC_TYPE)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_SGQ_PROCESS>()
				.Property(e => e.DESCRIPTION)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_SGQ_PROCESS>()
				.HasMany(e => e.ALC_SGQ_DOCUMENT)
				.WithOptional(e => e.ALC_SGQ_PROCESS)
				.HasForeignKey(e => e.process_ID_PROCESS);

			modelBuilder.Entity<ALC_SHIFT>()
				.Property(e => e.DESCRIPTION)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_SHIFT_PROD>()
				.Property(e => e.description)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_SHIFT_PROD>()
				.HasMany(e => e.ALC_INTERVAL_PROD)
				.WithOptional(e => e.ALC_SHIFT_PROD)
				.HasForeignKey(e => e.shift_idShift);

			modelBuilder.Entity<ALC_SYS_FLAG>()
				.Property(e => e.ID_FLAG)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_SYS_LOG>()
				.Property(e => e.ID_LOG)
				.HasPrecision(19, 0);

			modelBuilder.Entity<ALC_SYS_LOG>()
				.Property(e => e.CODE)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_SYS_LOG>()
				.Property(e => e.DESCRIPTION)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_SYS_LOG>()
				.Property(e => e.SYS_LOG)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_SYS_USER>()
				.Property(e => e.USER_LOGIN)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_SYS_USER>()
				.Property(e => e.userPassword)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_SYS_USER>()
				.Property(e => e.USER_NAME)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_SYS_USER>()
				.Property(e => e.USER_PASSWORD)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_SYS_USER>()
				.HasMany(e => e.ALC_INVENTORY)
				.WithOptional(e => e.ALC_SYS_USER)
				.HasForeignKey(e => e.USER_INVENTORY);

			modelBuilder.Entity<ALC_SYS_USER>()
				.HasMany(e => e.ALC_LOT_LIST)
				.WithOptional(e => e.ALC_SYS_USER)
				.HasForeignKey(e => e.RESP_PALLET);

			modelBuilder.Entity<ALC_SYS_USER>()
				.HasMany(e => e.ALC_LOTE_LIST)
				.WithOptional(e => e.ALC_SYS_USER)
				.HasForeignKey(e => e.RESP_PALLET);

			modelBuilder.Entity<ALC_SYS_USER>()
				.HasMany(e => e.ALC_OQC_INSPECTION)
				.WithOptional(e => e.ALC_SYS_USER)
				.HasForeignKey(e => e.oqc_operator);

			modelBuilder.Entity<ALC_SYS_USER>()
				.HasMany(e => e.ALC_ORDER)
				.WithOptional(e => e.ALC_SYS_USER)
				.HasForeignKey(e => e.user_USER_ID);

			modelBuilder.Entity<ALC_SYS_USER>()
				.HasMany(e => e.ALC_PRODUCTION_COUNTER)
				.WithOptional(e => e.ALC_SYS_USER)
				.HasForeignKey(e => e.RESP_PALLET);

			modelBuilder.Entity<ALC_SYS_USER>()
				.HasMany(e => e.ALC_REPAIR)
				.WithOptional(e => e.ALC_SYS_USER)
				.HasForeignKey(e => e.ENTRY_USER);

			modelBuilder.Entity<ALC_SYS_USER>()
				.HasMany(e => e.ALC_REPAIR1)
				.WithOptional(e => e.ALC_SYS_USER1)
				.HasForeignKey(e => e.TECHNICAL);

			modelBuilder.Entity<ALC_SYS_USER>()
				.HasMany(e => e.ALC_SYS_LOG)
				.WithOptional(e => e.ALC_SYS_USER)
				.HasForeignKey(e => e.RESPONSIBLE);

			modelBuilder.Entity<ALC_TARGET_HOUR>()
				.HasMany(e => e.ALC_ORDER)
				.WithOptional(e => e.ALC_TARGET_HOUR)
				.HasForeignKey(e => e.TARGET_HOUR);

			modelBuilder.Entity<ALC_TARGET_HOUR>()
				.HasMany(e => e.ALC_PROD_PLAN)
				.WithOptional(e => e.ALC_TARGET_HOUR)
				.HasForeignKey(e => e.target);

			modelBuilder.Entity<ALC_TARGET_HOUR>()
				.HasOptional(e => e.ALC_HOUR_SHIFT)
				.WithRequired(e => e.ALC_TARGET_HOUR);

			modelBuilder.Entity<alc_user_role>()
				.Property(e => e.authority)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_VOLUME>()
				.Property(e => e.ID_VOL)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_VOLUME>()
				.Property(e => e.STATUS)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_VOLUME>()
				.Property(e => e.ORDER_VOL)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_VOLUME>()
				.HasMany(e => e.ALC_LOT_LIST)
				.WithOptional(e => e.ALC_VOLUME)
				.HasForeignKey(e => e.ID_LOTE);

			modelBuilder.Entity<ALC_VOLUME>()
				.HasMany(e => e.ALC_OQC_INSPECTION)
				.WithOptional(e => e.ALC_VOLUME)
				.HasForeignKey(e => e.lot);

			modelBuilder.Entity<ALC_VOLUME>()
				.HasMany(e => e.ALC_PRODUCTION_COUNTER)
				.WithOptional(e => e.ALC_VOLUME)
				.HasForeignKey(e => e.ID_LOTE);

			modelBuilder.Entity<hdtconfig>()
				.Property(e => e.hdtProcessName)
				.IsUnicode(false);

			modelBuilder.Entity<hdtconfig>()
				.Property(e => e.urlPortal)
				.IsUnicode(false);

			modelBuilder.Entity<hdtmodel>()
				.Property(e => e.description)
				.IsUnicode(false);

			modelBuilder.Entity<hdtmodel>()
				.Property(e => e.modelNick)
				.IsUnicode(false);

			modelBuilder.Entity<hdtmodel>()
				.Property(e => e.pathImei)
				.IsUnicode(false);

			modelBuilder.Entity<hdtmodel>()
				.Property(e => e.pathMac)
				.IsUnicode(false);

			modelBuilder.Entity<hdtmodel>()
				.Property(e => e.userNameModified)
				.IsUnicode(false);

			modelBuilder.Entity<hdtmodel>()
				.HasMany(e => e.hdtmodelrange)
				.WithOptional(e => e.hdtmodel)
				.HasForeignKey(e => e.hdtModel_id);

			modelBuilder.Entity<hdtmodelrange>()
				.Property(e => e.status)
				.IsUnicode(false);

			modelBuilder.Entity<hdtmodelrange>()
				.Property(e => e.userNameCreated)
				.IsUnicode(false);

			modelBuilder.Entity<hdtmodelrange>()
				.Property(e => e.userNameEnded)
				.IsUnicode(false);

			modelBuilder.Entity<hdtupdatelog>()
				.Property(e => e.hdtId)
				.IsUnicode(false);

			modelBuilder.Entity<hdtupdatelog>()
				.Property(e => e.model)
				.IsUnicode(false);

			modelBuilder.Entity<hdtupdatelog>()
				.Property(e => e.type)
				.IsUnicode(false);

			modelBuilder.Entity<hdtupdatelog>()
				.Property(e => e.userName)
				.IsUnicode(false);

			modelBuilder.Entity<PACKING_SERIAL_VIEW>()
				.Property(e => e.HOSTNAME)
				.IsUnicode(false);

			modelBuilder.Entity<PACKING_SERIAL_VIEW>()
				.Property(e => e.IMEI_1)
				.IsUnicode(false);

			modelBuilder.Entity<PACKING_SERIAL_VIEW>()
				.Property(e => e.IMEI_2)
				.IsUnicode(false);

			modelBuilder.Entity<PACKING_SERIAL_VIEW>()
				.Property(e => e.IMEI_3)
				.IsUnicode(false);

			modelBuilder.Entity<PACKING_SERIAL_VIEW>()
				.Property(e => e.IMEI_4)
				.IsUnicode(false);

			modelBuilder.Entity<PACKING_SERIAL_VIEW>()
				.Property(e => e.PACKING)
				.IsUnicode(false);

			modelBuilder.Entity<PACKING_SERIAL_VIEW>()
				.Property(e => e.SERIAL)
				.IsUnicode(false);

			modelBuilder.Entity<TEST_ALCATEL>()
				.Property(e => e.TEST_NAME)
				.IsUnicode(false);

			modelBuilder.Entity<usertable>()
				.Property(e => e.password)
				.IsUnicode(false);

			modelBuilder.Entity<usertable>()
				.Property(e => e.userName)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LOT_LIST_BACK>()
				.Property(e => e.PACKING)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LOT_LIST_BACK>()
				.Property(e => e.ID_LOTE)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LOT_LIST_BACK>()
				.Property(e => e.ORDER_ID)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LOT_LIST_BACK>()
				.Property(e => e.PACK_STRING)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_LOT_LIST_BACK>()
				.Property(e => e.FAMILY)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_REPAIR_BACK>()
				.Property(e => e.COMPONENT)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_REPAIR_BACK>()
				.Property(e => e.OBSERVATION)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_REPAIR_BACK>()
				.Property(e => e.STEP)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_REPAIR_BACK>()
				.Property(e => e.ID_DEFECT)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_REPAIR_BACK>()
				.Property(e => e.ID_REASON)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_REPAIR_BACK>()
				.Property(e => e.STATUS_ENG)
				.IsUnicode(false);

			modelBuilder.Entity<ALC_REPAIR_BACK>()
				.Property(e => e.DURATION_TIME)
				.HasPrecision(19, 0);

			modelBuilder.Entity<LOT_VIEW>()
				.Property(e => e.PACKING)
				.IsUnicode(false);

			modelBuilder.Entity<LOT_VIEW>()
				.Property(e => e.ID_LOTE)
				.IsUnicode(false);

			modelBuilder.Entity<LOT_VIEW>()
				.Property(e => e.ORDER_ID)
				.IsUnicode(false);

			modelBuilder.Entity<LOT_VIEW>()
				.Property(e => e.order_idorder)
				.IsUnicode(false);

			modelBuilder.Entity<LOT_VIEW>()
				.Property(e => e.fullSku)
				.IsUnicode(false);

			modelBuilder.Entity<USEFUL_MODEL>()
				.Property(e => e.SKU)
				.IsUnicode(false);

			modelBuilder.Entity<USEFUL_MODEL>()
				.Property(e => e.EAN)
				.IsUnicode(false);

			modelBuilder.Entity<USEFUL_MODEL>()
				.Property(e => e.MODEL)
				.IsUnicode(false);

			modelBuilder.Entity<USEFUL_MODEL>()
				.Property(e => e.MODEL_NAME)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_ProdDia>()
				.Property(e => e.CodFab)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_ProdDia>()
				.Property(e => e.DatProducao)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<tbl_ProdDia>()
				.Property(e => e.CodCin)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_ProdDia>()
				.Property(e => e.FlagTP)
				.IsUnicode(false);

			modelBuilder.Entity<tbl_ProdDia>()
				.Property(e => e.NumAP)
				.IsUnicode(false);
		}
	}
}
