namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_macrange
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_macrange()
        {
            dw_macrange1 = new HashSet<dw_macrange>();
            dw_macuso = new HashSet<dw_macuso>();
        }

        [Key]
        public long id_macrange { get; set; }

        [Required]
        [StringLength(12)]
        public string cd_macinicial { get; set; }

        [Required]
        [StringLength(12)]
        public string cd_macfinal { get; set; }

        public long? id_gt { get; set; }

        [StringLength(30)]
        public string cd_modelo { get; set; }

        public int qt_porpeca { get; set; }

        public short tp_regra { get; set; }

        public short st_macrange { get; set; }

        public long id_usr { get; set; }

        public DateTime dthr_cadastro { get; set; }

        [StringLength(12)]
        public string cd_ultimomacusado { get; set; }

        public DateTime? dthr_ultimomacusado { get; set; }

        public long? id_macrangePAI { get; set; }

        public virtual om_gt om_gt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_macrange> dw_macrange1 { get; set; }

        public virtual dw_macrange dw_macrange2 { get; set; }

        public virtual om_usr om_usr { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_macuso> dw_macuso { get; set; }
    }
}
