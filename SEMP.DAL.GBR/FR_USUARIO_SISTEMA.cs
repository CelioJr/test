namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_USUARIO_SISTEMA
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int USR_CODIGO { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(3)]
        public string SIS_CODIGO { get; set; }

        [Required]
        [StringLength(1)]
        public string USS_ACESSO_EXTERNO { get; set; }

        [Required]
        [StringLength(1)]
        public string USS_ADMINISTRADOR { get; set; }

        [Required]
        [StringLength(1)]
        public string USS_ACESSO_MAKER { get; set; }

        [Required]
        [StringLength(1)]
        public string USS_CRIAR_FORMULARIO { get; set; }

        [Required]
        [StringLength(1)]
        public string USS_CRIAR_RELATORIO { get; set; }

        [Required]
        [StringLength(1)]
        public string USS_ACESSAR { get; set; }

        [Required]
        [StringLength(1)]
        public string USS_CRIAR_REGRA { get; set; }

        public virtual FR_USUARIO FR_USUARIO { get; set; }
    }
}
