﻿using SEMP.Model.DTO;
using SEMP.DAL.Integrator.DAO;
using System.Collections.Generic;
using System;

namespace SEMP.BO.Integrator
{
	public class Integrador
	{
		public static string ObterStringConexao() {
			return daoIntegrador.ObterStringConexao();
		}

		public static List<ZSPP_APONTAMENTO_PCI_DTO> ObterApontamentoPendenteEnvio()
		{
			return daoIntegrador.ObterApontamentoPendenteEnvio();

		}

		public static List<ZSPP_APONTAMENTO_PCI_DTO> ObterApontamentoPorPacote(string packno)
		{
			return daoIntegrador.ObterApontamentoPorPacote(packno);

		}

		public static List<ZSPP_APONTAMENTO_PCI_DTO> ObterApontamento(DateTime datInicio, DateTime datFim, string numOrdem = null, string packno = null) {
			return daoIntegrador.ObterApontamento(datInicio, datFim, numOrdem, packno);
		}

		public static bool AtualizarApontamentoPendenteEnvio(string PackNo)
		{
			return daoIntegrador.AtualizarApontamentoPendenteEnvio(PackNo);

		}

		public static bool AtualizarPackNOApontamento(string packno)
		{
			return daoIntegrador.AtualizarPackNOApontamento(packno);

		}

		public static bool DesfazerPackNOApontamento(string packno){
			return daoIntegrador.DesfazerPackNOApontamento(packno);

		}

		public static string ObterPackNumber()
		{

			return daoIntegrador.ObterPackNumber();
		}

		public static bool AtualizarStatus(bool status)
		{
			return daoIntegrador.AtualizarStatus(status);

		}

	}
}
