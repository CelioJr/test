using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
    public partial class CBCadDefeitoDTO
    {
        [Display(Name = "F�brica")]
        public string CodFab { get; set; }

        [Display(Name = "C�d. Defeito")]
        public string CodDefeito { get; set; }

        [Display(Name = "Descri��o")]
        [Required]
        public string DesDefeito { get; set; }

        [Display(Name = "Verifica��o")]
        public string DesVerificacao { get; set; }

        [Display(Name = "IAC")]
        public short? flgIAC { get; set; }

        [Display(Name = "IMC")]
        public short? flgIMC { get; set; }

        [Display(Name = "FEC")]
        public short? flgFEC { get; set; }

        [Display(Name = "LCM")]
        public short? flgLCM { get; set; }

        [Display(Name = "Ativo")]
        public string flgAtivo { get; set; }
    }
}