using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_CHECK_CQ_LIST_DTO
    {
        public int GBR_CHE_ID { get; set; }
        public string PALLET { get; set; }
        public string PACK { get; set; }
        public string IMEI { get; set; }
        public string USUARIO { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
        public Nullable<bool> FECHADOS { get; set; }
    }
}
