namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolcipoco
    {
        [Key]
        public long id_consolcipoco { get; set; }

        public DateTime? dthr_icip { get; set; }

        public DateTime? dthr_fcip { get; set; }

        public long id_consol { get; set; }

        public long? id_consolciplog { get; set; }

        public virtual dw_consol dw_consol { get; set; }

        public virtual dw_consolciplog dw_consolciplog { get; set; }
    }
}
