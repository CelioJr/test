namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_estpro
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_estpro()
        {
            dw_estlocalpro = new HashSet<dw_estlocalpro>();
            dw_estmov = new HashSet<dw_estmov>();
        }

        [Key]
        public long id_estpro { get; set; }

        public decimal? qt_entrada { get; set; }

        public decimal? qt_saida { get; set; }

        public long id_est { get; set; }

        public long id_produto { get; set; }

        public decimal? qt_reservada { get; set; }

        public long? id_cliente { get; set; }

        public decimal? qt_ajuste { get; set; }

        public decimal? qt_total { get; set; }

        public DateTime? dthr_ajuste { get; set; }

        public DateTime? dthr_entrada { get; set; }

        public DateTime? dthr_saida { get; set; }

        public DateTime? dthr_total { get; set; }

        public virtual dw_est dw_est { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_estlocalpro> dw_estlocalpro { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_estmov> dw_estmov { get; set; }

        public virtual pp_cliente pp_cliente { get; set; }

        public virtual om_produto om_produto { get; set; }
    }
}
