﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
    public class RelatorioProducao
	{
		public string CodLinha { get; set; }
		public string DesLinha { get; set; }
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public int QtdCapacidade { get; set; }
		public int QtdPlano { get; set; }
		public int QtdRealizado { get; set; }
		public int QtdDiferenca { get { return QtdRealizado - QtdPlano; } }
		public decimal PercDiferenca { get; set; }
	}

	public class ResumoProducao
	{
		[Display(Name = "Data")]
		[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
		public DateTime DatProducao { get; set; }
		[Display(Name = "Linha")]
		public String CodLinha { get; set; }
		[Display(Name = "Descrição")]
		public String DesLinha { get; set; }
		[Display(Name = "Modelo")]
		public String CodModelo { get; set; }
		[Display(Name = "Descrição")]
		public String DesModelo { get; set; }
		[Display(Name = "Cinescópio")]
		public String Cinescopio { get; set; }
		[Display(Name = "Plano")]
		public int QtdPlano { get; set; }
		[Display(Name = "Prod.")]
		public int QtdRealizado { get; set; }
		[Display(Name = "Apont.")]
		public int? QtdApontado { get; set; }
		[Display(Name = "Dif.")]
		public int QtdDiferenca { get { return (int)(QtdApontado.HasValue ? QtdApontado - QtdRealizado : 0); } }
		[Display(Name = "Dif.")]
		public int QtdDifProducao { get { return QtdRealizado - QtdPlano; } }
		[Display(Name = "PP Mês")]
		public int? QtdPlanoMes { get; set; }
		[Display(Name = "Prod. Mês")]
		public int? QtdProduzidoMes { get; set; }
		[Display(Name = "Dif. Mês")]
		public int QtdDifMes { get { return (int) (QtdProduzidoMes.HasValue && QtdPlanoMes.HasValue ? QtdProduzidoMes - QtdPlanoMes: 0); } }
		[Display(Name = "Família")]
		public String Familia { get; set; }
		[Display(Name = "Turno")]
		public int Turno { get; set; }
		[Display(Name = "Plano VPE")]
		public int QtdPlanoVPE { get; set; }

		public string FlagTp { get; set; }

		[Display(Name = "Exped")]
		public int QtdExped { get; set; }

		[Display(Name = "IDW")]
		public int QtdIDW { get; set; }

		[Display(Name = "Insp. OBA")]
		public int QtdOBA { get; set; }

		public decimal? QtdDefeito { get; set; }
		public decimal? QtdFalsaFalha { get; set; }

		public string CodFab { get; set; }
		public string NumAP { get; set; }

		public ResumoProducao()
		{
			QtdPlanoMes = 0;
			QtdProduzidoMes = 0;
			QtdApontado = 0;
			QtdRealizado = 0;
			QtdPlano = 0;
			QtdPlanoVPE = 0;
			FlagTp = "";
			QtdDefeito = 0;
			QtdFalsaFalha = 0;
		}

	}

	public class ParadasLinha
	{
		public string CodParada { get; set; }
		public string DesParada { get; set; }
		public char FlgPDL { get; set; }
		public string HoraInicio { get; set; }
		public string HoraFim { get; set; }
		public string TotalHoras { get; set; }
		public int QtdAparelhos { get; set; }
		public string SetorResp { get; set; }
		public String CodLinha { get; set; }

	}

	public class DefeitosLinha
	{
		public int CodLinha { get; set; }
		public string Origem { get; set; }
		public decimal Taxa { get; set; }
		public int Quantidade { get; set; }
		public string CodDefeito { get; set; }
		public string DesDefeito { get; set; }
		public string CodCausa { get; set; }
		public string DesCausa { get; set; }
		public string Posicao { get; set; }
		public string Disposicao { get; set; }
		public string Responsavel { get; set; }
		public string Acao { get; set; }
		public string PlanoDeAcao { get; set; }
		public decimal Prazo { get; set; }
		public string Verificacao { get; set; }
		public string Status { get; set; }
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public string FlgExibir { get; set; }
	}

}
