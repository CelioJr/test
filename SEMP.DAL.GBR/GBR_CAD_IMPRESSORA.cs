namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_CAD_IMPRESSORA
    {
        [Key]
        public int COD_IMPRESSORA { get; set; }

        [StringLength(50)]
        public string IMPRESSORA { get; set; }

        public bool? ATIVO { get; set; }

        public DateTime? DATA { get; set; }

        [StringLength(50)]
        public string CAMINHO { get; set; }
    }
}
