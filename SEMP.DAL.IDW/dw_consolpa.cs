namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolpa
    {
        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal id_consolpa { get; set; }

        public long id_consol { get; set; }

        public decimal? seg_auto_tempoparada_ab { get; set; }

        public long id_tparada { get; set; }

        public decimal? qt_auto_ocoparada_cp { get; set; }

        public decimal? qt_auto_ocoparada_sp { get; set; }

        public decimal? qt_manu_ocoparada_cp { get; set; }

        public decimal? qt_manu_ocoparada_sp { get; set; }

        public decimal? seg_auto_tempoparada_cp { get; set; }

        public decimal? seg_auto_tempoparada_sp { get; set; }

        public decimal? seg_manu_tempoparada_cp { get; set; }

        public decimal? seg_manu_tempoparada_sp { get; set; }

        public decimal? seg_auto_tempoparada_cp_vr { get; set; }

        public decimal? seg_auto_tempoparada_sp_vr { get; set; }

        public decimal? seg_manu_tempoparada_cp_vr { get; set; }

        public decimal? seg_manu_tempoparada_sp_vr { get; set; }

        public decimal? qt_auto_ocoparada_cp_vr { get; set; }

        public decimal? qt_auto_ocoparada_sp_vr { get; set; }

        public decimal? qt_manu_ocoparada_cp_vr { get; set; }

        public decimal? qt_manu_ocoparada_sp_vr { get; set; }

        public decimal? seg_auto_tempoparada_default { get; set; }

        public decimal? seg_auto_tempoparada_sem_op { get; set; }

        public decimal? seg_auto_tempoparada_sem_evt { get; set; }

        public decimal? seg_auto_tempoparada_sem_cnx { get; set; }

        public decimal? seg_manu_tempoparada_default { get; set; }

        public decimal? seg_manu_tempoparada_sem_op { get; set; }

        public decimal? seg_manu_tempoparada_sem_evt { get; set; }

        public decimal? seg_manu_tempoparada_sem_cnx { get; set; }

        public decimal? qt_auto_tempoparada_default { get; set; }

        public decimal? qt_auto_tempoparada_sem_op { get; set; }

        public decimal? qt_auto_tempoparada_sem_evt { get; set; }

        public decimal? qt_auto_tempoparada_sem_cnx { get; set; }

        public decimal? qt_manu_tempoparada_default { get; set; }

        public decimal? qt_manu_tempoparada_sem_op { get; set; }

        public decimal? qt_manu_tempoparada_sem_evt { get; set; }

        public decimal? qt_manu_tempoparada_sem_cnx { get; set; }

        public decimal? seg_auto_cta { get; set; }

        public decimal? seg_manu_cta { get; set; }

        public decimal? pcs_auto_perdaparada_cp { get; set; }

        public decimal? pcs_auto_perdaparada_sp { get; set; }

        public decimal? pcs_manu_perdaparada_cp { get; set; }

        public decimal? pcs_manu_perdaparada_sp { get; set; }

        public virtual dw_consol dw_consol { get; set; }

        public virtual dw_t_parada dw_t_parada { get; set; }
    }
}
