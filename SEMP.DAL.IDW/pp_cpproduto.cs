namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_cpproduto
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pp_cpproduto()
        {
            pp_cp_data = new HashSet<pp_cp_data>();
        }

        [Key]
        public long id_cpproduto { get; set; }

        public long? id_produto { get; set; }

        public long id_cp { get; set; }

        [StringLength(40)]
        public string nr_doc { get; set; }

        public long? pcs_producaoPlanejada { get; set; }

        public long? pcs_producaoBruta { get; set; }

        public decimal? pcs_producaorefugada { get; set; }

        public virtual om_produto om_produto { get; set; }

        public virtual pp_cp pp_cp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cp_data> pp_cp_data { get; set; }
    }
}
