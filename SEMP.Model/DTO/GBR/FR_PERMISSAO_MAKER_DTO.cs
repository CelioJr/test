using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_PERMISSAO_MAKER_DTO
    {
        public int PMK_CODIGO { get; set; }
        public int GRP_CODIGO { get; set; }
        public string PMK_EDITAR { get; set; }
        public string PMK_EXCLUIR { get; set; }
        public Nullable<int> FRM_CODIGO { get; set; }
        public Nullable<int> REL_CODIGO { get; set; }
        public Nullable<int> REG_COD { get; set; }
    }
}
