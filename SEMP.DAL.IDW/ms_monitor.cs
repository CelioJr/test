namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ms_monitor
    {
        [Key]
        public long id_monitor { get; set; }

        [StringLength(512)]
        public string ds_item_monitorado { get; set; }

        public DateTime? dthr_inicio { get; set; }

        public DateTime? dthr_fim { get; set; }

        public int? memI_free_embyte { get; set; }

        public int? memF_free_embyte { get; set; }

        public int? mili_duracao { get; set; }

        public int? mem_consumida { get; set; }

        public long? id_evt { get; set; }

        public virtual ms_evt ms_evt { get; set; }
    }
}
