﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
    public class RelatorioInspecaoIACLinhasVO
    {
        public string Descricao { get; set; }
        public int CodLinha { get; set; }

        public int TotalInspecionado { get; set; }
        public int TotalInspecionadoAcumulado { get; set; }
        public int TotalDefeito { get; set; }
        public int PPMAcumulado { get; set; }

        public List<RelatorioInspecaoIACPostos> Postos { get; set; }

        public List<RelatorioInspecaoIACHorarios> HorasLinha { get; set; }

        public List<RelatorioInspecaoIACModeloLinha> ModelosLinha { get; set; }
    }
}
