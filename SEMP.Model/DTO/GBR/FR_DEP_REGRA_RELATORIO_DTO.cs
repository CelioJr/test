using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DEP_REGRA_RELATORIO_DTO
    {
        public int REG_COD { get; set; }
        public int REL_CODIGO { get; set; }
    }
}
