using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBLinhaMontagemMap : EntityTypeConfiguration<tbl_CBLinhaMontagem>
    {
        public tbl_CBLinhaMontagemMap()
        {
            // Primary Key
            this.HasKey(t => t.codLinha);

            // Properties
            this.Property(t => t.codLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.tipo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.status)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.flgAtivo)
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("tbl_CBLinhaMontagem");
            this.Property(t => t.codLinha).HasColumnName("codLinha");
            this.Property(t => t.DatVigenciaIni).HasColumnName("DatVigenciaIni");
            this.Property(t => t.DatVigenciaFim).HasColumnName("DatVigenciaFim");
            this.Property(t => t.numero).HasColumnName("numero");
            this.Property(t => t.tipo).HasColumnName("tipo");
            this.Property(t => t.producao_horaria).HasColumnName("producao_horaria");
            this.Property(t => t.status).HasColumnName("status");
            this.Property(t => t.inicio_producao).HasColumnName("inicio_producao");
            this.Property(t => t.Dia_inicio_producao).HasColumnName("Dia_inicio_producao");
            this.Property(t => t.Dia_Intervalo_1_Inicio).HasColumnName("Dia_Intervalo_1_Inicio");
            this.Property(t => t.Dia_Intervalo_1_Fim).HasColumnName("Dia_Intervalo_1_Fim");
            this.Property(t => t.Dia_Intervalo_2_Inicio).HasColumnName("Dia_Intervalo_2_Inicio");
            this.Property(t => t.Dia_Intervalo_2_Fim).HasColumnName("Dia_Intervalo_2_Fim");
            this.Property(t => t.Dia_Intervalo_3_Inicio).HasColumnName("Dia_Intervalo_3_Inicio");
            this.Property(t => t.Dia_Intervalo_3_Fim).HasColumnName("Dia_Intervalo_3_Fim");
            this.Property(t => t.Dia_Fim_producao).HasColumnName("Dia_Fim_producao");
            this.Property(t => t.Noite_inicio_producao).HasColumnName("Noite_inicio_producao");
            this.Property(t => t.Noite_Intervalo_1_Inicio).HasColumnName("Noite_Intervalo_1_Inicio");
            this.Property(t => t.Noite_Intervalo_1_Fim).HasColumnName("Noite_Intervalo_1_Fim");
            this.Property(t => t.Noite_Intervalo_2_Inicio).HasColumnName("Noite_Intervalo_2_Inicio");
            this.Property(t => t.Noite_Intervalo_2_Fim).HasColumnName("Noite_Intervalo_2_Fim");
            this.Property(t => t.Noite_Intervalo_3_Inicio).HasColumnName("Noite_Intervalo_3_Inicio");
            this.Property(t => t.Noite_Intervalo_3_Fim).HasColumnName("Noite_Intervalo_3_Fim");
            this.Property(t => t.Noite_Fim_producao).HasColumnName("Noite_Fim_producao");
            this.Property(t => t.tempo_ciclo).HasColumnName("tempo_ciclo");
            this.Property(t => t.tempo_aviso_sonoro).HasColumnName("tempo_aviso_sonoro");
            this.Property(t => t.QtdPessoas).HasColumnName("QtdPessoas");
            this.Property(t => t.Eficiencia).HasColumnName("Eficiencia");
            this.Property(t => t.flgAtivo).HasColumnName("flgAtivo");
        }
    }
}
