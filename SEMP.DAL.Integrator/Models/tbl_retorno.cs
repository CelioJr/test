namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_retorno
    {
        public int ID { get; set; }

        [Required]
        [StringLength(16)]
        public string PACKNO { get; set; }

        public int EXTROW { get; set; }

        public DateTime DATAC { get; set; }

        public int STATUS { get; set; }

        public string MSG { get; set; }
    }
}
