using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_OPERADOR_DTO
    {
        public int OPDR_CODIGO { get; set; }
        public string OPDR_NOME { get; set; }
        public string OPDR_TIPO { get; set; }
        public string OPDR_PARAMETROS { get; set; }
    }
}
