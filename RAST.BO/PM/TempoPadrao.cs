﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.PM;
using SEMP.Model.DTO;

namespace RAST.BO.PM
{
	public class TempoPadrao
	{
		public static List<TempoPadraoDTO> ObterModelos()
		{
			return daoTempoPadrao.ObterModelos();
		}

		public static TempoPadraoDTO ObterTempoPadrao(string codModelo)
		{
			return daoTempoPadrao.ObterTempoPadrao(codModelo);
		}

		public static TempoPadraoDTO ObterTempoPadraoIDW(string codModelo)
		{
			return daoTempoPadrao.ObterTempoPadraoIDW(codModelo);
		}

		public static void GravarTempoPadrao(TempoPadraoDTO tempoPadrao)
		{
			daoTempoPadrao.GravarTempoPadrao(tempoPadrao);
		}

		public static void RemoverTempoPadrao(string codModelo)
		{
			daoTempoPadrao.RemoverTempoPadrao(codModelo);
		}

		public static List<TempoPadraoDTO> ObterItensModelo(string codModelo)
		{
			return daoTempoPadrao.ObterItensModelo(codModelo);
		}

	}
}
