using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEMP.DAL.Models
{
	public partial class tbl_CapacLinha
	{
		public System.DateTime DatProducao { get; set; }
		public string CodLinha { get; set; }
		public string CodModelo { get; set; }
		public Nullable<decimal> QtdCapacLinha { get; set; }
		public Nullable<decimal> QtdCapacMaxima { get; set; }
		public string Usuario { get; set; }
		public System.Guid rowguid { get; set; }
		public int NumTurno { get; set; }
	}
}
