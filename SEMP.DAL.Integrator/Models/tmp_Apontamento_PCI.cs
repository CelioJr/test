namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tmp_Apontamento_PCI
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(16)]
        public string PACKNO { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        public string EXTROW { get; set; }

        [StringLength(12)]
        public string AUFNR { get; set; }

        public decimal? YIELD { get; set; }

        [StringLength(3)]
        public string MEINS { get; set; }

        public DateTime? DatEnvio { get; set; }
    }
}
