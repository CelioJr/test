namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_testesubetapa
    {
        [Key]
        public long id_testesubetapa { get; set; }

        public decimal? meta { get; set; }

        public short? st { get; set; }

        public long id_subparam { get; set; }

        public decimal? minimo { get; set; }

        public decimal? maximo { get; set; }

        public long? id_testesub { get; set; }

        public virtual dw_ft_subparam dw_ft_subparam { get; set; }

        public virtual dw_testesub dw_testesub { get; set; }
    }
}
