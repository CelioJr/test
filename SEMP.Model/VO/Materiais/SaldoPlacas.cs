using SEMP.Model.DTO;
using System;
using System.Collections.Generic;

namespace SEMP.Model.VO.Materiais
{
    public partial class SaldoPlacas
    {
        public string CodFab { get; set; }
        public string CodItem { get; set; }
        public string CodLocal { get; set; }
        public decimal? QtdEstoque { get; set; }
        public DateTime? DatAtualizacao { get; set; }

		public string DesItem { get; set; }
		public decimal? TotalPrograma { get; set; }
		public decimal? TotalOP { get; set; }
		public decimal? TotalApontado { get; set; }

		public IEnumerable<SaldoPlacas> Modelos { get; set; }
		public IEnumerable<LSAAPDTO> OrdensProducao { get; set; }
	}
}
