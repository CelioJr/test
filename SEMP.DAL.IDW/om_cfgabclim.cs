namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_cfgabclim
    {
        [Key]
        public long id_cfgabclim { get; set; }

        public short? classe { get; set; }

        public decimal? limite_inf_classe { get; set; }

        public decimal? limite_sup_classe { get; set; }

        public long id_ind { get; set; }

        public long id_cfgabc { get; set; }

        public decimal? limite_inf { get; set; }

        public decimal? limite_sup { get; set; }

        public decimal? limite_inf_meta { get; set; }

        public decimal? limite_sup_meta { get; set; }

        [StringLength(11)]
        public string corClasse { get; set; }

        public virtual om_cfgabc om_cfgabc { get; set; }

        public virtual om_ind om_ind { get; set; }
    }
}
