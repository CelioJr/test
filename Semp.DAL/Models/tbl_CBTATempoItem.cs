using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBTATempoItem
    {
        public string CodModelo { get; set; }
        public string NumECB { get; set; }
        public string Fase { get; set; }
        public string SubFase { get; set; }
        public int CodLinha { get; set; }
        public Nullable<System.DateTime> DatEntrada { get; set; }
        public Nullable<System.DateTime> DatSaida { get; set; }
        public Nullable<int> TempoAtravessa { get; set; }
        public string Observacao { get; set; }
        public Nullable<int> QtdDefeito { get; set; }
        public Nullable<int> TempoCorrido { get; set; }
        public Nullable<int> TempoTecnico { get; set; }
    }
}
