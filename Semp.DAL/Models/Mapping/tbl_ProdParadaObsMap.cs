using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_ProdParadaObsMap : EntityTypeConfiguration<tbl_ProdParadaObs>
    {
        public tbl_ProdParadaObsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.DatParada, t.FlgTipo, t.rowguid });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.FlgTipo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_ProdParadaObs");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.DatParada).HasColumnName("DatParada");
            this.Property(t => t.FlgTipo).HasColumnName("FlgTipo");
            this.Property(t => t.DesObs).HasColumnName("DesObs");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
        }
    }
}
