﻿using SEMP.DAL.DAO.Mobilidade;
using SEMP.DAL.GBR;
using SEMP.Model.DTO;
using SEMP.Model.VO.Mobilidade;
using System;
using System.Collections.Generic;

namespace RAST.BO.Mobilidade
{
	public class GBR
	{
		public static List<ResumoProducaoGBR> ObterResumoGBR(DateTime datInicio, DateTime datFim) 
		{

			return daoGBR.ObterResumoGBR(datInicio, datFim);

		}

		public static List<GBR_CAD_LINHAS> ObterLinhas() 
		{
			return daoGBR.ObterLinhas();
		}

		public static GBR_CAD_LINHAS ObterLinha(int codLinha)
		{
			return daoGBR.ObterLinha(codLinha);
		}

		public static List<GBR_LIST_PACK_DTO> ObterPack(string packString) {
			return daoGBR.ObterPack(packString);

		}

		public static GBR_CAD_SMT_SERIAL_DTO ObterSerial(string numSerie)
		{
			return daoGBR.ObterSerial(numSerie);

		}
	}
}
