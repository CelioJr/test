namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_REPAIR_ACTION")]
    public partial class ALC_REPAIR_ACTION
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ALC_REPAIR_ACTION()
        {
            ALC_REPAIR = new HashSet<ALC_REPAIR>();
        }

        [Key]
        public int ID_ACTION { get; set; }

        [StringLength(30)]
        public string DESCRIPTION { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_REPAIR> ALC_REPAIR { get; set; }
    }
}
