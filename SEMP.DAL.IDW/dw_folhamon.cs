namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_folhamon
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_folhamon()
        {
            dw_folhamoncomp = new HashSet<dw_folhamoncomp>();
        }

        [Key]
        public long id_folhamon { get; set; }

        public long? id_folha { get; set; }

        public long id_produto { get; set; }

        public short? is_enviarembalagem { get; set; }

        public virtual dw_folha dw_folha { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhamoncomp> dw_folhamoncomp { get; set; }

        public virtual om_produto om_produto { get; set; }
    }
}
