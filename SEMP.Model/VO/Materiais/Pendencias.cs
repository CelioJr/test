﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Materiais
{
	public class Pendencias
	{
		public int NumRomaneio { get; set; }
		public string CodItem { get; set; }
		public string DesItem { get; set; }
		public decimal? QtdItem { get; set; }
		public string Status { get; set; }
		public DateTime DatGeracao { get; set; }
		public DateTime? DatAprovacao { get; set; }
		public DateTime? DatRecebimento { get; set; }
		public DateTime? DatPagamento { get; set; }
		public DateTime? DatLaudo { get; set; }
		public int? DifAprov { get; set; }
		public int? DifReceb { get; set; }
		public int? DifPagamento { get; set; }
		public int? DifGeracao { get; set; }
		public int? DifAtraso { get; set; }
		public decimal? QtdEstoque { get; set; }
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public string FlgCritico { get; set; }
	}
}
