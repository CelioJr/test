using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
    public class CBPassagemDTO
    {
		[Display(Name = "N�m. S�rie")]
        public string NumECB { get; set; }
        public int CodLinha { get; set; }
        public int NumPosto { get; set; }
		[Display(Name = "Data Leitura")]
		public System.DateTime DatEvento { get; set; }
		[Display(Name = "DRT")]
		public string CodDRT { get; set; }
		[Display(Name = "F�b.")]
		public string CodFab { get; set; }
		[Display(Name = "AP")]
		public string NumAP { get; set; }

		//outras propriedades
		[Display(Name = "Nome Operador")]
		public string NomeOperador { get; set; }
		[Display(Name = "Linha")]
		public string DescricaoLinha { get; set; }
		[Display(Name = "Posto")]
		public string DescricaoPosto { get; set; }
		[Display(Name = "Seq.")]
		public int NumSeq { get; set; }

		[Display(Name = "Defeitos")]
		public int QtdDefeitos { get; set; }
		[Display(Name = "Amarrado")]
		public int QtdAmarra { get; set; }
	}
}
