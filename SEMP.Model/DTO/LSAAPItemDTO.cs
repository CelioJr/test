using System;

namespace SEMP.Model.DTO
{
    public class LSAAPItemDTO
    {
        public string CodFab { get; set; }
        public string NumAP { get; set; }
        public string CodModelo { get; set; }
        public string CodItem { get; set; }
        public string CodItemAlt { get; set; }
        public string CodProcesso { get; set; }
        public string CodAplAlt { get; set; }
        public decimal QtdFrequencia { get; set; }
        public decimal QtdNecessaria { get; set; }
        public Nullable<decimal> QtdProduzida { get; set; }
        public Nullable<decimal> QtdEmpenho { get; set; }
        public Nullable<decimal> QtdEmpenho2 { get; set; }
        public Nullable<decimal> QtdSaldoProd { get; set; }
        public Nullable<decimal> QtdPaga { get; set; }
        public Nullable<decimal> QtdDevolucao { get; set; }
        public Nullable<decimal> QtdRejeito { get; set; }
        public string CodIdentificaItem { get; set; }
        public string CodIdentificaRej { get; set; }
        public string FlgItemOK { get; set; }
        public string FlgApontado { get; set; }
        public string NumNa { get; set; }
        public string CodProcesso2 { get; set; }
        public Nullable<decimal> QtdRecebida { get; set; }
        public Nullable<int> OrdConsumo { get; set; }
		public System.Guid rowguid { get; set; }
		public String NumOP { get; set; }

		public string DesModelo { get; set; }
        public string DesItem { get; set; }
        public string DesAlt { get; set; }


    }
}
