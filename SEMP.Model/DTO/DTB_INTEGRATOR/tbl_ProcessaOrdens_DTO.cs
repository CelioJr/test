namespace SEMP.Model.DTO
{
	using System;

	public partial class tbl_ProcessaOrdens_DTO
	{
        public int id { get; set; }

        public DateTime? datprocesso { get; set; }

        public bool? status { get; set; }
    }
}
