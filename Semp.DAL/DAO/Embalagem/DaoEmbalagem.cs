﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using SEMP.DAL.Models;
using SEMP.Model.DTO;
using SEMP.Model.VO.Embalagem;
using System.Data;
using SEMP.Model.VO;
using SEMP.Model;
using SEMP.DAL.DAO.Rastreabilidade;
using Newtonsoft.Json;
using SEMP.DAL.DTB_SSA.Models;

namespace SEMP.DAL.DAO.Embalagem
{
    /// <summary>
    /// Classe responsável por Atualizar e consultar informações do módulo embalagem 
    /// </summary>
    public static class DaoEmbalagem
	{
        //static DaoEmbalagem()
        //{
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<viw_STI_ModeloEtiqueta, viw_STI_ModeloEtiquetaDTO>();
        //        cfg.CreateMap<tbl_CBEmbalada, CBEmbaladaDTO>();
        //        cfg.CreateMap<tbl_CBLote, CBLoteDTO>();
        //        cfg.CreateMap<tbl_CBLoteConfig, CBLoteConfigDTO>();
        //        cfg.CreateMap<tbl_CBPesoEmbalagem, CBPesoEmbalagemDTO>();
        //        cfg.CreateMap<tbl_CBEtiqueta, CBEtiquetaDTO>();

        //    });
        //}

        private static string _licencaSoftware;
		private static string _licencaWindows;

		#region Notebook
		public static int Verificar_Serial_Filtro_Privacidade(String ne)
		{
			var resultado = (from prod in new DTB_SIMContext().tbl_ProdModelo
							 where prod.CodModelo == ne
							 select prod.NumProjeto);

			return (String.IsNullOrEmpty(resultado.ToString()) ? 0 : Convert.ToInt32(resultado));
		}

		public static bool Verificar_Serial_Filtro_Associado_Varios_NSs(String ne, String filtro, String dispositivoId)
		{
			var ctSsaContext = new DtbSsaContext();

			var qtde = (from m in ctSsaContext.TblMfgBuildD
						join key in ctSsaContext.TblKeyComponentD
							on m.Model_Number equals key.Model_Number
						where key.MFG_Part_Number == ne
						   && key.MFG_Serial_Number == filtro
						   && key.dispositivo_id == Convert.ToInt32(dispositivoId)
						   && m.Serial_Number == key.Serial_Number
						select m
				).Count();


			return (qtde > 0);
		}

		public static string Validar_Mac(String tipo, string numeroSerie)
		{
			var modelo = (tipo == "LAN_OnBoard" ? "Network Connection" : tipo == "Bluetooth" ? "Bluetooth" : tipo == "Wireless" ? "Wireless" : "");

			var dtbEng = new DtbEngContext();

			var resultado = (from p in dtbEng.Pc
							 join pr in dtbEng.PcRede on p.id equals pr.idPc
							 join r in dtbEng.Rede on pr.idRede equals r.id
							 where p.numeroDeSerie == numeroSerie
								&& (r.modelo.Contains(modelo))
							 select pr.macAddress);

			return resultado.ToString().Replace(":", "");
		}

		public static string Recuperar_Numero_Serie_Toshiba(String numeroSerie)
		{
			var resultado = (from hist in new DtbSsaContext().TblHistoricoToshiba
							 where hist.NumSerieSTI.Equals(numeroSerie)
							 select hist.NumSerieToshiba);

			return resultado.ToString();
		}

		public static KitAcessorio RecuperarDadosKitAcessorio(string strNumEcb)
		{
			var ctxSsaContext = new DtbSsaContext();

			var resultado = (from kit in ctxSsaContext.TblKitAcessorio
							 where kit.CodKit == Convert.ToInt32(strNumEcb)
							 select new KitAcessorio
							 {
								 CodKit = kit.CodKit,
								 NumSerie = kit.NumSerie,
								 CodOF = kit.CodOF,
								 SerialAdaptador = kit.SerialAdaptador,
								 NumLicencaNero = kit.NumLicencaNero
							 }).FirstOrDefault();

			return resultado;

		}

		public static string Verificar_NumeroSerie_Associado_kitEmbalagem(string numeroSerie)
		{
			var resultado = (from kit in new DtbSsaContext().TblKitAcessorio
							 where kit.NumSerie == numeroSerie
							 select new
							 {
								 kit.CodKit

							 }).FirstOrDefault();

			return (resultado == null ? "" : resultado.CodKit.ToString(CultureInfo.InvariantCulture));
		}

		public static string Validar_Imagem_Toshiba(int imgCode)
		{
			var resultado = (from img in new DtbSsaContext().TblSgiImagem
							 where img.imagem_id == imgCode
							 select img.codImagemToshiba).ToString();

			return resultado;
		}

		public static int Recuperar_Codigo_Imagem(string numSerie)
		{
			var resultado = (from vw in new DtbSsaContext().ViewNumSerie
							 where vw.DesSerie == numSerie
							 select new
							 {
								 vw.imagem_id
							 });

			return Convert.ToInt32(resultado);
		}

		public static int Recuperar_Codigo_Imagem_Toshiba(string numSerie)
		{
			var resultado = (from vw in new DtbSsaContext().ViewNumSerie
							 where vw.NumSerieToshiba == numSerie
							 select vw.imagem_id).FirstOrDefault();

			return Convert.ToInt32(resultado);
		}

		private static void Recuperar_Licenca_Software_Windows(string numSerie)
		{
			var resultado = (from vw in new DtbSsaContext().ViewNumSerie
							 where vw.NumSerieToshiba == numSerie
							 select new
							 {
								 vw.LicencaSoftware,
								 vw.LicencaWindows

							 }).FirstOrDefault();

			if (resultado != null)
			{
				_licencaSoftware = resultado.LicencaSoftware;
				_licencaWindows = resultado.LicencaWindows;
			}
			else
			{
				_licencaSoftware = "";
				_licencaWindows = "";
			}
		}

		public static bool Atualizar_tabela_dtbSSA_Notebook(CBEmbaladaDTO embalagem, IEnumerable<CBAmarraDTO> listaAmarra)
		{
			var dtbSsa = new DtbSsaContext();

			try
			{
				#region Atualizando Tabela Historico

				Recuperar_Licenca_Software_Windows(embalagem.NumECB);

				var resultado = (from hist in dtbSsa.TblHistorico
								 where hist.desSerie == embalagem.NumECB
								 select hist).FirstOrDefault();

				if (resultado != null)
				{
					resultado.dat6 = DateTime.Now;
					resultado.codOpe6 = Convert.ToInt32(embalagem.CodOperador);
					resultado.codAtual = 60; //codigo fixo no dtbSSA
					resultado.codPointControl = 5;
					resultado.firstDat6 = DateTime.Now;
					resultado.LicencaWindows = _licencaWindows;
					resultado.codLinhaEmbal = 1; //codigo fixo. até o sisap entrar essa linha não muda.
					resultado.LicencaSoftware = _licencaSoftware;
				}


				#endregion

				var leituraKit = listaAmarra.First(x => x.FlgTipo.Equals(10));  //recupera apenas o kit acessorio
				var numeroKit = Convert.ToInt32(Convert.ToDecimal(leituraKit.NumECB).ToString("D15").Substring(0, 5));

				#region Atualizando Tabela KitAcessorio
				var kitAcessorio = (from k in dtbSsa.TblKitAcessorio
									where k.CodKit.Equals(numeroKit)
									select k).FirstOrDefault();

				if (kitAcessorio != null)
				{
					kitAcessorio.NumSerie = leituraKit.NumSerie;
				}
				#endregion

				#region Atualizando Máquinas Toshiba

				int[] dispositivos = { 621, 53, 297, 83, 100, 676 };

				var res = (from nse in dtbSsa.ViewNumSerieEstrutura
						   join ht in dtbSsa.TblHistoricoToshiba on nse.DesSerie equals ht.NumSerieSTI
						   join mfg in dtbSsa.TblMfgBuildD on ht.NumSerieToshiba equals mfg.Serial_Number
						   where nse.DesSerie == embalagem.NumECB
								   && dispositivos.Contains(nse.Dispositivo_ID.Value)
						   select new Key_Component_dDTO
						   {
							   Model_Number = mfg.Model_Number,
							   Serial_Number = mfg.Serial_Number,
							   MFG_Part_Number = nse.NE,
							   MFG_Serial_Number = Retornar_MFG_Serial_Number(nse.Dispositivo_ID.Value),
							   Data_Registro = DateTime.Now,
							   cod_operador = Convert.ToInt32(embalagem.CodOperador)
						   }).FirstOrDefault();

				if (res != null)
				{
					var tblKey = new tbl_Key_Component_d
					{
						Model_Number = res.Model_Number,
						Serial_Number = res.Serial_Number,
						MFG_Part_Number = res.MFG_Part_Number,
						MFG_Serial_Number = res.MFG_Serial_Number,
						Data_Registro = res.Data_Registro,
						cod_operador = res.cod_operador
					};
					dtbSsa.TblKeyComponentD.Add(tblKey);
				}


				#endregion
			}
			catch (Exception)
			{
				return false;
			}

			dtbSsa.SaveChanges();
			return true;
		}

		private static string Retornar_MFG_Serial_Number(int valor)
		{
			switch (valor)
			{
				case 621:
					return "macwireless";
				case 53:
					return "maclanonboard";
				case 297:
					return "imagemTOshiba";
				case 83:
					return "licença windwos";
				case 100:
					return "serial adaptador";
				case 676:
					return "serial filtro privacidade";
			}
			return "";
		}

		public static void Update_KitAcessorio_DtbCB(int numKit, string numSerie, string numNero, bool salvar)
		{
			var dtbCb = new DTB_CBContext();

			var kitAcessorio = (from k in dtbCb.tbl_CBKitAcessorio
								where k.CodKit == numKit
								select k).FirstOrDefault();

			if (kitAcessorio != null)
			{
				kitAcessorio.NumSerie = numSerie;
				kitAcessorio.NumLicencaNero = numNero;
			}

			if (salvar) dtbCb.SaveChanges();
		}
		#endregion

		#region Tablet

		/// <summary>
		/// atualiza o banco dtb_ssa quando o produto é tablet
		/// </summary>
		/// <param name="embalagemDadosDto"></param>
		/// <returns></returns>
		public static bool Atualizar_tabela_dtbSSA_Tablet(CBEmbaladaDTO embalagemDadosDto)
		{
			var dtbSsa = new DtbSsaContext();

			var codOperador = Convert.ToInt32(embalagemDadosDto.CodOperador);

			try
			{
				var resultado = (from hist in dtbSsa.TblHistorico
								 where hist.desSerie.Equals(embalagemDadosDto.NumECB)
								 select hist).FirstOrDefault();

				if (resultado == null)
					return false;

				resultado.dat6 = DateTime.Now;
				resultado.codOpe6 = codOperador;
				resultado.codAtual = 60;
				resultado.codPointControl = 5;
				resultado.firstDat6 = DateTime.Now;
				resultado.codLinhaEmbal = 3;

				dtbSsa.SaveChanges();
			}
			catch (Exception)
			{
				return false;
			}

			return true;
		}

		#endregion

		#region Impressão

		/// <summary>
		/// Recupera os dados do modelo que serão usados na impressão das etiquetas
		/// </summary>
		/// <param name="codItem"></param>
		/// <returns></returns>
		public static viw_STI_ModeloEtiquetaDTO Recuperar_Dados_Modelo_Etiqueta(String codItem)
		{
			var dtbSim = new DtbSsaContext();

			var resultado = (from vw in dtbSim.viw_STI_ModeloEtiqueta
							 where vw.CodItem == codItem
							 select vw).FirstOrDefault();


			var lista = new viw_STI_ModeloEtiquetaDTO();

			AutoMapper.Mapper.Map(resultado, lista);

			return lista;
		}
		#endregion

		#region Caixa Coletiva
		/// <summary>
		/// Método que faz a abertura de uma nova caixa coletiva
		/// </summary>
		/// <param name="idLinha"></param>
		/// <param name="serial"></param>
		/// <param name="codModelo"></param>
		/// <returns></returns>
		public static string AbrirNovaCaixaColetiva(int idLinha, string codModelo, string codFab, string numAp)
		{
			string novaCC = "";
			string ret = "";
			string codLinha = idLinha.ToString("00");

			using (var dtbCb = new DTB_CBContext())
			{
				//recupera o setor, de acordo com a linha
				var setor = (from b in dtbCb.tbl_CBLinha where b.CodLinha == idLinha select b.Setor).FirstOrDefault();

				switch (setor)
				{
					case "IAC":
						setor = "1";
						break;
					case "IMC":
						setor = "2";
						break;
					case "FEC":
						setor = "3";
						break;
				}

				SqlParameter ParametroRetorno = new SqlParameter("Retorno", ret);
				ParametroRetorno.Direction = ParameterDirection.Output;

				SqlParameter[] parameters =
				{
					new SqlParameter("@idLinha", SqlDbType.VarChar) {Value= codLinha},
					new SqlParameter("@codModelo", SqlDbType.VarChar) {Value = codModelo},
					new SqlParameter("@setor", SqlDbType.VarChar) {Value = setor},
					new SqlParameter("@DataReferencia", SqlDbType.SmallDateTime) {Value = DateTime.Now},
					new SqlParameter("@CodFab", SqlDbType.VarChar) {Value = codFab},
					new SqlParameter("@NumAP", SqlDbType.VarChar) { Value = numAp},
					ParametroRetorno
				};

				SqlConnection con = new SqlConnection(dtbCb.Database.Connection.ConnectionString);
				con.Open();
				try
				{
					string cmd = "EXEC spc_CBAbrirCaixaColetiva @idLinha, @codModelo, @setor, @DataReferencia, @CodFab, @NumAP, @Retorno";
					SqlCommand oCom = new SqlCommand(cmd, con);

					foreach (SqlParameter param in parameters)
						oCom.Parameters.Add(param);

					DataSet oDs = new DataSet();

					using (SqlDataAdapter oDa = new SqlDataAdapter())
					{

						oDa.SelectCommand = oCom;

						oDa.Fill(oDs);
					}
					var result = "";

					if (oDs != null)
					{
						foreach (DataTable table in oDs.Tables)
						{
							foreach (DataColumn coluna in table.Columns)
							{
								if (coluna.ColumnName == "RESULT")
									result = table.Rows[0]["RESULT"].ToString();

								else if (coluna.ColumnName == "SECRESULT")
									novaCC = table.Rows[0]["SECRESULT"].ToString();
							}

						}
					}
				}
				catch (Exception ex)
				{
					throw new Exception("Erro ao abrir Caixa Coletiva", ex);
				}
				finally
				{
					con.Close();
				}
			}
			return novaCC;
		}

		/// <summary>
		/// Método que faz a abertura de um novo lote
		/// </summary>
		/// <param name="idLinha"></param>
		/// <param name="serial"></param>
		/// <param name="codModelo"></param>
		/// <returns></returns>
		public static string AbrirNovoLote(int idLinha, string codModelo, string codFab, string numAp)
		{
			string novoLote;
			string ret = "";
			string codlinha = idLinha.ToString("00");

			using (var dtbCb = new DTB_CBContext())
			{
				//recupera o setor, de acordo com a linha
				var setor = (from b in dtbCb.tbl_CBLinha where b.CodLinha == idLinha select b.Setor).FirstOrDefault();

				switch (setor)
				{
					case "IAC":
						setor = "1";
						break;
					case "IMC":
						setor = "2";
						break;
					case "FEC":
						setor = "3";
						break;
				}

				SqlParameter ParametroRetorno = new SqlParameter("Retorno", ret);
				ParametroRetorno.Direction = ParameterDirection.Output;



				var resultado = dtbCb.Database.SqlQuery<spc_CBFecOpenLote>(
					"EXEC spc_CBAbrirLote @idLinha,@codModelo, @setor, @DataReferencia, @CodFab, @NumAP, @Retorno",
					new SqlParameter("idLinha", codlinha),
					new SqlParameter("codModelo", codModelo),
					new SqlParameter("setor", setor),
					new SqlParameter("DataReferencia", DateTime.Now),
					new SqlParameter("CodFab", codFab),
					new SqlParameter("NumAP", numAp),
					ParametroRetorno
					);

				novoLote = resultado.First().RESULT;

				dtbCb.SaveChanges();
			}

			return novoLote;
		}

		/// <summary>
		/// Método que faz o fechamento da Caixa Coletiva atual
		/// </summary>
		/// <param name="numLote"></param>
		/// <param name="codDrt"></param>
		/// <param name="justificativa"></param>
		/// <returns></returns>
		public static bool FecharCaixaColetiva(string numLote, string codDrt, string justificativa)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				try
				{
					var res = dtbCb.Database.SqlQuery<spc_CBFecCloseLote>(
						"EXEC spc_CBFecharCaixaColetiva @numLote, @codDRT, @justificativa",
						new SqlParameter("numLote", numLote),
						new SqlParameter("codDRT", codDrt),
						new SqlParameter("justificativa", justificativa));

					var valor = res.First().Result;

					dtbCb.SaveChanges();
				}
				catch (Exception)
				{
					return false;
				}
			}

			return true;
		}

		/// <summary>
		/// Método que faz o fechamento do Lote atual
		/// </summary>
		/// <param name="numLote"></param>
		/// <param name="codDrt"></param>
		/// <param name="justificativa"></param>
		/// <returns></returns>
		public static bool FecharLote(string numLote, string codDrt, string justificativa)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				try
				{
					var res = dtbCb.Database.SqlQuery<spc_CBFecCloseLote>(
						"EXEC spc_CBFecharLote @numLote, @codDRT, @justificativa",
						new SqlParameter("numLote", numLote),
						new SqlParameter("codDRT", codDrt),
						new SqlParameter("justificativa", justificativa));

					var valor = res.First().Result;

					dtbCb.SaveChanges();
				}
				catch (Exception)
				{
					return false;
				}
			}

			return true;
		}

		/// <summary>
		/// Recupera os produtos associados a determinado lote em aberto
		/// </summary>
		/// <param name="numAp"></param>
		/// <param name="codModelo"></param>
		/// <param name="codLinha"></param>
		/// <param name="codFab"></param>
		/// <returns></returns>
		public static List<CBEmbaladaDTO> ProdutosDeLoteEmAberto(string numAp, string codModelo, int codLinha, string codFab)
		{
			var dtbCb = new DTB_CBContext();

			var resultado = (from emb in dtbCb.tbl_CBEmbalada
							 join lote in dtbCb.tbl_CBLote on emb.NumLote equals lote.NumLote
							 where lote.NumAP == numAp
								 && lote.CodModelo == codModelo
								 && lote.CodFab == codFab
								 && lote.NumLote.Substring(1, 2) == codLinha.ToString()
								 && lote.DatFechamento == null
								 && lote.NumLotePai == null
							 select emb);

			var lista = new List<CBEmbaladaDTO>();

			AutoMapper.Mapper.Map(resultado, lista);

			return lista;
		}

		/// <summary>
		/// Recuperar o número da caixa coletiva aberta para determinada linha
		/// </summary>
		/// <param name="numAp"></param>
		/// <param name="codModelo"></param>
		/// <param name="codLinha"></param>
		/// <param name="codFab"></param>
		/// <returns></returns>
		public static String RecuperarNumeroCaixaColetivaAberta(string numAp, int codLinha, string codFab)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				string codlinhaTemp = codLinha.ToString("00");

				var loteTemp = (from lote in dtbCb.tbl_CBLote
								where lote.NumAP == numAp
									&& lote.CodFab == codFab
									&& lote.NumLote.Trim().Substring(1, 2) == codlinhaTemp
									&& lote.DatFechamento == null
									&& lote.NumLotePai != null
								select lote).ToList().OrderByDescending(o => o.NumLote).FirstOrDefault();

				var numeroCC = (loteTemp == null ? "" : loteTemp.NumLote);

				return numeroCC;
			}
		}

		public static String RecuperarNumeroCaixaColetivaAberta(int codLinha)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				string codlinhaTemp = codLinha.ToString("00");

				var loteTemp = (from lote in dtbCb.tbl_CBLote
								where lote.NumLote.Trim().Substring(1, 2) == codlinhaTemp
									&& lote.DatFechamento == null
									&& lote.NumLotePai != null
								select lote).ToList().OrderByDescending(o => o.NumLote).FirstOrDefault();

				var numeroCC = (loteTemp == null ? "" : loteTemp.NumLote);

				return numeroCC;
			}
		}

		/// <summary>
		/// Procurar pelo numero da caixa coletiva aberta na linha com o mesmo modelo da AP atual
		/// </summary>
		/// <param name="codLinha">Código da Linha</param>
		/// <param name="codModelo">NE do Modelo</param>
		/// <returns>Número da Caixa coletiva</returns>
		public static String RecuperarNumeroCaixaColetivaAberta(int codLinha, string codModelo)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				string codlinhaTemp = codLinha.ToString("00");

				var loteTemp = (from lote in dtbCb.tbl_CBLote
								where lote.NumLote.Trim().Substring(1, 2) == codlinhaTemp
									&& lote.NumLote.Trim().Substring(7, 6) == codModelo
									&& lote.DatFechamento == null
									&& lote.NumLotePai != null
								select lote).ToList().OrderByDescending(o => o.NumLote).FirstOrDefault();

				var numeroCC = (loteTemp == null ? "" : loteTemp.NumLote);

				return numeroCC;
			}
		}

		public static bool AlterarAPLoteAberto(string numCC, string codFab, string numAP)
		{

			try
			{
				using (var dtbCb = new DTB_CBContext())
				{
					var numlotePai = dtbCb.tbl_CBLote.FirstOrDefault(x => x.NumLote == numCC).NumLotePai;

					var loteTemp = (from lote in dtbCb.tbl_CBLote
									where lote.NumLote == numCC || lote.NumLote == numlotePai
										&& lote.DatFechamento == null
										&& lote.NumLotePai != null
									select lote).ToList();

					if (loteTemp != null)
					{
						loteTemp.ForEach(x => { x.NumAP = numAP; x.CodFab = codFab; });
						dtbCb.SaveChanges();
						return true;
					}
					else
					{
						return false;

					}
				}
			}
			catch
			{
				return false;
			}

		}

		/// <summary>
		/// Rotina para verificar se o modelo da caixa coletiva é o mesmo de uma nova ap informada
		/// </summary>
		/// <param name="numCC">Número da caixa coletiva</param>
		/// <param name="codFab">Fábrica da AP</param>
		/// <param name="numAP">Número da AP</param>
		/// <returns></returns>
		public static bool VerificarModeloAPAbertaNovaAP(string numCC, string codFab, string numAP)
		{

			try
			{
				using (var dtbCb = new DTB_CBContext())
				using (var dtbSIM = new DTB_SIMContext())
				{
					// acha os dados da caixa coletiva
					var cc = dtbCb.tbl_CBLote.FirstOrDefault(x => x.NumLote == numCC);

					// modelo1 é a AP da caixa coletiva
					var modelo1 = dtbSIM.tbl_LSAAP.FirstOrDefault(x => x.CodFab == cc.CodFab && x.NumAP == cc.NumAP);
					// modelo2 é a AP informada no parâmetro
					var modelo2 = dtbSIM.tbl_LSAAP.FirstOrDefault(x => x.CodFab == codFab && x.NumAP == numAP);

					// se os dois objetos forem diferentes de nulo, ou seja, se encontrar as duas APs
					if (modelo1 != null && modelo2 != null)
					{
						// retorna a verificação de se o modelo é o mesmo pras duas APs
						return (modelo1.CodModelo == modelo2.CodModelo);
					}

					return false;
				}
			}
			catch
			{
				return false;
			}

		}

		public static String RecuperarNumeroCaixaColetivaAberta(string numAp, string modelo, int codLinha, string codFab)
		{
			var dtbCb = new DTB_CBContext();
			string codlinhaTemp = codLinha.ToString("00");

			var loteTemp = (from lote in dtbCb.tbl_CBLote
							where lote.NumAP == numAp
								&& lote.CodModelo == modelo
								&& lote.CodFab == codFab
								&& lote.NumLote.Trim().Substring(1, 2) == codlinhaTemp
								&& lote.DatFechamento == null
								&& lote.NumLotePai != null
							select lote).ToList().OrderByDescending(o => o.NumLote).FirstOrDefault();

			var numeroCC = (loteTemp == null ? "" : loteTemp.NumLote);

			return numeroCC;
		}


		public static String RecuperarNumeroCaixaColetivaAberta(string numAp, int codLinha, string codFab, string codModelo)
		{
			var dtbCb = new DTB_CBContext();
			string codlinhaTemp = codLinha.ToString("00");

			var loteTemp = (from lote in dtbCb.tbl_CBLote
							where lote.NumAP == numAp
								&& lote.CodFab == codFab
								&& lote.CodModelo == codModelo
								&& lote.NumLote.Trim().Substring(1, 2) == codlinhaTemp
								&& lote.DatFechamento == null
								&& lote.NumLotePai != null
							select lote).ToList().OrderByDescending(o => o.NumLote).FirstOrDefault();

			var numeroCC = (loteTemp == null ? "" : loteTemp.NumLote);

			return numeroCC;
		}


		public static String RecuperarNumeroLoteAberto(string codFab, string numAp, int codLinha)
		{
			var dtbCb = new DTB_CBContext();
			string codlinhaTemp = codLinha.ToString("00");

			var resultado = (from lote in dtbCb.tbl_CBLote
							 where lote.NumAP == numAp
								 && lote.CodFab == codFab
								 && lote.NumLote.Substring(1, 2) == codlinhaTemp
								 && lote.DatFechamento == null
								 && lote.NumLotePai == null
							 select lote).OrderByDescending(o => o.NumLote).FirstOrDefault();

			var numlote = (resultado == null ? "" : resultado.NumLote);

			return numlote;
		}

		public static String RecuperarNumeroLoteAberto(string numAp, string codModelo, int codLinha, string codFab)
		{
			var dtbCb = new DTB_CBContext();
			string codlinhaTemp = codLinha.ToString("00");

			var resultado = (from lote in dtbCb.tbl_CBLote
							 where lote.NumAP == numAp
								 && lote.CodModelo == codModelo
								 && lote.CodFab == codFab
								 && lote.NumLote.Substring(1, 2) == codlinhaTemp
								 && lote.DatFechamento == null
								 && lote.NumLotePai == null
							 select lote).OrderByDescending(o => o.NumLote).FirstOrDefault();

			var numlote = (resultado == null ? "" : resultado.NumLote);

			return numlote;
		}


		public static String RecuperarNumeroLoteAberto(string codModelo, int codLinha, string codFab)
		{
			//Alterado Por Joenne
			var dtbCb = new DTB_CBContext();
			string codlinhaTemp = codLinha.ToString("00");

			var resultado = (from lote in dtbCb.tbl_CBLote
							 where
								 lote.CodModelo == codModelo
								 && lote.CodFab == codFab
								 && lote.NumLote.Substring(1, 2) == codlinhaTemp
								 && lote.DatFechamento == null
								 && lote.NumLotePai == null
							 select lote).OrderByDescending(o => o.NumLote).FirstOrDefault();

			var numlote = (resultado == null ? "" : resultado.NumLote);

			return numlote;
		}

		/// <summary>
		/// Recupera os produtos que estão na caixa coletiva aberta para a linha
		/// </summary>
		/// <param name="numAp"></param>
		/// <param name="codModelo"></param>
		/// <param name="codLinha"></param>
		/// <param name="codFab"></param>
		/// <returns></returns>
		public static List<CBEmbaladaDTO> RecuperarItensEmbaladosDaCaixaColetivaAberta(string numAp, string codModelo, int codLinha, string codFab)
		{
			var dtbCb = new DTB_CBContext();

			var resultado = (from emb in dtbCb.tbl_CBEmbalada
							 join lote in dtbCb.tbl_CBLote on emb.NumLote equals lote.NumLote
							 where lote.NumAP == numAp
								 && lote.CodModelo == codModelo
								 && lote.CodFab == codFab
								 && lote.NumLote.Substring(1, 2) == codLinha.ToString()
								 && lote.DatFechamento == null
								 && lote.NumLotePai != null
							 select emb);

			var lista = new List<CBEmbaladaDTO>();
            
			AutoMapper.Mapper.Map(resultado, lista);

			return lista;
		}

		public static List<CBEmbaladaDTO> RecuperarItensEmbaladadosDaCaixaColetiva(string numCC)
		{
			var dtbCb = new DTB_CBContext();

			var resultado = (from emb in dtbCb.tbl_CBEmbalada where emb.NumCC == numCC select emb).ToList();

			var lista = new List<CBEmbaladaDTO>();
            
			AutoMapper.Mapper.Map(resultado, lista);

			return lista;
		}

		/// <summary>
		/// Recupera a quantidade de itens lidos em determinado lote
		/// </summary>
		/// <param name="numLote"></param>
		/// <returns></returns>
		public static int QtdeCaixasColetivasNoLote(string numLote)
		{
			int valor = 0;
			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				valor = contexto.tbl_CBLote.Count(x => x.NumLotePai == numLote && x.DatFechamento != null);
			}

			return valor;
		}

		/// <summary>
		/// Recupera a quantidade de itens embalados em determinada caixa coletiva
		/// </summary>
		/// <param name="numCC"></param>
		/// <returns></returns>
		public static int QuantidadeItensEmbaladosCaixaColetiva(string numCC)
		{
			int valor = 0;
			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				//valor = contexto.tbl_CBEmbalada.Count(x => x.NumCC == numCC);
				valor = contexto.spc_CBContaTotalCCEmbalada(numCC).FirstOrDefault();
			}

			return valor;
		}

		public static bool AlterarTamanhoCaixaColetiva(int codLinha, int tamanho)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				try
				{
					var linha = dtbCB.tbl_CBLoteConfig.FirstOrDefault(x => x.idLinha == codLinha);
					linha.QtdeCC = tamanho;
					dtbCB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}


		public static bool AlterarTamanhoLote(int codLinha, int tamanho)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				try
				{
					var linha = dtbCB.tbl_CBLoteConfig.FirstOrDefault(x => x.idLinha == codLinha);
					linha.Qtde = tamanho;
					dtbCB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}

		}


		public static CBLoteDTO RecuperarDadosLote(string numLote)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				try
				{
					var resultado = dtbCB.tbl_CBLote.FirstOrDefault(x => x.NumLote == numLote);

					var lote = new CBLoteDTO();

					AutoMapper.Mapper.Map(resultado, lote);

					return lote;
				}
				catch (Exception)
				{
					return null;
				}
			}
		}

		public static CBLoteConfigDTO RecuperarDadosCC(int codLinha)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				try
				{
					var resultado = dtbCB.tbl_CBLoteConfig.FirstOrDefault(x => x.idLinha == codLinha);

					var cc = new CBLoteConfigDTO();

					AutoMapper.Mapper.Map(resultado, cc);

					return cc;
				}
				catch (Exception)
				{
					return null;
				}
			}
		}



		public static CBLoteDTO RecuperarDadosCaixaColetiva(string numAp, string codModelo, int codLinha, string codFab)
		{
			var dtbCb = new DTB_CBContext();
			string codlinhaTemp = codLinha.ToString("00");

			var loteTemp = (from lote in dtbCb.tbl_CBLote
							where lote.NumAP == numAp
								&& lote.CodModelo == codModelo
								&& lote.CodFab == codFab
								&& lote.NumLote.Trim().Substring(1, 2) == codlinhaTemp
								&& lote.DatFechamento == null
								&& lote.NumLotePai != null
							select lote).FirstOrDefault();

			var tLote = new CBLoteDTO();

			AutoMapper.Mapper.Map(loteTemp, tLote);

			return tLote;
		}

		#endregion

		/// <summary>
		/// Atualiza o modo retrabalho para determinada linha/posto
		/// </summary>
		/// <param name="codLinha"></param>
		/// <param name="numPosto"></param>
		/// <param name="codOperador"></param>
		public static bool Atualizar_Modo_Retrabalho(int codLinha, int numPosto, int codOperador)
		{
			var status = Recuperar_Status_Retrabalho(codLinha, numPosto);

			var dt = new DTB_CBContext();

			if (status)
			{//está ativado, desativar
				var resultado = (from ret in dt.tbl_CBHistoricoRetPosto
								 where ret.codLinha == codLinha
									   && ret.numPosto == numPosto
									   && ret.DatAtivacao != null
									   && ret.DatDesativacao == null
								 select ret).FirstOrDefault();
				if (resultado != null)
				{
					resultado.DatDesativacao = DateTime.Now;
					resultado.CodOpeDesativacao = Convert.ToInt32(codOperador);
				}
			}
			else
			{
				dt.tbl_CBHistoricoRetPosto.Add(new tbl_CBHistoricoRetPosto()
				{
					codLinha = codLinha,
					CodOpeAtivacao = Convert.ToInt32(codOperador),
					CodOpeDesativacao = null,
					DatAtivacao = DateTime.Now,
					DatDesativacao = null,
					numPosto = numPosto
				});
			}
			dt.SaveChanges();

			return (!status);
		}

		/// <summary>
		/// Recupera o status do modo retrabalho para determinada linha/posto
		/// </summary>
		/// <param name="codLinha"></param>
		/// <param name="numPosto"></param>
		/// <returns></returns>
		public static bool Recuperar_Status_Retrabalho(int codLinha, int numPosto)
		{
			//se esta ativado, retorna true. caso contrário, false.
			var dt = new DTB_CBContext();
			var resultado = (from ret in dt.tbl_CBHistoricoRetPosto
							 where ret.codLinha == codLinha
								   && ret.numPosto == numPosto
								   && ret.DatAtivacao != null
								   && ret.DatDesativacao == null
							 select ret).FirstOrDefault();

			return resultado != null;
		}

		/// <summary>
		/// Insere registro na tbl_CBEmbalada
		/// </summary>
		/// <param name="embalada"></param>
		/// <param name="salvar"></param>
		/// <returns></returns>
		public static bool Inserir_Embalada(CBEmbaladaDTO embalada, bool salvar)
		{
			var dtbCb = new DTB_CBContext();

			try
			{
				var tabela = new tbl_CBEmbalada
				{
					CodFab = embalada.CodFab,
					CodLinha = embalada.CodLinha,
					CodLinhaFec = embalada.CodLinhaFec,
					CodModelo = embalada.CodModelo,
					CodOperador = embalada.CodOperador,
					CodTestador = embalada.CodTestador,
					DatLeitura = embalada.DatLeitura,
					DatReferencia = Convert.ToDateTime(embalada.DatReferencia),
					NumAP = embalada.NumAP,
					NumECB = embalada.NumECB,
					NumLote = embalada.NumLote,
					NumPosto = embalada.NumPosto,
					NumCC = embalada.NumCC
				};

				dtbCb.tbl_CBEmbalada.Add(tabela);

				dtbCb.SaveChanges();

				return true;
			}
			catch (Exception)
			{
				return false;
			}

		}

		/// <summary>
		/// Recupera a AP de determinado número de série
		/// </summary>
		/// <param name="numSerie"></param>
		/// <returns></returns>
		public static int Recuperar_AP_Serial(string numSerie)
		{
			var dtbSsa = new DtbSsaContext();

			var result =
				(from hist in dtbSsa.TblHistorico where hist.desSerie == numSerie select hist.codOF).FirstOrDefault();

			return result;

		}

		/// <summary>
		/// Verifica se determinado produto foi embalado
		/// </summary>
		/// <param name="serial"></param>
		/// <returns></returns>
		public static bool VerificaSeNumeroSerieExisteNaEmbalagem(string serial)
		{
			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				bool retorno = contexto.tbl_CBEmbalada.Any(a => a.NumECB == serial);
				return retorno;
			}
		}

		/// <summary>
		/// Chama procedure para gravação na tbl_barrasproducaoplaca
		/// </summary>
		/// <param name="codFabrica"></param>
		/// <param name="codLinha"></param>
		/// <param name="dataLeitura"></param>
		/// <param name="codDRT"></param>
		/// <param name="codModelo"></param>
		/// <param name="barcode"></param>
		/// <param name="numap"></param>
		/// <param name="turno"></param>
		/// <returns></returns>
		public static Mensagem SU_GravaSTA(string codFabrica, string codLinha, string dataLeitura, string codDRT, string codModelo, string barcode, string numap, int turno)
		{
			Mensagem retorno = new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "LIME", DesMensagem = "LEITURA OK. LEIA PRÓXIMO." };

			using (var dtbSIM = new DTB_SIMContext())
			{
				string comando = string.Empty;
				try
				{

					comando = string.Format("EXEC spc_BarrasPCIGravar {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}",
						codFabrica,
						codLinha,
						Convert.ToDateTime(dataLeitura),
						codDRT,
						"RASTREAB",
						codModelo,
						barcode,
						'S',
						"RASTREABILIDADE",
						"",
						numap,
						'N',
						turno);


					var resultado = dtbSIM.Database.SqlQuery<int>("EXEC spc_BarrasPCIGravar @CodFab, @CodLinha, @DatLeitura, @Operador, @Apontador, @CodModelo, @NumSerieModelo, @flgQuebra, @NomUsuario, @Motivo, @NumAP, @FlgOffLine, @Turno",
						new SqlParameter("CodFab", codFabrica),
						new SqlParameter("CodLinha", codLinha),
						new SqlParameter("DatLeitura", Convert.ToDateTime(dataLeitura)),
						new SqlParameter("Operador", codDRT),
						new SqlParameter("Apontador", "RASTREAB"),
						new SqlParameter("CodModelo", codModelo),
						new SqlParameter("NumSerieModelo", barcode),
						new SqlParameter("flgQuebra", 'S'),
						new SqlParameter("NomUsuario", "RASTREABILIDADE"),
						new SqlParameter("Motivo", ""),
						new SqlParameter("NumAP", numap),
						new SqlParameter("FlgOffline", 'N'),
						new SqlParameter("Turno", turno)
						).ToList<int>();

					if (resultado == null)
					{
						retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
						retorno.DesMensagem = "Erro executar spc_BarrasPCIGravar";
						return retorno;
					}
				}
				catch (Exception ex)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);

					retorno.DesMensagem = ex.Message;
					retorno.StackErro = comando;
				}
				finally
				{
					dtbSIM.Dispose();
				}
			}

			return retorno;
		}

		/// <summary>
		/// Valida leitura de produto na spc_BarrasValidaPCISerie
		/// </summary>
		/// <param name="codModelo"></param>
		/// <param name="barcode"></param>
		/// <param name="codLinha"></param>
		/// <param name="codFab"></param>
		/// <param name="ap"></param>
		/// <returns></returns>
		public static string SU_ValidaBarcodeAp(string codModelo, string barcode, string codLinha, string codFab, string ap)
		{
			string sSaida = "";

			using (var dtbSIM = new DTB_SIMContext())
			{
				try
				{
					var resultado =
						dtbSIM.Database.SqlQuery<spc_BarrasValidaPCISerie>
						("EXEC spc_BarrasValidaPCISerie @Codigo, @NumSerie, @flgTipo, @flgQuebra, @CodLinha, @CodFab, @NumAP, @FlgOffline",
							new SqlParameter("Codigo", codModelo),
							new SqlParameter("NumSerie", barcode),
							new SqlParameter("flgTipo", 'P'),
							new SqlParameter("flgQuebra", 'S'),
							new SqlParameter("CodLinha", codLinha),
							new SqlParameter("CodFab", codFab),
							new SqlParameter("NumAP", ap),
							new SqlParameter("FlgOffline", 'N')
							).ToList<spc_BarrasValidaPCISerie>();

					var query = resultado.FirstOrDefault();

					if (query != null)
						sSaida = query.DesStatus;

				}
				catch (Exception ex)
				{
					sSaida = ex.Message;
				}
				finally
				{
					dtbSIM.Dispose();
				}
			}

			return sSaida;

		}

		/// <summary>
		/// Método que recupera o número do cinescópio de determinado aparelho.
		/// O aparelho só pode ser embalado caso possua cinescópio associado
		/// </summary>
		/// <param name="numSerie"></param>
		/// <returns></returns>
		public static string RecuperarCodigoCinescopio(string numSerie)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				try
				{
					var cinescopio = dtbCB.tbl_CBAmarra.FirstOrDefault(x => x.NumSerie == numSerie && (x.FlgTipo == 3 || x.FlgTipo == 9));
					string retorno = "";

					if (cinescopio != null)
						retorno = String.Concat(cinescopio.CodItem, "|", cinescopio.NumECB);

					return retorno;
				}
				catch (Exception)
				{
					return "";
				}
			}
		}

		/// <summary>
		/// Executa a gravação do item na tabela tbl_barrasproducao, fazendo com que seja contado mais um produto como produzido
		/// </summary>
		/// <param name="embalagem"></param>
		/// <param name="listaAmarra"></param>
		/// <returns></returns>
		public static Mensagem SU_GravaFECSTA(CBEmbaladaDTO embalagem, List<CBAmarraDTO> listaAmarra)
		{
			Mensagem retorno = new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "LIME", DesMensagem = "LEITURA OK. LEIA PRÓXIMO." };

			string codcinescopio = "";
			string seriecin = "";
			string codcontrole = "";
			string seriecontrole = "";
			string codoutro = "";
			string serieoutro = "";
			string cinescopio = "";

			if (embalagem.Linha.Familia.Equals("LCD"))
			{
				cinescopio = RecuperarCodigoCinescopio(embalagem.NumECB);
				if (!String.IsNullOrEmpty(cinescopio))
				{
					string[] parseCin = cinescopio.Split('|');
					codcinescopio = parseCin[0];    //coditem
					seriecin = parseCin[1];         //NumECB
				}
			}

			foreach (CBAmarraDTO p in listaAmarra)
			{
				switch (p.FlgTipo)
				{
					case Constantes.AMARRA_KIT:
						codcontrole = p.CodItem;
						seriecontrole = p.NumECB.Substring(6);
						break;

					case Constantes.AMARRA_PEDESTAL:
						codoutro = p.CodItem;
						serieoutro = p.NumECB.Substring(6);
						break;
				}
			}

			using (var dtbSIM = new DTB_SIMContext())
			{
				string comando = "";
				try
				{

					comando = string.Format("EXEC spc_BarrasGravar {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}, {19}, {20}, {21}",
							embalagem.CodFab,
							embalagem.Linha.CodLinhaT1,
							DateTime.Now.ToString("yyyy-MM-dd"),
							embalagem.CodOperador,
							"RASTREAB",
							"RASTREAB",
							embalagem.NumECB.Substring(0, 6),
							embalagem.NumECB.Substring(8),
							embalagem.NumECB.Substring(6, 2),
							codcontrole,
							seriecontrole,
							codcinescopio,
							seriecin,
							"S",
							"RASTREABILIDADE",
							"",
							codoutro,
							serieoutro,
							embalagem.NumAP,
							"N",
							embalagem.NumLote,
							embalagem.Turno);


					dtbSIM.Database.ExecuteSqlCommand("EXEC spc_BarrasGravar @CodFab, @CodLinha, @DatLeitura, @Operador, @Testador, @Testador2, @CodModelo, @NumSerieModelo, @DigVerificador, @CodControle, @NumSerieControle, @CodCinescopio, @NumSerieCinescopio, @flgQuebra, @NomUsuario, @Motivo, @CodOutro, @NumSerieOutro, @NumAP, @FlgOffline, @NumLote, @Turno",
					 new SqlParameter("CodFab", embalagem.CodFab),
					 new SqlParameter("CodLinha", embalagem.Linha.CodLinhaT1),
					 new SqlParameter("DatLeitura", DateTime.Now.ToString("yyyy-MM-dd")),
					 new SqlParameter("Operador", embalagem.CodOperador),
					 new SqlParameter("Testador", "RASTREAB"),
					 new SqlParameter("Testador2", "RASTREAB"),
					 new SqlParameter("CodModelo", embalagem.NumECB.Substring(0, 6)),
					 new SqlParameter("NumSerieModelo", embalagem.NumECB.Substring(8)),
					 new SqlParameter("DigVerificador", embalagem.NumECB.Substring(6, 2)),
					 new SqlParameter("CodControle", codcontrole),
					 new SqlParameter("NumSerieControle", seriecontrole),
					 new SqlParameter("CodCinescopio", codcinescopio),
					 new SqlParameter("NumSerieCinescopio", seriecin),
					 new SqlParameter("flgQuebra", "S"),
					 new SqlParameter("NomUsuario", "RASTREABILIDADE"),
					 new SqlParameter("Motivo", ""),
					 new SqlParameter("CodOutro", codoutro),
					 new SqlParameter("NumSerieOutro", serieoutro),
					 new SqlParameter("NumAP", embalagem.NumAP),
					 new SqlParameter("FlgOffline", "N"),
					 new SqlParameter("NumLote", embalagem.NumLote),
					 new SqlParameter("Turno", embalagem.Turno)
					 );
				}
				catch (Exception ex)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);

					retorno.DesMensagem = ex.Message;
					retorno.StackErro = comando;
				}
				finally
				{
					dtbSIM.Dispose();
				}

				return retorno;
			}
		}

		/// <summary>
		/// Desfaz toda a operação de embalagem quando ocorre um erro.
		/// Deleta os registros que possivelmente foram inseridos nas tabelas tbl_CBAmarra, tbl_CBEmbalada, tbl_CBPassagem
		/// </summary>
		/// <param name="embalagem"></param>
		/// <param name="listaAmarra"></param>
		/// <returns></returns>
		public static bool DesfazerOperacoes(CBEmbaladaDTO embalagem, List<CBAmarraDTO> listaAmarra)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				try
				{
					var tbamarra = dtbCB.tbl_CBAmarra.Where(x => x.NumSerie == embalagem.NumECB && x.NumPosto == embalagem.NumPosto && x.CodLinha == embalagem.CodLinha);
					dtbCB.tbl_CBAmarra.RemoveRange(tbamarra);

					var tbpassagem = dtbCB.tbl_CBPassagem.Where(x => x.NumECB == embalagem.NumECB && x.CodLinha == embalagem.CodLinha && x.NumPosto == embalagem.NumPosto);
					dtbCB.tbl_CBPassagem.RemoveRange(tbpassagem);

					var tbemb = dtbCB.tbl_CBEmbalada.Where(x => x.NumECB == embalagem.NumECB && x.CodLinha == embalagem.CodLinha && x.NumPosto == embalagem.NumPosto);
					dtbCB.tbl_CBEmbalada.RemoveRange(tbemb);

					dtbCB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}

			}
		}

		/// <summary>
		/// Recupera os dados do posto de amarração quando do produto 
		/// </summary>
		/// <param name="numSerie"></param>
		/// <returns></returns>
		public static List<tbl_CBAmarra> RecuperarDadosAmarracaoProduto(string numSerie)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				try
				{//== numSerie && (x.FlgTipo == Constantes.AMARRA_PAINEL || x.FlgTipo == Constantes.AMARRA_PAINEL_LCD)
					var tabela = dtbCB.tbl_CBAmarra.Where(x => x.NumSerie.Equals(numSerie)).ToList();
					return tabela;
				}
				catch (Exception)
				{
					return null;
				}
			}

			#region atualizar assim que Deus permitir 
			/*Atualizar assim que puder
			 
			   public static List<ClassesAuxiliaresEmbalagem.ResultadoAmarraFamilia> RecuperarDadosAmarracaoProduto (string numSerie)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				try
				{
					//var tabela = dtbCB.tbl_CBAmarra.Where(x => x.NumSerie.Equals(numSerie)).ToList();
					//return tabela;


					var teste = (from amarra in dtbCB.tbl_CBAmarra
								 join tipo in dtbCB.tbl_CBTipoAmarra on amarra.FlgTipo equals tipo.CodTipoAmarra
								 where amarra.NumSerie.Equals(numSerie)
								 select new ClassesAuxiliaresEmbalagem.ResultadoAmarraFamilia { 
										NumSerie = amarra.NumSerie,
										NumECB = amarra.NumECB,
										CodModelo = amarra.CodModelo,
										FlgTipo  = amarra.FlgTipo,
										DatAmarra = amarra.DatAmarra,
										CodLinha  = amarra.CodLinha,
										NumPosto = amarra.NumPosto,
										CodFab = amarra.CodFab,
										NumAP  = amarra.NumAP,
										CodItem = amarra.CodItem,
										CodFam = tipo.CodFam
								 }).ToList();

					return teste;
				}
				catch (Exception)
				{
					return null;
				}
			}
		}
			 
			 
			 */

			#endregion


		}

		/// <summary>
		/// Recupera a AP usada no posto de amarração quando o produto atual amarrado
		/// </summary>
		/// <param name="numSerie"></param>
		/// <returns></returns>
		//public static string RecuperarAPPainelAssociado (string numSerie)
		//{
		//    using (var dtbCB = new DTB_CBContext())
		//    {
		//        try
		//        {
		//            var AP = dtbCB.tbl_CBAmarra.FirstOrDefault(x => x.NumSerie == numSerie && (x.FlgTipo == Constantes.AMARRA_PAINEL || x.FlgTipo == Constantes.AMARRA_PAINEL_LCD)).NumAP;
		//            return AP;
		//        }
		//        catch(Exception)
		//        {
		//            return "";
		//        }
		//    }
		//}

		//public static string RecuperarNEPainelAssociado (string numSerie)
		//{
		//    using (var dtbCB = new DTB_CBContext())
		//    {
		//        try
		//        {
		//            var NE = dtbCB.tbl_CBAmarra.FirstOrDefault(x => x.NumSerie == numSerie && (x.FlgTipo == Constantes.AMARRA_PAINEL || x.FlgTipo == Constantes.AMARRA_PAINEL_LCD)).CodItem;
		//            return NE;
		//        }
		//        catch (Exception)
		//        {
		//            return "";
		//        }
		//    }
		//}         

		public static List<ClassesAuxiliaresEmbalagem.DadosHoraHora> SU_GetHoraHoraFEC(DateTime data, string codLinha)
		{
			using (var dtbSIM = new DTB_SIMContext())
			{
				try
				{
					var resultado = dtbSIM.Database.SqlQuery<ClassesAuxiliaresEmbalagem.DadosHoraHora>("EXEC spc_BarrasProducaoHora @Data, @flgMes, @CodLinha",
								new SqlParameter("Data", data.ToString("yyyy-MM-dd")),
								new SqlParameter("flgMes", 'S'),
								new SqlParameter("CodLinha", codLinha)
								).ToList<ClassesAuxiliaresEmbalagem.DadosHoraHora>();

					if (resultado != null && resultado.Count > 0)
						return resultado;

				}
				catch (Exception)
				{

				}
			}

			return null;
		}

		public static List<ClassesAuxiliaresEmbalagem.DadosHoraHora> SU_GetPlacaHoraHora(DateTime data, string codLinha)
		{

			using (var dtbSIM = new DTB_SIMContext())
			{
				try
				{
					var resultado = dtbSIM.Database.SqlQuery<ClassesAuxiliaresEmbalagem.DadosHoraHora>("EXEC spc_BarrasProducaoPlacaHora @Data, @flgMes, @CodLinha",
								new SqlParameter("Data", data.ToString("yyyy-MM-dd")),
								new SqlParameter("flgMes", 'S'),
								new SqlParameter("CodLinha", codLinha)
								).ToList<ClassesAuxiliaresEmbalagem.DadosHoraHora>();

					if (resultado != null && resultado.Count > 0)
						return resultado;

				}
				catch (Exception)
				{

				}
			}

			return null;
		}

		public static CBPesoEmbalagemDTO RecuperarPesoEmbalagem(string codModelo)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				try
				{
					var PesoEmbalagem = dtbCB.tbl_CBPesoEmbalagem.FirstOrDefault(x => x.Modelo == codModelo);

					var tPesoEmbalagem = new CBPesoEmbalagemDTO();

					AutoMapper.Mapper.Map(PesoEmbalagem, tPesoEmbalagem);

					return tPesoEmbalagem;

				}
				catch (Exception)
				{
					return null;
				}
			}
		}

		public static List<CBEtiquetaDTO> RecuperarEtiquetasPorPosto(int codlinha, int numPosto, bool ativo)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				try
				{
					var Etiquetas = dtbCB.tbl_CBEtiqueta.Where(x => x.CodLinha == codlinha && x.NumPosto == numPosto && x.flgAtivo == ativo).ToList();

					var tEtiquetas = new List<CBEtiquetaDTO>();

					AutoMapper.Mapper.Map(Etiquetas, tEtiquetas);

					return tEtiquetas;

				}
				catch (Exception)
				{
					return null;
				}

			}

		}

		public static bool AtivarImpressaoDeEtiqueta(int codLinha, int numPosto, string nomeEtiqueta)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				try
				{
					var linha = dtbCB.tbl_CBEtiqueta.FirstOrDefault(x => x.CodLinha == codLinha && x.NumPosto == numPosto && x.NomeEtiqueta == nomeEtiqueta);
					if (linha != null)
						linha.flgAtivo = (linha.flgAtivo == true ? false : true);

					dtbCB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		/// <summary>
		/// Utiliza a procedure para fazer a embalagem do produto : DEPRECATED
		/// Utilizar Procedure_Embalar_Amarrar_Produto
		/// </summary>
		/// <param name="embalada"></param>
		/// <returns></returns>
		public static ClassesAuxiliaresEmbalagem.ResultadoEmbalagemProduto Exec_Procedure_EmbalarProduto(CBEmbaladaDTO embalada)
		{
			ClassesAuxiliaresEmbalagem.ResultadoEmbalagemProduto resultado1 = new ClassesAuxiliaresEmbalagem.ResultadoEmbalagemProduto();

			using (var dtbCB = new DTB_CBContext())
			using (SqlConnection con = new SqlConnection(dtbCB.Database.Connection.ConnectionString))
			{

				try
				{
					var dataReferencia = Convert.ToDateTime(embalada.DatReferencia);
					var dataLeitura = Convert.ToDateTime(embalada.DatLeitura);

					embalada.CodLinhaFec = "";
					embalada.CodTestador = "";

					SqlParameter[] parameters =
					{
						new SqlParameter("CODFAB", SqlDbType.VarChar)  {Value = embalada.CodFab},
						new SqlParameter("CODLINHA", SqlDbType.Int) {Value = embalada.CodLinha},
						new SqlParameter("CODLINHAFEC", SqlDbType.VarChar) {Value = embalada.CodLinhaFec},
						new SqlParameter("CODMODELO", SqlDbType.VarChar) { Value = embalada.CodModelo} ,
						new SqlParameter("CODOPERADOR",SqlDbType.VarChar) { Value = embalada.CodOperador},
						new SqlParameter("CODTESTADOR",SqlDbType.VarChar) { Value = embalada.CodTestador},
						new SqlParameter("NUMAP", SqlDbType.VarChar ) { Value = embalada.NumAP, IsNullable = true},
						new SqlParameter("NUMECB", SqlDbType.VarChar) { Value = embalada.NumECB},
						new SqlParameter("NUMPOSTO", SqlDbType.Int) { Value = embalada.NumPosto}
					};


					con.Open();

					string cmd = "EXEC SPC_CBEMBALARPRODUTO @CODFAB, @CODLINHA, @CODLINHAFEC, @CODMODELO, @CODOPERADOR, @CODTESTADOR, @NUMAP, @NUMECB, @NUMPOSTO";
					SqlCommand oCom = new SqlCommand(cmd, con);
					oCom.Parameters.Clear();

					foreach (SqlParameter param in parameters)
						oCom.Parameters.Add(param);

					DataSet oDs = new DataSet();

					using (SqlDataAdapter oDa = new SqlDataAdapter())
					{
						oDa.SelectCommand = oCom;
						oDa.Fill(oDs);
					}

					if (oDs != null)
					{
						foreach (DataTable table in oDs.Tables)
						{
							if (table.Columns.Count > 2)
							{
								resultado1.numeroCaixaColetiva = table.Rows[0]["numeroCaixaColetiva"].ToString();
								resultado1.numeroLote = table.Rows[0]["numeroLote"].ToString();
								resultado1.qtd_atual_cc = Convert.ToInt32(table.Rows[0]["qtd_atual_cc"]);
								resultado1.qtd_padrao_cc = Convert.ToInt32(table.Rows[0]["qtd_padrao_cc"]);
								resultado1.qtd_atual_lote = Convert.ToInt32(table.Rows[0]["qtd_atual_lote"]);
								resultado1.qtd_padrao_lote = Convert.ToInt32(table.Rows[0]["qtd_padrao_lote"]);
							}
						}
					}

					return resultado1;
				}
				catch (Exception ex)
				{
					throw new Exception(ex.Message, ex.InnerException);
				}
				finally
				{
					//con.Close();                
				}
			}
		}

		public static string RecuperarCCProdutoEmbalado(string numSerie)
		{
			var dtbCb = new DTB_CBContext();

			//recuperar apenas o se a caixa coletiva estiver fechada
			var ListaCC = (from emb in dtbCb.tbl_CBEmbalada
						   join lote in dtbCb.tbl_CBLote on emb.NumCC equals lote.NumLote
						   where lote.DatFechamento != null
							  && emb.NumECB == numSerie
						   select emb.NumCC).ToList();

			var resultado = (from lote in dtbCb.tbl_CBLote
							 where ListaCC.Contains(lote.NumLote)
							 select lote).ToList().OrderByDescending(c => c.DatFechamento).FirstOrDefault().NumLote;

			return (string.IsNullOrEmpty(resultado) ? "" : resultado);
		}

		/// <summary>
		/// Utiliza a procedure para embalar, amarrar e fazer a passagem dos produtos
		/// </summary>
		/// <param name="embalagem"></param>
		/// <param name="listaAmarra"></param>
		/// <returns></returns>
		public static ClassesAuxiliaresEmbalagem.ResultProcEmbAmarraProd Procedure_Embalar_Amarrar_Produto(CBEmbaladaDTO embalagem, List<CBAmarraDTO> listaAmarra)
		{
			string jsonAmarra = JsonConvert.SerializeObject(listaAmarra.Select(s => new
			{
				NumSerie = s.NumSerie,
				NumECB = s.NumECB,
				CodModelo = s.CodModelo,
				CodItem = s.CodItem,
				CodLinha = s.CodLinha,
				CodFab = s.CodFab,
				NumAP = s.NumAP,
				NumPosto = s.NumPosto,
				FlgTipo = s.FlgTipo
			}).ToArray());

			if (listaAmarra.Count == 0)
				jsonAmarra = string.Empty;

			List<CBEmbaladaDTO> lista = new List<CBEmbaladaDTO>();
			lista.Add(embalagem);

			string jsonEmbalar = JsonConvert.SerializeObject(lista.Select(s => new
			{
				CodFab = s.CodFab,
				CodLinha = s.CodLinha,
				CodLinhaFEc = s.CodLinhaFec,
				CodModelo = s.CodModelo,
				CodOperador = s.CodOperador,
				CodTestador = s.CodTestador,
				NumAP = s.NumAP,
				NumECB = s.NumECB,
				NumPosto = s.NumPosto
			}).ToArray());

			using (var contexto = new DTB_CBContext())
			{
				var resultado = contexto.Database.SqlQuery<ClassesAuxiliaresEmbalagem.ResultProcEmbAmarraProd>(
							"EXEC spc_CBEmbalagem_Amarracao @CodDRT, @JSONAMARRA, @JSONEMB",
							new SqlParameter("CodDRT", lista[0].CodOperador),
							new SqlParameter("JSONAMARRA", jsonAmarra),
							new SqlParameter("JSONEMB", jsonEmbalar)
							);

				contexto.SaveChanges();

				var retornoProc = resultado.FirstOrDefault();

				return retornoProc;
			}
		}

		public static bool BloquearPosto(int codLinha, int codPosto, string NovoStatus)
		{
			var dtbCb = new DTB_CBContext();

			try
			{
				var posto = (from p in dtbCb.tbl_CBPosto
							 where p.CodLinha == codLinha && p.NumPosto == codPosto
							 select p).FirstOrDefault();

				if (posto != null)
				{
					posto.PostoBloqueado = NovoStatus;
				}
				dtbCb.SaveChanges();

				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public static DateTime? RecuperarHoraPassagem(CBPassagemDTO passagem)
		{

			var dtbCb = new DTB_CBContext();

			try
			{
				var posto = (from p in dtbCb.tbl_CBPassagem
							 where p.CodLinha == passagem.CodLinha && p.NumPosto == passagem.NumPosto && p.NumECB == passagem.NumECB
							 select p).FirstOrDefault();


				if (posto != null) return posto.DatEvento;
			}
			catch (Exception)
			{
				return null;
			}
			return null;
		}

		public static bool ApontarProducao(string codFab, string numAP, string codLocalDeb, int qtdProduzida, string codTraInsumo, DateTime datTra, string codTraModelo,
											string codModelo, string codLocalCre, string reprocessa, string baixaCM, string rotina, string usuario,
											string codCin, DateTime datLancamento, int turno)
		{

			using (var dtbSIM = new DTB_SIMContext())
			{
				try
				{
					var resultado = dtbSIM.Database.SqlQuery<ClassesAuxiliaresEmbalagem.RetornoApontamento>("EXEC spc_EST417_3 @P_NUMAP, @P_LOCDEBITEM, @P_QTDPRODUZIDA, @P_TRAINSUMO, @P_DATTRA, @P_TRAMODELO, @P_CODMODELO, @P_LOCCREMOD, @P_REPROCESSA, @P_BAIXACM, @P_ROTINA, @P_USUARIO, @P_CODCIN, @P_DATLANCA, @P_TURNO",
								new SqlParameter("P_CODFAB", codFab),
								new SqlParameter("P_NUMAP", numAP),
								new SqlParameter("P_LOCDEBITEM", codLocalDeb),
								new SqlParameter("P_QTDPRODUZIDA", qtdProduzida),
								new SqlParameter("P_TRAINSUMO", codTraInsumo),
								new SqlParameter("P_DATTRA", datTra),
								new SqlParameter("P_TRAMODELO", codTraModelo),
								new SqlParameter("P_CODMODELO", codModelo),
								new SqlParameter("P_LOCCREMOD", codLocalCre),
								new SqlParameter("P_REPROCESSA", reprocessa),
								new SqlParameter("P_BAIXACM", baixaCM),
								new SqlParameter("P_ROTINA", rotina),
								new SqlParameter("P_USUARIO", usuario),
								new SqlParameter("P_CODCIN", codCin),
								new SqlParameter("P_DATLANCA", datLancamento),
								new SqlParameter("P_TURNO", turno)
								).ToList<ClassesAuxiliaresEmbalagem.RetornoApontamento>();

					if (resultado != null && resultado.Count > 0)
						return true;

				}
				catch (Exception)
				{
					return false;
				}
			}

			return false;
		}

	}
}