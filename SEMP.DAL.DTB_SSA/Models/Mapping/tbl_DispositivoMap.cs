using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.DTB_SSA.Models.Mapping
{
    public class tbl_DispositivoMap : EntityTypeConfiguration<tbl_Dispositivo>
    {
        public tbl_DispositivoMap()
        {
            // Primary Key
            this.HasKey(t => t.dispositivo_id);

            // Properties
            this.Property(t => t.dispositivo_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.dispositivo)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.flgTipoProduto)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.flgExigeNE)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.flgRastreavel)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.flgFrequencia)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.flgOpcional)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.flgMontagem)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.flgAtivo)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.flgReutilizavel)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.flgTipoConsumo)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.flgAssociacaoControlada)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.flgGEDB)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.flgColetaSerial)
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("tbl_Dispositivo");
            this.Property(t => t.dispositivo_id).HasColumnName("dispositivo_id");
            this.Property(t => t.dispositivo).HasColumnName("dispositivo");
            this.Property(t => t.flgTipoProduto).HasColumnName("flgTipoProduto");
            this.Property(t => t.flgExigeNE).HasColumnName("flgExigeNE");
            this.Property(t => t.codAgrupamento).HasColumnName("codAgrupamento");
            this.Property(t => t.flgRastreavel).HasColumnName("flgRastreavel");
            this.Property(t => t.flgFrequencia).HasColumnName("flgFrequencia");
            this.Property(t => t.flgOpcional).HasColumnName("flgOpcional");
            this.Property(t => t.flgMontagem).HasColumnName("flgMontagem");
            this.Property(t => t.flgAtivo).HasColumnName("flgAtivo");
            this.Property(t => t.flgReutilizavel).HasColumnName("flgReutilizavel");
            this.Property(t => t.flgTipoConsumo).HasColumnName("flgTipoConsumo");
            this.Property(t => t.flgAssociacaoControlada).HasColumnName("flgAssociacaoControlada");
            this.Property(t => t.flgGEDB).HasColumnName("flgGEDB");
            this.Property(t => t.flgColetaSerial).HasColumnName("flgColetaSerial");
        }
    }
}
