﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.Models.Mapping
{
    public class viw_CBDefeitoInspecaoIACMap : EntityTypeConfiguration<viw_CBDefeitoInspecaoIAC>
    {

        public viw_CBDefeitoInspecaoIACMap()
        {
            this.HasKey(t => new { t.Modelo, t.CodLinha, t.DatReferencia, t.Posicao, t.CodDefeito});

            // Properties           
            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
             
            this.Property(t => t.CodDefeito)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);
             
            this.Property(t => t.Posicao)
                .HasMaxLength(20)
                .IsUnicode(false);
          
            // Table & Column Mappings
            this.ToTable("viw_CBDefeitoInspecaoIAC");
          
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");           
            this.Property(t => t.CodDefeito).HasColumnName("CodDefeito");          
            this.Property(t => t.Posicao).HasColumnName("Posicao");           
            this.Property(t => t.DatReferencia).HasColumnName("DatReferencia");
            this.Property(t => t.Data).HasColumnName("Data");
            this.Property(t => t.Turno).HasColumnName("Turno");
            this.Property(t => t.hora).HasColumnName("hora");
            this.Property(t => t.DesDefeito).HasColumnName("DesDefeito");
            this.Property(t => t.Quantidade).HasColumnName("Quantidade");

        }
    }
}
