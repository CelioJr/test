namespace SEMP.Model.DTO
{
	using System;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;

	public partial class ZSPP_AP_DTO
	{
        [Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int CODITEM { get; set; }

        [Required]
        [StringLength(16)]
        public string PACKNO { get; set; }

        [StringLength(10)]
        public string EXTROW { get; set; }

        [StringLength(4)]
        public string WERKS { get; set; }

        [StringLength(12)]
        public string AUFNR { get; set; }

        [StringLength(4)]
        public string TYPE { get; set; }

        [StringLength(40)]
        public string MATNR { get; set; }

        [StringLength(8)]
        public string ARBPL { get; set; }

        public decimal? OQTY { get; set; }

        public DateTime? DTREF { get; set; }

        [StringLength(12)]
        public string IAUFNR { get; set; }

        [StringLength(4)]
        public string IWERKS { get; set; }

        [StringLength(40)]
        public string IMATNR { get; set; }

        public decimal? BDMNG { get; set; }

        [StringLength(3)]
        public string MEINS { get; set; }

        [StringLength(10)]
        public string SORTF { get; set; }

        [StringLength(9)]
        public string MATKL { get; set; }

        [StringLength(40)]
        public string BAUGR { get; set; }

        [StringLength(13)]
        public string WEMNG { get; set; }

        [StringLength(40)]
        public string SYSTEM_STATUS { get; set; }

        [StringLength(1)]
        public string POSTP { get; set; }

        [StringLength(1)]
        public string DUMPS { get; set; }
    }
}
