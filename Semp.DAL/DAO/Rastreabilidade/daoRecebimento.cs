﻿using System;
using System.Collections.Generic;
using System.Linq;
using SEMP.DAL.Models;
using SEMP.Model.DTO;
using SEMP.Model.VO.Rastreabilidade;
using SEMP.Model;

namespace SEMP.DAL.DAO.Rastreabilidade
{
    public class daoRecebimento
	{
        //static daoRecebimento()
        //{
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<tbl_CBLote, CBLoteDTO>();
        //        cfg.CreateMap<CBLoteDTO, tbl_CBLote>();
        //        cfg.CreateMap<CBTransfLoteDTO, tbl_CBTransfLote>();
        //    });
        //}

        #region Novos

        public static Boolean ValidaLoteRecebidoCC(string NumLote)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var lote = dtb_CB.tbl_CBLote.FirstOrDefault(x => x.NumLote == NumLote);

				//Caso o lote não exista
				if (lote == null)
					return false;

				//Caso ainda não tenha recebido o lote, então true
				if (lote.DatRecebimento != null)
					return true;
				else
					return false;
			}
		}

		/// <summary>
		/// Rotina para verificar se existe um lote no FIFO que ainda não foi recebido
		/// </summary>
		/// <param name="NumLote">Número do lote atual</param>
		/// <returns>
		///		True - existe lote pendente de recebimento
		///		False - não existe lote pendente
		/// </returns>
		public static Boolean ValidaLoteFIFO(string NumLote)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var loteAnt = NumLote.Substring(0, NumLote.Length-5);
				var numLoteAnt = int.Parse(NumLote.Substring(NumLote.Length-5, 5));

				loteAnt = String.Format("{0}{1:00000}", loteAnt, numLoteAnt - 1);

				var lote = dtb_CB.tbl_CBLote.FirstOrDefault(x => x.NumLote == loteAnt);

				//Caso o lote não exista
				if (lote == null)
					return true;

				//Caso ainda não tenha recebido o lote, então true
				if (lote.DatRecebimento == null)
					return true;
				else
					return false;
			}
		}

		public static int ObterTotalRecebido(int codLinha)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				DateTime DatInicio = new DateTime();
				DateTime DatFim = new DateTime();
				//DateTime Data = SEMP.DAL.Common.daoUtil.GetDate();
				DateTime Hora = SEMP.DAL.Common.daoUtil.GetDate();

				TimeSpan HoraAtual = new TimeSpan(Hora.Hour, Hora.Minute, Hora.Second);

				DatInicio = daoGeral.ObterDataInicio(codLinha, HoraAtual);

				if ((Hora.Hour >= 0) && (Hora.Hour < 7))
					DatInicio = DatInicio.AddDays(-1);

				DatFim = daoGeral.ObterDataFim(codLinha, HoraAtual);

				if ((DatFim.Hour >= 0) && (DatFim.Hour < 7))
					DatFim = DatFim.AddDays(1);

				//int totalRecebido = (from p in dtb_CB.tbl_CBLote
				//						join t in dtb_CB.tbl_CBTransfLote on p.NumLote equals t.NumLote
				//						where t.CodLinhaDest == codLinha && p.Status == Constantes.LOTE_RECEBIDO &&
				//								t.DatTransf >= DatInicio && t.DatTransf <= DatFim && p.NumLotePai == null
				//						select p).Count();
				int totalRecebido = (from t in dtb_CB.tbl_CBTransfLote
									 where t.CodLinhaDest == codLinha && t.DatTransf >= DatInicio && t.DatTransf <= DatFim &&
											!t.NumLote.StartsWith("C")
									 select t).Count();

				return totalRecebido;
			}
		}

		public static int ObterTotalRecebidoCC(int codLinha)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				DateTime DatInicio = new DateTime();
				DateTime DatFim = new DateTime();
				DateTime Data = SEMP.DAL.Common.daoUtil.GetDate();
				DateTime Hora = SEMP.DAL.Common.daoUtil.GetDate();

				TimeSpan HoraAtual = new TimeSpan(Hora.Hour, Hora.Minute, Hora.Second);

				DatInicio = daoGeral.ObterDataInicio(codLinha, HoraAtual);

				if ((Hora.Hour >= 0) && (Hora.Hour < 7))
					DatInicio = DatInicio.AddDays(-1);

				DatFim = daoGeral.ObterDataFim(codLinha, HoraAtual);

				if ((DatFim.Hour >= 0) && (DatFim.Hour < 7))
					DatFim = DatFim.AddDays(1);

				//int totalRecebido = (from p in dtb_CB.tbl_CBLote
				//					 join t in dtb_CB.tbl_CBTransfLote on p.NumLote equals t.NumLote
				//					 where t.CodLinhaDest == codLinha && p.Status == Constantes.LOTE_RECEBIDO &&
				//							 t.DatTransf >= DatInicio && t.DatTransf <= DatFim &&
				//							  p.NumLotePai != null
				//					 select p).Count();
				int totalRecebido = (from t in dtb_CB.tbl_CBTransfLote
									 where t.CodLinhaDest == codLinha && 
											 t.DatTransf >= DatInicio && t.DatTransf <= DatFim &&
											  t.NumLote.StartsWith("C")
									 select t).Count();

				return totalRecebido;
			}
		}

		public static String ObterUltimoLoteRecebido(int codLinha)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var lote = (from p in dtb_CB.tbl_CBLote
							join t in dtb_CB.tbl_CBTransfLote on p.NumLote equals t.NumLote
							where t.CodLinhaDest == codLinha && p.DatRecebimento != null && p.NumLotePai == null
							select p).OrderByDescending(x => x.DatRecebimento).FirstOrDefault();

				if (lote == null)
					return "";
				else
					return lote.NumLote;
			}
		}

		public static String ObterUltimoLoteRecebidoCC(int codLinha)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var lote = (from p in dtb_CB.tbl_CBLote
							join t in dtb_CB.tbl_CBTransfLote on p.NumLote equals t.NumLote
							where t.CodLinhaDest == codLinha && p.DatRecebimento != null && p.NumLotePai != null
							select p).OrderByDescending(x => x.DatRecebimento).FirstOrDefault();

				if (lote == null)
					return "";
				else
					return lote.NumLote;
			}
		}

		public static Boolean GravarRecebimento(string numLote, string codDRT)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				try
				{
					var lote = dtb_CB.tbl_CBLote.FirstOrDefault(a => a.NumLote == numLote);
					
					if (lote == null) return false;

					var data = SEMP.DAL.Common.daoUtil.GetDate();

					lote.DatRecebimento = data;
					lote.CodDRTRecebimento = codDRT;
					lote.Status = Constantes.LOTE_RECEBIDO;

					var cc = dtb_CB.tbl_CBLote.Where(x => x.NumLotePai == numLote).ToList();

					cc.ForEach(x => {x.DatRecebimento = data; x.CodDRTRecebimento = codDRT; x.Status = Constantes.LOTE_RECEBIDO; });
					
					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		public static Boolean GravarRecebimentoTransf(string numLote, string codDRT, int codLinhaDest)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				try
				{
					var lote = dtb_CB.tbl_CBLote.FirstOrDefault(x => x.NumLote == numLote);

					if (lote == null) return false;
					
					var loteTransf = dtb_CB.tbl_CBTransfLote.FirstOrDefault(x => x.NumLote == numLote);
					
					if (loteTransf != null) return false;
					
					var data = SEMP.DAL.Common.daoUtil.GetDate();
					
					if (lote.CodLinha == null){
						lote.CodLinha = int.Parse(lote.NumLote.Substring(1,2));
					}

					if (lote.NumPosto == null){
						var posto = daoGeral.ObterPostoLote(lote.CodLinha.Value);
						lote.NumPosto = posto.NumPosto;						
					}					
					
					loteTransf = new tbl_CBTransfLote(){
						CodLinhaOri = lote.CodLinha.Value,
						NumPostoOri = lote.NumPosto.Value,
						NumLote = lote.NumLote,
						CodDRT = codDRT,
						FabDestino = 0,
						CodLinhaDest = codLinhaDest,
						DatTransf = data,
						DatRecebimento = data,
						CodFab = lote.CodFab,
						NumAP = lote.NumAP
					};
					
					dtb_CB.tbl_CBTransfLote.Add(loteTransf);

					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		public static string GravarRecebimentoLote(string numLote, string codDRT, int codLinhaDest, int codLinha, int numPosto)
		{

			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				return contexto.spc_CBGravaRecebimentoLote(numLote, codLinha, numPosto, codDRT, codLinhaDest).FirstOrDefault();
			}


		}
		
		public static string ObterLotePaiCC(string numLote)
		{
			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				var caixa = contexto.tbl_CBLote.Where(a => a.NumLote == numLote).FirstOrDefault();

				if (caixa == null)
				{
					return "";
				}

				return caixa.NumLotePai;
			}
		}

		public static string ObterLotePaiSerie(string numECB)
		{
			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				var caixa = contexto.tbl_CBEmbalada.Where(a => a.NumECB == numECB).FirstOrDefault();

				if (caixa == null)
				{
					return "";
				}

				return caixa.NumLote;
			}
		}

		public static Boolean VerificaLoteExiste(string NumLote)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var lote = dtb_CB.tbl_CBLote.Count(x => x.NumLote == NumLote);

				if (lote > 0)
					return true;
				else
					return false;
			}
		}

		#endregion


		#region Verificações

		public static Boolean VerificaLoteTransferido(string NumLote)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var lote = dtb_CB.tbl_CBLote.Where(x => x.NumLote == NumLote).FirstOrDefault();

				if (lote.Status == SEMP.Model.Constantes.LOTE_TRANSFERIDO)
					return true;
				else
					return false;
			}
		}

		public static Boolean VerificaLoteLinhaDestino(int CodLinha, string NumLote)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var lote = dtb_CB.tbl_CBTransfLote.Where(x => x.CodLinhaDest == CodLinha && x.NumLote == NumLote).FirstOrDefault();

				if (lote != null)
					return true;
				else
					return false;
			}
		}

		public static Boolean ValidaLoteRecebido(string NumLote)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var lote = dtb_CB.tbl_CBLote.Where(x =>x.NumLote == NumLote).FirstOrDefault();
				var transfLote = dtb_CB.tbl_CBTransfLote.Where(x => x.NumLote == NumLote).FirstOrDefault();

				//Caso o lote não exista
				if(lote == null || transfLote == null) 
					return false;

				//Caso ainda não tenha recebido o lote, então true
				if (transfLote.DatRecebimento == null && lote.DatTransferencia != null && transfLote.DatTransf != null)
					return true;
				else
					return false;
			}
		}

		public static int ContadorLoteRecebido(int CodLinha)
		{
			int contador = 0;
			DateTime dataAtual = Common.daoUtil.GetDate();

			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				contador = dtb_CB.tbl_CBTransfLote.Where(x => x.CodLinhaDest == CodLinha && x.DatRecebimento == dataAtual.Date).Count();
			}

			return contador;
		}				

		public static RecebimentoLoteVO ValidarFIFOLote(CBLoteDTO caixaLote)
		{
			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				var numeroLote = caixaLote.NumLotePai;
				var faseLinha = numeroLote.Substring(0, 3);

				var retorno = contexto.tbl_CBLote.Where(a =>
					a.CodModelo == caixaLote.CodModelo
					&& a.NumLotePai == null
					&& a.NumLote.StartsWith(faseLinha)
					&& a.DatFechamento != null
					&& a.DatRecebimento == null
					).OrderByDescending(o => o.NumLote.Substring(13, 5))
					.Select(s => new { 

						NumLote = s.NumLote,
						DataFechamento = s.DatFechamento,
						DataRecebimento = s.DatRecebimento,
						DataTrasnferencia = s.DatTransferencia,
						Fase = s.NumLote.Substring(0,1), 
						Linha = s.NumLote.Substring(1,2), 
						Modelo = s.NumLote.Substring(7,6), 
						Num = s.NumLote.Substring(13,5)})
						
					.ToList().OrderByDescending(x => x.Num).ToList();

				var primeiroLoteASair = retorno.LastOrDefault();

				if (primeiroLoteASair == null)
				{
					return new RecebimentoLoteVO { Error = false };
				}
				else
				{
					if (int.Parse(numeroLote.Substring(13, 5)) > int.Parse(primeiroLoteASair.Num))
					{
						return new RecebimentoLoteVO { Error = true, PrimeiroLoteASair = new CBLoteDTO { NumLote = primeiroLoteASair.NumLote } };
					}
					else
					{
						return new RecebimentoLoteVO { Error = false };
					}
				}
			}
		}

		#endregion

		#region Consultas

		public static List<String> ObterItensLote(string NumLote)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				List<string> ListaNumECB = (from p in dtb_CB.tbl_CBEmbalada
								  where p.NumLote == NumLote
								  select p.NumECB).ToList();


				return ListaNumECB;
			}
		}

		public static int ObterTotalRecebido(string CodFab, string NumAP)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				int totalRecebido = (from p in dtb_CB.tbl_CBPassagem
									where p.CodFab == CodFab && p.NumAP == NumAP
									select p).Count();

				return totalRecebido;
			}
		}

		public static String ObterUltimoLoteRecebido(string CodFab, string NumAP)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var lote = (from p in dtb_CB.tbl_CBTransfLote
								where p.CodFab == CodFab && p.NumAP == NumAP && p.DatRecebimento != null
								select p).OrderByDescending(x => x.DatRecebimento).FirstOrDefault();

				if(lote == null)
					return "";
				else
					return lote.NumLote;
			}
		}

		public static List<String> ObterCaixaColetiva(string NumLote)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				List<String> NumCC = (from p in dtb_CB.tbl_CBLote
									where p.NumLotePai == NumLote
									select p.NumLote).ToList();

				return NumCC;
			}
		}

		public static int ObterTotalLote(string NumLote)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				int contador = 0;
				
				contador = (from p in dtb_CB.tbl_CBEmbalada
								where p.NumLote == NumLote
								select p).Count();

				return contador;
			}
		}

		public static int ObterTotalCC(string NumCC)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				int contador = 0;

				contador = (from p in dtb_CB.tbl_CBEmbalada
							where p.NumCC == NumCC
							select p).Count();

				return contador;
			}
		}

		public static CBLoteDTO GetCaixaColetiva(string serial)
		{
			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				var caixa = contexto.tbl_CBLote.Where(a => a.NumLote == serial).FirstOrDefault();

				if (caixa == null)
				{
					caixa = (from a in contexto.tbl_CBEmbalada
							 join b in contexto.tbl_CBLote on a.NumCC equals b.NumLote
							 where a.NumECB == serial
							 orderby a.DatLeitura descending
							 select b).FirstOrDefault();
				}

				var lote = new CBLoteDTO();

				AutoMapper.Mapper.Map(caixa, lote);

				return lote;
			}
		}

		public static List<CBLoteDTO> GetUltimosLido(DadosPosto posto)
		{
			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				var lista = new List<CBLoteDTO>();

				var dataAtual = DateTime.Now.Date;
				var dataFinal = dataAtual.AddDays(1).AddSeconds(-1);

				var listObj = (from a in contexto.tbl_CBLote
								   join b in contexto.tbl_CBEmbalada on a.NumLote equals b.NumCC
								   join c in contexto.tbl_CBPassagem on b.NumECB equals c.NumECB
								where a.DatRecebimento != null && c.CodLinha == posto.CodLinha && c.NumPosto == posto.NumPosto && c.DatEvento >= dataAtual && c.DatEvento <= dataFinal
								orderby a.DatRecebimento descending
								select a
								).Distinct().ToList();
                
				AutoMapper.Mapper.Map(listObj, lista);

				foreach (var item in lista)
				{
					item.DatRecebimentoString = item.DatRecebimento.ToString();
					item.QuantidadeCaixaLote = contexto.tbl_CBLote.FirstOrDefault(a => a.NumLote == item.NumLotePai).Qtde.Value;
					item.QuantidadeCaixaPendente = contexto.tbl_CBLote.Where(a => a.NumLotePai == item.NumLotePai && a.DatRecebimento == null).Count();
				}

				return lista;
			}
		}

		public static int GetTotalRecebido(DadosPosto posto)
		{
			using (DTB_CBContext contexto = new DTB_CBContext())
			{

				var dataAtual = DateTime.Now.Date;
				var dataFinal = dataAtual.AddDays(1).AddSeconds(-1);

				var listObj = (from a in contexto.tbl_CBLote
							   join b in contexto.tbl_CBEmbalada on a.NumLote equals b.NumCC
							   join c in contexto.tbl_CBPassagem on b.NumECB equals c.NumECB
							   where a.DatRecebimento != null && c.CodLinha == posto.CodLinha && c.NumPosto == posto.NumPosto && c.DatEvento >= dataAtual && c.DatEvento <= dataFinal
							   orderby a.DatRecebimento descending
							   select a
								).Distinct().ToList();

				return listObj.Count();
			}
		}


		public static int GetRecebidoLote(string numLote)
		{
			using (DTB_CBContext contexto = new DTB_CBContext())
			{

				var listObj = (from a in contexto.tbl_CBLote
							   where a.DatRecebimento != null && a.NumLotePai == numLote && a.Status == Constantes.LOTE_RECEBIDO
							   select a
								).Count();

				return listObj;
			}
		}

		public static int GetTotalCCLote(string numLote)
		{
			using (DTB_CBContext contexto = new DTB_CBContext())
			{

				var listObj = (from a in contexto.tbl_CBLote
							   where a.NumLotePai == numLote
							   select a
								).Count();

				return listObj;
			}
		}

		public static List<DadosRecebLote> ObterDadosRecebimento(int codLinha) {

			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				return contexto.spc_CBContagemRecebimentoLote(codLinha).ToList();
			}

		}
		#endregion

		#region Gravações

		public static Boolean GravarRecebimentoTransf(CBTransfLoteDTO transfLoteDTO)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				try
				{
					transfLoteDTO.DatRecebimento = Common.daoUtil.GetDate();					
					tbl_CBTransfLote lote = dtb_CB.tbl_CBTransfLote.Where(a => a.NumLote == transfLoteDTO.NumLote).FirstOrDefault();
					AutoMapper.Mapper.Map(transfLoteDTO, lote);
					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		public static Boolean DesfazRecebimento(CBLoteDTO loteDTO)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				try
				{
					loteDTO.DatTransferencia = null;
					tbl_CBLote lote = dtb_CB.tbl_CBLote.Where(a => a.NumLote == loteDTO.NumLote).FirstOrDefault();

					AutoMapper.Mapper.Map(loteDTO, lote);
					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		public static Boolean DesfazRecebimentoTransf(CBTransfLoteDTO transfLoteDTO)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				try
				{
					transfLoteDTO.DatTransf = null;
					tbl_CBTransfLote lote = dtb_CB.tbl_CBTransfLote.Where(a => a.NumLote == transfLoteDTO.NumLote).FirstOrDefault();

					AutoMapper.Mapper.Map(transfLoteDTO, lote);
					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		public static bool GravaPassagemRecebimentoLote(CBLoteDTO caixaLote, DadosPosto posto, string codDRT)
		{
			try
			{
				using (DTB_CBContext contexto = new DTB_CBContext())
				{
					var listaNumECBEmbalada = contexto.tbl_CBEmbalada.Where(a => a.NumCC == caixaLote.NumLote).ToList();
					List<tbl_CBPassagem> listaPassagem = new List<tbl_CBPassagem>();

					foreach (var item in listaNumECBEmbalada)
					{
						listaPassagem.Add(new tbl_CBPassagem { 
							CodDRT = codDRT,
							CodLinha = posto.CodLinha,
							NumPosto = posto.NumPosto,
							NumECB = item.NumECB,
							DatEvento = DateTime.Now
						});
					}

					contexto.tbl_CBPassagem.AddRange(listaPassagem);

					tbl_CBLote caixaColetiva = contexto.tbl_CBLote.FirstOrDefault(a => a.NumLote == caixaLote.NumLote && a.NumLotePai == caixaLote.NumLotePai);

					caixaColetiva.DatRecebimento = DateTime.Now;

					contexto.SaveChanges();

					if (!contexto.tbl_CBLote.Any(a => a.NumLotePai == caixaLote.NumLotePai && a.DatRecebimento == null))
					{
						tbl_CBLote lote = contexto.tbl_CBLote.FirstOrDefault(a => a.NumLote == caixaLote.NumLotePai);
						lote.DatRecebimento = DateTime.Now;
						contexto.SaveChanges();
					}

					return true;
				}
			}
			catch
			{
				return false;
			}
		}

		#endregion

	}
}
