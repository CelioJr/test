﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.Rastreabilidade;
using SEMP.Model.DTO;

namespace SEMP.Areas.Outros.Controllers
{
	public class IshikawaController : Controller
	{
		//
		// GET: /Outros/Ishikawa/
		public ActionResult Index(int? id, int? codLinha, string datEvento, int? posto)
		{
			try
			{
				var data = datEvento != null ? DateTime.Parse(datEvento) : DateTime.Parse(DateTime.Now.ToShortDateString());

				var plano = new CBPlanoAcaoDTO();

				if (codLinha != null)
				{
					var lista = PlanoAcao.ObterPlanoAcao(codLinha ?? 0, data);

					if (posto != null)
					{
						plano = lista.FirstOrDefault(x => x.NumPosto == posto);
					}
					else
					{
						plano = lista.FirstOrDefault();
					}
					
				}
				else
				{
					plano = PlanoAcao.ObterPlanoAcao(id ?? 0);
				}

				var dados = PlanoAcao.ObterPlanoAcaoCausaLista(plano.CodPlano);

				ViewBag.Plano = plano;
				ViewBag.TipoCausa = PlanoAcao.ObterTipoCausaLista();

				return View(dados);
			}
			catch (Exception)
			{
				var dados = new List<CBPlanoAcaoCausaDTO>();
				return View(dados);
			}
		}

		public ActionResult Visualizar(int? id, int? codLinha, string datEvento, int? posto)
		{
			try
			{
				var data = datEvento != null ? DateTime.Parse(datEvento) : DateTime.Parse(DateTime.Now.ToShortDateString());

				var plano = new CBPlanoAcaoDTO();

				if (codLinha != null)
				{
					var lista = PlanoAcao.ObterPlanoAcao(codLinha ?? 0, data);

					if (posto != null)
					{
						plano = lista.FirstOrDefault(x => x.NumPosto == posto);
					}
					else
					{
						plano = lista.FirstOrDefault();
					}

				}
				else
				{
					plano = PlanoAcao.ObterPlanoAcao(id ?? 0);
				}

				var dados = PlanoAcao.ObterPlanoAcaoCausaLista(plano.CodPlano);

				ViewBag.Plano = plano;
				ViewBag.TipoCausa = PlanoAcao.ObterTipoCausaLista();

				return View(dados);
			}
			catch (Exception)
			{
				var dados = new List<CBPlanoAcaoCausaDTO>();
				return View(dados);
			}
		}

		public JsonResult ObterPlanoDeAcaoLinha(int? codLinha, string datEvento, int? posto)
		{
			try
			{
				var data = datEvento != null ? DateTime.Parse(datEvento) : DateTime.Parse(DateTime.Now.ToShortDateString());

				var plano = new CBPlanoAcaoDTO();

				if (codLinha != null)
				{
					var lista = PlanoAcao.ObterPlanoAcao(codLinha ?? 0, data);

					if (posto != null)
					{
						plano = lista.FirstOrDefault(x => x.NumPosto == posto);
					}
					else
					{
						plano = lista.FirstOrDefault();
					}

				}

				if (plano != null && plano.CodPlano > 0)
				{
					return Json(plano, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}

			}
			catch (Exception)
			{
				
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}

		public ActionResult CadCausa(int id)
		{
			ViewBag.codPlano = id;
			return View();

		}

		[HttpPost]
		public ActionResult CadCausa(CBPlanoAcaoCausaDTO dado)
		{

			try
			{

				PlanoAcao.GravarPlanoAcaoCausa(dado);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json("Erro ao gravar. " + ex.Message, JsonRequestBehavior.AllowGet);
			}
		}

		public ActionResult EditCausa(int codPlano, int id)
		{
			ViewBag.codPlano = id;

			var causa = PlanoAcao.ObterPlanoAcaoCausa(codPlano, id);

			return View(causa);

		}

		public ActionResult Acompanhamento(int codPlano, int codPlanoCausa)
		{

			var causa = PlanoAcao.ObterPlanoAcaoCausa(codPlano, codPlanoCausa);
			var lista = PlanoAcao.ObterPlanoAcaoAcompanhamentoLista(codPlano, codPlanoCausa);

			ViewBag.DesCausa = causa.DesCausa;
			ViewBag.CodPlano = causa.CodPlano;
			ViewBag.CodPlanoCausa = causa.CodPlanoCausa;
			ViewBag.Lista = lista;

			return View();
		}

		public ActionResult AcompanhamentoLista(int codPlano, int codPlanoCausa)
		{

			var lista = PlanoAcao.ObterPlanoAcaoAcompanhamentoLista(codPlano, codPlanoCausa);

			return View(lista);
		}

		public ActionResult EditAcompanhamento(int codPlano, int codPlanoCausa, int codAcompanha)
		{
			var acompanha = PlanoAcao.ObterPlanoAcaoAcompanhamento(codPlano, codPlanoCausa, codAcompanha);
			var dados = new
			{
				dados = acompanha,
				dataIni = acompanha.DatInicio == null ? "" : acompanha.DatInicio.Value.ToShortDateString(),
				dataFim = acompanha.DatFim == null ? "" : acompanha.DatFim.Value.ToShortDateString()
			};

			return Json(dados, JsonRequestBehavior.AllowGet);

		}

		[HttpPost]
		public ActionResult CadAcompanhamento(CBPlanoAcaoAcompanhamentoDTO dado)
		{

			try
			{
				dado.DatAcompanha = DateTime.Now;
				PlanoAcao.GravarPlanoAcaoAcompanhamento(dado);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json("Erro ao gravar. " + ex.Message, JsonRequestBehavior.AllowGet);
			}
		}

	}
}