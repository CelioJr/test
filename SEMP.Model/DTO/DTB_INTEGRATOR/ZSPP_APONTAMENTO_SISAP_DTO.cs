namespace SEMP.Model.DTO
{
	using System.ComponentModel.DataAnnotations;

	public partial class ZSPP_APONTAMENTO_SISAP_DTO
	{
        [Key]
        [StringLength(16)]
        public string PACKNO { get; set; }
    }
}
