using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBCadDefeito
    {
        public string CodFab { get; set; }
        public string CodDefeito { get; set; }
        public string DesDefeito { get; set; }
        public string DesVerificacao { get; set; }
        public Nullable<short> flgIAC { get; set; }
        public Nullable<short> flgIMC { get; set; }
        public Nullable<short> flgFEC { get; set; }        
        public Nullable<short> flgLCM { get; set; }
        public string flgAtivo { get; set; }
    }
}
