﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SEMP.Areas.Coleta.Models
{
	public class FormSetup
	{

		public string Fabrica { get; set; }
		public int CodLinha { get; set; }
		public int NumPosto { get; set; }

	}
}