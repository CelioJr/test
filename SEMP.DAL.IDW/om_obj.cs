namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_obj
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_obj()
        {
            om_obj1 = new HashSet<om_obj>();
            om_obj11 = new HashSet<om_obj>();
        }

        [Key]
        public long id_obj { get; set; }

        public decimal? x { get; set; }

        public decimal? y { get; set; }

        public decimal? x2 { get; set; }

        public decimal? y2 { get; set; }

        public short? tp_obj { get; set; }

        [StringLength(11)]
        public string corFrente { get; set; }

        [StringLength(11)]
        public string corFundo { get; set; }

        public long? id_texto { get; set; }

        public long? id_webcam { get; set; }

        public long? id_img { get; set; }

        public long? id_pt { get; set; }

        public long? id_gtfilho { get; set; }

        public long id_gt { get; set; }

        public long? id_rota { get; set; }

        public long? id_rotapasso { get; set; }

        public long? id_folha { get; set; }

        public long? id_est { get; set; }

        public short? monitoracao { get; set; }

        public long? id_objorigem { get; set; }

        public long? id_objdestino { get; set; }

        public virtual dw_est dw_est { get; set; }

        public virtual dw_folha dw_folha { get; set; }

        public virtual dw_rota dw_rota { get; set; }

        public virtual dw_rotapasso dw_rotapasso { get; set; }

        public virtual om_gt om_gt { get; set; }

        public virtual om_gt om_gt1 { get; set; }

        public virtual om_img om_img { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_obj> om_obj1 { get; set; }

        public virtual om_obj om_obj2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_obj> om_obj11 { get; set; }

        public virtual om_obj om_obj3 { get; set; }

        public virtual om_pt om_pt { get; set; }

        public virtual om_texto om_texto { get; set; }

        public virtual om_webcam om_webcam { get; set; }
    }
}
