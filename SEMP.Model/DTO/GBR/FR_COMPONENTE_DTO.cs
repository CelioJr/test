using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_COMPONENTE_DTO
    {
        public FR_COMPONENTE_DTO()
        {
            this.FR_ACAOCOMPONENTE = new List<FR_ACAOCOMPONENTE_DTO>();
            this.FR_PROPRIEDADE = new List<FR_PROPRIEDADE_DTO>();
        }

        public int FRM_CODIGO { get; set; }
        public int COM_CODIGO { get; set; }
        public Nullable<int> IMG_CODIGO { get; set; }
        public string COM_TIPO { get; set; }
        public virtual ICollection<FR_ACAOCOMPONENTE_DTO> FR_ACAOCOMPONENTE { get; set; }
        public virtual FR_FORMULARIO_DTO FR_FORMULARIO { get; set; }
        public virtual ICollection<FR_PROPRIEDADE_DTO> FR_PROPRIEDADE { get; set; }
    }
}
