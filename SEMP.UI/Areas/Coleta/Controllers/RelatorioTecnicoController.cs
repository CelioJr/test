﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using RAST.BO.Relatorios;
using RAST.BO.Rastreabilidade;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using DotNet.Highcharts;
using SEMP.Models;
using DotNet.Highcharts.Helpers;
using SEMP.Model;
using SEMP.Model.DTO;
using SEMP.BO;
namespace SEMP.Areas.Coleta.Controllers
{
	public class RelatorioTecnicoController : Controller
	{

		public ActionResult Index(string Mensagem)
		{
			ViewBag.mensagem = Mensagem;


			var dIni = DateTime.Parse(DateTime.Now.AddDays(-DateTime.Now.Day).AddDays(1).ToString("dd/MM/yyyy"));
			var dFim = DateTime.Now.AddDays(1).AddMinutes(-1);

			var dados = Defeito.ObterDefeitos(dIni, dFim);
			ViewBag.resumo = dados;

			return View();
		}

		public ActionResult Tecnico()
		{
			return RedirectToAction("Index", "RelatorioTecnico");
		}

		public ActionResult Linha()
		{
			return RedirectToAction("Index", "RelatorioTecnico");
		}

		[HttpPost]
		public ActionResult Tecnico(string datInicio, string datFim, int? codLinha, int? turno, string codDRT, string codModelo, string fase)
		{

			DateTime dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			DateTime dFim = dIni.AddDays(1).AddMinutes(-1);

			if (!String.IsNullOrEmpty(datInicio) && !String.IsNullOrEmpty(datFim))
			{
				dIni = DateTime.Parse(datInicio);
				dFim = DateTime.Parse(datFim);
			}

			var nome = Login.ObterFuncionarioByDRT(codDRT).NomFuncionario;

			var fabrica = (fase == "PLACA") ? "FEC" : fase;
			var placa = (fase == "PLACA") ? 1 : 0;

			var defeitos = Defeito.ObterDefeitosRel(dIni, dFim, codLinha, fase, codDRT, placa);

			if (!String.IsNullOrEmpty(codModelo))
			{
				defeitos = defeitos.Where(x => x.CodModelo == codModelo).ToList();
			}

			if (codLinha != null)
			{
				defeitos = defeitos.Where(x => x.CodLinha == codLinha).ToList();
			}

			if (turno != null && turno > 0)
			{
				defeitos = defeitos.Where(x => x.Turno == turno).ToList();
			}

			if (!defeitos.Any())
			{
				return RedirectToAction("Index", "RelatorioTecnico", new { Mensagem = "Não foram encontrados registros com o filtro informado." });
			}

			var resumoDefeito = defeitos.GroupBy(x => new { x.CodDefeito, x.DesDefeito, x.QtdMes }).
				Select(p => new ResumoDefeito
				{
					CodDefeito = p.Key.CodDefeito,
					DesDefeito = p.Key.DesDefeito,
					QtdAcumulada = p.Key.QtdMes,
					QtdDefeito = p.Count(),
					Cor = Util.GetRandomColor(int.Parse(Guid.NewGuid().ToString().Substring(0, 8), System.Globalization.NumberStyles.HexNumber))
				}).
				OrderByDescending(x => x.QtdDefeito).ToList();

			var resumoCausa = defeitos.Where(x => !String.IsNullOrEmpty(x.CodCausa)).GroupBy(x => new { x.CodCausa, x.DesCausa, QtdMes = 0 }).
							Select(p => new ResumoDefeito
							{
								CodDefeito = p.Key.CodCausa,
								DesDefeito = p.Key.DesCausa,
								QtdAcumulada = p.Key.QtdMes,
								QtdDefeito = p.Count(),
								Cor = Util.GetRandomColor(int.Parse(Guid.NewGuid().ToString().Substring(0, 8), System.Globalization.NumberStyles.HexNumber))
							}).
							OrderByDescending(x => x.QtdDefeito).ToList();

			var resumoOrigem = defeitos.Where(x => !String.IsNullOrEmpty(x.CodOrigem)).GroupBy(x => new { x.CodOrigem, x.DesOrigem, QtdMes = 0 }).
							Select(p => new ResumoDefeito
							{
								CodDefeito = p.Key.CodOrigem,
								DesDefeito = p.Key.DesOrigem,
								QtdAcumulada = p.Key.QtdMes,
								QtdDefeito = p.Count(),
								Cor = Util.GetRandomColor(int.Parse(Guid.NewGuid().ToString().Substring(0, 8), System.Globalization.NumberStyles.HexNumber))
							}).
							OrderByDescending(x => x.QtdDefeito).ToList();

			var resumoPosicao = defeitos.Where(x => !String.IsNullOrEmpty(x.Posicao) && x.Posicao != "NULL").GroupBy(x => new { x.Posicao, DesDefeito = "", QtdMes = 0 }).
							Select(p => new ResumoDefeito
							{
								CodDefeito = p.Key.Posicao,
								DesDefeito = p.Key.DesDefeito,
								QtdAcumulada = p.Key.QtdMes,
								QtdDefeito = p.Count(),
								Cor = Util.GetRandomColor(int.Parse(Guid.NewGuid().ToString().Substring(0, 8), System.Globalization.NumberStyles.HexNumber))
							}).
							OrderByDescending(x => x.QtdDefeito).ToList();

			var modelos = defeitos.Select(x => x.CodModelo).Distinct().ToList();
			int qtdProducao = 0;
			int qtdAcumulado = 0;

			foreach (var modelo in modelos)
			{
				qtdProducao += RelatorioTecnico.ObterProducaoModelo(dIni, dFim, modelo);
				qtdAcumulado += RelatorioTecnico.ObterProdutosPendentes(dIni, dFim, modelo);
			}

			var resumoRel = new ResumoRelatorioTecnico();
			resumoRel.QtdPlacasAcumuladas = qtdAcumulado;
			resumoRel.QtdDefeitos = defeitos.Count();
			resumoRel.QtdProduzida = qtdProducao;
			/**/
			if (resumoRel.QtdDefeitos > 0)
			{
				resumoRel.QtdFalsaFalha = defeitos.Count(x => x.CodDefeito == "SF876" || (x.CodCausa != null && x.CodCausa.Contains("WX00")) || x.DesAcao == "DEFEITO NAO ENCONTRADO");

				var menorhora = defeitos.Where(x => x.TempoReparo != null).Min(x => x.TempoReparo) ?? TimeSpan.Zero;
				resumoRel.MenorTempo = menorhora.ToString().Substring(0, 8);
				if (menorhora.Days > 0)
				{
					resumoRel.MenorTempo = String.Format("{0} dias {1}", menorhora.Days, resumoRel.MenorTempo);
				}

				var maiorhora = defeitos.Where(x => x.TempoReparo != null).Max(x => x.TempoReparo) ?? TimeSpan.Zero;
				resumoRel.MaiorTempo = maiorhora.ToString().Substring(0, 8);
				if (maiorhora.Days > 0)
				{
					resumoRel.MaiorTempo = String.Format("{0} dias {1}", maiorhora.Days, resumoRel.MaiorTempo);
				}

				var totalhora = TimeSpan.FromMinutes(defeitos.Average(x => x.TempoReparo != null ? x.TempoReparo.Value.TotalMinutes : 0));
				resumoRel.TempoMedio = totalhora.ToString().Substring(0, 8);

				resumoRel.IndiceTotal = ((float)resumoRel.QtdDefeitos / (float)resumoRel.QtdProduzida) * 100;
				resumoRel.Indice = ((float)(resumoRel.QtdDefeitos - resumoRel.QtdFalsaFalha) / (float)resumoRel.QtdProduzida) * 100;
				resumoRel.IndiceFalsaFalha = ((float)resumoRel.QtdFalsaFalha / (float)resumoRel.QtdProduzida) * 100;
			}

			ViewBag.defeitos = defeitos;
			ViewBag.resumoDefeitos = resumoDefeito;
			ViewBag.resumoCausa = resumoCausa;
			ViewBag.resumoOrigem = resumoOrigem;
			ViewBag.resumoPosicao = resumoPosicao;
			ViewBag.resumoRel = resumoRel;

			ViewBag.nome = nome;
			ViewBag.grafDefeito = GerarGrafico(resumoDefeito, "Defeitos", "chartDefeito");
			ViewBag.grafCausa = GerarGrafico(resumoCausa, "Causa", "chartCausa");
			ViewBag.grafOrigem = GerarGrafico(resumoOrigem, "Origem", "chartOrigem");
			ViewBag.grafPosicao = GerarGrafico(resumoPosicao, "Posição", "chartPosicao");

			return View();
		}

		[HttpPost]
		public ActionResult Linha(string datInicio, string datFim, string fase, int? codLinha, int? turno, string codDRT, string codModelo, string flgGrafico = "S", string familia = "")
		{

			var dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			var dFim = dIni.AddDays(1).AddMinutes(-1);

			if (!String.IsNullOrEmpty(datInicio) && !String.IsNullOrEmpty(datFim))
			{
				dIni = DateTime.Parse(datInicio);
				dFim = DateTime.Parse(datFim);
			}

			var Nome = Login.ObterFuncionarioByDRT(codDRT).NomFuncionario;
			List<viw_CBDefeitoTurnoDTO> Defeitos = null;

			var fabrica = fase;
			var placa = 0;
			
			if (fase == "PLACA")
			{
				fabrica = "FEC";
				placa = 1;
				var seriespai = Defeito.ObterDefeitosRelRef(dIni, dFim, codLinha, fabrica, codDRT);
				ViewBag.seriespai = seriespai;
			}

			Defeitos = Defeito.ObterDefeitosRel(dIni, dFim, codLinha, fabrica, codDRT, placa, familia);

			if (!String.IsNullOrEmpty(codModelo))
			{
				Defeitos = Defeitos.Where(x => x.CodModelo == codModelo).ToList();
			}

			if (codLinha != null)
			{
				Defeitos = Defeitos.Where(x => x.CodLinha == codLinha).ToList();
			}

			if (turno != null && turno > 0)
			{
				Defeitos = Defeitos.Where(x => x.Turno == turno).ToList();
			}

			if (!Defeitos.Any())
			{
				return RedirectToAction("Index", "RelatorioTecnico", new { Mensagem = "Não foram encontrados registros com o filtro informado." });
			}

			var resumoDefeito = new List<ResumoDefeito>();
			var resumoCausa = new List<ResumoDefeito>();
			var resumoOrigem = new List<ResumoDefeito>();
			var resumoPosicao = new List<ResumoDefeito>();

			#region Resumo Defeitos
			if (flgGrafico.Equals("S"))
			{
				resumoDefeito = Defeitos.GroupBy(x => new {x.CodDefeito, x.DesDefeito, x.QtdMes}).
					Select(p => new ResumoDefeito
					{
						CodDefeito = p.Key.CodDefeito,
						DesDefeito = p.Key.DesDefeito,
						QtdAcumulada = p.Key.QtdMes,
						QtdDefeito = p.Count(),
						Cor =
							Util.GetRandomColor(int.Parse(Guid.NewGuid().ToString().Substring(0, 8),
								System.Globalization.NumberStyles.HexNumber))
					}).
					OrderByDescending(x => x.QtdDefeito).ToList();

				resumoCausa =
					Defeitos.Where(x => !String.IsNullOrEmpty(x.CodCausa)).GroupBy(x => new {x.CodCausa, x.DesCausa, QtdMes = 0}).
						Select(p => new ResumoDefeito
						{
							CodDefeito = p.Key.CodCausa,
							DesDefeito = p.Key.DesCausa,
							QtdAcumulada = p.Key.QtdMes,
							QtdDefeito = p.Count(),
							Cor =
								Util.GetRandomColor(int.Parse(Guid.NewGuid().ToString().Substring(0, 8),
									System.Globalization.NumberStyles.HexNumber))
						}).
						OrderByDescending(x => x.QtdDefeito).ToList();
				resumoCausa.AddRange(Defeitos.Where(x => String.IsNullOrEmpty(x.CodCausa)).GroupBy(x => new {QtdMes = 0}).
						Select(p => new ResumoDefeito
						{
							CodDefeito = "SC",
							DesDefeito = "SEM CAUSA / EM ABERTO",
							QtdAcumulada = p.Key.QtdMes,
							QtdDefeito = p.Count(),
							Cor =
								Util.GetRandomColor(int.Parse(Guid.NewGuid().ToString().Substring(0, 8),
									System.Globalization.NumberStyles.HexNumber))
						}).ToList());

				resumoOrigem =
					Defeitos.Where(x => !String.IsNullOrEmpty(x.CodOrigem) && x.CodOrigem.Trim() != "").GroupBy(x => new {x.CodOrigem, x.DesOrigem, QtdMes = 0}).
						Select(p => new ResumoDefeito
						{
							CodDefeito = p.Key.CodOrigem,
							DesDefeito = p.Key.DesOrigem,
							QtdAcumulada = p.Key.QtdMes,
							QtdDefeito = p.Count(),
							Cor =
								Util.GetRandomColor(int.Parse(Guid.NewGuid().ToString().Substring(0, 8),
									System.Globalization.NumberStyles.HexNumber))
						}).
						OrderByDescending(x => x.QtdDefeito).ToList();
				resumoOrigem.AddRange(Defeitos.Where(x => String.IsNullOrEmpty(x.CodOrigem) || x.CodOrigem.Trim() == "").GroupBy(x => new { QtdMes = 0}).
						Select(p => new ResumoDefeito
						{
							CodDefeito = "SO",
							DesDefeito = "SEM ORIGEM / EM ABERTO",
							QtdAcumulada = p.Key.QtdMes,
							QtdDefeito = p.Count(),
							Cor =
								Util.GetRandomColor(int.Parse(Guid.NewGuid().ToString().Substring(0, 8),
									System.Globalization.NumberStyles.HexNumber))
						}).ToList());

				resumoPosicao =
					Defeitos.Where(x => !String.IsNullOrEmpty(x.Posicao) && x.Posicao != "NULL").GroupBy(x => new {x.Posicao, x.DesPosicao, QtdMes = 0}).
						Select(p => new ResumoDefeito
						{
							CodDefeito = p.Key.Posicao,
							DesDefeito = p.Key.DesPosicao,
							QtdAcumulada = p.Key.QtdMes,
							QtdDefeito = p.Count(),
							Cor =
								Util.GetRandomColor(int.Parse(Guid.NewGuid().ToString().Substring(0, 8),
									System.Globalization.NumberStyles.HexNumber))
						}).
						OrderByDescending(x => x.QtdDefeito).ToList();
				resumoPosicao.AddRange(Defeitos.Where(x => String.IsNullOrEmpty(x.Posicao) || x.Posicao == "NULL").GroupBy(x => new {QtdMes = 0}).
						Select(p => new ResumoDefeito
						{
							CodDefeito = "SP",
							DesDefeito = "SEM POSIÇÃO / EM ABERTO",
							QtdAcumulada = p.Key.QtdMes,
							QtdDefeito = p.Count(),
							Cor =
								Util.GetRandomColor(int.Parse(Guid.NewGuid().ToString().Substring(0, 8),
									System.Globalization.NumberStyles.HexNumber))
						}).
						OrderByDescending(x => x.QtdDefeito).ToList());
			}
			#endregion

			int qtdProducao = 0;
			int qtdAcumulado = 0;

			string desLinha = "";
			if (codLinha != null)
			{
				desLinha = Geral.ObterLinha(codLinha).DesLinha;

				qtdProducao += RelatorioTecnico.ObterProducaoLinha(dIni, dFim, (int)codLinha);
				qtdAcumulado += RelatorioTecnico.ObterProdutosPendentes(dIni, dFim, (int)codLinha, codModelo);

			}
			else
			{
				desLinha = "Todas as linhas";
				qtdProducao += RelatorioTecnico.ObterProducaoFase(dIni, dFim, fase);
				qtdAcumulado += RelatorioTecnico.ObterPendentesFase(dIni, dFim, fase, codModelo);
			}

			var resumoRel = new ResumoRelatorioTecnico();
			resumoRel.QtdPlacasAcumuladas = qtdAcumulado;
			resumoRel.QtdDefeitos = Defeitos.Count(x => x.FlgRevisado == 1);
			resumoRel.QtdTotalDefeitos = Defeitos.Count();
			resumoRel.QtdProduzida = qtdProducao;
			resumoRel.IndiceTotal = 0;
			resumoRel.Indice = 0;
			resumoRel.IndiceFalsaFalha = 0;

			/**/
			if (resumoRel.QtdDefeitos > 0 && resumoRel.QtdProduzida > 0)
			{
				resumoRel.QtdFalsaFalha = Defeitos.Where(x => x.FlgRevisado == 1).Count(x => x.CodDefeito == "SF876" || (x.CodCausa != null && x.CodCausa.Contains("WX00")) || x.DesAcao == "DEFEITO NAO ENCONTRADO");

				var menorhora = Defeitos.Where(x => x.TempoReparo != null).Min(x => x.TempoReparo) ?? TimeSpan.Zero;
				resumoRel.MenorTempo = menorhora.ToString().Substring(0, 8);
				if (menorhora.Days > 0)
				{
					resumoRel.MenorTempo = String.Format("{0} dias {1}", menorhora.Days, resumoRel.MenorTempo);
				}

				var maiorhora = Defeitos.Where(x => x.TempoReparo != null).Max(x => x.TempoReparo) ?? TimeSpan.Zero;
				resumoRel.MaiorTempo = maiorhora.ToString().Substring(0, 8);
				if (maiorhora.Days > 0)
				{
					resumoRel.MaiorTempo = String.Format("{0} dias {1}", maiorhora.Days, resumoRel.MaiorTempo);
				}

				var totalhora = TimeSpan.FromMinutes(Defeitos.Average(x => x.TempoReparo != null ? x.TempoReparo.Value.TotalMinutes : 0));
				resumoRel.TempoMedio = totalhora.ToString().Substring(0, 8);

				resumoRel.IndiceTotal = ((float)resumoRel.QtdDefeitos / (float)resumoRel.QtdProduzida) * 100;
				resumoRel.Indice = ((float)(resumoRel.QtdDefeitos - resumoRel.QtdFalsaFalha) / (float)resumoRel.QtdProduzida) * 100;
				resumoRel.IndiceFalsaFalha = ((float)resumoRel.QtdFalsaFalha / (float)resumoRel.QtdProduzida) * 100;
			}

			/**/
			ViewBag.familia = familia;
			ViewBag.desLinha = desLinha;
			ViewBag.defeitos = Defeitos.OrderByDescending(x => x.FlgRevisado).ThenBy(x => x.CodDRT).ToList();			
			ViewBag.nome = Nome;
			ViewBag.resumoRel = resumoRel;
			ViewBag.fabrica = fabrica;

			if (flgGrafico.Equals("S"))
			{
				ViewBag.resumoDefeitos = resumoDefeito;
				ViewBag.resumoCausa = resumoCausa;
				ViewBag.resumoOrigem = resumoOrigem;
				ViewBag.resumoPosicao = resumoPosicao;
				ViewBag.grafDefeito = GerarGrafico(resumoDefeito, "Defeitos", "chartDefeito");
				ViewBag.grafCausa = GerarGrafico(resumoCausa, "Causa", "chartCausa");
				ViewBag.grafOrigem = GerarGrafico(resumoOrigem, "Origem", "chartOrigem");
				ViewBag.grafPosicao = GerarGrafico(resumoPosicao, "Posição", "chartPosicao");
			}

			return View();
		}

		public ActionResult Detalhe(string numECB, string codDefeito, int codLinha, int numPosto, string datEvento)
		{

			try
			{

				var resultado = Defeito.ObterRegistroDefeitoCompleto(numECB, codDefeito, codLinha, numPosto, DateTime.Parse(datEvento));

				return View(resultado);
			}
			catch (Exception ex)
			{
				ViewBag.mensagem = String.Format("Erro ao consultar: {0}", ex.Message);
				return View(new viw_CBDefeitoTurnoDTO());
			}

		}

		internal Highcharts GerarGrafico(List<ResumoDefeito> Lista, string titulo, string idGrafico)
		{

			List<DotNet.Highcharts.Options.Point> listaPoint = new List<DotNet.Highcharts.Options.Point>();
			Highcharts chart = null;
			string[] categories = null;

			categories = Lista.Select(s => s.CodDefeito).ToArray();
			Lista.ForEach(f => listaPoint.Add(new DotNet.Highcharts.Options.Point { Y = f.QtdDefeito, Color = f.Cor }));
			Data data = new Data(listaPoint.ToArray());
			chart = Grafico.GerarGraficoBarra(data, categories, titulo, idGrafico);

			return chart;
		}

		public ActionResult GerarExcel(string datInicio, string datFim, string fase, int? codLinha, int? turno, string codDRT, string codModelo, string familia = "")
		{
			Response.AddHeader("content-disposition", "attachment; filename=RelatorioTecnico.xls");
			Response.ContentType = "application/ms-excel";

			var dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			var dFim = dIni.AddDays(1).AddMinutes(-1);

			if (!String.IsNullOrEmpty(datInicio) && !String.IsNullOrEmpty(datFim))
			{
				dIni = DateTime.Parse(datInicio);
				dFim = DateTime.Parse(datFim);
			}

			List<viw_CBDefeitoTurnoDTO> Defeitos = null;

			var fabrica = fase;
			var placa = 0;

			if (fase == "PLACA")
			{
				fabrica = "FEC";
				placa = 1;
				var seriespai = Defeito.ObterDefeitosRelRef(dIni, dFim, codLinha, fabrica, codDRT);
				ViewBag.seriespai = seriespai;
			}

			Defeitos = Defeito.ObterDefeitosRel(dIni, dFim, codLinha, fabrica, codDRT, placa, familia);

			if (!String.IsNullOrEmpty(codModelo))
			{
				Defeitos = Defeitos.Where(x => x.CodModelo == codModelo).ToList();
			}

			if (codLinha != null)
			{
				Defeitos = Defeitos.Where(x => x.CodLinha == codLinha).ToList();
			}

			if (turno != null && turno > 0)
			{
				Defeitos = Defeitos.Where(x => x.Turno == turno).ToList();
			}

			if (!Defeitos.Any())
			{
				return RedirectToAction("Index", "RelatorioTecnico", new { Mensagem = "Não foram encontrados registros com o filtro informado." });
			}

			ViewBag.defeitos = Defeitos.OrderByDescending(x => x.FlgRevisado).ThenBy(x => x.CodDRT).ToList();


			/*Gráfico */
			var resumoDefeito = Defeitos.GroupBy(x => new { x.CodDefeito, x.DesDefeito, x.QtdMes }).
								Select(p => new ResumoDefeito
								{
									CodDefeito = p.Key.CodDefeito,
									DesDefeito = p.Key.DesDefeito,
									QtdAcumulada = p.Key.QtdMes,
									QtdDefeito = p.Count(),
									Cor =
										Util.GetRandomColor(int.Parse(Guid.NewGuid().ToString().Substring(0, 8),
											System.Globalization.NumberStyles.HexNumber))
								}).
								OrderByDescending(x => x.QtdDefeito).ToList();
			ViewBag.grafico = GerarGrafico(resumoDefeito, "Defeitos", "chartDefeito");
			/* fim gráfico*/


			return View();
		}

	}
}
