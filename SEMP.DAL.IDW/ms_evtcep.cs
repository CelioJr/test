namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ms_evtcep
    {
        [Key]
        public long id_msevtcep { get; set; }

        public decimal? energiaconsumida { get; set; }

        public decimal? fatorpotencia { get; set; }

        public decimal? tensao { get; set; }

        public decimal? tensao1 { get; set; }

        public decimal? tensao2 { get; set; }

        public decimal? tensao3 { get; set; }

        public decimal? corrente { get; set; }

        public decimal? corrente1 { get; set; }

        public decimal? corrente2 { get; set; }

        public decimal? corrente3 { get; set; }

        public long fk { get; set; }

        public decimal? temperatura { get; set; }

        public decimal? tensao1e2 { get; set; }

        public decimal? tensao1e3 { get; set; }

        public decimal? tensao2e3 { get; set; }

        public virtual ms_evt ms_evt { get; set; }
    }
}
