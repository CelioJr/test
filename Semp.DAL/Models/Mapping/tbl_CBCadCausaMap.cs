using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBCadCausaMap : EntityTypeConfiguration<tbl_CBCadCausa>
    {
        public tbl_CBCadCausaMap()
        {
            // Primary Key
            this.HasKey(t => t.CodCausa);

            // Properties
            this.Property(t => t.CodCausa)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(4)
                .IsUnicode(false);

            this.Property(t => t.DesCausa)
                .HasMaxLength(60)
                .IsUnicode(false);

            this.Property(t => t.flgAtivo)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_CBCadCausa");
            this.Property(t => t.CodCausa).HasColumnName("CodCausa");
            this.Property(t => t.DesCausa).HasColumnName("DesCausa");
            this.Property(t => t.flgIMC).HasColumnName("flgIMC");
            this.Property(t => t.flgMONTAGEM).HasColumnName("flgMONTAGEM");
            this.Property(t => t.flgAtivo).HasColumnName("flgAtivo");
        }
    }
}
