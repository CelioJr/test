﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.ApontaProd;
using RAST.BO.Rastreabilidade;
using SEMP.Model.VO.Rastreabilidade.Relatorios;

namespace SEMP.Areas.Coleta.Controllers
{
	public class LeituraPostosController : Controller
	{
		//
		// GET: /Coleta/LeituraPostos/

		public ActionResult Index(string datInicio, string datFim, int? codLinha, int? turno, string tipo)
		{

			var linhas = Geral.ObterLinhaPorFabrica("FEC");
			linhas.AddRange(Geral.ObterLinhaPorFabrica("IMC"));
			linhas.AddRange(Geral.ObterLinhaPorFabrica("IAC"));
			linhas.AddRange(Geral.ObterLinhaPorFabrica("DAT"));

			DateTime dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			DateTime dFim = dIni.AddDays(1).AddMinutes(-1);

			if (!String.IsNullOrEmpty(datInicio) && !String.IsNullOrEmpty(datFim))
			{
				dIni = DateTime.Parse(datInicio);
				dFim = DateTime.Parse(datFim);
			}

			var leitura = new List<LeiturasPostos>();
			var producaodiaria = new List<ProducaoDiariaDAT>();

			if (codLinha != null)
			{
				switch (tipo)
				{
					default:
						leitura = LeituraPostosBO.ObterLeiturasPostos(dIni, dFim, (int) codLinha, turno.Value);
						break;
					 case "1": case "2":
						leitura = LeituraPostosBO.ObterLeiturasPostosAnalitico(dIni, dFim, (int) codLinha, turno.Value);
						break;
					case "3":
						producaodiaria = LeituraPostosBO.ObterProducaoDAT(dIni, dFim, (int)codLinha);
						break;
				}
				
			}
			ViewBag.linhas = linhas;
			ViewBag.leitura = leitura;
			ViewBag.producaodiaria = producaodiaria;
			return View();
		}

		public ActionResult Series(int codLinha, int numPosto, string datInicio, string datFim){

			var passagem = new Passagem(codLinha, numPosto);

			DateTime dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			DateTime dFim = dIni.AddDays(1).AddMinutes(-1);

			if (!String.IsNullOrEmpty(datInicio) && !String.IsNullOrEmpty(datFim))
			{
				dIni = DateTime.Parse(datInicio);
				dFim = DateTime.Parse(datFim).AddDays(1).AddMinutes(-1);
			}

			var dados = passagem.ObterPassagemPosto(dIni, dFim);

			return View(dados);

		}

		public ActionResult ResumoContingencia(string datInicio, string datFim) {

			DateTime dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			DateTime dFim = dIni.AddDays(1).AddMinutes(-1);

			if (!String.IsNullOrEmpty(datInicio) && !String.IsNullOrEmpty(datFim))
			{
				dIni = DateTime.Parse(datInicio);
				dFim = DateTime.Parse(datFim).AddDays(1).AddMinutes(-1);
			}

			var dados = Geral.ObterResumoContingencia(dIni, dFim);

			return View(dados);
		}

	}
}
