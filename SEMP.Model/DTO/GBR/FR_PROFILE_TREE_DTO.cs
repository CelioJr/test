using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_PROFILE_TREE_DTO
    {
        public string PRT_CODIGO { get; set; }
        public string PRT_CODIGO_PARENT { get; set; }
        public string PRI_CODIGO { get; set; }
        public string PRT_DESCRIPTION { get; set; }
        public string PRT_IDENTIFIER { get; set; }
        public Nullable<long> PRT_START { get; set; }
        public Nullable<long> PRT_TIME { get; set; }
        public string PRT_TYPE { get; set; }
    }
}
