using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBPadraoIndicador
    {
        public int Indicador { get; set; }
        public string Fabrica { get; set; }
        public int CodLinha { get; set; }
        public Nullable<decimal> Valor { get; set; }
    }
}
