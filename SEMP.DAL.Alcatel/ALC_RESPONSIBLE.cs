namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_RESPONSIBLE")]
    public partial class ALC_RESPONSIBLE
    {
        [Key]
        [StringLength(50)]
        public string name { get; set; }
    }
}
