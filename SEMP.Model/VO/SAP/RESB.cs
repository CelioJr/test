﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.SAP
{
	public class RESB
	{
		public string aufnr { get; set; }
		public string werks { get; set; }
		public double rspos { get; set; }
		public string matnr { get; set; }
		public string baugr { get; set; }
		public Decimal bdmng { get; set; }
		public string meins { get; set; }
		public string postp { get; set; }
		public string sortf { get; set; }
		public string dumps { get; set; }
		public string matkl { get; set; }
	}
}
