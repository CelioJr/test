using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_RITurnoMap : EntityTypeConfiguration<tbl_RITurno>
    {
        public tbl_RITurnoMap()
        {
            // Primary Key
            this.HasKey(t => t.CodTurno);

            // Properties
            this.Property(t => t.CodTurno)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.DesTurno)
                .HasMaxLength(50);

            this.Property(t => t.NumCarga)
                .HasMaxLength(10);

            this.Property(t => t.NumCargaSem)
                .HasMaxLength(50);

            this.Property(t => t.NumCargaMes)
                .HasMaxLength(50);

            this.Property(t => t.CodJornada1)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.CodJornada2)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.CodJornada3)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.CodJornada4)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.CodJornada5)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.CodJornada6)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.CodJornada7)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.CodJornada8)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.CodJornada9)
                .IsFixedLength()
                .HasMaxLength(3);

            // Table & Column Mappings
            this.ToTable("tbl_RITurno");
            this.Property(t => t.CodTurno).HasColumnName("CodTurno");
            this.Property(t => t.DesTurno).HasColumnName("DesTurno");
            this.Property(t => t.NumCarga).HasColumnName("NumCarga");
            this.Property(t => t.NumCargaSem).HasColumnName("NumCargaSem");
            this.Property(t => t.NumCargaMes).HasColumnName("NumCargaMes");
            this.Property(t => t.TipoDia1).HasColumnName("TipoDia1");
            this.Property(t => t.TipoDia2).HasColumnName("TipoDia2");
            this.Property(t => t.TipoDia3).HasColumnName("TipoDia3");
            this.Property(t => t.TipoDia4).HasColumnName("TipoDia4");
            this.Property(t => t.TipoDia5).HasColumnName("TipoDia5");
            this.Property(t => t.TipoDia6).HasColumnName("TipoDia6");
            this.Property(t => t.TipoDia7).HasColumnName("TipoDia7");
            this.Property(t => t.TipoDia8).HasColumnName("TipoDia8");
            this.Property(t => t.TipoDia9).HasColumnName("TipoDia9");
            this.Property(t => t.CodJornada1).HasColumnName("CodJornada1");
            this.Property(t => t.CodJornada2).HasColumnName("CodJornada2");
            this.Property(t => t.CodJornada3).HasColumnName("CodJornada3");
            this.Property(t => t.CodJornada4).HasColumnName("CodJornada4");
            this.Property(t => t.CodJornada5).HasColumnName("CodJornada5");
            this.Property(t => t.CodJornada6).HasColumnName("CodJornada6");
            this.Property(t => t.CodJornada7).HasColumnName("CodJornada7");
            this.Property(t => t.CodJornada8).HasColumnName("CodJornada8");
            this.Property(t => t.CodJornada9).HasColumnName("CodJornada9");
            this.Property(t => t.DataFolga).HasColumnName("DataFolga");
        }
    }
}
