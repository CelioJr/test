﻿using MATERIAIS.BO.Estoque;
using SEMP.Model.VO.SAP;
using System;
using System.Linq;
using System.Web.Mvc;

namespace SEMP.Areas.Materiais.Controllers
{
	public class EstoqueController : Controller
	{
		// GET: Materiais/Estoque
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult LSAAP()
		{
			return View();
		}

		public ActionResult LSAAPCapa(string codFab, string numAP)
		{
			var dado = AP.ObterAP(codFab, numAP);

			var modelos = AP.ObterAPModelos(codFab, numAP);

			ViewBag.Modelos = modelos;
			return View(dado);
		}

		public ActionResult LSAAPItem(string codFab, string numAP)
		{

			var itens = AP.ObterAPItens(codFab, numAP);

			var posicoes = AP.ObterAPItensPosicoes(codFab, numAP);

			ViewBag.Posicoes = posicoes;

			return View(itens);

		}

		public ActionResult LSAAPListaXLS(string datReferencia, string codFab, string numAP, string codProcesso, string flgStatus, string numOP)
		{
			Response.AddHeader("content-disposition", "attachment; filename=OrdensProducao.xls");
			Response.ContentType = "application/ms-excel";

			var mesRef = DateTime.Parse(DateTime.Now.ToShortDateString());
			mesRef = mesRef.AddDays(-mesRef.Day).AddDays(1);

			if (!String.IsNullOrEmpty(datReferencia))
			{
				mesRef = DateTime.Parse(datReferencia);
			}

			var aps = AP.ObterAPs(mesRef);

			if (!String.IsNullOrEmpty(codFab))
			{
				aps = aps.Where(x => x.CodFab == codFab).ToList();
			}

			if (!String.IsNullOrEmpty(numAP) || !String.IsNullOrEmpty(numOP))
			{
				aps = aps.Where(x => x.NumAP == numAP || x.NumOP == numOP).ToList();
			}
			else
			{
				if (!String.IsNullOrEmpty(codProcesso))
				{
					aps = aps.Where(x => x.CodProcesso.StartsWith(codProcesso.Substring(0, 2))).ToList();
				}

				if (!String.IsNullOrEmpty(flgStatus))
				{
					if (flgStatus == "P")
					{
						aps = aps.Where(x => x.QtdLoteAP != x.QtdProduzida).ToList();
					}
					else if (flgStatus == "F")
					{
						aps = aps.Where(x => x.QtdLoteAP == x.QtdProduzida).ToList();
					}
					else if (flgStatus == "E")
					{
						aps = aps.Where(x => x.QtdLoteAP != x.QtdEmbalado).ToList();
					}
				}
			}

			return View(aps);
		}

		public JsonResult LSAAPLista(string datReferencia, string codFab, string numAP, string codProcesso, string flgStatus, string numOP)
		{
			var mesRef = DateTime.Parse(DateTime.Now.ToShortDateString());
			mesRef = mesRef.AddDays(-mesRef.Day).AddDays(1);

			if (!String.IsNullOrEmpty(datReferencia))
			{
				mesRef = DateTime.Parse(datReferencia);
			}

			var aps = AP.ObterAPs(mesRef);

			if (!String.IsNullOrEmpty(codFab))
			{
				aps = aps.Where(x => x.CodFab == codFab).ToList();
			}

			if (!String.IsNullOrEmpty(numAP) || !String.IsNullOrEmpty(numOP))
			{
				aps = aps.Where(x => x.NumAP == numAP || x.NumOP == numOP).ToList();
			}
			else
			{
				if (!String.IsNullOrEmpty(codProcesso))
				{
					aps = aps.Where(x => x.CodProcesso.StartsWith(codProcesso.Substring(0, 2))).ToList();
				}

				if (!String.IsNullOrEmpty(flgStatus))
				{
					if (flgStatus == "P")
					{
						aps = aps.Where(x => x.QtdLoteAP != x.QtdProduzida).ToList();
					}
					else if (flgStatus == "F")
					{
						aps = aps.Where(x => x.QtdLoteAP == x.QtdProduzida).ToList();
					}
					else if (flgStatus == "E")
					{
						aps = aps.Where(x => x.QtdLoteAP != x.QtdEmbalado).ToList();
					}
				}
			}
			var resultado = aps.Select(x => new
			{
				x.CodFab,
				x.NumAP,
				DatAP = x.DatAP.ToShortDateString(),
				x.CodProcesso,
				x.CodModelo,
				x.DesModelo,
				x.QtdLoteAP,
				x.QtdEmbalado,
				x.QtdProduzida,
				x.Diferenca,
				x.NumOP,
				x.CodRadial,
				x.CodLinha,
				x.CodStatus
			});

			return Json(resultado, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterAP(string numOP){

			var dados = AP.ObterAP(numOP);

			return Json(dados, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ObterPlanoIAC(string codModelo, string numRevisao)
		{

			var dados = PlanoEDP.ObterPlanoIAC(codModelo, numRevisao);

			return Json(dados, JsonRequestBehavior.AllowGet);

		}

		#region Ordem de Alimentação

		public ActionResult ComparaAlimenta(string codFab, string numAP, string codModelo)
		{

			var dados = PlanoEDP.ObterPlanoIACItem(codModelo);
			ViewBag.alimenta = dados;

			var ap = AP.ObterAPItensPosicoes(codFab, numAP, codModelo);

			return View(ap);

		}

		public ActionResult OrdemAlimenta() {

			return View();
		}

		public ActionResult ImprimeAlimenta(string numOP, string numRevisao, string numEtapa)
		{
			var lsaap = AP.ObterAP(numOP);
			ViewBag.lsaap = lsaap;

			var plano = PlanoEDP.ObterPlanoIAC(lsaap.CodModelo, numRevisao);
			ViewBag.planoIAC = plano;

			var planoEtapa = PlanoEDP.ObterPlanoEtapa(lsaap.CodModelo, numRevisao);
			if (!String.IsNullOrEmpty(numEtapa)){
				planoEtapa = planoEtapa.Where(x => x.NumEtapa.Trim() == numEtapa.Trim()).ToList();
			}
			ViewBag.planoEtapa = planoEtapa;

			var codModelo = lsaap != null ? lsaap.CodModelo : "";

			var dados = PlanoEDP.ObterPlanoIACItem(codModelo, numRevisao);
			ViewBag.alimenta = dados;

			var ap = AP.ObterAPItensPosicoes(numOP);
			ap = ap.Where(x => x.CodModelo == codModelo).ToList();

			return View(ap);

		}

		public JsonResult ObterDadosAlimenta(string codModelo, string numRevisao){

			return Json(false, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ObterRevisoes(string codModelo)
		{
			var revisoes = PlanoEDP.ObterPlanosIAC(codModelo);
			if (revisoes != null){
				revisoes = revisoes.Where(x => x.CodStatus.Trim() == "3").ToList();
			}

			return Json(revisoes, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ObterEtapas(string codModelo, string numRevisao)
		{
			var etapas = PlanoEDP.ObterPlanoEtapa(codModelo, numRevisao);

			return Json(etapas, JsonRequestBehavior.AllowGet);

		}
		#endregion

		#region Ordem de Produção SAP
		public ActionResult SAP_OP()
		{
			return View();
		}

		public ActionResult SAP_OPLista(string datInicio, string datFim, string divergentes)
		{

			var dados = AP.ObterOP(datInicio, datFim);

			if (!String.IsNullOrEmpty(divergentes))
			{
				dados = dados.Where(x => x.bDivergencia == true).ToList();
			}

			return View(dados);
		}

		public ActionResult SAP_OPOrdem(string ordem)
		{
			var dados = new BAPI_PRODORD_GET_DETAIL
			{
				NUMBER = ordem
			};
			dados.ORDER_OBJECTS.COMPONENTS = "X";
			dados.ORDER_OBJECTS.HEADER = "X";

			var retorno = SAPConnector.SAPFunctions.BAPI_PRODORD_GET_DETAIL(ref dados);
			var resb = SAPConnector.SAPFunctions.ObterRESB(ordem);

			var ap = AP.ObterAP(ordem);
			var APItem = AP.ObterAPItens(ordem);
			var Posicoes = AP.ObterAPItensPosicoes(ordem);

			ViewBag.retorno = retorno;
			ViewBag.AP = ap;
			ViewBag.APItem = APItem;
			ViewBag.resb = resb;
			ViewBag.posicoes = Posicoes;

			return View(dados);
		}

		#endregion

		#region Manutenções

		public JsonResult AlterarIdentificacao(string codfab, string numap, string codmodelo, string coditem, string codIdentifica)
		{

			var alterar = AP.AlteraIdentificacaoItem(codfab, numap, codmodelo, coditem, codIdentifica);

			return Json(alterar, JsonRequestBehavior.AllowGet);

		}

		public JsonResult AlterarModeloIMC_AP(string codFab, string numAP, string codModelo)
		{

			var resultado = AP.AlterarModeloIMC_AP(codFab, numAP, codModelo);

			return Json(resultado, JsonRequestBehavior.AllowGet);

		}

		public JsonResult AlterarLinha_AP(string codFab, string numAP, string codLinha)
		{

			var resultado = AP.AlterarLinha_AP(codFab, numAP, codLinha);

			return Json(resultado, JsonRequestBehavior.AllowGet);

		}

		#endregion

		#region SAP Functions
		[HttpPost]
		public JsonResult SAP_AtualizaOrdens(string datInicio, string datFim)
		{
			var atualiza = AP.SAP_AtualizaOrdens(datInicio, datFim);

			return Json(atualiza, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult SAP_AtualizaOrdem(string numOP)
		{
			var atualiza = AP.SAP_AtualizaOrdem(numOP);

			return Json(atualiza, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult SAP_ImportarOrdens(string datInicio, string datFim)
		{
			var atualiza = AP.SAP_ImportarOrdens(datInicio, datFim);

			return Json(atualiza, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult SAP_ImportarOrdem(string numOP)
		{
			var atualiza = AP.SAP_ImportarOrdem(numOP);

			return Json(atualiza, JsonRequestBehavior.AllowGet);
		}
		#endregion
	}
}