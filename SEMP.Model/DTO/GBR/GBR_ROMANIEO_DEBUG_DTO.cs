using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_ROMANIEO_DEBUG_DTO
    {
        public int COD_DEBUG { get; set; }
        public string DANFE { get; set; }
        public string COD { get; set; }
        public string DOC { get; set; }
        public Nullable<int> QTY { get; set; }
        public Nullable<int> SERIE { get; set; }
    }
}
