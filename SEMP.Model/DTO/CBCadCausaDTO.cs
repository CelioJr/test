using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
	public partial class CBCadCausaDTO
	{
		[Display(Name = "C�digo")]
		public string CodCausa { get; set; }
		[Display(Name = "Descri��o")]
		public string DesCausa { get; set; }
		[Display(Name = "IMC")]
		public short? flgIMC { get; set; }
		[Display(Name = "Montagem")]
		public short? flgMONTAGEM { get; set; }
		[Display(Name = "Ativo")]
		public string flgAtivo { get; set; }
	}
}
