using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_ProdDiaMap : EntityTypeConfiguration<tbl_ProdDia>
    {
        public tbl_ProdDiaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.CodLinha, t.CodModelo, t.DatProducao, t.CodCin, t.rowguid, t.FlagTP, t.NumTurno });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodLinha)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodCin)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.FlagTP)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.NumTurno)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("tbl_ProdDia");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.DatProducao).HasColumnName("DatProducao");
            this.Property(t => t.CodCin).HasColumnName("CodCin");
            this.Property(t => t.QtdProduzida).HasColumnName("QtdProduzida");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
            this.Property(t => t.QtdApontada).HasColumnName("QtdApontada");
            this.Property(t => t.QtdDivida).HasColumnName("QtdDivida");
            this.Property(t => t.FlagTP).HasColumnName("FlagTP");
            this.Property(t => t.NumTurno).HasColumnName("NumTurno");
        }
    }
}
