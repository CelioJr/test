namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_folha
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_folha()
        {
            dw_consolid = new HashSet<dw_consolid>();
            dw_consolpt = new HashSet<dw_consolpt>();
            dw_folhacic = new HashSet<dw_folhacic>();
            dw_folhaemb = new HashSet<dw_folhaemb>();
            dw_folhaiac = new HashSet<dw_folhaiac>();
            dw_folhamon = new HashSet<dw_folhamon>();
            dw_folhamedtemp = new HashSet<dw_folhamedtemp>();
            dw_folhaoperacao = new HashSet<dw_folhaoperacao>();
            dw_folharap = new HashSet<dw_folharap>();
            dw_folhasetup = new HashSet<dw_folhasetup>();
            dw_folhasetup1 = new HashSet<dw_folhasetup>();
            dw_folhateste = new HashSet<dw_folhateste>();
            dw_folhatv = new HashSet<dw_folhatv>();
            dw_rotapasso = new HashSet<dw_rotapasso>();
            om_mapa = new HashSet<om_mapa>();
            om_obj = new HashSet<om_obj>();
            om_pt = new HashSet<om_pt>();
            pp_cp = new HashSet<pp_cp>();
        }

        [Key]
        public long id_folha { get; set; }

        [StringLength(30)]
        public string cd_folha { get; set; }

        public int? revisao { get; set; }

        public DateTime? dt_revisao { get; set; }

        public short? st_ativo { get; set; }

        public DateTime? dt_stativo { get; set; }

        [StringLength(40)]
        public string ds_folha { get; set; }

        public long id_usrstativo { get; set; }

        public long id_usrrevisao { get; set; }

        public decimal? seg_ciclopadrao { get; set; }

        public decimal? seg_ciclotimeout { get; set; }

        public decimal? seg_ciclominimo { get; set; }

        public short? is_modelo { get; set; }

        public short? tp_folha { get; set; }

        public short? is_logonobrigatorio { get; set; }

        public decimal? seg_logoutauto { get; set; }

        public long id_tppt { get; set; }

        public long id_gt { get; set; }

        public decimal? seg_setup { get; set; }

        public long? id_pt { get; set; }

        public long? id_procedimentosetup { get; set; }

        public long? id_procedimentoMontagem { get; set; }

        public int? qt_pacoteciclo { get; set; }

        public long? id_cfgscrpimp { get; set; }

        public long? id_regras_nscb { get; set; }

        public decimal? qt_fatorcontagem { get; set; }

        public short? tp_validacao { get; set; }

        public short? limite_min_cb { get; set; }

        public short? limite_max_cb { get; set; }

        public short? is_avaliarlimites { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolid> dw_consolid { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolpt> dw_consolpt { get; set; }

        public virtual om_cfgscrpimp om_cfgscrpimp { get; set; }

        public virtual om_gt om_gt { get; set; }

        public virtual dw_procedimento dw_procedimento { get; set; }

        public virtual om_regras_nscb om_regras_nscb { get; set; }

        public virtual om_tppt om_tppt { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhacic> dw_folhacic { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhaemb> dw_folhaemb { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhaiac> dw_folhaiac { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhamon> dw_folhamon { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhamedtemp> dw_folhamedtemp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhaoperacao> dw_folhaoperacao { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folharap> dw_folharap { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhasetup> dw_folhasetup { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhasetup> dw_folhasetup1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhateste> dw_folhateste { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhatv> dw_folhatv { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rotapasso> dw_rotapasso { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_mapa> om_mapa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_obj> om_obj { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_pt> om_pt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cp> pp_cp { get; set; }
    }
}
