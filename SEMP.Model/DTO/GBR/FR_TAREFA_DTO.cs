using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_TAREFA_DTO
    {
        public FR_TAREFA_DTO()
        {
            this.FR_TAREFA_TEMPO = new List<FR_TAREFA_TEMPO_DTO>();
        }

        public int TRF_CODIGO { get; set; }
        public string TRF_DESCRICAO { get; set; }
        public string SIS_CODIGO { get; set; }
        public int REG_CODIGO { get; set; }
        public Nullable<System.DateTime> TRF_DATA_INICIAL { get; set; }
        public Nullable<System.DateTime> TRF_DATA_FINAL { get; set; }
        public string TRF_ATIVA { get; set; }
        public string TRF_REGRA_PARAMETROS { get; set; }
        public string TRF_TIPO_AGENDAMENTO { get; set; }
        public virtual FR_REGRAS_DTO FR_REGRAS { get; set; }
        public virtual FR_SISTEMA_DTO FR_SISTEMA { get; set; }
        public virtual ICollection<FR_TAREFA_TEMPO_DTO> FR_TAREFA_TEMPO { get; set; }
    }
}
