﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.DTO
{
	public class CBRetrabalhoDTO
	{
		public int CodRetrabalho { get; set; }		
		public string Justificativa { get; set; }
		public System.DateTime DatEvento { get; set; }
		public string CodDRT { get; set; }
		public int Status { get; set; }
		public string CodFab { get; set; }
		public string NumAP { get; set; }
		public int Quantidade { get; set; }				
	}
}
