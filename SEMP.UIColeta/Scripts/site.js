﻿
var DadosTela = {
	nomUsuario: ko.observable(),
	nomDepto: ko.observable(),
	pagina: ko.observable()
}

var Posto = {
	CodLinha: ko.observable(),
	NumPosto: ko.observable(),
	DesLinha: ko.observable(),
	DesPosto: ko.observable(),
}

var ViewModel = function (nomUsuario, nomDepto)
{

	self = this;

	self.dadosTela = DadosTela;
	self.dadosTela.nomUsuario(nomUsuario);
	self.dadosTela.nomDepto(nomDepto);
	self.dataAtual = ko.observable("");

	self.posto = Posto;

	self.Leitura = ko.observable("");
	self.Mensagem = ko.observable("");
	self.Status = ko.observable("");

	self.AtualizaPagina = function (titulo) {
		self.dadosTela.pagina(titulo);
		return true;
	}

	self.AtualizaData = function() {
		self.dataAtual(Relogio());
	}

}

function showtime() {
	var now = new Date();
	var hours = now.getHours();
	var minutes = now.getMinutes();
	var seconds = now.getSeconds();
	var timeValue = "" + (hours);
	timeValue += ((minutes < 10) ? ":0" : ":") + minutes;
	timeValue += ((seconds < 10) ? ":0" : ":") + seconds;

	return timeValue;
};

function showdate() {
	var now = new Date();
	var year = now.getFullYear();
	var month = now.getMonth() + 1;
	var day = now.getDate();
	var dateValue = "";
	dateValue += ((day < 10) ? "0" : "") + day;
	dateValue += ((month < 10) ? "/0" : "/") + month;
	dateValue += '/' + year;

	return dateValue;
};

function Relogio() {
	var data = showdate() + ' ' + showtime();
	return data;
};