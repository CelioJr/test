namespace SEMP.Model.DTO
{
	using System;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;

	public partial class ORDER_TEXT_DTO
	{
        public Guid ID { get; set; }

        public Guid ORDER_HEADER_IN_ID { get; set; }

        [StringLength(4)]
        public string TEXT_ID { get; set; }

        [Column(TypeName = "text")]
        public string TEXT_LINE { get; set; }
    }
}
