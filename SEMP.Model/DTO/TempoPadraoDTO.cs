using System;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
	public class TempoPadraoDTO
	{
		[Display(Name = "Modelo")]
		public string CodModelo { get; set; }

		[Display(Name = "Tempo Padr�o")]
		[DisplayFormat(DataFormatString = "{0:N4}")]
		public decimal? TempoPadrao { get; set; }

		[Display(Name = "Tempo LP")]
		[DisplayFormat(DataFormatString = "{0:N4}")]
		public decimal? TempoLp { get; set; }

		[Display(Name = "Cod. Pai")]
		public string CodPai { get; set; }

		[Display(Name = "Usu�rio")]
		public string Usuario { get; set; }

		public System.Guid rowguid { get; set; }

		[Display(Name = "Descri��o")]
		public string DesModelo { get; set; }

		[Display(Name = "Ativo")]
		public string flgAtivo { get; set; }
	}
}
