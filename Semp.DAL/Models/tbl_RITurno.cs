using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_RITurno
    {
        public string CodTurno { get; set; }
        public string DesTurno { get; set; }
        public string NumCarga { get; set; }
        public string NumCargaSem { get; set; }
        public string NumCargaMes { get; set; }
        public Nullable<short> TipoDia1 { get; set; }
        public Nullable<short> TipoDia2 { get; set; }
        public Nullable<short> TipoDia3 { get; set; }
        public Nullable<short> TipoDia4 { get; set; }
        public Nullable<short> TipoDia5 { get; set; }
        public Nullable<short> TipoDia6 { get; set; }
        public Nullable<short> TipoDia7 { get; set; }
        public Nullable<short> TipoDia8 { get; set; }
        public Nullable<short> TipoDia9 { get; set; }
        public string CodJornada1 { get; set; }
        public string CodJornada2 { get; set; }
        public string CodJornada3 { get; set; }
        public string CodJornada4 { get; set; }
        public string CodJornada5 { get; set; }
        public string CodJornada6 { get; set; }
        public string CodJornada7 { get; set; }
        public string CodJornada8 { get; set; }
        public string CodJornada9 { get; set; }
        public Nullable<System.DateTime> DataFolga { get; set; }
    }
}
