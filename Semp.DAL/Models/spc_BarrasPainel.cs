﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.Models
{
	public class spc_BarrasPainel
	{

		public string Linha { get; set; }
		public decimal? Producao { get; set; }
		public decimal? Embalados { get; set; }
		public int? Diferenca { get; set; }
		public int? ProdEsperada { get; set; }
		public decimal? AparHora { get; set; }
		public decimal? AparMin { get; set; }
		public decimal? HorasTrab { get; set; }
		public decimal? MinutosDia { get; set; }
		public decimal? QtdMinReal { get; set; }
		public decimal? QtdApontado { get; set; }
		public int? QtdLoteAP { get; set; }
		public int? QtdEmbaladoAP { get; set; }
		public decimal? DifModelo { get; set; }
		public int? DifUltLeitura { get; set; }
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public string FlgValida { get; set; }
		public string CodFam { get; set; }
		public DateTime? DatValida { get; set; }
		public DateTime? HoraInicio { get; set; }
		public DateTime? HoraFim { get; set; }
		public string CodLinha { get; set; }
		public DateTime? UltLeitura { get; set; }
		public string NumAP { get; set; }
		public string FlgDivida { get; set; }
		public decimal? TempoParaAcabar { get; set; }
		public DateTime? HoraPrevista { get; set; }
		public decimal? RitmoAtual { get; set; }
		public string StatusLinha { get; set; }
		public float? IntervaloTrans { get; set; }
		public decimal? QtdCapacidade { get; set; }
		public DateTime? DataHoraCongelada { get; set; }

	}
}
