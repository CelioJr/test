namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_DATABASE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FR_DATABASE()
        {
            FR_COMPILADOR_DATABASE = new HashSet<FR_COMPILADOR_DATABASE>();
        }

        [Key]
        [StringLength(10)]
        public string DBA_CODIGO { get; set; }

        [Required]
        [StringLength(50)]
        public string DBA_DESCRICAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_COMPILADOR_DATABASE> FR_COMPILADOR_DATABASE { get; set; }
    }
}
