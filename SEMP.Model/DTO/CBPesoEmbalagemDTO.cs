﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.DTO
{
    public class CBPesoEmbalagemDTO
    {
        public string Modelo { get; set; }
        public string PesoMin { get; set; }
        public string PesoMax { get; set; }
    }
}
