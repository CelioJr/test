﻿using RAST.BO.Relatorios;
using SEMP.BO.Integrator;
using System;
using System.Web.Mvc;

namespace SEMP.Areas.Outros.Controllers
{
	public class SAPController : Controller
    {
        // GET: Outros/SAP
        public ActionResult AuditorIntegra()
        {
            return View();
        }

		public ActionResult AuditorIntegraLista(string datInicio, string datFim, string numOP, string codModelo){

			DateTime dInicio = DateTime.Parse(DateTime.Now.ToShortDateString());
			dInicio = dInicio.AddDays(-dInicio.Day).AddDays(1);
			DateTime dFim = dInicio.AddMonths(1).AddDays(-1);

			if (!String.IsNullOrEmpty(datInicio)){
				dInicio = DateTime.Parse(datInicio);
			}
			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}

			var dados = SAPConnector.SAPFunctions.Obter_ZTPP_APONPCI(dInicio, dFim, numOP, codModelo);

			var integrador = Integrador.ObterApontamento(dInicio, dFim, numOP, codModelo);

			ViewBag.integrador = integrador;
			return View(dados);

		}

		public ActionResult AuditorIntegraQtd(string datInicio, string datFim, string numOP, string codModelo)
		{

			DateTime dInicio = DateTime.Parse(DateTime.Now.ToShortDateString());
			dInicio = dInicio.AddDays(-dInicio.Day).AddDays(1);
			DateTime dFim = dInicio.AddMonths(1).AddDays(-1);

			if (!String.IsNullOrEmpty(datInicio))
			{
				dInicio = DateTime.Parse(datInicio);
			}
			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}

			var dados = SAPConnector.SAPFunctions.Obter_ZTPP_APONPCI(dInicio, dFim, numOP, codModelo);

			var integrador = Integrador.ObterApontamento(dInicio, dFim, numOP, codModelo);

			var producao = Acompanhamento.ObterLeiturasIMC(dInicio, dFim, "", "", "");

			ViewBag.integrador = integrador;
			ViewBag.sap = dados;
			
			return View(producao);

		}
	}
}