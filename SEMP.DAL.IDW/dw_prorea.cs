namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_prorea
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_prorea()
        {
            dw_proreausr = new HashSet<dw_proreausr>();
            dw_proreaativ = new HashSet<dw_proreaativ>();
        }

        [Key]
        public long id_prorea { get; set; }

        public long? id_procedimento { get; set; }

        public long? id_consolid { get; set; }

        public short? st_prorea { get; set; }

        public virtual dw_consolid dw_consolid { get; set; }

        public virtual dw_procedimento dw_procedimento { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_proreausr> dw_proreausr { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_proreaativ> dw_proreaativ { get; set; }
    }
}
