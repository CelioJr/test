using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_WIPProducaoMap : EntityTypeConfiguration<tbl_WIPProducao>
    {
        public tbl_WIPProducaoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodLocal, t.CodItem, t.Familia });

            // Properties
            this.Property(t => t.CodLocal)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.CodItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6);

            this.Property(t => t.Familia)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("tbl_WIPProducao");
            this.Property(t => t.CodLocal).HasColumnName("CodLocal");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.Familia).HasColumnName("Familia");
            this.Property(t => t.QtdEstoque).HasColumnName("QtdEstoque");
            this.Property(t => t.Valor).HasColumnName("Valor");
        }
    }
}
