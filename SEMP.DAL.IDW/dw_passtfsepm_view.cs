namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_passtfsepm_view
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id_passtfsepm { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id_passtfse { get; set; }

        public DateTime? dthr_medicao { get; set; }

        public int? ms_dthrmedicao { get; set; }

        public decimal? vlcorrente { get; set; }

        public decimal? tensao { get; set; }

        public short? fluxoe { get; set; }

        public short? fluxos { get; set; }

        [StringLength(1)]
        public string st_fase { get; set; }
    }
}
