﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
	public class SeriesAusentes
	{
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public string Serie { get; set; }
	}
}
