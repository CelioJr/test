using System;

namespace SEMP.Model.DTO
{
    public class CBPadraoIndicadorDTO
    {
        public int Indicador { get; set; }
        public string Fabrica { get; set; }
        public int CodLinha { get; set; }
        public Nullable<decimal> Valor { get; set; }
    }
}
