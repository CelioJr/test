namespace SEMP.Model.DTO
{
	using System;
	using System.ComponentModel.DataAnnotations;

	public partial class ORDER_SCHEDULES_IN_DTO
	{
        public Guid ID { get; set; }

        public Guid ORDER_ITEMS_IN_ID { get; set; }

        public int? ITM_NUMBER { get; set; }

        public decimal? REQ_QTY { get; set; }

        [StringLength(10)]
        public string REQ_DATE { get; set; }

    }
}
