using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class UsuarioDTO
    {
        public string CodFab { get; set; }
        public string NomUsuario { get; set; }
        public string NomDepto { get; set; }
        public string FullName { get; set; }
        public string FlgAtivo { get; set; }
        public string Email { get; set; }
        public string NomDeptoAD { get; set; }
        public string Cpf { get; set; }
    }
}
