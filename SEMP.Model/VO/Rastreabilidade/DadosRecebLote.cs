﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade
{
	public class DadosRecebLote
	{
		[Key]
		public DateTime DatInicio { get; set; }
		public DateTime DatFim { get; set; }
		public int CodLinha { get; set; }
		public int QtdLoteDia { get; set; }
		public int QtdCCDia { get; set; }
		public string NumUltLote { get; set; }
		public int QtdUltLote { get; set; }
		public string NumUltCC { get; set; }
		public int QtdUltCC { get; set; }
		public string NumLotePaiCC { get; set; }
		public int QtdTotalPaiCC { get; set; }
		public int QtdRecPaiCC { get; set; }

	}
}
