using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_REGRAS_FUNCOES_DTO
    {
        public int FUN_COD { get; set; }
        public string FUN_NOME { get; set; }
        public string FUN_DESCRICAO { get; set; }
        public string FUN_NOME_REAL { get; set; }
        public string FUN_PARAMS { get; set; }
        public string FUN_RETORNO { get; set; }
        public int FUN_TIPO { get; set; }
        public string FUN_COMPATIBILIDADE { get; set; }
        public string FUN_RESUMO { get; set; }
        public string FUN_CONTEUDO_SERVIDOR { get; set; }
        public string FUN_CONTEUDO_CLIENTE { get; set; }
        public string FUN_CONTEUDO_BANCO { get; set; }
        public string FUN_VERSAO { get; set; }
    }
}
