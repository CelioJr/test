namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ms_pt_coleta
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ms_pt_coleta()
        {
            ms_pt_coleta_login = new HashSet<ms_pt_coleta_login>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id_pt { get; set; }

        public DateTime? dthr_heartbeat { get; set; }

        public DateTime? dthr_iparada { get; set; }

        public DateTime? dthr_fparada { get; set; }

        public long? id_tparada { get; set; }

        public long? id_tjust { get; set; }

        public long? id_tcausa { get; set; }

        public long? id_tacao { get; set; }

        public long? id_usrTecnicoUm { get; set; }

        public long? id_usrTecnicoDois { get; set; }

        public long? id_usrTecnicoResp { get; set; }

        [StringLength(100)]
        public string nrop { get; set; }

        public short? is_parada { get; set; }

        public virtual dw_t_acao dw_t_acao { get; set; }

        public virtual dw_t_causa dw_t_causa { get; set; }

        public virtual dw_t_just dw_t_just { get; set; }

        public virtual dw_t_parada dw_t_parada { get; set; }

        public virtual om_pt om_pt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_pt_coleta_login> ms_pt_coleta_login { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        public virtual om_usr om_usr2 { get; set; }
    }
}
