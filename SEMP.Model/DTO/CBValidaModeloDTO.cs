namespace SEMP.Model.DTO
{
    public class CBValidaModeloDTO
    {
        public int NumPosto { get; set; }
        public string CodModelo { get; set; }
        public int CodLinha { get; set; }
        public string Setor { get; set; }
    }
}
