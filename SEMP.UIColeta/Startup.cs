﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(SEMP.UIColeta.Startup))]

namespace SEMP.UIColeta
{
	public class Startup
	{
		public void Configuration(IAppBuilder app)
		{
			app.MapSignalR();
		}

	}
}