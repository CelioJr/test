namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_indisp
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pp_indisp()
        {
            pp_indisp_rappt = new HashSet<pp_indisp_rappt>();
        }

        [Key]
        public long id_indisp { get; set; }

        [StringLength(30)]
        public string cd_indisp { get; set; }

        public int? revisao { get; set; }

        [StringLength(256)]
        public string ds_indisp { get; set; }

        public DateTime? dt_revisao { get; set; }

        public DateTime? dt_stativo { get; set; }

        public short? st_ativo { get; set; }

        public long id_usrrevisao { get; set; }

        public long id_usrstativo { get; set; }

        public short? st_indisp { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_indisp_rappt> pp_indisp_rappt { get; set; }
    }
}
