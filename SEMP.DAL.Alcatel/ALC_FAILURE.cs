namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_FAILURE")]
    public partial class ALC_FAILURE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ALC_FAILURE()
        {
            ALC_DOWNTIME = new HashSet<ALC_DOWNTIME>();
        }

        [Key]
        [StringLength(5)]
        public string ID_FAILURE { get; set; }

        [StringLength(200)]
        public string DESCRIPTION { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_DOWNTIME> ALC_DOWNTIME { get; set; }
    }
}
