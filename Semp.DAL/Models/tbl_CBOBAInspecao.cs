using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBOBAInspecao
    {
		public string NumECB { get; set; }
		public int CodLinha { get; set; }
		public int NumPosto { get; set; }
		public string CodFab { get; set; }
		public string NumRPA { get; set; }
		public string CodPallet { get; set; }
		public Nullable<System.DateTime> DatLeitura { get; set; }
		public string FlgStatus { get; set; }
		public string Observacao { get; set; }
	}
}
