using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class viw_ItemMap : EntityTypeConfiguration<viw_Item>
    {
        public viw_ItemMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodItem, t.DesItem });

            // Properties
            this.Property(t => t.CodItem)
                .IsRequired()
                .HasMaxLength(15)
                .IsUnicode(false);

            this.Property(t => t.DesItem)
                .IsRequired()
                .HasMaxLength(40)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("viw_Item");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.DesItem).HasColumnName("DesItem");
        }
    }
}
