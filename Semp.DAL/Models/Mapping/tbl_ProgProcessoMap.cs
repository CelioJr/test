using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_ProgProcessoMap : EntityTypeConfiguration<tbl_ProgProcesso>
    {
        public tbl_ProgProcessoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodTipo, t.CodProcesso });

            // Properties
            this.Property(t => t.CodTipo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.CodProcesso)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.DesTipo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.DesTitulo)
                .HasMaxLength(30)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_ProgProcesso");
            this.Property(t => t.CodTipo).HasColumnName("CodTipo");
            this.Property(t => t.CodProcesso).HasColumnName("CodProcesso");
            this.Property(t => t.DesTipo).HasColumnName("DesTipo");
            this.Property(t => t.DesTitulo).HasColumnName("DesTitulo");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
        }
    }
}
