namespace SEMP.DAL.DTB_SSA.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_Key_Component_STI
    {
        public string numSerie { get; set; }
        public string NE { get; set; }
        public string serialDispositivo { get; set; }
        public System.DateTime dataRegistro { get; set; }
        public int? codOperador { get; set; }
        public DateTime? dataDesativacao { get; set; }
        public int? codOpeDesat { get; set; }
        public int? dispositivo_Id { get; set; }
    
        public virtual tbl_ItemEstoque tbl_ItemEstoque { get; set; }
    }
}
