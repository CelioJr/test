using System;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
    public class CnqCausaDTO
    {
		[Display(Name = "C�digo")]
        public string CodCausa { get; set; }
		[Display(Name = "Descri��o")]
        public string DesCausa { get; set; }
		[Display(Name = "IMC")]
        public Nullable<short> flgIMC { get; set; }
		[Display(Name = "Montagem")]
        public Nullable<short> flgMONTAGEM { get; set; }
		[Display(Name = "Ativo")]
        public string flgAtivo { get; set; }
    }
}
