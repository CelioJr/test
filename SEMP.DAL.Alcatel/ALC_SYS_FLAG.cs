namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_SYS_FLAG")]
    public partial class ALC_SYS_FLAG
    {
        [Key]
        [StringLength(30)]
        public string ID_FLAG { get; set; }

        public int? NEXT_VALUE { get; set; }
    }
}
