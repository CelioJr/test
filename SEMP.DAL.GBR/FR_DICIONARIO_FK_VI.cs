namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_DICIONARIO_FK_VI
    {
        [Key]
        public string TABELA { get; set; }

        [StringLength(128)]
        public string TABELAFK { get; set; }

        [StringLength(128)]
        public string CAMPOFK { get; set; }
    }
}
