using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_INTERVAL_PROD_DTO
    {
        public int idHour { get; set; }
        public Nullable<int> finalHour { get; set; }
        public Nullable<int> finalMin { get; set; }
        public Nullable<int> inicialHour { get; set; }
        public Nullable<int> inicialMin { get; set; }
        public Nullable<int> shift_idShift { get; set; }
        public virtual ALC_SHIFT_PROD_DTO ALC_SHIFT_PROD { get; set; }
    }
}
