namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_REPAIR_REASON")]
    public partial class ALC_REPAIR_REASON
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ALC_REPAIR_REASON()
        {
            ALC_REPAIR = new HashSet<ALC_REPAIR>();
            ALC_RMB_REGISTER = new HashSet<ALC_RMB_REGISTER>();
        }

        [Key]
        [StringLength(255)]
        public string ID_REASON { get; set; }

        [StringLength(255)]
        public string DESCRIPTION { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_REPAIR> ALC_REPAIR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_RMB_REGISTER> ALC_RMB_REGISTER { get; set; }
    }
}
