using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
	public partial class MAPScrapBaixaDTO
	{
		
		public int IDScrap { get; set; }
		[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
		[Display(Name = "Data")]
		public DateTime DatReferencia { get; set; }
		[Display(Name = "Modelo")]
		public string CodModelo { get; set; }
		[Display(Name = "Des. Modelo")]
		public string DesModelo { get; set; }
		[Display(Name = "Item")]
		public string CodItem { get; set; }
		[Display(Name = "Des. Item")]
		public string DesItem { get; set; }
		[DisplayFormat(DataFormatString = "{0:0}", ApplyFormatInEditMode = true)]
		[Display(Name = "Perda")]
		public decimal? QtdPerda { get; set; }
		[Display(Name = "Valor")]
		public decimal? ValTotal { get; set; }
		[Display(Name = "Baixa")]
		public decimal? QtdBaixa { get; set; }
		[Display(Name = "F�brica")]
		public string CodFabBxa { get; set; }
		[Display(Name = "Qtd. Estoque")]
		public decimal? QtdEstoque { get; set; }
		[Display(Name = "Data Baixa")]
		public DateTime? DatBaixa { get; set; }
		[Display(Name = "Data Gera��o")]
		public DateTime? DatGeracao { get; set; }
		[Display(Name = "Usu�rio")]
		public string NomUsuGerou { get; set; }
	}
}
