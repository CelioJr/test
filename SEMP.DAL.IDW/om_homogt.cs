namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_homogt
    {
        [Key]
        public long id_homogt { get; set; }

        public DateTime? dt_homogt { get; set; }

        public short? tp_homogt { get; set; }

        public long id_usrhomologado { get; set; }

        public long id_usr { get; set; }

        public long id_gt { get; set; }

        public virtual om_gt om_gt { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }
    }
}
