using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_REPAIR_DTO
    {
        public int ID_REPAIR { get; set; }
        public string COMPONENT { get; set; }
        public Nullable<System.DateTime> DT_ENTRY { get; set; }
        public Nullable<System.DateTime> DT_REPAIR { get; set; }
        public string OBSERVATION { get; set; }
        public string STEP { get; set; }
        public Nullable<int> ID_ACTION { get; set; }
        public string ID_DEFECT { get; set; }
        public int COD_LINE { get; set; }
        public int ID_POST { get; set; }
        public string ID_REASON { get; set; }
        public int ID_SERIAL { get; set; }
        public int COD_SKU { get; set; }
        public Nullable<int> TECHNICAL { get; set; }
        public string STATUS_ENG { get; set; }
        public Nullable<decimal> DURATION_TIME { get; set; }
        public Nullable<int> ENTRY_USER { get; set; }
        public Nullable<int> ID_ORIGIN { get; set; }
        public virtual ALC_DEFECT_DTO ALC_DEFECT { get; set; }
        public virtual ALC_POST_USER_DTO ALC_POST_USER { get; set; }
        public virtual ALC_REPAIR_ORIGIN_DTO ALC_REPAIR_ORIGIN { get; set; }
        public virtual ALC_SYS_USER_DTO ALC_SYS_USER { get; set; }
        public virtual ALC_SYS_USER_DTO ALC_SYS_USER1 { get; set; }
        public virtual ALC_REPAIR_ACTION_DTO ALC_REPAIR_ACTION { get; set; }
        public virtual ALC_REPAIR_REASON_DTO ALC_REPAIR_REASON { get; set; }
    }
}
