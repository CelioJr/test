﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using SEMP.DAL.Models;
using SEMP.Model.DTO;
using SEMP.Model.VO.PM;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;

namespace SEMP.DAL.DAO.PM
{
    public class daoParadaDeLinha
	{
        //static daoParadaDeLinha()
        //{
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<tbl_ProdParada, ProdParadaDTO>();
        //        cfg.CreateMap<ProdParadaDTO, tbl_ProdParada>();
        //        cfg.CreateMap<spc_PM009, ResumoHorasParadas>();
        //        cfg.CreateMap<spc_PM019, ResumoDetalhesHorasParadas>();
        //        cfg.CreateMap<tbl_ProdParadaDep, ProdParadaDepDTO>();

        //    });
        //}

        #region Paradas de Linha
        public static TimeSpan ToTimeSpan(String hora)
		{
			//DateTime t = DateTime.ParseExact(hora, "hh:mm", CultureInfo.InvariantCulture);
			if (!hora.Trim().Equals(String.Empty))
			{
				TimeSpan thora = TimeSpan.Parse(hora);//t.TimeOfDay;

				return thora;
			}
			else
			{
				return new TimeSpan();
			}
		}

		public static List<ProdParadaDTO> ObterParadas(DateTime DatInicio, DateTime DatFim, int Turno, string Linha)
		{
			TimeSpan HoraInicio;
			TimeSpan HoraFim;

			if (Turno == 1)
			{
				HoraInicio = new TimeSpan(07, 0, 0);
				HoraFim = new TimeSpan(17, 7, 59);
			}
			else
			{
				HoraInicio = new TimeSpan(17, 8, 0);
				HoraFim = new TimeSpan(02, 20, 0);
			}

			using (var dtbSim = new DTB_SIMContext())
			{

				var paradas = (from p in dtbSim.tbl_ProdParada
							   join pp in dtbSim.tbl_Parada on new { p.CodParada, p.CodFab } equals new { pp.CodParada, pp.CodFab }
							   where p.DatParada >= DatInicio && p.DatParada <= DatFim && p.CodLinha.Trim() != String.Empty
										&& p.CodLinha == Linha
							   select new
							   {
								   CodFab = p.CodFab,
								   CodItem = p.CodItem,
								   CodLinha = p.CodLinha,
								   CodModelo = p.CodModelo,
								   CodParada = p.CodParada,
								   CodTurno = p.CodTurno,
								   DatParada = p.DatParada,
								   DesObs = p.DesObs + " - " + pp.DesParada,
								   HorAlmoco = p.HorAlmoco,
								   HorFim = p.HorFim,
								   HorInicio = p.HorInicio,
								   NumDrt = p.NumDrt,
								   NumFuncionarios = p.NumFuncionarios,
								   NumParada = p.NumParada,
								   QtdFalta = p.QtdFalta,
								   QtdItem = p.QtdItem,
								   QtdNaoProduzida = p.QtdNaoProduzida,
								   rowguid = p.rowguid,
								   StatusEmail = p.StatusEmail,
								   StatusPerda = p.StatusPerda
							   }).ToList();

				//List<tbl_ProdParada> parada = paradas.Where(x => (ToTimeSpan(x.HorInicio) >= HoraInicio && ToTimeSpan(x.HorInicio) <= HoraFim)).Select(p => new tbl_ProdParada

				List<tbl_ProdParada> parada = paradas.Where(x =>
					HoraInicio < HoraFim ?
						(ToTimeSpan(x.HorInicio) >= HoraInicio && ToTimeSpan(x.HorInicio) <= HoraFim)
					:
						(ToTimeSpan(x.HorInicio) >= HoraInicio && ToTimeSpan(x.HorFim) <= HoraFim)
				).Select(p => new tbl_ProdParada
				{
					CodFab = p.CodFab,
					CodItem = p.CodItem,
					CodLinha = p.CodLinha,
					CodModelo = p.CodModelo,
					CodParada = p.CodParada,
					CodTurno = p.CodTurno,
					DatParada = p.DatParada,
					DesObs = p.DesObs,
					HorAlmoco = p.HorAlmoco,
					HorFim = p.HorFim,
					HorInicio = p.HorInicio,
					NumDrt = p.NumDrt,
					NumFuncionarios = p.NumFuncionarios,
					NumParada = p.NumParada,
					QtdFalta = p.QtdFalta,
					QtdItem = p.QtdItem,
					QtdNaoProduzida = p.QtdNaoProduzida,
					rowguid = p.rowguid,
					StatusEmail = p.StatusEmail,
					StatusPerda = p.StatusPerda
				}).ToList();

				List<ProdParadaDTO> paradasDTO = new List<ProdParadaDTO>();

				AutoMapper.Mapper.Map(parada, paradasDTO);

				return paradasDTO;
			}

		}

		public static List<ProdParadaDTO> ObterParadas(DateTime datInicio, DateTime datFim, string linha, string codFab, string codModelo, string tipoParada, string status, string codDepto)
		{

			using (var dtbSim = new DTB_SIMContext())
			{

				var paradas = (from p in dtbSim.tbl_ProdParada
							   from item in dtbSim.tbl_Item.Where(x => x.CodItem.Equals(p.CodItem)).DefaultIfEmpty()
							   from modelo in dtbSim.tbl_Item.Where(x => x.CodItem.Equals(p.CodModelo)).DefaultIfEmpty()
							   from familia in dtbSim.tbl_Familia.Where(x => x.CodFam.Equals(modelo.CodFam)).DefaultIfEmpty()
							   from pd in dtbSim.tbl_ProdParadaDep.Where(x => x.NumParada == p.NumParada && x.CodFab == p.CodFab).DefaultIfEmpty()
							   from pdi in dtbSim.tbl_ProdParadaDepItem.Where(x => x.CodFab.Equals(p.CodFab) && x.NumParada.Equals(p.NumParada) && x.CodDepto.Equals(pd.CodDepto)).DefaultIfEmpty()
							   from l in dtbSim.tbl_Linha.Where(x => x.CodLinha.Equals(p.CodLinha)).DefaultIfEmpty()
							   from fab in dtbSim.tbl_Fabrica.Where(x => x.CodFab.Equals(p.CodFab)).DefaultIfEmpty()
							   from parada in dtbSim.tbl_Parada.Where(x => x.CodParada == p.CodParada && x.CodFab == p.CodFab).DefaultIfEmpty()
							   where p.DatParada >= datInicio && p.DatParada <= datFim && p.CodFab.Equals(codFab)
							   select new
							   {
								   Familia = p.CodLinha.Substring(0, 3) == "110" ? "IAC" : p.CodLinha.Substring(0, 3) == "111" ? "IMC" : familia.NomFam,
								   CodFab = p.CodFab,
								   CodItem = p.CodItem,
								   CodLinha = p.CodLinha,
								   CodModelo = p.CodModelo,
								   CodParada = p.CodParada,
								   CodTurno = p.CodTurno,
								   DatParada = p.DatParada,
								   DesObs = p.DesObs,
								   HorAlmoco = p.HorAlmoco,
								   HorFim = p.HorFim,
								   HorInicio = p.HorInicio,
								   NumDrt = p.NumDrt,
								   NumFuncionarios = p.NumFuncionarios,
								   NumParada = p.NumParada,
								   QtdFalta = p.QtdFalta,
								   QtdItem = p.QtdItem,
								   QtdNaoProduzida = p.QtdNaoProduzida,
								   rowguid = p.rowguid,
								   StatusEmail = p.StatusEmail,
								   StatusPerda = p.StatusPerda,
								   CodDepto = pd.CodDepto,
								   TipItem = pd.TipItem,
								   DatRetorno = pdi.DatRetorno,
								   DesModelo = modelo.DesItem,
								   DesItem = item.DesItem,
								   DesLinha = l.DesLinha,
								   DesFab = fab.NomFab,
								   DesParada = parada.DesParada,
								   DesCausa = pdi.DesCausa
							   });

				if (!String.IsNullOrEmpty(linha))
				{
					paradas = paradas.Where(x => x.CodLinha.Equals(linha));
				}
				if (!String.IsNullOrEmpty(codModelo))
				{
					paradas = paradas.Where(x => x.CodModelo.Equals(codModelo));
				}
				if (!String.IsNullOrEmpty(codDepto))
				{
					paradas = paradas.Where(x => x.CodDepto.Equals(codDepto));
				}

				if (!String.IsNullOrEmpty(tipoParada))
				{
					paradas = paradas.Where(x => x.TipItem.Equals(tipoParada));
					if (tipoParada.Equals("O") && !String.IsNullOrEmpty(status))
					{
						paradas = paradas.Where(x => x.DatRetorno == null);
					}
				}
				var resultado = paradas.GroupBy(p => new
				{
					p.CodFab,
					p.CodItem,
					p.CodLinha,
					p.CodModelo,
					p.CodParada,
					p.CodTurno,
					p.DatParada,
					p.DesObs,
					p.HorAlmoco,
					p.HorFim,
					p.HorInicio,
					p.NumDrt,
					p.NumFuncionarios,
					p.NumParada,
					p.QtdFalta,
					p.QtdItem,
					p.QtdNaoProduzida,
					p.rowguid,
					p.StatusEmail,
					p.StatusPerda,
					p.DesItem,
					p.DesLinha,
					p.DesFab,
					p.DesParada,
					p.DesModelo,
					p.Familia,
					p.TipItem,
					p.DesCausa
				}).Select(p => new ProdParadaDTO()
				{
					CodFab = p.Key.CodFab,
					CodItem = p.Key.CodItem,
					CodLinha = p.Key.CodLinha,
					CodModelo = p.Key.CodModelo,
					CodParada = p.Key.CodParada,
					CodTurno = p.Key.CodTurno,
					DatParada = p.Key.DatParada,
					DesObs = p.Key.DesObs,
					HorAlmoco = p.Key.HorAlmoco,
					HorFim = p.Key.HorFim,
					HorInicio = p.Key.HorInicio,
					NumDrt = p.Key.NumDrt,
					NumFuncionarios = p.Key.NumFuncionarios,
					NumParada = p.Key.NumParada,
					QtdFalta = p.Key.QtdFalta,
					QtdItem = p.Key.QtdItem,
					QtdNaoProduzida = p.Key.QtdNaoProduzida,
					rowguid = p.Key.rowguid,
					StatusEmail = p.Key.StatusEmail,
					StatusPerda = p.Key.StatusPerda,
					DesModelo = p.Key.DesModelo,
					DesItem = p.Key.DesItem,
					DesLinha = p.Key.DesLinha,
					DesFabrica = p.Key.DesFab,
					DesParada = p.Key.DesParada,
					QtdRespostas = p.Count(x => x.DatRetorno != null),
					Familia = p.Key.Familia,
					Tipo = p.Key.TipItem,
					DesCausa = p.Key.DesCausa
				}).Distinct().ToList();

				foreach (var item in resultado)
				{
					if (!string.IsNullOrWhiteSpace(item.HorInicio) && !string.IsNullOrWhiteSpace(item.HorFim))
					{
						//var horaAlmoço = TimeSpan.Parse(item.HorAlmoco);
						item.TotalHoras = TimeSpan.Parse(item.HorFim).Subtract(TimeSpan.Parse(item.HorInicio)).TotalHours;
					}

				}

				return resultado;
			}

		}

		/*public static List<RelParadaDepto> ObterParadasDepto(string codFab, DateTime datInicio, DateTime datFim, string codDepto, string codFabDepto)
		{

			using (var dtbSim = new DTB_SIMContext())
			{

				var paradas = (from p in dtbSim.tbl_ProdParada
							   from pd in dtbSim.tbl_ProdParadaDep.Where(x => x.NumParada == p.NumParada && x.CodFab == p.CodFab)
							   where p.DatParada >= datInicio && p.DatParada <= datFim && p.CodFab.Equals(codFab)
							   select new
							   {
								   CodFab = p.CodFab,
								   CodItem = p.CodItem,
								   CodLinha = p.CodLinha,
								   CodModelo = p.CodModelo,
								   CodParada = p.CodParada,
								   CodTurno = p.CodTurno,
								   DatParada = p.DatParada,
								   DesObs = p.DesObs,
								   HorAlmoco = p.HorAlmoco,
								   HorFim = p.HorFim,
								   HorInicio = p.HorInicio,
								   NumDrt = p.NumDrt,
								   NumFuncionarios = p.NumFuncionarios,
								   NumParada = p.NumParada,
								   QtdFalta = p.QtdFalta,
								   QtdItem = p.QtdItem,
								   QtdNaoProduzida = p.QtdNaoProduzida,
								   rowguid = p.rowguid,
								   StatusEmail = p.StatusEmail,
								   StatusPerda = p.StatusPerda,
								   CodDepto = pd.CodDepto,
								   TipItem = pd.TipItem
							   });

				if (!String.IsNullOrEmpty(codDepto))
				{
					paradas = paradas.Where(x => x.CodDepto.Equals(codDepto));
				}

				var resultado = paradas.GroupBy(p => new
				{
					p.CodFab,
					p.CodItem,
					p.CodLinha,
					p.CodModelo,
					p.CodParada,
					p.CodTurno,
					p.DatParada,
					p.DesObs,
					p.HorAlmoco,
					p.HorFim,
					p.HorInicio,
					p.NumDrt,
					p.NumFuncionarios,
					p.NumParada,
					p.QtdFalta,
					p.QtdItem,
					p.QtdNaoProduzida,
					p.rowguid,
					p.StatusEmail,
					p.StatusPerda,
					p.DesItem,
					p.DesLinha,
					p.DesFab,
					p.DesParada,
					p.DesModelo
				}).Select(p => new ProdParadaDTO()
				{
					CodFab = p.Key.CodFab,
					CodItem = p.Key.CodItem,
					CodLinha = p.Key.CodLinha,
					CodModelo = p.Key.CodModelo,
					CodParada = p.Key.CodParada,
					CodTurno = p.Key.CodTurno,
					DatParada = p.Key.DatParada,
					DesObs = p.Key.DesObs,
					HorAlmoco = p.Key.HorAlmoco,
					HorFim = p.Key.HorFim,
					HorInicio = p.Key.HorInicio,
					NumDrt = p.Key.NumDrt,
					NumFuncionarios = p.Key.NumFuncionarios,
					NumParada = p.Key.NumParada,
					QtdFalta = p.Key.QtdFalta,
					QtdItem = p.Key.QtdItem,
					QtdNaoProduzida = p.Key.QtdNaoProduzida,
					rowguid = p.Key.rowguid,
					StatusEmail = p.Key.StatusEmail,
					StatusPerda = p.Key.StatusPerda,
					DesModelo = p.Key.DesModelo,
					DesItem = p.Key.DesItem,
					DesLinha = p.Key.DesLinha,
					DesFabrica = p.Key.DesFab,
					DesParada = p.Key.DesParada,
					QtdRespostas = p.Count(x => x.DatRetorno != null)
				}).Distinct().ToList();

				return resultado;
			}

		}
		*/
		public static ProdParadaDTO ObterParada(string codFab, string numParada)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var dado = (from p in dtbSim.tbl_ProdParada
							from l in dtbSim.tbl_Linha.Where(x => x.CodLinha.Equals(p.CodLinha)).DefaultIfEmpty()
							from fab in dtbSim.tbl_Fabrica.Where(x => x.CodFab.Equals(p.CodFab)).DefaultIfEmpty()
							from item in dtbSim.tbl_Item.Where(x => x.CodItem.Equals(p.CodItem)).DefaultIfEmpty()
							from modelo in dtbSim.tbl_Item.Where(x => x.CodItem.Equals(p.CodModelo)).DefaultIfEmpty()
							from parada in dtbSim.tbl_Parada.Where(x => x.CodParada.Equals(p.CodParada) && x.CodFab.Equals(p.CodFab))
							where p.CodFab.Equals(codFab) && p.NumParada.Equals(numParada)
							select new ProdParadaDTO()
							{
								CodFab = p.CodFab,
								CodItem = p.CodItem,
								CodLinha = p.CodLinha,
								CodModelo = p.CodModelo,
								CodParada = p.CodParada,
								CodTurno = p.CodTurno,
								DatParada = p.DatParada,
								DesObs = p.DesObs,
								HorAlmoco = p.HorAlmoco,
								HorFim = p.HorFim,
								HorInicio = p.HorInicio,
								NumDrt = p.NumDrt,
								NumFuncionarios = p.NumFuncionarios,
								NumParada = p.NumParada,
								QtdFalta = p.QtdFalta,
								QtdItem = p.QtdItem,
								QtdNaoProduzida = p.QtdNaoProduzida,
								rowguid = p.rowguid,
								StatusEmail = p.StatusEmail,
								StatusPerda = p.StatusPerda,
								DesModelo = modelo.DesItem,
								DesItem = item.DesItem,
								DesLinha = l.DesLinha,
								DesFabrica = fab.NomFab,
								DesParada = parada.DesParada,
								DesDepLinha = l.DesDep
							});

				return dado.FirstOrDefault();

			}
		}

		public static string ObterProximaParada()
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var ano = DateTime.Now.Year.ToString();

				var parada = (from p in dtbSim.tbl_ProdParada
							  where p.NumParada.EndsWith(ano)
							  orderby p.NumParada descending
							  select p.NumParada).FirstOrDefault();

				if (String.IsNullOrEmpty(parada))
				{
					return String.Format("0001/{0}", DateTime.Now.Year);
				}
				else
				{
					parada = (int.Parse(
						parada.Substring(0,
							parada.IndexOf('/')
							)
						) + 1).ToString().PadLeft(4, '0');

					return String.Format("{0}/{1}", parada, DateTime.Now.Year);
				}

			}
		}

		public static bool GravarParada(ProdParadaDTO parada)
		{
			try
			{
				using (var dtbSim = new DTB_SIMContext())
				{
					//dtbSim.Database.Log = Console.Write;
					var existe = //new tbl_ProdParada();
						dtbSim.tbl_ProdParada.FirstOrDefault(x => x.CodFab.Equals(parada.CodFab) && x.NumParada.Equals(parada.NumParada));

					if (existe != null)
					{
						AutoMapper.Mapper.Map(parada, existe);

					}
					else
					{
						tbl_ProdParada aparada = new tbl_ProdParada()
						{
							CodFab = parada.CodFab,
							CodLinha = parada.CodLinha,
							NumParada = parada.NumParada,
							CodModelo = parada.CodModelo,
							DatParada = parada.DatParada,
							CodParada = parada.CodParada,
							CodItem = parada.CodItem,
							HorInicio = parada.HorInicio,
							HorFim = parada.HorFim,
							StatusPerda = parada.StatusPerda,
							QtdItem = parada.QtdItem,
							QtdNaoProduzida = parada.QtdNaoProduzida,
							CodTurno = parada.CodTurno,
							HorAlmoco = parada.HorAlmoco,
							NumFuncionarios = parada.NumFuncionarios,
							QtdFalta = parada.QtdFalta,
							DesObs = parada.DesObs,
							StatusEmail = parada.StatusEmail,
							rowguid = Guid.NewGuid(),
							NumDrt = parada.NumDrt

						};

						dtbSim.tbl_ProdParada.Add(aparada);
					}

					dtbSim.SaveChanges();
					return true;

				}
			}
			catch (DbUpdateException ex)
			{

				foreach (var eve in ex.Entries)
				{
					Console.Write(eve);
				}

				Console.Write(ex.Message);
				return false;
			}
			catch (DbEntityValidationException ex)
			{
				Console.Write(ex.Message);
				return false;
			}
			catch (Exception ex)
			{
				Console.Write(ex.Message);
				return false;
			}


		}

		public static void RemoverParada(string codFab, string numParada)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var existe =
					dtbSim.tbl_ProdParada.FirstOrDefault(x => x.CodFab.Equals(codFab) && x.NumParada.Equals(numParada));

				if (existe != null)
				{
					dtbSim.tbl_ProdParada.Remove(existe);
					dtbSim.SaveChanges();

				}
			}
		}

		public static List<ProdParadaDTO> ObterResumoMensal(DateTime datInicio, DateTime datFim, string codFab, string codDepto)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var listaDTO = new List<ProdParadaDTO>();

				var resultado = dtbSim.Database.SqlQuery<spc_PM002>(
							"EXEC spc_PM002 @CodFab, @DatIni, @DatFim, @Depto, @CodFabDepto",
							new SqlParameter("CodFab", codFab),
							new SqlParameter("DatIni", datInicio),
							new SqlParameter("DatFim", datFim),
							new SqlParameter("Depto", codDepto),
							new SqlParameter("CodFabDepto", "")
							);

				//dtbSim.SaveChanges();

				foreach (var p in resultado.ToList())
				{
					var modelo = dtbSim.tbl_Item.FirstOrDefault(x => x.CodItem == p.CodModelo);
					var familia = dtbSim.tbl_Familia.FirstOrDefault(x => x.CodFam.Equals(modelo.CodFam));
					var linha = dtbSim.tbl_Linha.FirstOrDefault(x => x.CodLinha.Equals(p.Linha));
					listaDTO.Add(new ProdParadaDTO
					{
						//CodFab = p.,
						CodItem = p.CodItem,
						CodLinha = linha.CodLinha,
						CodModelo = p.CodModelo,
						//CodParada = p.,
						//CodTurno = p.,
						DatParada = p.DatParada,
						//DesObs = p.,
						HorAlmoco = p.HorAlmoco,
						HorFim = p.HorFim,
						HorInicio = p.HorIni,
						//NumDrt = p.NumDrt,
						NumFuncionarios = p.NumFuncionarios,
						NumParada = p.NumParada,
						//QtdFalta = p.,
						//QtdItem = p.QtdItem,
						QtdNaoProduzida = p.QtdNaoProduzida,
						//rowguid = p.rowguid,
						//StatusEmail = p.StatusEmail,
						//StatusPerda = p.StatusPerda,
						DesModelo = p.DesModelo,
						DesItem = p.DesItem,
						DesLinha = p.Linha,
						//DesFabrica = p.,
						DesParada = p.DesParada,
						Deptos = p.CodDepto,
						//QtdRespostas = p.Count(x => x.DatRetorno != null),
						Familia =
							p.Linha.Substring(0, 3) == "110"
								? "IAC"
								: p.Linha.Substring(0, 3) == "111"
									? "IMC"
									: familia.NomFam,
						Tipo = p.Tipo,
						TotalHoras = (double)p.TotHoras,
						TotalHorasTipo = p.TotHorasTipo,
						DesCausa = p.DesCausa
					});
				}

				return listaDTO;
			}
		}

		public static List<ResumoHorasParadas> ObterResumoHorasParadas(int codFab, DateTime dataInicio, DateTime dataFim)
		{
			using (var contexto = new DTB_SIMContext())
			{
				var resultado = contexto.Database.SqlQuery<spc_PM009>(
							"EXEC spc_PM009 @CodFab, @DatIni, @DatFim",
							new SqlParameter("CodFab", codFab.ToString("00")),
							new SqlParameter("DatIni", dataInicio),
							new SqlParameter("DatFim", dataFim)
							);

				contexto.SaveChanges();

				var lista = new List<ResumoHorasParadas>();

				var retorno = resultado.ToList();

				AutoMapper.Mapper.Map(retorno, lista);

				return lista;
			}
		}

		public static List<ResumoDetalhesHorasParadas> ObterDetalhesResumoHorasParadas(int codFab, DateTime dataInicio, DateTime dataFim)
		{
			using (var contexto = new DTB_SIMContext())
			{
				var resultado = contexto.Database.SqlQuery<spc_PM019>(
							"EXEC spc_PM019 @CodFab, @DatIni, @DatFim",
							new SqlParameter("CodFab", codFab.ToString("00")),
							new SqlParameter("DatIni", dataInicio),
							new SqlParameter("DatFim", dataFim)
							);

				contexto.SaveChanges();

				var lista = new List<ResumoDetalhesHorasParadas>();

				var retorno = resultado.ToList();

				AutoMapper.Mapper.Map(retorno, lista);

				return lista;
			}
		}

		#endregion

		#region Departamento de Paradas
		/// <summary>
		/// Rotina para buscar no banco de dados os registros cadastrados com base no filtro informado
		/// </summary>
		/// <param name="codFab"></param>
		/// <param name="tipoParada"></param>
		/// <returns></returns>
		public static List<ProdParadaDepDTO> ObterListaParadaDep(string codFab, string numParada)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var paradas = (from p in dtbSim.tbl_ProdParadaDep
							   where p.NumParada == numParada && p.CodFab.Equals(codFab)
							   select p).ToList();

				List<ProdParadaDepDTO> paradasDTO = new List<ProdParadaDepDTO>();

				AutoMapper.Mapper.Map(paradas, paradasDTO);

				return paradasDTO;
			}
		}

		/// <summary>
		/// Rotina para buscar no banco de dados um determinado registro
		/// </summary>
		/// <param name="codLinha"></param>
		/// <returns></returns>
		public static ProdParadaDepDTO ObterParadaDep(string codFab, string numParada, string codDepto)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from c in dtbSim.tbl_ProdParadaDep
								 from fab in dtbSim.tbl_Fabrica.Where(x => x.CodFab.Equals(c.CodFab)).DefaultIfEmpty()
								 from dep in dtbSim.tbl_Departamento.Where(x => x.CodFab.Equals(c.CodFabDepto) && x.CodDepto.Equals(c.CodDepto)).DefaultIfEmpty()
								 from grupo in dtbSim.tbl_Departamento.Where(x => x.CodFab.Equals(c.CodFabDepto) && x.CodDepto.Equals(c.CodDepto)).DefaultIfEmpty()
								 where c.NumParada.Equals(numParada) && c.CodFab.Equals(codFab) && c.CodDepto.Equals(codDepto)
								 select new ProdParadaDepDTO()
								 {
									 CodFab = c.CodFab,
									 NumParada = c.NumParada,
									 rowguid = c.rowguid,
									 CodDepto = c.CodDepto,
									 CodFabDepto = c.CodFabDepto,
									 CodGrupo = c.CodGrupo,
									 DatEnvio = c.DatEnvio,
									 TipItem = c.TipItem,
									 DesFabrica = fab.NomFab,
									 DesDepartamento = dep.NomDepto,
									 DesGrupoEmail = grupo.NomDepto ?? "GRUPO " + c.CodDepto
								 }).FirstOrDefault();

				return resultado;

			}
		}

		/// <summary>
		/// Rotina de manuntenção de dados, para inclusão e alteração
		/// </summary>
		/// <param name="linha"></param>
		public static void GravarParadaDep(ProdParadaDepDTO parada)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var existe =
					dtbSim.tbl_ProdParadaDep.FirstOrDefault(
						x => x.NumParada.Equals(parada.NumParada) &&
							x.CodFab == parada.CodFab &&
							x.CodDepto == parada.CodDepto
						);

				if (existe != null)
				{
					existe.CodGrupo = parada.CodGrupo;
					existe.TipItem = parada.TipItem;
					existe.CodFabDepto = parada.CodFabDepto;

					dtbSim.SaveChanges();
				}
				else
				{
					var nlinha = new tbl_ProdParadaDep()
					{
						CodFab = parada.CodFab,
						NumParada = parada.NumParada,
						rowguid = Guid.NewGuid(),
						CodDepto = parada.CodDepto,
						CodFabDepto = parada.CodFabDepto,
						CodGrupo = parada.CodGrupo,
						DatEnvio = parada.DatEnvio,
						TipItem = parada.TipItem
					};

					dtbSim.tbl_ProdParadaDep.Add(nlinha);
					dtbSim.SaveChanges();
				}
			}
		}

		/// <summary>
		/// Rotina para excluir um registro do banco de dados
		/// </summary>
		/// <param name="codLinha"></param>
		/// <param name="numTurno"></param>
		public static void RemoverParadaDep(string numParada, string codFab, string codDepto)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var existe =
					dtbSim.tbl_ProdParadaDep.FirstOrDefault(
						x => x.NumParada.Equals(numParada) &&
							x.CodFab == codFab &&
							x.CodDepto == codDepto
						);

				if (existe == null) return;

				dtbSim.tbl_ProdParadaDep.Remove(existe);
				dtbSim.SaveChanges();
			}
		}
		#endregion

		#region Itens de parada por departamento
		/// <summary>
		/// Rotina para buscar no banco de dados uma lista com os dados filtrados
		/// </summary>
		/// <param name="codFab"></param>
		/// <param name="numParada"></param>
		/// <returns></returns>
		public static List<ProdParadaDepItemDTO> ObterListaParadaDepItem(string codFab, string numParada)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from c in dtbSim.tbl_ProdParadaDepItem
								 from f in dtbSim.tbl_Fabrica.Where(x => x.CodFab.Equals(c.CodFab)).DefaultIfEmpty()
								 from d in dtbSim.tbl_Departamento.Where(x => x.CodFab.Equals(c.CodFab) && x.CodDepto.Equals(c.CodDepto)).DefaultIfEmpty()
								 where c.NumParada.Equals(numParada) && c.CodFab.Equals(codFab)
								 select new ProdParadaDepItemDTO()
								 {
									 CodFab = c.CodFab,
									 NumParada = c.NumParada,
									 rowguid = c.rowguid,
									 CodDepto = c.CodDepto,
									 CodFabDepto = c.CodFabDepto,
									 DatEnvio = c.DatEnvio,
									 DatRetorno = c.DatRetorno,
									 DesAcao = c.DesAcao,
									 DesCausa = c.DesCausa,
									 DesObs = c.DesObs,
									 FlgStatus = c.FlgStatus,
									 NomResponsavel = c.NomResponsavel,
									 DesDepartamento = d.NomDepto,
									 DesFabrica = f.NomFab
								 }).ToList();

				return resultado;

			}
		}

		/// <summary>
		/// Rotina para buscar no banco de dados um determinado registro
		/// </summary>
		/// <param name="codLinha"></param>
		/// <returns></returns>
		public static ProdParadaDepItemDTO ObterParadaDepItem(string codFab, string numParada, string codDepto)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from c in dtbSim.tbl_ProdParadaDepItem
								 from f in dtbSim.tbl_Fabrica.Where(x => x.CodFab.Equals(c.CodFab)).DefaultIfEmpty()
								 from d in dtbSim.tbl_Departamento.Where(x => x.CodFab.Equals(c.CodFab) && x.CodDepto.Equals(c.CodDepto)).DefaultIfEmpty()
								 where c.NumParada.Equals(numParada) && c.CodFab.Equals(codFab) && c.CodDepto.Equals(codDepto)
								 select new ProdParadaDepItemDTO()
								 {
									 CodFab = c.CodFab,
									 NumParada = c.NumParada,
									 rowguid = c.rowguid,
									 CodDepto = c.CodDepto,
									 CodFabDepto = c.CodFabDepto,
									 DatEnvio = c.DatEnvio,
									 DatRetorno = c.DatRetorno,
									 DesAcao = c.DesAcao,
									 DesCausa = c.DesCausa,
									 DesObs = c.DesObs,
									 FlgStatus = c.FlgStatus,
									 NomResponsavel = c.NomResponsavel,
									 DesDepartamento = d.NomDepto,
									 DesFabrica = f.NomFab
								 }).FirstOrDefault();

				return resultado;

			}
		}

		/// <summary>
		/// Rotina de manuntenção de dados, para inclusão e alteração
		/// </summary>
		/// <param name="linha"></param>
		public static void GravarParadaDepItem(ProdParadaDepItemDTO parada)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var existe =
					dtbSim.tbl_ProdParadaDepItem.FirstOrDefault(
						x => x.NumParada.Equals(parada.NumParada) &&
							x.CodFab == parada.CodFab &&
							x.CodDepto == parada.CodDepto
						);

				if (existe != null)
				{
					existe.DatRetorno = parada.DatRetorno;
					existe.CodFabDepto = parada.CodFabDepto;
					existe.DesAcao = parada.DesAcao;
					existe.DesCausa = parada.DesCausa;
					existe.DesObs = parada.DesObs;
					existe.FlgStatus = parada.FlgStatus;
					existe.NomResponsavel = parada.NomResponsavel;
					existe.DatEnvio = parada.DatEnvio;

					dtbSim.SaveChanges();
				}
				else
				{
					var nlinha = new tbl_ProdParadaDepItem()
					{
						CodFab = parada.CodFab,
						NumParada = parada.NumParada,
						rowguid = Guid.NewGuid(),
						CodDepto = parada.CodDepto,
						CodFabDepto = parada.CodFabDepto,
						NomResponsavel = parada.NomResponsavel,
						DatEnvio = parada.DatEnvio,
						DatRetorno = parada.DatRetorno,
						DesAcao = parada.DesAcao,
						DesCausa = parada.DesCausa,
						DesObs = parada.DesObs,
						FlgStatus = parada.FlgStatus
					};

					dtbSim.tbl_ProdParadaDepItem.Add(nlinha);
					dtbSim.SaveChanges();
				}
			}
		}

		/// <summary>
		/// Rotina para excluir um registro do banco de dados
		/// </summary>
		/// <param name="codLinha"></param>
		/// <param name="numTurno"></param>
		public static void RemoverParadaDepItem(string numParada, string codFab, string codDepto)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var existe =
					dtbSim.tbl_ProdParadaDepItem.FirstOrDefault(
						x => x.NumParada.Equals(numParada) &&
							x.CodFab == codFab &&
							x.CodDepto == codDepto
						);

				if (existe == null) return;

				dtbSim.tbl_ProdParadaDepItem.Remove(existe);
				dtbSim.SaveChanges();
			}
		}
		#endregion

		#region Motivos de Paradas
		/// <summary>
		/// Rotina para buscar no banco de dados os registros cadastrados
		/// </summary>
		/// <returns></returns>
		public static List<ParadaDTO> ObterMotivos()
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from c in dtbSim.tbl_Parada
								 select new ParadaDTO()
								 {
									 CodFab = c.CodFab,
									 CodParada = c.CodParada,
									 DesParada = c.DesParada,
									 rowguid = c.rowguid,
									 TipParada = c.TipParada
								 });

				return resultado.ToList();
			}
		}

		/// <summary>
		/// Rotina para buscar no banco de dados os registros cadastrados com base no filtro informado
		/// </summary>
		/// <param name="codFab"></param>
		/// <param name="tipoParada"></param>
		/// <returns></returns>
		public static List<ParadaDTO> ObterMotivos(string codFab, string tipoParada)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from c in dtbSim.tbl_Parada
								 where c.CodFab.Equals(codFab) && c.TipParada.Equals(tipoParada)
								 orderby c.CodParada
								 select new ParadaDTO()
								 {
									 CodFab = c.CodFab,
									 CodParada = c.CodParada,
									 DesParada = c.DesParada,
									 rowguid = c.rowguid,
									 TipParada = c.TipParada
								 });

				return resultado.ToList();
			}
		}

		/// <summary>
		/// Rotina para buscar no banco de dados um determinado registro
		/// </summary>
		/// <param name="codLinha"></param>
		/// <returns></returns>
		public static ParadaDTO ObterMotivo(string codParada, string codFab)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from c in dtbSim.tbl_Parada
								 join fab in dtbSim.tbl_Fabrica on c.CodFab equals fab.CodFab
								 where c.CodParada.Equals(codParada) && c.CodFab.Equals(codFab)
								 select new ParadaDTO()
								 {
									 CodFab = c.CodFab,
									 CodParada = c.CodParada,
									 DesParada = c.DesParada,
									 rowguid = c.rowguid,
									 TipParada = c.TipParada,
									 DesFabrica = fab.NomFab
								 }).FirstOrDefault();
				if (resultado != null)
					resultado.DesParada = resultado.DesParada.Trim();
				return resultado;

			}
		}

		/// <summary>
		/// Rotina de manuntenção de dados, para inclusão e alteração
		/// </summary>
		/// <param name="linha"></param>
		public static void GravarMotivo(ParadaDTO parada)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var existe =
					dtbSim.tbl_Parada.FirstOrDefault(
						x => x.CodParada.Equals(parada.CodParada) &&
							x.CodFab == parada.CodFab
						);

				if (existe != null)
				{
					existe.DesParada = parada.DesParada;
					existe.TipParada = parada.TipParada;

					dtbSim.SaveChanges();
				}
				else
				{
					var nlinha = new tbl_Parada()
					{
						CodFab = parada.CodFab,
						CodParada = ObterProximoMotivo(parada.CodFab),
						DesParada = parada.DesParada.ToUpper(),
						TipParada = parada.TipParada,
						rowguid = Guid.NewGuid()
					};

					dtbSim.tbl_Parada.Add(nlinha);
					dtbSim.SaveChanges();
				}
			}
		}

		/// <summary>
		/// Rotina para excluir um registro do banco de dados
		/// </summary>
		/// <param name="codLinha"></param>
		/// <param name="numTurno"></param>
		public static void RemoverMotivo(string codParada, string codFab)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var existe =
					dtbSim.tbl_Parada.FirstOrDefault(
						x => x.CodParada.Equals(codParada) &&
							x.CodFab.Equals(codFab));

				if (existe == null) return;

				dtbSim.tbl_Parada.Remove(existe);
				dtbSim.SaveChanges();
			}
		}

		/// <summary>
		/// Rotina para obter o último código cadastrado para o filtro Fábrica e Tipo
		/// </summary>
		/// <param name="codFab"></param>
		/// <param name="tipoParada"></param>
		/// <returns></returns>
		public static String ObterProximoMotivo(string codFab)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from c in dtbSim.tbl_Parada
								 where c.CodFab.Equals(codFab)
								 orderby c.CodParada descending
								 select c.CodParada).FirstOrDefault();
				if (String.IsNullOrEmpty(resultado))
				{
					resultado = "0";
				}
				resultado = (int.Parse(resultado) + 1).ToString("0000");

				return resultado;
			}
		}

		#endregion

		#region Amplitude para emails
		/// <summary>
		/// Rotina para buscar no banco de dados os registros cadastrados
		/// </summary>
		/// <returns></returns>
		public static List<ProdAmplitudeDTO> ObterListaAmplitude()
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from c in dtbSim.tbl_ProdAmplitude
								 from u in dtbSim.tbl_Usuario.Where(x => x.CodFab.Equals(c.CodFab) && x.NomUsuario.Equals(c.NomUsuario)).DefaultIfEmpty()
								 from f in dtbSim.tbl_Fabrica.Where(x => x.CodFab.Equals(c.CodFab)).DefaultIfEmpty()
								 from d in dtbSim.tbl_Departamento.Where(x => x.CodDepto.Equals(c.CodDepto) && x.CodFab.Equals(c.CodFab)).DefaultIfEmpty()
								 select new ProdAmplitudeDTO()
								 {
									 CodFab = c.CodFab,
									 DesFabrica = f.NomFab,
									 CodDepto = c.CodDepto,
									 DesDepto = d.NomDepto ?? "GRUPO " + c.CodDepto,
									 NomEmail = c.NomEmail,
									 NomUsuario = c.NomUsuario,
									 TipUsuario = c.TipUsuario,
									 NomeUsuario = u.FullName
								 });

				return resultado.ToList();
			}
		}

		/// <summary>
		/// Rotina para buscar no banco de dados os registros cadastrados
		/// <paramref name="depto">Departamento a ser filtrado</paramref>
		/// </summary>
		/// <returns></returns>
		public static List<ProdAmplitudeDTO> ObterListaAmplitude(string depto)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from c in dtbSim.tbl_ProdAmplitude
								 from u in dtbSim.tbl_Usuario.Where(x => x.CodFab.Equals(c.CodFab) && x.NomUsuario.Equals(c.NomUsuario)).DefaultIfEmpty()
								 from f in dtbSim.tbl_Fabrica.Where(x => x.CodFab.Equals(c.CodFab)).DefaultIfEmpty()
								 from d in dtbSim.tbl_Departamento.Where(x => x.CodDepto.Equals(c.CodDepto) && x.CodFab.Equals(c.CodFab)).DefaultIfEmpty()
								 where c.CodDepto == depto
								 select new ProdAmplitudeDTO()
								 {
									 CodFab = c.CodFab,
									 DesFabrica = f.NomFab,
									 CodDepto = c.CodDepto,
									 DesDepto = d.NomDepto ?? "GRUPO " + c.CodDepto,
									 NomEmail = c.NomEmail,
									 NomUsuario = c.NomUsuario,
									 TipUsuario = c.TipUsuario,
									 NomeUsuario = u.FullName
								 });

				return resultado.ToList();
			}
		}

		public static List<ProdAmplitudeDTO> ObterListaAmplitudeParada(string codFab, string numParada)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from c in dtbSim.tbl_ProdAmplitude
								 from p in dtbSim.tbl_ProdParadaDep.Where(x => x.CodFab == c.CodFab && x.CodGrupo == c.CodDepto)
								 from u in dtbSim.tbl_Usuario.Where(x => x.CodFab.Equals(c.CodFab) && x.NomUsuario.Equals(c.NomUsuario)).DefaultIfEmpty()
								 from f in dtbSim.tbl_Fabrica.Where(x => x.CodFab.Equals(c.CodFab)).DefaultIfEmpty()
								 from d in dtbSim.tbl_Departamento.Where(x => x.CodDepto.Equals(c.CodDepto) && x.CodFab.Equals(c.CodFab)).DefaultIfEmpty()
								 where c.CodFab == codFab && p.NumParada == numParada
								 select new ProdAmplitudeDTO()
								 {
									 CodFab = c.CodFab,
									 DesFabrica = f.NomFab,
									 CodDepto = c.CodDepto,
									 DesDepto = d.NomDepto ?? "GRUPO" + c.CodDepto,
									 NomEmail = c.NomEmail,
									 NomUsuario = c.NomUsuario,
									 TipUsuario = c.TipUsuario,
									 NomeUsuario = u.FullName
								 });

				return resultado.ToList();
			}
		}

		/// <summary>
		/// Rotina para buscar no banco de dados um determinado registro
		/// </summary>
		/// <param name="codLinha"></param>
		/// <returns></returns>
		public static ProdAmplitudeDTO ObterAmplitude(string codFab, string codDepto, string nomUsuario)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from c in dtbSim.tbl_ProdAmplitude
								 from u in dtbSim.tbl_Usuario.Where(x => x.CodFab.Equals(c.CodFab) && x.NomUsuario.Equals(c.NomUsuario)).DefaultIfEmpty()
								 from f in dtbSim.tbl_Fabrica.Where(x => x.CodFab.Equals(c.CodFab)).DefaultIfEmpty()
								 from d in dtbSim.tbl_Departamento.Where(x => x.CodDepto.Equals(c.CodDepto) && x.CodFab.Equals(c.CodFab))
								 where c.CodFab.Equals(codFab) && c.CodDepto.Equals(codDepto) && c.NomUsuario.Equals(nomUsuario)
								 select new ProdAmplitudeDTO()
								 {
									 CodFab = c.CodFab,
									 DesFabrica = f.Sigla,
									 CodDepto = c.CodDepto,
									 DesDepto = d.NomDepto,
									 NomEmail = c.NomEmail,
									 NomUsuario = c.NomUsuario,
									 TipUsuario = c.TipUsuario,
									 NomeUsuario = u.FullName
								 }).FirstOrDefault();

				return resultado;

			}
		}

		/// <summary>
		/// Rotina para buscar a descrição do grupo
		/// </summary>
		/// <param name="codLinha"></param>
		/// <returns></returns>
		public static string ObterDesGrupo(string codFab, string codDepto)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from c in dtbSim.tbl_ProdAmplitude
								 from d in dtbSim.tbl_Departamento.Where(x => x.CodDepto.Equals(c.CodDepto) && x.CodFab.Equals(c.CodFab)).DefaultIfEmpty()
								 where c.CodFab.Equals(codFab) && c.CodDepto.Equals(codDepto)
								 group d by d.NomDepto into grupo
								 select new { Descricao = grupo.Key }).FirstOrDefault();
				if (resultado != null)
				{
					return resultado.Descricao;
				}
				else
				{
					return String.Format("GRUPO {0}", codDepto);
				}

			}
		}

		/// <summary>
		/// Rotina de manuntenção de dados, para inclusão e alteração
		/// </summary>
		/// <param name="linha"></param>
		public static void GravarAmplitude(ProdAmplitudeDTO dado)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var existe =
					dtbSim.tbl_ProdAmplitude.FirstOrDefault(
						c => c.CodFab.Equals(dado.CodFab) && c.CodDepto.Equals(dado.CodDepto) && c.NomUsuario.Equals(dado.NomUsuario)
						);

				if (existe != null)
				{
					existe.TipUsuario = dado.TipUsuario;
					existe.NomEmail = dado.NomEmail;

					dtbSim.SaveChanges();
				}
				else
				{
					var nAmplitude = new tbl_ProdAmplitude()
					{
						CodFab = dado.CodFab,
						CodDepto = dado.CodDepto,
						NomEmail = dado.NomEmail,
						NomUsuario = dado.NomUsuario,
						TipUsuario = dado.TipUsuario,
						rowguid = Guid.NewGuid()
					};

					dtbSim.tbl_ProdAmplitude.Add(nAmplitude);
					dtbSim.SaveChanges();
				}
			}
		}

		/// <summary>
		/// Rotina para excluir um registro do banco de dados
		/// </summary>
		/// <param name="codLinha"></param>
		/// <param name="numTurno"></param>
		public static void RemoverAmplitude(string codFab, string codDepto, string nomUsuario)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var existe =
					dtbSim.tbl_ProdAmplitude.FirstOrDefault(
						x => x.CodFab.Equals(codFab) && x.CodDepto.Equals(codDepto) && x.NomUsuario.Equals(nomUsuario));

				if (existe == null) return;

				dtbSim.tbl_ProdAmplitude.Remove(existe);
				dtbSim.SaveChanges();
			}
		}

		#endregion
	}

	#region Class Procedures

	public class spc_PM009
	{
		public string CodFab { get; set; }
		public string CodFabDepto { get; set; }
		public string CodDepto { get; set; }
		public decimal QtdTV { get; set; }
		public decimal HorTV { get; set; }
		public decimal QtdAudio { get; set; }
		public decimal HorAudio { get; set; }
		public decimal QtdVCR { get; set; }
		public decimal HorVCR { get; set; }
		public decimal QtdFone { get; set; }
		public decimal HorFone { get; set; }
		public decimal QtdDVD { get; set; }
		public decimal HorDVD { get; set; }
		public decimal QtdIAC { get; set; }
		public decimal HorIAC { get; set; }
		public decimal QtdIMC { get; set; }
		public decimal HorIMC { get; set; }
		public decimal QtdMSC { get; set; }
		public decimal HorMSC { get; set; }
		public decimal QtdOutros { get; set; }
		public decimal HorOutros { get; set; }
		public decimal QtdTotal { get; set; }
		public decimal HorTotal { get; set; }
		public string NomDepto { get; set; }
	}

	public class spc_PM019
	{
		public DateTime MesBase { get; set; }
		public decimal? QtdParada { get; set; }
		public decimal? HorParada { get; set; }
		public decimal? QtdTotal { get; set; }
		public decimal? HorTotal { get; set; }
		public string MesAno { get; set; }
	}

	public class spc_PM002
	{
		public string DesCausa { get; set; }
		public string DesAcao { get; set; }
		public string DesParada { get; set; }
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public string Linha { get; set; }
		public DateTime DatParada { get; set; }
		public string HorIni { get; set; }
		public string HorFim { get; set; }
		public string HorAlmoco { get; set; }
		public int NumFuncionarios { get; set; }
		public string CodItem { get; set; }
		public string DesItem { get; set; }
		//public string DesParada { get; set; }
		public string CodDepto { get; set; }
		public int? QtdNaoProduzida { get; set; }
		public string Tipo { get; set; }
		public string NumParada { get; set; }
		public string QtdHoras { get; set; }
		public decimal QtdMinutos { get; set; }
		public decimal TotMinutos { get; set; }
		public decimal TotHoras { get; set; }
		public decimal TotMinutosTipo { get; set; }
		public decimal TotHorasTipo { get; set; }
	}

	#endregion

}
