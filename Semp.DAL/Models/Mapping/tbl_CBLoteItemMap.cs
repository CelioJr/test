﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBLoteItemMap: EntityTypeConfiguration<tbl_CBLoteItem>
    {
        public tbl_CBLoteItemMap()
		{
            this.HasKey(t => new { t.NumLote, t.NumCC, t.NumECB });

            this.Property(t => t.NumECB)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.NumLote)
                .HasMaxLength(18)
                .IsUnicode(false);

            this.Property(t => t.NumCC)
                .HasMaxLength(19)
                .IsUnicode(false);

            this.ToTable("tbl_CBLoteItem");
            this.Property(t => t.NumLote).HasColumnName("NumLote");
            this.Property(t => t.NumCC).HasColumnName("NumCC");
            this.Property(t => t.NumECB).HasColumnName("NumECB");
            this.Property(t => t.DatInclusao).HasColumnName("DatInclusao");
            this.Property(t => t.DatInspecao).HasColumnName("DatInspecao");
            
            this.Property(t => t.flgAmostra).HasColumnName("flgAmostra");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.ResultadoInspecao).HasColumnName("ResultadoInspecao");
        }
    }
}
