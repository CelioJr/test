namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_IMPORT_ERROR
    {
        [Key]
        public int COD_IMPORT { get; set; }

        [StringLength(500)]
        public string ERRO { get; set; }

        public DateTime? DATA { get; set; }

        [StringLength(30)]
        public string SERIAL { get; set; }
    }
}
