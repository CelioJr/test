using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DOC_DIAGRAMA_CONTEXTO_DTO
    {
        public FR_DOC_DIAGRAMA_CONTEXTO_DTO()
        {
            this.FR_DOC_DIAG_CONT_CAS_USO_ATO = new List<FR_DOC_DIAG_CONT_CAS_USO_ATO_DTO>();
        }

        public int CON_CODIGO { get; set; }
        public string CON_DESCRICAO { get; set; }
        public byte[] CON_IMAGEM { get; set; }
        public Nullable<int> DOC_CODIGO { get; set; }
        public virtual ICollection<FR_DOC_DIAG_CONT_CAS_USO_ATO_DTO> FR_DOC_DIAG_CONT_CAS_USO_ATO { get; set; }
        public virtual FR_DOC_PRINCIPAL_DTO FR_DOC_PRINCIPAL { get; set; }
    }
}
