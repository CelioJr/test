using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_Estoque
    {
        public string CodFab { get; set; }
        public string CodItem { get; set; }
        public string CodLocal { get; set; }
        public Nullable<decimal> QtdEstoque { get; set; }
        public Nullable<decimal> QtdEstCong { get; set; }
        public Nullable<System.DateTime> DatEstoque { get; set; }
    }
}
