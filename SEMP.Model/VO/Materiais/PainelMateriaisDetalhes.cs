﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Materiais
{
	public class PainelMateriaisDetalhes
	{
		public string CodItem { get; set; }
		public string DesItem { get; set; }
		public decimal? QtdNecessaria { get; set; }
		public string CodGalpao { get; set; }
		public string CodRua { get; set; }
		public string CodBloco { get; set; }
		public string CodNivel { get; set; }
		public string CodApto { get; set; }
		public string Documento { get; set; }
		public DateTime DatItem { get; set; }
		public decimal? QtdBaixar { get; set; }
		public decimal? QtdSaldoEndereco { get; set; }

	}
}
