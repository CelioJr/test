namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ORDER_CONDITIONS_IN
    {
        public Guid ID { get; set; }

        public Guid ORDER_ITEMS_IN_ID { get; set; }

        [StringLength(100)]
        public string ITM_NUMBER { get; set; }

        [StringLength(4)]
        public string COND_TYPE { get; set; }

        public decimal? COND_VALUE { get; set; }

        [StringLength(5)]
        public string KOEIN { get; set; }

        public virtual ORDER_ITEMS_IN ORDER_ITEMS_IN { get; set; }
    }
}
