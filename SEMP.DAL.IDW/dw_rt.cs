namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_rt
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_rt()
        {
            dw_consolid = new HashSet<dw_consolid>();
            dw_rtcic = new HashSet<dw_rtcic>();
        }

        [Key]
        public long id_rt { get; set; }

        public short? is_semplanejamento { get; set; }

        public short? is_operador { get; set; }

        public short? st_funcionamento { get; set; }

        public short? is_manutencaopre { get; set; }

        public short? is_vidautilmolde { get; set; }

        public short? st_qualidade { get; set; }

        public short? is_paradapeso { get; set; }

        public DateTime? dthr_iciclo { get; set; }

        public int? ms_dthriciclo { get; set; }

        public decimal? seg_paradaparcial { get; set; }

        public int? ordem_ciclos { get; set; }

        public short? is_regulagem { get; set; }

        public short? is_paradafechaciclo { get; set; }

        public decimal? seg_ciclopadraominimo { get; set; }

        public decimal? seg_ultimociclo { get; set; }

        public long? pcs_producaoplanejada_op { get; set; }

        public long? pcs_producaoliquida_op { get; set; }

        public long? id_consolpalog { get; set; }

        public DateTime? dt_referencia { get; set; }

        public long id_turno { get; set; }

        public long id_pt { get; set; }

        public short? is_conforme { get; set; }

        public decimal? ulttemperaturalida { get; set; }

        public short? is_alerta { get; set; }

        public short? is_cip { get; set; }

        public long? id_cp { get; set; }

        public short? is_gargaloteorico { get; set; }

        public short? is_gargalodinamico { get; set; }

        public short? is_offline { get; set; }

        public DateTime? dthr_heartbeat { get; set; }

        public DateTime? dthr_cadastro { get; set; }

        public long? id_rtbase { get; set; }

        public DateTime? dthr_evento { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolid> dw_consolid { get; set; }

        public virtual dw_consolpalog dw_consolpalog { get; set; }

        public virtual om_pt om_pt { get; set; }

        public virtual dw_turno dw_turno { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rtcic> dw_rtcic { get; set; }
    }
}
