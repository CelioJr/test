namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolpaoco
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_consolpaoco()
        {
            dw_consol_parammed = new HashSet<dw_consol_parammed>();
        }

        [Key]
        public long id_consolpaoco { get; set; }

        public long id_consolpa { get; set; }

        public DateTime? dthr_iparada { get; set; }

        public int? ms_dthriparada { get; set; }

        public DateTime? dthr_fparada { get; set; }

        public int? ms_dthrfparada { get; set; }

        public short? is_continuaproximoperiodo { get; set; }

        public long id_consolpalog { get; set; }

        public DateTime? dthr_fparada_ab { get; set; }

        public int? ms_dthrfparada_ab { get; set; }

        public long? id_tcausa { get; set; }

        public long? id_tjust { get; set; }

        public long? id_tacao { get; set; }

        public DateTime? dthr_cadastro { get; set; }

        [StringLength(40)]
        public string origem { get; set; }

        public decimal? pcs_auto_perdaparada_cp { get; set; }

        public decimal? pcs_auto_perdaparada_sp { get; set; }

        public decimal? pcs_manu_perdaparada_cp { get; set; }

        public decimal? pcs_manu_perdaparada_sp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consol_parammed> dw_consol_parammed { get; set; }

        public virtual dw_consolpalog dw_consolpalog { get; set; }

        public virtual dw_t_acao dw_t_acao { get; set; }

        public virtual dw_t_causa dw_t_causa { get; set; }

        public virtual dw_t_just dw_t_just { get; set; }
    }
}
