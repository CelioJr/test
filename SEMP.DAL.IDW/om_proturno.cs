namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_proturno
    {
        [Key]
        public long id_omproturno { get; set; }

        public long id_produto { get; set; }

        public long id_turno { get; set; }

        public short? tp_relacao { get; set; }

        public virtual dw_turno dw_turno { get; set; }

        public virtual om_produto om_produto { get; set; }
    }
}
