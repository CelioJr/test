using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_MOD_POSTOS_DTO
    {
        public int COD_MOD_POSTOS { get; set; }
        public Nullable<int> COD_POSTOS { get; set; }
        public Nullable<int> COD_SKU { get; set; }
    }
}
