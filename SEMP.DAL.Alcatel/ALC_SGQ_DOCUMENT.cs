namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_SGQ_DOCUMENT")]
    public partial class ALC_SGQ_DOCUMENT
    {
        [Key]
        [StringLength(25)]
        public string ID_DOCUMENT { get; set; }

        [StringLength(255)]
        public string DESCRIPTION { get; set; }

        [StringLength(255)]
        public string LINK { get; set; }

        public int? process_ID_PROCESS { get; set; }

        [StringLength(255)]
        public string DOC_TYPE { get; set; }

        [MaxLength(255)]
        public byte[] DOC_BYTES { get; set; }

        public virtual ALC_SGQ_PROCESS ALC_SGQ_PROCESS { get; set; }
    }
}
