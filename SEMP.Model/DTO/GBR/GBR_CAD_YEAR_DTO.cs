using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_CAD_YEAR_DTO
    {
        public int COD_LINHA { get; set; }
        public Nullable<int> VALOR { get; set; }
        public string LETRAS { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
    }
}
