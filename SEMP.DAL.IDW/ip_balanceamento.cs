namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ip_balanceamento
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ip_balanceamento()
        {
            dw_folhaoperacao = new HashSet<dw_folhaoperacao>();
        }

        [Key]
        public long id_balanceamento { get; set; }

        [StringLength(60)]
        public string cd_balanceamento { get; set; }

        public int? revisao { get; set; }

        [Column(TypeName = "date")]
        public DateTime? dt_revisao { get; set; }

        public short? st_ativo { get; set; }

        [Column(TypeName = "date")]
        public DateTime? dt_stativo { get; set; }

        public short? st_balanceamento { get; set; }

        public long? id_usrRevisao { get; set; }

        public long? id_usrstAtivo { get; set; }

        public long? id_gt { get; set; }

        public long? id_produtoAcabado { get; set; }

        public long? id_produtoSemiacabado { get; set; }

        public decimal? pcs_producaoPlanejadaDiaria { get; set; }

        public decimal? seg_tempoDIsponivelDiario { get; set; }

        public decimal? seg_taktTime { get; set; }

        public decimal? ind_fadiga { get; set; }

        public short? tp_algoritmo { get; set; }

        [StringLength(256)]
        public string ds_balanceamento { get; set; }

        public short? unidadeTakttime { get; set; }

        [StringLength(256)]
        public string comentarioRevisao { get; set; }

        public long? id_usrAutorizou { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhaoperacao> dw_folhaoperacao { get; set; }

        public virtual om_gt om_gt { get; set; }

        public virtual om_produto om_produto { get; set; }

        public virtual om_produto om_produto1 { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        public virtual om_usr om_usr2 { get; set; }
    }
}
