using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBTipoEtiquetaMap : EntityTypeConfiguration<tbl_CBTipoEtiqueta>
    {
        public tbl_CBTipoEtiquetaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.TipoEtiqueta, t.DesEtiqueta });

            // Properties
            this.Property(t => t.TipoEtiqueta)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DesEtiqueta)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("tbl_CBTipoEtiqueta");
            this.Property(t => t.TipoEtiqueta).HasColumnName("TipoEtiqueta");
            this.Property(t => t.DesEtiqueta).HasColumnName("DesEtiqueta");
        }
    }
}
