namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_PPB_TARGET")]
    public partial class ALC_PPB_TARGET
    {
        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal TARGET_ID { get; set; }

        [StringLength(255)]
        public string MATERIAL { get; set; }

        public double? TARGET { get; set; }

        public int? TARGET_YEAR { get; set; }
    }
}
