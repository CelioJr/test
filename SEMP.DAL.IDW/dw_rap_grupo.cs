namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_rap_grupo
    {
        [Key]
        public long id_rap_grupo { get; set; }

        public long id_rap { get; set; }

        public long id_grupo_ferramenta { get; set; }

        public virtual dw_grupo_ferramenta dw_grupo_ferramenta { get; set; }

        public virtual dw_rap dw_rap { get; set; }
    }
}
