﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Materiais
{
	public class PainelMateriais
	{
		public Int64 CodSeparacao { get; set; }
		public DateTime DatSeparacao { get; set; }
		public string NumAP { get; set; }
		public string CodLinha { get; set; }
		public string Modelo { get; set; }
		public string Situacao { get; set; }
		public string CodFab { get; set; }

	}
}
