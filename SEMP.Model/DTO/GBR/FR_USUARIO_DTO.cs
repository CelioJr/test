using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_USUARIO_DTO
    {
        public FR_USUARIO_DTO()
        {
            this.FR_USUARIO_SISTEMA = new List<FR_USUARIO_SISTEMA_DTO>();
            this.FR_GRUPO = new List<FR_GRUPO_DTO>();
        }

        public int USR_CODIGO { get; set; }
        public string USR_LOGIN { get; set; }
        public string USR_SENHA { get; set; }
        public string USR_ADMINISTRADOR { get; set; }
        public string USR_TIPO_EXPIRACAO { get; set; }
        public Nullable<int> USR_DIAS_EXPIRACAO { get; set; }
        public byte[] USR_IMAGEM_DIGITAL { get; set; }
        public byte[] USR_FOTO { get; set; }
        public string USR_NOME { get; set; }
        public string USR_EMAIL { get; set; }
        public Nullable<long> USR_DIGITAL { get; set; }
        public Nullable<System.DateTime> USR_INICIO_EXPIRACAO { get; set; }
        public virtual ICollection<FR_USUARIO_SISTEMA_DTO> FR_USUARIO_SISTEMA { get; set; }
        public virtual ICollection<FR_GRUPO_DTO> FR_GRUPO { get; set; }
    }
}
