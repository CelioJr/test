namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_erros
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_erros()
        {
            tbl_dados = new HashSet<tbl_dados>();
            tbl_logs = new HashSet<tbl_logs>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(5)]
        public string CODIGO { get; set; }

        [Required]
        [StringLength(150)]
        public string TEXTO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_dados> tbl_dados { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_logs> tbl_logs { get; set; }
    }
}
