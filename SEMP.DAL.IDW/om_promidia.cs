namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_promidia
    {
        [Key]
        public long id_promidia { get; set; }

        [StringLength(256)]
        public string ds_promidia { get; set; }

        [Column(TypeName = "image")]
        public byte[] midia { get; set; }

        public long id_produto { get; set; }

        [StringLength(60)]
        public string extensaoArquivo { get; set; }

        public virtual om_produto om_produto { get; set; }
    }
}
