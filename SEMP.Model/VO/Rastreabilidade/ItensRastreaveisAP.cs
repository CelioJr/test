﻿namespace SEMP.Model.VO.Rastreabilidade
{
	public class ItensRastreaveisAP
	{

		public string CodFab { get; set; }
		public string NumAP { get; set; }
		public string CodModelo { get; set; }
		public string CodItem { get; set; }
		public string CodItemAlt { get; set; }
		public decimal QtdFrequencia { get; set; }
		public decimal QtdNecessaria { get; set; }
		public decimal QtdProduzida { get; set; }
		public string Origem { get; set; }
		public string CodFam { get; set; }
		public string NomFam { get; set; }

		public string DesModelo { get; set; }
		public string DesItem { get; set; }
		public string DesItemAlt { get; set; }

	}
}
