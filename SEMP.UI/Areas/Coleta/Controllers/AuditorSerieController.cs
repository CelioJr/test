﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.Relatorios;
using SEMP.Model.DTO;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using SEMP.Models;

namespace SEMP.Areas.Coleta.Controllers
{
	/// <summary>
	/// Classe que controla as Action da funcionalidade AuditorSerie
	/// </summary>
	[Authorize]
	public class AuditorSerieController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}
		/*public ActionResult Index(string DatInicio, string DatFim, int? CodLinha, String CodModelo, String NumSerie, String MostrarTudo)
		{
			//DateTime dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			//DateTime dFim = DateTime.Parse(DateTime.Now.AddDays(1).ToString("dd/MM/yyyy"));
			try
			{
				if (DatInicio != null && DatFim != null)
				{
					DateTime dIni = Convert.ToDateTime(DatInicio);
					DateTime dFim = Convert.ToDateTime(DatFim).AddDays(1);

					int linha = 36;
					if (CodLinha != null)
					{
						linha = (int)CodLinha;
					}

					List<SEMP.Model.VO.Rastreabilidade.Relatorios.PassagemSerie> passagem = new List<Model.VO.Rastreabilidade.Relatorios.PassagemSerie>();

					if (!String.IsNullOrEmpty(NumSerie))
					{
						passagem = AuditorSerie.ObterPassagemSerie(NumSerie);
					}
					else if (!String.IsNullOrEmpty(MostrarTudo))
					{
						passagem = AuditorSerie.ObterSeriesFalta(linha);
					}
					else if (!String.IsNullOrEmpty(CodModelo))
					{
						passagem = AuditorSerie.ObterSeriesFalta(dIni, dFim, linha, CodModelo);
					}
					else
					{
						passagem = AuditorSerie.ObterSeriesFalta(dIni, dFim, linha);
					}

					var postos = AuditorSerie.ObterPostosObrigatorios(linha);

					ViewBag.passagem = passagem;
					ViewBag.postos = postos;
				}

			}
			catch (Exception ex)
			{
				Log.AppLog().ErrorException("\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name, ex);
				ViewBag.Erro = "Erro ao consultar.";
			}
			return View();
		}*/


		/// <summary>
		/// Action ObterDados - método que retorna os dados de séries lidas conforme os parâmetros informados.
		/// </summary>
		/// <param name="DatInicio">Parâmetro que recebe a data inidicio do filtro.</param>
		/// <param name="DatFim">Parâmetro que recebe a data fim do filtro.</param>
		/// <param name="CodLinha">Parâmetro que recebe o código da linha do filtro.</param>
		/// <param name="CodModelo">Parâmetro que recebe o código do modelo do filtro.</param>
		/// <param name="NumSerie">Parâmetro que recebe o npumero de série do filtro.</param>
		/// <param name="MostrarTudo">Parâmetro que recebe a opção de mostrar tudo do filtro.</param>
		/// <returns>JSON com os dados</returns>
		public JsonResult ObterDados(string DatInicio, string DatFim, int? CodLinha, String CodModelo, String NumSerie, String MostrarTudo, string leituras)
		{
			var passagem = new List<PassagemSerie>();

			try
			{
				if (!String.IsNullOrEmpty(DatInicio) && !String.IsNullOrEmpty(DatFim))
				{
					DateTime dIni = Convert.ToDateTime(DatInicio);
					DateTime dFim = Convert.ToDateTime(DatFim).AddDays(1);

					int linha = 36;
					if (CodLinha != null)
					{
						linha = (int)CodLinha;
					}

					if (!String.IsNullOrEmpty(NumSerie))
					{
						passagem = AuditorSerie.ObterPassagemSerie(NumSerie);
					}
					else if (!String.IsNullOrEmpty(MostrarTudo))
					{
						passagem = AuditorSerie.ObterSeriesFalta(linha);
					}
					else if (!String.IsNullOrEmpty(leituras))
					{
						passagem = AuditorSerie.ObterPassagemLinha(linha, dIni, dFim);
					}
					else if (!String.IsNullOrEmpty(CodModelo))
					{
						passagem = AuditorSerie.ObterSeriesFalta(dIni, dFim, linha, CodModelo);
					}
					else
					{
						passagem = AuditorSerie.ObterSeriesFalta(dIni, dFim, linha);
					}
				}

			}
			catch (Exception ex)
			{
				Log.AppLog().Error(ex, $"\r\n{ex.StackTrace}\r\n{ex.TargetSite.Name}");
				//Log.AppLog().ErrorException($"\r\n{ex.StackTrace}\r\n{ex.TargetSite.Name}", ex);
				ViewBag.Erro = "Erro ao consultar.";
			}

			return Json(passagem, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterPostos(int? codLinha)
		{

			try
			{
				var postos = AuditorSerie.ObterPostosObrigatorios(codLinha).Select(x => x.DesPosto).ToList();

				return Json(postos, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				Log.AppLog().Error(ex, "\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name);
				//Log.AppLog().ErrorException("\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name, ex);
				ViewBag.Erro = "Erro ao consultar.";
			}

			return null;
		}

		public JsonResult ObterPostosSerie(string NumSerie)
		{

			try
			{
				var codLinha = AuditorSerie.ObterLinhaPorSerie(NumSerie);

				var postosobrig = AuditorSerie.ObterPostosObrigatorios(codLinha);
				var postospassagem = AuditorSerie.ObterPostosPassagem(NumSerie).Where(x => !(postosobrig.Any(p => p.NumPosto == x.NumPosto)));
				postosobrig.AddRange(postospassagem);

				var postos = postosobrig.OrderBy(x => x.NumSeq).Select(x => x.DesPosto).ToList();

				return Json(postos, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				Log.AppLog().Error(ex, "\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name);
				//Log.AppLog().ErrorException("\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name, ex);
				ViewBag.Erro = "Erro ao consultar.";
			}

			return null;
		}

		public JsonResult ObterDadosResumo(string DatInicio, string DatFim, int? CodLinha, String CodModelo, String NumSerie)
		{
			var resumo = new List<ResumoSeries>();

			try
			{
				if (!String.IsNullOrEmpty(DatInicio) && !String.IsNullOrEmpty(DatFim))
				{
					DateTime dIni = Convert.ToDateTime(DatInicio);
					DateTime dFim = Convert.ToDateTime(DatFim).AddDays(1);

					resumo = AuditorSerie.ObterResumoNaoLidas(CodModelo, "", "", dIni, dFim);
				}

			}
			catch (Exception ex)
			{
				//Log.AppLog().ErrorException("\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name, ex);
				Log.AppLog().Error(ex, "\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name);
				ViewBag.Erro = "Erro ao consultar.";
			}

			return Json(resumo, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterModelos(string DatInicio, string DatFim, int CodLinha)
		{

			try
			{

				DateTime dIni = Convert.ToDateTime(DatInicio);
				DateTime dFim = Convert.ToDateTime(DatFim).AddDays(1);

				var modelos = AuditorSerie.ObterModelos(dIni, dFim, CodLinha);

				return Json(modelos, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				//Log.AppLog().ErrorException("\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name, ex);
				Log.AppLog().Error(ex, "\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name);
				ViewBag.Erro = "Erro ao consultar.";
			}

			return null;
		}

	}
}
