﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.Models
{
	public class MAPGeraSCRAP
	{
		[Key]
		public int IDScrap { get; set; }
		public DateTime DatReferencia { get; set; }
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public string CodItem { get; set; }
		public string DesItem { get; set; }
		public decimal? QtdPerda { get; set; }
		public decimal? ValTotal { get; set; }
		public decimal? QtdBaixa { get; set; }
		public string CodFabBxa { get; set; }
		public decimal? QtdEstoque { get; set; }
		public DateTime? DatBaixa { get; set; }
		public DateTime? DatGeracao { get; set; }
		public string NomUsuGerou { get; set; }

	}
}
