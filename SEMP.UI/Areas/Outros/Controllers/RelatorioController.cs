﻿using RAST.BO.PM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SEMP.Model.DTO;

namespace SEMP.Areas.Outros.Controllers
{
    public class RelatorioController : Controller
    {
        //
        // GET: /Outros/Relatorio/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListaRelatorioParadaLinhaDiario(string datInicio, string datFim, string linha, string codFab, string codModelo, string codDepto)
        {

            var dIni = DateTime.Parse(datInicio);
            var dFim = DateTime.Parse(datFim);

            var dados = ParadaDeLinha.ObterParadas(dIni, dFim, linha, codFab, codModelo, "", "",codDepto).Where(a => a.Deptos.Trim() != "OFP").ToList();

            ViewBag.Periodo = datInicio;

            return PartialView(dados);
        }

        public ActionResult RelatorioOrigemPlano()
        {
            return View();
        }

        public ActionResult ListaRelatorioOrigemPlano(string datInicio, string datFim, string linha, string codFab, string codModelo, string codDepto)
        {
            var dIni = DateTime.Parse(datInicio);
            var dFim = DateTime.Parse(datFim);

            dFim = dFim.AddMonths(1).AddSeconds(-1);

            //var dados = ParadaDeLinha.ObterParadas(dIni, dFim, linha, codFab, codModelo, "", "", codDepto).Where(a => a.Deptos.Trim() != "OFP").ToList();

            var dados2 = ParadaDeLinha.ObterResumoMensal(dIni, dFim, codFab, codDepto);

            ViewBag.Periodo = datInicio;

            return PartialView(dados2);
        }

        public ActionResult ResumoHorasParadas()
        {
            return View();
        }

        public ActionResult ListaResumoHorasParadas(string datInicio, string datFim, string codFab)
        {
            var dIni = DateTime.Parse(datInicio);
            var dFim = DateTime.Parse(datFim);

            dFim = dFim.AddMonths(1).AddSeconds(-1);

            var listaResumo = ParadaDeLinha.ObterResumoHorasParadas(int.Parse(codFab), dIni, dFim);
            var listaDetalhes = ParadaDeLinha.ObterDetalhesResumoHorasParadas(int.Parse(codFab), dIni, dFim);

            ViewBag.ListaResumo = listaResumo;
            ViewBag.ListaDetalhes = listaDetalhes;
            ViewBag.Periodo = datInicio;

            return PartialView();
        }
    }
}
