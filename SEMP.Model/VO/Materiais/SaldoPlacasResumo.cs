﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Materiais
{
	public class SaldoPlacasResumo
	{

		public string CodPlaca { get; set; }
		public string DesPlaca { get; set; }
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public int? QtdLoteAP { get; set; }
		public int? QtdApontado { get; set; }
		public int? QtdPrograma { get; set; }
		public decimal? QtdEstoque { get; set; }
		public int Ordem { get; set; }
		public DateTime? DatAtualizacao { get; set; }

	}
}
