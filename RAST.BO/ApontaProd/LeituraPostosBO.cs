﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.VO.Rastreabilidade.Relatorios;

namespace RAST.BO.ApontaProd
{
	public class LeituraPostosBO
	{
		public static List<LeiturasPostos> ObterLeiturasPostos(DateTime datInicio, DateTime datFim, int codLinha, int turno)
		{
			return daoLeituraPostos.ObterLeiturasPostos(datInicio, datFim, codLinha, turno);
		}

		public static List<LeiturasPostos> ObterLeiturasPostosAnalitico(DateTime datInicio, DateTime datFim, int codLinha, int turno)
		{
			return daoLeituraPostos.ObterLeiturasPostosAnalitico(datInicio, datFim, codLinha, turno);
		}

		public static List<ProducaoDiariaDAT> ObterProducaoDAT(DateTime datInicio, DateTime datFim, int codLinha)
		{
			return daoLeituraPostos.ObterProducaoDAT(datInicio, datFim, codLinha);

		}
	}
}
