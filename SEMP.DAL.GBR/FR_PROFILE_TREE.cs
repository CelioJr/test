namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_PROFILE_TREE
    {
        [Key]
        [StringLength(38)]
        public string PRT_CODIGO { get; set; }

        [StringLength(38)]
        public string PRT_CODIGO_PARENT { get; set; }

        [StringLength(38)]
        public string PRI_CODIGO { get; set; }

        [StringLength(1024)]
        public string PRT_DESCRIPTION { get; set; }

        [StringLength(1024)]
        public string PRT_IDENTIFIER { get; set; }

        public long? PRT_START { get; set; }

        public long? PRT_TIME { get; set; }

        [StringLength(1)]
        public string PRT_TYPE { get; set; }
    }
}
