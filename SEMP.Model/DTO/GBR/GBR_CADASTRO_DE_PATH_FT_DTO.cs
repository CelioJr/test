using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_CADASTRO_DE_PATH_FT_DTO
    {
        public int GBR_CAD_ID { get; set; }
        public string POSTO { get; set; }
        public string CAMINHO { get; set; }
        public string CAMINHO_BACKUP { get; set; }
        public Nullable<int> COD_SKU { get; set; }
        public Nullable<bool> CHK_ATIVO { get; set; }
        public Nullable<bool> IGNORAR { get; set; }
        public virtual GBR_CAD_SKU_DTO GBR_CAD_SKU { get; set; }
    }
}
