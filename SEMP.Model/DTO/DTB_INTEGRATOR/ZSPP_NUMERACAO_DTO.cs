namespace SEMP.Model.DTO
{
	using System.ComponentModel.DataAnnotations;

	public partial class ZSPP_NUMERACAO_DTO
	{
        [Key]
        [StringLength(20)]
        public string Codigo { get; set; }

        public int? Sequencia { get; set; }
    }
}
