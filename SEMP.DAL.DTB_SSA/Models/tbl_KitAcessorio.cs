namespace SEMP.DAL.DTB_SSA.Models
{
    public partial class tbl_KitAcessorio
    {
        public int CodKit { get; set; }
        public System.DateTime DatImpressao { get; set; }
        public int CodOperador { get; set; }
        public int CodOF { get; set; }
        public string NumSerie { get; set; }
        public string SerialAdaptador { get; set; }
        public string NumLicencaNero { get; set; }
    }
}
