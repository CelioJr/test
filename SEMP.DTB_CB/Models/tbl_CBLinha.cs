using System;
using System.Collections.Generic;

namespace SEMP.DTB_CB.Models
{
    public partial class tbl_CBLinha
    {
        public tbl_CBLinha()
        {
            this.tbl_CBDefeito = new List<tbl_CBDefeito>();
            this.tbl_CBDefeito1 = new List<tbl_CBDefeito>();
            this.tbl_CBLinhaPostoPerfil = new List<tbl_CBLinhaPostoPerfil>();
            this.tbl_CBPassagem = new List<tbl_CBPassagem>();
            this.tbl_CBPosto = new List<tbl_CBPosto>();
        }

        public int CodLinha { get; set; }
        public string DesLinha { get; set; }
        public string DesObs { get; set; }
        public string Setor { get; set; }
        public string Familia { get; set; }
        public string CodLinhaT1 { get; set; }
        public string CodLinhaT2 { get; set; }
        public string CodLinhaT3 { get; set; }
        public string CodPlaca { get; set; }
        public string CodModelo { get; set; }
        public string CodAuxiliar { get; set; }
        public string CodFam { get; set; }
        public virtual ICollection<tbl_CBDefeito> tbl_CBDefeito { get; set; }
        public virtual ICollection<tbl_CBDefeito> tbl_CBDefeito1 { get; set; }
        public virtual ICollection<tbl_CBLinhaPostoPerfil> tbl_CBLinhaPostoPerfil { get; set; }
        public virtual ICollection<tbl_CBPassagem> tbl_CBPassagem { get; set; }
        public virtual ICollection<tbl_CBPosto> tbl_CBPosto { get; set; }
        public virtual tbl_CBTrocaDRT tbl_CBTrocaDRT { get; set; }
    }
}
