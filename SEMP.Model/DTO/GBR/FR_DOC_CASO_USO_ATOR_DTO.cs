using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DOC_CASO_USO_ATOR_DTO
    {
        public int ATO_CODIGO_ATIVO { get; set; }
        public int ATO_CODIGO_PASSIVO { get; set; }
        public int CAS_USO_ATO_CODIGO { get; set; }
        public int DOC_CODIGO { get; set; }
        public int USO_CODIGO { get; set; }
        public virtual FR_DOC_ATOR_DTO FR_DOC_ATOR { get; set; }
    }
}
