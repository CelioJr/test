using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DIAGRAMA_DTO
    {
        public int DGR_COD { get; set; }
        public string DGR_NOME { get; set; }
        public string DGR_AUTOR { get; set; }
        public string DGR_VERSAO { get; set; }
        public Nullable<int> DGR_TIPOTABELA { get; set; }
        public Nullable<System.DateTime> DGR_DATACRIACAO { get; set; }
        public Nullable<System.DateTime> DGR_ATUALIZACAO { get; set; }
        public string DGR_COMENTARIO { get; set; }
        public Nullable<int> DGR_PADRAOFK { get; set; }
        public string DGR_LAYOUT { get; set; }
    }
}
