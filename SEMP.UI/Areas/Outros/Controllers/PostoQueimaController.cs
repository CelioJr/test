﻿using RAST.BO.Rastreabilidade;
using SEMP.Model.DTO;
using SEMP.Model.VO;
using SEMP.Model.VO.Rastreabilidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEMP.Areas.Outros.Controllers
{
	public class PostoQueimaController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult Lista(string datInicio, string datFim, string numSerie)
		{
			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}

			dFim = dFim.AddHours(23);
			try
			{

			
			var dado = PostoQueima.ObterRegistros(dIni, dFim);

			if (!String.IsNullOrEmpty(numSerie)) {
					var modelo = numSerie.Substring(0, 6);
					var serie = numSerie.Substring(numSerie.Length - 8);
				var pallet = PostoQueima.ObterPalletSerie(modelo, serie);

				dado = dado.Where(x => x.CodPallet == pallet.CodPallet.PadLeft(6, '0')).ToList();
				
			}

			var defeitosEmAberto = PostoQueima.ObterRegistrosDefeitos(dIni);

			dado.AddRange(defeitosEmAberto.Where(x => !dado.Select(y => y.NumRPA).ToList().Contains(x.NumRPA)));

			return View(dado.OrderBy(x => x.DatProducao).ThenBy(x => x.DatFechamento).ToList());
			}
			catch (Exception ex)
			{
				ViewBag.Erro = ex.Message;

				return View(new List<CBQueimaRegistroDTO>());
			
			}
		}

		public ActionResult ListaResumo(string datInicio, string datFim)
		{
			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}


			var dado = PostoQueima.ObterResumoInspecao(dIni, dFim);

			return View(dado);

		}

        /// <summary>
        /// Obsoleto pra essa rotina
        /// </summary>
        /// <returns></returns>
		public ActionResult NovaInspecao()
		{
			Session["QueimaCodFab"] = "";
			Session["QueimaNumRPA"] = "";
			Session["QueimaCodPallet"] = "";

			var linhas = Geral.ObterLinhaPorFabrica("CQ");

			ViewBag.linhas = linhas;

			return View();
		}

        /// <summary>
        /// Obsoleto pra essa rotina
        /// </summary>
        /// <returns></returns>
		[HttpPost]
		public ActionResult NovaInspecao(string CodFab, string NumRPA, string CodLinha, string NumPosto, string CodPallet, string CodModelo, string codOperador, string qtdAmostras, string codLinhaOri) {

			/// Cadastrar os dados de movimentação de pallet
			/// e fazer a movimentação do ponto de estoque da expedição
			/// para o do CQ
			/// 

			var pallet = PostoQueima.ObterPalletGPA(CodPallet, CodModelo, ""); // dados do pallet
			var configLinha = PostoQueima.ObterConfigLote(int.Parse(CodLinha)); // dados de configuração de linha para amostragem

			var dado = PostoQueima.ObterRegistro(CodFab, NumRPA, CodPallet);

			if (dado == null)
			{
				dado = new CBQueimaRegistroDTO()
				{
					CodFab = CodFab,
					NumRPA = NumRPA,
					CodLinha = int.Parse(CodLinha),
					NumPosto = int.Parse(NumPosto),
					CodPallet = CodPallet,
					CodModelo = CodModelo,
					CodOperador = codOperador,
					DatAbertura = BO.Util.GetDate(),
					DatProducao = pallet.Data,
					FlgStatus = "A",
					QtdPallet = pallet.QtdPallet,
					QtdAmostras = int.Parse(qtdAmostras),
					CodLinhaOri = codLinhaOri
				};
			}
			else
			{
				if (dado.FlgStatus == "1" && dado.DatFechamento != null) 
				{

					ViewBag.erro = "Pallet já inspecionado";
					return View();

				}

				dado.QtdAmostras = int.Parse(qtdAmostras);
				dado.CodOperador = codOperador;
				dado.CodPallet = CodPallet;
				dado.CodLinhaOri = codLinhaOri;
				dado.DatProducao = pallet != null ? pallet.Data : SEMP.BO.Util.GetDate();
			}

			var resultado = PostoQueima.GravarRegistro(dado, false);

			if (resultado.StartsWith("false")) {
				ViewBag.erro = "Erro ao tentar iniciar lote, informe ao DTI <br />" + resultado.Replace("false|", "");
				return View();

			}

			/// antes de redirecionar, setar os cookies
			#region Cookies
			/// Setar posto no cookie do browser
			/// cria a variável e configura os valores

			HttpCookie c = new HttpCookie("Posto")
			{
				Expires = DateTime.Now.AddYears(1)
			};
			c.Values.Add("Fabrica", "Queima");
			c.Values.Add("codLinha", CodLinha);
			c.Values.Add("numPosto", NumPosto);

			this.ControllerContext.HttpContext.Response.Cookies.Add(c);

			#endregion


			Session["QueimaCodFab"] = CodFab;
			Session["QueimaNumRPA"] = NumRPA;
			Session["QueimaCodPallet"] = CodPallet;

			return RedirectToAction("Coleta");

		}

		public ActionResult NovaInspecaoRPA()
		{
			Session["QueimaCodFab"] = "";
			Session["QueimaNumRPA"] = "";
			Session["QueimaCodPallet"] = "";

			var linhas = Geral.ObterLinhaPorFabrica("CQ");

			ViewBag.linhas = linhas;

			return View();
		}

		[HttpPost]
		public ActionResult NovaInspecaoRPA(string CodFab, string NumRPA, string CodLinha, string NumPosto, string CodPallet, string CodModelo, string codOperador, string qtdAmostras, string codLinhaOri)
		{

			/// Cadastrar os dados de movimentação de pallet
			/// e fazer a movimentação do ponto de estoque da expedição
			/// para o do CQ
			/// 

			var pallet = PostoQueima.ObterPalletGPA(CodPallet, CodModelo, ""); // dados do pallet
			var configLinha = PostoQueima.ObterConfigLote(int.Parse(CodLinha)); // dados de configuração de linha para amostragem

			var dado = PostoQueima.ObterRegistro(CodFab, NumRPA, CodPallet);

            var rpa = PostoOBA.ObterRPA(CodFab, NumRPA);

            if (dado == null)
			{
				dado = new CBQueimaRegistroDTO()
				{
					CodFab = CodFab,
					NumRPA = NumRPA,
					CodLinha = int.Parse(CodLinha),
					NumPosto = int.Parse(NumPosto),
					CodPallet = CodPallet,
					CodModelo = CodModelo,
					CodOperador = codOperador,
					DatAbertura = BO.Util.GetDate(),
					DatProducao = pallet.Data,
					FlgStatus = "A",
					QtdPallet = pallet.QtdPallet,
					QtdAmostras = int.Parse(qtdAmostras),
					CodLinhaOri = codLinhaOri,
                    DatLiberado = rpa.DatEmissao
				};
			}
			else
			{
				if (dado.FlgStatus == "1" && dado.DatFechamento != null)
				{

					ViewBag.erro = "Pallet já inspecionado";
					return View();

				}

				dado.QtdAmostras = int.Parse(qtdAmostras);
				dado.CodOperador = codOperador;
				dado.CodPallet = CodPallet;
				dado.CodLinhaOri = codLinhaOri;
				dado.DatProducao = pallet.Data;
			}

			var resultado = PostoQueima.GravarRegistro(dado, true);

			if (resultado.StartsWith("false"))
			{
				ViewBag.erro = "Erro ao tentar iniciar lote, informe ao DTI <br />" + resultado.Replace("false|", "");
				return View();

			}

			/// antes de redirecionar, setar os cookies
			#region Cookies
			/// Setar posto no cookie do browser
			/// cria a variável e configura os valores

			HttpCookie c = new HttpCookie("Posto")
			{
				Expires = DateTime.Now.AddYears(1)
			};
			c.Values.Add("Fabrica", "Queima");
			c.Values.Add("codLinha", CodLinha);
			c.Values.Add("numPosto", NumPosto);

			this.ControllerContext.HttpContext.Response.Cookies.Add(c);

			#endregion


			Session["QueimaCodFab"] = CodFab;
			Session["QueimaNumRPA"] = NumRPA;
			Session["QueimaCodPallet"] = CodPallet;

			return RedirectToAction("Coleta");

		}


		public ActionResult Coleta()
		{
			string CodFab = Session["QueimaCodFab"].ToString();
			string NumRPA = Session["QueimaNumRPA"].ToString();
			string CodPallet = Session["QueimaCodPallet"].ToString();

			if (CodFab == "" ) {
				CodFab = "60";
			}

			// recupera os dados do registro para leitura
			var dado = PostoQueima.ObterRegistro(CodFab, NumRPA, CodPallet);

			if (dado == null) {
				return RedirectToAction("NovaInspecao");

			}

			var series = !String.IsNullOrEmpty(CodPallet) ? PostoQueima.ObterSeriesPallet(int.Parse(CodPallet).ToString()) : new List<string>() { "" };

			var testesQueima = PostoQueima.ObterTestesQueima();
			var testesEspecificos = PostoQueima.ObterTestesEspecificosQueima();
			var registros = PostoQueima.ObterInspecoes(CodFab, NumRPA, CodPallet);
			var registrosTestes = PostoQueima.ObterTestes(CodFab, NumRPA);

			ViewBag.Series = series;
			ViewBag.TestesQueima = testesQueima;
			ViewBag.TestesEspecificos = testesEspecificos;
			ViewBag.Registros = registros;
			ViewBag.RegistrosTestes = registrosTestes;

			return View(dado);

		}

		[HttpPost]
		public ActionResult ColetaSeries(FormCollection dados) {

			var numECB = PostoQueima.ObterECBSerie(dados["CodModelo"], dados["NumSerieColeta"]);
			
			foreach (string chave in dados)
			{
				if (chave.StartsWith("Defeitos") && !String.IsNullOrEmpty(dados[chave]) )
				{

					var codDefeito = chave.Substring(8, 4);
					var codSubDefeito = int.Parse(chave.Substring(12));
					var obs = dados["ObsDefeito" + codDefeito + codSubDefeito.ToString()];

					var defeito = new CBQueimaTestesDTO() {
						NumECB = numECB,
						CodLinha = int.Parse(dados["CodLinha"]),
						NumPosto = int.Parse(dados["NumPosto"]),
						CodFab = dados["CodFab"],
						NumRPA = dados["NumRPA"],
						CodDefeito = codDefeito,
						CodSubDefeito = codSubDefeito,
						Observacao = obs,
						FlgStatus = dados[chave],
						DatTeste = BO.Util.GetDate()
					};
					
					var resultado = PostoQueima.GravarTeste(defeito);
				}

			}

			return RedirectToAction("Coleta");
		}

		[HttpPost]
		public ActionResult FecharQueima(FormCollection dados)
		{

			var defeitos = new List<CBQueimaTestesDTO>();

			var numECB = PostoQueima.ObterECBSerie(dados["CodModelo"], dados["NumSerieColeta"]);

			var inspecao = new CBQueimaInspecaoDTO()
			{
				NumECB = numECB,
				CodLinha = int.Parse(dados["CodLinha"]),
				NumPosto = int.Parse(dados["NumPosto"]),
				CodFab = dados["CodFab"],
				NumRPA = dados["NumRPA"],
				CodPallet = dados["CodPallet"],
				FlgStatus = "S",
				Observacao = dados["Observacao"],
				DatLeitura = BO.Util.GetDate()

			};

			var resultado = PostoQueima.GravarInspecao(inspecao, false);

			return RedirectToAction("Coleta");
		}

		[HttpPost]
		public ActionResult GravarRPE(string codFab, string numRPA, string numRPE)
		{


			if (codFab != "" && numRPA != "" && numRPE != "")
			{

				var dado = PostoQueima.ObterRegistro(codFab, numRPA);

				dado.NumRPE = numRPE;
                dado.FlgStatus = "F";
                dado.DatFechamento = BO.Util.GetDate();

				PostoQueima.GravarRegistro(dado);

			}

			return RedirectToAction("Index");

		}

		[HttpPost]
		public ActionResult GravarLiberarReprov(string codFab, string numRPA, string obsLiberado, string numRPE)
		{


			if (codFab != "" && numRPA != "")
			{

				var dado = PostoQueima.ObterRegistro(codFab, numRPA);

				dado.DatLiberado = BO.Util.GetDate();
				dado.ObsRevisao = obsLiberado;
				dado.NumRPE = numRPE;
				dado.FlgStatus = "L";

				PostoQueima.GravarRegistro(dado);

			}

			return RedirectToAction("Index");

		}

        public ActionResult VisualizarTestes(string codFab, string numRPA)
        {

            var registro = PostoQueima.ObterRegistro(codFab, numRPA);
            var inspecoes = PostoQueima.ObterInspecoes(codFab, numRPA, "");
            var series = new List<string>();
            var testes = PostoQueima.ObterTestes(codFab, numRPA, "");
            var testesQueima = PostoQueima.ObterTestesQueima();
            var testesEspecificos = PostoQueima.ObterTestesEspecificosQueima();

            if (!inspecoes.Any())
            {
                series = inspecoes.Select(x => x.NumECB).Distinct().ToList();
            }
            else
            {
                series = inspecoes.Select(x => x.NumECB).ToList();
            }

            ViewBag.series = series;
            ViewBag.testes = testes;
            ViewBag.testesQueima = testesQueima;
            ViewBag.testesEspecificos = testesEspecificos;

            return View(registro);

        }

        public ActionResult VisualizarInspecao(string codFab, string numRPA) {

			var registro = PostoQueima.ObterRegistro(codFab, numRPA);
			var series = PostoQueima.ObterInspecoes(codFab, numRPA, "");
			var testes = PostoQueima.ObterTestes(codFab, numRPA, "");
			var testesQueima = PostoQueima.ObterTestesQueima();
			var testesEspecificos = PostoQueima.ObterTestesEspecificosQueima();

			ViewBag.series = series;
			ViewBag.testes = testes;
			ViewBag.testesQueima = testesQueima;
			ViewBag.testesEspecificos = testesEspecificos;

			return View(registro);

		}

		public ActionResult VisualizarInspecaoModelo(string datInicio, string datFim, string codModelo)
		{

			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}

			var dados = PostoQueima.ObterInspecoes(dIni, dFim);
			dados = dados.Where(x => x.CodModelo == codModelo).ToList();
			var testesQueima = PostoQueima.ObterTestesQueima();
			var testesEspecificos = PostoQueima.ObterTestesEspecificosQueima();

			ViewBag.testesQueima = testesQueima;
			ViewBag.testesEspecificos = testesEspecificos;

			return View(dados);

		}

		public ActionResult Relatorio(string datInicio, string datFim) {


			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}

			var dado = PostoQueima.ObterInspecoes(dIni, dFim);

			return View(dado);
		}

		public ActionResult GraficoResumo(string datInicio, string datFim)
		{

			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}


			var dado = PostoQueima.ObterResumoInspecao(dIni, dFim);

			return View(dado);
		}

		public ActionResult GraficoMensal()
		{

			var dIni = DateTime.Parse(String.Format("01/01/{0}", DateTime.Now.Year));
			var dFim = DateTime.Parse(String.Format("31/12/{0}", DateTime.Now.Year));

			var dado = PostoQueima.ObterResumoInspecao(dIni, dFim);

			var resumo = (from m in dado
						  group m by new { m.CodModelo, m.DesModelo, m.QtdP042, m.QtdP067, m.QtdP068, m.QtdP823, Data = DateTime.Parse(String.Format("01/{0}/{1}", m.Data.Month, m.Data.Year)) } into grupo
						  select new QueimaResumoInspecao()
						  {
							  Data = grupo.Key.Data,
							  CodModelo = grupo.Key.CodModelo,
							  DesModelo = grupo.Key.DesModelo,
							  QtdAprovados = grupo.Sum(x => x.QtdAprovados),
							  QtdInspecionados = grupo.Sum(x => x.QtdInspecionados),
							  QtdLotes = grupo.Sum(x => x.QtdLotes),
							  QtdP042 = grupo.Key.QtdP042,
							  QtdP067 = grupo.Key.QtdP067,
							  QtdP068 = grupo.Key.QtdP068,
							  QtdP823 = grupo.Key.QtdP823,
							  QtdProduzido = grupo.Sum(x => x.QtdProduzido),
							  QtdPrograma = grupo.Sum(x => x.QtdPrograma),
							  QtdReprovados = grupo.Sum(x => x.QtdReprovados)
						  }
			 ).ToList();

			return View(dado);
		}

		#region Consultas RPA

		public JsonResult ObterRPA(string codFab, string numRPA)
		{

			var dado = PostoOBA.ObterRPA(codFab, numRPA);
			var disponivel = true;

			var registro = PostoQueima.ObterRegistro(codFab, numRPA);

			if (registro != null && registro.FlgStatus != "A") {
				disponivel = false;
			}

			var retorno = new { 
				dado,
				disponivel
			};

			return Json(retorno, JsonRequestBehavior.AllowGet);

		}

		#endregion

		#region Consultas GPA

		public JsonResult ObterPalletsGPA(string datInicio, string datFim)
		{

			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}


			var dado = PostoQueima.ObterPalletsGPA(dIni, dFim);

			return Json(dado, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ObterPalletGPA(string numPallet, string codModelo, string numSerie)
		{

			var dado = PostoQueima.ObterPalletGPA(numPallet, codModelo, numSerie);

			return Json(dado, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ObterSeriesPallet(string numPallet)
		{

			var dados = PostoQueima.ObterSeriesPallet(numPallet);

			return Json(dados, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ObterConfigLinha(int codLinha) {

			var configLinha = PostoQueima.ObterConfigLote(codLinha);

			return Json(configLinha, JsonRequestBehavior.AllowGet);

		}

		#endregion

	}

}
