﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web;

namespace SEMP.Models
{
    public class LerExcel
    {
        private OleDbConnection _olecon;
        private OleDbCommand _oleCmd;

        private String _Arquivo = @"C:\temp\Arquivo.xlsx";

        private String _StringConexao;

        public LerExcel()
        {
            _StringConexao = String.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0 Xml;HDR=YES;ReadOnly=False';", _Arquivo);

            _olecon = new OleDbConnection(_StringConexao);
            _olecon.Open();

            _oleCmd = new OleDbCommand();
            _oleCmd.Connection = _olecon;
            _oleCmd.CommandType = CommandType.Text;
        }

        public LerExcel(string arquivo)
        {
            this._Arquivo = arquivo;

            _StringConexao = String.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0 Xml;HDR=YES;ReadOnly=False';", _Arquivo);

            _olecon = new OleDbConnection(_StringConexao);
            _olecon.Open();

            _oleCmd = new OleDbCommand();
            _oleCmd.Connection = _olecon;
            _oleCmd.CommandType = CommandType.Text;
        }

        public List<object> Consulta(string consulta)
        {
            try
            {
                _oleCmd.CommandText = consulta;

                OleDbDataReader reader = _oleCmd.ExecuteReader();

                List<object> lista = new List<object>();

                string texto = string.Empty;
                
                while (reader.Read())
                {
                    texto = string.Empty;

                    for (int i = 0; i < reader.FieldCount; i++)
			        {
                        if ((i+1) == reader.FieldCount)
                        {
                            if (!string.IsNullOrWhiteSpace(reader.GetValue(i).ToString()))
                            {
                                texto += string.Format("{0}", reader.GetValue(i)) + "";       
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(reader.GetValue(i).ToString()))
                            {
                                texto += string.Format("{0}", reader.GetValue(i)) + "|";
                            }
                        }
			        }

                    if (!string.IsNullOrWhiteSpace(texto.ToString()))
                    {
                        lista.Add(texto);   
                    }
                }

                reader.Close();

                Dispose();

                return lista;
            }
            catch (Exception)
            {
                throw new Exception("Erro ao consultar");
            }
        }

        public void Dispose()
        {
            if (_oleCmd != null)
            {
                _oleCmd.Parameters.Clear();
                _oleCmd.Dispose();
            }

            _oleCmd = null;

            if (_olecon != null)
            {
                if (_olecon.State == ConnectionState.Open)
                    _olecon.Close();

                _olecon.Dispose();
            }

            _olecon = null;
        }
    }
}