namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_nserieobs
    {
        [Key]
        public long id_nserieobs { get; set; }

        public DateTime? dthr_obs { get; set; }

        [StringLength(256)]
        public string ds_obs { get; set; }

        public long id_nserie { get; set; }

        public long id_usr { get; set; }

        public long id_pt { get; set; }

        public virtual dw_nserie dw_nserie { get; set; }

        public virtual om_pt om_pt { get; set; }

        public virtual om_usr om_usr { get; set; }
    }
}
