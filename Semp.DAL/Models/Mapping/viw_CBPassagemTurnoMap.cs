using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class viw_CBPassagemTurnoMap : EntityTypeConfiguration<viw_CBPassagemTurno>
    {
        public viw_CBPassagemTurnoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.NumECB, t.CodLinha, t.NumPosto, t.DatEvento });

            // Properties
            this.Property(t => t.NumECB)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumPosto)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodDRT)
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("viw_CBPassagemTurno");
            this.Property(t => t.NumECB).HasColumnName("NumECB");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.NumPosto).HasColumnName("NumPosto");
            this.Property(t => t.DatEvento).HasColumnName("DatEvento");
            this.Property(t => t.CodDRT).HasColumnName("CodDRT");
            this.Property(t => t.DatReferencia).HasColumnName("DatReferencia");
            this.Property(t => t.Turno).HasColumnName("Turno");
        }
    }
}
