﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade
{
	public class ResumoEmbalada
	{
		public String CodModelo { get; set; }
		public String NumECB { get; set; }
		public DateTime? DatProducao { get; set; }
		public int QtdProduzida { get; set; }

	}
}
