﻿using SEMP.DAL.Models;
using SEMP.Model.DTO;
using SEMP.Model.VO.Rastreabilidade;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace SEMP.DAL.DAO.Rastreabilidade
{
    public class daoPostoOBA
	{
        //static daoPostoOBA()
        //{
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<tbl_RPA, RPADTO>();
        //        cfg.CreateMap<tbl_CBOBAInspecao, CBOBAInspecaoDTO>();
        //        cfg.CreateMap<CBOBAInspecaoDTO, tbl_CBOBAInspecao>();
        //        cfg.CreateMap<tbl_CBOBATestes, CBOBATestesDTO>();
        //        cfg.CreateMap<CBOBATestesDTO, tbl_CBOBATestes>();
        //        cfg.CreateMap<CBOBARegistroDTO, tbl_CBOBARegistro>();
        //    });
        //}

        #region Consultas RPA

        public static RPADTO ObterRPA(string codFab, string numRPA)
		{

			using (var contexto = new DTB_SIMContext())
			{

				var dado = contexto.tbl_RPA.FirstOrDefault(x => x.CodFab == codFab && x.NumRPA == numRPA);

				var dto = new RPADTO();

				AutoMapper.Mapper.Map(dado, dto);

				return dto;


			}

		}

		#endregion

		#region Consultas GPA

		public static List<ResumoGPA> ObterPalletsGPA(DateTime datInicio, DateTime datFim)
		{

			using (var contexto = new DTB_SIMContext())
			{

				var dados = (from g in contexto.TBL_GpaRecItem
							 join l in contexto.tbl_Linha on g.CodLinha equals l.CodLinha
							 join i in contexto.tbl_Item on g.CodItem equals i.CodItem
							 where g.DataReferencia >= datInicio && g.DataReferencia <= datFim
							 group new { g, l, i } by new { g.CodPallet, g.CodLinha, g.CodItem, g.DataReferencia, l.DesLinha, i.DesItem } into grupo
							 select new ResumoGPA()
							 {
								 CodLinha = grupo.Key.CodLinha,
								 CodModelo = grupo.Key.CodItem,
								 CodPallet = grupo.Key.CodPallet.ToString(),
								 Data = grupo.Key.DataReferencia,
								 DesLinha = grupo.Key.DesLinha.Trim(),
								 DesModelo = grupo.Key.DesItem.Trim(),
								 QtdPallet = grupo.Count(),
								 NumSerieIni = grupo.Min(x => x.g.NumSerie),
								 NumSerieFim = grupo.Max(x => x.g.NumSerie),
							 }).ToList();

				return dados;

			}

		}

		public static ResumoGPA ObterPalletGPA(string numPallet, string codModelo, string numSerie)
		{

			using (var contexto = new DTB_SIMContext())
			{

				if (!String.IsNullOrEmpty(numPallet))
				{

					var dados = (from m in contexto.tbl_GpaMovimentacao
								 join g in contexto.tbl_BarrasProducao on new { m.CodItem, NumSerie = m.DocInterno2 } equals new { CodItem = g.CodModelo, NumSerie = g.NumSerieModelo }
								 join l in contexto.tbl_Linha on g.CodLinha equals l.CodLinha
								 join i in contexto.tbl_Item on g.CodModelo equals i.CodItem
								 where m.DocInterno1 == numPallet && m.DataFinal == null
								 group new { g, l, i } by new { m.DocInterno1, g.CodLinha, g.CodModelo, l.DesLinha, i.DesItem } into grupo
								 select new ResumoGPA()
								 {
									 CodLinha = grupo.Key.CodLinha,
									 CodModelo = grupo.Key.CodModelo,
									 CodPallet = grupo.Key.DocInterno1,
									 Data = grupo.Min(d => d.g.DatReferencia),
									 DesLinha = grupo.Key.DesLinha.Trim(),
									 DesModelo = grupo.Key.DesItem.Trim(),
									 QtdPallet = grupo.Count(),
									 NumSerieIni = grupo.Min(x => x.g.NumSerieModelo),
									 NumSerieFim = grupo.Max(x => x.g.NumSerieModelo)
								 });

					return dados.FirstOrDefault();
				}
				else
				{
					if (!String.IsNullOrEmpty(numSerie))
					{
						numSerie = numSerie.Substring(8);

						var query2 = (from m in contexto.tbl_GpaMovimentacao
									 join g in contexto.tbl_BarrasProducao on new { m.CodItem, NumSerie = m.DocInterno2 } equals new { CodItem = g.CodModelo, NumSerie = g.NumSerieModelo }
									 join l in contexto.tbl_Linha on g.CodLinha equals l.CodLinha
									 join i in contexto.tbl_Item on g.CodModelo equals i.CodItem
									 let NumPallet = contexto.tbl_GpaMovimentacao.FirstOrDefault(x => x.CodItem == codModelo && x.DocInterno2 == numSerie && x.DataFinal == null).DocInterno1
									 where m.DocInterno1 == NumPallet && m.DataFinal == null
									 group new { g, l, i } by new { m.DocInterno1, g.CodLinha, g.CodModelo, l.DesLinha, i.DesItem } into grupo
									 select new ResumoGPA()
									 {
										 CodLinha = grupo.Key.CodLinha,
										 CodModelo = grupo.Key.CodModelo,
										 CodPallet = grupo.Key.DocInterno1,
										 Data = grupo.Min(d => d.g.DatReferencia),
										 DesLinha = grupo.Key.DesLinha.Trim(),
										 DesModelo = grupo.Key.DesItem.Trim(),
										 QtdPallet = grupo.Count(),
										 NumSerieIni = grupo.Min(x => x.g.NumSerieModelo),
										 NumSerieFim = grupo.Max(x => x.g.NumSerieModelo)
									 });

						return query2.FirstOrDefault();
					}
					else
					{
						return null;
					}
				}

			}

		}

		public static List<string> ObterSeriesPallet(string numPallet)
		{

			using (var contexto = new DTB_SIMContext())
			{
				numPallet = numPallet.PadLeft(6, '0');
				var series = contexto.tbl_GpaMovimentacao.Where(x => x.DocInterno1 == numPallet && x.DataFinal == null).Select(x => x.DocInterno2.Trim()).ToList();

				return series;


			}

		}

		#endregion

		#region Consultas Testes (Defeitos)

		public static List<CBCadDefeitoDTO> ObterTestesOBA()
		{

			using (var contexto = new DTB_CBContext())
			{

				var series = contexto.tbl_CBCadDefeito.Where(x => x.CodDefeito.StartsWith("2")).ToList();

				return series.Select(x => new CBCadDefeitoDTO()
				{
					CodDefeito = x.CodDefeito,
					CodFab = x.CodFab,
					DesDefeito = x.DesDefeito,
					DesVerificacao = x.DesVerificacao,
					flgAtivo = x.flgAtivo,
					flgFEC = x.flgFEC,
					flgIAC = x.flgIAC,
					flgIMC = x.flgIMC,
					flgLCM = x.flgLCM
				}).ToList();


			}

		}

		public static List<CBCadSubDefeitoDTO> ObterTestesEspecificosOBA()
		{

			using (var contexto = new DTB_CBContext())
			{

				var series = contexto.tbl_CBCadSubDefeito.Where(x => x.CodDefeito.StartsWith("2")).ToList();

				return series.Select(x => new CBCadSubDefeitoDTO()
				{
					CodDefeito = x.CodDefeito,
					CodSubDefeito = x.CodSubDefeito,
					DesDefeito = x.DesDefeito,
					DesVerificacao = x.DesVerificacao,
					flgAtivo = x.flgAtivo
				}).ToList();


			}

		}

		#endregion

		#region Consultas OBA
		public static List<OBAResumoInspecao> ObterResumoInspecao(DateTime datInicio, DateTime datFim)
		{

			using (var dtb_CB = new DTB_CBContext())
			using (var dtb_SIM = new DTB_SIMContext())
			{
				if (datInicio == datFim)
				{
					datFim = datFim.AddDays(1).AddMinutes(-1);
				}

				var programa = (from j in dtb_SIM.tbl_ProgDiaJit
								join i in dtb_SIM.tbl_Item on j.CodModelo equals i.CodItem
								from p in dtb_SIM.tbl_ProdDia.Where(x => x.CodFab == j.CodFab && x.CodModelo == j.CodModelo && x.DatProducao == j.DatProducao && x.CodLinha == j.CodLinha).DefaultIfEmpty()
								from Est042 in dtb_SIM.tbl_Estoque.Where(x => x.CodFab == j.CodFab && x.CodItem == j.CodModelo && x.CodLocal == "042").DefaultIfEmpty()
								from Est823 in dtb_SIM.tbl_Estoque.Where(x => x.CodFab == j.CodFab && x.CodItem == j.CodModelo && x.CodLocal == "823").DefaultIfEmpty()
								from Est067 in dtb_SIM.tbl_Estoque.Where(x => x.CodFab == j.CodFab && x.CodItem == j.CodModelo && x.CodLocal == "067").DefaultIfEmpty()
								from Est068 in dtb_SIM.tbl_Estoque.Where(x => x.CodFab == j.CodFab && x.CodItem == j.CodModelo && x.CodLocal == "068").DefaultIfEmpty()
								where j.DatProducao >= datInicio && j.DatProducao <= datFim
								group new { j, i, p} by new { j.CodModelo, j.DatProducao, i.DesItem, Qtd042 = Est042.QtdEstoque, Qtd067 = Est067.QtdEstoque, Qtd068 = Est068.QtdEstoque, Qtd823 = Est823.QtdEstoque } into grupo
								select new OBAResumoInspecao()
								{
									CodModelo = grupo.Key.CodModelo,
									Data = grupo.Key.DatProducao,
									DesModelo = grupo.Key.DesItem.Trim(),
									QtdPrograma = grupo.Sum(x => x.j.QtdProdPm ?? 0),
									QtdProduzido = grupo.Sum(z => z.p.QtdProduzida ?? 0),
									QtdP042 = grupo.Key.Qtd042 ?? 0,
									QtdP823 = grupo.Key.Qtd823 ?? 0,
									QtdP067 = grupo.Key.Qtd067 ?? 0,
									QtdP068 = grupo.Key.Qtd068 ?? 0

								})
								.ToList();

				var pallets = (from g in dtb_SIM.TBL_GpaRecItem
							   where g.DataReferencia >= datInicio && g.DataReferencia <= datFim
							   group g by new { g.CodItem, g.CodPallet } into grupo
							   select new
							   {
								   CodModelo = grupo.Key.CodItem,
								   CodPallet = grupo.Key.CodPallet,
								   QtdPallet = grupo.Count()
							   })
							   .ToList();

				var inspecao = (from i in dtb_CB.tbl_CBOBAInspecao
								join r in dtb_CB.tbl_CBOBARegistro on new { i.CodFab, i.NumRPA, i.CodPallet } equals new { r.CodFab, r.NumRPA, r.CodPallet }
								where r.DatAbertura >= datInicio && r.DatAbertura <= datFim
								group new { i, r } by new { r.CodModelo, i.FlgStatus, i.NumECB } into grupo
								select new
								{
									CodModelo = grupo.Key.CodModelo,
									FlgStatus = grupo.Key.FlgStatus,
									NumECB = grupo.Key.NumECB,
									QtdInspec = grupo.Count()
								}).ToList();

				foreach (var item in programa)
				{
					var qtdPallet = pallets.Count(x => x.CodModelo == item.CodModelo);

					var qtdInspec = inspecao.Count(x => x.CodModelo == item.CodModelo);

					var consInspec = inspecao.Where(x => x.CodModelo == item.CodModelo)
												.GroupBy(x => x.CodModelo)
												.Select(x => new
												{
													CodModelo = x.Key,
													QtdAprov = x.Count(y => y.FlgStatus == "S"),
													QtdReprov = x.Count(z => z.FlgStatus == "X")
												})
												.FirstOrDefault();

					item.QtdLotes = qtdPallet;
					item.QtdInspecionados = qtdInspec;
					item.QtdAprovados = consInspec != null ? consInspec.QtdAprov : 0;
					item.QtdReprovados = consInspec != null ? consInspec.QtdReprov : 0;
				}

				return programa;

			}


		}


		public static CBLoteConfigDTO ObterConfigLote(int codLinha)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dado = contexto.tbl_CBLoteConfig.FirstOrDefault(x => x.idLinha == codLinha);

				if (dado == null)
				{
					return new CBLoteConfigDTO();
				}

				var retorno = new CBLoteConfigDTO()
				{
					idLinha = dado.idLinha,
					CodDRT = dado.CodDRT,
					Qtde = dado.Qtde,
					QtdeCC = dado.QtdeCC,
					PercAmostras = dado.PercAmostras,
					QtdAmostras = dado.QtdAmostras
				};

				return retorno;
			}

		}


		public static List<CBOBARegistroDTO> ObterRegistros(DateTime datInicio, DateTime datFim)
		{

			using (var contexto = new DTB_CBContext())
			{
				if (datInicio == datFim)
				{
					datFim = datFim.AddDays(1).AddMinutes(-1);
				}

				var dados = (from r in contexto.tbl_CBOBARegistro
							 join i in contexto.tbl_Item on r.CodModelo equals i.CodItem
							 join l in contexto.tbl_CBLinha on r.CodLinha equals l.CodLinha
							 join lOri in contexto.tbl_CBLinha on r.CodLinhaOri equals lOri.CodLinhaT1
							 join p in contexto.tbl_CBPosto on new { r.CodLinha, r.NumPosto } equals new { p.CodLinha, p.NumPosto }
							 join s in contexto.tbl_CBOBATipoStatus on r.FlgStatus equals s.CodStatus
							 where r.DatAbertura >= datInicio && r.DatAbertura <= datFim
							 orderby r.DatFechamento
							 select new CBOBARegistroDTO()
							 {
								 CodLinha = r.CodLinha,
								 NumPosto = r.NumPosto,
								 CodFab = r.CodFab,
								 NumRPA = r.NumRPA,
								 CodPallet = r.CodPallet,
								 CodModelo = r.CodModelo,
								 QtdPallet = r.QtdPallet,
								 QtdAmostras = r.QtdAmostras,
								 DatProducao = r.DatProducao,
								 DatAbertura = r.DatAbertura,
								 DatFechamento = r.DatFechamento,
								 CodOperador = r.CodOperador,
								 FlgStatus = r.FlgStatus,
								 CodLinhaOri = r.CodLinhaOri,
								 NumRPE = r.NumRPE,
								 DatLiberado = r.DatLiberado,
								 ObsRevisao = r.ObsRevisao,

								 DesLinha = l.DesLinha,
								 DesPosto = p.DesPosto,
								 DesModelo = i.DesItem.Trim(),
								 DesLinhaOri = lOri.DesLinha,

								 DesCorStatus = s.DesCor,
								 DesStatus = s.DesStatus
							 }).ToList();

				return dados;

			}
		}

		public static List<CBOBARegistroDTO> ObterRegistrosStatus(DateTime datInicio, string codStatus)
		{

			using (var contexto = new DTB_CBContext())
			{
				if (datInicio.Day != 1)
				{
					datInicio = datInicio.AddDays(-datInicio.Day).AddDays(1);
				}

				var dados = (from r in contexto.tbl_CBOBARegistro
							 join i in contexto.tbl_Item on r.CodModelo equals i.CodItem
							 join l in contexto.tbl_CBLinha on r.CodLinha equals l.CodLinha
							 join lOri in contexto.tbl_CBLinha on r.CodLinhaOri equals lOri.CodLinhaT1
							 join p in contexto.tbl_CBPosto on new { r.CodLinha, r.NumPosto } equals new { p.CodLinha, p.NumPosto }
							 join s in contexto.tbl_CBOBATipoStatus on r.FlgStatus equals s.CodStatus
							 where r.FlgStatus == codStatus && r.DatAbertura >= datInicio
							 orderby r.DatFechamento
							 select new CBOBARegistroDTO()
							 {
								 CodLinha = r.CodLinha,
								 NumPosto = r.NumPosto,
								 CodFab = r.CodFab,
								 NumRPA = r.NumRPA,
								 CodPallet = r.CodPallet,
								 CodModelo = r.CodModelo,
								 QtdPallet = r.QtdPallet,
								 QtdAmostras = r.QtdAmostras,
								 DatProducao = r.DatProducao,
								 DatAbertura = r.DatAbertura,
								 DatFechamento = r.DatFechamento,
								 CodOperador = r.CodOperador,
								 FlgStatus = r.FlgStatus,
								 CodLinhaOri = r.CodLinhaOri,
								 NumRPE = r.NumRPE,
								 DatLiberado = r.DatLiberado,
								 ObsRevisao = r.ObsRevisao,

								 DesLinha = l.DesLinha,
								 DesPosto = p.DesPosto,
								 DesModelo = i.DesItem.Trim(),
								 DesLinhaOri = lOri.DesLinha,

								 DesCorStatus = s.DesCor,
								 DesStatus = s.DesStatus
							 }).ToList();

				return dados;

			}
		}

		public static CBOBARegistroDTO ObterRegistro(string codFab, string numRPA, string codPallet)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = (from r in contexto.tbl_CBOBARegistro
							 join i in contexto.tbl_Item on r.CodModelo equals i.CodItem
							 join l in contexto.tbl_CBLinha on r.CodLinha equals l.CodLinha
							 join lOri in contexto.tbl_CBLinha on r.CodLinhaOri equals lOri.CodLinhaT1
							 join p in contexto.tbl_CBPosto on new { r.CodLinha, r.NumPosto } equals new { p.CodLinha, p.NumPosto }
							 where r.CodFab == codFab && r.NumRPA == numRPA && r.CodPallet == codPallet
							 select new CBOBARegistroDTO()
							 {
								 CodLinha = r.CodLinha,
								 NumPosto = r.NumPosto,
								 CodFab = r.CodFab,
								 NumRPA = r.NumRPA,
								 CodPallet = r.CodPallet,
								 CodModelo = r.CodModelo,
								 QtdPallet = r.QtdPallet,
								 QtdAmostras = r.QtdAmostras,
								 DatProducao = r.DatProducao,
								 DatAbertura = r.DatAbertura,
								 DatFechamento = r.DatFechamento,
								 CodOperador = r.CodOperador,
								 FlgStatus = r.FlgStatus,
								 CodLinhaOri = r.CodLinhaOri,
								 NumRPE = r.NumRPE,

								 DesLinha = l.DesLinha,
								 DesPosto = p.DesPosto,
								 DesModelo = i.DesItem.Trim(),
								 DesLinhaOri = lOri.DesLinha
							 });

				return dados.FirstOrDefault();

			}
		}

		public static CBOBARegistroDTO ObterRegistro(string codFab, string numRPA)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = (from r in contexto.tbl_CBOBARegistro
							 join i in contexto.tbl_Item on r.CodModelo equals i.CodItem
							 join l in contexto.tbl_CBLinha on r.CodLinha equals l.CodLinha
							 join lOri in contexto.tbl_CBLinha on r.CodLinhaOri equals lOri.CodLinhaT1
							 join p in contexto.tbl_CBPosto on new { r.CodLinha, r.NumPosto } equals new { p.CodLinha, p.NumPosto }
							 where r.CodFab == codFab && r.NumRPA == numRPA
							 select new CBOBARegistroDTO()
							 {
								 CodLinha = r.CodLinha,
								 NumPosto = r.NumPosto,
								 CodFab = r.CodFab,
								 NumRPA = r.NumRPA,
								 CodPallet = r.CodPallet,
								 CodModelo = r.CodModelo,
								 QtdPallet = r.QtdPallet,
								 QtdAmostras = r.QtdAmostras,
								 DatProducao = r.DatProducao,
								 DatAbertura = r.DatAbertura,
								 DatFechamento = r.DatFechamento,
								 CodOperador = r.CodOperador,
								 FlgStatus = r.FlgStatus,
								 CodLinhaOri = r.CodLinhaOri,
								 NumRPE = r.NumRPE,
								 ObsRevisao = r.ObsRevisao,
								 DatLiberado = r.DatLiberado,

								 DesLinha = l.DesLinha,
								 DesPosto = p.DesPosto,
								 DesModelo = i.DesItem.Trim(),
								 DesLinhaOri = lOri.DesLinha
							 }).FirstOrDefault();

				return dados;

			}
		}


		public static List<OBARelatorioInspecao> ObterInspecoes(DateTime datInicio, DateTime datFim)
		{

			using (var contexto = new DTB_CBContext())
			{
				if (datInicio == datFim)
				{
					datFim = datFim.AddDays(1).AddMinutes(-1);
				}

				var dados = (from i in contexto.tbl_CBOBAInspecao
							 join r in contexto.tbl_CBOBARegistro on new { i.CodFab, i.NumRPA, i.CodPallet } equals new { r.CodFab, r.NumRPA, r.CodPallet }
							 join l in contexto.tbl_CBLinha on i.CodLinha equals l.CodLinha
							 join p in contexto.tbl_CBPosto on new { i.CodLinha, i.NumPosto } equals new { p.CodLinha, p.NumPosto }
							 join lOri in contexto.tbl_CBLinha on r.CodLinhaOri equals lOri.CodLinhaT1
							 join item in contexto.tbl_Item on r.CodModelo equals item.CodItem
							 from f in contexto.tbl_RIFuncionario.Where(x => r.CodOperador == x.NomLogin).DefaultIfEmpty()
							 where r.DatAbertura >= datInicio && r.DatAbertura <= datFim
							 orderby new { r.CodModelo, r.NumRPA }
							 select new OBARelatorioInspecao()
							 {
								 NumECB = i.NumECB,
								 CodLinha = i.CodLinha,
								 NumPosto = i.NumPosto,
								 CodFab = i.CodFab,
								 NumRPA = i.NumRPA,
								 CodPallet = i.CodPallet,
								 DatLeitura = i.DatLeitura,
								 FlgStatus = i.FlgStatus,
								 Observacao = i.Observacao,
								 DesLinha = l.DesLinha.Trim(),
								 DesPosto = p.DesPosto.Trim(),
								 CodLinhaOri = r.CodLinhaOri,
								 CodModelo = r.CodModelo,
								 CodOperador = r.CodOperador,
								 DatProducao = r.DatProducao.Value,
								 DesLinhaOri = lOri.DesLinha.Trim(),
								 DesModelo = item.DesItem.Trim(),
								 NomOperador = f.NomFuncionario.Trim(),
								 NumRPE = r.NumRPE
							 }).ToList();

				return dados;

			}
		}

		public static List<CBOBAInspecaoDTO> ObterInspecoes(string codFab, string numRPA, string codPallet)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = contexto.tbl_CBOBAInspecao.Where(x => x.CodFab == codFab && x.NumRPA == numRPA && x.CodPallet == codPallet).ToList();

				var dadosDTO = new List<CBOBAInspecaoDTO>();

				AutoMapper.Mapper.Map(dados, dadosDTO);

				return dadosDTO;

			}
		}

		public static List<CBOBAInspecaoDTO> ObterInspecoes(string codFab, string numRPA)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = contexto.tbl_CBOBAInspecao.Where(x => x.CodFab == codFab && x.NumRPA == numRPA).ToList();

				var dadosDTO = new List<CBOBAInspecaoDTO>();

				AutoMapper.Mapper.Map(dados, dadosDTO);

				return dadosDTO;

			}
		}

		public static CBOBAInspecaoDTO ObterInspecaoSerie(string numECB, string codFab, string numRPA)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = contexto.tbl_CBOBAInspecao.FirstOrDefault(x => x.CodFab == codFab && x.NumRPA == numRPA && x.NumECB == numECB);

				var dadosDTO = new CBOBAInspecaoDTO();

				AutoMapper.Mapper.Map(dados, dadosDTO);

				return dadosDTO;

			}
		}


		public static List<CBOBATestesDTO> ObterTestes(string codFab, string numRPA, string numECB)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = (from t in contexto.tbl_CBOBATestes
							 join def in contexto.tbl_CBCadDefeito on t.CodDefeito equals def.CodDefeito
							 join sub in contexto.tbl_CBCadSubDefeito on new { t.CodDefeito, t.CodSubDefeito } equals new { sub.CodDefeito, sub.CodSubDefeito }
							 where t.CodFab == codFab && t.NumRPA == numRPA && t.NumECB == numECB
							 select new CBOBATestesDTO()
							 {
								 NumECB = t.NumECB,
								 CodLinha = t.CodLinha,
								 NumPosto = t.NumPosto,
								 CodFab = t.CodFab,
								 NumRPA = t.NumRPA,
								 CodDefeito = t.CodDefeito,
								 CodSubDefeito = t.CodSubDefeito,
								 FlgStatus = t.FlgStatus,
								 Observacao = t.Observacao,

								 DesDefeito = def.DesDefeito,
								 DesSubDefeito = sub.DesDefeito
							 }).ToList();

				return dados;

			}
		}

		public static List<CBOBATestesDTO> ObterTestes(string codFab, string numRPA)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = (from t in contexto.tbl_CBOBATestes
							 join def in contexto.tbl_CBCadDefeito on t.CodDefeito equals def.CodDefeito
							 join sub in contexto.tbl_CBCadSubDefeito on new { t.CodDefeito, t.CodSubDefeito } equals new { sub.CodDefeito, sub.CodSubDefeito }
							 where t.CodFab == codFab && t.NumRPA == numRPA
							 select new CBOBATestesDTO()
							 {
								 NumECB = t.NumECB,
								 CodLinha = t.CodLinha,
								 NumPosto = t.NumPosto,
								 CodFab = t.CodFab,
								 NumRPA = t.NumRPA,
								 CodDefeito = t.CodDefeito,
								 CodSubDefeito = t.CodSubDefeito,
								 FlgStatus = t.FlgStatus,
								 Observacao = t.Observacao,

								 DesDefeito = def.DesDefeito,
								 DesSubDefeito = sub.DesDefeito
							 }).ToList();

				return dados;

			}
		}

		public static CBOBATestesDTO ObterTesteSerie(string numECB, string codFab, string numRPA, string codDefeito, int codSubDefeito)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = contexto.tbl_CBOBATestes.FirstOrDefault(x => x.CodFab == codFab && x.NumRPA == numRPA && x.NumECB == numECB && x.CodDefeito == codDefeito && x.CodSubDefeito == codSubDefeito);

				var dadosDTO = new CBOBATestesDTO();

				AutoMapper.Mapper.Map(dados, dadosDTO);

				return dadosDTO;

			}
		}

		public static string ObterECBSerie(string codModelo, string numSerie)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dado = contexto.tbl_CBIdentifica.FirstOrDefault(x => x.NumECB.StartsWith(codModelo) && x.NumECB.EndsWith(numSerie));


				if (dado != null)
				{
					return dado.NumECB;
				}
				else
				{
					return "";
				}

			}



		}

		public static bool VerificaPalletTotal(string codFab, string numRPA)
		{

			using (var contexto = new DTB_CBContext())
			{

				var qtdAmostra = 0;

				var registro = contexto.tbl_CBOBARegistro.FirstOrDefault(x => x.CodFab == codFab && x.NumRPA == numRPA);
				if (registro != null)
				{
					qtdAmostra = registro.QtdAmostras ?? 0;
				}

				var inspecao = contexto.tbl_CBOBAInspecao.Count(x => x.CodFab == codFab && x.NumRPA == numRPA);

				return (inspecao == qtdAmostra);

			}

		}

		public static int ObterQtdAprovPallet(string codFab, string numRPA, string flgAprov)
		{

			using (var contexto = new DTB_CBContext())
			{
				return contexto.tbl_CBOBAInspecao.Count(x => x.CodFab == codFab && x.NumRPA == numRPA && x.FlgStatus == flgAprov);
			}

		}

		#endregion

		#region Manutenção OBA

		public static string GravarRegistro(CBOBARegistroDTO dados, bool flgExcluir, bool flgMovimenta)
		{
			try
			{

				using (var contexto = new DTB_CBContext())
				{

					var existe = contexto.tbl_CBOBARegistro.FirstOrDefault(x => x.CodFab == dados.CodFab && x.NumRPA == dados.NumRPA);

					if (existe != null)
					{
						if (flgExcluir)
						{
							contexto.tbl_CBOBARegistro.Remove(existe);
						}
						else
						{
							existe.QtdAmostras = dados.QtdAmostras;
							existe.FlgStatus = dados.FlgStatus;
							existe.DatAbertura = dados.DatAbertura;
							existe.DatFechamento = dados.DatFechamento;
							existe.CodOperador = dados.CodOperador;
							existe.CodLinhaOri = dados.CodLinhaOri;
							existe.DatProducao = dados.DatProducao;
							existe.NumRPE = dados.NumRPE;
							existe.DatLiberado = dados.DatLiberado;
							existe.ObsRevisao = dados.ObsRevisao;

						}
					}
					else
					{
						var dadosFinal = new tbl_CBOBARegistro();

						AutoMapper.Mapper.Map(dados, dadosFinal);

						contexto.tbl_CBOBARegistro.Add(dadosFinal);

						if (flgMovimenta)
						{
							var resultado = EfetuaMovimentacao(dados.CodFab, dados.NumRPA, dados.CodModelo, dados.CodPallet, dados.CodOperador);

							if (resultado.StartsWith("false"))
							{
								return resultado;
							}
						}

					}


					contexto.SaveChanges();

					return "true";


				}

			}
			catch
			{
				return "false";
			}

		}

		public static bool GravarInspecao(CBOBAInspecaoDTO dados, bool flgExcluir)
		{

			using (var contexto = new DTB_CBContext())
			{

				var existe = contexto.tbl_CBOBAInspecao.FirstOrDefault(x => x.CodFab == dados.CodFab && x.NumRPA == dados.NumRPA && x.NumECB == dados.NumECB);

				if (existe != null)
				{
					if (flgExcluir)
					{
						contexto.tbl_CBOBAInspecao.Remove(existe);
					}
					else
					{
						existe.FlgStatus = dados.FlgStatus;
						existe.Observacao = dados.Observacao;
						existe.DatLeitura = dados.DatLeitura;
					}
				}
				else
				{
					var dadosFinal = new tbl_CBOBAInspecao();

					AutoMapper.Mapper.Map(dados, dadosFinal);

					contexto.tbl_CBOBAInspecao.Add(dadosFinal);

				}


				contexto.SaveChanges();

				return true;


			}

		}

		public static bool GravarTeste(CBOBATestesDTO dados, bool flgExcluir)
		{

			using (var contexto = new DTB_CBContext())
			{

				var existe = contexto.tbl_CBOBATestes.FirstOrDefault(x => x.CodFab == dados.CodFab && x.NumRPA == dados.NumRPA && x.NumECB == dados.NumECB && x.CodDefeito == dados.CodDefeito && x.CodSubDefeito == dados.CodSubDefeito);

				if (existe != null)
				{
					if (flgExcluir)
					{
						contexto.tbl_CBOBATestes.Remove(existe);
					}
					else
					{
						existe.FlgStatus = dados.FlgStatus;
						existe.Observacao = dados.Observacao;
					}
				}
				else
				{
					var dadosFinal = new tbl_CBOBATestes();

					AutoMapper.Mapper.Map(dados, dadosFinal);

					contexto.tbl_CBOBATestes.Add(dadosFinal);

				}

				contexto.SaveChanges();

				return true;


			}

		}

		#endregion

		#region Manutenção GPA 

		public static string EfetuaMovimentacao(string CodFab, string NumRPA, string CodModelo, string CodPallet, string CodOperador)
		{
			try
			{

				using (var contexto = new DTB_SIMContext())
				{
					var resultado = contexto.Database.SqlQuery<int>(
						"EXEC spc_SISAP_OBAMovimenta @CodFab, @NumRPA, @CodModelo, @NumPallet, @CodOperador",
						new SqlParameter("CodFab", CodFab),
						new SqlParameter("NumRPA", NumRPA),
						new SqlParameter("CodModelo", CodModelo),
						new SqlParameter("NumPallet", CodPallet),
						new SqlParameter("CodOperador", CodOperador)
						);

					var retorno = resultado.First();
					return "true";
				}


			}
			catch (Exception ex)
			{
				return "false|" + ex.Message;
			}

		}


		#endregion

		#region Consultas Status

		public static List<CBOBATipoStatusDTO> ObterListaStatus() {

			using (var contexto = new DTB_CBContext())
			{
				var dados = contexto.tbl_CBOBATipoStatus
									.Select(x => new CBOBATipoStatusDTO() { 
										CodStatus = x.CodStatus,
										DesCor = x.DesCor,
										DesStatus = x.DesStatus
									})
									.ToList();

				return dados;

			}

		}

		public static CBOBATipoStatusDTO ObterTipoStatus(string codStatus)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = contexto.tbl_CBOBATipoStatus
									.Where(c => c.CodStatus == codStatus)
									.Select(x => new CBOBATipoStatusDTO()
									{
										CodStatus = x.CodStatus,
										DesCor = x.DesCor,
										DesStatus = x.DesStatus
									})
									.FirstOrDefault();

				return dados;

			}

		}

		#endregion

	}
}
