using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBPassagem_HMap : EntityTypeConfiguration<tbl_CBPassagem_H>
    {
        public tbl_CBPassagem_HMap()
        {
            // Primary Key
            this.HasKey(t => new { t.NumECB, t.CodLinha, t.NumPosto, t.DatEvento });

            // Properties
            this.Property(t => t.NumECB)
                .IsRequired()
                .HasMaxLength(13);

            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumPosto)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodDRT)
                .IsFixedLength()
                .HasMaxLength(8);

            // Table & Column Mappings
            this.ToTable("tbl_CBPassagem_H");
            this.Property(t => t.NumECB).HasColumnName("NumECB");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.NumPosto).HasColumnName("NumPosto");
            this.Property(t => t.DatEvento).HasColumnName("DatEvento");
            this.Property(t => t.CodDRT).HasColumnName("CodDRT");
        }
    }
}
