namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.LOT_VIEW")]
    public partial class LOT_VIEW
    {
        public long? ID { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_LIST { get; set; }

        public DateTime? dtPallet { get; set; }

        [StringLength(255)]
        public string PACKING { get; set; }

        public int? LINE { get; set; }

        [StringLength(25)]
        public string ID_LOTE { get; set; }

        [StringLength(255)]
        public string ORDER_ID { get; set; }

        public int? RESP_PALLET { get; set; }

        [StringLength(255)]
        public string order_idorder { get; set; }

        [StringLength(40)]
        public string fullSku { get; set; }
    }
}
