﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using RAST.BO.ApontaProd;
using SEMP.Model.VO.Rastreabilidade.Relatorios;

namespace SEMP.Areas.Coleta.Controllers
{
	[Authorize]
	public class ProducaoHoraController : Controller
	{
		//
		// GET: /Coleta/ProducaoHora/
		public ActionResult Index()
		{
			return View();
		}


		public ActionResult Lista(String Data, String Fase)
		{

			if (String.IsNullOrEmpty(Data))
			{
				Data = DateTime.Now.ToShortDateString();
			}

			if (String.IsNullOrEmpty(Fase))
			{
				Fase = "FEC";
			}

			var producao = ProducaoHoraAHoraBO.ObterProducaoHoraAHora(DateTime.Parse(Data), Fase) ?? new List<ProducaoHoraAHora>();

			return View(producao);
		}

		public ActionResult ListaHora(String data, int? minutos, string fase)
		{
			try
			{
				if (String.IsNullOrEmpty(data))
				{
					data = DateTime.Now.ToShortDateString();
				}

				var producao = ProducaoHoraAHoraBO.ObterProducaoLinhaMinuto(DateTime.Parse(data), fase);

				return View(producao);
			}
			catch (Exception)
			{
				var erro = new List<ProducaoLinhaMinuto>();
				return View(erro);

			}

		}



	}
}
