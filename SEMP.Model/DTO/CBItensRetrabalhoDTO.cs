﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.DTO
{
	public class CBItensRetrabalhoDTO
	{
		public int CodRetrabalho { get; set; }
		public string NumECB { get; set; }
	}
}
