﻿using System;
using System.Collections.Generic;
using System.Linq;
using SEMP.Model.DTO;
using SEMP.DAL.Models;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;

namespace SEMP.DAL.DAO.Rastreabilidade
{
	public class daoResumoIAC
	{
        //static daoResumoIAC()
        //{
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<tbl_CBResumoIAC, CBResumoIACDTO>();
        //        cfg.CreateMap<CBResumoIACDTO, tbl_CBResumoIAC>();
        //    });
        //}

        public static List<CBResumoIACDTO> ObterResumoIAC(DateTime DatInicio, DateTime DatFim)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				var resumoQ = (from i in dtbCb.tbl_CBResumoIAC
							   where i.Data >= DatInicio && i.Data <= DatFim
							   select i).OrderBy(x => x.Linha);

				var resumo = resumoQ.ToList();

				((IObjectContextAdapter)dtbCb).ObjectContext.Refresh(RefreshMode.StoreWins, resumo);

				List<CBResumoIACDTO> CBResumoIAC = new List<CBResumoIACDTO>();

				AutoMapper.Mapper.Map(resumo, CBResumoIAC);

				return CBResumoIAC;
			}
		}

		public static CBResumoIACDTO ObterResumoIAC(DateTime Data, int Turno, string Linha, string CodModelo)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				var resumoQ = (from i in dtbCb.tbl_CBResumoIAC
							   where i.Data == Data && i.Turno == Turno && i.Linha == Linha && i.CodModelo == CodModelo
							   select i);

				var resumo = resumoQ.FirstOrDefault();

				CBResumoIACDTO CBResumoIACDTO = new CBResumoIACDTO();
                
				AutoMapper.Mapper.Map(resumo, CBResumoIACDTO);

				return CBResumoIACDTO;
			}
		}

		public static List<CBResumoIACDTO> ObterProgIAC(DateTime DatInicio, DateTime DatFim)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var programaQ = (from jit in dtbSim.tbl_ProgDiaPlaca
								 join item in dtbSim.tbl_Item on jit.CodPlaca equals item.CodItem
								 join p in dtbSim.tbl_ProdDiaPlaca on new { jit.CodLinha, jit.CodPlaca, jit.DatProducao, jit.NumTurno }
																			equals new { p.CodLinha, p.CodPlaca, p.DatProducao, p.NumTurno } into p2
								 from prod in p2.DefaultIfEmpty()
								 where jit.DatProducao >= DatInicio && jit.DatProducao <= DatFim && jit.CodProcesso.StartsWith("01")
								 select new CBResumoIACDTO
								 {
									 Data = jit.DatProducao,
									 Turno = jit.NumTurno,
									 Linha = jit.CodLinha.Trim(),
									 CodModelo = jit.CodPlaca,
									 DesModelo = item.DesItem,
									 QtdPlano = (int)jit.QtdProdPM,
									 QtdReal = (int)prod.QtdProduzida,
									 Apontado = (int)prod.QtdApontada,
									 FlagTP = jit.FlagTP.Trim()
								 }).OrderBy(x => x.Linha);

				var programa = programaQ.ToList();

				var produzidoQ = (from jit in dtbSim.tbl_ProdDiaPlaca
								  join item in dtbSim.tbl_Item on jit.CodPlaca equals item.CodItem
								  join linha in dtbSim.tbl_BarrasLinha on jit.CodLinha equals linha.CodLinhaRel
								  where jit.DatProducao >= DatInicio && jit.DatProducao <= DatFim && jit.CodProcesso.StartsWith("01")
								  select new CBResumoIACDTO
								  {
									  Data = jit.DatProducao,
									  Turno = jit.NumTurno,
									  Linha = linha.CodLinha.Trim(),
									  CodModelo = jit.CodPlaca,
									  DesModelo = item.DesItem,
									  QtdPlano = 0,
									  QtdReal = (int)jit.QtdProduzida,
									  Apontado = (int)jit.QtdApontada,
									  FlagTP = jit.FlagTP.Trim()
								  }).OrderBy(x => x.Linha);

				var produzido = produzidoQ.ToList();
				var excessao = produzido.Except(programa).ToList();

				programa.AddRange(excessao);

				return programa;
			}
		}

		public static CBResumoIACDTO AtualizaResumo(CBResumoIACDTO resumo)
		{

			try
			{
				tbl_CBResumoIAC resumoIAC = new tbl_CBResumoIAC();

				using (DTB_CBContext dtbCb = new DTB_CBContext())
				{
					if (dtbCb.tbl_CBResumoIAC.Any(a => a.Data == resumo.Data && a.Turno == resumo.Turno && a.Linha == resumo.Linha && a.CodModelo == resumo.CodModelo && a.FlagTP == resumo.FlagTP))
					{
						resumoIAC = dtbCb.tbl_CBResumoIAC.FirstOrDefault(a => a.Data == resumo.Data && a.Turno == resumo.Turno && a.Linha == resumo.Linha && a.CodModelo == resumo.CodModelo && a.FlagTP == resumo.FlagTP);
						AutoMapper.Mapper.Map(resumo, resumoIAC);

						if (resumo.QtdReal == 0 && resumo.QtdPlano == 0)
						{
							dtbCb.tbl_CBResumoIAC.Remove(resumoIAC);
						}
					}
					else
					{
						AutoMapper.Mapper.Map(resumo, resumoIAC);
						dtbCb.tbl_CBResumoIAC.Add(resumoIAC);
					}

					dtbCb.SaveChanges();

					return resumo;
				}
			}
			catch
			{
				return resumo;
			}
		}

		public static string ObterRevisaoPlaca(string CodModelo)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var tblPlanoIac = dtbSim.tbl_PlanoIAC.OrderByDescending(o => o.DatUltAlt).FirstOrDefault(x => x.CodModelo == CodModelo && x.CodStatus == "3");

				return tblPlanoIac != null ? tblPlanoIac.NumRevisao : "01";
			}
		}

		public static string ObterProcessoPlaca(string CodModelo)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var tblPlanoIac = dtbSim.tbl_PlanoIAC.FirstOrDefault(x => x.CodModelo == CodModelo);

				return tblPlanoIac != null ? tblPlanoIac.CodProcesso : "01-05";
			}
		}

		public static int ObterQuantidadePontos(string CodModelo)
		{
			var numRevisao = ObterRevisaoPlaca(CodModelo);
			var codProcesso = ObterProcessoPlaca(CodModelo);

			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var itens = (from iac in dtbSim.tbl_PlanoIAC
							 join etapa in dtbSim.tbl_PlanoEtapa on
								 new { iac.CodModelo, iac.NumRevisao, iac.CodProcesso, iac.FlgLeftRight }
								 equals new { etapa.CodModelo, etapa.NumRevisao, etapa.CodProcesso, etapa.FlgLeftRight }
							 join item in dtbSim.tbl_PlanoIACItem on
								 new { etapa.CodModelo, etapa.NumRevisao, etapa.CodProcesso, etapa.NumEtapa, etapa.FlgLeftRight }
								 equals new { item.CodModelo, item.NumRevisao, item.CodProcesso, item.NumEtapa, item.FlgLeftRight }
							 where iac.CodModelo == CodModelo && iac.NumRevisao == numRevisao && iac.CodProcesso == codProcesso &&
									iac.CodStatus == "3" && !item.NumPosicao.Trim().Equals(String.Empty)
							 select item.CodItem
					).Count();

				return itens;
			}
		}

		public static List<CBLinhaDTO> LinhasIAC(string Setor, DateTime? datProducao)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				var linhas = (from l in dtbCb.tbl_CBLinha
							  from o in dtbCb.tbl_CBLinhaObservacao.Where(x => l.CodLinha == x.CodLinha && x.DatProducao == datProducao).DefaultIfEmpty()
							  where l.Setor == Setor && l.CodLinhaT1.Trim().Substring(0, 3) == "110"
							  select new CBLinhaDTO()
							  {
								  CodLinha = l.CodLinha,
								  CodFam = l.CodFam,
								  CodLinhaT1 = l.CodLinhaT1,
								  CodModelo = l.CodModelo,
								  CodPlaca = l.CodPlaca,
								  DesLinha = l.DesLinha,
								  DesObs = l.DesObs,
								  Observacao = o.Observacao,
								  Familia = l.Familia,
								  Setor = l.Setor
							  }).ToList();

				return linhas;
			}
		}

		public static int RecuperarCodLinhaFromT3(string codlinha)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				var CodLinha = (from l in dtbCb.tbl_CBLinha where l.CodLinhaT3 == codlinha select l.CodLinha).FirstOrDefault();

				return CodLinha;
			}
		}

		public static string RecuperarCodLinhaT1FromT3(string codlinha)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				var CodLinha = (from l in dtbCb.tbl_CBLinha where l.CodLinhaT3 == codlinha select l.CodLinhaT1).FirstOrDefault();

				return CodLinha;
			}
		}

		public static int ObterQuantidadeProduzida(DateTime dataproducao, string codlinha, string modelo, int turno, string flagTP = "")
		{

			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var producao = (
								from p in dtbSim.tbl_ProdDiaPlaca
								where p.DatProducao == dataproducao && p.CodLinha == codlinha && p.CodPlaca == modelo && p.NumTurno == turno && p.FlagTP == flagTP
								select p.QtdProduzida).FirstOrDefault();
				return Convert.ToInt32(producao);
			}

		}
	}
}
