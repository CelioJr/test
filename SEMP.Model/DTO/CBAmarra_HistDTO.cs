using System;

namespace SEMP.Model.DTO
{
    /// <summary>
    /// Representa a tabela CBAmarra_Hist do banco de dados.
    /// </summary>
    public class CBAmarra_HistDTO
    {
        public string NumSerie { get; set; }
        public string NumECB { get; set; }
        public string CodModelo { get; set; }
        public Nullable<int> FlgTipo { get; set; }
        public Nullable<System.DateTime> DatAmarra { get; set; }
        public Nullable<int> CodLinha { get; set; }
        public Nullable<int> NumPosto { get; set; }
        public string CodFab { get; set; }
        public string NumAP { get; set; }
        public string CodItem { get; set; }

        //Outros
        public string DesTipoAmarra { get; set; }
        public bool IsDispositivoIntegrado { get; set; }
        public int DipositivoID { get; set; }

        public string DataReferencia { get; set; }

        public string CrachaSupervisor { get; set; }

        public string DescricaoLinha { get; set; }
        public string DescricaoPosto { get; set; }
    }
}
