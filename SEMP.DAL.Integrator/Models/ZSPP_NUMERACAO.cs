namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ZSPP_NUMERACAO
    {
        [Key]
        [StringLength(20)]
        public string Codigo { get; set; }

        public int? Sequencia { get; set; }
    }
}
