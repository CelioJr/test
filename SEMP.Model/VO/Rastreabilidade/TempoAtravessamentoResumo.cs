﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade
{
	public class TempoAtravessamentoResumo
	{
		public DateTime Data { get; set; }
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public int QtdProduzido { get; set; }
		public int? QtdDefeitos { get; set; }
		public int? TempoAtravessa { get; set; }
		public int? TempoWIP { get; set; }
		public int? MenorTempo { get; set; }
		public int? MaiorTempo { get; set; }
		public int? TempoCorrido { get; set; }
		public int? TempoTecnico { get; set; }
		public int? TempoIAC { get; set; }
		public int? TempoIMC { get; set; }
		public int? TempoMF { get; set; }
		public int? TempoProducao { get; set; }
		public int? QtdAnalisados { get; set; }

		public int? WIP1 { get; set; }
		public int? WIP2 { get; set; }
		public int? WIP3 { get; set; }

	}
}
