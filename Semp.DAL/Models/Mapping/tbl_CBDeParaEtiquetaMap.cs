using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBDeParaEtiquetaMap : EntityTypeConfiguration<tbl_CBDeParaEtiqueta>
    {
        public tbl_CBDeParaEtiquetaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodModelo, t.CodEtiqueta });

            // Properties
            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6);

            this.Property(t => t.CodEtiqueta)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6);

            // Table & Column Mappings
            this.ToTable("tbl_CBDeParaEtiqueta");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.CodEtiqueta).HasColumnName("CodEtiqueta");
        }
    }
}
