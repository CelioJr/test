namespace SEMP.Model.DTO
{
	using System;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;

	public partial class tbl_LogsServico_DTO
	{
        public int id { get; set; }

        [StringLength(50)]
        public string NomPrograma { get; set; }

        public DateTime? DatExecucao { get; set; }

        [Column(TypeName = "text")]
        public string Mensagem { get; set; }
    }
}
