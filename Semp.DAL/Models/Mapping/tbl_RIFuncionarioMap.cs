using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_RIFuncionarioMap : EntityTypeConfiguration<tbl_RIFuncionario>
    {
        public tbl_RIFuncionarioMap()
        {
            // Primary Key
            this.HasKey(t => t.CodDRT);

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodDRT)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.FlgTipo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.NomUsuario)
                .IsFixedLength()
                .HasMaxLength(30)
                .IsUnicode(false);

            this.Property(t => t.NomFuncionario)
                .IsFixedLength()
                .HasMaxLength(60)
                .IsUnicode(false);

            this.Property(t => t.NomLogin)
                .HasMaxLength(50)
                .IsUnicode(false);

            this.Property(t => t.CodCargo)
                .IsFixedLength()
                .HasMaxLength(4)
                .IsUnicode(false);

            this.Property(t => t.CodSetor)
                .IsFixedLength()
                .HasMaxLength(15)
                .IsUnicode(false);

            this.Property(t => t.CodSecao)
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.CodDepto)
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.CodCBO)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumCTPS)
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.UF_CTPS)
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.Serie_CTPS)
                .IsFixedLength()
                .HasMaxLength(7)
                .IsUnicode(false);

            this.Property(t => t.NumPIS)
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.NumCPF)
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.FlgAtivo)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.FlgSexo)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.Endereco)
                .HasMaxLength(50)
                .IsUnicode(false);

            this.Property(t => t.Numero)
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.Bairro)
                .HasMaxLength(30)
                .IsUnicode(false);

            this.Property(t => t.Cidade)
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.NumCEP)
                .HasMaxLength(9)
                .IsUnicode(false);

            this.Property(t => t.UF)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NumTelefone)
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.NomEmpresa)
                .HasMaxLength(80)
                .IsUnicode(false);

            this.Property(t => t.NomMae)
                .HasMaxLength(60)
                .IsUnicode(false);

            this.Property(t => t.NomPai)
                .HasMaxLength(60)
                .IsUnicode(false);

            this.Property(t => t.FlgECivil)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.DesNatural)
                .HasMaxLength(50)
                .IsUnicode(false);

            this.Property(t => t.DesNacional)
                .HasMaxLength(50)
                .IsUnicode(false);

            this.Property(t => t.FlgAutoriza)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.NumRamal)
                .HasMaxLength(9)
                .IsUnicode(false);

            this.Property(t => t.NumComplemento)
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.NumRG)
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.NomExpedidor)
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.UFNacto)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NumCracha)
                .HasMaxLength(16)
                .IsUnicode(false);

            this.Property(t => t.CodContabil)
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.flgTurno)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.CodPagamento)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.TituloEleitor)
                .IsFixedLength()
                .HasMaxLength(15)
                .IsUnicode(false);

            this.Property(t => t.ZonaTitulo)
                .IsFixedLength()
                .HasMaxLength(4)
                .IsUnicode(false);

            this.Property(t => t.SecaoTitulo)
                .IsFixedLength()
                .HasMaxLength(4)
                .IsUnicode(false);

            this.Property(t => t.CodRegra)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CargaHoraDiaria)
                .IsFixedLength()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.CargaHoraMensal)
                .IsFixedLength()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.StatusProvisorio)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.FlgExtra)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.NumNIT)
                .IsFixedLength()
                .HasMaxLength(12)
                .IsUnicode(false);

            this.Property(t => t.TipBeneficio)
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.TipRevezamento)
                .IsFixedLength()
                .HasMaxLength(15)
                .IsUnicode(false);

            this.Property(t => t.CodDRTCript)
                .IsFixedLength()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.flgGerarEspelho)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.CodProcesso)
                .HasMaxLength(30)
                .IsUnicode(false);

            this.Property(t => t.Rota)
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.PontoReferencia)
                .HasMaxLength(80)
                .IsUnicode(false);

            this.Property(t => t.flgSempar)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.Agencia)
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumConta)
                .HasMaxLength(10)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_RIFuncionario");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.CodDRT).HasColumnName("CodDRT");
            this.Property(t => t.FlgTipo).HasColumnName("FlgTipo");
            this.Property(t => t.FlgControleAcesso).HasColumnName("FlgControleAcesso");
            this.Property(t => t.NomUsuario).HasColumnName("NomUsuario");
            this.Property(t => t.NomFuncionario).HasColumnName("NomFuncionario");
            this.Property(t => t.NomLogin).HasColumnName("NomLogin");
            this.Property(t => t.CodCargo).HasColumnName("CodCargo");
            this.Property(t => t.CodSetor).HasColumnName("CodSetor");
            this.Property(t => t.CodSecao).HasColumnName("CodSecao");
            this.Property(t => t.CodDepto).HasColumnName("CodDepto");
            this.Property(t => t.CodCBO).HasColumnName("CodCBO");
            this.Property(t => t.DatAdm).HasColumnName("DatAdm");
            this.Property(t => t.DatNas).HasColumnName("DatNas");
            this.Property(t => t.NumCTPS).HasColumnName("NumCTPS");
            this.Property(t => t.UF_CTPS).HasColumnName("UF_CTPS");
            this.Property(t => t.Serie_CTPS).HasColumnName("Serie_CTPS");
            this.Property(t => t.NumPIS).HasColumnName("NumPIS");
            this.Property(t => t.NumCPF).HasColumnName("NumCPF");
            this.Property(t => t.FlgAtivo).HasColumnName("FlgAtivo");
            this.Property(t => t.FlgSexo).HasColumnName("FlgSexo");
            this.Property(t => t.Endereco).HasColumnName("Endereco");
            this.Property(t => t.Numero).HasColumnName("Numero");
            this.Property(t => t.Bairro).HasColumnName("Bairro");
            this.Property(t => t.Cidade).HasColumnName("Cidade");
            this.Property(t => t.NumCEP).HasColumnName("NumCEP");
            this.Property(t => t.UF).HasColumnName("UF");
            this.Property(t => t.NumTelefone).HasColumnName("NumTelefone");
            this.Property(t => t.NomEmpresa).HasColumnName("NomEmpresa");
            this.Property(t => t.NomMae).HasColumnName("NomMae");
            this.Property(t => t.NomPai).HasColumnName("NomPai");
            this.Property(t => t.FlgECivil).HasColumnName("FlgECivil");
            this.Property(t => t.DesNatural).HasColumnName("DesNatural");
            this.Property(t => t.DesNacional).HasColumnName("DesNacional");
            this.Property(t => t.FlgAutoriza).HasColumnName("FlgAutoriza");
            this.Property(t => t.FlgEscola).HasColumnName("FlgEscola");
            this.Property(t => t.FlgLivre).HasColumnName("FlgLivre");
            this.Property(t => t.FlgIncentivo).HasColumnName("FlgIncentivo");
            this.Property(t => t.NumRamal).HasColumnName("NumRamal");
            this.Property(t => t.NumComplemento).HasColumnName("NumComplemento");
            this.Property(t => t.NumRG).HasColumnName("NumRG");
            this.Property(t => t.DatExpedicao).HasColumnName("DatExpedicao");
            this.Property(t => t.NomExpedidor).HasColumnName("NomExpedidor");
            this.Property(t => t.UFNacto).HasColumnName("UFNacto");
            this.Property(t => t.NumCracha).HasColumnName("NumCracha");
            this.Property(t => t.CodContabil).HasColumnName("CodContabil");
            this.Property(t => t.DatDemissao).HasColumnName("DatDemissao");
            this.Property(t => t.flgTurno).HasColumnName("flgTurno");
            this.Property(t => t.CodPagamento).HasColumnName("CodPagamento");
            this.Property(t => t.TituloEleitor).HasColumnName("TituloEleitor");
            this.Property(t => t.ZonaTitulo).HasColumnName("ZonaTitulo");
            this.Property(t => t.SecaoTitulo).HasColumnName("SecaoTitulo");
            this.Property(t => t.CodRegra).HasColumnName("CodRegra");
            this.Property(t => t.CargaHoraDiaria).HasColumnName("CargaHoraDiaria");
            this.Property(t => t.CargaHoraMensal).HasColumnName("CargaHoraMensal");
            this.Property(t => t.StatusProvisorio).HasColumnName("StatusProvisorio");
            this.Property(t => t.FlgExtra).HasColumnName("FlgExtra");
            this.Property(t => t.NumNIT).HasColumnName("NumNIT");
            this.Property(t => t.TipBeneficio).HasColumnName("TipBeneficio");
            this.Property(t => t.TipRevezamento).HasColumnName("TipRevezamento");
            this.Property(t => t.CodDRTCript).HasColumnName("CodDRTCript");
            this.Property(t => t.flgGerarEspelho).HasColumnName("flgGerarEspelho");
            this.Property(t => t.ValSalario).HasColumnName("ValSalario");
            this.Property(t => t.DatFim1Periodo).HasColumnName("DatFim1Periodo");
            this.Property(t => t.DatFim2Periodo).HasColumnName("DatFim2Periodo");
            this.Property(t => t.CodProcesso).HasColumnName("CodProcesso");
            this.Property(t => t.Rota).HasColumnName("Rota");
            this.Property(t => t.PontoReferencia).HasColumnName("PontoReferencia");
            this.Property(t => t.flgSempar).HasColumnName("flgSempar");
            this.Property(t => t.Agencia).HasColumnName("Agencia");
            this.Property(t => t.NumConta).HasColumnName("NumConta");
        }
    }
}
