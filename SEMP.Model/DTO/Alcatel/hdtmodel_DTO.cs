using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class hdtmodel_DTO
    {
        public hdtmodel_DTO()
        {
            this.hdtmodelranges = new List<hdtmodelrange_DTO>();
        }

        public long id { get; set; }
        public string description { get; set; }
        public Nullable<System.DateTime> lastModified { get; set; }
        public string modelNick { get; set; }
        public int option1 { get; set; }
        public int option2 { get; set; }
        public int option3 { get; set; }
        public string pathImei { get; set; }
        public string pathMac { get; set; }
        public string userNameModified { get; set; }
        public virtual ICollection<hdtmodelrange_DTO> hdtmodelranges { get; set; }
    }
}
