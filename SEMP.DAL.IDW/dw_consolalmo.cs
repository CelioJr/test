namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolalmo
    {
        [Key]
        public long id_consolalmo { get; set; }

        public decimal? seg_auto_tempoalerta { get; set; }

        public decimal? seg_manu_tempoalerta { get; set; }

        public long id_consolal { get; set; }

        public long id_consolmo { get; set; }

        public virtual dw_consolal dw_consolal { get; set; }

        public virtual dw_consolmo dw_consolmo { get; set; }
    }
}
