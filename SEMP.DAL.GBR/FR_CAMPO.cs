namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_CAMPO
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(96)]
        public string TAB_NOME { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(96)]
        public string CMP_NOME { get; set; }

        [Required]
        [StringLength(200)]
        public string CMP_DESCRICAO { get; set; }

        [StringLength(60)]
        public string CMP_VALORPADRAO { get; set; }

        public int? TPD_CODIGO { get; set; }

        public virtual FR_TABELA FR_TABELA { get; set; }
    }
}
