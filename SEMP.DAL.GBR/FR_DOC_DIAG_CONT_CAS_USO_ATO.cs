namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_DOC_DIAG_CONT_CAS_USO_ATO
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CAS_USO_ATO_CODIGO { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CON_CODIGO { get; set; }

        public virtual FR_DOC_DIAGRAMA_CONTEXTO FR_DOC_DIAGRAMA_CONTEXTO { get; set; }
    }
}
