namespace SEMP.Model.DTO
{
    public class CnqFamiliaDTO
    {
        public string CodFam { get; set; }
        public string NomFam { get; set; }
    }
}
