namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_modulo_recurso
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_modulo_recurso()
        {
            om_licmodrec = new HashSet<om_licmodrec>();
        }

        [Key]
        public long id_modulo_recurso { get; set; }

        [StringLength(256)]
        public string ds_modulo_recurso { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_licmodrec> om_licmodrec { get; set; }
    }
}
