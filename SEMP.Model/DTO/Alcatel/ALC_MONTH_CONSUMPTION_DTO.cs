using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_MONTH_CONSUMPTION_DTO
    {
        public decimal ID_MONTH_CONSUMPTION { get; set; }
        public Nullable<double> CONDOMINIUM { get; set; }
        public Nullable<System.DateTime> DT_CREATED { get; set; }
        public Nullable<double> ENERGY_VLR { get; set; }
        public string MONTH_CONSUMPTION { get; set; }
        public Nullable<int> PEOPLE { get; set; }
        public Nullable<double> RENTING { get; set; }
        public Nullable<double> WATER_VLR { get; set; }
        public Nullable<int> YEAR_CONSUMPTION { get; set; }
    }
}
