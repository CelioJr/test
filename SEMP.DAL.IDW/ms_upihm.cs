namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ms_upihm
    {
        [Key]
        public long id_upihm { get; set; }

        public DateTime? dthr_cadastro { get; set; }

        public long id_up { get; set; }

        public long id_ihm { get; set; }

        [StringLength(100)]
        public string url_conexao { get; set; }

        public virtual ms_ihm ms_ihm { get; set; }

        public virtual ms_up ms_up { get; set; }
    }
}
