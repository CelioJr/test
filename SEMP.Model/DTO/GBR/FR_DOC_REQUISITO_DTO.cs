using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DOC_REQUISITO_DTO
    {
        public int DOC_CODIGO { get; set; }
        public string REQ_DESCRICAO { get; set; }
        public string REQ_TIPO { get; set; }
        public virtual FR_DOC_PRINCIPAL_DTO FR_DOC_PRINCIPAL { get; set; }
    }
}
