﻿using RAST.BO.Relatorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEMP.Areas.Outros.Controllers
{
    public class RelatorioProdutosController : Controller
    {
        // GET: Outros/RelatorioProdutos
        public ActionResult Index()
        {
            return View();
        }

		public ActionResult Lista(string startDate, int? codLinha, string codModelo)
		{
			Response.AddHeader("content-disposition", "attachment; filename=Amplitude.xls");
			Response.ContentType = "application/ms-excel";

			var datInicio = DateTime.Now.AddDays(-DateTime.Now.Day).AddDays(1);
			if (!String.IsNullOrEmpty(startDate)){
				datInicio = DateTime.Parse(startDate);
			}

			var dados = AuditorSerie.ObterSeriesNaoAmarradas(datInicio, codLinha, codModelo);

			return View(dados);
		}

		public ActionResult ListaNaoEmbalados(string startDate, int? codLinha)
		{
			Response.AddHeader("content-disposition", "attachment; filename=Amplitude.xls");
			Response.ContentType = "application/ms-excel";

			var datInicio = DateTime.Now.AddDays(-DateTime.Now.Day).AddDays(1);
			if (!String.IsNullOrEmpty(startDate))
			{
				datInicio = DateTime.Parse(startDate);
			}

			var dados = AuditorSerie.ObterSeriesNaoEmbaladas(datInicio, codLinha);

			return View(dados);
		}

		public ActionResult ListaEmbaladosIACIMC(string startDate, int? codLinha, string codModelo)
		{
			Response.AddHeader("content-disposition", "attachment; filename=Amplitude.xls");
			Response.ContentType = "application/ms-excel";

			var datInicio = DateTime.Now.AddDays(-DateTime.Now.Day).AddDays(1);
			if (!String.IsNullOrEmpty(startDate))
			{
				datInicio = DateTime.Parse(startDate);
			}

			var dados = AuditorSerie.ObterEmbaladosIACIMC(datInicio, codLinha, codModelo);

			return View(dados);
		}

	}
}