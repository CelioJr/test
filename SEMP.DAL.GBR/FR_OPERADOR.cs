namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_OPERADOR
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OPDR_CODIGO { get; set; }

        [Required]
        [StringLength(40)]
        public string OPDR_NOME { get; set; }

        [Required]
        [StringLength(1)]
        public string OPDR_TIPO { get; set; }

        [Required]
        [StringLength(80)]
        public string OPDR_PARAMETROS { get; set; }
    }
}
