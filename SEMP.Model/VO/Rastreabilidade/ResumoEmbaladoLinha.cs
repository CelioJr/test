﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade
{
	public class ResumoEmbaladoLinha
	{

		public int CodLinha { get; set; }
		public string CodLinhaERP { get; set; }
		public string DesLinha { get; set; }
		public string Setor { get; set; }
		public decimal QtdPrograma { get; set; }
		public decimal QtdProduzido { get; set; }
		public int QtdProdutos { get; set; }
		public decimal Percentual { 
			get {
				if (QtdPrograma > 0)
				{
					return Math.Round(QtdProduzido / QtdPrograma, 2) * 100;
				}
				else{
					return 0;
				}
			} 
		}

	}
}
