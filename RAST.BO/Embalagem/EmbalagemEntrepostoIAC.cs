﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RAST.BO.Rastreabilidade;
using SEMP.DAL.DAO.Embalagem;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model;
using SEMP.Model.DTO;
using SEMP.Model.VO;
using SEMP.Model.VO.Embalagem;
using System.Diagnostics;

namespace RAST.BO.Embalagem
{
	public class EmbalagemEntrepostoIAC : Embalagem, IEmbalagem
	{
		private new void DesfazerOperacoes(CBEmbaladaDTO embalagem, List<CBAmarraDTO> listaAmarra)
		{
			DaoEmbalagem.DesfazerOperacoes(embalagem, listaAmarra);
		}

		public new static ClassesAuxiliaresEmbalagem.ResultadoEmbalagemProduto ExecProcedure_EmbalarProduto(CBEmbaladaDTO embalada)
		{
			var dadosEmbalada = DaoEmbalagem.Exec_Procedure_EmbalarProduto(embalada);

			return dadosEmbalada;
		}


		public override Mensagem Embalar_Produto(CBEmbaladaDTO embalagem, List<CBAmarraDTO> listaAmarra, Passagem passagem, CBPostoDTO posto, bool gravarBancoSemp = true)
		{
           // return base.Embalar_Produto(embalagem, listaAmarra, passagem, posto, false);



			Stopwatch sw = new Stopwatch();
			string tempoExecucaoMetodo = string.Empty;
			string TextoTempo = string.Empty;
			TimeSpan ts;

			Mensagem retorno = new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "LIME", DesMensagem = "LEITURA OK. LEIA PRÓXIMO." };

			try
			{
				#region Embalar Produto
				sw.Start();

				var resultProcedure = DaoEmbalagem.Procedure_Embalar_Amarrar_Produto(embalagem, listaAmarra);
				sw.Stop();
				ts = sw.Elapsed;

				tempoExecucaoMetodo = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
														   ts.Hours, ts.Minutes, ts.Seconds,
														   ts.Milliseconds / 10);
				TextoTempo += "\n Tempo Embalagem Produto: " + tempoExecucaoMetodo;

				if (resultProcedure == null || resultProcedure.CodSucesso == 1)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
					retorno.DesMensagem = "Erro ao executar procedure embalagem";
					retorno.MensagemAuxiliar = TextoTempo;
					return retorno;
				}
				#endregion

				embalagem.NumLote = resultProcedure.numeroLote;

				#region Impressão de etiquetas
				if (resultProcedure.qtd_atual_cc >= resultProcedure.qtd_padrao_cc)
				{
					if (posto.flgImprimeEtq == "S")
					{
						sw.Restart();
						this.ImprimirCaixaColetiva(resultProcedure.numeroCaixaColetiva, embalagem.Linha, posto);
						sw.Stop();
						ts = sw.Elapsed;
						tempoExecucaoMetodo = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);

						TextoTempo += "\n Tempo Impressão: " + tempoExecucaoMetodo;
					}
				}
				#endregion


			}
			catch (Exception ex)
			{
				if (retorno.CodMensagem == Constantes.RAST_IMPRESSAO_NAO_REALIZADA)
				{
					retorno = daoMensagem.ObterMensagem(Constantes.RAST_IMPRESSAO_NAO_REALIZADA);
				}
				else
				{
					DesfazerOperacoes(embalagem, listaAmarra);
					retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				}

				retorno.DesMensagem = ex.Message;
			}

			retorno.MensagemAuxiliar = TextoTempo;

			return retorno;

		}

		public override Mensagem ValidarLeitura(int tipoAmarra, string leitura, string numAp, string numSerie, string numKitProvisorio,
			CBLinhaDTO linha)
		{
			return new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "Lime", DesMensagem = "LEITURA OK. LEIA PRÓXIMO." };
		}

		public override Mensagem ValidarProduto(Passagem passagem, string codDrt, string numAp, string leitura, string modelo, string codFab,
			string codigoCinescopio, CBLinhaDTO linha)
		{
			return passagem.ValidaPassagem(leitura, codDrt, passagem.posto.CodLinha, passagem.posto.NumPosto, numAp);

		}

		public new string AbrirCaixaColetiva(int idLinha, string codModelo, string codFab, string umAP)
		{
			return base.AbrirCaixaColetiva(idLinha, codModelo, codFab, umAP);
		}

		public new Mensagem FecharCaixaColetiva(string numLote, string codDrt, string justificativa)
		{
			return base.FecharCaixaColetiva(numLote, codDrt, justificativa);
		}

		public new Mensagem FecharLote(string numLote, string codDrt, string justificativa)
		{
			var retorno = new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "LIME", DesMensagem = "LEITURA OK. LEIA PRÓXIMO." }; // Defeito.ObterMensagem(Constantes.RAST_OK);
			var resultado = DaoEmbalagem.FecharLote(numLote, codDrt, justificativa);

			if (resultado) return retorno;

			retorno = Defeito.ObterMensagem(Constantes.RAST_LOTE_NAOFINALIZADO);

			return retorno;
		}

		public new List<CBEmbaladaDTO> ProdutosDeLoteEmAberto(string numAp, string codModelo, int codLinha, string codFab)
		{
			return base.ProdutosDeLoteEmAberto(numAp, codModelo, codLinha, codFab);
		}

		public new List<CBEmbaladaDTO> RecuperarItensEmbaladosDaCaixaColetivaAberta(string numAp, string codModelo, int codLinha, string codFab)
		{
			return base.RecuperarItensEmbaladosDaCaixaColetivaAberta(numAp, codModelo, codLinha, codFab);
		}

		public override Mensagem ImprimirEtiquetasIndividuais(string numAp, string leitura, string codModelo, CBPostoDTO posto, double PesoCaixa)
		{
			return base.ImprimirEtiquetasIndividuais(numAp, leitura, codModelo, posto, PesoCaixa);
		}

		public override Mensagem ImprimirCaixaColetiva(string magazine, CBLinhaDTO linha, CBPostoDTO posto)
		{
			var retorno = new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "LIME", DesMensagem = "LEITURA OK. LEIA PRÓXIMO." };
		 try
            {
			

			int qtdeImprimir = new Embalagem().QuantidadeItensEmbaladosCaixaColetiva(magazine);
	         var magazineObj = Embalagem.RecuperarDadosCC(linha.CodLinha);
			var lote = Embalagem.RecuperarDadosLote(magazine);
			string data1 = (lote.DatFechamento == null ? DateTime.Now.ToString() : lote.DatFechamento.ToString());
			DateTime dataImprimir = Convert.ToDateTime(data1);
			string modelo = lote.CodModelo;
			int qtdLotePadrao = Geral.ObterQuantidadeLotePorLinha(linha.CodLinha);

			var EtiquetasPorPosto = Embalagem.RecuperarEtiquetasPorPosto(posto.CodLinha, posto.NumPosto, true); //.FirstOrDefault(x => x.NomeEtiqueta.Equals("EtiquetaLoteIMC_IAC"));

			string descModelo = Geral.ObterDescricaoItem(modelo);

			var dados = new Hashtable
                    {
                        {"setor", linha.Setor},
                        {"desOrigem", linha.DesLinha},
                        {"desModelo", descModelo},
                        {"modelo", modelo},
                        {"codFab", lote.CodFab},
                        {"numMagazine", magazine},
						{"numLote", lote.NumLotePai},
                        {"qtdMagazine", qtdeImprimir},
						{"tamMagazine", magazineObj.QtdeCC},
                        {"tamLote", qtdLotePadrao},
                        {"data", dataImprimir.ToString("dd/MM/yyyy")},
                        {"hora", dataImprimir.ToShortTimeString()}

                    };
			foreach (var etiqueta in EtiquetasPorPosto)
			{
				GerenciarImpressao.ImprimirEtiqueta(dados, etiqueta);
			}


			}
		 catch (Exception ex)
		 {
			 retorno = daoMensagem.ObterMensagem(Constantes.RAST_IMPRESSAO_NAO_REALIZADA);
			 retorno.DesMensagem = ex.Message;
		 }

		 return retorno;
		}

		public new int QtdeCaixasColetivasNoLote(string numLote)
		{
			return base.QtdeCaixasColetivasNoLote(numLote);
		}

		public new bool AlterarTamanhoCaixaColetiva(int codLinha, int tamanho)
		{
			return base.AlterarTamanhoCaixaColetiva(codLinha, tamanho);
		}


		public new bool AlterarTamanhoLote(int codLinha, int tamanho)
		{
			return base.AlterarTamanhoLote(codLinha, tamanho);
		}

		public new string RecuperarLoteAberto(string numAp, string codModelo, int codLinha, string codFab)
		{
			return base.RecuperarLoteAberto(numAp, codModelo, codLinha, codFab);
		}
		public new string RecuperarLoteAberto(string codModelo, int codLinha, string codFab)
		{

			return DaoEmbalagem.RecuperarNumeroLoteAberto(codModelo, codLinha, codFab);
		}

		public new virtual string RecuperarNumeroCaixaColetivaAberta(string numAp, int codLinha, string codFab)
		{
			return base.RecuperarNumeroCaixaColetivaAberta(numAp, codLinha, codFab);
		}
		public new virtual string RecuperarNumeroCaixaColetivaAberta(string numAp, string modelo,int codLinha, string codFab)
		{
			return base.RecuperarNumeroCaixaColetivaAberta(numAp, modelo, codLinha, codFab);
		}
		public new string ObterTotalProduzidoCaixaColetiva(string codFabrica, string numAp, int codLinha)
		{
			string numCC = RecuperarNumeroCaixaColetivaAberta(numAp, codLinha, codFabrica);
			int qtdeEmb = 0;

			if (!string.IsNullOrEmpty(numCC))
				qtdeEmb = DaoEmbalagem.QuantidadeItensEmbaladosCaixaColetiva(numCC);

			//string qtdePadrao = Geral.ObterQuantidadeCaixaColetivaPorLinha(codLinha).ToString();

			//return (qtdeEmb.ToString() + "/" + qtdePadrao);
			return qtdeEmb.ToString();
			//return base.ObterTotalProduzidoCaixaColetiva(codFabrica, numAp, codLinha);
		}

		public new string ObterTotalProduzidoCaixaColetiva(string codFabrica, string numAp, int codLinha, string codModelo)
		{
			string numCC = DaoEmbalagem.RecuperarNumeroCaixaColetivaAberta(numAp, codLinha, codFabrica, codModelo);
			int qtdeEmb = 0;

			if (!string.IsNullOrEmpty(numCC))
				qtdeEmb = DaoEmbalagem.QuantidadeItensEmbaladosCaixaColetiva(numCC);

			//string qtdePadrao = Geral.ObterQuantidadeCaixaColetivaPorLinha(codLinha).ToString();

			//return (qtdeEmb.ToString() + "/" + qtdePadrao);
			return qtdeEmb.ToString();
		}
	}
}
