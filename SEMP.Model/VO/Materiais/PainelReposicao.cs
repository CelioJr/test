﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Materiais
{
	public class PainelReposicao
	{

		public int NumRomaneio { get; set; }
		public string DatGeracao { get; set; }
		public string Status { get; set; }
		public string StatusAprov { get; set; }
		public int QtdItens { get; set; }
		public int QtdPagos { get; set; }
		public int QtdDias { get; set; }
		public int QtdCritico { get; set; }
	}
}
