﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade
{
	public class QueimaRelatorioInspecao
	{
		public string NumECB { get; set; }
		public int CodLinha { get; set; }
		public int NumPosto { get; set; }
		public string CodFab { get; set; }
		public string NumRPA { get; set; }
		public string CodPallet { get; set; }
		public Nullable<System.DateTime> DatLeitura { get; set; }
        public DateTime? DatEntrada { get; set; }
        public Nullable<System.DateTime> DatSaida { get; set; }
        public string FlgStatus { get; set; }
		public string Observacao { get; set; }

		public string CodModelo { get; set; }
		public string DesModelo { get; set; }

		public string CodOperador { get; set; }
		public string NomOperador { get; set; }

		public string NumRPE { get; set; }
		public DateTime DatProducao { get; set; }

		public string CodLinhaOri { get; set; }
		public string DesLinhaOri { get; set; }
		
		public string DesLinha { get; set; }
		public string DesPosto { get; set; }

	}
}
