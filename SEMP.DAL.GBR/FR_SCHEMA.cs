namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_SCHEMA
    {
        [Key]
        [StringLength(200)]
        public string SCH_NOME { get; set; }

        public int SCH_VERSAO { get; set; }
    }
}
