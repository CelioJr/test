using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_HistoricoRetPosto
    {
        public int Posto_id { get; set; }
        public System.DateTime DatAtivacao { get; set; }
        public int CodOpeAtivacao { get; set; }
        public DateTime? DatDesativacao { get; set; }
        public int? CodOpeDesativacao { get; set; }
        public bool? flgPesagemKitAces { get; set; }        
    }
}
