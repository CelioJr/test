namespace SEMP.Model.DTO
{
    public class LinhaTurnoDTO
    {
        public string CodLinhaOri { get; set; }
        public string CodLinhaDest { get; set; }
        public int Turno { get; set; }
    }
}
