using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_ROLE_PROF_DTO
    {
        public decimal id { get; set; }
        public int role { get; set; }
        public Nullable<decimal> profile_id { get; set; }
        public virtual ALC_PROFILE_DTO ALC_PROFILE { get; set; }
    }
}
