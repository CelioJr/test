using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_CAD_IMPRESSORA_DTO
    {
        public int COD_IMPRESSORA { get; set; }
        public string IMPRESSORA { get; set; }
        public Nullable<bool> ATIVO { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
        public string CAMINHO { get; set; }
    }
}
