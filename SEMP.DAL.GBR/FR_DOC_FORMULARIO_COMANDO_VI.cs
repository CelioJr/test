namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_DOC_FORMULARIO_COMANDO_VI
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int com_ordem { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(17)]
        public string com_botao { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(66)]
        public string com_acao { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(62)]
        public string com_restricao { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int com_ativo { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int frm_codigo { get; set; }

        [Key]
        [Column(Order = 6)]
        [StringLength(3)]
        public string sis_codigo { get; set; }
    }
}
