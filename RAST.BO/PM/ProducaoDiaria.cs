﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.Mobilidade;
using SEMP.DAL.DAO.PM;
using SEMP.Model.DTO;
using SEMP.Model.VO.PM;

namespace RAST.BO.PM
{
	public class ProducaoDiaria
	{
		public static List<ProgramaDiario> ObterProgramaDiario(DateTime data)
		{

			var dados = daoProducaoDiaria.ObterProdutos(data);

			var programaDia = daoProducaoDiaria.ObterProgramaDia(data);
			var producaoDia = daoProducaoDiaria.ObterApontadoDia(data);
			var producaoAcum = daoProducaoDiaria.ObterProducaoAcumulada(data);
			//var progVPE = daoProducaoDiaria.ObterProgVPE(data).GroupBy(x => new { x.CodFab, x.CodModelo, x.DatProducao }).Select(x => new { x.Key.CodFab, x.Key.CodModelo, x.Key.DatProducao, QtdProdPm = x.Sum(y => y.QtdProdPm) }).ToList();
			var defeitos = daoProducaoDiaria.ObterDefeitosDia(data, "FEC");
			var observacoes = daoProducaoDiaria.ObterObservacoesDia(data);

			foreach (var dado in dados)
			{

				var progDia = programaDia.Where(x => x.CodModelo == dado.CodModelo ).Sum(x => x.QtdProdPm);
				var prodDia = producaoDia.Where(x => x.CodModelo == dado.CodModelo).Sum(x => x.QtdProdDia);
				var prodMes = producaoAcum.FirstOrDefault(x => x.CodModelo == dado.CodModelo);
				var defDia = defeitos.FirstOrDefault(x => x.CodModelo == dado.CodModelo);
				var obs = observacoes.FirstOrDefault(x => x.CodModelo == dado.CodModelo);

				dado.QtdProgDia = progDia ?? 0;
				dado.QtdProdDia = prodDia ?? 0;
				dado.QtdAcumulada = prodMes != null ? prodMes.QtdAcumulada : 0;
				dado.QtdDefeitos = defDia != null ? defDia.QtdDefeitos : 0;
				dado.Observacao = obs != null ? obs.Observacao : "";

			}

			return dados.OrderBy(x => x.Ordem).ThenBy(x => x.DesModelo).ToList();
		}

		public static List<ProgramaDiario> ObterProgramaDiarioPlaca(DateTime data, string fase)
		{

			var dados = daoProducaoDiaria.ObterProdutosPlaca(data, fase);

			var programaDia = daoProducaoDiaria.ObterProgramaDiaPlaca(data, fase);
			//var producaoDia = daoProducaoDiaria.ObterApontadoDiaPlaca(data, fase);
			var producaoDia = daoProducaoDiaria.ObterProducaoDiaPlaca(data, fase);
			var producaoAcum = daoProducaoDiaria.ObterProducaoAcumuladaPlaca(data, fase);

			if (fase == "M") {
				fase = "IMC";
			}
			else if (fase == "I") {
				fase = "IAC";
			}
			else
			{
				fase = "FEC";
			}

			var defeitos = daoProducaoDiaria.ObterDefeitosDia(data, fase);
			var observacoes = daoProducaoDiaria.ObterObservacoesDia(data);

			//dados.AddRange(producaoDia.Where(x => !dados.Select(y => y.CodModelo).ToList().Contains(x.CodModelo)));

			foreach (var dado in dados)
			{

				var progDia = programaDia.FirstOrDefault(x => x.CodModelo == dado.CodModelo);
				var prodDia = producaoDia.FirstOrDefault(x => x.CodModelo == dado.CodModelo);
				var prodMes = producaoAcum.FirstOrDefault(x => x.CodModelo == dado.CodModelo);
				var defDia = defeitos.FirstOrDefault(x => x.CodModelo == dado.CodModelo);
				var obs = observacoes.FirstOrDefault(x => x.CodModelo == dado.CodModelo);

				dado.QtdProgDia = progDia != null ? progDia.QtdProdPM : 0;
				dado.QtdProdDia = prodDia != null ? prodDia.QtdProdDia : 0;
				dado.QtdAcumulada = prodMes != null ? prodMes.QtdAcumulada : 0;
				dado.QtdDefeitos = defDia != null ? defDia.QtdDefeitos : 0;
				dado.Observacao = obs != null ? obs.Observacao : "";

			}

			return dados.OrderBy(x => x.NomFam).ThenBy(x => x.DesModelo).ToList();
		}

		public static List<ProgramaDiario> ObterProgramaDiarioDAT(DateTime data)
		{

			var dados = daoProducaoDiaria.ObterDadosDAT(data);

			var programaDia = daoProducaoDiaria.ObterProgramaDiaDAT(data);
			var producaoDia = daoProducaoDiaria.ObterProduzidoDAT(data);
			var producaoAcum = daoProducaoDiaria.ObterProdAcumuladaDAT(data);
			var defeitos = daoProducaoDiaria.ObterDefeitosDiaDAT(data);

			//var prodDiaPlaca = daoProducaoDiaria.ObterProdPlacaDiaDAT(data);
			var prodMesPlaca = daoProducaoDiaria.ObterProdPlacaMesDAT(data);
			//var progDiaPlaca = daoProducaoDiaria.ObterProgPlacaDiaDAT(data);
			var progMesPlaca = daoProducaoDiaria.ObterProgPlacaMesDAT(data);

			dados.AddRange(producaoAcum.Where(x => !dados.Select(y => y.CodModelo).Contains(x.CodModelo)));
			dados.AddRange(prodMesPlaca.Where(x => !dados.Select(y => y.CodModelo).Contains(x.CodModelo)));
			dados.AddRange(progMesPlaca.Where(x => !dados.Select(y => y.CodModelo).Contains(x.CodModelo)));

			foreach (var dado in dados)
			{
				dado.QtdProdDia = dado.QtdProdDia ?? 0;
				dado.QtdProgDia = dado.QtdProgDia ?? 0;

				var progDia = programaDia.FirstOrDefault(x => x.CodModelo == dado.CodModelo);
				var prodDia = producaoDia.FirstOrDefault(x => x.CodModelo == dado.CodModelo);
				var prodMes = producaoAcum.FirstOrDefault(x => x.CodModelo == dado.CodModelo);
				var defDia = defeitos.FirstOrDefault(x => x.CodModelo == dado.CodModelo);

				dado.QtdProgDia += progDia != null ? (progDia.QtdProgDia ?? 0) : (dado.QtdProgDia ?? 0);
				dado.QtdProdDia += prodDia != null ? prodDia.QtdProdDia : (dado.QtdProdDia ?? 0);
				dado.QtdAcumulada = prodMes != null ? prodMes.QtdAcumulada : (dado.QtdAcumulada ?? 0);
				dado.QtdDefeitos = defDia != null ? defDia.QtdDefeitos : (dado.QtdDefeitos ?? 0);

				/*progDia = progDiaPlaca.FirstOrDefault(x => x.CodModelo == dado.CodModelo);
				var progMes = progMesPlaca.FirstOrDefault(x => x.CodModelo == dado.CodModelo);
				prodDia = prodDiaPlaca.FirstOrDefault(x => x.CodModelo == dado.CodModelo);
				prodMes = prodMesPlaca.FirstOrDefault(x => x.CodModelo == dado.CodModelo);

				dado.QtdProgDia = progDia != null ? (progDia.QtdProgDia ?? 0 ): dado.QtdProgDia;
				dado.QtdProdDia = prodDia != null ? prodDia.QtdProdDia : dado.QtdProdDia;
				dado.QtdAcumulada = prodMes != null ? prodMes.QtdAcumulada : dado.QtdAcumulada;
				dado.QtdPP = progMes != null ? progMes.QtdPP : dado.QtdPP;*/

			}
			
			return dados.OrderBy(x => x.NomFam).ThenBy(x => x.DesModelo).ToList();
		}

		public static List<ProgramaDiario> ObterProgramaDiarioDATTeste(DateTime data)
		{

			var dados = daoProducaoDiaria.ObterDadosDATTeste(data);

			var programaDia = daoProducaoDiaria.ObterProgramaDiaDATTeste(data);
			var producaoDia = daoProducaoDiaria.ObterProduzidoDATTeste(data);
			var producaoAcum = daoProducaoDiaria.ObterProdAcumuladaDATTeste(data);
			var defeitos = daoProducaoDiaria.ObterDefeitosDiaDATTeste(data);

			var prodMesPlaca = daoProducaoDiaria.ObterProdPlacaMesDATTeste(data);
			var progMesPlaca = daoProducaoDiaria.ObterProgPlacaMesDATTeste(data);

			dados.AddRange(producaoAcum.Where(x => !dados.Select(y => y.CodModelo).Contains(x.CodModelo)));
			dados.AddRange(prodMesPlaca.Where(x => !dados.Select(y => y.CodModelo).Contains(x.CodModelo)));
			dados.AddRange(progMesPlaca.Where(x => !dados.Select(y => y.CodModelo).Contains(x.CodModelo)));

			foreach (var dado in dados)
			{
				dado.QtdProdDia = dado.QtdProdDia ?? 0;
				dado.QtdProgDia = dado.QtdProgDia ?? 0;

				var progDia = programaDia.FirstOrDefault(x => x.CodModelo == dado.CodModelo);
				var prodDia = producaoDia.FirstOrDefault(x => x.CodModelo == dado.CodModelo);
				var prodMes = producaoAcum.FirstOrDefault(x => x.CodModelo == dado.CodModelo);
				var defDia = defeitos.FirstOrDefault(x => x.CodModelo == dado.CodModelo);

				dado.QtdProgDia += progDia != null ? (progDia.QtdProgDia ?? 0) : (dado.QtdProgDia ?? 0);
				dado.QtdProdDia += prodDia != null ? prodDia.QtdProdDia : (dado.QtdProdDia ?? 0);
				dado.QtdAcumulada = prodMes != null ? prodMes.QtdAcumulada : (dado.QtdAcumulada ?? 0);
				dado.QtdDefeitos = defDia != null ? defDia.QtdDefeitos : (dado.QtdDefeitos ?? 0);

			}

			return dados.OrderBy(x => x.NomFam).ThenBy(x => x.DesModelo).ToList();
		}


		public static List<ProgramaDiario> ObterProducaoDia(DateTime data)
		{
			return daoProducaoDiaria.ObterProducaoDia(data);
		}

		public static List<ProgramaDiario> ObterProducaoDiaPlaca(DateTime data, string fase)
		{
			return daoProducaoDiaria.ObterProducaoDiaPlaca(data, fase);
		}

		public static List<ProgDiaJitDTO> ObterProgramaDia(DateTime data)
		{
			return daoProducaoDiaria.ObterProgramaDia(data);
		}

		public static List<ProgDiaJitDTO> ObterProgramaDia(DateTime datInicio, DateTime datFim)
		{
			return daoProducaoDiaria.ObterProgramaDia(datInicio, datFim);
		}

		public static List<ProgDiaPlacaDTO> ObterProgramaDiaPlaca(DateTime data, string fase)
		{
			return daoProducaoDiaria.ObterProgramaDiaPlaca(data, fase);
		}

		#region Mobilidade
		public static List<ProgramaDiario> ObterProgramaDiarioMobilidadePA(DateTime data)
		{

			var dados = daoAlcatel.ObterProdutos(data);

			var programaDia = daoAlcatel.ObterProgramaDia(data);
			var producaoDia = daoAlcatel.ObterProducaoDia(data);
			var producaoAcum = daoAlcatel.ObterProducaoAcumulada(data);
			var defeitos = daoAlcatel.ObterDefeitosDia(data, "PA");

			foreach (var dado in dados)
			{

				var progDia = programaDia.Where(x => x.CodModeloMob == dado.CodModeloMob && x.CodModelo == dado.CodModelo).Sum(x => x.QtdPP);
				var prodDia = producaoDia.Where(x => x.CodModeloMob == dado.CodModeloMob && x.CodModelo == dado.CodModelo).Sum(x => x.QtdProdDia);
				var prodMes = producaoAcum.FirstOrDefault(x => x.CodModeloMob == dado.CodModeloMob && x.CodModelo == dado.CodModelo);
				var defDia = defeitos.FirstOrDefault(x => x.CodModeloMob == dado.CodModeloMob && x.CodModelo == dado.CodModelo);

				dado.QtdProgDia = progDia ?? 0;
				dado.QtdProdDia = prodDia ?? 0;
				dado.QtdAcumulada = prodMes != null ? prodMes.QtdAcumulada : 0;
				dado.QtdDefeitos = defDia != null ? defDia.QtdDefeitos : 0;

			}

			return dados.OrderBy(x => x.Ordem).ThenBy(x => x.DesModelo).ToList();
		}

		#endregion
	}
}
