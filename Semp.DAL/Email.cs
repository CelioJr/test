﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using SEMP.DAL.Models;

namespace SEMP.DAL
{
    public class Email
    {
        public Email ()
        {
        }

        public static string SU_EnviaEmail (string msgError, string comando)
        {
            String sSQL = "EXEC msdb.dbo.sp_send_dbmail " +
                            "@profile_name = 'Fucapi - Sisap', " +
//#if DEBUG								
//                                "@recipients = 'flavio.paula@fucapi.br', " +
//#else
 "@recipients = 'fucapi.sisap@semptoshiba.com.br', " +
 //"@recipients = 'flavio.paula@fucapi.br', " +
//#endif
 "@body = 'Mensagem: " + msgError + " ao tentar executar: " + comando + " ', " +
                            "@subject = 'Error de Banco de Dados' ;";

            string sSaida = "";


            string conexao = (new DTB_CBContext()).Database.Connection.ConnectionString;
            SqlConnection con = new SqlConnection(conexao);

            con.Open();
            try
            {
                DataSet oDs = new DataSet();
                SqlCommand oCom = new SqlCommand(sSQL, con);

                using (SqlDataAdapter oDa = new SqlDataAdapter())
                {
                    oDa.SelectCommand = oCom;
                    oDa.Fill(oDs);
                }
            }
            catch (SqlException sqlex)
            {
                sSaida = sqlex.Message;
            }
            catch (DBConcurrencyException sqlex1)
            {
                sSaida = sqlex1.Message;
            }
            catch (Exception ex1)
            {
                sSaida = ex1.Message;
            }
                
            finally
            {
                con.Close();
            }

            
                          
            return sSaida;
        }
    }
}
