namespace SEMP.Model.DTO
{
	using System.ComponentModel.DataAnnotations;

	public partial class tbl_acao_DTO
	{

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string ACAO { get; set; }

    }
}
