using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_gpaboletos
    {
        public string CodFab { get; set; }
        public string numgrim { get; set; }
        public string CodItem { get; set; }
        public string CodLocal { get; set; }
        public string CodGalpao { get; set; }
        public string CodRua { get; set; }
        public string CodBloco { get; set; }
        public string CodApto { get; set; }
        public string SubApto { get; set; }
        public string CodNivel { get; set; }
        public string SubNivel { get; set; }
        public decimal QtdEnderecar { get; set; }
        public string numcontrole { get; set; }
        public System.DateTime DatEmissao { get; set; }
        public string usuario { get; set; }
        public string tipendent { get; set; }
        public Nullable<decimal> QtdReserva { get; set; }
    }
}
