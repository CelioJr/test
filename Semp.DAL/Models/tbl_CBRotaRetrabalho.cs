﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.Models
{
	public partial class tbl_CBRotaRetrabalho
	{
		public int CodRetrabalho { get; set; }
		public int CodLinha { get; set; }
		public int NumPosto { get; set; }
		public string FlgUltimoPosto { get; set; }
		public virtual tbl_CBRetrabalho tbl_CBRetrabalho { get; set; }
	}
}
