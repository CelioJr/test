using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_RELATORIO_DTO
    {
        public FR_RELATORIO_DTO()
        {
            this.FR_DOC_RELATORIO = new List<FR_DOC_RELATORIO_DTO>();
            this.FR_FORMULARIO = new List<FR_FORMULARIO_DTO>();
            this.FR_PERMISSAO = new List<FR_PERMISSAO_DTO>();
        }

        public int REL_CODIGO { get; set; }
        public string SIS_CODIGO { get; set; }
        public string REL_NOME { get; set; }
        public string REL_CONTEUDO { get; set; }
        public Nullable<System.DateTime> REL_MODIFICADO { get; set; }
        public Nullable<int> REL_TAMANHO { get; set; }
        public int USR_CODIGO { get; set; }
        public virtual ICollection<FR_DOC_RELATORIO_DTO> FR_DOC_RELATORIO { get; set; }
        public virtual ICollection<FR_FORMULARIO_DTO> FR_FORMULARIO { get; set; }
        public virtual ICollection<FR_PERMISSAO_DTO> FR_PERMISSAO { get; set; }
    }
}
