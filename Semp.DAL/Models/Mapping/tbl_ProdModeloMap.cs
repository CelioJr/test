using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_ProdModeloMap : EntityTypeConfiguration<tbl_ProdModelo>
    {
        public tbl_ProdModeloMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodModelo, t.LotePadrao });

            // Properties
            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.DesResumida)
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.Projeto)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NumMacAddress)
                .HasMaxLength(16)
                .IsUnicode(false);

            this.Property(t => t.CodAcessorio)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodEAN)
                .IsFixedLength()
                .HasMaxLength(4)
                .IsUnicode(false);

            this.Property(t => t.CodEAN13a)
                .HasMaxLength(13)
                .IsUnicode(false);

            this.Property(t => t.CodEAN13b)
                .HasMaxLength(13)
                .IsUnicode(false);

            this.Property(t => t.CodEAN128)
                .HasMaxLength(16)
                .IsUnicode(false);

            this.Property(t => t.PotModelo)
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.RefCinescopio)
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.DesCP1)
                .HasMaxLength(15)
                .IsUnicode(false);

            this.Property(t => t.DesCor)
                .HasMaxLength(30)
                .IsUnicode(false);

            this.Property(t => t.flgAtivo)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.NumIMEI)
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.NumAnatel)
                .HasMaxLength(15)
                .IsUnicode(false);

            this.Property(t => t.LotePadrao)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.VersaoSoft)
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.FlgLeitura)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.Mascara)
                .HasMaxLength(50)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_ProdModelo");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.DesResumida).HasColumnName("DesResumida");
            this.Property(t => t.Projeto).HasColumnName("Projeto");
            this.Property(t => t.NumProjeto).HasColumnName("NumProjeto");
            this.Property(t => t.NumMacAddress).HasColumnName("NumMacAddress");
            this.Property(t => t.CodAcessorio).HasColumnName("CodAcessorio");
            this.Property(t => t.CodEAN).HasColumnName("CodEAN");
            this.Property(t => t.CodEAN13a).HasColumnName("CodEAN13a");
            this.Property(t => t.CodEAN13b).HasColumnName("CodEAN13b");
            this.Property(t => t.CodEAN128).HasColumnName("CodEAN128");
            this.Property(t => t.ImpModelo).HasColumnName("ImpModelo");
            this.Property(t => t.PotModelo).HasColumnName("PotModelo");
            this.Property(t => t.RefCinescopio).HasColumnName("RefCinescopio");
            this.Property(t => t.DesCP1).HasColumnName("DesCP1");
            this.Property(t => t.DesCor).HasColumnName("DesCor");
            this.Property(t => t.EmbModelo).HasColumnName("EmbModelo");
            this.Property(t => t.flgAtivo).HasColumnName("flgAtivo");
            this.Property(t => t.NumIMEI).HasColumnName("NumIMEI");
            this.Property(t => t.NumAnatel).HasColumnName("NumAnatel");
            this.Property(t => t.LotePadrao).HasColumnName("LotePadrao");
            this.Property(t => t.NumIMEIDOA).HasColumnName("NumIMEIDOA");
            this.Property(t => t.VersaoSoft).HasColumnName("VersaoSoft");
            this.Property(t => t.FlgLeitura).HasColumnName("FlgLeitura");
            this.Property(t => t.Mascara).HasColumnName("Mascara");
        }
    }
}
