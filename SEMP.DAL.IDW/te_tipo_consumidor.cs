namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class te_tipo_consumidor
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public te_tipo_consumidor()
        {
            te_tarifas = new HashSet<te_tarifas>();
        }

        [Key]
        public long id_tipo_consumidor { get; set; }

        [StringLength(60)]
        public string cd_tipo_consumidor { get; set; }

        public int? revisao { get; set; }

        [StringLength(256)]
        public string ds_tipo_consumidor { get; set; }

        public short? st_ativo { get; set; }

        public DateTime? dt_stativo { get; set; }

        public DateTime? dt_revisao { get; set; }

        public long? id_usrstativo { get; set; }

        public long? id_usrrevisao { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<te_tarifas> te_tarifas { get; set; }
    }
}
