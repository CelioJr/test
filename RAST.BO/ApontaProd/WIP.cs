﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.DTO;

namespace RAST.BO.ApontaProd
{
	public class WIP
	{
		public static List<WIPProducaoDTO> ObterDadosWIP() {
			
			return daoWIP.ObterDadosWIP();
		}

		public static List<WIPProducaoDTO> ObterDadosWIPItem(string local, string familia) {
			
			return daoWIP.ObterDadosWIPItem(local, familia);
		
		}

		public static void GerarDadosWIP(){

			daoWIP.GerarDadosWIP();

		}

		public static List<string> ObterFamiliasWIP() {

			return daoWIP.ObterFamiliasWIP();

		}
	}
}
