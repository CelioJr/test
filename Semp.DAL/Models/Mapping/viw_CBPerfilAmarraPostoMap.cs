using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class viw_CBPerfilAmarraPostoMap : EntityTypeConfiguration<viw_CBPerfilAmarraPosto>
    {
        public viw_CBPerfilAmarraPostoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodLinha, t.NumPosto, t.CodTipoAmarra });

            // Properties
            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumPosto)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodTipoAmarra)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DesTipoAmarra)
                .HasMaxLength(15)
                .IsUnicode(false);

            this.Property(t => t.CodFam)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("viw_CBPerfilAmarraPosto");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.NumPosto).HasColumnName("NumPosto");
            this.Property(t => t.CodTipoAmarra).HasColumnName("CodTipoAmarra");
            this.Property(t => t.DesTipoAmarra).HasColumnName("DesTipoAmarra");
            this.Property(t => t.CodFam).HasColumnName("CodFam");
        }
    }
}
