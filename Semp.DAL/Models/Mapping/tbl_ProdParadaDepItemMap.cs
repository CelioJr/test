using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_ProdParadaDepItemMap : EntityTypeConfiguration<tbl_ProdParadaDepItem>
    {
        public tbl_ProdParadaDepItemMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.NumParada, t.CodDepto, t.rowguid, t.CodFabDepto });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NumParada)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(9)
                .IsUnicode(false);

            this.Property(t => t.CodDepto)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.DesCausa)
                .HasMaxLength(2000)
                .IsUnicode(false);

            this.Property(t => t.DesAcao)
                .HasMaxLength(2000)
                .IsUnicode(false);

            this.Property(t => t.NomResponsavel)
                .IsFixedLength()
                .HasMaxLength(30)
                .IsUnicode(false);

            this.Property(t => t.FlgStatus)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.DesObs)
                .HasMaxLength(2000)
                .IsUnicode(false);

            this.Property(t => t.CodFabDepto)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_ProdParadaDepItem");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.NumParada).HasColumnName("NumParada");
            this.Property(t => t.CodDepto).HasColumnName("CodDepto");
            this.Property(t => t.DatRetorno).HasColumnName("DatRetorno");
            this.Property(t => t.DesCausa).HasColumnName("DesCausa");
            this.Property(t => t.DesAcao).HasColumnName("DesAcao");
            this.Property(t => t.NomResponsavel).HasColumnName("NomResponsavel");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
            this.Property(t => t.DatEnvio).HasColumnName("DatEnvio");
            this.Property(t => t.FlgStatus).HasColumnName("FlgStatus");
            this.Property(t => t.DesObs).HasColumnName("DesObs");
            this.Property(t => t.CodFabDepto).HasColumnName("CodFabDepto");
        }
    }
}
