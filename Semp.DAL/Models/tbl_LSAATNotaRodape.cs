using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_LSAATNotaRodape
    {
        public string CodFab { get; set; }
        public string NumAT { get; set; }
        public string Sequencia { get; set; }
        public System.DateTime DatPedido { get; set; }
        public Nullable<decimal> QtdVolume { get; set; }
        public Nullable<decimal> PesoLiquido { get; set; }
        public Nullable<decimal> PesoBruto { get; set; }
        public string Especificacao { get; set; }
        public string NumNota { get; set; }
        public string CodEspecie { get; set; }
        public string TipNota { get; set; }
        public string CodSerie { get; set; }
    }
}
