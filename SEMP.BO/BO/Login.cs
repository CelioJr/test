﻿using System;
using SEMP.DAL.Common;
using SEMP.Model.VO;
using SEMP.Model.DTO;
using SEMP.Model;

namespace SEMP.BO
{
	public class Login
	{

		/// <summary>
		/// Rotina utilizada quando o usuário entrar no sistema lendo o crachá
		/// </summary>
		/// <param name="userName">Crachá do funcionário</param>
		/// <returns>Dados do funcionário e código de erro</returns>
		public static RetornoLogin ValidaCracha(string userName)
		{

			RetornoLogin retorno = new RetornoLogin();

			try
			{

				RIFuncionarioDTO funcionario = daoLogin.ObterFuncionarioByCPF(userName);

				if (funcionario == null || String.IsNullOrEmpty(funcionario.CodDRT))
				{
					retorno.Status.CodMensagem = Constantes.DB_ERRO;
					return retorno;
				}

				retorno.CodDRT = funcionario.CodDRT;
				retorno.Cargo = funcionario.CodCargo.Trim();
				retorno.NomFuncionario = funcionario.NomFuncionario.Trim();
				retorno.Cracha = userName;
				retorno.Depto = funcionario.CodDepto.Trim();
				retorno.NomUsuario = string.IsNullOrEmpty(funcionario.NomLogin) ? userName : funcionario.NomLogin;
				retorno.UsuarioCracha = true;
				retorno.Status.CodMensagem = Constantes.DB_OK;

			}
			catch (Exception ex)
			{
				retorno.Status.CodMensagem = Constantes.DB_ERRO;
				retorno.Status.StackErro = ex.InnerException.Message;

			}

			return retorno;

		}

		/// <summary>
		/// Rotina utilizada quando o usuário entrar no sistema com login e senha
		/// </summary>
		/// <param name="userName"></param>
		/// <returns></returns>
		public static RetornoLogin ValidaLogin(string userName)
		{

			RetornoLogin retorno = new RetornoLogin();

			try
			{

				var funcionario = daoLogin.ObterFuncionarioByLogin(userName);

				if (funcionario != null)
				{
					if (funcionario.FlgAtivo != "S")
					{
						retorno.Status.CodMensagem = Constantes.LOGIN_FUNC_DEMITIDO;
					}
					else
					{
						var usuario = daoLogin.ObterUsuarioByLogin(userName);

						retorno.CodDRT = funcionario.CodDRT;
						retorno.NomFuncionario = funcionario.NomFuncionario.Trim();
						retorno.NomUsuario = userName;
						retorno.Cargo = funcionario.CodCargo.Trim();
						retorno.Depto = usuario.NomDepto; //funcionario.CodDepto.Trim();
						retorno.UsuarioCracha = false;
						retorno.Status.CodMensagem = Constantes.DB_OK;
					}
				}
				else
				{
					var usuario = daoLogin.ObterUsuarioByLogin(userName);

					if (usuario != null)
					{
						retorno.NomUsuario = userName;
						retorno.Depto = usuario.NomDepto.Trim();
						retorno.NomFuncionario = usuario.FullName.Trim();
						retorno.UsuarioCracha = false;
						retorno.Cargo = "";
						retorno.Status.CodMensagem = Constantes.DB_OK;
					}
					else
					{
						retorno.NomUsuario = userName;
						retorno.NomFuncionario = userName;
						retorno.UsuarioCracha = false;
						retorno.Depto = "";
						retorno.Cargo = "";
						retorno.Status.CodMensagem = Constantes.DB_ERRO;
					}
				}

			}
			catch (Exception ex)
			{
				retorno.Status.CodMensagem = Constantes.DB_ERRO;
				retorno.Status.StackErro = ex.InnerException.Message;

			}

			return retorno;

		}

		public static String ObterFuncionario(string userName)
		{
			try
			{
				return daoLogin.ObterFuncionarioByCPF(userName).NomFuncionario;
			}
			catch
			{
				return null;
			}
		}

		public static RIFuncionarioDTO ObterFuncionarioByDRT(string CodDRT)
		{
			try
			{
				return daoLogin.ObterFuncionarioByDRT(CodDRT);
			}
			catch
			{
				return null;
			}
		}

		public static RIFuncionarioDTO ObterFuncionarioByCPF(string userName)
		{
			return daoLogin.ObterFuncionarioByCPF(userName);
		}


		public static UsuarioMobileDTO ObterFuncionarioDRT(string drt)

		{
			return daoLogin.ObterFuncionarioDRT(drt);
		}

		public static RIFuncionarioDTO ObterFuncionarioByLogin(string userName)
		{

			return daoLogin.ObterFuncionarioByLogin(userName);

		}

	}
}
