using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_REPAIR_REASON_DTO
    {
        public ALC_REPAIR_REASON_DTO()
        {
            this.ALC_REPAIR = new List<ALC_REPAIR_DTO>();
            this.ALC_RMB_REGISTER = new List<ALC_RMB_REGISTER_DTO>();
        }

        public string ID_REASON { get; set; }
        public string DESCRIPTION { get; set; }
        public virtual ICollection<ALC_REPAIR_DTO> ALC_REPAIR { get; set; }
        public virtual ICollection<ALC_RMB_REGISTER_DTO> ALC_RMB_REGISTER { get; set; }
    }
}
