using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_OPERANDO_DTO
    {
        public int FMLA_CODIGO { get; set; }
        public int OPDO_ORDEM { get; set; }
        public Nullable<int> OPDO_FMLA_CODIGO { get; set; }
        public string OPDO_EXPRESSAO { get; set; }
    }
}
