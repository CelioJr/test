﻿using RAST.BO.Rastreabilidade;
using SEMP.Model.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEMP.Areas.Coleta.Controllers
{
    public class CadModeloController : Controller
    {
        // GET: Coleta/CadModelo
        public ActionResult Index()
        {
            return View();
        }

        // GET: Coleta/CadModelo/Details/5
        public ActionResult Lista(string codItem, string desItem)
        {
			try
			{
				if (codItem == null) { codItem = ""; }
				if (desItem == null) { desItem = ""; }

				var dados = ModeloBO.ObterModelos(codItem, desItem);

				return View(dados);
			}
			catch (Exception ex)
			{
				ViewBag.Erro = "Erro ao consultar: " + ex.Message;
				return View(new List<CBModeloDTO>());
			}

            
        }
		

        // GET: Coleta/CadModelo/Edit/5
        public ActionResult Edit(string codModelo)
        {
			if (!String.IsNullOrEmpty(codModelo)) {
				var dados = ModeloBO.ObterModelo(codModelo);

				return View(dados);

			}

            return View();
        }

        // POST: Coleta/CadModelo/Edit/5
        [HttpPost]
        public ActionResult Edit(CBModeloDTO dados)
        {
            try
            {
				var resultado = ModeloBO.AtualizaModelo(dados);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Coleta/CadModelo/Delete/?codItem=
        public ActionResult Delete(string codItem)
        {

			var dados = ModeloBO.ObterModelo(codItem);

            return View(dados);
        }

        // POST: Coleta/CadModelo/Delete/
        [HttpPost]
        public ActionResult Delete(CBModeloDTO modelo)
        {
            try
            {
				ModeloBO.ExcluirModelo(modelo);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
