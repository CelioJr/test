﻿using System;
using System.Collections.Generic;
using System.Linq;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using SEMP.DAL.Models;
using System.Data.SqlClient;
using SEMP.Model.DTO;

namespace SEMP.DAL.DAO.Rastreabilidade
{
    public class daoAuditorSerie
	{
        //static daoAuditorSerie()
        //{
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<tbl_CBPosto, CBPostoDTO>();
        //        cfg.CreateMap<TBL_GpaRecItem, GpaRecItemDTO>();
        //    });
        //}

        public static List<String> ObterPassagemSerie(DateTime DatInicio, DateTime DatFim, int CodLinha)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				var series = (from p in dtbCb.tbl_CBPassagem
					where p.CodLinha == CodLinha &&
					      p.DatEvento >= DatInicio &&
					      p.DatEvento <= DatFim
					select p.NumECB
					).Distinct();
				return series.ToList();
			}
		}

		public static List<CBPassagemDTO> ObterPassagemLinha(DateTime DatInicio, DateTime DatFim, int CodLinha)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				var series = (from p in dtbCb.tbl_CBPassagem
							  join posto in dtbCb.tbl_CBPosto on p.NumPosto equals posto.NumPosto
							  let s = dtbCb.tbl_CBPassagem.Where(x => x.CodLinha == CodLinha && x.DatEvento >= DatInicio && x.DatEvento <= DatFim).Select(x => x.NumECB).ToList()
							  where p.CodLinha == CodLinha &&
									s.Contains(p.NumECB) &&
									posto.CodLinha == p.CodLinha
							  select new CBPassagemDTO() {
								CodDRT = p.CodDRT,
								CodLinha = p.CodLinha,
								DatEvento = p.DatEvento,
								DescricaoLinha = "",
								DescricaoPosto = posto.DesPosto,
								NomeOperador = "",
								NumECB = p.NumECB,
								NumPosto = p.NumPosto,
								NumSeq = posto.NumSeq
							  }
					);
				return series.ToList();
			}
		}

		public static List<PassagemSerie> ObterPassagem(DateTime DatInicio, DateTime DatFim, int CodLinha)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var pass = (from p in dtbCb.tbl_CBPassagem
					join id in dtbCb.tbl_CBIdentifica on p.NumECB equals id.NumECB
					join i in dtbCb.tbl_Item on p.NumECB.Substring(0,6) equals i.CodItem
					where (from p2 in dtbCb.tbl_CBPassagem
						where p2.CodLinha == CodLinha &&
						      p2.DatEvento >= DatInicio &&
						      p2.DatEvento <= DatFim
						select p2.NumECB
						).Distinct().Contains(p.NumECB)
					orderby p.NumECB
					select new
					{
						CodLinha = p.CodLinha,
						CodModelo = id.CodModelo ?? id.CodPlacaIMC ?? id.CodPlacaIAC,
						DatLeitura = p.DatEvento,
						NumPosto = p.NumPosto,
						NumSerie = p.NumECB,
						DesItem = i.DesItem
					}
					);
				var passagem = pass.ToList();

				var resultado = passagem.Select(p => new PassagemSerie()
				{
					CodLinha = p.CodLinha,
					CodModelo = p.CodModelo,
					DatLeitura = p.DatLeitura.ToString(),
					NumPosto = p.NumPosto,
					NumSerie = p.NumSerie.ToUpper(),
					DesModelo = p.DesItem.Trim()
				}).ToList();

				return resultado;
			}
		}

		public static List<PassagemSerie> ObterPassagem(DateTime DatInicio, DateTime DatFim, int CodLinha, String CodModelo)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var pass = (from p in dtbCb.tbl_CBPassagem
					join id in dtbCb.tbl_CBIdentifica on p.NumECB equals id.NumECB
					join posto in dtbCb.tbl_CBPosto on p.NumPosto equals posto.NumPosto
					join i in dtbCb.tbl_Item on p.NumECB.Substring(0, 6) equals i.CodItem
					where (id.CodModelo == CodModelo || id.CodPlacaIMC == CodModelo || id.CodPlacaIAC == CodModelo) &&
					      (from p2 in dtbCb.tbl_CBPassagem
						      where p2.CodLinha == CodLinha &&
						            p2.DatEvento >= DatInicio &&
						            p2.DatEvento <= DatFim
						      select p2.NumECB
							  ).Distinct().Contains(p.NumECB) && p.CodLinha == posto.CodLinha
					orderby posto.NumSeq
					select new
					{
						CodLinha = p.CodLinha,
						CodModelo = id.CodModelo ?? id.CodPlacaIMC ?? id.CodPlacaIAC,
						DatLeitura = p.DatEvento,
						NumPosto = p.NumPosto,
						NumSerie = p.NumECB.ToUpper(),
						DesItem = i.DesItem
					}
					);
				var passagem = pass.ToList();

				var resultado = passagem.Select(p => new PassagemSerie()
				{
					CodLinha = p.CodLinha,
					CodModelo = p.CodModelo,
					DatLeitura = p.DatLeitura.ToString(),
					NumPosto = p.NumPosto,
					NumSerie = p.NumSerie.ToUpper(),
					DesModelo = p.DesItem.Trim()
				}).ToList();

				return resultado;
			}
		}

		public static List<PassagemSerie> ObterPassagem(int CodLinha, String CodModelo)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				var pass = (from p in dtbCb.tbl_CBPassagem
					join id in dtbCb.tbl_CBIdentifica on p.NumECB equals id.NumECB
					join posto in dtbCb.tbl_CBPosto on p.NumPosto equals posto.NumPosto
					join i in dtbCb.tbl_Item on p.NumECB.Substring(0, 6) equals i.CodItem
					where (id.CodModelo == CodModelo || id.CodPlacaIMC == CodModelo || id.CodPlacaIAC == CodModelo) &&
					      (from p2 in dtbCb.tbl_CBPassagem
						      where p2.CodLinha == CodLinha
						      select p2.NumECB.ToUpper()
							  ).Distinct().Contains(p.NumECB) && p.CodLinha == posto.CodLinha
							orderby posto.NumSeq
					select new
					{
						CodLinha = p.CodLinha,
						CodModelo = id.CodModelo ?? id.CodPlacaIMC ?? id.CodPlacaIAC,
						DatLeitura = p.DatEvento,
						NumPosto = p.NumPosto,
						NumSerie = p.NumECB.ToUpper(),
						DesItem = i.DesItem
					}
					);
				var passagem = pass.ToList();

				var resultado = passagem.Select(p => new PassagemSerie()
				{
					CodLinha = p.CodLinha,
					CodModelo = p.CodModelo,
					DatLeitura = p.DatLeitura.ToString(),
					NumPosto = p.NumPosto,
					NumSerie = p.NumSerie.ToUpper(),
					DesModelo = p.DesItem.Trim()
				}).ToList();

				return resultado;
			}
		}

		public static List<PassagemSerie> ObterPassagem(String NumECB)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var pass = (from p in dtbCb.tbl_CBPassagem
					join id in dtbCb.tbl_CBIdentifica on p.NumECB equals id.NumECB
					join posto in dtbCb.tbl_CBPosto on p.NumPosto equals posto.NumPosto
					join i in dtbCb.tbl_Item on p.NumECB.Substring(0, 6) equals i.CodItem
					where p.NumECB == NumECB.ToUpper() && p.CodLinha == posto.CodLinha
					orderby posto.NumSeq
					select new
					{
						CodLinha = p.CodLinha,
						CodModelo = id.CodModelo ?? id.CodPlacaIMC ?? id.CodPlacaIAC,
						DatLeitura = p.DatEvento,
						NumPosto = p.NumPosto,
						NumSerie = p.NumECB.ToUpper(),
						DesItem = i.DesItem
					});
				var passagem = pass.ToList();

				var resultado = passagem.Select(p => new PassagemSerie()
				{
					CodLinha = p.CodLinha,
					CodModelo = p.CodModelo,
					DatLeitura = p.DatLeitura.ToString(),
					NumPosto = p.NumPosto,
					NumSerie = p.NumSerie.ToUpper(),
					DesModelo = p.DesItem.Trim()
				}).ToList();

				return resultado;
			}
		}

		public static bool ExisteLeitura(String NumECB)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				var passagem = (from p in dtbCb.tbl_CBPassagem
					where p.NumECB == NumECB.ToUpper()
					group p by p.NumECB.ToUpper()
					into g
					select g.Key).FirstOrDefault();

				return passagem != null;
			}
		}

		public static List<CBPostoDTO> ObterPostosObrigatorios(int CodLinha)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				var postos = (from t in dtbCb.tbl_CBPosto
					where t.CodLinha == CodLinha && t.flgObrigatorio == true
					orderby t.NumSeq
					select t
					).ToList();

				List<CBPostoDTO> postoDTO = new List<CBPostoDTO>();

				AutoMapper.Mapper.Map(postos, postoDTO);


				return postoDTO;
			}
		}

		public static List<CBPostoDTO> ObterPostosPassagem(string NumSerie)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				var postos = (from t in dtbCb.tbl_CBPosto
								join p in dtbCb.tbl_CBPassagem on new { t.CodLinha, t.NumPosto } equals new { p.CodLinha, p.NumPosto }
							  where p.NumECB == NumSerie
							  orderby t.NumSeq
							  select t
					).ToList();

				List<CBPostoDTO> postoDTO = new List<CBPostoDTO>();

				AutoMapper.Mapper.Map(postos, postoDTO);


				return postoDTO;
			}
		}

		public static int ObterLinhaPorSerie(string NumSerie)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				var postos = (from t in dtbCb.tbl_CBPassagem
							  where t.NumECB == NumSerie
							  select t.CodLinha
					).FirstOrDefault();

					return postos;
			}
		}


		public static List<String> ObterModelos(DateTime DatInicio, DateTime DatFim, int CodLinha)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				var modelos = (from p in dtbCb.tbl_CBPassagem
					join id in dtbCb.tbl_CBIdentifica on p.NumECB equals id.NumECB
					where p.CodLinha == CodLinha &&
					      p.DatEvento >= DatInicio &&
					      p.DatEvento <= DatFim
					group id by id.CodModelo
					into g
					select g.Key
					);

				return modelos.ToList();
			}
		}

		public static List<String> ObterModelos(int CodLinha)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				var modelos = (from p in dtbCb.tbl_CBPassagem
					join id in dtbCb.tbl_CBIdentifica on p.NumECB equals id.NumECB
					where p.CodLinha == CodLinha && id.CodModelo != null
					group id by id.CodModelo
					into g
					select g.Key
					).ToList();

				return modelos.ToList();
			}
		}

		public static List<String> ObterNaoLidos(DateTime DatInicio, DateTime DatFim, int CodLinha, String CodModelo)
		{
			try
			{
				using (DTB_CBContext dtbCb = new DTB_CBContext())
				{
					var naolido = dtbCb.Database.SqlQuery<Spc_CBAuditorSeriesNaoLidas>(
						"EXEC Spc_CBAuditorSeriesNaoLidas @DatInicio, @DatFim, @CodLinha, @CodModelo",
						new SqlParameter("DatInicio", DatInicio),
						new SqlParameter("DatFim", DatFim),
						new SqlParameter("CodLinha", CodLinha),
						new SqlParameter("CodModelo", CodModelo)
						);

					List<string> series = naolido.Select(x => x.Serie).ToList();

					return series;
				}
			}
			catch
			{
				return null;
			}

		}

		public static List<ResumoSeries> ObterResumoNaoLidas(String CodModelo, String SerieIni, String SerieFim, DateTime DatIni, DateTime DatFim){
			
			using (DTB_CBContext dtbCb = new DTB_CBContext()) {

				var resumo = dtbCb.Database.SqlQuery<ResumoSeries>(
					"EXEC spc_CBVerificaSerie @CodModelo, @SerieIni, @SerieFim, @DatIni, @DatFim, @FlgResumo",
					new SqlParameter("CodModelo", CodModelo),
					new SqlParameter("SerieIni", SerieIni),
					new SqlParameter("SerieFim", SerieFim),
					new SqlParameter("DatIni", DatIni),
					new SqlParameter("DatFim", DatFim),
					new SqlParameter("FlgResumo", "S")
					).ToList();

				return resumo;

			}
		}

		public static List<SeriesAusentes> ObterSeriesNaoLidas(String CodModelo, String SerieIni, String SerieFim, DateTime DatIni, DateTime DatFim)
		{

			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{

				var naolido = dtbCb.Database.SqlQuery<SeriesAusentes>(
					"EXEC spc_CBVerificaSerie @CodModelo, @SerieIni, @SerieFim, @DatIni, @DatFim, @FlgResumo",
					new SqlParameter("CodModelo", CodModelo),
					new SqlParameter("SerieIni", SerieIni),
					new SqlParameter("SerieFim", SerieFim),
					new SqlParameter("DatIni", DatIni),
					new SqlParameter("DatFim", DatFim),
					new SqlParameter("FlgResumo", "N")
					).ToList();
				
				return naolido;

			}
		}

		public static int ObterDefeitosDesassociacao(String NumSerie)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				var defeitos = (from d in dtbCb.tbl_CBAmarra_Hist
					where d.NumSerie == NumSerie
					group d by new {d.NumSerie, d.CodLinha, d.NumPosto}
					into grupo
					select new
					{
						grupo.Key.NumSerie,
						grupo.Key.CodLinha,
						grupo.Key.NumPosto,
						QtdDefeito = grupo.Count()
					}
					).OrderBy(x => x.NumPosto).Take(1);

				var result = defeitos.FirstOrDefault();

				return result != null ? result.QtdDefeito : 0;
			}
		}

		public static GpaRecItemDTO ObterLeituraGPA(String CodModelo, String NumSerie)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var leituragpa = dtbSim.TBL_GpaRecItem.FirstOrDefault(x => x.CodItem == CodModelo && x.NumSerie == NumSerie);

				GpaRecItemDTO leituragpaDTO = new GpaRecItemDTO();

				AutoMapper.Mapper.Map(leituragpa, leituragpaDTO);

				return leituragpaDTO;
			}
		}


		public static List<CBEmbaladaDTO> ObterSeriesNaoAmarradas(DateTime startDate, int? codLinha, string codModelo) {

			using (var contexto = new DTB_CBContext())
			{
				var embalados = (from emb in contexto.tbl_CBEmbalada
								 join linha in contexto.tbl_CBLinha on new { emb.CodLinha, Setor = "IMC" } equals new { linha.CodLinha, linha.Setor }
								 join func in contexto.tbl_RIFuncionario on emb.CodOperador equals func.CodDRT
								 join modelo in contexto.tbl_Item on emb.CodModelo equals modelo.CodItem
								 from amarra in contexto.tbl_CBAmarra.Where(x => x.NumECB == emb.NumECB).DefaultIfEmpty()
								 let qtdDefeitos = contexto.tbl_CBDefeito.Count(x => x.NumECB == emb.NumECB && x.CodLinha == emb.CodLinha)
								 where emb.DatReferencia >= startDate && 
										(codLinha == null || emb.CodLinha == codLinha) &&
										(codModelo == null || codModelo == "" || emb.CodModelo == codModelo) &&
										amarra == null
								orderby new { emb.CodLinha, emb.CodModelo, emb.DatLeitura }
								 select new CBEmbaladaDTO() { 
									CodFab = emb.CodFab,
									CodLinha = emb.CodLinha,
									CodLinhaFec = emb.CodLinhaFec,
									CodModelo = emb.CodModelo,
									CodOperador = emb.CodOperador,
									CodTestador = emb.CodTestador,
									DatLeitura = emb.DatLeitura,
									DatReferencia = emb.DatReferencia.Value.ToString(),
									DescricaoLinha = linha.DesLinha,
									NumAP = emb.NumAP,
									NumCC = emb.NumCC,
									NumECB = emb.NumECB,
									NumLote = emb.NumLote,
									NumPosto = emb.NumPosto,
									Turno = 1,
									Operador = func.NomFuncionario,
									DescricaoModelo = modelo.DesItem.Trim(),
									QtdDefeitos = qtdDefeitos
								 }).AsQueryable();

				return embalados.ToList();
			}
		
		}

		public static List<CBPassagemDTO> ObterSeriesNaoEmbaladas(DateTime startDate, int? codLinha)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = (from pass in contexto.tbl_CBPassagem
								 join linha in contexto.tbl_CBLinha on pass.CodLinha equals linha.CodLinha
								 join func in contexto.tbl_RIFuncionario on pass.CodDRT equals func.CodDRT
								 from embalado in contexto.tbl_CBEmbalada.Where(x => x.NumECB == pass.NumECB && x.CodLinha == pass.CodLinha).DefaultIfEmpty()
								 let qtdDefeitos = contexto.tbl_CBDefeito.Count(x => x.NumECB == pass.NumECB && x.CodLinha == pass.CodLinha)
								 let qtdAmarrado = contexto.tbl_CBAmarra.Count(x => x.NumECB == pass.NumECB)
								 where pass.DatEvento >= startDate &&
										(codLinha == null || pass.CodLinha == codLinha) &&
										embalado == null
								 group new { pass, linha } by new { pass.NumECB, pass.CodLinha, linha.DesLinha, qtdDefeitos, qtdAmarrado } into grupo
								 select new CBPassagemDTO()
								 {
									 CodLinha = grupo.Key.CodLinha,
									 DescricaoLinha = grupo.Key.DesLinha,
									 NumECB = grupo.Key.NumECB,
									 QtdDefeitos = grupo.Key.qtdDefeitos,
									 DatEvento = grupo.Max(x => x.pass.DatEvento),
									 QtdAmarra = grupo.Key.qtdAmarrado
								 }).OrderBy(x => x.DatEvento).AsQueryable();

				return dados.ToList();
			}

		}

		public static List<CBEmbaladaDTO> ObterEmbaladosIACIMC(DateTime startDate, int? codLinha, string codModelo)
		{

			using (var contexto = new DTB_CBContext())
			{
				var embalados = (from emb in contexto.tbl_CBEmbalada
								 join linha in contexto.tbl_CBLinha on new { emb.CodLinha, Setor = "IAC" } equals new { linha.CodLinha, linha.Setor }
								 join func in contexto.tbl_RIFuncionario on emb.CodOperador equals func.CodDRT
								 join modelo in contexto.tbl_Item on emb.CodModelo equals modelo.CodItem
								 from passagem in contexto.tbl_CBPassagem.Where(x => x.NumECB == emb.NumECB && contexto.tbl_CBLinha.Where(y => y.Setor == "IMC").Select(z => z.CodLinha).Contains(x.CodLinha) ).DefaultIfEmpty()
								 let qtdDefeitos = contexto.tbl_CBDefeito.Count(x => x.NumECB == emb.NumECB && x.CodLinha == emb.CodLinha)
								 where emb.DatReferencia >= startDate &&
										(codLinha == null || emb.CodLinha == codLinha) &&
										(codModelo == null || codModelo == "" || emb.CodModelo == codModelo) &&
										passagem == null
								 orderby new { emb.CodLinha, emb.CodModelo, emb.DatLeitura }
								 select new CBEmbaladaDTO()
								 {
									 CodFab = emb.CodFab,
									 CodLinha = emb.CodLinha,
									 CodLinhaFec = emb.CodLinhaFec,
									 CodModelo = emb.CodModelo,
									 CodOperador = emb.CodOperador,
									 CodTestador = emb.CodTestador,
									 DatLeitura = emb.DatLeitura,
									 DatReferencia = emb.DatReferencia.Value.ToString(),
									 DescricaoLinha = linha.DesLinha,
									 NumAP = emb.NumAP,
									 NumCC = emb.NumCC,
									 NumECB = emb.NumECB,
									 NumLote = emb.NumLote,
									 NumPosto = emb.NumPosto,
									 Turno = 1,
									 Operador = func.NomFuncionario,
									 DescricaoModelo = modelo.DesItem.Trim(),
									 QtdDefeitos = qtdDefeitos
								 }).AsQueryable();

				return embalados.ToList();
			}

		}
		
	}
}
