namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolpalogtec
    {
        [Key]
        public long id_consolpalogtec { get; set; }

        public long? id_consolpalog { get; set; }

        public long? id_usr { get; set; }

        public short? tp_usuario { get; set; }
    }
}
