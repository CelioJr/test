using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DTB_CB.Models.Mapping
{
    public class tbl_CBPostoMap : EntityTypeConfiguration<tbl_CBPosto>
    {
        public tbl_CBPostoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodLinha, t.NumPosto });

            // Properties
            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumPosto)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DesPosto)
                .HasMaxLength(50);

            this.Property(t => t.NumIP)
                .HasMaxLength(15);

            this.Property(t => t.CodDRT)
                .IsFixedLength()
                .HasMaxLength(8);

            this.Property(t => t.FlgTipoTerminal)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.FlgAtivo)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.TipoAmarra)
                .IsFixedLength()
                .HasMaxLength(2);

            //this.Property(t => t.flgPedeAP)
            //    .IsFixedLength()
            //    .HasMaxLength(1);

            //this.Property(t => t.flgImprimeEtq)
            //    .IsFixedLength()
            //    .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("tbl_CBPosto");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.NumPosto).HasColumnName("NumPosto");
            this.Property(t => t.CodTipoPosto).HasColumnName("CodTipoPosto");
            this.Property(t => t.DesPosto).HasColumnName("DesPosto");
            this.Property(t => t.NumIP).HasColumnName("NumIP");
            this.Property(t => t.CodDRT).HasColumnName("CodDRT");
            this.Property(t => t.FlgTipoTerminal).HasColumnName("FlgTipoTerminal");
            this.Property(t => t.FlgAtivo).HasColumnName("FlgAtivo");
            this.Property(t => t.NumSeq).HasColumnName("NumSeq");
            this.Property(t => t.flgObrigatorio).HasColumnName("flgObrigatorio");
            this.Property(t => t.flgDRTObrig).HasColumnName("flgDRTObrig");
            this.Property(t => t.TipoAmarra).HasColumnName("TipoAmarra");
            //this.Property(t => t.flgPedeAP).HasColumnName("flgPedeAP");
            //this.Property(t => t.flgImprimeEtq).HasColumnName("flgImprimeEtq");
            //this.Property(t => t.TempoLeitura).HasColumnName("TempoLeitura");
            //this.Property(t => t.TipoEtiqueta).HasColumnName("TipoEtiqueta");

            // Relationships
            this.HasRequired(t => t.tbl_CBLinha)
                .WithMany(t => t.tbl_CBPosto)
                .HasForeignKey(d => d.CodLinha);
            this.HasRequired(t => t.tbl_CBTipoPosto)
                .WithMany(t => t.tbl_CBPosto)
                .HasForeignKey(d => d.CodTipoPosto);

        }
    }
}
