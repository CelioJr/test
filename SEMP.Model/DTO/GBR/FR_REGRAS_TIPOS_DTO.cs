using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_REGRAS_TIPOS_DTO
    {
        public int TIP_COD { get; set; }
        public string TIP_NOME { get; set; }
        public Nullable<int> TIP_VISIVEL { get; set; }
        public string TIP_EQUIVALENTE { get; set; }
        public Nullable<int> TIP_ASPAS { get; set; }
        public Nullable<int> TIP_DEFAULT { get; set; }
        public Nullable<int> TIP_CATEGORIA { get; set; }
        public Nullable<int> TIP_SUPER { get; set; }
        public string TIP_NOME_INTERNO { get; set; }
        public Nullable<int> TIP_TAM_OBRIGATORIO { get; set; }
        public Nullable<int> TIP_VISIVEL_PARAM_ENTRADA { get; set; }
        public Nullable<int> TIP_VISIVEL_VAR { get; set; }
        public Nullable<int> TIP_VISIVEL_CONST { get; set; }
        public Nullable<int> TIP_VISIVEL_PARAM_SAIDA { get; set; }
    }
}
