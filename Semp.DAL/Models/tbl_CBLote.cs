using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBLote
    {
        public string NumLote { get; set; }
        public Nullable<int> Qtde { get; set; }
        public Nullable<System.DateTime> DatAbertura { get; set; }
        public Nullable<System.DateTime> DatFechamento { get; set; }
		public Nullable<int> Status { get; set; }
        public Nullable<System.DateTime> DatTransferencia { get; set; }
        public Nullable<System.DateTime> DatRecebimento { get; set; }
        public string CodDRTFechamento { get; set; }
        public string JustFechamento { get; set; }
        public string CodDRTRecebimento { get; set; }
        public string CodModelo { get; set; }
        public string CodFab { get; set; }
        public string NumAP { get; set; }
        public Nullable<int> TamLote { get; set; }
		public string NumLotePai { get; set; }

        public int? CodLinha { get; set; }
		public int? NumPosto { get; set; }

		public int? TamAmostras { get; set; }
    }
}
