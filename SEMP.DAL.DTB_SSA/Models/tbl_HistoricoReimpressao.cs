﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.DTB_SSA.Models
{
	public class tbl_HistoricoReimpressao
	{
		public string DesSerie { get; set; }
		public System.DateTime DatImpressao { get; set; }
		public Nullable<int> CodOpe { get; set; }
		public string TipoEtiqueta { get; set; }
	}
}
