﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.Apontaprod;
using SEMP.Model.DTO;

namespace RAST.BO.ApontaProd
{
	public class RastreiaPCI
	{

		public static List<CBRastreiaPCIDTO> ObterRastreio(string Codigo)
		{
			var rastreio = daoRastreiaPCI.ObterRastreio(Codigo);

			if (rastreio == null || !rastreio.Any())
			{
				rastreio = daoRastreiaPCI.ObterRastreioOld(Codigo);
			}

			return rastreio;
		}

	}
}
