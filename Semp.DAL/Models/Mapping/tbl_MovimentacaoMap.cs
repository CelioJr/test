using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_MovimentacaoMap : EntityTypeConfiguration<tbl_Movimentacao>
    {
        public tbl_MovimentacaoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.NumSeq, t.CreDeb, t.CodTra, t.DatTra, t.CodItem, t.DocInterno1, t.CodLocal, t.DatDig, t.QtdTra });

            // Properties
            this.Property(t => t.CodFab)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CreDeb)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.CodTra)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.CodItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.DocInterno1)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.DocInterno2)
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.AgeExterno)
                .IsFixedLength()
                .HasMaxLength(4)
                .IsUnicode(false);

            this.Property(t => t.CodLocal)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.QtdTra)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DocExterno)
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.UnidPreco)
                .IsFixedLength()
                .HasMaxLength(4)
                .IsUnicode(false);

            this.Property(t => t.NomUsuario)
                .HasMaxLength(30)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_Movimentacao");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.NumSeq).HasColumnName("NumSeq");
            this.Property(t => t.CreDeb).HasColumnName("CreDeb");
            this.Property(t => t.CodTra).HasColumnName("CodTra");
            this.Property(t => t.DatTra).HasColumnName("DatTra");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.DocInterno1).HasColumnName("DocInterno1");
            this.Property(t => t.DocInterno2).HasColumnName("DocInterno2");
            this.Property(t => t.AgeExterno).HasColumnName("AgeExterno");
            this.Property(t => t.CodLocal).HasColumnName("CodLocal");
            this.Property(t => t.DatDig).HasColumnName("DatDig");
            this.Property(t => t.QtdTra).HasColumnName("QtdTra");
            this.Property(t => t.DocExterno).HasColumnName("DocExterno");
            this.Property(t => t.PrecoUnitario).HasColumnName("PrecoUnitario");
            this.Property(t => t.UnidPreco).HasColumnName("UnidPreco");
            this.Property(t => t.NomUsuario).HasColumnName("NomUsuario");
        }
    }
}
