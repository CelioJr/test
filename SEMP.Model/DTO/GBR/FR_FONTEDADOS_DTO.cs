using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_FONTEDADOS_DTO
    {
        public int FNT_CODIGO { get; set; }
        public Nullable<int> FNT_CODIGO_PARENT { get; set; }
        public string FNT_CAMPOCHAVE { get; set; }
        public string FNT_TABELA { get; set; }
        public string FNT_SQLSELECT { get; set; }
        public string FNT_SQLINSERT { get; set; }
        public string FNT_SQLUPDATE { get; set; }
        public string FNT_SQLDELETE { get; set; }
        public string FNT_CAMPOINCREMENTO { get; set; }
        public int FRM_CODIGO { get; set; }
        public string FNT_CAMPOGRADE { get; set; }
        public string FNT_CAMPOPESQUISA { get; set; }
        public string FNT_SQLDEFAULT { get; set; }
        public string FNT_SQLMASCARA { get; set; }
        public virtual FR_FORMULARIO_DTO FR_FORMULARIO { get; set; }
    }
}
