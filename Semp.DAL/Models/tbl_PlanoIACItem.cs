namespace SEMP.DAL.Models
{
    public partial class tbl_PlanoIACItem
    {
        public string CodModelo { get; set; }
        public decimal NumEstacaoZ { get; set; }
        public string NumPosicao { get; set; }
        public string NumEtapa { get; set; }
        public string NumRevisao { get; set; }
        public string CodProcesso { get; set; }
        public string CodItem { get; set; }
        public string Polaridade { get; set; }
        public System.Guid rowguid { get; set; }
        public short FlgLeftRight { get; set; }
        public string DesLeftRight { get; set; }
    }
}
