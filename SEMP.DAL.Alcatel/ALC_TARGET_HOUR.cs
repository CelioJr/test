namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_TARGET_HOUR")]
    public partial class ALC_TARGET_HOUR
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ALC_TARGET_HOUR()
        {
            ALC_ORDER = new HashSet<ALC_ORDER>();
            ALC_PROD_PLAN = new HashSet<ALC_PROD_PLAN>();
        }

        [Key]
        public int ID_TARGET { get; set; }

        public int? VALUE { get; set; }

        public DateTime? DT_END { get; set; }

        public DateTime? DT_START { get; set; }

        public int COD_LINE { get; set; }

        public int COD_SKU { get; set; }

        [MaxLength(255)]
        public byte[] line { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_ORDER> ALC_ORDER { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_PROD_PLAN> ALC_PROD_PLAN { get; set; }

        public virtual ALC_HOUR_SHIFT ALC_HOUR_SHIFT { get; set; }
    }
}
