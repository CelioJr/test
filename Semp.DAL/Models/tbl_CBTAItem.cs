using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBTAItem
    {
        public string CodModelo { get; set; }
        public string NumSerieModelo { get; set; }
        public string CodModeloPCI { get; set; }
        public string NumSeriePCI { get; set; }
        public string Fase { get; set; }
        public string SubFase { get; set; }
        public string CodProcesso { get; set; }
        public int Ordem { get; set; }
        public Nullable<System.DateTime> DatEntrada { get; set; }
        public Nullable<System.DateTime> DatSaida { get; set; }
        public Nullable<int> TempoCorrido { get; set; }
        public Nullable<int> TempoAtravessa { get; set; }
        public Nullable<int> QtdDefeito { get; set; }
        public Nullable<int> TempoTecnico { get; set; }
        public Nullable<int> CodLinha { get; set; }
		public bool? FlgTempoMedio { get; set; }
	}
}
