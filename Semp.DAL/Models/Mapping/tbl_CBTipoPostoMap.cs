using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBTipoPostoMap : EntityTypeConfiguration<tbl_CBTipoPosto>
    {
        public tbl_CBTipoPostoMap()
        {
            // Primary Key
            this.HasKey(t => t.CodTipoPosto);

            // Properties
            this.Property(t => t.CodTipoPosto)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DesTipoPosto)
                .HasMaxLength(50)
                .IsUnicode(false);

            this.Property(t => t.DesAcao)
                .HasMaxLength(30)
                .IsUnicode(false);

			this.Property(t => t.DesCor)
				.HasMaxLength(20)
                .IsUnicode(false);

			this.Property(t => t.FlgEntradaSaida)
				.HasMaxLength(1)
                .IsUnicode(false);

			// Table & Column Mappings
			this.ToTable("tbl_CBTipoPosto");
            this.Property(t => t.CodTipoPosto).HasColumnName("CodTipoPosto");
            this.Property(t => t.DesTipoPosto).HasColumnName("DesTipoPosto");
            this.Property(t => t.DesAcao).HasColumnName("DesAcao");
			this.Property(t => t.DesCor).HasColumnName("DesCor");
			this.Property(t => t.FlgEntradaSaida).HasColumnName("FlgEntradaSaida");
		}
    }
}
