﻿using System;
using System.Collections.Generic;
using System.Linq;
using SEMP.DAL.Common;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.DAL.Models;
using SEMP.Model.VO;
using SEMP.Model.DTO;
using SEMP.Model;
using SEMP.Model.VO.Embalagem;
using DateTime = System.DateTime;
using SEMP.Model.VO.Rastreabilidade;

namespace RAST.BO.Rastreabilidade
{
	public class Geral
	{

		public static List<CBLinhaDTO> ObterLinhaPorFabrica(string fabrica)
		{
			return daoGeral.ObterLinhaPorFabrica(fabrica);
		}

		public static List<CBLinhaDTO> ObterLinhaPorData(DateTime datProducao)
		{
			return daoGeral.ObterLinhaPorData(datProducao);
		}

		public static List<CBLinhaDTO> ObterLinhasRecebimento(string fabrica)
		{
			return daoGeral.ObterLinhasRecebimento(fabrica);
		}

		public static string ObterFabricaPorLinha(int CodLinha)
		{
			return daoGeral.ObterFabricaPorLinha(CodLinha);
		}

		public static List<CBPostoDTO> ObterPostoPorLinha(int codLinha, bool flgNome = false)
		{
			var postos = daoGeral.ObterPostoPorLinha(codLinha);

			if (!flgNome) return postos;

			var postosdrt = postos.Where(x => !String.IsNullOrEmpty(x.CodDRT)).ToList();

			foreach (var posto in postosdrt)
			{
				var func = daoLogin.ObterFuncionarioByDRT(posto.CodDRT);

				if (func != null)
				{
					posto.NomFuncionario = func.NomFuncionario;
				}
				else
				{
					posto.NomFuncionario = "";
				}
			}

			return postos;
		}

		public static CBPostoDTO ObterPostoPorDRT(string codDrt)
		{
			return daoGeral.ObterPostoPorDRT(codDrt);
		}

		public static String ObterLinhaCliente(int codLinha)
		{

			return daoGeral.ObterLinhaCliente(codLinha);

		}

		public static CBLinhaDTO ObterLinha(int? codLinha)
		{
			if (codLinha != null)
			{
				return daoGeral.ObterLinha((int)codLinha);
			}
			return null;
		}

		public static CBLinhaDTO ObterLinhaT1(string codLinha)
		{
			return daoGeral.ObterLinhaT1(codLinha);
		}

		public static LinhaDTO ObterLinha(string codLinha)
		{
			if (codLinha != null)
			{
				return daoGeral.ObterLinha(codLinha);
			}
			return null;
		}

		public static FabricaDTO ObterFabrica(string codFab)
		{
			if (codFab != null)
			{
				return daoGeral.ObterFabrica(codFab);
			}
			return null;
		}

		public static CBPostoDTO ObterPostoPorIP(string numIP)
		{
			return daoGeral.ObterPostoPorIP(numIP);
		}

		public static CBTipoPostoDTO ObterTipoPosto(int codTipoPosto)
		{
			return daoGeral.ObterTipoPosto(codTipoPosto);
		}

		public static List<CBTipoPostoDTO> ObterTipoPostos()
		{
			return daoGeral.ObterTipoPostos();
		}

		public static CBPostoDTO ObterPosto(int? codLinha, int? numPosto)
		{
			if (codLinha != null && numPosto != null)
			{
				return daoGeral.ObterPosto((int)codLinha, (int)numPosto);
			}
			return null;
		}

		public static String ObterDescricaoItem(String CodItem)
		{
			return daoGeral.ObterDescricaoItem(CodItem);
		}

		public static ItemDTO ObterItem(String CodItem)
		{
			return daoGeral.ObterItem(CodItem);
		}

		public static List<viw_ItemDTO> ObterItens()
		{
			return daoGeral.ObterItens();
		}

		public static string ObterDesDepartamento(string codFab, string codDepto)
		{
			return daoGeral.ObterDesDepartamento(codFab, codDepto);
		}

		/// <summary>
		/// Consulta no DAL a rotina que obtem modelos
		/// </summary>
		/// <returns></returns>
		public static List<viw_ItemDTO> ObterModelos()
		{
			return daoGeral.ObterModelos();
		}

		public static List<viw_ItemDTO> ObterModelos(string descricao, int? pagina = 1)
		{
			return daoGeral.ObterModelos(descricao, pagina);
		}

		public static List<viw_ItemDTO> ObterModelosPCI(string descricao, int? pagina = 1)
		{
			return daoGeral.ObterModelosPCI(descricao, pagina);
		}

		public static List<ItemDTO> ObterModelosAll()
		{
			return daoGeral.ObterModelosAll();
		}

		public static List<viw_ItemDTO> ObterModelosPrograma(string localizar, DateTime data)
		{
			return daoGeral.ObterModelosPrograma(localizar, data);
		}

		public static List<viw_ItemDTO> ObterModelosProgramaMes(string localizar, DateTime data)
		{
			return daoGeral.ObterModelosProgramaMes(localizar, data);
		}

		public static List<viw_ItemDTO> ObterModelos(string localizar, DateTime data)
		{
			return daoGeral.ObterModelos(localizar);
		}

		public static String ObterDescResumidaItem(String CodItem)
		{
			return daoGeral.ObterDescResumidaItem(CodItem);
		}

		public static string ObterCodigoModelo(string codFabrica, string numeroAP)
		{
			return daoGeral.ObterCodigoModelo(codFabrica, numeroAP);
		}

		public static Mensagem CancelarLeitura(string NumECB, int CodLinha, int NumPosto)
		{
			DateTime DatEventoAtual = SEMP.BO.Util.GetDate();
			DateTime DatEvento = new DateTime();

			Mensagem mensagem = new Mensagem();

			DatEvento = daoPassagem.ObterDataPassagem(NumECB, CodLinha, NumPosto);

			if (DatEvento == DateTime.MinValue)
				DatEvento = daoDefeitos.ObterDataDefeito(NumECB, CodLinha, NumPosto);

			if (DatEvento == DateTime.MinValue)
			{
				mensagem = daoMensagem.ObterMensagem(Constantes.RAST_REGISTRONAOENCONTRADO);
				return mensagem;
			}
			else if ((DatEventoAtual - DatEvento).TotalMinutes > 5)
			{
				mensagem = daoMensagem.ObterMensagem(Constantes.RAST_TEMPO_LIMITE);
				return mensagem;
			}
			else
			{
				Boolean gravar = daoPassagem.CancelamentoLeitura(NumECB, CodLinha, NumPosto);

				if (gravar == false)
				{
					gravar = daoDefeitos.CancelamentoDefeito(NumECB, CodLinha, NumPosto);

					if (gravar == false)
					{
						mensagem = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
						return mensagem;
					}
					mensagem = daoMensagem.ObterMensagem(Constantes.ITEM_CANCELADO);
					return mensagem;
				}
				mensagem = daoMensagem.ObterMensagem(Constantes.ITEM_CANCELADO);
				return mensagem;
			}
		}

		public static int ObterTotalAprovadas(int CodLinha, int NumPosto)
		{
			int Aprovadas = 0;
			DateTime DatInicio = new DateTime();
			DateTime DatFim = new DateTime();
			DateTime Data = SEMP.BO.Util.GetDate();
			DateTime Hora = SEMP.BO.Util.GetDate();
			//DateTime Hora = DateTime.Now;
			TimeSpan HoraAtual = new TimeSpan(Hora.Hour, Hora.Minute, Hora.Second);

			/*if((Hora.Hour >= 0 ) && (Hora.Hour < 7))
				Data.AddDays(Data.Day - 1);*/

			DatInicio = daoGeral.ObterDataInicio(CodLinha, HoraAtual);
			if ((Hora.Hour >= 0) && (Hora.Hour < 6))
				DatInicio = DatInicio.AddDays(-1);
			DatFim = daoGeral.ObterDataFim(CodLinha, HoraAtual);
			if ((DatFim.Hour >= 0) && (DatFim.Hour < 6))
				DatFim = DatFim.AddDays(1);

			Aprovadas = daoGeral.ObterTotalAprovadas(CodLinha, NumPosto, DatInicio, DatFim);

			return Aprovadas;
		}

		public static int ObterTotalAprovadasModelo(int CodLinha, int NumPosto, string codModelo)
		{
			int Aprovadas = 0;
			DateTime DatInicio = new DateTime();
			DateTime DatFim = new DateTime();
			DateTime Data = SEMP.BO.Util.GetDate();
			DateTime Hora = SEMP.BO.Util.GetDate();
			//DateTime Hora = DateTime.Now;
			TimeSpan HoraAtual = new TimeSpan(Hora.Hour, Hora.Minute, Hora.Second);

			/*if((Hora.Hour >= 0 ) && (Hora.Hour < 7))
				Data.AddDays(Data.Day - 1);*/

			DatInicio = daoGeral.ObterDataInicio(CodLinha, HoraAtual);
			if ((Hora.Hour >= 0) && (Hora.Hour < 6))
				DatInicio = DatInicio.AddDays(-1);
			DatFim = daoGeral.ObterDataFim(CodLinha, HoraAtual);
			if ((DatFim.Hour >= 0) && (DatFim.Hour < 6))
				DatFim = DatFim.AddDays(1);

			Aprovadas = daoGeral.ObterTotalAprovadasModelo(CodLinha, NumPosto, DatInicio, DatFim, codModelo);

			return Aprovadas;
		}

		public static int ObterTotalAmarradoAP(string AP)
		{
			return daoGeral.ObterTotalAmarradoAP(AP);
		}

		public static int ObterTotalAmarradoAP(int codLinha, int numPosto, string codFab, string numAP)
		{
			return daoGeral.ObterTotalAmarradoAP(codLinha, numPosto, codFab, numAP);
		}

		public static int ObterTotalReprovadas(int CodLinha, int NumPosto)
		{
			int Reprovadas = 0;
			DateTime DatInicio = new DateTime();
			DateTime DatFim = new DateTime();
			DateTime Data = SEMP.BO.Util.GetDate();
			DateTime Hora = SEMP.BO.Util.GetDate();
			//DateTime Hora = DateTime.Now;
			TimeSpan HoraAtual = new TimeSpan(Hora.Hour, Hora.Minute, Hora.Second);

			if ((Hora.Hour >= 0) && (Hora.Hour < 6))
				Data.AddDays(Data.Day - 1);

			DatInicio = daoGeral.ObterDataInicio(CodLinha, HoraAtual);
			if ((Hora.Hour >= 0) && (Hora.Hour < 6))
				DatInicio = DatInicio.AddDays(-1);
			DatFim = daoGeral.ObterDataFim(CodLinha, HoraAtual);
			if ((DatFim.Hour >= 0) && (DatFim.Hour < 6))
				DatFim = DatFim.AddDays(1);

			Reprovadas = daoGeral.ObterTotalReprovadas(CodLinha, NumPosto, DatInicio, DatFim);

			return Reprovadas;
		}

		public static int ObterTotal(int CodLinha, int NumPosto)
		{
			int Aprovadas = 0;
			DateTime DatInicio = new DateTime();
			DateTime DatFim = new DateTime();
			DateTime Data = SEMP.BO.Util.GetDate();
			DateTime Hora = SEMP.BO.Util.GetDate();
			//DateTime Hora = DateTime.Now;
			TimeSpan HoraAtual = new TimeSpan(Hora.Hour, Hora.Minute, Hora.Second);

			if ((Hora.Hour >= 0) && (Hora.Hour < 6))
				Data.AddDays(Data.Day - 1);

			DatInicio = daoGeral.ObterDataInicio(CodLinha, HoraAtual);
			if ((Hora.Hour >= 0) && (Hora.Hour < 6))
				DatInicio = DatInicio.AddDays(-1);
			DatFim = daoGeral.ObterDataFim(CodLinha, HoraAtual);
			if ((DatFim.Hour >= 0) && (DatFim.Hour < 6))
				DatFim = DatFim.AddDays(1);

			Aprovadas = daoGeral.ObterTotal(CodLinha, NumPosto, DatInicio, DatFim);

			return Aprovadas;
		}

		public static int ObterQuantidadeLotePorLinha(int codLinha)
		{
			return daoGeral.ObterQuantidadeLotePorLinha(codLinha);
		}

		public static int ObterQuantidadeCaixaColetivaPorLinha(int codLinha)
		{
			return daoGeral.ObterQuantidadeCaixaColetivaPorLinha(codLinha);
		}

		public static int QtdeCaixasColetivasNoLote(string numLote)
		{
			return daoGeral.QtdeCaixasColetivasNoLote(numLote);
		}

		public static int QtdeCaixasColetivasNoLote(int codLinha)
		{
			return daoGeral.QtdeCaixasColetivasNoLote(codLinha);
		}

		public static int ObterTotalAP(string CodFab, string NumAP)
		{
			return daoGeral.ObterTotalAP(CodFab, NumAP);
		}

		public static tbl_CBLoteConfig Obter_tblCBLoteConfig(int codLinha)
		{
			return daoGeral.Obter_tblCBLoteConfig(codLinha);
		}

		public static string ValidarAP(string codFabrica, string numeroAp, DateTime dataReferencia, CBLinhaDTO linha)
		{
			if (linha.Setor.Equals("FEC") && linha.Familia.Equals("INF"))
			{
				string apVerificar = Convert.ToInt32(numeroAp).ToString("000000");

				string numTemp = Geral.VerificarAPDestino(codFabrica, apVerificar);

				if (!(string.IsNullOrEmpty(numTemp)))
				{
					numTemp = Convert.ToInt32((numTemp)).ToString("000000");
					return daoGeral.ValidarAP(codFabrica, numTemp, dataReferencia);
				}
			}

			return daoGeral.ValidarAP(codFabrica, numeroAp, dataReferencia);
		}

		public static string ValidarAPAmarra(string codFabrica, string numeroAp, DateTime dataReferencia, CBLinhaDTO linha, int numPosto)
		{

			var retorno = "";

			if (linha.Setor.Equals("FEC") && linha.Familia.Equals("INF"))
			{
				string apVerificar = Convert.ToInt32(numeroAp).ToString("000000");

				string numTemp = Geral.VerificarAPDestino(codFabrica, apVerificar);

				if (!(string.IsNullOrEmpty(numTemp)))
				{
					numTemp = Convert.ToInt32((numTemp)).ToString("000000");
					retorno = daoGeral.ValidarAP(codFabrica, numTemp, dataReferencia);
					numeroAp = numTemp;
				}
			}
			else {
				retorno = daoGeral.ValidarAP(codFabrica, numeroAp, dataReferencia);
			}

			if (retorno != string.Empty && retorno.Contains("AP Informada Está Fora da Ordem do FIFO")) {

				var apFifo = retorno.Split(' ').Last();

				var qtdAmarrado = daoGeral.ObterTotalAmarradoAP(linha.CodLinha, numPosto, codFabrica, apFifo);

				var AP = daoGeral.ObterAP(codFabrica, apFifo);

				if (AP != null && AP.QtdLoteAP <= qtdAmarrado) {

					retorno = "";
				}

			}

			return retorno;
		}

		/// <summary>
		/// Obter AP pela Fábrica e Número
		/// </summary>
		/// <param name="codFab">Código da Fábrica</param>
		/// <param name="numAP">Número da AP</param>
		/// <returns></returns>
		public static LSAAPDTO ObterAP(string codFab, string numAP)
		{
			return daoGeral.ObterAP(codFab, numAP);
		}

		/// <summary>
		/// Retorna a AP pelo número da Ordem de Produção
		/// </summary>
		/// <param name="numOP">Número da Ordem de Produção no SAP</param>
		/// <returns></returns>
		public static LSAAPDTO ObterAP(string numOP)
		{
			return daoGeral.ObterAP(numOP);
		}

		public static List<LSAAPDTO> ObterAPModeloData(string codModelo, DateTime datReferencia)
		{
			return daoGeral.ObterAPModeloData(codModelo, datReferencia);
		}

		public static List<LSAAPItemDTO> ObterAPItem(string NumOP){

			return daoGeral.ObterAPItem(NumOP);

		}

		public static string GetCodigoCinescopio(string codFabrica, string numeroAP, DateTime dataReferencia)
		{
			return daoGeral.RecuperarInformacoes_ValidarAP(codFabrica, numeroAP, dataReferencia);
		}

		public static CBAmarraDTO GetItemAmarrado(string NumSerie)
		{
			return daoGeral.GetItemAmarrado(NumSerie);
		}

		public static string GetPlaca(string NumSerie, string codItem)
		{
			return daoGeral.GetPlaca(NumSerie, codItem);
		}

		public static string GetItem(string NumSerie, string codItem)
		{
			return daoGeral.GetItem(NumSerie, codItem);
		}

		public static string GetPainel(string NumSerie, string codItem)
		{
			return daoGeral.GetPainel(NumSerie, codItem);
		}

		public static CBHorarioDTO ObterHorarioPorID(int id)
		{
			return daoGeral.ObterHorariosPorID(id);
		}

		public static List<CBPassagemDTO> RastrearPassagem(string NumSerie)
		{
			return daoGeral.RastrearPassagem(NumSerie);
		}

		public static List<CBPassagem_HistDTO> RastrearPassagemHist(string NumSerie)
		{
			return daoGeral.RastrearPassagemHist(NumSerie);
		}

		public static List<CBAmarraDTO> RastrearAmarracao(string NumSerie)
		{
			return daoGeral.RastrearAmarracao(NumSerie);
		}

		public static List<CBAmarra_HistDTO> RastrearAmarracaoHist(string NumSerie)
		{
			return daoGeral.RastrearAmarracaoHist(NumSerie);
		}

		public static List<CBDefeitoDTO> RastrearDefeito(string NumSerie)
		{
			return daoGeral.RastrearDefeito(NumSerie);
		}

		public static int RecuperarTurnoAtual(int codLinha)
		{
			return daoGeral.RecuperarTurnoAtual(codLinha);
		}

		public static List<CBEmbaladaDTO> RastrearEmbalagem(string NumSerie)
		{
			return daoGeral.RastrearEmbalagem(NumSerie);
		}

		public static CBEmbaladaDTO ObterUltimoEmbaladoLinha(int codLinha)
		{
			return daoGeral.ObterUltimoEmbaladoLinha(codLinha);
		}

		public static CBEmbaladaDTO ObterUltimoEmbaladoLinha(string codLinha)
		{
			int linha = 0;

			linha = ObterLinhaT1(codLinha).CodLinha;

			return daoGeral.ObterUltimoEmbaladoLinha(linha);
		}

		public static List<ResumoEmbaladoLinha> ObterResumoEmbaladoLinha(DateTime datInicio, DateTime datFim){
			return daoGeral.ObterResumoEmbaladoLinha(datInicio, datFim);
		}

		public static int GetSequenciaPorTipoAmarracao(int tipoAmarracao)
		{
			if (tipoAmarracao == Constantes.AMARRA_PRODUTO)
			{
				return SEMP.Model.Constantes.SEQ_PRODUTO;
			}
			else if (tipoAmarracao == Constantes.AMARRA_PLACA)
			{
				return SEMP.Model.Constantes.SEQ_PLACA;
			}
			else if (tipoAmarracao == Constantes.AMARRA_KIT)
			{
				return SEMP.Model.Constantes.SEQ_KIT_ACESSORIO;
			}
			else if (tipoAmarracao == Constantes.AMARRA_PAINEL || tipoAmarracao == Constantes.AMARRA_PAINEL_LCD)
			{
				return SEMP.Model.Constantes.SEQ_PAINEL;
			}
			else if (tipoAmarracao == Constantes.AMARRA_SUPORTE)
			{
				return SEMP.Model.Constantes.SEQ_SUPORTE;
			}
			else if (tipoAmarracao == Constantes.AMARRA_PEDESTAL)
			{
				return SEMP.Model.Constantes.SEQ_PEDESTAL;
			}
			else if (tipoAmarracao == Constantes.AMARRA_BASE)
			{
				return SEMP.Model.Constantes.SEQ_BASE;
			}
			else if (tipoAmarracao == Constantes.AMARRA_EAN)
			{
				return SEMP.Model.Constantes.SEQ_CAIXA;
			}
			else if (tipoAmarracao == Constantes.AMARRA_FONTE)
			{
				return SEMP.Model.Constantes.SEQ_FONTE;
			}
			else if (tipoAmarracao == Constantes.AMARRA_ESPACADOR)
			{
				return SEMP.Model.Constantes.SEQ_Espacador;
			}
			else if (tipoAmarracao == Constantes.AMARRA_OCULOS)
			{
				return SEMP.Model.Constantes.SEQ_Oculos;
			}

			else
			{ return 1; }
		}

		public static UsuarioDTO ObterUsuario(string codFab, string nomUsuario)
		{
			return daoGeral.ObterUsuario(codFab, nomUsuario);
		}

		public static bool EnviarEmail(string de, string deNome, string para, string assunto, string mensagem,
			string cc = "")
		{
			return daoGeral.EnviarEmail(de, deNome, para, assunto, mensagem, cc);
		}

        public static string EnviarEmail(string de, string deNome, string para, string assunto, string mensagem,
            string cc = "", bool retornaErro = true)
        {
            return daoGeral.EnviarEmail(de, deNome, para, assunto, mensagem, cc, retornaErro);
        }

        public static ProgDiaJitDTO ObterPrograma(string codLinha, DateTime datProducao, string codModelo)
		{
			return daoGeral.ObterPrograma(codLinha, datProducao, codModelo);
		}

		public static ProdDiaDTO ObterProduzido(string codLinha, DateTime datProducao, string codModelo)
		{
			return daoGeral.ObterProduzido(codLinha, datProducao, codModelo);
		}

		public static int ObterIDPostoGargalo(int codLinha)
		{
			return daoGeral.ObterIDPostoGargalo(codLinha);
		}


		public static string VerificarAPDestino(string codFab, string numAP)
		{
			return daoGeral.VerificarAPDestino(codFab, numAP);
		}


		public static List<ClassesAuxiliaresEmbalagem.ResultProcGetApsModelo> GetApsModelo(string codProcesso, string codModelo)
		{
			return daoGeral.GetApsModelo(codProcesso, codModelo);


		}

		public static String ObterCodModeloIAC(string CodModelo)
		{
			return daoGeral.ObterCodModeloIAC(CodModelo);
		}

		public static String ObterCodModeloSMD(string CodModelo)
		{
			return daoGeral.ObterCodModeloSMD(CodModelo);
		}

		public static String ObterCodModeloIMC(string CodModelo)
		{
			return daoGeral.ObterCodModeloIMC(CodModelo);
		}

		public static String ObterCodModeloIAC_IMC(string CodModelo)
		{
			return daoGeral.ObterCodModeloIAC_IMC(CodModelo);
		}

		public static String ObterCodModeloIAC_IAC(string CodModelo)
		{
			return daoGeral.ObterCodModeloIAC_IAC(CodModelo);
		}


		public static List<ItensRastreaveisAP> ObterItensRastreaveis(string codFab, string numAP)
		{
			return daoGeral.ObterItensRastreaveis(codFab, numAP);
		}

		public static DateTime ObterUltimaDataProducao()
		{

			return daoGeral.ObterUltimaDataProducao();

		}

		public static List<ResumoEmbalada> ObterResumoEmbalagem(DateTime datReferencia, string fabrica)
		{
			return daoGeral.ObterResumoEmbalagem(datReferencia, fabrica);
		}

		public static List<ResumoModelo> ObterResumoModelo(DateTime datInicio, DateTime datFim)
		{
			return daoGeral.ObterResumoModelo(datInicio, datFim);
		}

		public static List<VelocidadeLinha> ObterVelocidades(){

			return daoGeral.ObterVelocidades();
		}

		public static BarrasProducaoDTO ObterProdutoMilhao(){
			return daoGeral.ObterProdutoMilhao();

		}

		public static List<LinhaDTO> ObterLinhas(string descricao, int? pagina = 1)
		{
			return daoGeral.ObterLinhas(descricao, pagina);
		}

		public static List<CBLinhaDTO> ObterLinhasCB()
		{
			return daoGeral.ObterLinhasCB();
		}

		public static List<ResumoContingencia> ObterResumoContingencia(DateTime datInicio, DateTime datFim) {

			return daoGeral.ObterResumoContingencia(datInicio, datFim);

		}

		public static List<CBDestinatarioEmailDTO> ObterDestinatariosEmail(string codRotina){
			return daoGeral.ObterDestinatariosEmail(codRotina);
		}

		#region AcessoMAO
		public static List<AcessoMaoDTO> ObterAcessos(){
			return daoGeral.ObterAcessos();
		}

		public static List<AcessoMaoDTO> ObterAcessosCodigo(string codigo)
		{
			return daoGeral.ObterAcessosCodigo(codigo);
		}

		public static List<AcessoMaoDTO> ObterAcessosAcesso(string acesso)
		{
			return daoGeral.ObterAcessosAcesso(acesso);
		}

		public static bool ObterAcessosBarApontamento()
		{
			return daoGeral.ObterAcessosBarApontamento();
		}

		public static bool SetarAcessosBarApontamento(string acesso)
		{
			return daoGeral.SetarAcessosBarApontamento(acesso);
		}

		#endregion

	}


}
