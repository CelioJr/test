namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.hdtmodel")]
    public partial class hdtmodel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public hdtmodel()
        {
            hdtmodelrange = new HashSet<hdtmodelrange>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id { get; set; }

        [StringLength(255)]
        public string description { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? lastModified { get; set; }

        [StringLength(255)]
        public string modelNick { get; set; }

        public int option1 { get; set; }

        public int option2 { get; set; }

        public int option3 { get; set; }

        [StringLength(255)]
        public string pathImei { get; set; }

        [StringLength(255)]
        public string pathMac { get; set; }

        [StringLength(255)]
        public string userNameModified { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<hdtmodelrange> hdtmodelrange { get; set; }
    }
}
