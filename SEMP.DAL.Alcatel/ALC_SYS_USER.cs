namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_SYS_USER")]
    public partial class ALC_SYS_USER
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ALC_SYS_USER()
        {
            ALC_INVENTORY = new HashSet<ALC_INVENTORY>();
            ALC_LOT_LIST = new HashSet<ALC_LOT_LIST>();
            ALC_LOTE_LIST = new HashSet<ALC_LOTE_LIST>();
            ALC_OQC_INSPECTION = new HashSet<ALC_OQC_INSPECTION>();
            ALC_ORDER = new HashSet<ALC_ORDER>();
            ALC_PRODUCTION_COUNTER = new HashSet<ALC_PRODUCTION_COUNTER>();
            ALC_REPAIR = new HashSet<ALC_REPAIR>();
            ALC_REPAIR1 = new HashSet<ALC_REPAIR>();
            ALC_SYS_LOG = new HashSet<ALC_SYS_LOG>();
            ALC_ROLE_USER = new HashSet<ALC_ROLE_USER>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int USER_ID { get; set; }

        public bool? enable { get; set; }

        [Required]
        [StringLength(255)]
        public string USER_LOGIN { get; set; }

        [Required]
        [StringLength(255)]
        public string userPassword { get; set; }

        [Required]
        [StringLength(255)]
        public string USER_NAME { get; set; }

        public int? USER_POST { get; set; }

        [StringLength(255)]
        public string USER_PASSWORD { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_INVENTORY> ALC_INVENTORY { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_LOT_LIST> ALC_LOT_LIST { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_LOTE_LIST> ALC_LOTE_LIST { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_OQC_INSPECTION> ALC_OQC_INSPECTION { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_ORDER> ALC_ORDER { get; set; }

        public virtual ALC_POST_USER ALC_POST_USER { get; set; }

        public virtual ALC_POST_USER ALC_POST_USER1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_PRODUCTION_COUNTER> ALC_PRODUCTION_COUNTER { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_REPAIR> ALC_REPAIR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_REPAIR> ALC_REPAIR1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_SYS_LOG> ALC_SYS_LOG { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_ROLE_USER> ALC_ROLE_USER { get; set; }
    }
}
