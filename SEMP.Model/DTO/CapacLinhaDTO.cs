using System;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
	public class CapacLinhaDTO
	{

		[Display(Name = "M�s referente")]
		public System.DateTime DatProducao { get; set; }

		[Display(Name = "Linha")]
		[Required(ErrorMessage = "Informe a linha")]
		public string CodLinha { get; set; }

		[Display(Name = "Linha")]
		public string DescricaoLinha { get; set; }

		[Display(Name = "Modelo")]
		[Required(ErrorMessage = "Informe o modelo")]
		public string CodModelo { get; set; }

		[Display(Name = "Modelo")]
		public string DescricaoModelo { get; set; }

		[Display(Name = "Capac. por Linha")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? QtdCapacLinha { get; set; }

		[Display(Name = "Capac. m�xima")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? QtdCapacMaxima { get; set; }

		public string Usuario { get; set; }

		public System.Guid rowguid { get; set; }

		[Display(Name = "Turno")]
		[Required(ErrorMessage = "Informe o turno")]
		public int NumTurno { get; set; }

		public bool? PostoEntrada { get; set; }
	}
}
