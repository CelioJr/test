using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_BarrasProducaoItemMap : EntityTypeConfiguration<tbl_BarrasProducaoItem>
    {
        public tbl_BarrasProducaoItemMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodItem, t.NumSerieItem });

            // Properties
            this.Property(t => t.CodModelo)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumSerieModelo)
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.CodItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumSerieItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.CodTipoItem)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_BarrasProducaoItem");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.NumSerieModelo).HasColumnName("NumSerieModelo");
            this.Property(t => t.DatLeitura).HasColumnName("DatLeitura");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.NumSerieItem).HasColumnName("NumSerieItem");
            this.Property(t => t.CodTipoItem).HasColumnName("CodTipoItem");
        }
    }
}
