using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DTB_CB.Models.Mapping
{
    public class tbl_CBAmarraMap : EntityTypeConfiguration<tbl_CBAmarra>
    {
        public tbl_CBAmarraMap()
        {
            // Primary Key
            this.HasKey(t => new { t.NumSerie, t.NumECB, t.CodModelo });

            // Properties
            this.Property(t => t.NumSerie)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.NumECB)
                .IsRequired()
                .HasMaxLength(40);

            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6);

            //this.Property(t => t.CodFab)
            //    .IsFixedLength()
            //    .HasMaxLength(2);

            //this.Property(t => t.NumAP)
            //    .IsFixedLength()
            //    .HasMaxLength(6);

            //this.Property(t => t.CodItem)
            //    .IsFixedLength()
            //    .HasMaxLength(6);

            // Table & Column Mappings
            this.ToTable("tbl_CBAmarra");
            this.Property(t => t.NumSerie).HasColumnName("NumSerie");
            this.Property(t => t.NumECB).HasColumnName("NumECB");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.FlgTipo).HasColumnName("FlgTipo");
            this.Property(t => t.DatAmarra).HasColumnName("DatAmarra");
            //this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            //this.Property(t => t.NumPosto).HasColumnName("NumPosto");
            //this.Property(t => t.CodFab).HasColumnName("CodFab");
            //this.Property(t => t.NumAP).HasColumnName("NumAP");
            //this.Property(t => t.CodItem).HasColumnName("CodItem");
        }
    }
}
