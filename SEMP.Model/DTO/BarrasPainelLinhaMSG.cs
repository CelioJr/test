﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.DTO
{
	public class BarrasPainelLinhaMSG
	{
		public string Saudacao { get; set; }
		public string Mensagem { get; set; }
		public string CodLinha { get; set; }
	}
}
