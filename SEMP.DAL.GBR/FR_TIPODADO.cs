namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_TIPODADO
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TPD_CODIGO { get; set; }

        [Required]
        [StringLength(30)]
        public string TPD_DESCRICAO { get; set; }

        [StringLength(254)]
        public string TPD_MASCARAFORMATACAO { get; set; }

        [StringLength(254)]
        public string TPD_MASCARAEDICAO { get; set; }
    }
}
