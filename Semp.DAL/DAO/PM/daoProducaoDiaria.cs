﻿using System;
using System.Collections.Generic;
using System.Linq;
using SEMP.DAL.Models;
using SEMP.Model.DTO;
using SEMP.Model.VO.PM;
using SEMP.Model;

namespace SEMP.DAL.DAO.PM
{
    public class daoProducaoDiaria
	{
        //static daoProducaoDiaria()
        //{
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<tbl_ProgDiaJit, ProgDiaJitDTO>();
        //        cfg.CreateMap<tbl_ProgMesVPE, ProgMesVPEDTO>();
        //        cfg.CreateMap<tbl_ProgDiaPlaca, ProgDiaPlacaDTO>();
        //    });
        //}

        #region MF
        public static List<ProgramaDiario> ObterProdutos(DateTime data)
		{
			using (var dtbSim = new DTB_SIMContext())
			{

				var inicio = data.AddDays(-data.Day).AddDays(1);
				var fim = inicio.AddMonths(1).AddDays(-1);

				#region MF
				var dados = (from p in dtbSim.tbl_ProgDiaJit
							 join item in dtbSim.tbl_Item on p.CodModelo equals item.CodItem
							 from fam in dtbSim.tbl_IAMCFamilia.Where(x => item.CodFam == x.CodFam).DefaultIfEmpty()
							 //from vpe in dtbSim.tbl_ProgMesVPE.Where(v => p.CodModelo == v.CodModelo && v.DatProducao == inicio).DefaultIfEmpty()
							 from resumo in dtbSim.tbl_ProgMesResumo.Where(v => p.CodModelo == v.CodModelo && v.DatProducao == inicio).DefaultIfEmpty()
							 where p.DatProducao >= inicio && p.DatProducao <= fim && (p.CodFab == Constantes.CodFab02 || p.CodFab == Constantes.CodFab20) && p.FlagTP != "D"
							 group p by new { p.CodModelo, item.DesItem, item.CodFam, fam.NomFam, fam.NumOrdem, QtdResumo = resumo.QtdProdPm/*, vpe.QtdProdPm*/ }
								 into grupo
								 select new ProgramaDiario()
								 {
									 CodModelo = grupo.Key.CodModelo,
                                     DesModelo = grupo.Key.DesItem,
									 CodFam = grupo.Key.CodFam,
									 NomFam = grupo.Key.NomFam ?? "OUTROS",
									 //QtdVPE = grupo.Key.QtdProdPm,
									 QtdPP = grupo.Key.QtdResumo ?? grupo.Sum(x => x.QtdProducao ?? x.QtdProdPm),
									 Ordem = grupo.Key.NumOrdem ?? 99
								 }).ToList();

				var dadosVPE = (from vpe in dtbSim.tbl_ProgMesVPE
							 join item in dtbSim.tbl_Item on vpe.CodModelo equals item.CodItem
							 from fam in dtbSim.tbl_IAMCFamilia.Where(x => item.CodFam == x.CodFam).DefaultIfEmpty()
							 from resumo in dtbSim.tbl_ProgMesResumo.Where(v => vpe.CodModelo == v.CodModelo && v.DatProducao == inicio).DefaultIfEmpty()
							 where vpe.DatProducao == inicio //&& p == null
							 group vpe by new { vpe.CodModelo, item.DesItem, item.CodFam, fam.NomFam, fam.NumOrdem, QtdResumo = resumo.QtdProdPm}
								 into grupo
								 select new ProgramaDiario()
								 {
									 CodModelo = grupo.Key.CodModelo,
									 DesModelo = grupo.Key.DesItem,
									 CodFam = grupo.Key.CodFam,
									 NomFam = grupo.Key.NomFam ?? "OUTROS",
									 QtdVPE = grupo.Sum(x => x.QtdProdPm),
									 QtdPP = grupo.Key.QtdResumo ?? 0,
									 Ordem = grupo.Key.NumOrdem ?? 99
								 }).ToList();

                foreach (var item in dadosVPE)
                {
                    var dado = dados.FirstOrDefault(x => x.CodModelo == item.CodModelo);
                    if (dado != null)
                    {
                        dado.QtdVPE = item.QtdVPE;
                    }
                }

                var dadosNovos = dadosVPE.Where(x => !dados.Select(z => z.CodModelo).Distinct().ToList().Contains(x.CodModelo)).ToList();

				dados.AddRange(dadosNovos);

				#endregion

				return dados;
			}
		}

		public static List<ProgDiaJitDTO> ObterProgramaDia(DateTime data)
		{
			using (var dtbSim = new DTB_SIMContext())
			{

				var programaQ = (from p in dtbSim.tbl_ProgDiaJit
								 where p.DatProducao == data && (p.CodFab == Constantes.CodFab02 || p.CodFab == Constantes.CodFab20) && p.FlagTP != "D"
								 select p).ToList();

				var programaDTO = new List<ProgDiaJitDTO>();

				AutoMapper.Mapper.Map(programaQ, programaDTO);

				return programaDTO;
			}
		}

		public static List<ProgDiaJitDTO> ObterProgramaDia(DateTime datInicio, DateTime datFim)
		{
			using (var dtbSim = new DTB_SIMContext())
			{

				var programaQ = (from p in dtbSim.tbl_ProgDiaJit
								 where p.DatProducao >= datInicio && p.DatProducao <= datFim && (p.CodFab == Constantes.CodFab02 || p.CodFab == Constantes.CodFab20) && p.FlagTP != "D"
								 select new ProgDiaJitDTO()
								 {
									 CodConjCin = p.CodConjCin,
									 CodConjFly = p.CodConjFly,
									 CodFab = p.CodFab,
									 CodLinha = p.CodLinha,
									 CodModelo = p.CodModelo,
									 DatEstudo = p.DatEstudo,
									 DatProducao = p.DatProducao,
									 FlagTP = p.FlagTP,
									 NumTurno = p.NumTurno,
									 Observacao = p.Observacao,
									 QtdProdPm = p.QtdProdPm,
									 QtdProducao = p.QtdProducao,
									 Usuario = p.Usuario,
									 flgCobertura = p.flgCobertura,
									 rowguid = p.rowguid
								 }).ToList();

				return programaQ;
			}
		}

		public static List<ProgramaDiario> ObterProducaoDia(DateTime data)
		{
			using (var dtbSim = new DTB_SIMContext())
			{

				var inicio = data.AddDays(-data.Day).AddDays(1);

				var produzidoQ = (from p in dtbSim.tbl_ProdDia
								  join item in dtbSim.tbl_Item on p.CodModelo equals item.CodItem
								  from fam in dtbSim.tbl_IAMCFamilia.Where(x => item.CodFam == x.CodFam).DefaultIfEmpty()
								  from vpe in dtbSim.tbl_ProgMesVPE.Where(v => p.CodModelo == v.CodModelo && v.DatProducao == inicio).DefaultIfEmpty()
								  where p.DatProducao == data && (p.CodFab == Constantes.CodFab02 || p.CodFab == Constantes.CodFab20) && p.FlagTP != "D"
								  group p by new { p.CodModelo, item.DesItem, vpe.QtdProdPm, item.CodFam, fam.NomFam, fam.NumOrdem } into grupo
								  select new ProgramaDiario()
								  {
									  CodModelo = grupo.Key.CodModelo,
									  DesModelo = grupo.Key.DesItem,
									  CodFam = grupo.Key.CodFam,
									  NomFam = grupo.Key.NomFam ?? "OUTRO",
									  QtdVPE = grupo.Key.QtdProdPm,
									  QtdProdDia = grupo.Sum(x => x.QtdProduzida),
									  Ordem = grupo.Key.NumOrdem ?? 99
								  });

				return produzidoQ.ToList();
			}
		}

		public static List<ProgramaDiario> ObterApontadoDia(DateTime data)
		{
			using (var dtbSim = new DTB_SIMContext())
			{

				var inicio = data.AddDays(-data.Day).AddDays(1);

				var produzidoQ = (from p in dtbSim.tbl_ProdDia
								  join item in dtbSim.tbl_Item on p.CodModelo equals item.CodItem
								  from fam in dtbSim.tbl_IAMCFamilia.Where(x => item.CodFam == x.CodFam).DefaultIfEmpty()
								  from vpe in dtbSim.tbl_ProgMesVPE.Where(v => p.CodModelo == v.CodModelo && v.DatProducao == inicio).DefaultIfEmpty()
								  where p.DatProducao == data && (p.CodFab == Constantes.CodFab02 || p.CodFab == Constantes.CodFab20) && p.FlagTP != "D"
								  group p by new { p.CodModelo, item.DesItem, vpe.QtdProdPm, item.CodFam, fam.NomFam, fam.NumOrdem } into grupo
								  select new ProgramaDiario()
								  {
									  CodModelo = grupo.Key.CodModelo,
									  DesModelo = grupo.Key.DesItem,
									  CodFam = grupo.Key.CodFam,
									  NomFam = grupo.Key.NomFam ?? "OUTRO",
									  QtdVPE = grupo.Key.QtdProdPm,
									  QtdProdDia = grupo.Sum(x => x.QtdApontada),
									  Ordem = grupo.Key.NumOrdem ?? 99
								  });

				return produzidoQ.ToList();
			}
		}

		public static List<ProgramaDiario> ObterProducaoAcumulada(DateTime data)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var inicio = data.AddDays(-data.Day).AddDays(1);
				var fim = inicio.AddMonths(1).AddDays(-1);

				var produzido = (from p in dtbSim.tbl_ProdDia
								 where p.DatProducao >= inicio && p.DatProducao <= data && p.FlagTP != "D"
								 group p by p.CodModelo into grupo
								 select new ProgramaDiario()
								 {
									 CodModelo = grupo.Key,
									 QtdAcumulada = grupo.Sum(x => x.QtdApontada)
								 });

				return produzido.ToList();

			}
		}

		public static List<ProgMesVPEDTO> ObterProgVPE(DateTime data)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var progVPE = (from p in dtbSim.tbl_ProgMesVPE
							   where p.DatProducao == data
							   select p).ToList();

				var programaDTO = new List<ProgMesVPEDTO>();

				AutoMapper.Mapper.Map(progVPE, programaDTO);

				return programaDTO;
			}
		}
		#endregion

		#region Placas
		public static List<ProgramaDiario> ObterProdutosPlaca(DateTime data, string fase)
		{
			using (var dtbSim = new DTB_SIMContext())
			{

				var inicio = data.AddDays(-data.Day).AddDays(1);
				var fim = inicio.AddMonths(1).AddDays(-1);
				//var tipo = fase.ToUpper().Equals("I") ? "1" : "2";
				var setor = fase.ToUpper().Equals("I") ? "IAC" : "IMC";

				var dados = (from p in dtbSim.tbl_ProgDiaPlaca
							 join item in dtbSim.tbl_Item on p.CodPlaca equals item.CodItem
							 join linha in dtbSim.tbl_BarrasLinha.Where(x => x.Setor == setor) on p.CodLinha equals	linha.CodLinha
							 //join processo in dtbSim.tbl_ProgProcesso on p.CodProcesso equals processo.CodProcesso
							 from modelo in dtbSim.tbl_ProdModelo.Where(v => p.CodPlaca == v.CodModelo).DefaultIfEmpty()
							 where p.DatProducao >= inicio && p.DatProducao <= fim && (p.CodFab == Constantes.CodFab02 || p.CodFab == Constantes.CodFab20 || p.CodFab == Constantes.CodFab21) 
										//&& (processo.CodTipo.Equals(fase) || processo.CodTipo.Equals(tipo))// && !p.CodLinha.Equals("DAT")
							 group p by new { p.CodModelo, item.DesItem, modelo.DesResumida, linha.Produto }
								 into grupo
								 select new ProgramaDiario()
								 {
									 CodModelo = grupo.Key.CodModelo,
									 DesModelo = grupo.Key.DesResumida ?? grupo.Key.DesItem,
									 CodFam = grupo.Key.Produto ?? "OUTRO",
									 NomFam = grupo.Key.Produto ?? "OUTRO",
									 QtdVPE = 0,
									 QtdPP = grupo.Sum(x => x.QtdProducao ?? x.QtdProdPM)
								 });
				var dados2 = dados.ToList();
				return dados2;
			}
		}

		public static List<ProgDiaPlacaDTO> ObterProgramaDiaPlaca(DateTime data, string fase)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				//var tipo = fase.ToUpper().Equals("I") ? "1" : "2";
				var setor = fase.ToUpper().Equals("I") ? "IAC" : "IMC";

				var programaQ = (from p in dtbSim.tbl_ProgDiaPlaca
								 join linha in dtbSim.tbl_BarrasLinha.Where(x => x.Setor == setor) on p.CodLinha equals linha.CodLinha
								 //join processo in dtbSim.tbl_ProgProcesso on p.CodProcesso equals processo.CodProcesso
								 where p.DatProducao == data && (p.CodFab == Constantes.CodFab02 || p.CodFab == Constantes.CodFab20 || p.CodFab == Constantes.CodFab21) 
										//&& (processo.CodTipo.Equals(fase) || processo.CodTipo.Equals(tipo))
								 select p).ToList();

				var programaDTO = new List<ProgDiaPlacaDTO>();

				AutoMapper.Mapper.Map(programaQ, programaDTO);

				return programaDTO;
			}
		}

		public static List<ProgramaDiario> ObterProducaoDiaPlaca(DateTime data, string fase)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				//var tipo = fase.ToUpper().Equals("I") ? "1" : "2";
				var setor = fase.ToUpper().Equals("I") ? "IAC" : "IMC";

				var produzidoQ = (from p in dtbSim.tbl_ProdDiaPlaca
								  join item in dtbSim.tbl_Item on p.CodPlaca equals item.CodItem
								  join linha in dtbSim.tbl_BarrasLinha.Where(x => x.Setor == setor) on p.CodLinha equals linha.CodLinhaRel
								  //join processo in dtbSim.tbl_ProgProcesso on p.CodProcesso equals processo.CodProcesso
								  where p.DatProducao == data && (p.CodFab == Constantes.CodFab02 || p.CodFab == Constantes.CodFab20 || p.CodFab == Constantes.CodFab21)
										//(processo.CodTipo.Equals(fase) || processo.CodTipo.Equals(tipo))
										&& !p.CodLinha.StartsWith("DAT")
								  group p by new { p.CodPlaca, item.DesItem, linha.Produto } into grupo
								  select new ProgramaDiario()
								  {
									  CodModelo = grupo.Key.CodPlaca,
									  DesModelo = grupo.Key.DesItem,
									  CodFam = grupo.Key.Produto ?? "OUTRO",
									  NomFam = grupo.Key.Produto ?? "OUTRO",
									  QtdVPE = 0,
									  QtdProdDia = grupo.Sum(x => x.QtdProduzida)
								  });

				return produzidoQ.ToList();
			}
		}

		public static List<ProgramaDiario> ObterApontadoDiaPlaca(DateTime data, string fase)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				//var tipo = fase.ToUpper().Equals("I") ? "1" : "2";
				var setor = fase.ToUpper().Equals("I") ? "IAC" : "IMC";

				var produzidoQ = (from p in dtbSim.tbl_ProdDiaPlaca
								  join item in dtbSim.tbl_Item on p.CodPlaca equals item.CodItem
								  join linha in dtbSim.tbl_BarrasLinha.Where(x => x.Setor == setor) on p.CodLinha equals linha.CodLinhaRel
								  //join processo in dtbSim.tbl_ProgProcesso on p.CodProcesso equals processo.CodProcesso
								  where p.DatProducao == data && (p.CodFab == Constantes.CodFab02 || p.CodFab == Constantes.CodFab20 || p.CodFab == Constantes.CodFab21)
								  //(processo.CodTipo.Equals(fase) || processo.CodTipo.Equals(tipo))
								  group p by new { p.CodPlaca, item.DesItem, linha.Produto } into grupo
								  select new ProgramaDiario()
								  {
									  CodModelo = grupo.Key.CodPlaca,
									  DesModelo = grupo.Key.DesItem,
									  CodFam = grupo.Key.Produto ?? "OUTRO",
									  NomFam = grupo.Key.Produto ?? "OUTRO",
									  QtdVPE = 0,
									  QtdProdDia = grupo.Sum(x => x.QtdApontada)
								  });

				return produzidoQ.ToList();
			}
		}

		public static List<ProgramaDiario> ObterProducaoAcumuladaPlaca(DateTime data, string fase)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var inicio = data.AddDays(-data.Day).AddDays(1);
				var fim = inicio.AddMonths(1).AddDays(-1);

				var produzido = (from p in dtbSim.tbl_ProdDiaPlaca
								 where p.DatProducao >= inicio && p.DatProducao <= fim && !p.CodLinha.StartsWith("DAT")
								 group p by p.CodPlaca into grupo
								 select new ProgramaDiario()
								 {
									 CodModelo = grupo.Key,
									 QtdAcumulada = grupo.Sum(x => x.QtdProduzida)
								 });

				return produzido.ToList();

			}
		}

		public static List<ProgramaDiario> ObterApontadoAcumuladaPlaca(DateTime data, string fase)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var inicio = data.AddDays(-data.Day).AddDays(1);
				var fim = inicio.AddMonths(1).AddDays(-1);

				var produzido = (from p in dtbSim.tbl_ProdDiaPlaca
								 where p.DatProducao >= inicio && p.DatProducao <= fim
								 group p by p.CodPlaca into grupo
								 select new ProgramaDiario()
								 {
									 CodModelo = grupo.Key,
									 QtdAcumulada = grupo.Sum(x => x.QtdApontada)
								 });

				return produzido.ToList();

			}
		}

		#endregion

		#region Comum
		public static List<ProgramaDiario> ObterDefeitosDia(DateTime data, string Fase)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				/*var dFim = data.AddDays(1).AddMinutes(-1);
				data = data.AddHours(6);

				var defeitos = (from d in dtbCb.tbl_CBDefeito
								where d.DatEvento >= data && d.DatEvento <= dFim && String.IsNullOrEmpty(d.NumSeriePai) &&
										!d.CodCausa.StartsWith("WX")
										&& d.FlgRevisado < 2
								group d by d.NumECB.Substring(0, 6) into grupo
								select new ProgramaDiario()
								{
									CodModelo = grupo.Key,
									QtdDefeitos = grupo.Count()
								});*/
				var defeitos = (from d in dtbCb.viw_CBDefeitoTurno
								join l in dtbCb.tbl_CBLinha on d.CodLinha equals l.CodLinha
								where d.DatReferencia == data 
										&& String.IsNullOrEmpty(d.NumSeriePai) 
										&& !d.CodCausa.StartsWith("WX")
										&& d.FlgRevisado < 2
										&& l.Setor == Fase
								group d by d.NumECB.Substring(0, 6) into grupo
								select new ProgramaDiario()
								{
									CodModelo = grupo.Key,
									QtdDefeitos = grupo.Count()
								});
				var resultado = defeitos.ToList();
				return resultado;
			}

		}

		public static List<ProgramaDiario> ObterObservacoesDia(DateTime data)
		{
			using (var dtbSim = new DTB_SIMContext())
			{

				var obs = (from d in dtbSim.tbl_IAMCDiferenca
						   where d.DatProducao == data
						   select new ProgramaDiario()
						   {
							   CodModelo = d.CodModelo,
							   Observacao = d.MotivoDiferenca
						   }).ToList();
				return obs;
			}

		}
		#endregion

		#region DAT

		public static List<ProgramaDiario> ObterDadosDAT(DateTime data)
		{
			using (var dtbSim = new DTB_SIMContext())
			{

				var inicio = data.AddDays(-data.Day).AddDays(1);
				var fim = inicio.AddMonths(1).AddDays(-1);

				#region DAT
				var dados = (from p in dtbSim.tbl_ProgDiaPlaca
						   join item in dtbSim.tbl_Item on p.CodPlaca equals item.CodItem
							 where p.DatProducao >= inicio && p.DatProducao <= fim && (p.CodFab == Constantes.CodFab02 || p.CodFab == Constantes.CodFab20 || p.CodFab == Constantes.CodFab21) && p.FlagTP == "D"
						   group p by new { p.CodPlaca, item.DesItem, item.CodFam }
							   into grupo
							   select new ProgramaDiario()
							   {
								   CodModelo = grupo.Key.CodPlaca,
								   DesModelo = grupo.Key.DesItem,
								   CodFam = "DAT",
								   NomFam = "DAT",
								   QtdVPE = 0,
								   QtdPP = grupo.Sum(x => x.QtdProducao ?? x.QtdProdPM),
								   Ordem = 99
							   }).ToList();

				#endregion

				return dados;
			}

			#region Modo antigo
			/*using (var dtbCB = new SEMP.DTB_CB.Models.DTB_CBContext())
			{
				var dIni = data.AddDays(-data.Day).AddDays(1);
				var dFim = dIni.AddMonths(1).AddDays(-1);

				var dados = (from p in dtbCB.tbl_CBProdDiaDAT
							 where p.DatProducao >= dIni && p.DatProducao <= dFim
							 group p by p.CodModelo into grupo
							 select new ProgramaDiario()
							 {
								 CodFam = "DAT",
								 NomFam = "DAT",
								 CodModelo = grupo.Key,
								 QtdPP = grupo.Sum(x => x.QtdProgramada),
								 QtdVPE = 0
							 }).ToList();

				// atualizar as descrições dos modelos
				dados.ForEach(x => x.DesModelo = daoGeral.ObterDescricaoItem(x.CodModelo));

				return dados;
			}*/
			#endregion
		}

		public static List<ProgramaDiario> ObterProgramaDiaDAT(DateTime data)
		{
			using (var dtbSim = new DTB_SIMContext())
			{

				var programaQ = (from p in dtbSim.tbl_ProgDiaPlaca
								 where p.DatProducao == data && (p.CodFab == Constantes.CodFab02 || p.CodFab == Constantes.CodFab20 || p.CodFab == Constantes.CodFab21) && p.FlagTP == "D"
								 select new ProgramaDiario()
								 {
									 CodFam = "DAT",
									 NomFam = "DAT",
									 CodModelo = p.CodPlaca,
									 QtdProgDia = p.QtdProdPM,
									 QtdVPE = 0
								 }).ToList();


				return programaQ;
			}

			#region Desuso
			/*using (var dtbCB = new SEMP.DTB_CB.Models.DTB_CBContext())
			{

				var dados = (from p in dtbCB.tbl_CBProdDiaDAT
							 where p.DatProducao == data
							 group p by p.CodModelo into grupo
							 select new ProgramaDiario()
							 {
								 CodFam = "DAT",
								 NomFam = "DAT",
								 CodModelo = grupo.Key,
								 QtdProgDia = grupo.Sum(x => x.QtdProgramada),
								 QtdVPE = 0
							 }).ToList();

				// atualizar as descrições dos modelos
				dados.ForEach(x => x.DesModelo = daoGeral.ObterDescricaoItem(x.CodModelo));

				return dados;
			}*/
			#endregion

		}

		public static List<ProgramaDiario> ObterProdAcumuladaDAT(DateTime data)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var inicio = data.AddDays(-data.Day).AddDays(1);
				var fim = inicio.AddMonths(1).AddDays(-1);

				var produzido = (from p in dtbSim.tbl_ProdDiaPlaca
								 where p.DatProducao >= inicio && p.DatProducao <= data && p.FlagTP == "D"
								 group p by p.CodPlaca into grupo
								 select new ProgramaDiario()
								 {
                                     CodFam="DAT",
                                     NomFam = "DAT",
									 CodModelo = grupo.Key,
                                     QtdVPE = 0,
                                     QtdPP = 0,
									 QtdAcumulada = grupo.Sum(x => x.QtdApontada)
								 }).ToList();

				return produzido;

			}

			#region Desuso
			/*using (var dtbCB = new SEMP.DTB_CB.Models.DTB_CBContext())
			{
				var dIni = data.AddDays(-data.Day).AddDays(1);
				var dFim = dIni.AddMonths(1).AddDays(-1);

				var dados = (from l in dtbCB.tbl_CBLote
					join e in
						dtbCB.tbl_CBEmbalada.GroupBy(x => new {x.CodModelo, x.NumLote}).Select(x => new {x.Key.CodModelo, x.Key.NumLote})
						on l.NumLote equals e.NumLote.ToString()
					where (l.Status.Equals("STIAPRVOBA") || l.Status.Equals("Aprov.OBA")) &&
					      l.DatFechamento >= dIni &&
					      l.DatFechamento <= dFim
					group l by new {e.CodModelo}
					into grupo
					select new ProgramaDiario()
					{
						CodFam = "DAT",
						NomFam = "DAT",
						CodModelo = grupo.Key.CodModelo,
						QtdAcumulada = grupo.Sum(x => x.Qtde),
						QtdVPE = 0,
						QtdPP = 0
					}).ToList();

				// atualizar as descrições dos modelos
				dados.ForEach(x => x.DesModelo = daoGeral.ObterDescricaoItem(x.CodModelo));

				return dados;

			}*/
			#endregion
		}

		public static List<ProgramaDiario> ObterProduzidoDAT(DateTime data)
		{

			using (var dtbSim = new DTB_SIMContext())
			{

				var inicio = data.AddDays(-data.Day).AddDays(1);

				var produzidoQ = (from p in dtbSim.tbl_ProdDiaPlaca
								  join item in dtbSim.tbl_Item on p.CodPlaca equals item.CodItem
								  where p.DatProducao == data && (p.CodFab == Constantes.CodFab02 || p.CodFab == Constantes.CodFab21 || p.CodFab == Constantes.CodFab20) && p.FlagTP == "D"
								  group p by new { p.CodPlaca, item.DesItem, item.CodFam } into grupo
								  select new ProgramaDiario()
								  {
									  CodModelo = grupo.Key.CodPlaca,
									  DesModelo = grupo.Key.DesItem,
									  CodFam = "DAT",
									  NomFam = "DAT",
									  QtdVPE = 0,
									  QtdProdDia = grupo.Sum(x => x.QtdProduzida),
									  Ordem =  99
								  });

				return produzidoQ.ToList();
			}

			#region Desuso
			/*using (var dtbCB = new SEMP.DTB_CB.Models.DTB_CBContext())
			{
				var dFim = data.AddDays(1);

				var dados = (from l in dtbCB.tbl_CBLote
							 join e in dtbCB.tbl_CBEmbalada.GroupBy(x => new { x.CodModelo, x.NumLote })
															.Select(x => new { x.Key.CodModelo, x.Key.NumLote, QtdEmb = x.Count() }) on 
															l.NumLote equals e.NumLote.ToString()
							 where (l.Status.Equals("STIAPRVOBA") || l.Status.Equals("Aprov.OBA") || l.Status.Contains("FATURADO")) &&
									 l.DatFechamento >= data &&
									 l.DatFechamento <= dFim
							 group e by new { e.CodModelo } into grupo
							 select new ProgramaDiario()
							 {
								 CodFam = "DAT",
								 NomFam = "DAT",
								 CodModelo = grupo.Key.CodModelo,
								 QtdProdDia = grupo.Sum(x => x.QtdEmb)
							 });

				// atualizar as descrições dos modelos
				var dados2 = dados.ToList();
				dados2.ForEach(x => x.DesModelo = daoGeral.ObterDescricaoItem(x.CodModelo));

				return dados2;

			}*/
			#endregion
		}

		public static List<ProgramaDiario> ObterProgPlacaMesDAT(DateTime data)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var dIni = data.AddDays(-data.Day).AddDays(1);
				var dFim = dIni.AddMonths(1).AddDays(-1);

				var programaQ = (from p in dtbSim.tbl_ProgDiaPlaca
								 where p.DatProducao >= dIni && p.DatProducao <= dFim && (p.CodFab == Constantes.CodFab02 || p.CodFab == Constantes.CodFab20 || p.CodFab == Constantes.CodFab21) && p.FlagTP == "D"
								 group p by p.CodPlaca into grupo
								 select new ProgramaDiario()
								 {
									 CodFam = "DAT",
									 NomFam = "DAT",
									 CodModelo = grupo.Key,
									 QtdPP = grupo.Sum(x => x.QtdProducao ?? x.QtdProdPM),
									 QtdVPE = 0,
									 QtdAcumulada = 0,
									 QtdProdDia = 0,
									 QtdProgDia = 0
								 }).ToList();

				return programaQ;
			}
			#region Desuso
			/*using (var dtbSim = new DTB_SIMContext())
			{

				var dIni = data.AddDays(-data.Day).AddDays(1);
				var dFim = dIni.AddMonths(1).AddDays(-1);

				var dados = (from p in dtbSim.tbl_ProgDiaPlaca
							 where p.DatProducao >= dIni && p.DatProducao <= dFim && p.FlagTP == "D" &
									 (p.CodLinha == "111.20-0" || p.CodLinha == "111.16-0" || p.CodLinha == "111.17-0")
							 group p by p.CodPlaca into grupo
							 select new ProgramaDiario()
							 {
								 CodFam = "DAT",
								 NomFam = "DAT",
								 CodModelo = grupo.Key,
								 QtdPP = grupo.Sum(x => x.QtdProducao ?? x.QtdProdPM),
								 QtdVPE = 0,
								 QtdAcumulada = 0,
								 QtdProdDia = 0,
								 QtdProgDia = 0
							 }).ToList();
				
				// atualizar as descrições dos modelos
				dados.ForEach(x => x.DesModelo = daoGeral.ObterDescricaoItem(x.CodModelo));

				return dados;

			}*/
			#endregion
		}

		public static List<ProgramaDiario> ObterProdPlacaMesDAT(DateTime data)
		{

			using (var dtbSim = new DTB_SIMContext())
			{
				var inicio = data.AddDays(-data.Day).AddDays(1);
				var fim = inicio.AddMonths(1).AddDays(-1);

				var produzido = (from p in dtbSim.tbl_ProdDiaPlaca
								 where p.DatProducao >= inicio && p.DatProducao <= fim && p.FlagTP == "D"
								 group p by p.CodPlaca into grupo
								 select new ProgramaDiario()
								 {
                                     CodFam = "DAT",
                                     NomFam = "DAT",
                                     QtdVPE = 0,
                                     QtdPP = 0,
									 CodModelo = grupo.Key,
									 QtdAcumulada = grupo.Sum(x => x.QtdApontada)
								 });

				return produzido.ToList();

			}

			#region Desuso
			/*using (var dtbSim = new DTB_SIMContext())
			{

				var dIni = data.AddDays(-data.Day).AddDays(1);
				var dFim = dIni.AddMonths(1).AddDays(-1);

				var dados = (from p in dtbSim.tbl_ProdDiaPlaca
							 where p.DatProducao >= dIni && p.DatProducao <= dFim && p.FlagTP == "D" &
									 (p.CodLinha == "111.20-0" || p.CodLinha == "111.16-0" || p.CodLinha == "111.17-0")
							 group p by p.CodPlaca into grupo
							 select new ProgramaDiario()
							 {
								 CodFam = "DAT",
								 NomFam = "DAT",
								 CodModelo = grupo.Key,
								 QtdAcumulada = grupo.Sum(x => x.QtdApontada ?? x.QtdProduzida),
								 QtdVPE = 0,
								 QtdPP = 0,
								 QtdProdDia = 0,
								 QtdProgDia = 0
							 }).ToList();

				// atualizar as descrições dos modelos
				dados.ForEach(x => x.DesModelo = daoGeral.ObterDescricaoItem(x.CodModelo));

				return dados;

			}*/
			#endregion
		}

		public static List<ProgramaDiario> ObterProgPlacaDiaDAT(DateTime data)
		{
			using (var dtbSim = new DTB_SIMContext())
			{

				var dados = (from p in dtbSim.tbl_ProgDiaPlaca
							 where p.DatProducao == data && p.FlagTP == "D" &
									 (p.CodLinha == "111.20-0" || p.CodLinha == "111.16-0" || p.CodLinha == "111.17-0")
							 group p by p.CodPlaca into grupo
							 select new ProgramaDiario()
							 {
								 CodFam = "DAT",
								 NomFam = "DAT",
								 CodModelo = grupo.Key,
								 QtdProgDia = grupo.Sum(x => x.QtdProducao ?? x.QtdProdPM),
								 QtdVPE = 0,
								 QtdPP = 0,
								 QtdProdDia = 0,
								 QtdAcumulada = 0
							 }).ToList();

				return dados;

			}
		}

		public static List<ProgramaDiario> ObterProdPlacaDiaDAT(DateTime data)
		{
			using (var dtbSim = new DTB_SIMContext())
			{

				var dados = (from p in dtbSim.tbl_ProdDiaPlaca
							 where p.DatProducao == data && p.FlagTP == "D" &
									 (p.CodLinha == "111.20-0" || p.CodLinha == "111.16-0" || p.CodLinha == "111.17-0")
							 group p by p.CodPlaca into grupo
							 select new ProgramaDiario()
							 {
								 CodFam = "DAT",
								 NomFam = "DAT",
								 CodModelo = grupo.Key,
								 QtdProdDia = grupo.Sum(x => x.QtdApontada ?? x.QtdProduzida),
								 QtdVPE = 0,
								 QtdPP = 0,
								 QtdAcumulada = 0,
								 QtdProgDia = 0
							 }).ToList();

				return dados;

			}
		}

		public static List<ProgramaDiario> ObterDefeitosDiaDAT(DateTime data)
		{

			using (var dtbCb = new DTB_CBContext())
			{
				var dFim = data.AddDays(1).AddMinutes(-1);
				data = data.AddHours(7);
				var defeitos = (from d in dtbCb.tbl_CBDefeito
								where d.DatEvento >= data && d.DatEvento <= dFim &&
										d.CodCausa != "WX" && d.CodCausa != "WX00"
								group d by d.NumECB.Substring(0, 6) into grupo
								select new ProgramaDiario()
								{
                                    CodFam = "DAT",
                                    NomFam = "DAT",
									CodModelo = grupo.Key,
                                    QtdVPE = 0,
                                    QtdPP = 0,
									QtdDefeitos = grupo.Count()
								}).ToList();
				return defeitos;
			}

			#region Desuso
			/*using (var dtbCb = new SEMP.DTB_CB.Models.DTB_CBContext())
			{
				var dFim = data.AddDays(1).AddMinutes(-1);
				data = data.AddHours(7);
				var defeitos = (from d in dtbCb.tbl_CBDefeito
								where d.DatEvento >= data && d.DatEvento <= dFim &&
										d.CodCausa != "WX" && d.CodCausa != "WX00"
								group d by d.NumECB.Substring(0, 6) into grupo
								select new ProgramaDiario()
								{
									CodModelo = grupo.Key,
									QtdDefeitos = grupo.Count()
								}).ToList();
				return defeitos;
			}*/
			#endregion
		}
		#endregion

		#region DATTeste

		public static List<ProgramaDiario> ObterDadosDATTeste(DateTime data)
		{
			using (var dtbSim = new DTB_SIMContext())
			{

				var inicio = data.AddDays(-data.Day).AddDays(1);
				var fim = inicio.AddMonths(1).AddDays(-1);

				#region DAT
				var dados = (from p in dtbSim.tbl_ProgDiaPlaca
							 join item in dtbSim.tbl_Item on p.CodPlaca equals item.CodItem
							 where p.DatProducao >= inicio && p.DatProducao <= fim && (p.CodFab == Constantes.CodFab02 || p.CodFab == Constantes.CodFab20 || p.CodFab == Constantes.CodFab21) && p.CodLinha.StartsWith("DAT")
							 group p by new { p.CodPlaca, item.DesItem, item.CodFam }
								 into grupo
								 select new ProgramaDiario()
								 {
									 CodModelo = grupo.Key.CodPlaca,
									 DesModelo = grupo.Key.DesItem,
									 CodFam = "DAT",
									 NomFam = "DAT",
									 QtdVPE = 0,
									 QtdPP = grupo.Sum(x => x.QtdProducao ?? x.QtdProdPM),
									 Ordem = 99
								 }).ToList();

				#endregion

				return dados;
			}

		}

		public static List<ProgramaDiario> ObterProgramaDiaDATTeste(DateTime data)
		{
			using (var dtbSim = new DTB_SIMContext())
			{

				var programaQ = (from p in dtbSim.tbl_ProgDiaPlaca
								 join item in dtbSim.tbl_Item on p.CodPlaca equals item.CodItem
								 where p.DatProducao == data && (p.CodFab == Constantes.CodFab02 || p.CodFab == Constantes.CodFab20 || p.CodFab == Constantes.CodFab21) && p.CodLinha.StartsWith("DAT")
								 select new ProgramaDiario()
								 {
									 CodFam = "DAT",
									 NomFam = "DAT",
									 CodModelo = p.CodPlaca,
									 DesModelo = item.DesItem.Trim(),
									 QtdProgDia = p.QtdProdPM,
									 QtdVPE = 0
								 }).ToList();


				return programaQ;
			}

		}

		public static List<ProgramaDiario> ObterProdAcumuladaDATTeste(DateTime data)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var inicio = data.AddDays(-data.Day).AddDays(1);
				var fim = inicio.AddMonths(1).AddDays(-1);

				var produzido = (from p in dtbSim.tbl_ProdDiaPlaca
								 join item in dtbSim.tbl_Item on p.CodPlaca equals item.CodItem
								 where p.DatProducao >= inicio && p.DatProducao <= data && p.CodLinha.StartsWith("DAT") &&
								  (p.CodFab == Constantes.CodFab02 || p.CodFab == Constantes.CodFab20 || p.CodFab == Constantes.CodFab21) 
								 group p by new { p.CodPlaca, item.DesItem, item.CodFam } into grupo
								 select new ProgramaDiario()
								 {
									 CodFam = "DAT",
									 NomFam = "DAT",
									 CodModelo = grupo.Key.CodPlaca,
									 DesModelo = grupo.Key.DesItem,
									 QtdVPE = 0,
									 QtdPP = 0,
									 QtdAcumulada = grupo.Sum(x => x.QtdProduzida)
								 }).ToList();

				return produzido;

			}

		}

		public static List<ProgramaDiario> ObterProduzidoDATTeste(DateTime data)
		{

			using (var dtbSim = new DTB_SIMContext())
			{

				var inicio = data.AddDays(-data.Day).AddDays(1);

				var produzidoQ = (from p in dtbSim.tbl_ProdDiaPlaca
								  join item in dtbSim.tbl_Item on p.CodPlaca equals item.CodItem
								  where p.DatProducao == data && (p.CodFab == Constantes.CodFab02 || p.CodFab == Constantes.CodFab21 || p.CodFab == Constantes.CodFab20) && p.CodLinha.StartsWith("DAT")
								  group p by new { p.CodPlaca, item.DesItem, item.CodFam } into grupo
								  select new ProgramaDiario()
								  {
									  CodModelo = grupo.Key.CodPlaca,
									  DesModelo = grupo.Key.DesItem,
									  CodFam = "DAT",
									  NomFam = "DAT",
									  QtdVPE = 0,
									  QtdProdDia = grupo.Sum(x => x.QtdProduzida),
									  Ordem = 99
								  });

				return produzidoQ.ToList();
			}

		}

		public static List<ProgramaDiario> ObterProgPlacaMesDATTeste(DateTime data)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var dIni = data.AddDays(-data.Day).AddDays(1);
				var dFim = dIni.AddMonths(1).AddDays(-1);

				var programaQ = (from p in dtbSim.tbl_ProgDiaPlaca
								 join item in dtbSim.tbl_Item on p.CodPlaca equals item.CodItem
								 where p.DatProducao >= dIni && p.DatProducao <= dFim && (p.CodFab == Constantes.CodFab02 || p.CodFab == Constantes.CodFab20 || p.CodFab == Constantes.CodFab21) && p.CodLinha.StartsWith("DAT")
								 group p by new { p.CodPlaca, item.DesItem, item.CodFam } into grupo
								 select new ProgramaDiario()
								 {
									 CodFam = "DAT",
									 NomFam = "DAT",
									 CodModelo = grupo.Key.CodPlaca,
									 DesModelo = grupo.Key.DesItem,
									 QtdPP = grupo.Sum(x => x.QtdProducao ?? x.QtdProdPM),
									 QtdVPE = 0,
									 QtdAcumulada = 0,
									 QtdProdDia = 0,
									 QtdProgDia = 0
								 }).ToList();

				return programaQ;
			}
		}

		public static List<ProgramaDiario> ObterProdPlacaMesDATTeste(DateTime data)
		{

			using (var dtbSim = new DTB_SIMContext())
			{
				var inicio = data.AddDays(-data.Day).AddDays(1);
				var fim = inicio.AddMonths(1).AddDays(-1);

				var produzido = (from p in dtbSim.tbl_ProdDiaPlaca
								 join item in dtbSim.tbl_Item on p.CodPlaca equals item.CodItem
								 where p.DatProducao >= inicio && p.DatProducao <= fim && p.CodLinha.StartsWith("DAT")
								  && (p.CodFab == Constantes.CodFab02 || p.CodFab == Constantes.CodFab20 || p.CodFab == Constantes.CodFab21) 
								 group p by new { p.CodPlaca, item.DesItem, item.CodFam } into grupo
								 select new ProgramaDiario()
								 {
									 CodModelo = grupo.Key.CodPlaca,
									 DesModelo = grupo.Key.DesItem,
									 CodFam = "DAT",
									 NomFam = "DAT",
									 QtdVPE = 0,
									 QtdPP = 0,
									 QtdAcumulada = grupo.Sum(x => x.QtdProduzida)
								 });

				return produzido.ToList();

			}

		}

		public static List<ProgramaDiario> ObterDefeitosDiaDATTeste(DateTime data)
		{

			using (var dtbCb = new DTB_CBContext())
			{
				var dFim = data.AddDays(1).AddMinutes(-1);
				data = data.AddHours(7);
				var defeitos = (from d in dtbCb.tbl_CBDefeito
								join l in dtbCb.tbl_CBLinha on d.CodLinha equals l.CodLinha
								where d.DatEvento >= data && d.DatEvento <= dFim &&
										d.CodCausa != "WX" && d.CodCausa != "WX00" && l.CodLinhaT1.StartsWith("DAT")
								group d by d.NumECB.Substring(0, 6) into grupo
								select new ProgramaDiario()
								{
									CodFam = "DAT",
									NomFam = "DAT",
									CodModelo = grupo.Key,
									QtdVPE = 0,
									QtdPP = 0,
									QtdDefeitos = grupo.Count()
								}).ToList();
				return defeitos;
			}
		}
		#endregion
	}
}
