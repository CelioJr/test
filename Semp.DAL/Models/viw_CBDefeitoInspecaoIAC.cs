﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.Models
{
    public class viw_CBDefeitoInspecaoIAC
    {
        public int CodLinha { get; set; }

        public Nullable<System.DateTime> DatReferencia { get; set; }
        public Nullable<int> Turno { get; set; }

      

        public string Modelo { get; set; }
        public String Posicao { get; set; }
        public String CodDefeito { get; set; }
        public String Data { get; set; }
        public int hora { get; set; }
        public string DesDefeito { get; set; }
        public int  Quantidade { get; set; }
             
    }
}
