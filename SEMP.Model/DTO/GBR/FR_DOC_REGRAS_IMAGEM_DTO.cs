using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DOC_REGRAS_IMAGEM_DTO
    {
        public int DOC_CODIGO { get; set; }
        public int REG_COD { get; set; }
        public string REG_DEPENDENCIA { get; set; }
        public byte[] REG_IMAGEM { get; set; }
        public string SIS_COD { get; set; }
        public string REG_HASH { get; set; }
        public virtual FR_DOC_PRINCIPAL_DTO FR_DOC_PRINCIPAL { get; set; }
        public virtual FR_REGRAS_DTO FR_REGRAS { get; set; }
    }
}
