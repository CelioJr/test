using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_IMPORT_ERROR_DTO
    {
        public int COD_IMPORT { get; set; }
        public string ERRO { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
        public string SERIAL { get; set; }
    }
}
