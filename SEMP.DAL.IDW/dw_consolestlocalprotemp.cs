namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolestlocalprotemp
    {
        [Key]
        public long id_consolestlocalprotemp { get; set; }

        public long id_turno { get; set; }

        public long id_cal { get; set; }

        public long id_estlocalpro { get; set; }

        public long? id_usr { get; set; }

        public DateTime dt_referencia { get; set; }

        public DateTime dthr_iturno { get; set; }

        public DateTime dthr_fturno { get; set; }

        public DateTime dthr { get; set; }

        public int ano { get; set; }

        public int mes { get; set; }

        public short tp_ajusteestoque { get; set; }

        public decimal qt { get; set; }

        public virtual dw_cal dw_cal { get; set; }

        public virtual dw_estlocalpro dw_estlocalpro { get; set; }

        public virtual dw_turno dw_turno { get; set; }

        public virtual om_usr om_usr { get; set; }
    }
}
