﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade
{
	public class CBTempoMedioLinha
	{
		[Key]
		[Column(Order = 2)] 
		[Display(Name="Linha")]
		public int CodLinha { get; set; }
		[Key]
		[Column(Order = 3)] 
		[Display(Name = "Posto")]
		public int NumPosto { get; set; }
		[Display(Name = "Descrição")]
		public string DesPosto { get; set; }
		[Key]
		[Column(Order = 1)]
		[Display(Name = "Modelo")]
		public string CodModelo { get; set; }
		[Display(Name = "Descrição")]
		public string DesModelo { get; set; }
		[Display(Name = "Tempo Médio")]
		public decimal TempoMedio { get; set; }
		[Display(Name = "Maior tempo")]
		public decimal Maior { get; set; }
		[Display(Name = "Menor Tempo")]
		public decimal Menor { get; set; }
		[Display(Name = "Qtde Leituras")]
		public int Qtde { get; set; }
		[Display(Name = "Tempo Cadastrado")]
		public decimal? TempoCiclo { get; set; }
	}
}
