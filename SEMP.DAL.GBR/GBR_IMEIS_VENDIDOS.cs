namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_IMEIS_VENDIDOS
    {
        [Key]
        public int COD_LISTA_IMEI { get; set; }

        [StringLength(15)]
        public string IMEI { get; set; }

        public DateTime? DATA { get; set; }

        [StringLength(20)]
        public string USUARIO { get; set; }

        [StringLength(30)]
        public string MATERIAL { get; set; }

        [StringLength(10)]
        public string DEPOSITO { get; set; }

        [StringLength(10)]
        public string CENTRO { get; set; }

        [StringLength(10)]
        public string STATUS { get; set; }
    }
}
