using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_LISTA_REMESSAS_DTO
    {
        public int COD_REMESSA { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
        public string USUARIO { get; set; }
        public string REMESSA { get; set; }
        public string IMEI { get; set; }
        public string CODVENDA { get; set; }
        public Nullable<int> QTY { get; set; }
        public string FILE_PATH { get; set; }
    }
}
