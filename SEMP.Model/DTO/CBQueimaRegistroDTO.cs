using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class CBQueimaRegistroDTO
	{
        public int CodLinha { get; set; }
        public int NumPosto { get; set; }
        public string CodFab { get; set; }
        public string NumRPA { get; set; }
        public string CodPallet { get; set; }
        public string CodModelo { get; set; }
        public Nullable<int> QtdPallet { get; set; }
        public Nullable<int> QtdAmostras { get; set; }
        public Nullable<System.DateTime> DatProducao { get; set; }
        public Nullable<System.DateTime> DatAbertura { get; set; }
        public Nullable<System.DateTime> DatFechamento { get; set; }
		public string CodOperador { get; set; }
		public string FlgStatus { get; set; }
		public string CodLinhaOri { get; set; }
		public string NumRPE { get; set; }
		public DateTime? DatLiberado { get; set; }
		public string ObsRevisao { get; set; }

		public string DesLinha { get; set; }
		public string DesPosto { get; set; }
		public string DesModelo { get; set; }
		public string DesLinhaOri { get; set; }

		public string DesStatus { get; set; }
		public string DesCorStatus { get; set; }

	}
}
