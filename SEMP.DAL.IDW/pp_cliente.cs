namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_cliente
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pp_cliente()
        {
            dw_estpro = new HashSet<dw_estpro>();
            dw_rap = new HashSet<dw_rap>();
            om_clidiario = new HashSet<om_clidiario>();
            om_contato = new HashSet<om_contato>();
            om_produto = new HashSet<om_produto>();
            pp_cp = new HashSet<pp_cp>();
            pp_nec = new HashSet<pp_nec>();
        }

        [Key]
        public long id_cliente { get; set; }

        [StringLength(30)]
        public string cd_cliente { get; set; }

        public int? revisao { get; set; }

        public DateTime? dt_revisao { get; set; }

        public DateTime? dt_stativo { get; set; }

        public short? st_ativo { get; set; }

        [StringLength(256)]
        public string nm_cliente { get; set; }

        public short? tp_cliente { get; set; }

        [StringLength(30)]
        public string cnpj_cpf { get; set; }

        [StringLength(256)]
        public string endereco { get; set; }

        [StringLength(10)]
        public string cidade { get; set; }

        [StringLength(4)]
        public string estado { get; set; }

        [StringLength(10)]
        public string pais { get; set; }

        [StringLength(20)]
        public string telefoneUm { get; set; }

        [StringLength(20)]
        public string telefoneDois { get; set; }

        [StringLength(20)]
        public string telefoneTres { get; set; }

        [StringLength(100)]
        public string contato { get; set; }

        public int? hr_leadtime { get; set; }

        public long? id_usrrevisao { get; set; }

        public long? id_usrstativo { get; set; }

        [StringLength(255)]
        public string url_site { get; set; }

        [StringLength(30)]
        public string depara { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_estpro> dw_estpro { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rap> dw_rap { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_clidiario> om_clidiario { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_contato> om_contato { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_produto> om_produto { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cp> pp_cp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_nec> pp_nec { get; set; }
    }
}
