﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.Models;
using SEMP.Model.DTO;

namespace SEMP.DAL.DAO.PM
{
	public class daoProgramaProducao
	{
		#region MF
		/// <summary>
		/// Obter o plano de produção de um mês para a fase Montagem Final
		/// </summary>
		/// <param name="mes"></param>
		/// <param name="fase"></param>
		/// <param name="codlinha"></param>
		/// <param name="familia"></param>
		/// <returns></returns>
		public static List<ProgDiaJitDTO> ObterProgramacao(DateTime mes, string codlinha, string familia)
		{

			using (var dtbSim = new DTB_SIMContext())
			{
				var dias = mes.Day - 1;
				var dIni = mes.AddDays(-dias);
				var dFim = mes.AddMonths(1);

				var resultado = (from programa in dtbSim.tbl_ProgDiaJit 
								join modelo in dtbSim.tbl_Item on programa.CodModelo equals modelo.CodItem
								join fam in dtbSim.tbl_IAMCFamilia on modelo.CodFam equals fam.CodFam
								join linha in dtbSim.tbl_Linha on programa.CodLinha equals linha.CodLinha
								from ppmes in dtbSim.tbl_ProgMesResumo.Where(x => x.CodFab == programa.CodFab && x.CodModelo == programa.CodModelo && x.DatProducao == dIni).DefaultIfEmpty()
								from capac in dtbSim.tbl_CapacLinha.Where(c => programa.CodModelo == c.CodModelo && programa.CodLinha == c.CodLinha && programa.NumTurno == c.NumTurno && dIni == c.DatProducao).DefaultIfEmpty()
								 where programa.DatProducao >= dIni && programa.DatProducao < dFim
								 select new ProgDiaJitDTO() { 
									CodFab = programa.CodFab,
									CodConjCin = programa.CodConjCin,
									CodConjFly = programa.CodConjFly,
									CodLinha = programa.CodLinha,
									CodModelo = programa.CodModelo,
									DatEstudo = programa.DatEstudo,
									DatProducao = programa.DatProducao,
									DesLinha = linha.DesLinha,
									DesModelo = modelo.DesItem,
									Familia = fam.NomFam,
									FlagTP = programa.FlagTP,
									NumTurno = programa.NumTurno,
									Observacao = programa.Observacao,
									QtdProdPm = programa.QtdProdPm,
									QtdProducao = programa.QtdProducao,
									Usuario = programa.Usuario,
									flgCobertura = programa.flgCobertura,
									rowguid = programa.rowguid,
									QtdCapac = capac.QtdCapacLinha,
									PlanoMes = ppmes.QtdProdPm
									});

				if (!String.IsNullOrEmpty(codlinha))
				{
					resultado = resultado.Where(x => x.CodLinha == codlinha);
				}

				if (!String.IsNullOrEmpty(familia))
				{
					resultado = resultado.Where(x => x.Familia == familia);
				}

				return resultado.OrderBy(x => x.CodLinha).ThenBy(x => x.Familia).ThenBy(x => x.CodModelo).ThenBy(x => x.NumTurno).ToList();

			}
		}

		/// <summary>
		/// Rotina para obter os meses com programação posteriores a um mês indicado no parâmetro
		/// </summary>
		/// <param name="mesInicial"></param>
		/// <returns></returns>
		public static List<String> ObterMesesPrograma(DateTime mesInicial)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var mesAtual = DateTime.Parse("01" + DateTime.Now.ToShortDateString().Substring(2, 8));
				if (mesInicial < mesAtual)
				{
					mesAtual = mesInicial;
				}

				var query = dtbSim.tbl_ProgDiaJit.Where(x => x.DatProducao >= mesAtual).GroupBy(x => x.DatProducao).Select(x => x.Key).ToList();

				var resultado = query.GroupBy(x => x.ToShortDateString().Substring(3, 7)).Select(x => x.Key).ToList();

				return resultado;

			}
		}

		public static List<ProgDiaJitDTO> ObterProgramacaoSAP(DateTime datInicio)
		{

			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from programa in dtbSim.tbl_ProgDiaJit
								 where programa.DatProducao >= datInicio
								 select new ProgDiaJitDTO()
								 {
									 CodFab = programa.CodFab,
									 CodConjCin = programa.CodConjCin,
									 CodConjFly = programa.CodConjFly,
									 CodLinha = programa.CodLinha,
									 CodModelo = programa.CodModelo,
									 DatEstudo = programa.DatEstudo,
									 DatProducao = programa.DatProducao,
									 FlagTP = programa.FlagTP,
									 NumTurno = programa.NumTurno,
									 Observacao = programa.Observacao,
									 QtdProdPm = programa.QtdProdPm,
									 QtdProducao = programa.QtdProducao,
									 Usuario = programa.Usuario,
									 flgCobertura = programa.flgCobertura,
									 rowguid = programa.rowguid
								 });

				return resultado.OrderBy(x => x.CodLinha).ThenBy(x => x.CodModelo).ThenBy(x => x.NumTurno).ToList();

			}
		}

		#endregion

		#region Placas
		/// <summary>
		/// Obter o plano de produção de um mês para a fase de Placas
		/// </summary>
		/// <param name="mes"></param>
		/// <param name="fase"></param>
		/// <param name="codlinha"></param>
		/// <param name="familia"></param>
		/// <returns></returns>
		public static List<ProgDiaJitDTO> ObterProgramacaoPlacas(DateTime mes, string fase, string codlinha, string familia)
		{

			using (var dtbSim = new DTB_SIMContext())
			{
				var dias = mes.Day - 1;
				var dIni = mes.AddDays(-dias);
				var dFim = mes.AddMonths(1);
				var tipo = fase.ToUpper().Equals("I") ? "1" : "2";

				var resultado = (from programa in dtbSim.tbl_ProgDiaPlaca
								 join modelo in dtbSim.tbl_Item on programa.CodPlaca equals modelo.CodItem
								 join processo in dtbSim.tbl_ProgProcesso on programa.CodProcesso equals processo.CodProcesso
								 from linha in dtbSim.tbl_Linha.Where(l => programa.CodLinha == l.CodLinha).DefaultIfEmpty()
								 from ppmes in dtbSim.tbl_ProgMesPlaca
														.GroupBy(x => new { x.CodFab, x.CodModelo, x.DatProducao })
														.Select(x => new { CodFab = x.Key.CodFab, CodModelo = x.Key.CodModelo, DatProducao = x.Key.DatProducao, QtdPrograma = x.Sum(y => y.QtdPrograma) })
														.Where(x => x.CodFab == programa.CodFab && x.CodModelo == programa.CodPlaca && x.DatProducao == dIni)
														.DefaultIfEmpty()
								 from capac in dtbSim.tbl_CapacLinha.Where(c => programa.CodPlaca == c.CodModelo && programa.CodLinha == c.CodLinha && programa.NumTurno == c.NumTurno && dIni == c.DatProducao).DefaultIfEmpty()
								 from blinha in dtbSim.tbl_BarrasLinha.Where(c => programa.CodLinha == c.CodLinha).DefaultIfEmpty()
								 where programa.DatProducao >= dIni && programa.DatProducao <= dFim && 
										(processo.CodTipo.Equals(fase) || processo.CodTipo.Equals(tipo))
								 select new ProgDiaJitDTO()
								 {
									 CodFab = programa.CodFab,
									 CodConjCin = programa.CodConjCin,
									 CodConjFly = String.Empty,
									 CodLinha = programa.CodLinha,
									 CodModelo = programa.CodModelo,
									 DatEstudo = programa.DatEstudo,
									 DatProducao = programa.DatProducao,
									 DesLinha = linha.DesLinha ?? programa.CodLinha,
									 DesModelo = modelo.DesItem,
									 Familia = blinha.Produto,
									 FlagTP = programa.FlagTP,
									 NumTurno = programa.NumTurno,
									 Observacao = programa.Observacao,
									 QtdProdPm = programa.QtdProdPM,
									 QtdProducao = programa.QtdProducao,
									 Usuario = String.Empty,
									 flgCobertura = programa.flgCobertura,
									 rowguid = programa.rowguid,
									 QtdCapac = capac.QtdCapacLinha,
									 PlanoMes = ppmes.QtdPrograma
								 });

				if (!String.IsNullOrEmpty(codlinha))
				{
					resultado = resultado.Where(x => x.CodLinha == codlinha);
				}

				if (!String.IsNullOrEmpty(familia))
				{
					resultado = resultado.Where(x => x.Familia == familia);
				}

				return resultado.OrderBy(x => x.CodLinha).ThenBy(x => x.Familia).ThenBy(x => x.CodModelo).ThenBy(x => x.NumTurno).ToList();

			}
		}

		/// <summary>
		/// Rotina para obter os meses com programação de placas posteriores a um mês indicado no parâmetro
		/// </summary>
		/// <param name="mesInicial"></param>
		/// <returns></returns>
		public static List<String> ObterMesesProgramaPlacas(DateTime mesInicial)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var mesAtual = DateTime.Parse("01" + DateTime.Now.ToShortDateString().Substring(2, 8));
				if (mesInicial < mesAtual)
				{
					mesAtual = mesInicial;
				}

				var query = dtbSim.tbl_ProgDiaPlaca.Where(x => x.DatProducao >= mesAtual).GroupBy(x => x.DatProducao).Select(x => x.Key).ToList();

				var resultado = query.GroupBy(x => x.ToShortDateString().Substring(3, 7)).Select(x => x.Key).ToList();

				return resultado;

			}
		}

		#endregion

		#region APs

		public static List<LSAAPDTO> ObterAPs(string codModelo, DateTime datReferencia) {

			using (var contexto = new DTB_SIMContext())
			{
				var dados = contexto.tbl_LSAAP
							.Where(x => x.CodModelo == codModelo && x.DatReferencia == datReferencia)
							.Select(x => new LSAAPDTO() {
								CodFab = x.CodFab,
								NumAP = x.NumAP,
								CodModelo = x.CodModelo,
								QtdLoteAP = x.QtdLoteAP,
								QtdEmbalado = x.QtdEmbalado,
								QtdProduzida = x.QtdProduzida,
								CodStatus = x.CodStatus,
								CodLinha = x.CodLinha,
								DatReferencia = x.DatReferencia,
								CodLocal = x.CodLocal,
								CodLocalAP = x.CodLocalAP,
								TipAP = x.TipAP							
							})
							.ToList();

				return dados;

			}
		
		}

		public static List<LSAAPDTO> ObterAPs( DateTime datReferencia)
		{

			using (var contexto = new DTB_SIMContext())
			{
				var dados = contexto.tbl_LSAAP
							.Where(x => x.DatReferencia == datReferencia)
							.Select(x => new LSAAPDTO()
							{
								CodFab = x.CodFab,
								NumAP = x.NumAP,
								CodModelo = x.CodModelo,
								QtdLoteAP = x.QtdLoteAP,
								QtdEmbalado = x.QtdEmbalado,
								QtdProduzida = x.QtdProduzida,
								CodStatus = x.CodStatus,
								CodLinha = x.CodLinha,
								DatReferencia = x.DatReferencia,
								CodLocal = x.CodLocal,
								CodLocalAP = x.CodLocalAP,
								TipAP = x.TipAP
							})
							.ToList();

				return dados;

			}

		}

        public static List<LSAAPDTO> ObterAPs(DateTime datReferencia, string codFase)
        {

            using (var contexto = new DTB_SIMContext())
            {
                var dados = contexto.tbl_LSAAP
                            .Where(x => x.DatReferencia == datReferencia && x.CodProcesso.StartsWith(codFase))
                            .Select(x => new LSAAPDTO()
                            {
                                CodFab = x.CodFab,
                                NumAP = x.NumAP,
                                CodModelo = x.CodModelo,
                                QtdLoteAP = x.QtdLoteAP,
                                QtdEmbalado = x.QtdEmbalado,
                                QtdProduzida = x.QtdProduzida,
                                CodStatus = x.CodStatus,
                                CodLinha = x.CodLinha,
                                DatReferencia = x.DatReferencia,
                                CodLocal = x.CodLocal,
                                CodLocalAP = x.CodLocalAP,
                                TipAP = x.TipAP
                            })
                            .ToList();

                return dados;

            }

        }

        #endregion

    }
}
