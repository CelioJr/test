namespace SEMP.Model.DTO
{
	using System.ComponentModel.DataAnnotations;

	public partial class ZCSD_INTERFACE_ITEM_NF_DTO
	{
        public int id { get; set; }

        [Required]
        [StringLength(16)]
        public string PACKNO { get; set; }

        [Required]
        [StringLength(10)]
        public string EXTROW { get; set; }

        [StringLength(12)]
        public string DOCUMENTOSAP { get; set; }

        [StringLength(10)]
        public string ITEMDOCUMENTO { get; set; }

        [StringLength(46)]
        public string MATERIAL { get; set; }

        [StringLength(8)]
        public string CENTRO { get; set; }

        [StringLength(14)]
        public string CFOP { get; set; }

        [StringLength(4)]
        public string NCM { get; set; }

        [StringLength(4)]
        public string ORIGEM { get; set; }

        [StringLength(4)]
        public string CST { get; set; }

        [StringLength(38)]
        public string INVOICE { get; set; }

        [StringLength(8)]
        public string ITEMINVOICE { get; set; }

        [StringLength(18)]
        public string QUANTIDADE { get; set; }

        [StringLength(19)]
        public string VALORUNITARIO { get; set; }

        [StringLength(19)]
        public string QUANTIDADETRIB { get; set; }

        [StringLength(9)]
        public string ALIQUOTAICMS { get; set; }

        [StringLength(18)]
        public string BASEICMS { get; set; }

        [StringLength(18)]
        public string BASEEXCLUIDA { get; set; }

        [StringLength(18)]
        public string OUTRASBASES { get; set; }

        [StringLength(18)]
        public string VALORICMS { get; set; }

        [StringLength(9)]
        public string ALIQUOTAICMSST { get; set; }

        [StringLength(19)]
        public string BASEICMSST { get; set; }

        [StringLength(19)]
        public string VALORICMSST { get; set; }

        [StringLength(10)]
        public string ALIQUOTAIPI { get; set; }

        [StringLength(18)]
        public string BASEIPI { get; set; }

        [StringLength(18)]
        public string BASEEXCLUIDA1 { get; set; }

        [StringLength(18)]
        public string OUTRASBASES1 { get; set; }

        [StringLength(18)]
        public string VALORIPI { get; set; }

        [StringLength(10)]
        public string ALIQUOTAPIS { get; set; }

        [StringLength(18)]
        public string BASEPIS { get; set; }

        [StringLength(18)]
        public string VALORPIS { get; set; }

        [StringLength(10)]
        public string ALIQUOTACOFINS { get; set; }

        [StringLength(19)]
        public string BASECOFINS { get; set; }

        [StringLength(19)]
        public string VALORCOFINS { get; set; }

        [StringLength(10)]
        public string ALIQUOTAFCP { get; set; }

        [StringLength(19)]
        public string BASEFCP { get; set; }

        [StringLength(19)]
        public string VALORFCP { get; set; }

        [StringLength(10)]
        public string ALIQUOTADIFALORIG { get; set; }

        [StringLength(19)]
        public string BASEDIFALORIG { get; set; }

        [StringLength(19)]
        public string VALORDIFALORIG { get; set; }

        [StringLength(10)]
        public string ALIQUOTADIFALDEST { get; set; }

        [StringLength(19)]
        public string BASEDIFALDEST { get; set; }

        [StringLength(19)]
        public string VALORDIFALDEST { get; set; }

        [StringLength(19)]
        public string NRREMESSA { get; set; }

        [StringLength(10)]
        public string ITEMREMESSA { get; set; }

        [StringLength(14)]
        public string ORDEMVENDAS { get; set; }

        [StringLength(10)]
        public string ITEMORDEMVENDA { get; set; }
    }
}
