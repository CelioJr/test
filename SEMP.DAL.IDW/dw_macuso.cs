namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_macuso
    {
        [Key]
        public long id_macuso { get; set; }

        public long id_macrange { get; set; }

        [StringLength(12)]
        public string cd_mac { get; set; }

        public DateTime? dthr_uso { get; set; }

        public int? grupo { get; set; }

        public virtual dw_macrange dw_macrange { get; set; }
    }
}
