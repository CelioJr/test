﻿using SEMP.DAL.Models;
using SEMP.Model.DTO;
using SEMP.Model.VO.Materiais;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace SEMP.DAL.DAO.Materiais
{
	public class daoEstoque
	{

		public static List<EstoquePCIDTO> ObterEstoquePCI() 
		{
			try
			{
				using (var contexto = new DTB_SIMContext())
				{

					var dados = (from e in contexto.tbl_EstoquePCI
								 join i in contexto.tbl_Item on e.CodItem equals i.CodItem
								 select new EstoquePCIDTO() { 
									 CodFab = e.CodFab,
									 DesItem = i.DesItem,
									 CodItem = e.CodItem,
									 CodLocal = e.CodLocal,
									 DatAtualizacao = e.DatAtualizacao,
									 QtdEstoque = e.QtdEstoque
								 });

					var retorno = dados.ToList();

					return retorno;

				}
			}
			catch
			{
				return new List<EstoquePCIDTO>();
			}

		}

		public static List<EstoquePCIDTO> ObterSaldoPlacas()
		{
			try
			{
				using (var contexto = new DTB_SIMContext())
				{

					var dados = (from e in contexto.tbl_EstoquePCI
								 join i in contexto.tbl_Item on e.CodItem equals i.CodItem
								 select new EstoquePCIDTO()
								 {
									 CodFab = e.CodFab,
									 DesItem = i.DesItem,
									 CodItem = e.CodItem,
									 CodLocal = e.CodLocal,
									 DatAtualizacao = e.DatAtualizacao,
									 QtdEstoque = e.QtdEstoque
								 });

					var retorno = dados.ToList();

					return retorno;

				}
			}
			catch
			{
				return new List<EstoquePCIDTO>();
			}

		}

		public static List<SaldoPlacasResumo> ObterResumoSaldoPlacas(string agrupa, string codModelo, string codPlaca){

			using (var contexto = new DTB_SIMContext())
			{
				var resumo = contexto.Database.SqlQuery<SaldoPlacasResumo>(
					"EXEC spc_SISAPSaldoPlacas @Agrupa, @Modelo, @Placa",
					new SqlParameter("Agrupa", agrupa),
					new SqlParameter("Modelo", codModelo),
					new SqlParameter("Placa", codPlaca)
					).ToList();

				return resumo;
			}
		}

		public static bool GravarEstoquePCI(List<EstoquePCIDTO> dados)
		{
			try
			{
				using (var contexto = new DTB_SIMContext())
				{
					contexto.Database.ExecuteSqlCommand("TRUNCATE TABLE [tbl_EstoquePCI]");

					var insere = dados.Select(x => new tbl_EstoquePCI() { 
						CodFab = x.CodFab,
						CodItem = x.CodItem.TrimStart('0'),
						CodLocal = x.CodLocal,
						DatAtualizacao = x.DatAtualizacao,
						QtdEstoque = x.QtdEstoque
					}).ToList();

					contexto.tbl_EstoquePCI.AddRange(insere);

					contexto.SaveChanges();

					return true;
				}
			}
			catch
			{
				return false;
			}

		}
	}
}
