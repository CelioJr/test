namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_INVENTORY")]
    public partial class ALC_INVENTORY
    {
        [Key]
        public int ID_INVENTORY { get; set; }

        public DateTime? DT_INVENTORY { get; set; }

        [StringLength(30)]
        public string PACKAGE_CODE { get; set; }

        [MaxLength(255)]
        public byte[] ppb { get; set; }

        public int? SERIAL_CODE { get; set; }

        public int? SKU_CODE { get; set; }

        public int? USER_INVENTORY { get; set; }

        public virtual ALC_SYS_USER ALC_SYS_USER { get; set; }
    }
}
