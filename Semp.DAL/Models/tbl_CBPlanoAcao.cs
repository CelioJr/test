using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEMP.DAL.Models
{
	public partial class tbl_CBPlanoAcao
	{
		public int CodPlano { get; set; }
		public int CodLinha { get; set; }
		public Nullable<int> NumPosto { get; set; }
		public Nullable<System.DateTime> DatPlano { get; set; }
		public string CodDRT { get; set; }
		public string DesPlano { get; set; }
		public Nullable<int> FlgStatus { get; set; }
		public Nullable<System.DateTime> DatConclusao { get; set; }
		public Nullable<int> Hora { get; set; }
	}
}
