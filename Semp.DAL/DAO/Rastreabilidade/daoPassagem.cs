﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.VO;
using SEMP.DAL.Models;
using SEMP.Model.DTO;
using SEMP.Model.VO.Rastreabilidade;
using SEMP.DAL.DTB_SSA.Models;

namespace SEMP.DAL.DAO.Rastreabilidade
{   
	/// <summary>
	/// Classe responsável por controlar as transações com o banco de dados.
	/// </summary>
	/// <summary>
	/// Classe responsável por controlar as transações com o banco de dados.
	/// </summary>
	public class daoPassagem
    {
        //static daoPassagem()
        //{
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<tbl_CBPassagem, CBPassagemDTO>();
        //        cfg.CreateMap<tbl_CBDefeito, CBDefeitoDTO>();
        //    });
        //}

        #region Consultas
        /// <summary>
        /// Este método é responsável por verificar se o respectivo produto passou no respectivo posto. 
        /// </summary>
        /// <param name="numECB"></param>
        /// <param name="codLinha"></param>
        /// <param name="numPosto"></param>
        /// <returns>Retorna o registro da passagem.</returns>
        public static CBPassagemDTO ObterPassagemPosto(string numECB, int codLinha, int numPosto)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var passagem = (from i in dtb_CB.tbl_CBPassagem
								where i.NumECB == numECB && i.CodLinha == codLinha && i.NumPosto == numPosto
								select i).FirstOrDefault();

				CBPassagemDTO cBPassagemDTO = new CBPassagemDTO();

				AutoMapper.Mapper.Map(passagem, cBPassagemDTO);

				return cBPassagemDTO;
			}
		}

		/// <summary>
		/// Metodo responsável por verificar se o respectivo produto passou no respectivo posto. 
		/// </summary>
		/// <param name="numECB"></param>
		/// <param name="codLinha"></param>
		/// <param name="numPosto"></param>
		/// <param name="numSeq"></param>
		/// <returns>Retorna um true or false</returns>
		public static Boolean PassouNoPosto(string numECB, int codLinha, int numPosto)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				dtb_CB.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

				/*var passagem = (from i in dtb_CB.tbl_CBPassagem
								where (i.NumECB == numECB && i.CodLinha == codLinha && i.NumPosto == numPosto) //|| 
								select i).FirstOrDefault();*/

				var postos = (from p in dtb_CB.tbl_CBPosto
								join p2 in dtb_CB.tbl_CBPosto on new { p.CodLinha, p.NumSeq } equals new { p2.CodLinha, p2.NumSeq }
								where p2.CodLinha == codLinha && p2.NumPosto == numPosto
								select p.NumPosto).ToList();

				Boolean passagem = false;
				
				passagem = dtb_CB.tbl_CBPassagem.Any(i => i.NumECB == numECB && i.CodLinha == codLinha && postos.Contains(i.NumPosto));

				return passagem;
			}
		}

		/// <summary>
		/// Verifica se o produto tem algum defeito não resolvido.
		/// </summary>
		/// <param name="numECB"></param>
		/// <returns> Retorna um registro.</returns>
		public static CBDefeitoDTO ObterDefeitoAberto(string numECB)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var defeito = (from i in dtb_CB.tbl_CBDefeito
							   where i.NumECB == numECB && i.FlgRevisado == 0
							   select i).FirstOrDefault();

				CBDefeitoDTO cBDefeitoDTO = new CBDefeitoDTO();

				AutoMapper.Mapper.Map(defeito, cBDefeitoDTO);

				return cBDefeitoDTO;
			}
		}

		/// <summary>
		/// Verifica se o produto tem algum defeito não resolvido.
		/// </summary>
		/// <param name="numECB"></param>
		/// <returns> Retorna um true or false.</returns>
		public static Boolean DefeitoAberto(string numECB)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				dtb_CB.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

				var defeito = (from i in dtb_CB.tbl_CBDefeito
							   where i.NumECB == numECB && i.FlgRevisado == 0
							   select new { FlgRevisado = i.FlgRevisado }).FirstOrDefault();

				return (defeito != null);
			}
		}

		/// <summary>
		/// Dado uma linha e uma sequencia ele verifica quantos postos antes dessa sequencia existem.
		/// </summary>
		/// <param name="codLinha"></param>
		/// <param name="numSeq"></param>
		/// <returns>Um valor inteiro, quantidade.</returns>
		public static int ObtertQtdPostosSequencia(int codLinha, int numSeq)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				dtb_CB.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
				var query = (from i in dtb_CB.tbl_CBPosto
						where i.CodLinha == codLinha && i.flgObrigatorio == true && i.NumSeq < numSeq
						select new { Sequencia = i.NumSeq }).Distinct().ToList();

				return query.Count();
			}

		}

		/// <summary>
		/// Obtem a quantidade de postos da linha que o número de série passou.
		/// </summary>
		/// <param name="codLinha"></param>
		/// <param name="numECB"></param>
		/// <param name="sequencia"></param>
		/// <returns></returns>
		public static int ObterQtdLeituras(int codLinha, string numECB, int sequencia)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				dtb_CB.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
				var postoObrigatorios = dtb_CB.tbl_CBPosto.Where(a => a.CodLinha == codLinha && a.flgObrigatorio == true && a.NumSeq < sequencia).Select(s => s.NumPosto).ToList();

				var query = (from i in dtb_CB.tbl_CBPassagem
						where i.CodLinha == codLinha && i.NumECB == numECB && postoObrigatorios.Contains(i.NumPosto)
						select new { Postos = i.NumPosto }).ToList();

				return query.Count();
			}

		}

		/// <summary>
		/// Obtem a sequencias que existe em um posto de uma determinada linha.
		/// </summary>
		/// <param name="codLinha"></param>
		/// <param name="numPosto"></param>
		/// <returns></returns>
		public static int ObterNumSeq(int codLinha, int numPosto)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				dtb_CB.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
				int seq = (from p in dtb_CB.tbl_CBPosto
						   where p.CodLinha == codLinha && p.NumPosto == numPosto
						   select p.NumSeq).FirstOrDefault();

				return seq;
			}
		}

		/// <summary>
		/// Obtem o ultimo posto em que o número de série passou.
		/// </summary>
		/// <param name="numECB"></param>
		/// <returns></returns>
		public static CBPassagemDTO ObterUltimaLeitura(string numECB)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				dtb_CB.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
				var passagem = dtb_CB.tbl_CBPassagem.Where(x => x.NumECB == numECB).OrderByDescending(x => x.DatEvento).FirstOrDefault();

				CBPassagemDTO cBPassagemDTO = new CBPassagemDTO();

				AutoMapper.Mapper.Map(passagem, cBPassagemDTO);

				return cBPassagemDTO;
			}
		}

		/// <summary>
		/// Obtem as 7 ultimas leituras de em determinado posto da uma linha.
		/// </summary>
		/// <param name="codLinha"></param>
		/// <param name="numPosto"></param>
		/// <returns>Retorna uma lista de registros.</returns>
		public static List<CBPassagemDTO> ObterPassagemPosto(int codLinha, int numPosto)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				dtb_CB.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
				var passagens = (from p in dtb_CB.tbl_CBPassagem
								 where p.CodLinha == codLinha && p.NumPosto == numPosto
								 select p).OrderByDescending(x => x.DatEvento).Take(7);

				var p2 = passagens.ToList();

				List<CBPassagemDTO> cBPassagemDTO = new List<CBPassagemDTO>();

				AutoMapper.Mapper.Map(p2, cBPassagemDTO);

				return cBPassagemDTO;
			}
		}

		/// <summary>
		/// Obter as passagens de um posto em um determinado período
		/// </summary>
		/// <param name="codLinha"></param>
		/// <param name="numPosto"></param>
		/// <param name="datInicio"></param>
		/// <param name="datFim"></param>
		/// <returns></returns>
		public static List<CBPassagemDTO> ObterPassagemPosto(int codLinha, int numPosto, DateTime datInicio, DateTime datFim)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				dtb_CB.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
				var passagens = (from p in dtb_CB.tbl_CBPassagem
								 join l in dtb_CB.tbl_CBLinha on p.CodLinha equals l.CodLinha
								 join po in dtb_CB.tbl_CBPosto on new { p.NumPosto, p.CodLinha } equals new { po.NumPosto, po.CodLinha }
								 from f in dtb_CB.tbl_RIFuncionario.Where(x => x.CodDRT.Contains(p.CodDRT) && x.CodFab == "60").DefaultIfEmpty()
								 where p.CodLinha == codLinha && p.NumPosto == numPosto &&
										p.DatEvento >= datInicio && p.DatEvento <= datFim
								 select new CBPassagemDTO() { 
									CodDRT = p.CodDRT,
									CodFab = p.CodFab,
									CodLinha = p.CodLinha,
									NumPosto = p.NumPosto,
									DatEvento = p.DatEvento,
									DescricaoLinha = l.DesLinha,
									DescricaoPosto = po.DesPosto,
									NomeOperador = f.NomFuncionario,
									NumAP = p.NumAP,
									NumECB = p.NumECB,
									NumSeq = po.NumSeq,
									QtdAmarra = 0,
									QtdDefeitos = 0
								 }).OrderByDescending(x => x.DatEvento);

				var p2 = passagens.ToList();

				return p2;
			}
		}

		/// <summary>
		///  Verifica se o número de série já existe na tabela de identificação tbl_CBIdentifica.
		/// </summary>
		/// <param name="numECB"></param>
		/// <returns>Boolean</returns>
		public static Boolean VerificaIdentifica(string numECB)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				dtb_CB.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
				bool identifica = dtb_CB.tbl_CBIdentifica.Any(x => x.NumECB == numECB);
				
				return identifica;
			}
		}

		/// <summary>
		/// Verifica se existe qualquer tipo de passagem por posto para o número de série informado.
		/// </summary>
		/// <param name="numECB"></param>
		/// <returns>Retorna um boolean</returns>
		public static Boolean VerificaPassagem(string numECB)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				dtb_CB.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
				var passagem = dtb_CB.tbl_CBPassagem.Where(x => x.NumECB == numECB).FirstOrDefault(); ;

				if (passagem == null)
					return false;
				else
					return true;
			}
		}

		/// <summary>
		/// Obtem a data que o número de série passsou em determinado posto de uma determinada linha.
		/// </summary>
		/// <param name="numECB"></param>
		/// <param name="CodLinha"></param>
		/// <param name="NumPosto"></param>
		/// <returns>Retorna data de evento.</returns>
		public static DateTime ObterDataPassagem(string numECB, int CodLinha, int NumPosto)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				dtb_CB.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
				DateTime DatEvento = (from p in dtb_CB.tbl_CBPassagem
									  where numECB == p.NumECB && CodLinha == p.CodLinha && NumPosto == p.NumPosto
									  select p.DatEvento).FirstOrDefault();

				return DatEvento;
			}
		}

		/// <summary>
		/// Obtem o primeiro posto de leitura de uma determinada linha.
		/// </summary>
		/// <param name="codLinha"></param>
		/// <returns></returns>
		public static int ObterPrimeiroPostoLeitura(int codLinha) {

			using (DTB_CBContext dtb_CB = new DTB_CBContext() ) {
				dtb_CB.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
				DateTime data = DateTime.Parse(DateTime.Now.ToShortDateString());

				var query = (from p in dtb_CB.tbl_CBPassagem
							join posto in dtb_CB.tbl_CBPosto on new {p.CodLinha, p.NumPosto } equals new {posto.CodLinha, posto.NumPosto}
							where p.DatEvento >= data && p.CodLinha == codLinha && posto.flgObrigatorio == true && posto.FlgAtivo == "S"
							orderby posto.NumSeq
							select p.NumPosto);

				return query.FirstOrDefault();

			}
		
		}


		/// <summary>
		/// Obtem a quantidade de leituras de um determinado posto de uma linha em um determinado intervalo de tempo.
		/// </summary>
		/// <param name="DatInicio"></param>
		/// <param name="DatFim"></param>
		/// <param name="codLinha"></param>
		/// <param name="numPosto"></param>
		/// <returns></returns>
		public static int ObterQtdLeiturasPosto(DateTime DatInicio, DateTime DatFim, int codLinha, int numPosto)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				dtb_CB.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
				var passagens = dtb_CB.tbl_CBPassagem.Where(a => a.CodLinha == codLinha && a.NumPosto == numPosto && a.DatEvento >= DatInicio && a.DatEvento <= DatFim);

				return passagens.Count();
			}

		}

		/// <summary>
		/// Executa a historia de procedure que faz a validação de um determinado numero de série.
		/// </summary>
		/// <param name="numECB"></param>
		/// <param name="codDRT"></param>
		/// <param name="codLinha"></param>
		/// <param name="numPosto"></param>
		/// <returns></returns>
		public static Mensagem ValidarErros(string numECB, string codDRT, int codLinha, int numPosto, string numAP)
		{
			try
			{
				using (DTB_CBContext dtbCB = new DTB_CBContext())
				{
					var resultado = dtbCB.Database.SqlQuery<spc_CBFecValidaErros>(
							"EXEC spc_CBFecValidaErros @CodLinha, @NumPosto, @NumSerie, @DRT, @NumAP",
							new SqlParameter("CodLinha", codLinha),
							new SqlParameter("NumPosto", numPosto),
							new SqlParameter("NumSerie", numECB),
							new SqlParameter("DRT", codDRT),
							new SqlParameter("NumAP", numAP)
							);

					dtbCB.SaveChanges();

					spc_CBFecValidaErros validaErro = resultado.FirstOrDefault();
					Mensagem retorno = new Mensagem();

					if (validaErro.CodErro == 0)
					{
						retorno.CodMensagem = "R0001";
						retorno.DesCor = "lime";
						retorno.DesMensagem = "LEITURA OK. LEIA PRÓXIMO.";
					}
					else if (validaErro.CodErro == 1)
					{
						retorno.CodMensagem = "E0002";
						retorno.DesCor = "red";
						retorno.DesMensagem = "DEFEITO EM ABERTO.";
					}
					else if (validaErro.CodErro == 2)
					{
						retorno.CodMensagem = "R0003";
						retorno.DesCor = "yellow";
						retorno.DesMensagem = "JÁ LIDO NESTA ETAPA.";
					}
					else if (validaErro.CodErro == 3)
					{
						retorno.CodMensagem = "R0004";
						retorno.DesCor = "red";
						retorno.DesMensagem = validaErro.Erro.ToUpper();
					}
					else if (validaErro.CodErro == 4)
					{
						retorno.CodMensagem = "E0002";
						retorno.DesCor = "red";
						retorno.DesMensagem = "POSTO NÃO EXISTE NA LINHA.";
					}
					else if (validaErro.CodErro == 5)
					{
						retorno.CodMensagem = "R0007";
						retorno.DesCor = "red";
						retorno.DesMensagem = "ITEM JÁ AMARRADO A UM PRODUTO.";
					}
					else
					{
						retorno.CodMensagem = "E0002";
						retorno.DesCor = "red";
						retorno.DesMensagem = "OCORREU UM ERRO NÃO IDENTIFICADO. ENTRE EM CONTATO COM O ADMINISTRADOR DO SISTEMA.";
					}

					//retorno.CodMensagem = validaErro.CodErro.ToString();

					return retorno;
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Erro ao validar erros: " + ex.Message);
			}
		}

		public static Mensagem ValidarErrosTablet(string numECB, string codDRT, int codLinha, int numPosto)
		{
			try
			{
				using (DTB_CBContext dtbCB = new DTB_CBContext())
				{
					var resultado = dtbCB.Database.SqlQuery<spc_CBFecValidaErros>(
							"EXEC spc_CBFecValidaErros @CodLinha, @NumPosto, @NumSerie, @DRT",
							new SqlParameter("CodLinha", codLinha),
							new SqlParameter("NumPosto", numPosto),
							new SqlParameter("NumSerie", numECB),
							new SqlParameter("DRT", codDRT)
							);

					dtbCB.SaveChanges();

					spc_CBFecValidaErros validaErro = resultado.FirstOrDefault();
					Mensagem retorno = new Mensagem();

					if (validaErro.CodErro == 4)
					{
						retorno.CodMensagem = "E0002";
						retorno.DesCor = "red";
						retorno.DesMensagem = "POSTO NÃO EXISTE NA LINHA.";
					}
					else if (validaErro.CodErro == 1)
					{
						retorno.CodMensagem = "E0002";
						retorno.DesCor = "red";
						retorno.DesMensagem = "DEFEITO EM ABERTO.";
					}
					else if (validaErro.CodErro == 3)
					{
						retorno.CodMensagem = "R0004";
						retorno.DesCor = "red";
						retorno.DesMensagem = validaErro.Erro.ToUpper();
					}
					else if (validaErro.CodErro == 2)
					{
						daoDefeitos.ApagarPassagemInformatica(codLinha, numPosto, numECB);
						retorno.CodMensagem = "R0003";
						retorno.DesCor = "yellow";
						retorno.DesMensagem = "TESTE LEITURA DE WAKEUP NOVAMENTE";
					}
					else if (validaErro.CodErro == 0)
					{
						retorno.CodMensagem = "R0001";
						retorno.DesCor = "lime";
						retorno.DesMensagem = "LEITURA OK. LEIA PRÓXIMO.";
					}
					else
					{
						retorno.CodMensagem = "E0002";
						retorno.DesCor = "red";
						retorno.DesMensagem = "OCORREU UM ERRO NÃO IDENTIFICADO. ENTRE EM CONTATO COM O ADMINISTRADOR DO SISTEMA.";
					}

					//retorno.CodMensagem = validaErro.CodErro.ToString();

					return retorno;
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Erro ao validar erros: " + ex.Message);
			}
		}

		public static RealPrevisto ObterQuantidadeRealEPrevisto(int codLinha)
		{
			try
			{
				using (DtbSsaContext contexto = new DtbSsaContext())
				{
					int codigoLinha = 0;
					if (codLinha == 42)
					{
						codigoLinha = 1;
					}
					else if (codLinha == 43)
					{
						codigoLinha = 2;
					}
					else if (codLinha == 44)
					{
						codigoLinha = 3;
					}

					var resultado = contexto.Database.SqlQuery<spc_GePro_CalcularQtdePrevistaReal>(
							"EXEC spc_GePro_CalcularQtdePrevistaReal @CodLinha",
							new SqlParameter("CodLinha", codigoLinha)
							);

					contexto.SaveChanges();

					var retorno = resultado.FirstOrDefault();

					RealPrevisto realPrevisto = new RealPrevisto();

					realPrevisto.QuantidadePrevista = retorno.Qtde_Montagem_Prevista;
					realPrevisto.QuantidadeReal = retorno.Qtde_Montagem_Real;

					return realPrevisto;

				}
			}
			catch (Exception ex)
			{
				throw new Exception("Erro ao transferir a Amarração: " + ex.Message);
			}
		}

		#endregion

		#region Gravações
		/// <summary>
		/// Insere os dados referentes ao produto na tabela tbl_CBPassagem e atualiza o histórico na tabela TblHistorico.
		/// </summary>
		/// <param name="passagem"></param>
		/// <param name="CodDRT"></param>
		/// <param name="posto"></param>
		/// <returns></returns>
		public static Boolean InserePassagem(tbl_CBPassagem passagem, string CodDRT, DadosPosto posto)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				try
				{
					dtb_CB.tbl_CBPassagem.Add(passagem);

					if (posto.CodTipoPosto == 20 || posto.CodTipoPosto == 23 || posto.CodTipoPosto == 27)
					{
						byte codLinha = 0;

						if (posto.CodLinha == 42)
						{
							codLinha = 1; 
						}
						else if (posto.CodLinha == 43)
						{
							codLinha = 2;  
						}
						else if (posto.CodLinha == 44)
						{
							codLinha = 3;
						}

						using (DtbSsaContext contextoSSA = new DtbSsaContext())
						{
							tbl_Historico historico = contextoSSA.TblHistorico.Where(a => a.desSerie == passagem.NumECB).FirstOrDefault();
							historico.codAtual = 30;
							historico.codOpe3 = int.Parse(CodDRT);

							historico.codPointControl = 2;
							historico.firstDat3 = DateTime.Now;

							historico.dat3 = DateTime.Now;
							historico.codLinha = codLinha;
							contextoSSA.SaveChanges();
						}
					}

					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		public static Boolean InserePassagemProcedure(tbl_CBPassagem passagem, string CodDRT, DadosPosto posto)
		{
			try
			{
				using (var contexto    = new DTB_CBContext())
				using (var contextoSSA = new DtbSsaContext())
				{
					bool erro = false;
					byte codLinha = 0;

					if (posto.CodTipoPosto == 20 || posto.CodTipoPosto == 23 || posto.CodTipoPosto == 27)
					{
						if (posto.CodLinha == 42)
						{
							codLinha = 1;
						}
						else if (posto.CodLinha == 43)
						{
							codLinha = 2;
						}
						else if (posto.CodLinha == 44)
						{
							codLinha = 3;
						}

						var resultadoHistorico = contextoSSA.Database.SqlQuery<spc_CBFecGravarPassagem>(
							"EXEC spc_SISAP_AtualizarHistoricoInformatica @NumSerie, @CodAtual, @CodOpe3, @CodPointControl, @CodLinha",
							new SqlParameter("NumSerie", passagem.NumECB),
							new SqlParameter("CodAtual", 30),
							new SqlParameter("CodOpe3", int.Parse(CodDRT)),
							new SqlParameter("CodPointControl", 2),
							new SqlParameter("CodLinha", codLinha)
							);

						var retornoInfor = resultadoHistorico.FirstOrDefault();

						if (retornoInfor.Result != 0)
						{
							erro = true;
						}
					}

					if (erro == false)
					{

						if (passagem.CodFab != null && passagem.NumAP != null)
						{
							var resultado = contexto.Database.SqlQuery<spc_CBFecGravarPassagem>(
									"EXEC spc_CBFecGravarPassagem @CodLinha, @NumPosto, @NumSerie, @DRT, @CodFab, @NumAp",
									new SqlParameter("CodLinha", passagem.CodLinha),
									new SqlParameter("NumPosto", passagem.NumPosto),
									new SqlParameter("NumSerie", passagem.NumECB),
									new SqlParameter("DRT", passagem.CodDRT),
									new SqlParameter("CodFab", passagem.CodFab),
									new SqlParameter("NumAp", passagem.NumAP)
									);
							contexto.SaveChanges();

							var retorno = resultado.FirstOrDefault();

							if (retorno.Result == 0)
							{
								return true;
							}
							else
							{
								return false;
							}
						}
						else
						{
							var resultado = contexto.Database.SqlQuery<spc_CBFecGravarPassagem>(
									"EXEC spc_CBFecGravarPassagem @CodLinha, @NumPosto, @NumSerie, @DRT",// @CodFab, @NumAp",
									new SqlParameter("CodLinha", passagem.CodLinha),
									new SqlParameter("NumPosto", passagem.NumPosto),
									new SqlParameter("NumSerie", passagem.NumECB),
									new SqlParameter("DRT", passagem.CodDRT)//,
								//new SqlParameter("CodFab", passagem.CodFab),
								//new SqlParameter("NumAp", passagem.NumAP)
									);
							contexto.SaveChanges();

							var retorno = resultado.FirstOrDefault();

							if (retorno.Result == 0)
							{
								return true;
							}
							else
							{
								return false;
							}
						}

					}
					else
					{
						return false;
					}
				}
			}
			catch
			{
				return false;
			}
		}


		/// <summary>
		/// Insere os dados do produto na tabela de histórico tbl_CBPassagem_Hist.
		/// </summary>
		/// <param name="passagem"></param>
		/// <returns></returns>
		public static Boolean InserePassagemHistorico(tbl_CBPassagem_Hist passagem)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				try
				{
					dtb_CB.tbl_CBPassagem_Hist.Add(passagem);
					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		/// <summary>
		/// Insere identificação do produto na tabela tbl_CBIdentifica.
		/// </summary>
		/// <param name="identifica"></param>
		/// <returns></returns>
		public static Boolean InsereIdentifica(tbl_CBIdentifica identifica)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				try
				{
					dtb_CB.tbl_CBIdentifica.Add(identifica);
					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		/// <summary>
		/// Remove os dados de um produto da tabela tbl_CBPassagem
		/// </summary>
		/// <param name="passagem"></param>
		/// <returns></returns>
		public static Boolean RemoverPassagem(tbl_CBPassagem passagem)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				tbl_CBPassagem instancia = new tbl_CBPassagem();

				instancia = dtb_CB.tbl_CBPassagem.FirstOrDefault(a => a.NumECB == passagem.NumECB && a.CodLinha == passagem.CodLinha && a.NumPosto == passagem.NumPosto);
				try
				{
					dtb_CB.tbl_CBPassagem.Remove(instancia);
					dtb_CB.SaveChanges();
					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}
		}

		/// <summary>
		/// Remove da tabela tbl_CBPassagem o registro de passagem do número de série e inclui o mesmo na tabela histórico tbl_CBPassagem_Hist.
		/// </summary>
		/// <param name="NumECB"></param>
		/// <param name="CodLinha"></param>
		/// <param name="NumPosto"></param>
		/// <returns></returns>
		public static Boolean CancelamentoLeitura(string NumECB, int CodLinha, int NumPosto)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var evento = (from registro in dtb_CB.tbl_CBPassagem
							  where NumECB == registro.NumECB && CodLinha == registro.CodLinha && NumPosto == registro.NumPosto
							  select registro).FirstOrDefault();

				if (evento != null)
				{
					tbl_CBPassagem_Hist passagemHist = new tbl_CBPassagem_Hist();
					tbl_CBPassagem passagem = new tbl_CBPassagem();

					passagemHist.NumECB = evento.NumECB;
					passagemHist.CodLinha = evento.CodLinha;
					passagemHist.NumPosto = evento.NumPosto;
					passagemHist.CodDRT = evento.CodDRT;
					passagemHist.DatEvento = evento.DatEvento;

					Boolean gravar = InserePassagemHistorico(passagemHist);

					if (gravar == true)
					{
						passagem.NumECB = passagemHist.NumECB;
						passagem.CodLinha = passagemHist.CodLinha;
						passagem.NumPosto = passagemHist.NumPosto;
						passagem.CodDRT = passagemHist.CodDRT;
						passagem.DatEvento = passagemHist.DatEvento;
						return daoPassagem.RemoverPassagem(passagem);
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}

		}

		#endregion

	}

	/// <summary>
	/// Objeto que foi criado pra receber dados da procedure spc_CBFecValidaErros
	/// </summary>
	public class spc_CBFecValidaErros
	{
		public int CodErro { get; set; }
		public string Erro { get; set; }
	}

	/// <summary>
	/// Objeto que foi criado pra receber dados da procedure spc_Coleta001
	/// </summary>
	public class spc_Coleta001
	{
		public string CodFab { get; set; }
		public string NumAP { get; set; }
		public string CodStatus { get; set; }
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public string CodLinha { get; set; }
		public string CodCin { get; set; }
		public string DesCin { get; set; }
		public string CodProcesso { get; set; }

		public int QtdLoteAP { get; set; }
		public int QtdProduzida { get; set; }
		public int? QtdEmbalado { get; set; }

		public string Mascara { get; set; }
		public string FlgDivida { get; set; }
		public DateTime? DatReferencia { get; set; }
		public DateTime? DatDivida { get; set; }
		public string TipAP { get; set; }
		public string FlgQuebra { get; set; }
		public string CodFam { get; set; }

	}

	public class spc_GePro_CalcularQtdePrevistaReal
	{
		public int Producao_Horaria_Real { get; set; }
		public int Producao_Diaria_Real { get; set; }
		public int Qtde_Montagem_Real { get; set; }
		public int Producao_Horaria_Prevista { get; set; }
		public int Producao_Diaria_Prevista { get; set; }
		public int Qtde_Montagem_Prevista { get; set; }
	}

	public class spc_CBFecGravarPassagem
	{
		public int Result { get; set; }
	}
}
