namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_proaltglo
    {
        [Key]
        public long id_proalt { get; set; }

        public long id_produtoMae { get; set; }

        public long id_produtoFilho { get; set; }

        public long? id_produtosemiacabado { get; set; }

        [StringLength(100)]
        public string posicao { get; set; }

        public decimal? qt { get; set; }

        public int? prioridade { get; set; }

        public virtual om_produto om_produto { get; set; }

        public virtual om_produto om_produto1 { get; set; }

        public virtual om_produto om_produto2 { get; set; }

        public virtual om_produto om_produto3 { get; set; }
    }
}
