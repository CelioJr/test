using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_PlanoIACItemMap : EntityTypeConfiguration<tbl_PlanoIACItem>
    {
        public tbl_PlanoIACItemMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodModelo, t.NumEstacaoZ, t.NumPosicao, t.NumEtapa, t.NumRevisao, t.CodProcesso, t.rowguid, t.FlgLeftRight });

            // Properties
            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumEstacaoZ)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumPosicao)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.NumEtapa)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NumRevisao)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodProcesso)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.CodItem)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.Polaridade)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.FlgLeftRight)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DesLeftRight)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_PlanoIACItem");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.NumEstacaoZ).HasColumnName("NumEstacaoZ");
            this.Property(t => t.NumPosicao).HasColumnName("NumPosicao");
            this.Property(t => t.NumEtapa).HasColumnName("NumEtapa");
            this.Property(t => t.NumRevisao).HasColumnName("NumRevisao");
            this.Property(t => t.CodProcesso).HasColumnName("CodProcesso");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.Polaridade).HasColumnName("Polaridade");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
            this.Property(t => t.FlgLeftRight).HasColumnName("FlgLeftRight");
            this.Property(t => t.DesLeftRight).HasColumnName("DesLeftRight");
        }
    }
}
