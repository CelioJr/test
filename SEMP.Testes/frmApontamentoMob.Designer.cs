﻿namespace SEMP.Testes
{
	partial class frmApontamentoMOB
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.grdDados = new System.Windows.Forms.DataGridView();
			this.btnExecutar = new System.Windows.Forms.Button();
			this.txtLote = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.txtOrdem = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.txtQuantidade = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.txtPeso = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.txtCodLinha = new System.Windows.Forms.TextBox();
			this.txtNECOD1 = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.lblFields = new System.Windows.Forms.Label();
			this.txtNECOD2 = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.chkPing = new System.Windows.Forms.CheckBox();
			this.label9 = new System.Windows.Forms.Label();
			this.txtQtdRegistros = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.txtPosto = new System.Windows.Forms.TextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.txtCPF = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.txtPESO_ITEM = new System.Windows.Forms.TextBox();
			this.label16 = new System.Windows.Forms.Label();
			this.txtDATA_REAL_PROD = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.txtHORA_REAL_PROD = new System.Windows.Forms.TextBox();
			this.label14 = new System.Windows.Forms.Label();
			this.txtIMEICOD1 = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.txtIMEICOD2 = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.txtSERIAL = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.btnAdicionar = new System.Windows.Forms.Button();
			this.txtRetorno = new System.Windows.Forms.RichTextBox();
			this.grdLotes = new System.Windows.Forms.DataGridView();
			this.label17 = new System.Windows.Forms.Label();
			this.btnCarregarLotes = new System.Windows.Forms.Button();
			this.btnCarregarGRPS = new System.Windows.Forms.Button();
			this.grdGRP = new System.Windows.Forms.DataGridView();
			this.label18 = new System.Windows.Forms.Label();
			this.btnSeriais = new System.Windows.Forms.Button();
			this.txtGRP = new System.Windows.Forms.TextBox();
			this.btnAddGRP = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.grdDados)).BeginInit();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.grdLotes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.grdGRP)).BeginInit();
			this.SuspendLayout();
			// 
			// grdDados
			// 
			this.grdDados.AllowUserToAddRows = false;
			this.grdDados.AllowUserToDeleteRows = false;
			this.grdDados.AllowUserToOrderColumns = true;
			this.grdDados.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.grdDados.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
			this.grdDados.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
			this.grdDados.BackgroundColor = System.Drawing.Color.White;
			this.grdDados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.grdDados.Location = new System.Drawing.Point(365, 147);
			this.grdDados.Name = "grdDados";
			this.grdDados.ReadOnly = true;
			this.grdDados.RowHeadersVisible = false;
			this.grdDados.Size = new System.Drawing.Size(492, 164);
			this.grdDados.TabIndex = 0;
			this.grdDados.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.grdDados_DataBindingComplete);
			// 
			// btnExecutar
			// 
			this.btnExecutar.Location = new System.Drawing.Point(786, 12);
			this.btnExecutar.Name = "btnExecutar";
			this.btnExecutar.Size = new System.Drawing.Size(75, 23);
			this.btnExecutar.TabIndex = 1;
			this.btnExecutar.Text = "Executar";
			this.btnExecutar.UseVisualStyleBackColor = true;
			this.btnExecutar.Click += new System.EventHandler(this.btnConsultar_Click);
			// 
			// txtLote
			// 
			this.txtLote.Location = new System.Drawing.Point(47, 12);
			this.txtLote.MaxLength = 18;
			this.txtLote.Name = "txtLote";
			this.txtLote.Size = new System.Drawing.Size(116, 20);
			this.txtLote.TabIndex = 2;
			this.txtLote.Text = "A00000000000000010";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(35, 13);
			this.label1.TabIndex = 3;
			this.label1.Text = "LOTE";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(183, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(47, 13);
			this.label2.TabIndex = 5;
			this.label2.Text = "ORDEM";
			// 
			// txtOrdem
			// 
			this.txtOrdem.Location = new System.Drawing.Point(230, 12);
			this.txtOrdem.MaxLength = 12;
			this.txtOrdem.Name = "txtOrdem";
			this.txtOrdem.Size = new System.Drawing.Size(85, 20);
			this.txtOrdem.TabIndex = 4;
			this.txtOrdem.Text = "4000001467";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(328, 16);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(78, 13);
			this.label3.TabIndex = 7;
			this.label3.Text = "QUANTIDADE";
			// 
			// txtQuantidade
			// 
			this.txtQuantidade.Location = new System.Drawing.Point(412, 12);
			this.txtQuantidade.MaxLength = 3;
			this.txtQuantidade.Name = "txtQuantidade";
			this.txtQuantidade.Size = new System.Drawing.Size(39, 20);
			this.txtQuantidade.TabIndex = 6;
			this.txtQuantidade.Text = "10";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(469, 16);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(36, 13);
			this.label4.TabIndex = 9;
			this.label4.Text = "PESO";
			// 
			// txtPeso
			// 
			this.txtPeso.Location = new System.Drawing.Point(511, 12);
			this.txtPeso.MaxLength = 6;
			this.txtPeso.Name = "txtPeso";
			this.txtPeso.Size = new System.Drawing.Size(47, 20);
			this.txtPeso.TabIndex = 8;
			this.txtPeso.Text = "1000";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(575, 16);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(39, 13);
			this.label5.TabIndex = 11;
			this.label5.Text = "LINHA";
			// 
			// txtCodLinha
			// 
			this.txtCodLinha.Location = new System.Drawing.Point(620, 12);
			this.txtCodLinha.MaxLength = 2;
			this.txtCodLinha.Name = "txtCodLinha";
			this.txtCodLinha.Size = new System.Drawing.Size(28, 20);
			this.txtCodLinha.TabIndex = 10;
			this.txtCodLinha.Text = "71";
			// 
			// txtNECOD1
			// 
			this.txtNECOD1.Location = new System.Drawing.Point(50, 21);
			this.txtNECOD1.MaxLength = 18;
			this.txtNECOD1.Multiline = true;
			this.txtNECOD1.Name = "txtNECOD1";
			this.txtNECOD1.Size = new System.Drawing.Size(58, 23);
			this.txtNECOD1.TabIndex = 14;
			this.txtNECOD1.Text = "940074";
			this.txtNECOD1.WordWrap = false;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(13, 26);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(31, 13);
			this.label7.TabIndex = 15;
			this.label7.Text = "NE 1";
			// 
			// lblFields
			// 
			this.lblFields.AutoSize = true;
			this.lblFields.Location = new System.Drawing.Point(13, 53);
			this.lblFields.Name = "lblFields";
			this.lblFields.Size = new System.Drawing.Size(31, 13);
			this.lblFields.TabIndex = 17;
			this.lblFields.Text = "NE 2";
			// 
			// txtNECOD2
			// 
			this.txtNECOD2.Location = new System.Drawing.Point(50, 50);
			this.txtNECOD2.MaxLength = 18;
			this.txtNECOD2.Multiline = true;
			this.txtNECOD2.Name = "txtNECOD2";
			this.txtNECOD2.Size = new System.Drawing.Size(58, 23);
			this.txtNECOD2.TabIndex = 16;
			this.txtNECOD2.WordWrap = false;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(366, 129);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(33, 13);
			this.label8.TabIndex = 18;
			this.label8.Text = "ITEM";
			// 
			// chkPing
			// 
			this.chkPing.AutoSize = true;
			this.chkPing.Enabled = false;
			this.chkPing.Location = new System.Drawing.Point(781, 44);
			this.chkPing.Name = "chkPing";
			this.chkPing.Size = new System.Drawing.Size(76, 17);
			this.chkPing.TabIndex = 19;
			this.chkPing.Text = "PING SAP";
			this.chkPing.UseVisualStyleBackColor = true;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(737, 124);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(74, 13);
			this.label9.TabIndex = 21;
			this.label9.Text = "Qtd. Registros";
			// 
			// txtQtdRegistros
			// 
			this.txtQtdRegistros.Location = new System.Drawing.Point(813, 121);
			this.txtQtdRegistros.Name = "txtQtdRegistros";
			this.txtQtdRegistros.Size = new System.Drawing.Size(48, 20);
			this.txtQtdRegistros.TabIndex = 20;
			this.txtQtdRegistros.Text = "0";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(655, 16);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(44, 13);
			this.label6.TabIndex = 23;
			this.label6.Text = "POSTO";
			// 
			// txtPosto
			// 
			this.txtPosto.Location = new System.Drawing.Point(705, 12);
			this.txtPosto.MaxLength = 2;
			this.txtPosto.Name = "txtPosto";
			this.txtPosto.Size = new System.Drawing.Size(28, 20);
			this.txtPosto.TabIndex = 22;
			this.txtPosto.Text = "12";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.txtCPF);
			this.groupBox1.Controls.Add(this.label15);
			this.groupBox1.Controls.Add(this.txtPESO_ITEM);
			this.groupBox1.Controls.Add(this.label16);
			this.groupBox1.Controls.Add(this.txtDATA_REAL_PROD);
			this.groupBox1.Controls.Add(this.label11);
			this.groupBox1.Controls.Add(this.txtHORA_REAL_PROD);
			this.groupBox1.Controls.Add(this.label14);
			this.groupBox1.Controls.Add(this.txtIMEICOD1);
			this.groupBox1.Controls.Add(this.label12);
			this.groupBox1.Controls.Add(this.txtIMEICOD2);
			this.groupBox1.Controls.Add(this.label13);
			this.groupBox1.Controls.Add(this.txtSERIAL);
			this.groupBox1.Controls.Add(this.label10);
			this.groupBox1.Controls.Add(this.btnAdicionar);
			this.groupBox1.Controls.Add(this.txtNECOD1);
			this.groupBox1.Controls.Add(this.label7);
			this.groupBox1.Controls.Add(this.txtNECOD2);
			this.groupBox1.Controls.Add(this.lblFields);
			this.groupBox1.Location = new System.Drawing.Point(9, 42);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(690, 84);
			this.groupBox1.TabIndex = 24;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Cadastrar Item";
			// 
			// txtCPF
			// 
			this.txtCPF.Location = new System.Drawing.Point(431, 21);
			this.txtCPF.MaxLength = 15;
			this.txtCPF.Multiline = true;
			this.txtCPF.Name = "txtCPF";
			this.txtCPF.Size = new System.Drawing.Size(83, 23);
			this.txtCPF.TabIndex = 31;
			this.txtCPF.Text = "74676083220";
			this.txtCPF.WordWrap = false;
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(394, 26);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(27, 13);
			this.label15.TabIndex = 32;
			this.label15.Text = "CPF";
			// 
			// txtPESO_ITEM
			// 
			this.txtPESO_ITEM.Location = new System.Drawing.Point(431, 48);
			this.txtPESO_ITEM.MaxLength = 6;
			this.txtPESO_ITEM.Multiline = true;
			this.txtPESO_ITEM.Name = "txtPESO_ITEM";
			this.txtPESO_ITEM.Size = new System.Drawing.Size(83, 23);
			this.txtPESO_ITEM.TabIndex = 33;
			this.txtPESO_ITEM.Text = "100";
			this.txtPESO_ITEM.WordWrap = false;
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(394, 51);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(36, 13);
			this.label16.TabIndex = 34;
			this.label16.Text = "PESO";
			// 
			// txtDATA_REAL_PROD
			// 
			this.txtDATA_REAL_PROD.Location = new System.Drawing.Point(307, 21);
			this.txtDATA_REAL_PROD.MaxLength = 10;
			this.txtDATA_REAL_PROD.Multiline = true;
			this.txtDATA_REAL_PROD.Name = "txtDATA_REAL_PROD";
			this.txtDATA_REAL_PROD.Size = new System.Drawing.Size(83, 23);
			this.txtDATA_REAL_PROD.TabIndex = 27;
			this.txtDATA_REAL_PROD.Text = "28/11/2018";
			this.txtDATA_REAL_PROD.WordWrap = false;
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(265, 26);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(36, 13);
			this.label11.TabIndex = 28;
			this.label11.Text = "DATA";
			// 
			// txtHORA_REAL_PROD
			// 
			this.txtHORA_REAL_PROD.Location = new System.Drawing.Point(307, 48);
			this.txtHORA_REAL_PROD.MaxLength = 8;
			this.txtHORA_REAL_PROD.Multiline = true;
			this.txtHORA_REAL_PROD.Name = "txtHORA_REAL_PROD";
			this.txtHORA_REAL_PROD.Size = new System.Drawing.Size(83, 23);
			this.txtHORA_REAL_PROD.TabIndex = 29;
			this.txtHORA_REAL_PROD.Text = "08:50:00";
			this.txtHORA_REAL_PROD.WordWrap = false;
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(265, 53);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(38, 13);
			this.label14.TabIndex = 30;
			this.label14.Text = "HORA";
			// 
			// txtIMEICOD1
			// 
			this.txtIMEICOD1.Location = new System.Drawing.Point(161, 21);
			this.txtIMEICOD1.MaxLength = 18;
			this.txtIMEICOD1.Multiline = true;
			this.txtIMEICOD1.Name = "txtIMEICOD1";
			this.txtIMEICOD1.Size = new System.Drawing.Size(98, 23);
			this.txtIMEICOD1.TabIndex = 23;
			this.txtIMEICOD1.Text = "353541101051035";
			this.txtIMEICOD1.WordWrap = false;
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(117, 26);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(38, 13);
			this.label12.TabIndex = 24;
			this.label12.Text = "IMEI 1";
			// 
			// txtIMEICOD2
			// 
			this.txtIMEICOD2.Location = new System.Drawing.Point(161, 50);
			this.txtIMEICOD2.MaxLength = 18;
			this.txtIMEICOD2.Multiline = true;
			this.txtIMEICOD2.Name = "txtIMEICOD2";
			this.txtIMEICOD2.Size = new System.Drawing.Size(98, 23);
			this.txtIMEICOD2.TabIndex = 25;
			this.txtIMEICOD2.Text = "353541101051060";
			this.txtIMEICOD2.WordWrap = false;
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(117, 53);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(38, 13);
			this.label13.TabIndex = 26;
			this.label13.Text = "IMEI 2";
			// 
			// txtSERIAL
			// 
			this.txtSERIAL.Location = new System.Drawing.Point(572, 21);
			this.txtSERIAL.MaxLength = 20;
			this.txtSERIAL.Multiline = true;
			this.txtSERIAL.Name = "txtSERIAL";
			this.txtSERIAL.Size = new System.Drawing.Size(108, 23);
			this.txtSERIAL.TabIndex = 19;
			this.txtSERIAL.Text = "FXLF01J1QOBSCD0";
			this.txtSERIAL.WordWrap = false;
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(526, 26);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(45, 13);
			this.label10.TabIndex = 20;
			this.label10.Text = "SERIAL";
			// 
			// btnAdicionar
			// 
			this.btnAdicionar.Location = new System.Drawing.Point(609, 50);
			this.btnAdicionar.Name = "btnAdicionar";
			this.btnAdicionar.Size = new System.Drawing.Size(75, 23);
			this.btnAdicionar.TabIndex = 18;
			this.btnAdicionar.Text = "Adicionar";
			this.btnAdicionar.UseVisualStyleBackColor = true;
			this.btnAdicionar.Click += new System.EventHandler(this.btnAdicionar_Click);
			// 
			// txtRetorno
			// 
			this.txtRetorno.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtRetorno.Location = new System.Drawing.Point(12, 317);
			this.txtRetorno.Name = "txtRetorno";
			this.txtRetorno.Size = new System.Drawing.Size(848, 96);
			this.txtRetorno.TabIndex = 25;
			this.txtRetorno.Text = "";
			// 
			// grdLotes
			// 
			this.grdLotes.AllowUserToAddRows = false;
			this.grdLotes.AllowUserToDeleteRows = false;
			this.grdLotes.AllowUserToResizeColumns = false;
			this.grdLotes.AllowUserToResizeRows = false;
			this.grdLotes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.grdLotes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
			this.grdLotes.BackgroundColor = System.Drawing.Color.White;
			this.grdLotes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.grdLotes.Location = new System.Drawing.Point(12, 147);
			this.grdLotes.Name = "grdLotes";
			this.grdLotes.RowHeadersVisible = false;
			this.grdLotes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.grdLotes.Size = new System.Drawing.Size(125, 164);
			this.grdLotes.TabIndex = 26;
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(13, 131);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(65, 13);
			this.label17.TabIndex = 27;
			this.label17.Text = "Lote SIGMA";
			// 
			// btnCarregarLotes
			// 
			this.btnCarregarLotes.Location = new System.Drawing.Point(143, 146);
			this.btnCarregarLotes.Name = "btnCarregarLotes";
			this.btnCarregarLotes.Size = new System.Drawing.Size(92, 23);
			this.btnCarregarLotes.TabIndex = 28;
			this.btnCarregarLotes.Text = "Carregar Lista";
			this.btnCarregarLotes.UseVisualStyleBackColor = true;
			this.btnCarregarLotes.Click += new System.EventHandler(this.btnCarregarLotes_Click);
			// 
			// btnCarregarGRPS
			// 
			this.btnCarregarGRPS.Location = new System.Drawing.Point(143, 175);
			this.btnCarregarGRPS.Name = "btnCarregarGRPS";
			this.btnCarregarGRPS.Size = new System.Drawing.Size(92, 23);
			this.btnCarregarGRPS.TabIndex = 29;
			this.btnCarregarGRPS.Text = "Carregar GRPS";
			this.btnCarregarGRPS.UseVisualStyleBackColor = true;
			this.btnCarregarGRPS.Click += new System.EventHandler(this.btnCarregarGRPS_Click);
			// 
			// grdGRP
			// 
			this.grdGRP.AllowUserToAddRows = false;
			this.grdGRP.AllowUserToDeleteRows = false;
			this.grdGRP.AllowUserToResizeColumns = false;
			this.grdGRP.AllowUserToResizeRows = false;
			this.grdGRP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.grdGRP.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
			this.grdGRP.BackgroundColor = System.Drawing.Color.White;
			this.grdGRP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.grdGRP.Location = new System.Drawing.Point(242, 147);
			this.grdGRP.Name = "grdGRP";
			this.grdGRP.RowHeadersVisible = false;
			this.grdGRP.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.grdGRP.Size = new System.Drawing.Size(117, 164);
			this.grdGRP.TabIndex = 30;
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(239, 129);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(84, 13);
			this.label18.TabIndex = 31;
			this.label18.Text = "Caixas Coletivas";
			// 
			// btnSeriais
			// 
			this.btnSeriais.Location = new System.Drawing.Point(143, 204);
			this.btnSeriais.Name = "btnSeriais";
			this.btnSeriais.Size = new System.Drawing.Size(92, 23);
			this.btnSeriais.TabIndex = 32;
			this.btnSeriais.Text = "Carregar Seriais";
			this.btnSeriais.UseVisualStyleBackColor = true;
			this.btnSeriais.Click += new System.EventHandler(this.btnSeriais_Click);
			// 
			// txtGRP
			// 
			this.txtGRP.Location = new System.Drawing.Point(143, 233);
			this.txtGRP.MaxLength = 18;
			this.txtGRP.Multiline = true;
			this.txtGRP.Name = "txtGRP";
			this.txtGRP.Size = new System.Drawing.Size(92, 23);
			this.txtGRP.TabIndex = 33;
			this.txtGRP.WordWrap = false;
			this.txtGRP.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGRP_KeyDown);
			// 
			// btnAddGRP
			// 
			this.btnAddGRP.Location = new System.Drawing.Point(143, 262);
			this.btnAddGRP.Name = "btnAddGRP";
			this.btnAddGRP.Size = new System.Drawing.Size(92, 23);
			this.btnAddGRP.TabIndex = 34;
			this.btnAddGRP.Text = "Adicionar GRP";
			this.btnAddGRP.UseVisualStyleBackColor = true;
			this.btnAddGRP.Click += new System.EventHandler(this.btnAddGRP_Click);
			// 
			// frmApontamentoMOB
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(872, 425);
			this.Controls.Add(this.btnAddGRP);
			this.Controls.Add(this.txtGRP);
			this.Controls.Add(this.btnSeriais);
			this.Controls.Add(this.label18);
			this.Controls.Add(this.grdGRP);
			this.Controls.Add(this.btnCarregarGRPS);
			this.Controls.Add(this.btnCarregarLotes);
			this.Controls.Add(this.label17);
			this.Controls.Add(this.grdLotes);
			this.Controls.Add(this.txtRetorno);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.txtPosto);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.txtQtdRegistros);
			this.Controls.Add(this.chkPing);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.txtCodLinha);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.txtPeso);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.txtQuantidade);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.txtOrdem);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.txtLote);
			this.Controls.Add(this.btnExecutar);
			this.Controls.Add(this.grdDados);
			this.Location = new System.Drawing.Point(0, 50);
			this.Name = "frmApontamentoMOB";
			this.Text = "Consulta tabela SAP";
			this.Load += new System.EventHandler(this.frmApontamentoMOB_Load);
			((System.ComponentModel.ISupportInitialize)(this.grdDados)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.grdLotes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.grdGRP)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.DataGridView grdDados;
		private System.Windows.Forms.Button btnExecutar;
		private System.Windows.Forms.TextBox txtLote;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtOrdem;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtQuantidade;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtPeso;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox txtCodLinha;
		private System.Windows.Forms.TextBox txtNECOD1;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label lblFields;
		private System.Windows.Forms.TextBox txtNECOD2;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.CheckBox chkPing;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox txtQtdRegistros;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox txtPosto;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button btnAdicionar;
		private System.Windows.Forms.TextBox txtCPF;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.TextBox txtPESO_ITEM;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox txtDATA_REAL_PROD;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox txtHORA_REAL_PROD;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox txtIMEICOD1;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox txtIMEICOD2;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox txtSERIAL;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.RichTextBox txtRetorno;
		private System.Windows.Forms.DataGridView grdLotes;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Button btnCarregarLotes;
		private System.Windows.Forms.Button btnCarregarGRPS;
		private System.Windows.Forms.DataGridView grdGRP;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Button btnSeriais;
		private System.Windows.Forms.TextBox txtGRP;
		private System.Windows.Forms.Button btnAddGRP;
	}
}