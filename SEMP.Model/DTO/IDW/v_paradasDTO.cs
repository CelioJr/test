﻿using System;

namespace SEMP.Model.DTO
{
	public partial class v_paradasDTO
	{
		public string maquina { get; set; }
		public string codigo { get; set; }
		public string descricao { get; set; }
		public Nullable<DateTime> inicio { get; set; }
		public Nullable<DateTime> fim { get; set; }
		public Nullable<decimal> tempoComPeso { get; set; }
		public Nullable<decimal> tempoSemPeso { get; set; }
		
	}
}
