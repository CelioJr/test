namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolmolog
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_consolmolog()
        {
            dw_consolmooco = new HashSet<dw_consolmooco>();
        }

        [Key]
        public long id_consolmolog { get; set; }

        public DateTime? dthr_ilogin { get; set; }

        public int? ms_dthrilogin { get; set; }

        public DateTime? dthr_flogin { get; set; }

        public int? ms_dthrflogin { get; set; }

        public long id_usr { get; set; }

        public long? id_gt { get; set; }

        public long? id_pt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolmooco> dw_consolmooco { get; set; }

        public virtual om_gt om_gt { get; set; }

        public virtual om_pt om_pt { get; set; }

        public virtual om_usr om_usr { get; set; }
    }
}
