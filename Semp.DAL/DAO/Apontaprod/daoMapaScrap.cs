﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.DAL.Models;
using SEMP.Model.DTO;

namespace SEMP.DAL.DAO.Apontaprod
{
	public class daoMapaScrap
	{

        //static daoMapaScrap()
        //{
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<tbl_MAPScrapBaixa, MAPScrapBaixaDTO>();
        //    });
        //}

        public static List<MAPScrapBaixaDTO> ObterListagem(DateTime datInicio, DateTime datFim)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var datRef = datInicio.AddDays(-datInicio.Day).AddDays(1).AddMonths(-1);

				var resumo = (from scrap in dtbSim.tbl_MAPScrapBaixa 
								join item in dtbSim.tbl_Item on scrap.CodItem equals item.CodItem
								join modelo in dtbSim.tbl_Item on scrap.CodModelo equals modelo.CodItem
							    from estoque in dtbSim.tbl_Estoque.Where(x => scrap.CodItem == x.CodItem && x.CodFab == scrap.CodFabBxa && x.CodLocal == "801").DefaultIfEmpty()
								from preco in dtbSim.tbl_KcmNac.Where(x => x.CodFab == scrap.CodFabBxa && x.CodItem == scrap.CodItem && x.Data == datRef).DefaultIfEmpty()
								where scrap.DatReferencia >= datInicio && scrap.DatReferencia <= datFim
								select new MAPScrapBaixaDTO()
								{
									DatReferencia = scrap.DatReferencia,
									CodModelo = scrap.CodModelo,
									DesModelo = modelo.DesItem,
									CodItem = scrap.CodItem,
									DesItem = item.DesItem,
									QtdPerda = scrap.QtdPerda,
									CodFabBxa = scrap.CodFabBxa,
									DatBaixa = scrap.DatBaixa,
									DatGeracao = scrap.DatGeracao,
									IDScrap = scrap.IDScrap,
									NomUsuGerou = scrap.NomUsuGerou,
									QtdBaixa = scrap.QtdBaixa,
									QtdEstoque = estoque.QtdEstoque,
									ValTotal = scrap.QtdPerda * (preco.CustoB ?? 0)//scrap.ValTotal
								}).ToList();

				return resumo;

			}
		}

		public static MAPScrapBaixaDTO ObterMapa(int idScrap)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var mapa = dtbSim.tbl_MAPScrapBaixa.FirstOrDefault(x => x.IDScrap == idScrap);

				var mapaDTO = new MAPScrapBaixaDTO();

				AutoMapper.Mapper.Map(mapa, mapaDTO);

				mapaDTO.DesItem = daoGeral.ObterDescricaoItem(mapaDTO.CodItem);
				mapaDTO.DesModelo = daoGeral.ObterDescricaoItem(mapaDTO.CodModelo);

				return mapaDTO;

			}
		}

		public static bool VerificaData(DateTime data)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var mapa = dtbSim.tbl_MAPScrapBaixa.FirstOrDefault(x => x.DatReferencia == data);

				return mapa != null && mapa.CodItem != null;
			}
		}

		public static void Alterar(int idScrap, string CodItem, decimal QtdPerda)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var mapa = dtbSim.tbl_MAPScrapBaixa.FirstOrDefault(x => x.IDScrap == idScrap);

				if (mapa == null) return;

				mapa.CodItem = CodItem;
				mapa.QtdPerda = QtdPerda;

				dtbSim.SaveChanges();
			}
		}

		public static void GerarMapa(DateTime DatInicio, DateTime DatFim, string Usuario)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{

				var resumo = dtbSim.Database.SqlQuery<MAPScrapBaixaDTO>(
					"EXEC SPC_MAPGeraScrap @pDatRefInicio, @pDatRefFinal, @pCodTipo, @pUsuario",
					new SqlParameter("pDatRefInicio", DatInicio),
					new SqlParameter("pDatRefFinal", DatFim),
					new SqlParameter("pCodTipo", "G"),
					new SqlParameter("pUsuario", Usuario)
					).FirstOrDefault();

			}
		}

		public static void BaixarScrap(DateTime DatInicio, DateTime DatFim, string Usuario)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{

				var resumo = dtbSim.Database.SqlQuery<MAPScrapBaixaDTO>(
					"EXEC SPC_MAPGeraScrap @pDatRefInicio, @pDatRefFinal, @pCodTipo, @pUsuario",
					new SqlParameter("pDatRefInicio", DatInicio),
					new SqlParameter("pDatRefFinal", DatFim),
					new SqlParameter("pCodTipo", "B"),
					new SqlParameter("pUsuario", Usuario)
					).FirstOrDefault();

			}
		}

	}
}
