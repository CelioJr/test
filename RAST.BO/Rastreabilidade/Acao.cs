﻿using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAST.BO.Rastreabilidade
{
    public class Acao
    {
        public static List<CBAcaoDTO> Listar()
        {
            return daoAcao.Listar();
        }

        public static List<CBAcaoDTO> ObterAcoes(string descricao, string tipo)
        {
            return daoAcao.ObterAcoes(descricao, tipo);
        }

        public static CBAcaoDTO ObterAcao(int codAcao)
        {
            return daoAcao.ObterAcao(codAcao);
        }

        public static void Gravar(CBAcaoDTO acao)
        {
            daoAcao.Gravar(acao);
        }

        public static void Remover(CBAcaoDTO acao)
        {
            daoAcao.Remover(acao);
        }
    }
}
