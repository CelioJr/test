﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SEMP.Model.DTO;
using RAST.BO.Rastreabilidade;

namespace SEMP.Areas.Coleta.Controllers
{
    public class TempoModeloController : Controller
    {
        
        //
        // GET: /Coleta/TempoModelo/

        public ActionResult Index()
        {
            return View(TempoModelo.Lista());
        }

        //
        // GET: /Coleta/TempoModelo/Details/5

        public ActionResult Details(int id)
        {
            CBTempoModeloDTO tbl_cbtempomodelo = TempoModelo.DetalheItem(id);
            if (tbl_cbtempomodelo == null)
            {
                return HttpNotFound();
            }
            return View(tbl_cbtempomodelo);
        }

        //
        // GET: /Coleta/TempoModelo/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Coleta/TempoModelo/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CBTempoModeloDTO tbl_cbtempomodelo)
        {
            if (ModelState.IsValid)
            {
                var bln = TempoModelo.Criar(tbl_cbtempomodelo);
                
                if (bln)
                    return RedirectToAction("Index");
            }

            return View(tbl_cbtempomodelo);
        }

        //
        // GET: /Coleta/TempoModelo/Edit/5
        public ActionResult Edit(int id)
        {
            CBTempoModeloDTO tbl_cbtempomodelo = TempoModelo.DetalheItem(id);
            if (tbl_cbtempomodelo == null)
            {
                return HttpNotFound();
            }
            return View(tbl_cbtempomodelo);
        }

        //
        // POST: /Coleta/TempoModelo/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CBTempoModeloDTO tbl_cbtempomodelo)
        {
            if (ModelState.IsValid)
            {
                if (TempoModelo.Editar(tbl_cbtempomodelo))
                    return RedirectToAction("Index");
            }
            return View(tbl_cbtempomodelo);
        }

        //
        // GET: /Coleta/TempoModelo/Delete/5

        public ActionResult Delete(int id)
        {
            CBTempoModeloDTO tbl_cbtempomodelo = TempoModelo.DetalheItem(id);
            if (tbl_cbtempomodelo == null)
            {
                return HttpNotFound();
            }
            return View(tbl_cbtempomodelo);
        }

        //
        // POST: /Coleta/TempoModelo/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TempoModelo.Deletar(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            //db.Dispose();
            //base.Dispose(disposing);
        }
    }
}