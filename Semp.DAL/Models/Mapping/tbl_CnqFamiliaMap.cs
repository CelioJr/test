using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CnqFamiliaMap : EntityTypeConfiguration<tbl_CnqFamilia>
    {
        public tbl_CnqFamiliaMap()
        {
            // Primary Key
            this.HasKey(t => t.CodFam);

            // Properties
            this.Property(t => t.CodFam)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.NomFam)
                .HasMaxLength(20)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_CnqFamilia");
            this.Property(t => t.CodFam).HasColumnName("CodFam");
            this.Property(t => t.NomFam).HasColumnName("NomFam");
        }
    }
}
