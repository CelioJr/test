namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_img
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_img()
        {
            om_gt = new HashSet<om_gt>();
            om_obj = new HashSet<om_obj>();
        }

        [Key]
        public long id_img { get; set; }

        [StringLength(30)]
        public string cd_img { get; set; }

        public int? revisao { get; set; }

        public DateTime? dt_revisao { get; set; }

        public DateTime? dt_stativo { get; set; }

        public short? st_ativo { get; set; }

        public long id_usrstativo { get; set; }

        public long id_usrrevisao { get; set; }

        [StringLength(100)]
        public string ds_img { get; set; }

        [StringLength(255)]
        public string url_img { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_gt> om_gt { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_obj> om_obj { get; set; }
    }
}
