﻿using RAST.BO.Rastreabilidade;
using SEMP.Model.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEMP.Areas.Coleta.Controllers
{
    public class AcaoController : Controller
    {
        //
        // GET: /Coleta/Acao/
        public ActionResult Index(string descricao, string tipo)
        {
            var acoes = Acao.ObterAcoes(descricao, tipo);

            return View(acoes);
        }

        //
        // GET: /Coleta/Acao/Create
        public ActionResult Create()
        {
            List<string> listaTipo = new List<string>
				{
                    "Selecione",
					"APROVAÇÃO",
					"REPROVAÇÃO",
					"DESASSOCIAÇÃO",
					"SCRAP"
                };

            ViewBag.ListaTipo = listaTipo;

            return View();
        }

        //
        // POST: /Coleta/Acao/Create
        [HttpPost]
        public ActionResult Create(CBAcaoDTO acao)
        {
            var json = new
            {
                message = "Código de linha inválido.",
                retorno = false
            };

            try
            {
                var flgAtivo = Request.Form["FlgAtivo"];
                acao.FlgAtivo = flgAtivo == null ? false : true;

                Acao.Gravar(acao);

                var jsonData = new
                {
                    message = "Salvo com sucesso.",
                    retorno = true
                };

                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(json, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Coleta/Acao/Edit/5
        public ActionResult Edit(int id)
        {
            var acao = Acao.ObterAcao(id);

            List<string> listaTipo = new List<string>
				{
                    "Selecione",
					"APROVAÇÃO",
					"REPROVAÇÃO",
					"DESASSOCIAÇÃO",
					"SCRAP"
                };

            ViewBag.ListaTipo = listaTipo;

            return View(acao);
        }

        //
        // POST: /Coleta/Acao/Edit/5

        [HttpPost]
        public ActionResult Edit(CBAcaoDTO acao)
        {
            var json = new
            {
                message = "Código de linha inválido.",
                retorno = false
            };

            try
            {
                var flgAtivo = Request.Form["FlgAtivo"];
                acao.FlgAtivo = flgAtivo == null ? false : true;

                Acao.Gravar(acao);

                var jsonData = new
                {
                    message = "Salvo com sucesso.",
                    retorno = true
                };


                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(json, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Coleta/Acao/Delete/5
        public ActionResult Delete(int id)
        {
            var acao = Acao.ObterAcao(id);

            return View(acao);
        }

        //
        // POST: /Coleta/Acao/Delete/5
        [HttpPost]
        public ActionResult Delete(CBAcaoDTO acao)
        {
            try
            {
                Acao.Remover(acao);

                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }
    }
}
