using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEMP.Model.DTO
{
	public partial class CBPlanoAcaoTipoCausaDTO
	{
		[Display(Name = "C�digo")]
		public int CodTipoCausa { get; set; }
		[Display(Name = "Descri��o")]
		public string DesTipoCausa { get; set; }
	}
}
