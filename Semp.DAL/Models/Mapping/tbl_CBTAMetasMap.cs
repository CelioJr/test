using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBTAMetasMap : EntityTypeConfiguration<tbl_CBTAMetas>
    {
        public tbl_CBTAMetasMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFase, t.SubFase, t.CodModelo, t.Ordem });

            // Properties
            this.Property(t => t.CodFase)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.SubFase)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.CodModelo)
                .IsRequired()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.Ordem)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("tbl_CBTAMetas");
            this.Property(t => t.CodFase).HasColumnName("CodFase");
            this.Property(t => t.SubFase).HasColumnName("SubFase");
            this.Property(t => t.Meta).HasColumnName("Meta");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.Ordem).HasColumnName("Ordem");
        }
    }
}
