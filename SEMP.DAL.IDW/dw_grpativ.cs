namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_grpativ
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_grpativ()
        {
            dw_procativ = new HashSet<dw_procativ>();
        }

        [Key]
        public long id_grpativ { get; set; }

        [StringLength(60)]
        public string cd_grpativ { get; set; }

        public int? revisao { get; set; }

        [StringLength(256)]
        public string ds_grpativ { get; set; }

        public DateTime? dt_revisao { get; set; }

        public DateTime? dt_stativo { get; set; }

        public short? st_ativo { get; set; }

        public long id_usrRevisao { get; set; }

        public long id_usrstativo { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_procativ> dw_procativ { get; set; }
    }
}
