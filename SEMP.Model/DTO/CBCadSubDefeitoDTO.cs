﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.DTO
{
	public partial class CBCadSubDefeitoDTO
	{
		public string CodDefeito { get; set; }
		public int CodSubDefeito { get; set; }
		public string DesDefeito { get; set; }
		public string DesVerificacao { get; set; }
		public string flgAtivo { get; set; }
	}
}
