using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBLoteConfig
    {
        public int idLinha { get; set; }
        public Nullable<int> Qtde { get; set; }        
        public string CodDRT { get; set; }
        public Nullable<int> QtdeCC { get; set; }
		public Nullable<int> PercAmostras { get; set; }
		public Nullable<int> QtdAmostras { get; set; }
    }
}
