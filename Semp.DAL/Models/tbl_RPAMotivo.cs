using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_RPAMotivo
    {
        public string CodMotivo { get; set; }
        public string DesMotivo { get; set; }
        public string CodLocalOri { get; set; }
        public string CodLocalDest { get; set; }
        public System.Guid rowguid { get; set; }
    }
}
