using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_EntMovMap : EntityTypeConfiguration<tbl_EntMov>
    {
        public tbl_EntMovMap()
        {
            // Primary Key
            this.HasKey(t => new { t.NumRomaneio, t.CodOrigem, t.CodTipo, t.DatGerac });

            // Properties
            this.Property(t => t.NumRomaneio)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodOrigem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.CodFab)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodTipo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.NomUsuario)
                .HasMaxLength(50)
                .IsUnicode(false);

            this.Property(t => t.NomMaquina)
                .HasMaxLength(30)
                .IsUnicode(false);

            this.Property(t => t.CodStatus)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.UltImpUser)
                .HasMaxLength(20)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_EntMov");
            this.Property(t => t.NumRomaneio).HasColumnName("NumRomaneio");
            this.Property(t => t.CodOrigem).HasColumnName("CodOrigem");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.CodTipo).HasColumnName("CodTipo");
            this.Property(t => t.NomUsuario).HasColumnName("NomUsuario");
            this.Property(t => t.NomMaquina).HasColumnName("NomMaquina");
            this.Property(t => t.CodStatus).HasColumnName("CodStatus");
            this.Property(t => t.DatGerac).HasColumnName("DatGerac");
            this.Property(t => t.QtdImpressa).HasColumnName("QtdImpressa");
            this.Property(t => t.DtUltimaImp).HasColumnName("DtUltimaImp");
            this.Property(t => t.UltImpUser).HasColumnName("UltImpUser");
        }
    }
}
