using System;
using System.Collections.Generic;

namespace SEMP.DTB_CB.Models
{
    public partial class tbl_CBDefeitoRastreamento
    {
        public string NumECB { get; set; }
        public int CodLinha { get; set; }
        public int NumPosto { get; set; }
        public string CodDefeito { get; set; }
        public System.DateTime DatEvento { get; set; }
        public string NomUsuario { get; set; }
        //public string CodDRT { get; set; }
    }
}
