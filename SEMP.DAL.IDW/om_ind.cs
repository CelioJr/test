namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_ind
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_ind()
        {
            om_cfgabclim = new HashSet<om_cfgabclim>();
            om_indgt = new HashSet<om_indgt>();
            om_indpt = new HashSet<om_indpt>();
            om_indtppt = new HashSet<om_indtppt>();
        }

        [Key]
        public long id_ind { get; set; }

        [StringLength(30)]
        public string cd_ind { get; set; }

        public int? revisao { get; set; }

        [StringLength(100)]
        public string ds_ind { get; set; }

        [StringLength(10)]
        public string ds_curta { get; set; }

        public DateTime? dt_cadastro { get; set; }

        public DateTime? dt_revisao { get; set; }

        public DateTime? dt_stativo { get; set; }

        public short? st_ativo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfgabclim> om_cfgabclim { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_indgt> om_indgt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_indpt> om_indpt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_indtppt> om_indtppt { get; set; }
    }
}
