﻿using RAST.BO.ApontaProd;
using SEMP.Model.DTO;
using SEMP.Model.VO.Rastreabilidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEMP.Areas.Outros.Controllers
{
    public class TemposModeloController : Controller
    {
        // GET: Outros/TemposModelo
        public ActionResult Index()
        {
            return View();
        }


		public ActionResult Lista(string datIni, string datFim, string codModelo, int? codLinha, string horaInicio, string horaFim)
		{
			try
			{
				var dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
				var dFim = dIni.AddDays(1).AddMinutes(-1);

				if (!String.IsNullOrEmpty(datIni))
				{
					dIni = DateTime.Parse(datIni);
				}
				if (!String.IsNullOrEmpty(datFim))
				{
					dFim = DateTime.Parse(datFim);
				}

				var dados = codLinha != null ? ProducaoHoraAHoraBO.ObterTempoMedioModelo(dIni, dFim, codModelo, codLinha.Value, horaInicio, horaFim) ??
							new List<CBTempoMedioLinha>() : new List<CBTempoMedioLinha>();

				return View(dados);
			}
			catch
			{
				return View(new List<CBTempoMedioLinha>());
			}
		}

		public ActionResult Leituras(string datIni, string datFim, string codModelo, int? codLinha, string horaInicio, string horaFim)
		{

			try
			{
				var dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
				var dFim = dIni.AddDays(1).AddMinutes(-1);

				if (!String.IsNullOrEmpty(datIni))
				{
					dIni = DateTime.Parse(datIni);
				}
				if (!String.IsNullOrEmpty(datFim))
				{
					dFim = DateTime.Parse(datFim);
				}

				var dados = codLinha != null ? ProducaoHoraAHoraBO.ObterTempoMedioModelo(dIni, dFim, codModelo, codLinha.Value, horaInicio, horaFim) ??
							new List<CBTempoMedioLinha>() : new List<CBTempoMedioLinha>();

				return View(dados);
			}
			catch
			{
				return View(new List<CBTempoMedioLinha>());
			}

		}

    }
}