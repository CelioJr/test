﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.Models
{
	public class spc_PCP201b_19
	{
		public string CodFab { get; set; }
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public decimal? QtdTotalProg { get; set; }
		public decimal? QtdTotalAP { get; set; }
		public decimal? QtdTotalApont { get; set; }
		public decimal? SaldoInicial { get; set; }
		public decimal? P006 { get; set; }
		public decimal? P007 { get; set; }
		public decimal? P008 { get; set; }
		public decimal? P701 { get; set; }
		public decimal? P801 { get; set; }
		public decimal? P702 { get; set; }
		public decimal? P802 { get; set; }
		public decimal? P703 { get; set; }
		public decimal? P803 { get; set; }
		public decimal? P704 { get; set; }
		public decimal? P804 { get; set; }
		public decimal? P805 { get; set; }
		public decimal? P806 { get; set; }
		public decimal? P807 { get; set; }
		public decimal? P822 { get; set; }
		public string NomFam { get; set; }
		public string Fase { get; set; }
		public string CodAparelho { get; set; }
		public int Ordem { get; set; }
		public decimal? Total { get; set; }
	}
}
