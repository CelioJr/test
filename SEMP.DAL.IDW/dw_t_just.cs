namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_t_just
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_t_just()
        {
            dw_consolpaoco = new HashSet<dw_consolpaoco>();
            ms_pt_coleta = new HashSet<ms_pt_coleta>();
        }

        [Key]
        public long id_tjust { get; set; }

        public long? id_tppt { get; set; }

        [StringLength(30)]
        public string cd_tjust { get; set; }

        public int? revisao { get; set; }

        [StringLength(40)]
        public string ds_tjust { get; set; }

        public DateTime? dt_revisao { get; set; }

        public short? st_ativo { get; set; }

        public DateTime? dt_stativo { get; set; }

        public long? id_usrrevisao { get; set; }

        public long? id_usrstativo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolpaoco> dw_consolpaoco { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_pt_coleta> ms_pt_coleta { get; set; }
    }
}
