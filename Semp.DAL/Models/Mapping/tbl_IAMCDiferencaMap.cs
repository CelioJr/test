using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_IAMCDiferencaMap : EntityTypeConfiguration<tbl_IAMCDiferenca>
    {
        public tbl_IAMCDiferencaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodModelo, t.DatProducao, t.Fase });

            // Properties
            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.Fase)
                .IsRequired()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.MotivoDiferenca)
                .HasMaxLength(500)
                .IsUnicode(false);

            this.Property(t => t.FlgFechado)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_IAMCDiferenca");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.DatProducao).HasColumnName("DatProducao");
            this.Property(t => t.Fase).HasColumnName("Fase");
            this.Property(t => t.DatEstudo).HasColumnName("DatEstudo");
            this.Property(t => t.QtdProgramada).HasColumnName("QtdProgramada");
            this.Property(t => t.QtdProduzida).HasColumnName("QtdProduzida");
            this.Property(t => t.Diferenca).HasColumnName("Diferenca");
            this.Property(t => t.MotivoDiferenca).HasColumnName("MotivoDiferenca");
            this.Property(t => t.FlgFechado).HasColumnName("FlgFechado");
        }
    }
}
