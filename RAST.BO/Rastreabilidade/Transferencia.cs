﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.Model.DTO;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.DAL.Models;
using SEMP.Model.VO;
using SEMP.Model.VO.Rastreabilidade;
using SEMP.BO;
using System.Globalization;
using System.Text.RegularExpressions;

namespace RAST.BO.Rastreabilidade
{
	public class Transferencia
	{
		#region Validações

		public static Boolean VerificaLoteFechado(string numLote)
		{
			return daoTransferencia.VerificaLoteFechado(numLote);
		}

		public static string ObterNumeroLote(string NumECB)
		{
			return daoTransferencia.ObterNumeroLote(NumECB);
		}

		#endregion
	}
}
