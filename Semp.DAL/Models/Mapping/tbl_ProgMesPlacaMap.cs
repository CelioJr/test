using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_ProgMesPlacaMap : EntityTypeConfiguration<tbl_ProgMesPlaca>
    {
        public tbl_ProgMesPlacaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.DatProducao, t.CodPlaca, t.CodProcesso, t.CodModelo, t.CodAparelho });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodPlaca)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodProcesso)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.flgCobertura)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodAparelho)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_ProgMesPlaca");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.DatProducao).HasColumnName("DatProducao");
            this.Property(t => t.CodPlaca).HasColumnName("CodPlaca");
            this.Property(t => t.CodProcesso).HasColumnName("CodProcesso");
            this.Property(t => t.QtdPrograma).HasColumnName("QtdPrograma");
            this.Property(t => t.flgCobertura).HasColumnName("flgCobertura");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.CodAparelho).HasColumnName("CodAparelho");
            this.Property(t => t.QtdFrequencia).HasColumnName("QtdFrequencia");
            this.Property(t => t.QtdSaldoAnt).HasColumnName("QtdSaldoAnt");
        }
    }
}
