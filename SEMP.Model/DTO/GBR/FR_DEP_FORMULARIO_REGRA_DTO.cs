using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DEP_FORMULARIO_REGRA_DTO
    {
        public int FRM_CODIGO { get; set; }
        public int REG_COD { get; set; }
    }
}
