using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_SYS_FLAG_DTO
    {
        public string ID_FLAG { get; set; }
        public Nullable<int> NEXT_VALUE { get; set; }
    }
}
