﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade
{
	public class GraficoHistograma
	{
		public int id { get; set; }
		public string DesClasse { get; set; }
		public int Valor { get; set; }
		public int K { get; set; }

	}
}
