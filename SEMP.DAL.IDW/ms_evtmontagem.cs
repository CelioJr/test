namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ms_evtmontagem
    {
        [Key]
        public long id_evtmontagem { get; set; }

        [StringLength(256)]
        public string cb { get; set; }

        public int? ordem { get; set; }

        [StringLength(40)]
        public string dsMontagem { get; set; }

        public long id_evt { get; set; }

        [StringLength(60)]
        public string cd_produto { get; set; }

        public virtual ms_evt ms_evt { get; set; }
    }
}
