using System;

namespace SEMP.Model.DTO
{
    public partial class EstoquePCIDTO
    {
        public string CodFab { get; set; }
        public string CodItem { get; set; }
        public string CodLocal { get; set; }
        public decimal? QtdEstoque { get; set; }
        public DateTime? DatAtualizacao { get; set; }

		public string DesItem { get; set; }
	}
}
