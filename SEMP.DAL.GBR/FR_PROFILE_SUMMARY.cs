namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_PROFILE_SUMMARY
    {
        [Key]
        [StringLength(38)]
        public string PRS_CODIGO { get; set; }

        [Required]
        [StringLength(38)]
        public string PRI_CODIGO { get; set; }

        [Required]
        [StringLength(1024)]
        public string PRS_IDENTIFIER { get; set; }

        [StringLength(1024)]
        public string PRS_PARENT_IDENTIFIER { get; set; }

        [Required]
        [StringLength(1024)]
        public string PRS_DESCRIPTION { get; set; }

        public long? PRS_HITS { get; set; }

        public long? PRS_TOTAL { get; set; }

        public long? PRS_CHILDREN_TOTAL { get; set; }

        public long? PRS_MAX { get; set; }

        public long? PRS_CHILDREN_MAX { get; set; }

        public long? PRS_MIN { get; set; }

        public long? PRS_CHILDREN_MIN { get; set; }

        public long? PRS_AVG { get; set; }

        public long? PRS_CHILDREN_AVG { get; set; }

        [Required]
        [StringLength(1)]
        public string PRS_TYPE { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PRS_SHARED_TOTAL { get; set; }
    }
}
