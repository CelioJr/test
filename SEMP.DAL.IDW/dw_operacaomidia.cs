namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_operacaomidia
    {
        [Key]
        public long id_operacaomidia { get; set; }

        public int? ordem { get; set; }

        public long? id_operacao { get; set; }

        [Column(TypeName = "text")]
        public string ds_operacaomidia { get; set; }

        [Column(TypeName = "image")]
        public byte[] midia { get; set; }

        public virtual dw_operacao dw_operacao { get; set; }
    }
}
