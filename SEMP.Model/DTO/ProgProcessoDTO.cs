using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ProgProcessoDTO
    {
        public string CodTipo { get; set; }
        public string CodProcesso { get; set; }
        public string DesTipo { get; set; }
        public string DesTitulo { get; set; }
        public System.Guid rowguid { get; set; }
    }
}
