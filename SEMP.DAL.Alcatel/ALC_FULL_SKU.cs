namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_FULL_SKU")]
    public partial class ALC_FULL_SKU
    {
        [Key]
        [StringLength(60)]
        public string FULL_SKU { get; set; }

        public bool? AUXILIARY_1 { get; set; }

        public bool? AUXILIARY_2 { get; set; }

        public bool? BATTERY { get; set; }

        public bool? CHARGER { get; set; }

        public bool? MAIN_BOARD { get; set; }

        public bool? MEMORY { get; set; }

        public bool? SD_CARD { get; set; }

        public bool? SUB_BOARD { get; set; }

        public int? BOARD_QTY { get; set; }

        public bool? HAS_SD_CARD { get; set; }

        public bool? HAS_DTV { get; set; }
    }
}
