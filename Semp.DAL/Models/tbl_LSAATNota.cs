using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_LSAATNota
    {
        public string CodFab { get; set; }
        public string NumAT { get; set; }
        public string CodItem { get; set; }
        public string CodItemAlt { get; set; }
        public string Sequencia { get; set; }
        public Nullable<decimal> QtdNota { get; set; }
        public Nullable<decimal> PrecoUnitario { get; set; }
        public string CodNumTec { get; set; }
    }
}
