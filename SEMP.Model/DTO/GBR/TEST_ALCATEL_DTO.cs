using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class TEST_ALCATEL_DTO
    {
        public int ID_TEST { get; set; }
        public string TEST_NAME { get; set; }
    }
}
