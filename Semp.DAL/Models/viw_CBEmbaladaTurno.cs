using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class viw_CBEmbaladaTurno
    {
        public string NumECB { get; set; }
        public System.DateTime DatLeitura { get; set; }
        public int CodLinha { get; set; }
        public string NumLote { get; set; }
        public Nullable<System.DateTime> DatReferencia { get; set; }
        public string CodModelo { get; set; }
        public string CodLinhaFec { get; set; }
        public string NumAP { get; set; }
        public string CodOperador { get; set; }
        public string CodTestador { get; set; }
        public string CodFab { get; set; }
        public Nullable<int> NumPosto { get; set; }
        public string NumCC { get; set; }
        public Nullable<int> Turno { get; set; }
        public string CodLinhaT1 { get; set; }
    }
}
