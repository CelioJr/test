using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_COMPILADOR_DATABASE_DTO
    {
        public string CPL_DESCRITOR { get; set; }
        public string DBA_CODIGO { get; set; }
        public string CDB_SINTAXE { get; set; }
        public virtual FR_COMPILADOR_DTO FR_COMPILADOR { get; set; }
        public virtual FR_DATABASE_DTO FR_DATABASE { get; set; }
    }
}
