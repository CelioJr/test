using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_CQTF_DTO
    {
        public decimal ID_CQTF { get; set; }
        public string cqtfControl { get; set; }
        public Nullable<System.DateTime> dtEntry { get; set; }
        public Nullable<int> product { get; set; }
        public Nullable<System.DateTime> dtCheck { get; set; }
        public string remessa { get; set; }
    }
}
