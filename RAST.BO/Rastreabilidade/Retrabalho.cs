﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.DAL.Models;
using SEMP.Model.VO;
using SEMP.Model.VO.Rastreabilidade;
using SEMP.BO;
using SEMP.Model.DTO;
using SEMP.Model;

namespace RAST.BO.Rastreabilidade
{
	public class Retrabalho
	{
		#region Consulta

		/// <summary>
		/// Método responsável por retornar lista de retrabalhos cadastrados
		/// </summary>
		/// <returns></returns>
		public static List<CBRetrabalhoDTO> ObterRetrabalhos()
		{            
			return daoRetrabalho.ObterRetrabalhos();
		}

        public static List<CBRelatorioRetrabalhoDTO> ListarRetrabalho(DateTime datIni, DateTime datFim)
        {
            return daoRetrabalho.ListarRetrabalho(datIni, datFim);
        }

        public static int TotalCount(DateTime datIni, DateTime datFim)
        {
            return daoRetrabalho.TotalCount(datIni, datFim);
        }

		public static int UltimoCodRetrabalho()
		{
			return daoRetrabalho.ObterUltimoCodRetrabalho();
		}

		public static List<HistoricoReimpressaoDTO> ObterAPReimpressao(List<HistoricoReimpressaoDTO> historico)
		{
			return daoRetrabalho.ObterAPReimpressao(historico);
		}


        public static List<SIMRelatorioRetrabalhoDTO> ObterRetrabalhoSimPorData(DateTime DatInicio, DateTime DatFim)
        {

           var retrabalhoSim = daoRetrabalho.ObterRetrabalhoSimPorData(DatInicio, DatFim);

         //  var ns = retrabalhoSim.Select(x => x.NS).Distinct().ToList();

           return retrabalhoSim;

        }


		public static List<HistoricoReimpressaoDTO> ObterRetrabalhoInfoPorData(DateTime datIni, DateTime datFim)
		{
			var retrabalho = daoRetrabalho.ObterRetrabalhoInfoPorData(datIni, datFim);

			var codModelos = retrabalho.Select(x => x.CodModelo).Distinct().ToList();			

			foreach (var item in codModelos)
			{
				var desModelo = daoGeral.ObterDescricaoItem(item);
				foreach (var item2 in retrabalho)
				{
					if (item2.CodModelo == item)
					{
						item2.DesModelo = desModelo;
					}
				}
			}

			return retrabalho;
		}

		#endregion 
	}
}