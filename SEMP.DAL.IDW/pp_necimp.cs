namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_necimp
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pp_necimp()
        {
            pp_necimplog = new HashSet<pp_necimplog>();
            pp_necimpurl = new HashSet<pp_necimpurl>();
        }

        [Key]
        public long id_necimp { get; set; }

        [StringLength(30)]
        public string cd_necimp { get; set; }

        public int? revisao { get; set; }

        public long id_usrRevisao { get; set; }

        public long id_usrStativo { get; set; }

        [StringLength(256)]
        public string ds_necimp { get; set; }

        public short? st_ativo { get; set; }

        public DateTime? dt_revisao { get; set; }

        public DateTime? dt_stativo { get; set; }

        public short? tp_necimp { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_necimplog> pp_necimplog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_necimpurl> pp_necimpurl { get; set; }
    }
}
