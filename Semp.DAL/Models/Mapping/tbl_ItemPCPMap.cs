using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_ItemPCPMap : EntityTypeConfiguration<tbl_ItemPCP>
    {
        public tbl_ItemPCPMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodItem, t.rowguid });

            // Properties
            this.Property(t => t.CodItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodComp)
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.CodProg)
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.CodAgrup)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.CodTransporte)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.CodClasse)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.TipConsumo)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.FlgAleFixo)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.CodRastr)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.Usuario)
                .IsFixedLength()
                .HasMaxLength(25)
                .IsUnicode(false);

            this.Property(t => t.CodProgSeg)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.FlgFabEspecial)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.CodProcesso)
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.MovEstoque)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.FlgBaixaDiaria)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_ItemPCP");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.CodComp).HasColumnName("CodComp");
            this.Property(t => t.CodProg).HasColumnName("CodProg");
            this.Property(t => t.CodAgrup).HasColumnName("CodAgrup");
            this.Property(t => t.CodTransporte).HasColumnName("CodTransporte");
            this.Property(t => t.CodClasse).HasColumnName("CodClasse");
            this.Property(t => t.TipConsumo).HasColumnName("TipConsumo");
            this.Property(t => t.PerPerdas).HasColumnName("PerPerdas");
            this.Property(t => t.FlgAleFixo).HasColumnName("FlgAleFixo");
            this.Property(t => t.LeadMont).HasColumnName("LeadMont");
            this.Property(t => t.PontoPed).HasColumnName("PontoPed");
            this.Property(t => t.CodRastr).HasColumnName("CodRastr");
            this.Property(t => t.Usuario).HasColumnName("Usuario");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
            this.Property(t => t.CodProgSeg).HasColumnName("CodProgSeg");
            this.Property(t => t.FlgFabEspecial).HasColumnName("FlgFabEspecial");
            this.Property(t => t.CodProcesso).HasColumnName("CodProcesso");
            this.Property(t => t.MovEstoque).HasColumnName("MovEstoque");
            this.Property(t => t.FlgBaixaDiaria).HasColumnName("FlgBaixaDiaria");
        }
    }
}
