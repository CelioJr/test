namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_estlocalpro
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_estlocalpro()
        {
            dw_consolestlocalpro = new HashSet<dw_consolestlocalpro>();
            dw_consolestlocalprotemp = new HashSet<dw_consolestlocalprotemp>();
            dw_desalimpendcontag = new HashSet<dw_desalimpendcontag>();
            dw_estmov = new HashSet<dw_estmov>();
        }

        [Key]
        public long id_estlocalpro { get; set; }

        public long? id_estlocal { get; set; }

        public long? id_estpro { get; set; }

        public decimal? qt_entrada { get; set; }

        public decimal? qt_saida { get; set; }

        public decimal? qt_ajuste { get; set; }

        public long? id_cp { get; set; }

        public decimal? qt_total { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolestlocalpro> dw_consolestlocalpro { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolestlocalprotemp> dw_consolestlocalprotemp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_desalimpendcontag> dw_desalimpendcontag { get; set; }

        public virtual dw_estlocal dw_estlocal { get; set; }

        public virtual pp_cp pp_cp { get; set; }

        public virtual dw_estpro dw_estpro { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_estmov> dw_estmov { get; set; }
    }
}
