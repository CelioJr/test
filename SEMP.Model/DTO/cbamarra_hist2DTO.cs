using System;

namespace SEMP.Model.DTO
{
    /// <summary>
    /// Representa a tabela cbamarra_hist2 do banco de dados. 
    /// </summary>
    public class cbamarra_hist2DTO
    {
        public string NumSerie { get; set; }
        public string NumECB { get; set; }
        public string CodModelo { get; set; }
        public Nullable<int> FlgTipo { get; set; }
        public Nullable<System.DateTime> DatAmarra { get; set; }
        public Nullable<int> CodLinha { get; set; }
        public Nullable<int> NumPosto { get; set; }
        public string CodFab { get; set; }
        public string NumAP { get; set; }
        public string CodItem { get; set; }
    }
}
