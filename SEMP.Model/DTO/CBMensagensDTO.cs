namespace SEMP.Model.DTO
{
    public class CBMensagensDTO
    {
        public string CodMensagem { get; set; }
        public string DesMensagem { get; set; }
        public string DesCor { get; set; }
        public string DesSom { get; set; }

        public bool erro { get; set; }
    }
}
