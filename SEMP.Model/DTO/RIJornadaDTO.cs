using System;

namespace SEMP.Model.DTO
{
    public class RIJornadaDTO
    {
        public string CodJornada { get; set; }
        public Nullable<System.DateTime> NumCarga { get; set; }
        public Nullable<System.DateTime> HEntrada { get; set; }
        public Nullable<System.DateTime> HSaida { get; set; }
        public Nullable<System.DateTime> HSaidaRefeicao { get; set; }
        public Nullable<System.DateTime> HEntradaRefeicao { get; set; }
        public Nullable<System.DateTime> HEntradaAntes { get; set; }
        public Nullable<System.DateTime> HEntradaDepois { get; set; }
        public Nullable<System.DateTime> HSaidaAntes { get; set; }
        public Nullable<System.DateTime> HSaidaDepois { get; set; }
        public Nullable<System.DateTime> HAdicionalNoturnoIni { get; set; }
        public Nullable<System.DateTime> HAdicionalNoturnoFim { get; set; }
        public Nullable<System.DateTime> HoraExtra1 { get; set; }
        public Nullable<decimal> NumHoraExtra1 { get; set; }
        public Nullable<System.DateTime> HoraExtra2 { get; set; }
        public Nullable<decimal> NumHoraExtra2 { get; set; }
        public Nullable<System.DateTime> HoraExtra3 { get; set; }
        public Nullable<decimal> NumHoraExtra3 { get; set; }
        public Nullable<System.DateTime> HoraExtra4 { get; set; }
        public Nullable<decimal> NumHoraExtra4 { get; set; }
        public Nullable<System.DateTime> HoraExtra5 { get; set; }
        public Nullable<decimal> NumHoraExtra5 { get; set; }
        public string flgRegra1 { get; set; }
        public string flgRegra2 { get; set; }
        public string flgRegra3 { get; set; }
        public string flgRegra4 { get; set; }
        public string flgRegra5 { get; set; }
    }
}
