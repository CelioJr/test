﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.Relatorios;
using SEMP.Model;
using SEMP.Model.VO;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using SEMP.Model.DTO;
using SEMP.Models;
using SEMP.BO;

namespace SEMP.Areas.Coleta.Controllers
{
	[Authorize]
	public class RelatorioProducaoController : Controller
	{
		// GET: /Coleta/ResumoProducao/Relatorio/?DatInicio=&DatFim=&Turno=&GrupoLinha
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult Relatorio(String DatInicio, String DatFim, int? Turno, string Familia, String EsconderDef, string codLinha = "", string fase = "FEC")
		{
			try
			{
				int t = 1;
				if (Turno != null)
				{
					t = (int)Turno;
				}

				DateTime dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
				DateTime dFim = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));

				if (DatInicio != null && DatFim != null)
				{
					dIni = Convert.ToDateTime(DatInicio);
					dFim = Convert.ToDateTime(DatFim);
				}

				if (String.IsNullOrEmpty(Familia)) Familia = "";
				string grupoLinha = "";

				var linhas = new List<CBLinhaDTO>();

				if (fase == "FEC")
				{
					if (Familia == "LCD") grupoLinha = "101";
					if (Familia == "DVD") grupoLinha = "102";
					if (Familia == "AUD") grupoLinha = "104";
					if (Familia == "INF") grupoLinha = "103";

					if (!String.IsNullOrEmpty(codLinha))
					{
						linhas = RelatorioProducaoMF.ObterLinhasMF(grupoLinha, dIni).Where(x => x.CodLinhaT1 == codLinha).ToList();
					}
					else
					{
						linhas = RelatorioProducaoMF.ObterLinhasMF(grupoLinha, dIni);
					}

					linhas = linhas.OrderBy(x => x.CodLinhaT1).ToList();
				}
				else if (fase == "DAT")
				{
					linhas = RelatorioProducaoIMC.ObterLinhasSetorAll(dIni, fase).OrderBy(x => x.DesLinha).ToList();

					if (!String.IsNullOrEmpty(Familia))
					{
						//if (Familia != "DVD")
						linhas = linhas.Where(x => x.Familia == Familia).ToList();
						//else
						//	linhas = linhas.Where(x => x.Familia == Familia || x.Familia == "AUD").ToList();
					}
					if (!String.IsNullOrEmpty(codLinha))
					{
						linhas = linhas.Where(x => x.CodLinhaT1 == codLinha).ToList();
					}

				}
				else
				{
					linhas = RelatorioProducaoIMC.ObterLinhasIMC(dIni).OrderBy(x => x.DesLinha).ToList();

					if (!String.IsNullOrEmpty(Familia))
					{
						//if (Familia != "DVD")
							linhas = linhas.Where(x => x.Familia == Familia).ToList();
						//else
						//	linhas = linhas.Where(x => x.Familia == Familia || x.Familia == "AUD").ToList();
					}
					if (!String.IsNullOrEmpty(codLinha))
					{
						linhas = linhas.Where(x => x.CodLinhaT1 == codLinha).ToList();
					}

				}

				ViewBag.linhas = linhas;

				var resumo = fase == "FEC" ? RelatorioProducaoMF.ObterResumo(dIni, dFim, t, grupoLinha) : RelatorioProducaoIMC.ObterResumo(dIni, dFim, t, fase);
				ViewBag.paradas = fase == "FEC" ? RelatorioProducaoMF.ObterParadas(dIni, dFim, t, grupoLinha) : RelatorioProducaoIMC.ObterParadas(dIni, dFim, t);

				resumo = resumo.Select(x => new RelatorioProducao(){
					CodLinha = x.CodLinha,
					CodModelo = x.CodModelo,
					DesLinha = x.DesLinha,
					DesModelo = x.DesModelo,
					PercDiferenca = x.PercDiferenca,
					QtdCapacidade = x.QtdCapacidade,
					QtdPlano = x.QtdPlano,
					QtdRealizado = x.QtdRealizado
				}).Distinct().ToList();

				ViewBag.resumo = resumo;

				var defeitos = fase == "FEC" ? RelatorioProducaoMF.ObterDefeitos(dIni, dFim, t, Familia, EsconderDef) : RelatorioProducaoIMC.ObterDefeitos(dIni, dFim, t, fase);

				var horario = RAST.BO.Rastreabilidade.Geral.ObterHorarioPorID(t == 2 ? 3 : 1);

				dIni = Convert.ToDateTime(dIni.ToShortDateString() + " " + horario.HoraInicio);
				dFim = Convert.ToDateTime(dFim.ToShortDateString() + " " + horario.HoraFim);

				ViewBag.indiceDefeito = fase == "FEC" ? RelatorioProducaoMF.ObterIndiceDefeito(dIni, dFim, t, Familia) : RelatorioProducaoIMC.ObterIndiceDefeito(dIni, dFim, t, Familia, fase);
				ViewBag.defeitos = defeitos;

			}
			catch (Exception ex)
			{
                //Log.AppLog().ErrorException("\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name, ex);
                Log.AppLog().Error(ex, "\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name);
                ViewBag.Erro = "Erro ao consultar";
			}
			return View();
		}

		// GET: /Coleta/ResumoProducao/IMC/?DatInicio=&DatFim=&Turno=
		public ActionResult IMC(String DatInicio, String DatFim, int? Turno)
		{

			DateTime dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			DateTime dFim = dIni.AddDays(1).AddMinutes(-1);

			if (DatInicio != null && DatFim != null)
			{
				dIni = Convert.ToDateTime(DatInicio);
				dFim = Convert.ToDateTime(DatFim).AddDays(1).AddMinutes(-1);
			}

			int t = 1;
			if (Turno != null)
			{
				t = (int)Turno;
			}

			var linhas = RelatorioProducaoIMC.ObterLinhasIMC(dIni).OrderBy(x => x.DesLinha);
			ViewBag.linhas = linhas;
			ViewBag.resumo = RelatorioProducaoIMC.ObterResumo(dIni, dFim, t);
			ViewBag.paradas = RelatorioProducaoIMC.ObterParadas(dIni, dFim, t);
			ViewBag.defeitos = RelatorioProducaoIMC.ObterDefeitos(dIni, dFim, t, "IMC");


			return View();
		}

		// GET: /Coleta/ResumoProducao/IMC/?DatInicio=&DatFim=&Turno=
		public ActionResult IAC(String DatInicio, String DatFim, int? Turno)
		{
			DateTime dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			DateTime dFim = dIni.AddDays(1).AddMinutes(-1);

			if (DatInicio != null && DatFim != null)
			{
				dIni = Convert.ToDateTime(DatInicio);
				dFim = Convert.ToDateTime(DatFim).AddDays(1).AddMinutes(-1);
			}


			var resumo = RelatorioProducaoIAC.ObterResumoIAC(dIni, dFim);

			return View(resumo);
		}

		public ActionResult IACXLS(String DatInicio, String DatFim, int? Turno)
		{
			Response.AddHeader("content-disposition", "attachment; filename=RelatorioProducaoIAC.xls");
			Response.ContentType = "application/ms-excel";

			DateTime dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			DateTime dFim = dIni.AddDays(1).AddMinutes(-1);

			if (DatInicio != null && DatFim != null)
			{
				dIni = Convert.ToDateTime(DatInicio);
				dFim = Convert.ToDateTime(DatFim).AddDays(1).AddMinutes(-1);
			}

			var resumo = RelatorioProducaoIAC.ObterResumoIAC(dIni, dFim);

			return View(resumo);
		}

		// GET: /User/Edit/5
		public ActionResult EditIAC(DateTime Data, int Turno, string Linha, string CodModelo)
		{
			CBResumoIACDTO resumo = RelatorioProducaoIAC.ObterProducaoIAC(Data, Turno, Linha, CodModelo);
			if (resumo == null)
			{
				return HttpNotFound();
			}
			return View(resumo);
		} 

		// POST: /User/Edit/5
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult EditIAC(CBResumoIACDTO resumo)
		{
			resumo = RelatorioProducaoIAC.AtualizaResumo(resumo);

			return RedirectToAction("Index");
		}

		// POST: /User/Edit/5
		[HttpPost]
		public string EditObservacao(int CodLinha, string Observacao, String DatProducao)
		{
			try
			{
				DateTime data = DateTime.Parse(DatProducao);

				CBLinhaObservacaoDTO observacao = new CBLinhaObservacaoDTO()
				{
					CodLinha = CodLinha,
					DatProducao = data,
					DatModificacao = Util.GetDate(),
					NomUsuario = ((RetornoLogin)Session["RetornoLogin"]).NomUsuario,
					Observacao = Observacao
				};

				var retorno = RelatorioProducaoMF.GravaObservacaoLinha(observacao);

				return "OK";
			}
			catch (Exception ex)
			{
				return ex.Message;
			}

		}

		public string IncluirDefeito(int CodLinha, String DatProducao, string CodDefeito)
		{
			try
			{
				DateTime data = DateTime.Parse(DatProducao);

				RelatorioProducaoMF.IncluirDefeitoRelatorio(CodLinha, data, CodDefeito);

				return "OK";
			}
			catch (Exception ex)
			{
				return ex.Message;
			}

		}

		public string RemoveDefeito(int CodLinha, String DatProducao, string CodDefeito)
		{
			try
			{
				DateTime data = DateTime.Parse(DatProducao);

				RelatorioProducaoMF.RemoveDefeitoRelatorio(CodLinha, data, CodDefeito);

				return "OK";
			}
			catch (Exception ex)
			{
				return ex.Message;
			}

		}
	}
}
