using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DOC_FORMULARIO_COMANDO_VI_DTO
    {
        public int com_ordem { get; set; }
        public string com_botao { get; set; }
        public string com_acao { get; set; }
        public string com_restricao { get; set; }
        public int com_ativo { get; set; }
        public int frm_codigo { get; set; }
        public string sis_codigo { get; set; }
    }
}
