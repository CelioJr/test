using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class CBDestinatarioEmailDTO
    {
        public string CodRotina { get; set; }
        public string EmailDestinatario { get; set; }
        public string NomDestinatario { get; set; }
    }
}
