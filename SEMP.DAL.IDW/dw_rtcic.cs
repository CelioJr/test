namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_rtcic
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_rtcic()
        {
            dw_consolmedparamlog = new HashSet<dw_consolmedparamlog>();
            dw_consolpt = new HashSet<dw_consolpt>();
        }

        [Key]
        public long id_rtcic { get; set; }

        public DateTime? dthr_iciclo { get; set; }

        public int? ms_dthriciclo { get; set; }

        public DateTime? dthr_fciclo { get; set; }

        public int? ms_dthrfciclo { get; set; }

        public long id_rt { get; set; }

        public decimal? seg_duracao { get; set; }

        public long? id_folha { get; set; }

        public long? id_cp { get; set; }

        public short? is_regulagem { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolmedparamlog> dw_consolmedparamlog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolpt> dw_consolpt { get; set; }

        public virtual dw_rt dw_rt { get; set; }
    }
}
