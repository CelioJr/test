using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    /// <summary>
    /// Classe responsável por espelhar a tabela tbl_CBPostoSequencia do banco de dados.
    /// </summary>
    public partial class tbl_CBPostoSequencia
    {
        public int IdRota { get; set; }
        public string CodModelo { get; set; }
        public string CodFab { get; set; }
        public int CodLinha { get; set; }
        public string LetraLinha { get; set; }
        public int NumPosto { get; set; }
        public int CodTipoPosto { get; set; }
        public string DesPosto { get; set; }
        public string NumIP { get; set; }
        public string CodDRT { get; set; }
        public string FlgTipoTerminal { get; set; }
        public string FlgAtivo { get; set; }
        public int NumSeq { get; set; }
        public Nullable<bool> flgObrigatorio { get; set; }
        public Nullable<bool> flgDRTObrig { get; set; }
        public string TipoAmarra { get; set; }
    }
}
