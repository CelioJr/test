using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_TAREFA_TEMPO_DTO
    {
        public int TRT_CODIGO { get; set; }
        public int TRF_CODIGO { get; set; }
        public string TRT_TIPO { get; set; }
        public int TRT_VALOR { get; set; }
        public virtual FR_TAREFA_DTO FR_TAREFA { get; set; }
    }
}
