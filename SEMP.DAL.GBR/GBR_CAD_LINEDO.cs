namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_CAD_LINEDO
    {
        [Key]
        public int COD_LINHA { get; set; }

        public int? VALOR { get; set; }

        [StringLength(3)]
        public string LETRAS { get; set; }

        public DateTime? DATA { get; set; }
    }
}
