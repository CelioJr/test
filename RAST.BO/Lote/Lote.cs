﻿using System;
using System.Collections.Generic;
using System.Linq;
using SEMP.Model.DTO;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.VO;
using SEMP.DAL.Models;
using SEMP.DAL.DAO.Embalagem;
using RAST.BO.Rastreabilidade;
using RAST.BO.Embalagem;
using SEMP.Model;
using System.Collections;
using SEMP.Model.VO.Rastreabilidade;

namespace RAST.BO.Lote
{
    public class Lote
    {

		#region Consultas
		public static CBLoteDTO UltimoLote(int codLinha, int numPosto)
		{
			return daoLote.UltimoLote(codLinha, numPosto);

		}

		public static CBLoteDTO UltimoLoteCC(int codLinha, int numPosto)
		{
			return daoLote.UltimoLoteCC(codLinha, numPosto);

		}

		public static string NumUltimoLote(int codLinha, int numPosto)
		{
			return daoLote.NumUltimoLote(codLinha, numPosto);
		}

		public static int QtdTamLote(string numLote){
			return daoLote.QtdTamLote(numLote);
		}

		public static int QtdLidoLote(string numLote){
			return daoLote.QtdLidoLote(numLote);
		}

		public static int QtdLidoLoteCC(string numLote){
			return daoLote.QtdLidoLoteCC(numLote);
		}

		public static int QtdAprovadoLoteCC(string numLote)
		{
			return daoLote.QtdAprovadoLoteCC(numLote);
		}

		public static bool LoteFechadoAmostras(string numLote)
		{
			return daoLote.LoteFechadoAmostras(numLote);
		}

		public static CBLoteDTO ObterLote(string numLote)
		{
			return daoLote.ObterLote(numLote);

		}

		public static CBLoteDTO ObterLoteCCSerie(string numSerie)
		{
			return daoLote.ObterLoteCCSerie(numSerie);

		}

		public static CBLoteDTO ObterLoteCCSerie(string numSerie, int codLinha)
		{
			return daoLote.ObterLoteCCSerie(numSerie, codLinha);

		}

		public static CBLoteDTO ObterLoteSerie(string numSerie)
		{
			return daoLote.ObterLoteSerie(numSerie);

		}

		public static List<CBLoteItemDTO> ObterPassagemPostoLote(int codLinha, int numPosto)
		{
			return daoLote.ObterPassagemPostoLote(codLinha, numPosto);
		}

		public static List<CBLoteItemDTO> ObterPassagemPostoLote(string numLote)
		{
			return daoLote.ObterPassagemPostoLote(numLote);
		}

		public static List<CBLoteItemDTO> ObterItensLoteSerie(string numECB)
		{
			return daoLote.ObterItensLoteSerie(numECB);
		}

		public static Mensagem ValidaPassagemItemLote(string numECB, string numlote, int codLinha, int numPosto, string codDRT)
		{

			// passa para a procedure de validação de número de série e espera o retorno
			var retorno = daoPassagem.ValidarErros(numECB, codDRT, codLinha, numPosto, "");

			// se o fluxo do número de série estiver normal, valida o item no lote
			if (retorno.CodMensagem == SEMP.Model.Constantes.RAST_OK)
			{
				// verifica se o item pertence ao lote informado
				var pertence = daoLote.PertenceItemLote(numECB, numlote);

				// se o número de série não existir no lote, manda mensagem de erro
				if (!pertence)
				{
					retorno.CodMensagem = SEMP.Model.Constantes.RAST_LOTE_ITEM_INEXISTENTE;
					retorno.DesMensagem = "ITEM NÃO PERTENCE AO LOTE";
					retorno.DesCor = "RED";
				}

			}
			
			return retorno;
		}

		public static Mensagem ImprimirCaixaColetiva(string numeroLote, CBLinhaDTO linha, CBPostoDTO posto)
		{
			var retorno = new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "LIME", DesMensagem = "LEITURA OK. LEIA PRÓXIMO." };

			try
			{
				var EtiquetasPorPosto = Embalagem.Embalagem.RecuperarEtiquetasPorPosto(posto.CodLinha, posto.NumPosto, true);

				//existem etiquetas para serem impressas
				if (EtiquetasPorPosto != null)
				{
					var etiqueta = EtiquetasPorPosto.FirstOrDefault(x => x.NomeEtiqueta == "EtqInspecaoPlacas2");

					//string numeroLote = Embalagem.RecuperarCCProdutoEmbalado(numECB);

					var itensLote = new Embalagem.Embalagem().RecuperarItensEmbaladadosDaCaixaColetiva(numeroLote);

                    var tabelaImpressao = new Hashtable
                    {
                        { "csDataAprovProd", DateTime.Today.ToShortDateString() },
                        { "csDataAprovCQ", DateTime.Today.ToShortDateString() },
                        { "Date0", DateTime.Today.ToShortDateString() },

                        { "csRespCQ", "" },
                        { "csNumLote", numeroLote },
                        { "csQtdSerial", itensLote.Count() },

                        { "csCItm", itensLote[0].CodModelo },
                        { "csDescItem", Geral.ObterDescricaoItem(itensLote[0].CodModelo) },

                        { "csBarCode", numeroLote }
                    };
                    var csNumSerie = "";
					foreach (var item in itensLote)
					{
						csNumSerie += item.NumECB.Substring(6, 6) + "  ";
					}

					tabelaImpressao.Add("csNumSerie", csNumSerie);
					GerenciarImpressao.ImprimirEtiqueta(tabelaImpressao, etiqueta);
				}

			}
			catch (Exception ex)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.RAST_IMPRESSAO_NAO_REALIZADA);
				retorno.DesMensagem = ex.Message;
			}

			return retorno;
		}

		#endregion

		#region Manutenção

		public static Mensagem GravaPassagemProcedureLote(string numECB, string codDRT, int codLinha, int numPosto, string codFab, string numAP, string numLote, string numCC)
		{
			Mensagem retorno = new Mensagem();

			try
			{
				tbl_CBPassagem passagem = new tbl_CBPassagem() { 
					CodLinha = codLinha,
					NumPosto = numPosto,
					NumECB = numECB,
					CodDRT = codDRT
				};

				if (codFab != null && numAP != null)
				{
					passagem.CodFab = codFab;
					passagem.NumAP = numAP;
				}

				// se o número do lote não for informado, abre um novo lote
				if (String.IsNullOrEmpty(numLote))
				{
					numLote = AbrirNovoLote(codLinha, numPosto, numECB.Substring(0, 6), "", "");
				}

				// e se a caixa coletiva não houver sido iniciada, inicia uma nova
				if (String.IsNullOrEmpty(numCC))
				{
					numCC = AbrirNovaCaixaColetiva(codLinha, numPosto, numECB.Substring(0, 6), "", "");
				}

				var gravar = daoLote.InserePassagemProcedureLote(passagem, codDRT, numLote, numCC);

				if (gravar)
				{
					var loteCC = daoLote.ObterLote(numCC);
					// verificar se o lote CC fechou
					if (loteCC.Qtde == loteCC.TamLote)
					{
						// rotina para fechar o lote
						daoLote.FecharLote(numCC, codDRT, "");

						var lote = daoLote.ObterLote(numLote);

						// verifica se o lote principal já fechou
						if (lote.TamLote == lote.Qtde)
						{
							daoLote.FecharLote(numLote, codDRT, "");

							retorno.CodMensagem = "R0010";
							retorno.DesCor = "lime";
							retorno.DesMensagem = "LOTE COMPLETADO COM SUCESSO. LEIA PRÓXIMO.";
						}
						else { 
							retorno.CodMensagem = "R0010";
							retorno.DesCor = "lime";
							retorno.DesMensagem = "CAIXA COLETIVA COMPLETADA COM SUCESSO. LEIA PRÓXIMO.";
						}
					}
					else { 
						retorno.CodMensagem = "R0001";
						retorno.DesCor = "lime";
						retorno.DesMensagem = "LEITURA OK. LEIA PRÓXIMO.";
					}
				}
				else
				{
					retorno.CodMensagem = "E0002";
					retorno.DesCor = "red";
					retorno.DesMensagem = "OCORREU UM ERRO NÃO IDENTIFICADO. ENTRE EM CONTATO COM O ADMINISTRADOR DO SISTEMA.";

					if (!String.IsNullOrEmpty(numCC))
					{
						daoLote.ExcluirLote(numCC);
					}
					if (!String.IsNullOrEmpty(numLote))
					{
						daoLote.ExcluirLote(numLote);
					}
				}
			}
			catch (Exception ex)
			{
				retorno.CodMensagem = "E0002";
				retorno.DesCor = "red";
				retorno.DesMensagem = "OCORREU UM ERRO NÃO IDENTIFICADO. ENTRE EM CONTATO COM O ADMINISTRADOR DO SISTEMA.";
				retorno.StackErro = ex.InnerException.Message;

				if (!String.IsNullOrEmpty(numCC))
				{
					daoLote.ExcluirLote(numCC);
				}
				if (!String.IsNullOrEmpty(numLote))
				{
					daoLote.ExcluirLote(numLote);
				}

			}

			return retorno;
		}

		public static Boolean AprovaLeituraItemLote(string numECB, string numCC)
		{
			return daoLote.AprovaLeituraItemLote(numECB, numCC);
		}

		public static string AbrirNovaCaixaColetiva(int idLinha, int numPosto, string codModelo, string codFab, string numAp)
		{
			return daoLote.AbrirNovaCaixaColetiva(idLinha, numPosto, codModelo, codFab, numAp);
		}

		public static string AbrirNovoLote(int codLinha, int numPosto, string codModelo, string codFab, string numAp)
		{
			return daoLote.AbrirNovoLote(codLinha, numPosto, codModelo, codFab, numAp);
		}

		public static bool FecharLote(string numLote, string codDrt, string justificativa)
		{

			return daoLote.FecharLote(numLote, codDrt, justificativa);

		}

		public static bool AlterarTamanhoCaixaColetiva(int codLinha, int tamanho)
		{
			return DaoEmbalagem.AlterarTamanhoCaixaColetiva(codLinha, tamanho);
		}

		public static bool AlterarTamanhoLote(int codLinha, int tamanho)
		{
			return DaoEmbalagem.AlterarTamanhoLote(codLinha, tamanho);
		}

		public static Boolean AprovarLoteOBA(string numCC, string codDRT, int codLinha, int numPosto)
		{
			var aprova = daoLote.AprovarLoteOBA(numCC, codDRT, codLinha, numPosto);

			if (aprova)
			{
				var itens = daoLote.ObterPassagemPostoLote(numCC);
				var linha = daoGeral.ObterLinhaCliente(codLinha);

				foreach (var item in itens) {
					// encontrar fábrica da placa
					var fabrica = daoLote.ObterFabricaPlaca(item.NumECB);

					var gravar = DaoEmbalagem.SU_GravaSTA(fabrica, linha, item.DatInspecao.Value.ToLongDateString(), codDRT, item.NumECB.Substring(0,6), item.NumECB, "", 1);

				}
			}

			return aprova;

		}

		public static Mensagem ReprovarItemOBA(string codDefeito, string numSerie, string codDRT, int codLinha, int numPosto, string numLote)
		{

			// inclui o defeito no sisap
			var defeito = Defeito.GravarDefeito(codLinha, numPosto, numSerie, codDefeito, codDRT, "");

			// atualiza os dados do lote indicando a reprovação
			var reprovar = daoLote.ReprovarItemLoteOBA(numLote, numSerie, codDefeito);

			defeito.CodMensagem = SEMP.Model.Constantes.RAST_LOTE_FECHADO;
			defeito.DesMensagem = "LOTE REPROVADO. LEIA PRÓXIMO LOTE";
			defeito.DesCor = "ORANGE";

			return defeito;

		}

		public static Boolean ImprimirEtiquetaMagazine(string numCC, string codDRT, int codLinha) {

			return daoLote.ImprimirEtiquetaMagazine(numCC, codDRT, codLinha);

		}

		public static bool LiberarReimpressaoLote(string numCC) {
		
			return daoLote.LiberarReimpressaoLote(numCC);
		
		}

		#endregion

		#region Relatório de Lotes
		public static List<CBLoteDTO> ObterLotes(DateTime datInicio, string setor, string codModelo, int? codLinha = null, int? status = -1, DateTime? datFim = null)
		{

			try
			{
				var query = daoLote.ObterLotes(datInicio, setor);

				if (!String.IsNullOrEmpty(codModelo))
				{
					query = query.Where(x => x.CodModelo == codModelo).ToList();
				}
				
				if (datFim != null)
				{
					query = query.Where(x => x.DatAbertura >= datInicio && x.DatAbertura <= datFim.Value).ToList();
				}
				
				if (codLinha.HasValue)
				{
					query = query.Where(x => x.CodLinha == codLinha).ToList();
				}
				
				if (status.HasValue && status != -1)
				{
					query = query.Where(x => x.Status == status.Value).ToList();
				}

				return query;
			}
			catch
			{
				return new List<CBLoteDTO>();
			}

		}

		public static List<CBLoteDTO> ObterCaixasColetivas(string numLote)
		{

			try
			{
				var query = daoLote.ObterCaixasColetivas(numLote);

				return query;
			}
			catch
			{
				return new List<CBLoteDTO>();
			}

		}

		public static List<CBLoteDTO> ObterLotesModelo(string codModelo, DateTime? datInicio = null, DateTime? datFim = null)
		{

			var query = daoLote.ObterLotes(x => x.CodModelo == codModelo);

			if (datInicio != null && datFim != null)
			{
				query = query.Where(x => x.DatAbertura >= datInicio.Value && x.DatAbertura <= datFim.Value).ToList();
			}

			return query;

		}

		public static List<CBLoteDTO> ObterLotesLinha(int codLinha, DateTime? datInicio = null, DateTime? datFim = null)
		{

			var query = daoLote.ObterLotes(x => x.CodLinha == codLinha);

			if (datInicio != null && datFim != null)
			{
				query = query.Where(x => x.DatAbertura >= datInicio.Value && x.DatAbertura <= datFim.Value).ToList();
			}

			return query;

		}

		public static List<LotesDaVez> ObterLotesDaVez(string setor, string codModelo, int? codLinha)
		{

			return daoLote.ObterLotesDaVez(setor, codModelo, codLinha);
		
		}

		public static List<ItensLote> ObterEmbaladosLote(string numCC)
		{
			return daoLote.ObterEmbaladosLote(numCC);

		}


		#endregion
		}
}
