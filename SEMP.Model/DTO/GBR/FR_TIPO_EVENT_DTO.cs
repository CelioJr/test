using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_TIPO_EVENT_DTO
    {
        public int FTE_COD { get; set; }
        public string FTE_DESCRICAO { get; set; }
        public string FTE_SIGLA { get; set; }
    }
}
