namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_jobdet
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_jobdet()
        {
            om_jobdetlog = new HashSet<om_jobdetlog>();
        }

        [Key]
        public long id_jobdet { get; set; }

        public long id_job { get; set; }

        public long id_job_recurso { get; set; }

        [StringLength(255)]
        public string url_origem { get; set; }

        public int? ordem { get; set; }

        public virtual om_job om_job { get; set; }

        public virtual om_job_recurso om_job_recurso { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_jobdetlog> om_jobdetlog { get; set; }
    }
}
