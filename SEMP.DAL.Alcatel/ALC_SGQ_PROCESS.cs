namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_SGQ_PROCESS")]
    public partial class ALC_SGQ_PROCESS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ALC_SGQ_PROCESS()
        {
            ALC_SGQ_DOCUMENT = new HashSet<ALC_SGQ_DOCUMENT>();
        }

        [Key]
        public int ID_PROCESS { get; set; }

        [StringLength(50)]
        public string DESCRIPTION { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_SGQ_DOCUMENT> ALC_SGQ_DOCUMENT { get; set; }
    }
}
