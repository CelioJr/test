﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.Models
{
	public class spc_CBRastreiaPCI
	{
		public int Contador { get; set; }
		public int NumSeq { get; set; }
		public string DesPosto { get; set; }
		public DateTime DatEvento { get; set; }
		public string Setor { get; set; }
		public int CodLinha { get; set; }
		public string DesLinha { get; set; }
		public int? NumPosto { get; set; }
		public string CodDefeito { get; set; }
		public string NomFuncionario { get; set; }
		public string DesModelo { get; set; }
	}
}
