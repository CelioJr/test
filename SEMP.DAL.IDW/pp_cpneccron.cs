namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_cpneccron
    {
        [Key]
        public long id_cpneccron { get; set; }

        public long id_cp { get; set; }

        public long? id_planeccron { get; set; }

        public long? qt_atendida { get; set; }

        public virtual pp_cp pp_cp { get; set; }

        public virtual pp_planeccron pp_planeccron { get; set; }
    }
}
