using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_SYS_LOG_DTO
    {
        public decimal ID_LOG { get; set; }
        public string CODE { get; set; }
        public string DESCRIPTION { get; set; }
        public Nullable<System.DateTime> DT_MAKED { get; set; }
        public Nullable<int> RESPONSIBLE { get; set; }
        public string SYS_LOG { get; set; }
        public virtual ALC_SYS_USER_DTO ALC_SYS_USER { get; set; }
    }
}
