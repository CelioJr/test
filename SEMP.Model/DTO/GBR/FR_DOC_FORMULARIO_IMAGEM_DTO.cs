using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DOC_FORMULARIO_IMAGEM_DTO
    {
        public int DOC_CODIGO { get; set; }
        public int FRM_CODIGO { get; set; }
        public string FIM_ABA { get; set; }
        public string FIM_ABA_NOME_ORIGINAL { get; set; }
        public byte[] FIM_IMAGEM { get; set; }
        public string FIM_VERSAO { get; set; }
        public virtual FR_DOC_PRINCIPAL_DTO FR_DOC_PRINCIPAL { get; set; }
    }
}
