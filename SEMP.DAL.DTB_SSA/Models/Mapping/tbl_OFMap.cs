using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.DTB_SSA.Models.Mapping
{
	public class tbl_OFMap : EntityTypeConfiguration<tbl_OF>
    {
        public tbl_OFMap()
        {
            // Primary Key
            this.HasKey(t => t.codOF);

            // Properties
            this.Property(t => t.codOF)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.desCliente)
                .HasMaxLength(30);

            this.Property(t => t.codModelo)
                .IsRequired()
                .HasMaxLength(13);

            this.Property(t => t.desSerieIni)
                .IsRequired()
                .HasMaxLength(13);

            this.Property(t => t.desSerieFim)
                .IsRequired()
                .HasMaxLength(13);

            this.Property(t => t.flgSoho)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.sistema_operacional)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.CodImagem)
                .HasMaxLength(10);

            this.Property(t => t.flgEAN)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.flgConfiguracao)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.flgModelo)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.flgApontamento)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.flgTipoMaquina)
                .HasMaxLength(3);

            this.Property(t => t.desHCL)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.codSO)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.flgSubTipoMaq)
                .IsFixedLength()
                .HasMaxLength(3);

            // Table & Column Mappings
            this.ToTable("tbl_OF");
            this.Property(t => t.codOF).HasColumnName("codOF");
            this.Property(t => t.numQtde).HasColumnName("numQtde");
            this.Property(t => t.desCliente).HasColumnName("desCliente");
            this.Property(t => t.codModelo).HasColumnName("codModelo");
            this.Property(t => t.desSerieIni).HasColumnName("desSerieIni");
            this.Property(t => t.desSerieFim).HasColumnName("desSerieFim");
            this.Property(t => t.flgSoho).HasColumnName("flgSoho");
            this.Property(t => t.flgNotebook).HasColumnName("flgNotebook");
            this.Property(t => t.flgMultipla).HasColumnName("flgMultipla");
            this.Property(t => t.validade).HasColumnName("validade");
            this.Property(t => t.servidor).HasColumnName("servidor");
            this.Property(t => t.codOperador).HasColumnName("codOperador");
            this.Property(t => t.datAtualizacao).HasColumnName("datAtualizacao");
            this.Property(t => t.sistema_operacional).HasColumnName("sistema_operacional");
            this.Property(t => t.imagem_id).HasColumnName("imagem_id");
            this.Property(t => t.CodImagem).HasColumnName("CodImagem");
            this.Property(t => t.DatImagem).HasColumnName("DatImagem");
            this.Property(t => t.CodOpeImagem).HasColumnName("CodOpeImagem");
            this.Property(t => t.flgEAN).HasColumnName("flgEAN");
            this.Property(t => t.flgConfiguracao).HasColumnName("flgConfiguracao");
            this.Property(t => t.flgModelo).HasColumnName("flgModelo");
            this.Property(t => t.flgHCL).HasColumnName("flgHCL");
            this.Property(t => t.flgApontamento).HasColumnName("flgApontamento");
            this.Property(t => t.flgTipoMaquina).HasColumnName("flgTipoMaquina");
            this.Property(t => t.desHCL).HasColumnName("desHCL");
            this.Property(t => t.codOfMae).HasColumnName("codOfMae");
            this.Property(t => t.codSO).HasColumnName("codSO");
            this.Property(t => t.Qtd_Sep_Fis).HasColumnName("Qtd_Sep_Fis");
            this.Property(t => t.Qtd_Emb).HasColumnName("Qtd_Emb");
            this.Property(t => t.datLiberacao).HasColumnName("datLiberacao");
            this.Property(t => t.codOpeLiberacao).HasColumnName("codOpeLiberacao");
            this.Property(t => t.flgSubTipoMaq).HasColumnName("flgSubTipoMaq");

        }
    }
}
