﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.Rastreabilidade;
using SEMP.Model.DTO;

namespace SEMP.Areas.Coleta.Controllers
{
	public class TipoAmarraController : Controller
	{
		#region Listagem
		//
		// GET: /Coleta/TipoAmarra/
		public ActionResult Index()
		{
			return View();
		}

		//
		// GET: /Coleta/TipoAmarra/Lista/5
		public ActionResult Lista()
		{
			Configuracao config = new Configuracao();

			var dados = config.ObterTiposAmarra();

			return View(dados);
		}
		#endregion

		#region Create
		//
		// GET: /Coleta/TipoAmarra/Create
		public ActionResult Create()
		{
			return View();
		}

		//
		// POST: /Coleta/TipoAmarra/Create
		[HttpPost]
		public ActionResult Create(CBTipoAmarraDTO dado)
		{
			try
			{
				Configuracao config = new Configuracao();

				var lista = config.ObterTiposAmarra();
				var maior = lista.Max(x => x.CodTipoAmarra) + 1;
				dado.CodTipoAmarra = maior;

				config.GravarTipoAmarra(dado);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json("Erro ao gravar. " + ex.Message, JsonRequestBehavior.AllowGet);
			}
		}
		#endregion

		#region Edit
		//
		// GET: /Coleta/TipoAmarra/Edit/5
		public ActionResult Edit(int id)
		{
			Configuracao config = new Configuracao();
			var dado = config.ObterTipoAmarra(id);

			return View(dado);
		}

		//
		// POST: /Coleta/TipoAmarra/Edit/5
		[HttpPost]
		[ValidateInput(false)]
		public ActionResult Edit(CBTipoAmarraDTO dado)
		{
			try
			{
				Configuracao config = new Configuracao();

				config.GravarTipoAmarra(dado);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}
		#endregion

		#region Delete
		//
		// GET: /Coleta/TipoAmarra/Delete/5
		public ActionResult Delete(int id)
		{
			Configuracao config = new Configuracao();
			var dado = config.ObterTipoAmarra(id);

			return View(dado);
		}

		//
		// POST: /Coleta/TipoAmarra/Delete/5
		[HttpPost]
		public ActionResult Delete(int id, FormCollection collection)
		{
			try
			{
				Configuracao config = new Configuracao();
				var dado = config.RemoverTipoAmarra(id);

				return RedirectToAction("Index");
			}
			catch
			{
				return RedirectToAction("Index");
			}
		}
		#endregion
	}
}
