using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.DTB_SSA.Models.Mapping
{
    public class tbl_SGI_ImagemMap : EntityTypeConfiguration<tbl_SGI_Imagem>
    {
        public tbl_SGI_ImagemMap()
        {
            // Primary Key
            this.HasKey(t => t.imagem_id);

            // Properties
            this.Property(t => t.codEstrutura)
                .HasMaxLength(13);

            this.Property(t => t.codImagem)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.desImagem)
                .HasMaxLength(600);

            this.Property(t => t.tipParticao)
                .HasMaxLength(6);

            this.Property(t => t.tipMaquina)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.codItem)
                .IsRequired()
                .HasMaxLength(3);

            this.Property(t => t.codImagemToshiba)
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("tbl_SGI_Imagem");
            this.Property(t => t.imagem_id).HasColumnName("imagem_id");
            this.Property(t => t.codEstrutura).HasColumnName("codEstrutura");
            this.Property(t => t.codImagem).HasColumnName("codImagem");
            this.Property(t => t.codOperador).HasColumnName("codOperador");
            this.Property(t => t.desImagem).HasColumnName("desImagem");
            this.Property(t => t.tipParticao).HasColumnName("tipParticao");
            this.Property(t => t.tipMaquina).HasColumnName("tipMaquina");
            this.Property(t => t.codItem).HasColumnName("codItem");
            this.Property(t => t.dataHoraArquivo).HasColumnName("dataHoraArquivo");
            this.Property(t => t.codOpeUpdate).HasColumnName("codOpeUpdate");
            this.Property(t => t.datInsercao).HasColumnName("datInsercao");
            this.Property(t => t.datAtualizacao).HasColumnName("datAtualizacao");
            this.Property(t => t.tamanhoarquivo).HasColumnName("tamanhoarquivo");
            this.Property(t => t.tipImagem).HasColumnName("tipImagem");
            this.Property(t => t.codImagemToshiba).HasColumnName("codImagemToshiba");

        }
    }
}
