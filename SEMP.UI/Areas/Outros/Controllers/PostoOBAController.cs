﻿using RAST.BO.Rastreabilidade;
using SEMP.Model.DTO;
using SEMP.Model.VO;
using SEMP.Model.VO.Rastreabilidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEMP.Areas.Outros.Controllers
{
	public class PostoOBAController : Controller
	{
		// GET: Outros/PostoOBA
		/// <summary>
		/// Posto de entrada para o OBA
		/// </summary>
		/// <returns></returns>
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult Lista(string datInicio, string datFim)
		{
			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}


			var dado = PostoOBA.ObterRegistros(dIni, dFim);

			var defeitosEmAberto = PostoOBA.ObterRegistrosDefeitos(dIni);

			dado.AddRange(defeitosEmAberto.Where(x => !dado.Select(y => y.NumRPA).ToList().Contains(x.NumRPA)));

			return View(dado.OrderBy(x => x.DatProducao).ThenBy(x => x.DatFechamento).ToList());

		}

		public ActionResult ListaResumo(string datInicio, string datFim)
		{
			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}


			var dado = PostoOBA.ObterResumoInspecao(dIni, dFim);

			return View(dado);

		}

		public ActionResult NovaInspecao()
		{
			Session["OBACodFab"] = "";
			Session["OBANumRPA"] = "";
			Session["OBACodPallet"] = "";

			var linhas = Geral.ObterLinhaPorFabrica("OBA");

			ViewBag.linhas = linhas;

			return View();
		}

		[HttpPost]
		public ActionResult NovaInspecao(string CodFab, string NumRPA, string CodLinha, string NumPosto, string CodPallet, string CodModelo, string codOperador, string qtdAmostras, string codLinhaOri) {

			/// Cadastrar os dados de movimentação de pallet
			/// e fazer a movimentação do ponto de estoque da expedição
			/// para o do CQ
			/// 

			var pallet = PostoOBA.ObterPalletGPA(CodPallet, CodModelo, ""); // dados do pallet
			var configLinha = PostoOBA.ObterConfigLote(int.Parse(CodLinha)); // dados de configuração de linha para amostragem

			var dado = PostoOBA.ObterRegistro(CodFab, NumRPA, CodPallet);

			if (dado == null)
			{
				dado = new CBOBARegistroDTO()
				{
					CodFab = CodFab,
					NumRPA = NumRPA,
					CodLinha = int.Parse(CodLinha),
					NumPosto = int.Parse(NumPosto),
					CodPallet = CodPallet,
					CodModelo = CodModelo,
					CodOperador = codOperador,
					DatAbertura = BO.Util.GetDate(),
					DatProducao = pallet.Data,
					FlgStatus = "A",
					QtdPallet = pallet.QtdPallet,
					QtdAmostras = int.Parse(qtdAmostras),
					CodLinhaOri = codLinhaOri
				};
			}
			else
			{
				if (dado.FlgStatus == "1" && dado.DatFechamento != null) 
				{

					ViewBag.erro = "Pallet já inspecionado";
					return View();

				}

				dado.QtdAmostras = int.Parse(qtdAmostras);
				dado.CodOperador = codOperador;
				dado.CodPallet = CodPallet;
				dado.CodLinhaOri = codLinhaOri;
				dado.DatProducao = pallet.Data;
			}

			var resultado = PostoOBA.GravarRegistro(dado, false);

			if (resultado.StartsWith("false")) {
				ViewBag.erro = "Erro ao tentar iniciar lote, informe ao DTI <br />" + resultado.Replace("false|", "");
				return View();

			}

			/// antes de redirecionar, setar os cookies
			#region Cookies
			/// Setar posto no cookie do browser
			/// cria a variável e configura os valores

			HttpCookie c = new HttpCookie("Posto");

			c.Expires = DateTime.Now.AddYears(1);
			c.Values.Add("Fabrica", "OBA");
			c.Values.Add("codLinha", CodLinha);
			c.Values.Add("numPosto", NumPosto);

			this.ControllerContext.HttpContext.Response.Cookies.Add(c);

			#endregion


			Session["OBACodFab"] = CodFab;
			Session["OBANumRPA"] = NumRPA;
			Session["OBACodPallet"] = CodPallet;

			return RedirectToAction("Coleta");

		}

		public ActionResult NovaInspecaoRPA()
		{
			Session["OBACodFab"] = "";
			Session["OBANumRPA"] = "";
			Session["OBACodPallet"] = "";

			var linhas = Geral.ObterLinhaPorFabrica("OBA");

			ViewBag.linhas = linhas;

			return View();
		}

		[HttpPost]
		public ActionResult NovaInspecaoRPA(string CodFab, string NumRPA, string CodLinha, string NumPosto, string CodPallet, string CodModelo, string codOperador, string qtdAmostras, string codLinhaOri)
		{

			/// Cadastrar os dados de movimentação de pallet
			/// e fazer a movimentação do ponto de estoque da expedição
			/// para o do CQ
			/// 

			var pallet = PostoOBA.ObterPalletGPA(CodPallet, CodModelo, ""); // dados do pallet
			var configLinha = PostoOBA.ObterConfigLote(int.Parse(CodLinha)); // dados de configuração de linha para amostragem

			var dado = PostoOBA.ObterRegistro(CodFab, NumRPA, CodPallet);

			if (dado == null)
			{
				dado = new CBOBARegistroDTO()
				{
					CodFab = CodFab,
					NumRPA = NumRPA,
					CodLinha = int.Parse(CodLinha),
					NumPosto = int.Parse(NumPosto),
					CodPallet = CodPallet,
					CodModelo = CodModelo,
					CodOperador = codOperador,
					DatAbertura = BO.Util.GetDate(),
					DatProducao = pallet.Data,
					FlgStatus = "A",
					QtdPallet = pallet.QtdPallet,
					QtdAmostras = int.Parse(qtdAmostras),
					CodLinhaOri = codLinhaOri
				};
			}
			else
			{
				if (dado.FlgStatus == "1" && dado.DatFechamento != null)
				{

					ViewBag.erro = "Pallet já inspecionado";
					return View();

				}

				dado.QtdAmostras = int.Parse(qtdAmostras);
				dado.CodOperador = codOperador;
				dado.CodPallet = CodPallet;
				dado.CodLinhaOri = codLinhaOri;
				dado.DatProducao = pallet.Data;
			}

			var resultado = PostoOBA.GravarRegistro(dado, true);

			if (resultado.StartsWith("false"))
			{
				ViewBag.erro = "Erro ao tentar iniciar lote, informe ao DTI <br />" + resultado.Replace("false|", "");
				return View();

			}

			/// antes de redirecionar, setar os cookies
			#region Cookies
			/// Setar posto no cookie do browser
			/// cria a variável e configura os valores

			HttpCookie c = new HttpCookie("Posto");

			c.Expires = DateTime.Now.AddYears(1);
			c.Values.Add("Fabrica", "OBA");
			c.Values.Add("codLinha", CodLinha);
			c.Values.Add("numPosto", NumPosto);

			this.ControllerContext.HttpContext.Response.Cookies.Add(c);

			#endregion


			Session["OBACodFab"] = CodFab;
			Session["OBANumRPA"] = NumRPA;
			Session["OBACodPallet"] = CodPallet;

			return RedirectToAction("Coleta");

		}


		public ActionResult Coleta()
		{
			string CodFab = Session["OBACodFab"].ToString();
			string NumRPA = Session["OBANumRPA"].ToString();
			string CodPallet = Session["OBACodPallet"].ToString();

			if (CodFab == "" ) {
				CodFab = "60";
			}

			// recupera os dados do registro para leitura
			var dado = PostoOBA.ObterRegistro(CodFab, NumRPA, CodPallet);

			if (dado == null) {
				return RedirectToAction("NovaInspecao");

			}

			var series = !String.IsNullOrEmpty(CodPallet) ? PostoOBA.ObterSeriesPallet(int.Parse(CodPallet).ToString()) : new List<string>() { "" };

			var testesOBA = PostoOBA.ObterTestesOBA();
			var testesEspecificos = PostoOBA.ObterTestesEspecificosOBA();
			var registros = PostoOBA.ObterInspecoes(CodFab, NumRPA, CodPallet);

			ViewBag.Series = series;
			ViewBag.TestesOBA = testesOBA;
			ViewBag.TestesEspecificos = testesEspecificos;
			ViewBag.Registros = registros;

			return View(dado);

		}

		[HttpPost]
		public ActionResult ColetaSeries(FormCollection dados) {

			var defeitos = new List<CBOBATestesDTO>();

			var numECB = PostoOBA.ObterECBSerie(dados["CodModelo"], dados["NumSerieColeta"]);
			
			var inspecao = new CBOBAInspecaoDTO() {
				NumECB = numECB,
				CodLinha = int.Parse(dados["CodLinha"]),
				NumPosto = int.Parse(dados["NumPosto"]),
				CodFab = dados["CodFab"],
				NumRPA = dados["NumRPA"],
				CodPallet = dados["CodPallet"],
				FlgStatus = "0",
				Observacao = dados["Observacao"]
				
			};

			foreach (string chave in dados)
			{
				if (chave.StartsWith("Defeitos"))
				{

					var codDefeito = chave.Substring(8, 4);
					var codSubDefeito = int.Parse(chave.Substring(12));
					var obs = dados["ObsDefeito" + codDefeito + codSubDefeito.ToString()];

					defeitos.Add(new CBOBATestesDTO() {
						NumECB = numECB,
						CodLinha = int.Parse(dados["CodLinha"]),
						NumPosto = int.Parse(dados["NumPosto"]),
						CodFab = dados["CodFab"],
						NumRPA = dados["NumRPA"],
						CodDefeito = codDefeito,
						CodSubDefeito = codSubDefeito,
						Observacao = obs,
						FlgStatus = dados[chave]
					});
				}

			}


			var resultado = PostoOBA.GravarInspecao(inspecao, defeitos);
			
			if (PostoOBA.VerificaPalletTotal(dados["CodFab"], dados["NumRPA"])) 
			{
				PostoOBA.FecharInspecao(dados["CodFab"], dados["NumRPA"], dados["CodPallet"]);

				return RedirectToAction("Index");
			}

			return RedirectToAction("Coleta");
		}

		[HttpPost]
		public ActionResult GravarRPE(string codFab, string numRPA, string numRPE)
		{


			if (codFab != "" && numRPA != "" && numRPE != "")
			{

				var dado = PostoOBA.ObterRegistro(codFab, numRPA);

				dado.NumRPE = numRPE;

				PostoOBA.GravarRegistro(dado);

			}

			return RedirectToAction("Index");

		}

		[HttpPost]
		public ActionResult GravarLiberarReprov(string codFab, string numRPA, string obsLiberado, string numRPE)
		{


			if (codFab != "" && numRPA != "")
			{

				var dado = PostoOBA.ObterRegistro(codFab, numRPA);

				dado.DatLiberado = BO.Util.GetDate();
				dado.ObsRevisao = obsLiberado;
				dado.NumRPE = numRPE;
				dado.FlgStatus = "L";

				PostoOBA.GravarRegistro(dado);

			}

			return RedirectToAction("Index");

		}
		
		public ActionResult VisualizarInspecao(string codFab, string numRPA) {

			var registro = PostoOBA.ObterRegistro(codFab, numRPA);
			var series = PostoOBA.ObterInspecoes(codFab, numRPA, "");
			var testes = PostoOBA.ObterTestes(codFab, numRPA, "");
			var testesOBA = PostoOBA.ObterTestesOBA();
			var testesEspecificos = PostoOBA.ObterTestesEspecificosOBA();

			ViewBag.series = series;
			ViewBag.testes = testes;
			ViewBag.testesOBA = testesOBA;
			ViewBag.testesEspecificos = testesEspecificos;

			return View(registro);

		}

		public ActionResult VisualizarInspecaoModelo(string datInicio, string datFim, string codModelo)
		{

			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}

			var dados = PostoOBA.ObterInspecoes(dIni, dFim);
			dados = dados.Where(x => x.CodModelo == codModelo).ToList();
			var testesOBA = PostoOBA.ObterTestesOBA();
			var testesEspecificos = PostoOBA.ObterTestesEspecificosOBA();

			ViewBag.testesOBA = testesOBA;
			ViewBag.testesEspecificos = testesEspecificos;

			return View(dados);

		}

		public ActionResult RelatorioInspecao(string datInicio, string datFim) {


			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}

			var dado = PostoOBA.ObterInspecoes(dIni, dFim);

			return View(dado);
		}

		public ActionResult GraficoResumo(string datInicio, string datFim)
		{

			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}


			var dado = PostoOBA.ObterResumoInspecao(dIni, dFim);

			return View(dado);
		}

		public ActionResult GraficoMensal()
		{

			var dIni = DateTime.Parse(String.Format("01/01/{0}", DateTime.Now.Year));
			var dFim = DateTime.Parse(String.Format("31/12/{0}", DateTime.Now.Year));

			var dado = PostoOBA.ObterResumoInspecao(dIni, dFim);

			var resumo = (from m in dado
						  group m by new { m.CodModelo, m.DesModelo, m.QtdP042, m.QtdP067, m.QtdP068, m.QtdP823, Data = DateTime.Parse(String.Format("01/{0}/{1}", m.Data.Month, m.Data.Year)) } into grupo
						  select new OBAResumoInspecao()
						  {
							  Data = grupo.Key.Data,
							  CodModelo = grupo.Key.CodModelo,
							  DesModelo = grupo.Key.DesModelo,
							  QtdAprovados = grupo.Sum(x => x.QtdAprovados),
							  QtdInspecionados = grupo.Sum(x => x.QtdInspecionados),
							  QtdLotes = grupo.Sum(x => x.QtdLotes),
							  QtdP042 = grupo.Key.QtdP042,
							  QtdP067 = grupo.Key.QtdP067,
							  QtdP068 = grupo.Key.QtdP068,
							  QtdP823 = grupo.Key.QtdP823,
							  QtdProduzido = grupo.Sum(x => x.QtdProduzido),
							  QtdPrograma = grupo.Sum(x => x.QtdPrograma),
							  QtdReprovados = grupo.Sum(x => x.QtdReprovados)
						  }
			 ).ToList();

			return View(dado);
		}

		public ActionResult GraficoMeta(string datInicio, string datFim)
		{

			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}

			ViewBag.datInicio = dIni;
			ViewBag.datFim = dFim;
			var dados = PostoOBA.ObterInspecoes(dIni, dFim);

			return View(dados);
		}

		public ActionResult GraficoProdutividade(string datInicio, string datFim)
		{

			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}


			var dados = PostoOBA.ObterInspecoes(dIni, dFim);

			return View(dados);
		}

		public ActionResult GraficoProdutividadeMensal(string datInicio, string datFim)
		{

			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}

			//dIni = dIni.AddDays(-dIni.Day).AddDays(1);
			//dFim = dFim.AddDays(-dFim.Day).AddDays(1).AddMonths(1).AddDays(-1);


			ViewBag.dIni = dIni;
			ViewBag.dFim = dFim;
			var dados = PostoOBA.ObterInspecoes(dIni, dFim);

			return View(dados);
		}


		#region Consultas RPA

		public JsonResult ObterRPA(string codFab, string numRPA)
		{

			var dado = PostoOBA.ObterRPA(codFab, numRPA);
			var disponivel = true;

			var registro = PostoOBA.ObterRegistro(codFab, numRPA);

			if (registro != null && registro.FlgStatus != "A") {
				disponivel = false;
			}

			var retorno = new { 
				dado,
				disponivel
			};

			return Json(retorno, JsonRequestBehavior.AllowGet);

		}

		#endregion

		#region Consultas GPA

		public JsonResult ObterPalletsGPA(string datInicio, string datFim)
		{

			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni;

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}

			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}


			var dado = PostoOBA.ObterPalletsGPA(dIni, dFim);

			return Json(dado, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ObterPalletGPA(string numPallet, string codModelo, string numSerie)
		{

			var dado = PostoOBA.ObterPalletGPA(numPallet, codModelo, numSerie);

			return Json(dado, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ObterSeriesPallet(string numPallet)
		{

			var dados = PostoOBA.ObterSeriesPallet(numPallet);

			return Json(dados, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ObterConfigLinha(int codLinha) {

			var configLinha = PostoOBA.ObterConfigLote(codLinha);

			return Json(configLinha, JsonRequestBehavior.AllowGet);

		}

		#endregion

		}

}
