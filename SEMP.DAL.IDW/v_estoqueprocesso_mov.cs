namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class v_estoqueprocesso_mov
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id_estmov { get; set; }

        public DateTime? dthr_cadastro { get; set; }

        public DateTime? dthr_mov { get; set; }

        [StringLength(30)]
        public string cd_produto { get; set; }

        public decimal? qt_mov { get; set; }

        public decimal? qt_total_atual { get; set; }

        public decimal? qt_total_Ant { get; set; }

        public short? tp_mov { get; set; }

        public decimal? qt_entrada_ant { get; set; }

        public decimal? qt_saida_ant { get; set; }

        public decimal? qt_ajuste_ant { get; set; }
    }
}
