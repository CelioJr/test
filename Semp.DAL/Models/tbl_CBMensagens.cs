using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBMensagens
    {
        public string CodMensagem { get; set; }
        public string DesMensagem { get; set; }
        public string DesCor { get; set; }
        public string DesSom { get; set; }
    }
}
