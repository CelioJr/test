using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_PlanoEtapaMaqMap : EntityTypeConfiguration<tbl_PlanoEtapaMaq>
    {
        public tbl_PlanoEtapaMaqMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodModelo, t.NumRevisao, t.NumEtapa, t.NomMaq, t.CodProcesso, t.rowguid, t.FlgLeftRight });

            // Properties
            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumRevisao)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NumEtapa)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NomMaq)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.CodProcesso)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.FlgLeftRight)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodInsersora)
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.CodFam)
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.CodBloco)
                .IsFixedLength()
                .HasMaxLength(10)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_PlanoEtapaMaq");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.NumRevisao).HasColumnName("NumRevisao");
            this.Property(t => t.NumEtapa).HasColumnName("NumEtapa");
            this.Property(t => t.NomMaq).HasColumnName("NomMaq");
            this.Property(t => t.CodProcesso).HasColumnName("CodProcesso");
            this.Property(t => t.TempoPadrao).HasColumnName("TempoPadrao");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
            this.Property(t => t.FlgLeftRight).HasColumnName("FlgLeftRight");
            this.Property(t => t.CodInsersora).HasColumnName("CodInsersora");
            this.Property(t => t.CodFam).HasColumnName("CodFam");
            this.Property(t => t.CodBloco).HasColumnName("CodBloco");
        }
    }
}
