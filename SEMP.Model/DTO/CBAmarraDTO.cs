using System;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
    /// <summary>
    /// Esta classe � espelho da tabela tbl_cbAmarra do banco de dados.
    /// </summary>
    public class CBAmarraDTO
    {
        public string NumSerie { get; set; }
        public string NumECB { get; set; }
        public string CodModelo { get; set; }
        public Nullable<int> FlgTipo { get; set; }
        public Nullable<System.DateTime> DatAmarra { get; set; }
        public Nullable<int> CodLinha { get; set; }
        public Nullable<int> NumPosto { get; set; }
                
        [Display(Name = "C�digo da f�brica")]
        [Required]
        public string CodFab { get; set; }

        [Display(Name = "N�mero da AP")]
        [Required]
        public string NumAP { get; set; }

        public string CodItem { get; set; }

        //Outros
        public string DesTipoAmarra { get; set; }
        public bool IsDispositivoIntegrado { get; set; }
        public int DipositivoID { get; set; }

        [Display(Name = "Data de refer�ncia")]
        [Required]
        public string DataReferencia { get; set; }

        [Display(Name = "Crach� do supervisor")]
        public string CrachaSupervisor { get; set; }

        public string DescricaoLinha { get; set; }
        public string DescricaoPosto { get; set; }

        public string numCC { get; set; }
        public string numLote { get; set; }
    }
}
