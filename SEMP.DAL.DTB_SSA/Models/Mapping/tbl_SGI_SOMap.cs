using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.DTB_SSA.Models.Mapping
{
    public class tbl_SGI_SOMap : EntityTypeConfiguration<tbl_SGI_SO>
    {
        public tbl_SGI_SOMap()
        {
            // Primary Key
            this.HasKey(t => t.codSO);

            // Properties
            this.Property(t => t.codSO)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.nomSO)
                .IsRequired()
                .HasMaxLength(30);

            this.Property(t => t.TipoConector)
                .IsFixedLength()
                .HasMaxLength(3);

            // Table & Column Mappings
            this.ToTable("tbl_SGI_SO");
            this.Property(t => t.codSO).HasColumnName("codSO");
            this.Property(t => t.nomSO).HasColumnName("nomSO");
            this.Property(t => t.TipoConector).HasColumnName("TipoConector");
        }
    }
}
