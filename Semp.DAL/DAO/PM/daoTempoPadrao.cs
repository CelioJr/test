﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.IDW;
using SEMP.DAL.Models;
using SEMP.Model.DTO;

namespace SEMP.DAL.DAO.PM
{
	public class daoTempoPadrao
	{
        /// <summary>
        /// Rotina para buscar no banco de dados os registros cadastrados
        /// </summary>
        /// <returns></returns>
        public static List<TempoPadraoDTO> ObterModelos()
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from c in dtbSim.tbl_TempoPadrao
								join i in dtbSim.tbl_Item on c.CodModelo equals i.CodItem
								 select new TempoPadraoDTO()
								 {
									CodModelo = c.CodModelo,
									CodPai = c.CodPai,
									TempoLp = c.TempoLp,
									TempoPadrao = c.TempoPadrao,
									Usuario = c.Usuario,
									rowguid = c.rowguid,
									DesModelo = i.DesItem,
									flgAtivo = i.FlgAtivoItem
								 });

				return resultado.ToList();
			}
		}

		/// <summary>
		/// Rotina para buscar no banco de dados um determinado registro
		/// </summary>
		/// <param name="codLinha"></param>
		/// <returns></returns>
		public static TempoPadraoDTO ObterTempoPadrao(string codModelo)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from c in dtbSim.tbl_TempoPadrao
								 join i in dtbSim.tbl_Item on c.CodModelo equals i.CodItem
								where c.CodModelo.Equals(codModelo)
								 select new TempoPadraoDTO()
								 {
									CodModelo = c.CodModelo,
									CodPai = c.CodPai,
									TempoLp = c.TempoLp,
									TempoPadrao = c.TempoPadrao,
									Usuario = c.Usuario,
									rowguid = c.rowguid,
									DesModelo = i.DesItem
								}).FirstOrDefault();

				return resultado;

			}
		}

		/// <summary>
		/// Rotina de manuntenção de dados, para inclusão e alteração
		/// </summary>
		/// <param name="linha"></param>
		public static void GravarTempoPadrao(TempoPadraoDTO tempoPadrao)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var existe =
					dtbSim.tbl_TempoPadrao.FirstOrDefault(
						x => x.CodModelo.Equals(tempoPadrao.CodModelo));

				if (existe != null)
				{
					existe.TempoLp = tempoPadrao.TempoLp;
					existe.TempoPadrao = tempoPadrao.TempoPadrao;
					existe.Usuario = tempoPadrao.Usuario;
					existe.CodPai = tempoPadrao.CodPai;
					dtbSim.SaveChanges();
				}
				else
				{
					var ntempopadrao = new tbl_TempoPadrao()
					{
						CodModelo = tempoPadrao.CodModelo,
						TempoPadrao = tempoPadrao.TempoPadrao,
						TempoLp = tempoPadrao.TempoLp,
						CodPai = tempoPadrao.CodPai,
						Usuario = tempoPadrao.Usuario,
						rowguid = Guid.NewGuid()
					};

					dtbSim.tbl_TempoPadrao.Add(ntempopadrao);
					dtbSim.SaveChanges();
				}
			}
		}

		/// <summary>
		/// Rotina para excluir um registro do banco de dados
		/// </summary>
		/// <param name="codLinha"></param>
		public static void RemoverTempoPadrao(string codModelo)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var existe =
					dtbSim.tbl_TempoPadrao.FirstOrDefault(
						x => x.CodModelo.Equals(codModelo));

				if (existe == null) return;

				dtbSim.tbl_TempoPadrao.Remove(existe);
				dtbSim.SaveChanges();
			}
		}

		/// <summary>
		/// Rotina para buscar no banco de dados os registros cadastrados
		/// </summary>
		/// <returns></returns>
		public static List<TempoPadraoDTO> ObterItensModelo(string codModelo)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from c in dtbSim.tbl_TempoPadrao
								 join i in dtbSim.tbl_Item on c.CodModelo equals i.CodItem
								 join p in dtbSim.tbl_PlanoItem on c.CodModelo equals p.CodItem
								 where p.CodModelo == codModelo
								 select new TempoPadraoDTO()
								 {
									 CodModelo = c.CodModelo,
									 CodPai = c.CodPai,
									 TempoLp = c.TempoLp,
									 TempoPadrao = c.TempoPadrao,
									 Usuario = c.Usuario,
									 rowguid = c.rowguid,
									 DesModelo = i.DesItem,
									 flgAtivo = i.FlgAtivoItem
								 });

				return resultado.ToList();
			}
		}

		public static TempoPadraoDTO ObterTempoPadraoIDW(string codModelo)
		{

			using (var idw = new IDWContext())
			{
				var resultado = (from c in idw.dw_folhacic
								 join f in idw.dw_folha on c.id_folha equals f.id_folha
								 where f.cd_folha.Contains(codModelo) && f.st_ativo == 1
								 orderby c.seg_ciclopadrao descending
								 select new TempoPadraoDTO()
								 {
									 CodModelo = codModelo,
									 CodPai = "",
									 TempoLp = 0,
									 TempoPadrao = c.seg_ciclopadrao,
									 Usuario = "",
									 rowguid = Guid.NewGuid()
								 }).FirstOrDefault();

				if (!String.IsNullOrEmpty(codModelo)) { 
					resultado.DesModelo = DAO.Rastreabilidade.daoGeral.ObterDescricaoItem(codModelo);
				}
				return resultado;

				/*
				 
				 
				 SELECT TOP 1000 [id_folhacic]
					  ,[seg_ciclopadrao]
					  ,[id_folha]
					  ,[id_pt]
					  ,[id_gt]
				-- select avg(seg_ciclopadrao)
				  FROM [IDW].[dbo].[dw_folhacic]
				  where  id_gt is not null and id_folha in (
					select id_folha FROM [IDW].[dbo].[dw_folha]
					where cd_folha like '598672-SMD' and st_ativo = 1
					) 
				 */

			}
		}

	}
}
