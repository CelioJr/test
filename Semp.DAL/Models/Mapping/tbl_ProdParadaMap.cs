using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_ProdParadaMap : EntityTypeConfiguration<tbl_ProdParada>
    {
        public tbl_ProdParadaMap()
        {
			// Primary Key
			this.HasKey(t => new { t.CodFab, t.NumParada });

			// Properties
			this.Property(t => t.CodFab)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(2)
                .IsUnicode(false);

			this.Property(t => t.CodLinha)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(8)
                .IsUnicode(false);

			this.Property(t => t.NumParada)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(9)
                .IsUnicode(false);

			this.Property(t => t.CodModelo)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(6)
                .IsUnicode(false);

			this.Property(t => t.CodParada)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(4)
                .IsUnicode(false);

			this.Property(t => t.CodItem)
				.IsFixedLength()
				.HasMaxLength(6)
                .IsUnicode(false);

			this.Property(t => t.HorInicio)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(5)
                .IsUnicode(false);

			this.Property(t => t.HorFim)
				.IsFixedLength()
				.HasMaxLength(5)
                .IsUnicode(false);

			this.Property(t => t.StatusPerda)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			this.Property(t => t.CodTurno)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			this.Property(t => t.HorAlmoco)
				.IsFixedLength()
				.HasMaxLength(5)
                .IsUnicode(false);

			this.Property(t => t.DesObs)
				.IsFixedLength()
				.HasMaxLength(5000)
                .IsUnicode(false);

			this.Property(t => t.StatusEmail)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			this.Property(t => t.NumDrt)
				.HasMaxLength(8)
                .IsUnicode(false);

			// Table & Column Mappings
			this.ToTable("tbl_ProdParada");
			this.Property(t => t.CodFab).HasColumnName("CodFab");
			this.Property(t => t.CodLinha).HasColumnName("CodLinha");
			this.Property(t => t.NumParada).HasColumnName("NumParada");
			this.Property(t => t.CodModelo).HasColumnName("CodModelo");
			this.Property(t => t.DatParada).HasColumnName("DatParada");
			this.Property(t => t.CodParada).HasColumnName("CodParada");
			this.Property(t => t.CodItem).HasColumnName("CodItem");
			this.Property(t => t.HorInicio).HasColumnName("HorInicio");
			this.Property(t => t.HorFim).HasColumnName("HorFim");
			this.Property(t => t.StatusPerda).HasColumnName("StatusPerda");
			this.Property(t => t.QtdItem).HasColumnName("QtdItem");
			this.Property(t => t.QtdNaoProduzida).HasColumnName("QtdNaoProduzida");
			this.Property(t => t.CodTurno).HasColumnName("CodTurno");
			this.Property(t => t.HorAlmoco).HasColumnName("HorAlmoco");
			this.Property(t => t.NumFuncionarios).HasColumnName("NumFuncionarios");
			this.Property(t => t.QtdFalta).HasColumnName("QtdFalta");
			this.Property(t => t.DesObs).HasColumnName("DesObs");
			this.Property(t => t.StatusEmail).HasColumnName("StatusEmail");
			this.Property(t => t.NumDrt).HasColumnName("NumDrt");
			this.Property(t => t.rowguid).HasColumnName("rowguid");
        }
    }
}
