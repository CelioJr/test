namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_PPB_MONTH")]
    public partial class ALC_PPB_MONTH
    {
        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal ID_PPB_MONTH { get; set; }

        [StringLength(30)]
        public string FULL_SKU { get; set; }

        public int? PPB_MONTH { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? QTY { get; set; }

        public int? PPB_YEAR { get; set; }
    }
}
