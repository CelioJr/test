using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_ProdDiaPlacaMap : EntityTypeConfiguration<tbl_ProdDiaPlaca>
    {
        public tbl_ProdDiaPlacaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.CodPlaca, t.DatProducao, t.CodLinha, t.CodProcesso, t.QtdProduzida, t.rowguid, t.FlagTP, t.NumTurno });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodPlaca)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodLinha)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.CodProcesso)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.QtdProduzida)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FlagTP)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.NumTurno)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("tbl_ProdDiaPlaca");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.CodPlaca).HasColumnName("CodPlaca");
            this.Property(t => t.DatProducao).HasColumnName("DatProducao");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.CodProcesso).HasColumnName("CodProcesso");
            this.Property(t => t.QtdProduzida).HasColumnName("QtdProduzida");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
            this.Property(t => t.QtdApontada).HasColumnName("QtdApontada");
            this.Property(t => t.QtdDivida).HasColumnName("QtdDivida");
            this.Property(t => t.FlagTP).HasColumnName("FlagTP");
            this.Property(t => t.NumTurno).HasColumnName("NumTurno");
        }
    }
}
