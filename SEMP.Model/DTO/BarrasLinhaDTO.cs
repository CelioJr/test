using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
    public class BarrasLinhaDTO
    {
		[Display(Name = "Linha SISAP")]
        public string CodLinhaCB { get; set; }
		[Display(Name = "C�d. Linha")]
        public string CodLinha { get; set; }
		[Display(Name = "F�b.")]
        public int flgFabrica { get; set; }
		[Display(Name = "Descri��o")]
        public string DesLinha { get; set; }
		[Display(Name = "Nome Est.")]
        public string WStation { get; set; }
		[Display(Name = "Linha Rel")]
		public string CodLinhaRel { get; set; }
		[Display(Name = "Linha T1")]
        public string CodLinhaT1 { get; set; }
		[Display(Name = "Linha T2")]
        public string CodLinhaT2 { get; set; }
		[Display(Name = "Linha T3")]
        public string CodLinhaT3 { get; set; }
        public string Setor { get; set; }
        public string Produto { get; set; }
		[Display(Name = "Fam.")]
        public string CodFam { get; set; }
		[Display(Name = "Seq. Leitura")]
        public string flgSeqLeit { get; set; }
		[Display(Name = "Sa�da Inter. 1")]
        public string HoraSaiIntervalo1 { get; set; }
		[Display(Name = "Entrada Inter. 1")]
        public string HoraRetIntervalo1 { get; set; }
		[Display(Name = "Sa�da Inter. 2")]
        public string HoraSaiIntervalo2 { get; set; }
		[Display(Name = "Entrada Inter. 2")]
        public string HoraRetIntervalo2 { get; set; }
		[Display(Name = "Sa�da Almo�o")]
        public string HoraSaiAlmoco { get; set; }
		[Display(Name = "Retorno Almo�o")]
        public string HoraRetAlmoco { get; set; }
		[Display(Name = "Sa�da Janta")]
        public string HoraSaiJanta { get; set; }
		[Display(Name = "Retorno Janta")]
        public string HoraRetJanta { get; set; }
		[Display(Name = "Hora Entrada")]
        public string HoraEntrada { get; set; }
		[Display(Name = "Hora Sa�da")]
        public string HoraSaida { get; set; }
		[Display(Name = "Ativo")]
        public string flgAtivo { get; set; }
		[Display(Name = "Status")]
        public string flgStatus { get; set; }
		[Display(Name = "C�d. Fam.")]
        public string Familia { get; set; }
		[Display(Name = "Hora Entrada 2T")]
        public string HoraEntradaT2 { get; set; }
		[Display(Name = "Hora Sa�da 2T")]
        public string HoraSaidaT2 { get; set; }
		[Display(Name = "Est. Expedi��o")]
        public string NomeEstacaoExp { get; set; }
    }
}
