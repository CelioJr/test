namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_HOUR_SHIFT")]
    public partial class ALC_HOUR_SHIFT
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_TARGET { get; set; }

        public int? HOUR { get; set; }

        public int? SEQ { get; set; }

        public int? TARGET { get; set; }

        public virtual ALC_TARGET_HOUR ALC_TARGET_HOUR { get; set; }
    }
}
