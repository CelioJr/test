namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_indpt
    {
        [Key]
        public long id_indpt { get; set; }

        public decimal? ind_inferior { get; set; }

        public decimal? ind_superior { get; set; }

        public decimal? ind_meta { get; set; }

        public long id_pt { get; set; }

        public long id_ind { get; set; }

        public virtual om_ind om_ind { get; set; }

        public virtual om_pt om_pt { get; set; }
    }
}
