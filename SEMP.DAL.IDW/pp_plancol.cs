namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_plancol
    {
        [Key]
        public long id_plancol { get; set; }

        public long id_plano { get; set; }

        public short? is_roteiro { get; set; }

        public short? is_produto { get; set; }

        public short? is_qtdmagaz { get; set; }

        public short? is_ncjt { get; set; }

        public short? is_qtplan { get; set; }

        public short? is_qtcjt { get; set; }

        public short? is_st { get; set; }

        public short? is_prodhora { get; set; }

        public short? is_tmpestim { get; set; }

        public short? is_acuml { get; set; }

        public short? is_inicio { get; set; }

        public short? is_fim { get; set; }

        public short? is_turno { get; set; }

        public short? is_dtcobertura { get; set; }

        public short? is_idcp { get; set; }

        public short? is_producao { get; set; }

        public short? is_saldoproduzir { get; set; }

        public short? is_iniciosaldo { get; set; }

        public short? is_ap_aberta { get; set; }

        public virtual pp_plano pp_plano { get; set; }
    }
}
