﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Materiais
{
	public class PainelGestao
	{
		public enum Tipo { Laudo, Recebimento, Pagamento, Aprovacao, RecebimentoProducao }

		public Tipo Categoria { get; set; }
		public string Setor { get; set; }
		public int Quantidade { get; set; }
		public string Cor { get; set; }
		public string DatUltimoRegistro { get; set; }
		public int QtdCritico { get; set; }
		
	}
}
