using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_IMAGEM_DTO
    {
        public FR_IMAGEM_DTO()
        {
            this.FR_SISTEMA = new List<FR_SISTEMA_DTO>();
        }

        public int IMG_CODIGO { get; set; }
        public byte[] IMG_IMAGEM { get; set; }
        public string IMG_GUID { get; set; }
        public virtual ICollection<FR_SISTEMA_DTO> FR_SISTEMA { get; set; }
    }
}
