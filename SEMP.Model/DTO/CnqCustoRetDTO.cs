using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
	public partial class CnqCustoRetDTO
	{
		[Display(Name = "F�b.")]
		public string CodFab { get; set; }
		[Display(Name = "M�s de Refer�ncia")]
		[DisplayFormat(DataFormatString = "{0:MM/yyyy}")]
		public System.DateTime MesRef { get; set; }
		[Display(Name = "C�d. Fam.")]
		public string CodFam { get; set; }
		[Display(Name = "Valor do Retrabalho")]
		[DisplayFormat(DataFormatString = "{0:c2}")]
		public decimal? ValReTrabalho { get; set; }
		[Display(Name = "Production Amount")]
		[DisplayFormat(DataFormatString = "{0:c2}")]
		public decimal? ProdAmount { get; set; }
		[Display(Name = "Fam�lia")]
		public string NomFam { get; set; }
	}
}
