namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_licenca
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_licenca()
        {
            om_licenca_pt = new HashSet<om_licenca_pt>();
            om_licmodrec = new HashSet<om_licmodrec>();
        }

        [Key]
        public long id_licenca { get; set; }

        public long id_empresa { get; set; }

        public DateTime? dt_validade { get; set; }

        public short? is_tryout { get; set; }

        [StringLength(512)]
        public string chave_verificacao { get; set; }

        public int? qt_licencas { get; set; }

        public long id_tplicenca { get; set; }

        public DateTime? dt_emissao { get; set; }

        public int? qt_vezesclienteautoupdate { get; set; }

        [StringLength(100)]
        public string documento { get; set; }

        [StringLength(512)]
        public string chave_verificacao_empresa { get; set; }

        public virtual om_empresa om_empresa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_licenca_pt> om_licenca_pt { get; set; }

        public virtual om_tplicenca om_tplicenca { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_licmodrec> om_licmodrec { get; set; }
    }
}
