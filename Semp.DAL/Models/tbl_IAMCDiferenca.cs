using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_IAMCDiferenca
    {
        public string CodModelo { get; set; }
        public System.DateTime DatProducao { get; set; }
        public string Fase { get; set; }
        public Nullable<System.DateTime> DatEstudo { get; set; }
        public Nullable<decimal> QtdProgramada { get; set; }
        public Nullable<decimal> QtdProduzida { get; set; }
        public Nullable<decimal> Diferenca { get; set; }
        public string MotivoDiferenca { get; set; }
        public string FlgFechado { get; set; }
    }
}
