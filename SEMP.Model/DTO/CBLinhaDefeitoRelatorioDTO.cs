using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class CBLinhaDefeitoRelatorioDTO
    {
        public int CodLinha { get; set; }
        public System.DateTime DatProducao { get; set; }
        public string CodDefeito { get; set; }
    }
}
