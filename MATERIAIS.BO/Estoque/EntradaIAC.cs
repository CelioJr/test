﻿using SEMP.DAL.DAO.Materiais;
using SEMP.Model.DTO;
using System;
using System.Collections.Generic;

namespace MATERIAIS.BO.Estoque
{
	public class EntradaIAC
	{
		/// <summary>
		/// Obter dados de entrada de materiais por Ordem de Produção
		/// </summary>
		/// <param name="numOP"></param>
		/// <returns></returns>
		public static List<CBEntradaIACDTO> ObterEntradasIAC(string numOP, DateTime datInicio, DateTime datFim)
		{
			return daoEntradaIAC.ObterEntradasIAC(numOP, datInicio, datFim);
		}

		public static bool GravarEntradaIAC(CBEntradaIACDTO dados)
		{
			return daoEntradaIAC.GravarEntradaIAC(dados);
		}

		/// <summary>
		/// Obter os dados de uma etiqueta lida
		/// </summary>
		/// <param name="BarrasUD">Código de barras da etiqueta</param>
		/// <returns></returns>
		public static CBEntradaIACDTO ObterLeituraUD(string BarrasUD)
		{
			return daoEntradaIAC.ObterLeituraUD(BarrasUD);
		}

		/// <summary>
		/// Obter os dados de um range de UDs
		/// </summary>
		/// <param name="numUD">Lista com as UDs a consultar</param>
		/// <returns></returns>
		public static List<CBEntradaIACDTO> ObterLeituraUD(List<string> numUD)
		{
			return daoEntradaIAC.ObterLeituraUD(numUD);
		}

		/// <summary>
		/// Obter os dados de uma etiqueta de fornecedor lida
		/// </summary>
		/// <param name="BarrasFornecedor">Código de barras da etiqueta</param>
		/// <returns></returns>
		public static CBEntradaIACDTO ObterLeituraFornecedor(string BarrasFornecedor)
		{
			return daoEntradaIAC.ObterLeituraFornecedor(BarrasFornecedor);

		}

	}
}
