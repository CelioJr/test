﻿using RAST.BO.Mobilidade;
using SEMP.Model.DTO;
using SEMP.Model.VO.SAP;
using SEMP.SAPConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace SEMP.Testes
{
	public partial class frmApontamentoMOB : Form
	{
		public ZSPP_HEADER_APONTAMENTO_MOB apontamento = new ZSPP_HEADER_APONTAMENTO_MOB();
		public List<ALC_VOLUME_DTO> lotes = new List<ALC_VOLUME_DTO>();
		public List<ALC_LOT_LIST_DTO> caixas = new List<ALC_LOT_LIST_DTO>();

		public frmApontamentoMOB()
		{
			InitializeComponent();
		}

		private void btnConsultar_Click(object sender, EventArgs e)
		{
			if (grdDados.Rows.Count == 0)
			{
				MessageBox.Show("Não foi incluído nenhum serial no lote");
				return;
			}

			if (String.IsNullOrEmpty(txtLote.Text)) return;
			if (String.IsNullOrEmpty(txtCodLinha.Text))
			{
				txtCodLinha.Text = "71";
			}
			if (String.IsNullOrEmpty(txtPosto.Text))
			{
				txtCodLinha.Text = "13";
			}
			if (String.IsNullOrEmpty(txtPeso.Text))
			{
				txtPeso.Text = "10";
			}
			if (String.IsNullOrEmpty(txtOrdem.Text))
			{
				MessageBox.Show("Não foi informada a ordem");
				return;
			}
			try
			{
				apontamento.LOTE = txtLote.Text;
				apontamento.ORDEM = txtOrdem.Text.PadLeft(12, '0');
				apontamento.CODLINHA = int.Parse(txtCodLinha.Text);
				apontamento.CODPOSTO = int.Parse(txtPosto.Text);
				apontamento.PESO = double.Parse(txtPeso.Text);
				apontamento.QUANTIDADE = int.Parse(txtQuantidade.Text);

				var dados = SAPFunctions.ZFMPP_APONTAMENTO_OP_MOB(apontamento);
				//txtRetorno.AppendText(dados.Mensagem);
				MessageBox.Show(dados.Mensagem);
			}
			catch (Exception ex)
			{

				MessageBox.Show($"Erro ao consultar: {ex.Message}");
			}
		}

		private void grdDados_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			txtQtdRegistros.Text = grdDados.Rows.Count.ToString();
			txtQuantidade.Text = grdDados.Rows.Count.ToString();
		}

		private void btnAdicionar_Click(object sender, EventArgs e)
		{
			var item = new ZSPP_ITEM_APONTAMENTO_MOB()
			{
				CPF = txtCPF.Text.PadLeft(15, '0'),
				DATA_REAL_PROD = DateTime.Parse(txtDATA_REAL_PROD.Text),
				HORA_REAL_PROD = txtHORA_REAL_PROD.Text,
				IMEICOD1 = txtIMEICOD1.Text,
				IMEICOD2 = txtIMEICOD2.Text,
				NECOD1 = txtNECOD1.Text.PadLeft(18, '0'),
				NECOD2 = txtNECOD2.Text != "" ? txtNECOD2.Text.PadLeft(18, '0') : "",
				PESO = txtPESO_ITEM.Text,
				SERIAL = txtSERIAL.Text
			};

			apontamento.ITEM.Add(item);

			grdDados.DataSource = null;
			grdDados.DataSource = apontamento.ITEM;
			grdDados.Refresh();
		}

		private void frmApontamentoMOB_Load(object sender, EventArgs e)
		{
			var ping = SAPFunctions.TestaConn();

			chkPing.Checked = ping;

			apontamento = new ZSPP_HEADER_APONTAMENTO_MOB();

			this.Top = 90;
			this.Left = 0;
			this.Height = Screen.PrimaryScreen.WorkingArea.Height - 90;
			this.Width = Screen.PrimaryScreen.WorkingArea.Width;
		}

		private void btnCarregarLotes_Click(object sender, EventArgs e)
		{
			grdGRP.DataSource = null;
			grdGRP.Refresh();

			grdDados.DataSource = null;
			grdDados.Refresh();

			var datInicio = DateTime.Parse(DateTime.Now.AddDays(-DateTime.Now.Day).AddDays(1).ToShortDateString());
			var datFim = datInicio.AddMonths(1).AddDays(-1);

			lotes = Alcatel.ObterLotes(datInicio, datFim);
			if (lotes != null && lotes.Count > 0){

				var dados = lotes.Where(x => x.STATUS == "FINALIZADO").Select(x => new { 
					Lote = x.ID_VOL
				}).ToList();

				grdLotes.DataSource = dados;
				grdDados.Refresh();
			}
			
		}

		private void btnCarregarGRPS_Click(object sender, EventArgs e)
		{
			if (grdLotes.SelectedRows.Count > 0)
			{
				grdDados.DataSource = null;
				grdDados.Refresh();

				var idlote = grdLotes.SelectedRows[0].Cells[0].Value.ToString();

				var dado = lotes.FirstOrDefault(x => x.ID_VOL == idlote);
				txtQuantidade.Text = dado.QTY_PROD.ToString();
				txtDATA_REAL_PROD.Text = dado.DATE_CREATE.Value.ToShortDateString();
				txtHORA_REAL_PROD.Text = dado.DATE_CREATE.Value.ToString("HH:mm:ss");

				caixas = Alcatel.ObterLotesItens(idlote);

				if (caixas != null && caixas.Count > 0) {
					var dados = caixas.Select(x => new { x.PACKING }).ToList();
					grdGRP.DataSource = dados;
					grdGRP.Refresh();
				}
			}
		}

		private void btnAddGRP_Click(object sender, EventArgs e)
		{

			try
			{

				if (txtGRP.Text != "")
				{

					var grp = Alcatel.ObterLoteItem(txtGRP.Text.Replace("\r\n", ""));
					caixas.Add(grp);

					var dados = caixas.Select(x => new { x.PACKING }).ToList();
					grdGRP.DataSource = dados;
					grdGRP.Refresh();

					txtGRP.Text = "";
				}
				else
				{
					return;
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show($"Erro: {ex.Message}");
				throw;
			}

		}

		private void txtGRP_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter){
				btnAddGRP_Click(sender, e);
			}
		}

		private void btnSeriais_Click(object sender, EventArgs e)
		{

			var lista = new List<ZSPP_ITEM_APONTAMENTO_MOB>();
			decimal pesoLote = 0;

			for (int i = 0; i < grdGRP.RowCount; i++)
			{
				var seriais = GBR.ObterPack(grdGRP.Rows[i].Cells[0].Value.ToString());
				//var seriais = GBR.ObterPack(txtGRP.Text);

				foreach (var serial in seriais)
				{
					var cadSerial = GBR.ObterSerial(serial.SERIAL);
					pesoLote += cadSerial.PESO ?? 0;
					var item = new ZSPP_ITEM_APONTAMENTO_MOB()
					{
						CPF = txtCPF.Text.PadLeft(15, '0'),
						DATA_REAL_PROD = DateTime.Parse(txtDATA_REAL_PROD.Text),
						HORA_REAL_PROD = txtHORA_REAL_PROD.Text,
						IMEICOD1 = cadSerial.IMEI_1,
						IMEICOD2 = cadSerial.IMEI_2,
						NECOD1 = txtNECOD1.Text.PadLeft(18, '0'),
						NECOD2 = txtNECOD2.Text != "" ? txtNECOD2.Text.PadLeft(18, '0') : "",
						PESO = cadSerial.PESO.ToString(),
						SERIAL = serial.SERIAL
					};
					lista.Add(item);
				}
			}

			txtPeso.Text = pesoLote.ToString();
			apontamento.ITEM = lista;

			grdDados.DataSource = null;
			grdDados.DataSource = apontamento.ITEM;
			grdDados.Refresh();
		}
	}
}
