namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_turno
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_turno()
        {
            dw_calavu = new HashSet<dw_calavu>();
            dw_calsem = new HashSet<dw_calsem>();
            dw_consolestlocalpro = new HashSet<dw_consolestlocalpro>();
            dw_consolestlocalprotemp = new HashSet<dw_consolestlocalprotemp>();
            dw_consolid = new HashSet<dw_consolid>();
            dw_estmov = new HashSet<dw_estmov>();
            dw_rt = new HashSet<dw_rt>();
            om_proturno = new HashSet<om_proturno>();
            pp_cp_turno = new HashSet<pp_cp_turno>();
        }

        [Key]
        public long id_turno { get; set; }

        [StringLength(30)]
        public string cd_turno { get; set; }

        public int? revisao { get; set; }

        [StringLength(100)]
        public string ds_turno { get; set; }

        public long id_usrstativo { get; set; }

        public long id_usrrevisao { get; set; }

        public DateTime? dt_revisao { get; set; }

        public DateTime? dt_stativo { get; set; }

        public short? st_ativo { get; set; }

        [StringLength(11)]
        public string cor { get; set; }

        public short? is_improdutivo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_calavu> dw_calavu { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_calsem> dw_calsem { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolestlocalpro> dw_consolestlocalpro { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolestlocalprotemp> dw_consolestlocalprotemp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolid> dw_consolid { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_estmov> dw_estmov { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rt> dw_rt { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_proturno> om_proturno { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cp_turno> pp_cp_turno { get; set; }
    }
}
