﻿using System;
using System.Collections.Generic;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
	public class RetornoGraficoGargalo
	{
		public List<DadosGraficoA> dados { get; set; }
		public List<DadosGraficoA> metas { get; set; }
		public List<DadosGraficoA> plano { get; set; }
		public List<DadosGraficoA> menor { get; set; }
		public List<DadosGraficoA> modelos { get; set; }
		public Limite limite { get; set; }
	}

	public class RetornoGraficoGargalo2
	{
		public List<Categorias> Categorias { get; set; }
		public List<DadosGraficoA> dados { get; set; }
		public List<DadosGraficoB> agregado { get; set; }
		public List<DadosGraficoA> plano { get; set; }
		public List<DadosGraficoA> menor { get; set; }
		public List<DadosGraficoA> modelos { get; set; }
		public Limite limite { get; set; }
	}

	public class DadosGraficoA
	{
		public string name { get; set; }
		public double y { get; set; }
		public string color { get; set; }
		public string drilldown { get; set; }
		public string codmodelo { get; set; }

	}

	public class DadosGraficoB
	{
		public string name { get; set; }
		public string codmodelo { get; set; }
		public int[] data { get; set; }
		public string stack { get; set; }
		public string color { get; set; }

	}

	public struct Limite
	{
		public double max { get; set; }
		public double min { get; set; }
	}

	public class Categorias
	{
		public string CodModelo { get; set; }
		public string DesPosto { get; set; }
	}
}
