﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.Rastreabilidade;
using SEMP.Model;
using SEMP.BO;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using RAST.BO.Relatorios;
using SEMP.Model.DTO;
using DotNet.Highcharts;
using DotNet.Highcharts.Options;
using System.Drawing;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using SEMP.Models;

namespace SEMP.Areas.Coleta.Controllers
{
	/// <summary>
	/// Classe que controla as Action da funcionalidade Indicadores
	/// </summary>
	[Authorize]
	public class IndicadoresController : Controller
	{
		/// <summary>
		/// Método Index método que controla a pagina Inicial da Home.
		/// </summary>
		/// <param name="DatIni">Parâmetro que recebe a Data inicial do filtro.</param>
		/// <param name="DatFim">Parâmetro que recebe a Data Final do filtro.</param>
		/// <returns>Retorna a View Index.cshtml</returns>
		public ActionResult Index(string DatIni, string DatFim)
		{
			List<IndicadorGlobal> indicador = new List<IndicadorGlobal>();
			try
			{

				DateTime dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
				DateTime dFim = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));

				if (DatIni != null && DatFim != null)
				{
					dIni = Convert.ToDateTime(DatIni);
					dFim = Convert.ToDateTime(DatFim);
				}

				indicador = Indicadores.ObterIndicadorGlobal(dIni, dFim);
			}
			catch (Exception ex)
			{
				//Log.AppLog().ErrorException("\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name, ex);
				Log.AppLog().Error(ex, "\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name);
				ViewBag.Erro = "Erro: " + ex.Message + "\n " + DatIni + "\n " + DatFim;
			}
			return View(indicador);
		}

		/// <summary>
		/// Método RelFabrica que controla a pagina de listagem de Indicadores por Fabrica.
		/// </summary>
		/// <param name="DatIni">Parâmetro que recebe a data inicial do filtro.</param>
		/// <param name="DatFim">Parâmetro que recebe a data final do filtro.</param>
		/// <param name="Fabrica">Parâmetro que recebe a Fabrica do filtro.</param>
		/// <param name="Familia">Parâmetro que recebe a Familia do Filtro.</param>
		/// <returns>Retorna a View RelFabrica.cshtml</returns>
		public ActionResult RelFabrica(string DatIni, string DatFim, string Fabrica, string Familia)
		{
			try
			{

				List<IndicadorMF> indicador = new List<IndicadorMF>();
				List<IndicadorMF> resumoFabrica = new List<IndicadorMF>();
				List<IndicadorMF> postoTecnico = new List<IndicadorMF>();
				List<string> TitulosIndicadores = new List<string> { "Total Produzido", "Eficácia (%)", "Total Defeitos", "Índice Defeitos (%)" };
				List<string> TitulosResumoFabrica = new List<string> { "Total Produzido", "Total Defeitos", "Índice Defeitos (%)" };
				List<string> TitulosPostoTecnico = new List<string> { "Qtd Defeitos Apontados", "Qtd Defeitos Reparados", "Qtd Pendentes Reparo" };

				var linhas = Indicadores.GetLinhas(Familia, Fabrica);

				DateTime dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
				DateTime dFim = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));

				if (DatIni != null && DatFim != null)
				{
					dIni = Convert.ToDateTime(DatIni);
					dFim = Convert.ToDateTime(DatFim).AddDays(1).AddMinutes(-1);
				}

				if (Fabrica == "IAC")
				{
					indicador = Indicadores.ObterIndicadorMF(dIni, dFim, Fabrica, Familia);
				}
				else if (Fabrica == "IMC")
				{
					indicador = Indicadores.ObterIndicadorMF(dIni, dFim, Fabrica, Familia);
				}
				else
				{
					indicador = Indicadores.ObterIndicadorMF(dIni, dFim, Fabrica, Familia);
				}

				resumoFabrica.Add(new IndicadorMF
				{
					Titulo = "Total Produzido",
					Indicador = 1,
					Real = indicador.Where(a => a.Titulo == "Total Produzido").Sum(s => s.Real),
					Plan = indicador.Where(a => a.Titulo == "Total Produzido").Sum(s => s.Plan)
				});

				resumoFabrica.Add(new IndicadorMF
				{
					Titulo = "Total Defeitos",
					Indicador = 3,
					Real = indicador.Where(a => a.Titulo == "Total Defeitos").Sum(s => s.Real),
					Plan = (Indicadores.ObterPadraoIndicadorFabrica(4, Fabrica) / 100) * indicador.Where(a => a.Titulo == "Total Produzido").FirstOrDefault().Plan
				});

				resumoFabrica.Add(new IndicadorMF
				{
					Titulo = "Índice Defeitos (%)",
					Indicador = 4,
					Real = resumoFabrica.Where(a => a.Titulo == "Total Produzido").Sum(s => s.Real) == 0 ? 0 : Math.Round((resumoFabrica.Where(a => a.Titulo == "Total Defeitos").Sum(s => s.Real) / resumoFabrica.Where(a => a.Titulo == "Total Produzido").Sum(s => s.Real)) * 100, 2),
					Plan = Indicadores.ObterPadraoIndicadorFabrica(4, Fabrica)
				});

				postoTecnico.Add(new IndicadorMF
				{
					Titulo = "Qtd Defeitos Apontados",
					Real = indicador.Where(a => a.Titulo == "Total Defeitos").Sum(s => s.Real),
				});

				postoTecnico.Add(new IndicadorMF
				{
					Titulo = "Qtd Defeitos Reparados",
					Real = Indicadores.ObterTotalDefeitoReparadosPorFabrica(dIni, dFim, Fabrica, Familia)

				});

				postoTecnico.Add(new IndicadorMF
				{
					Titulo = "Qtd Pendentes Reparo",
					Real = postoTecnico.FirstOrDefault(a => a.Titulo == "Qtd Defeitos Apontados").Real - postoTecnico.FirstOrDefault(a => a.Titulo == "Qtd Defeitos Reparados").Real
				});

				ViewData["ListaFamilia"] = new SelectList(ListFamilia, "Value", "Text", Familia);
				ViewData["ListaFabrica"] = new SelectList(ListFabricas, "Value", "Text", Fabrica);

				ViewBag.Linhas = linhas;
				ViewBag.TitulosIndicadores = TitulosIndicadores;
				ViewBag.TitulosResumoFabrica = TitulosResumoFabrica;
				ViewBag.TitulosPostoTecnico = TitulosPostoTecnico;

				ViewBag.Indicador = indicador;
				ViewBag.ResumoFabrica = resumoFabrica;
				ViewBag.PostoTecnico = postoTecnico;
				ViewBag.Fabrica = Fabrica;
				ViewBag.Familia = string.IsNullOrWhiteSpace(Familia) == true ? "LCD" : Familia;

				return View(indicador);
			}
			catch (Exception ex)
			{
				//Log.AppLog().ErrorException("\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name, ex);
				Log.AppLog().Error(ex, "\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name);
				ViewBag.Erro = "Erro: " + ex.Message;
			}
			return View(new IndicadorMF());
		}

		/// <summary>
		/// Método Postos que controla a pagina de listagem das linhas de acordo com os filtros.
		/// </summary>
		/// <param name="DatIni">Parâmetro que recebe a data inicial do filtro.</param>
		/// <param name="DatFim">Parâmetro que recebe a data final do filtro.</param>
		/// <param name="Fabrica">Parâmetro que recebe a Fabrica do filtro.</param>
		/// <param name="CodLinha">Parâmetro que recebe o Codigo da linha do filtro.</param>
		/// <param name="Familia">Parâmetro que recebe a Familia do filtro.</param>
		/// <param name="Turno">Parâmetro que recebe o Turno do Filtro.</param>
		/// <returns>Retorna a View Postos.cshtml</returns>
		public ActionResult Postos(string DatIni, string DatFim, string Fabrica, int CodLinha, string Familia, int? Turno)
		{

			try
			{

				DateTime dIni = DateTime.Now;
				DateTime dFim = dIni.AddDays(1).AddMinutes(-1);

				if (DatIni != null && DatFim != null)
				{
					dIni = Convert.ToDateTime(DatIni);
					dFim = Convert.ToDateTime(DatFim).AddDays(1).AddMinutes(-1);
				}

				int t = 1;
				if (Turno != null)
				{
					t = (int)Turno;
				}

				List<IndicadorPostos> listaIndicadores = new List<IndicadorPostos>();
				List<IndicadorMF> resumoPosto = new List<IndicadorMF>();
				List<IndicadorMF> postoTecnico = new List<IndicadorMF>();

				List<string> TitulosResumoPosto = new List<string> { "Qtd Produzida", "Qtd Defeitos", "Índice Defeitos (%)" };
				List<string> TitulosPostoTecnico = new List<string> { "Qtd Defeitos Apontados", "Qtd Defeitos Reparados", "Qtd Pendentes Reparo" };

				List<CBPostoDTO> postos = Indicadores.GetPostos(CodLinha);
				List<IndicadorPostos> listaIndicadoresQuantidadeProduzidaConsulta = Indicadores.ObterIndicadoresQuantidadeProduzidaPostos(dIni, dFim, CodLinha, t);
				List<IndicadorPostos> listaIndicadoresQuantidadeDefeitoConsulta = Indicadores.ObterIndicadoresQuantidadeDefeitos(dIni, dFim, CodLinha, t);

				CBLinhaDTO Linha = Indicadores.GetLinhaByCodigo(CodLinha);

				IndicadorPostos qtdProduzida = null;
				IndicadorPostos qtdDefeitos = null;
				IndicadorPostos indiceDefeito = null;

				foreach (CBPostoDTO p in postos)
				{
					qtdProduzida = new IndicadorPostos
					{
						Titulo = "Qtd. Produzida",
						NumPosto = p.NumPosto,
						Quantidade = listaIndicadoresQuantidadeProduzidaConsulta.Any(a => a.NumPosto.Equals(p.NumPosto)) == true ?
						listaIndicadoresQuantidadeProduzidaConsulta.FirstOrDefault(a => a.NumPosto.Equals(p.NumPosto)).Quantidade
						:
						0
					};

					listaIndicadores.Add(qtdProduzida);

					qtdDefeitos = new IndicadorPostos
					{
						Titulo = "Qtd. Defeitos",
						NumPosto = p.NumPosto,
						Quantidade = listaIndicadoresQuantidadeDefeitoConsulta.Any(a => a.NumPosto.Equals(p.NumPosto)) == true ?
						listaIndicadoresQuantidadeDefeitoConsulta.Where(a => a.NumPosto.Equals(p.NumPosto)).FirstOrDefault().Quantidade
						:
						0
					};

					listaIndicadores.Add(qtdDefeitos);

					indiceDefeito = new IndicadorPostos
					{
						Titulo = "Índice Defeitos (%)",
						NumPosto = p.NumPosto,
						Quantidade = qtdProduzida.Quantidade == 0 ? 0 : Math.Round((qtdDefeitos.Quantidade / qtdProduzida.Quantidade) * 100, 2)
					};

					listaIndicadores.Add(indiceDefeito);
				}

				resumoPosto.Add(new IndicadorMF
				{
					Titulo = "Qtd Produzida",
					Real = listaIndicadores.Where(a => a.Titulo == "Qtd. Produzida").Max(m => m.Quantidade),
					Plan = Indicadores.ObterPlano(Fabrica, dIni, dFim, Linha.CodLinhaT1)
				});

				resumoPosto.Add(new IndicadorMF
				{
					Titulo = "Qtd Defeitos",
					Real = listaIndicadores.Where(a => a.Titulo == "Qtd. Defeitos").Sum(m => m.Quantidade),
					Plan = (Indicadores.ObterPadraoIndicador(4, Fabrica, CodLinha) / 100) * resumoPosto.FirstOrDefault(x => x.Titulo == "Qtd Produzida").Plan
				});

				var indicadorMf = resumoPosto.FirstOrDefault(a => a.Titulo == "Qtd Defeitos");
				if (indicadorMf != null)
				{
					var prod = resumoPosto.FirstOrDefault(a => a.Titulo == "Qtd Produzida");

					if (prod != null && prod.Real > 0)
						resumoPosto.Add(new IndicadorMF
						{
							Titulo = "Índice Defeitos (%)",
							Real = indicadorMf.Real == 0 ? 0 : Math.Round((indicadorMf.Real / prod.Real) * 100, 2),
							Plan = Indicadores.ObterPadraoIndicador(4, Fabrica, CodLinha)
						});
				}

				postoTecnico.Add(new IndicadorMF
				{
					Titulo = "Qtd Defeitos Apontados",
					Real = listaIndicadores.Where(a => a.Titulo == "Qtd. Defeitos").Sum(m => m.Quantidade)
				});

				postoTecnico.Add(new IndicadorMF
				{
					Titulo = "Qtd Defeitos Reparados",
					Real = Indicadores.ObterTotalDefeitoReparadosPorLinha(dIni, dFim, CodLinha)
				});

				postoTecnico.Add(new IndicadorMF
				{
					Titulo = "Qtd Pendentes Reparo",
					Real = postoTecnico.FirstOrDefault(a => a.Titulo == "Qtd Defeitos Apontados").Real - postoTecnico.FirstOrDefault(a => a.Titulo == "Qtd Defeitos Reparados").Real
				});

				ViewBag.Fabrica = Fabrica;
				ViewBag.Familia = string.IsNullOrWhiteSpace(Familia) == true ? "LCD" : Familia; ;
				ViewBag.CodLinha = CodLinha;
				ViewBag.DatIni = DatIni;
				ViewBag.DatFim = DatFim;
				ViewBag.Postos = postos;
				ViewBag.CountPostos = postos.Count + 1;
				ViewBag.Turno = t;
				ViewBag.IndicadorPosto = listaIndicadores;
				ViewBag.Linha = Linha.DesLinha.ToUpper();

				ViewBag.ResumoPosto = resumoPosto;
				ViewBag.TituloResumoPosto = TitulosResumoPosto;

				ViewBag.PostoTecnico = postoTecnico;
				ViewBag.TitulosPostoTecnico = TitulosPostoTecnico;

				return View();
			}
			catch (Exception ex)
			{
				Log.AppLog().Error(ex, "\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name);
				//Log.AppLog().ErrorException("\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name, ex);
				ViewBag.Erro = "Erro: " + ex.Message;
			}
			return View();
		}

		/// <summary>
		/// Método PostoDetalhe que controla a pagina de listagem de indicadores por hora.
		/// </summary>
		/// <param name="DatIni">Parâmetro que recebe a data inicial do filtro.</param>
		/// <param name="DatFim">Parâmetro que recebe a data final do filtro.</param>
		/// <param name="Fabrica">Parâmetro que recebe a Fabrica do filtro.</param>
		/// <param name="CodLinha">Parâmetro que recebe o Codigo da linha do filtro.</param>
		/// <param name="Familia">Parâmetro que recebe a Familia do filtro.</param>
		/// <param name="Turno">Parâmetro que recebe o Turno do filtro.</param>
		/// <param name="Posto">Parâmetro que recebe o Posto do filtro.</param>
		/// <returns>Retorna a View PostoDetalhe.cshtml</returns>
		public ActionResult PostoDetalhe(string DatIni, string DatFim, string Fabrica, string Familia, int CodLinha, int Posto, int? Turno)
		{

			try
			{

				DateTime dIni = DateTime.Now;
				DateTime dFim = dIni.AddDays(1).AddMinutes(-1);

				if (DatIni != null && DatFim != null)
				{
					dIni = Convert.ToDateTime(DatIni);
					dFim = Convert.ToDateTime(DatFim).AddDays(1).AddMinutes(-1);
				}

				int t = 1;
				if (Turno != null)
				{
					t = (int)Turno;
				}

				var horarios = Indicadores.GetHorarios(t);

				List<IndicadorPostoHoraHora> listaIndicadoresPostoHora = new List<IndicadorPostoHoraHora>();
				List<string> TitulosResumoPosto = new List<string> { "Qtd Produzida", "Qtd Defeitos", "Índice Defeitos (%)" };
				List<string> TitulosPostoTecnico = new List<string> { "Qtd Defeitos Apontados", "Qtd Defeitos Reparados", "Qtd Pendentes Reparo" };
				List<IndicadorMF> resumoPosto = new List<IndicadorMF>();
				List<IndicadorMF> postoTecnico = new List<IndicadorMF>();
				IndicadorPostoHoraHora qtdProduzida = null;
				IndicadorPostoHoraHora qtdDefeitos = null;
				IndicadorPostoHoraHora indiceDefeito = null;

				foreach (var item in horarios)
				{
					qtdProduzida = new IndicadorPostoHoraHora("Qtd. Produzida");
					qtdProduzida.Hora = item.Hora;
					qtdProduzida.HoraInicio = item.HoraInicio;
					qtdProduzida.HoraFim = item.HoraFim;
					qtdProduzida.Quantidade = Indicadores.GetQuantidadeProduzidaPostoEHora(dIni, dFim, item.HoraInicio, item.HoraFim, Posto, t, CodLinha);
					listaIndicadoresPostoHora.Add(qtdProduzida);

					qtdDefeitos = new IndicadorPostoHoraHora("Qtd. Defeitos");
					qtdDefeitos.Hora = item.Hora;
					qtdDefeitos.HoraInicio = item.HoraInicio;
					qtdDefeitos.HoraFim = item.HoraFim;
					qtdDefeitos.Quantidade = Indicadores.GetQuantidadeDefeitosPostoEHora(dIni, dFim, item.HoraInicio, item.HoraFim, Posto, t, CodLinha);
					listaIndicadoresPostoHora.Add(qtdDefeitos);

					indiceDefeito = new IndicadorPostoHoraHora("Índice Defeitos (%)");
					indiceDefeito.Hora = item.Hora;
					indiceDefeito.HoraInicio = item.HoraInicio;
					indiceDefeito.HoraFim = item.HoraFim;
					indiceDefeito.Quantidade = qtdProduzida.Quantidade == 0 ? 0 : Math.Round((qtdDefeitos.Quantidade / qtdProduzida.Quantidade) * 100, 2);
					listaIndicadoresPostoHora.Add(indiceDefeito);
				}

				resumoPosto.Add(new IndicadorMF
				{
					Titulo = "Qtd Produzida",
					Real = listaIndicadoresPostoHora.Where(a => a.Titulo == "Qtd. Produzida").Sum(m => m.Quantidade)
				});

				resumoPosto.Add(new IndicadorMF
				{
					Titulo = "Qtd Defeitos",
					Real = listaIndicadoresPostoHora.Where(a => a.Titulo == "Qtd. Defeitos").Sum(m => m.Quantidade)
				});

				resumoPosto.Add(new IndicadorMF
				{
					Titulo = "Índice Defeitos (%)",
					Real = resumoPosto.FirstOrDefault(a => a.Titulo == "Qtd Produzida").Real == 0 ? 0 : Math.Round(resumoPosto.FirstOrDefault(a => a.Titulo == "Qtd Defeitos").Real / resumoPosto.FirstOrDefault(a => a.Titulo == "Qtd Produzida").Real * 100, 2)
				});

				postoTecnico.Add(new IndicadorMF
				{
					Titulo = "Qtd Defeitos Apontados",
					Real = listaIndicadoresPostoHora.Where(a => a.Titulo == "Qtd. Defeitos").Sum(m => m.Quantidade)
				});

				postoTecnico.Add(new IndicadorMF
				{
					Titulo = "Qtd Defeitos Reparados",
					Real = Indicadores.ObterTotalDefeitoReparadosPorPosto(dIni, dFim, CodLinha, Posto)
				});

				postoTecnico.Add(new IndicadorMF
				{
					Titulo = "Qtd Pendentes Reparo",
					Real = postoTecnico.FirstOrDefault(a => a.Titulo == "Qtd Defeitos Apontados").Real - postoTecnico.FirstOrDefault(a => a.Titulo == "Qtd Defeitos Reparados").Real
				});

				ViewBag.Horarios = horarios;
				ViewBag.CountColunas = horarios.Count + 1;
				ViewBag.Fabrica = Fabrica;
				ViewBag.Familia = string.IsNullOrWhiteSpace(Familia) == true ? "LCD" : Familia; ;
				ViewBag.CodLinha = CodLinha;
				ViewBag.DatIni = DatIni;
				ViewBag.DatFim = DatFim;
				ViewBag.Turno = t;
				ViewBag.Posto = Posto;
				ViewBag.ListaIndicadorPostoHora = listaIndicadoresPostoHora;

				ViewBag.ResumoPosto = resumoPosto;
				ViewBag.TituloResumoPosto = TitulosResumoPosto;

				ViewBag.PostoTecnico = postoTecnico;
				ViewBag.TitulosPostoTecnico = TitulosPostoTecnico;

				return View();
			}
			catch (Exception ex)
			{
				//Log.AppLog().ErrorException("\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name, ex);
				Log.AppLog().Error(ex, "\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name);
				ViewBag.Erro = "Erro: " + ex.Message;
			}
			return View();
		}

		/// <summary>
		/// Método PostoTecnico que controla a pagina de listagem de defeitos do posto técnico de acordo com o filtro informado.
		/// </summary>
		/// <param name="DatIni">Parâmetro que recebe a data inicial do filtro.</param>
		/// <param name="DatFim">Parâmetro que recebe a data final do filtro.</param>
		/// <param name="Fabrica">Parâmetro que recebe a Fabrica do filtro.</param>
		/// <param name="CodLinha">Parâmetro que recebe o Codigo da linha do filtro.</param>
		/// <param name="Familia">Parâmetro que recebe a Familia do filtro.</param>
		/// <param name="Turno">Parâmetro que recebe o Turno do filtro.</param>
		/// <param name="Posto">Parâmetro que recebe o Posto do filtro.</param>
		/// <returns>Retorna a View PostoTecnico.cshtml</returns>
		public ActionResult PostoTecnico(string DatIni, string DatFim, string Fabrica, string Familia, int? CodLinha, int? Posto, int? Turno)
		{
			try
			{

				DateTime dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
				DateTime dFim = dIni.AddDays(1).AddMinutes(-1);

				if (!String.IsNullOrEmpty(DatIni) && !String.IsNullOrEmpty(DatFim))
				{
					dIni = Convert.ToDateTime(DatIni);
					dFim = Convert.ToDateTime(DatFim).AddDays(1).AddMinutes(-1);
				}

				int t = 1;
				if (Turno != null)
				{
					t = (int)Turno;
				}

				List<viw_CBDefeitoTurnoDTO> defeitos = Indicadores.GetDefeitos(dIni, dFim, Fabrica, Familia, CodLinha, Posto).OrderBy(o => o.FlgRevisado).ToList();

				ViewBag.Fabrica = Fabrica;
				ViewBag.Familia = string.IsNullOrWhiteSpace(Familia) ? "LCD" : Familia; ;
				ViewBag.CodLinha = CodLinha;
				var linha = Geral.ObterLinha(CodLinha);
				ViewBag.DesLinha = linha != null ? linha.DesLinha : "";
				ViewBag.DatIni = DatIni;
				ViewBag.DatFim = DatFim;
				ViewBag.Turno = t;
				ViewBag.Posto = Posto;
				ViewBag.DesPosto = Posto != null ? Geral.ObterPosto(CodLinha, Posto).DesPosto : "";

				return View(defeitos);
			}
			catch (Exception ex)
			{
				//Log.AppLog().ErrorException("\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name, ex);
				Log.AppLog().Error(ex, "\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name);
				ViewBag.Erro = "Erro: " + ex.Message;
			}
			return View(new List<viw_CBDefeitoTurnoDTO>());
		}

		/// <summary>
		/// Método Grafico que exibe o grafico em barra/pizza.
		/// </summary>
		/// <param name="DatIni">Parâmetro que recebe a data inicial do filtro.</param>
		/// <param name="DatFim">Parâmetro que recebe a data final do filtro.</param>
		/// <param name="Fabrica">Parâmetro que recebe a Fabrica do filtro.</param>
		/// <param name="Familia">Parâmetro que recebe a Familia do filtro.</param>
		/// <param name="Grafico">Parâmero que recebe qual o grafico que vai ser gerado.</param>
		/// <param name="CodLinha">Parâmetro que recebe o Codigo da linha do filtro.</param>
		/// <param name="Turno">Parâmetro que recebe o Turno do filtro.</param>
		/// <param name="Posto">Parâmetro que recebe o Posto do filtro.</param>
		/// <returns>Retorna a View Grafico.cshtml</returns>
		public ActionResult Grafico(string DatIni, string DatFim, string Fabrica, string Familia, string Grafico, int? CodLinha, int? Posto, int? Turno)
		{
			try
			{

				DateTime dIni = DateTime.Now;
				DateTime dFim = dIni.AddDays(1).AddMinutes(-1);

				if (DatIni != null && DatFim != null)
				{
					dIni = Convert.ToDateTime(DatIni);
					dFim = Convert.ToDateTime(DatFim).AddDays(1).AddMinutes(-1);
				}

				int t = 1;
				if (Turno != null)
				{
					t = (int)Turno;
				}

				ViewBag.Fabrica = Fabrica;
				ViewBag.Familia = string.IsNullOrWhiteSpace(Familia) == true ? "LCD" : Familia; ;
				ViewBag.CodLinha = CodLinha;
				ViewBag.DatIni = DatIni;
				ViewBag.DatFim = DatFim;
				ViewBag.Turno = t;
				ViewBag.Posto = Posto;

				List<viw_CBDefeitoTurnoDTO> defeitos = Indicadores.GetDefeitos(dIni, dFim, Fabrica, Familia, CodLinha, Posto);
				List<DotNet.Highcharts.Options.Point> listaPoint = null;
				Highcharts chart = null;
				string[] categories = null;

				if (Grafico.Equals("CodDefeito"))
				{
					var lista = defeitos.GroupBy(a => new { a.CodDefeito, a.DesDefeito }).Select(s => new IndicadorLegenda()
					{
						Codigo = s.Key.CodDefeito,
						Descricao = s.Key.DesDefeito,
						Quantidade = s.Count()
					}).ToList();

					for (var i = 0; i < lista.Count; i++)
					{
						lista[i].Cor = Util.GetRandomColor(i);
					}

					ViewBag.legenda = lista;

					categories = lista.Select(s => s.Codigo).ToArray();
					listaPoint = new List<DotNet.Highcharts.Options.Point>();
					lista.ToList().ForEach(f => listaPoint.Add(new DotNet.Highcharts.Options.Point
					{
						Y = f.Quantidade,
						Color = f.Cor
					}));
					Data data = new Data(listaPoint.ToArray());
					chart = GerarGraficoBarra(data, categories, "Código Defeito");
				}
				else if (Grafico.Equals("CodCausa"))
				{
					var lista = defeitos.Where(x => !String.IsNullOrEmpty(x.CodCausa)).GroupBy(a => new { a.CodCausa, a.DesCausa }).Select(s => new IndicadorLegenda()
					{
						Codigo = s.Key.CodCausa,
						Descricao = s.Key.DesCausa,
						Quantidade = s.Count()
					}).ToList();

					for (var i = 0; i < lista.Count; i++)
					{
						lista[i].Cor = Util.GetRandomColor(i);
					}

					//var lista = defeitos.Where(w => !string.IsNullOrWhiteSpace(w.CodCausa)).GroupBy(a => a.CodCausa).Select(s => new { CodCausa = s.Key, Count = s.Count() });
					var listaVazia = defeitos.Where(w => string.IsNullOrWhiteSpace(w.CodCausa)).GroupBy(a => a.CodCausa).Select(s => new IndicadorLegenda()
					{
						Codigo = "Sem Causa",
						Descricao = "Sem Causa",
						Quantidade = s.Count()
					}).ToList();

					listaVazia = listaVazia.GroupBy(a => new { a.Codigo, a.Descricao }).Select(s => new IndicadorLegenda()
					{
						Codigo = s.Key.Codigo,
						Descricao = s.Key.Descricao,
						Quantidade = s.Sum(d => d.Quantidade),
						Cor = Color.Black
					}).ToList();

					lista = lista.Union(listaVazia).ToList();

					ViewBag.legenda = lista;

					categories = lista.Select(s => s.Codigo).ToArray();
					listaPoint = new List<DotNet.Highcharts.Options.Point>();
					lista.ToList().ForEach(f => listaPoint.Add(new DotNet.Highcharts.Options.Point
					{
						Y = f.Quantidade,
						Color = f.Cor
					}));
					Data data = new Data(listaPoint.ToArray());
					chart = GerarGraficoBarra(data, categories, "Código Causa");
				}
				else if (Grafico.Equals("Posicao"))
				{
					var lista = defeitos.Where(w => !string.IsNullOrWhiteSpace(w.Posicao)).GroupBy(a => a.Posicao).Select(s => new { Posicao = s.Key, Count = s.Count() });
					var listaVazia = defeitos.Where(w => string.IsNullOrWhiteSpace(w.Posicao)).GroupBy(a => a.Posicao).Select(s => new { Posicao = "Sem Posição", Count = s.Count() });

					listaVazia = listaVazia.GroupBy(a => a.Posicao).Select(s => new { Posicao = "Sem Posição", Count = s.Sum(d => d.Count) }).ToList();

					lista = lista.Union(listaVazia);

					categories = lista.Select(s => s.Posicao).ToArray();
					listaPoint = new List<DotNet.Highcharts.Options.Point>();
					lista.ToList().ForEach(f => listaPoint.Add(new DotNet.Highcharts.Options.Point
					{
						Y = f.Count,
						Color = Color.FromName("colors[0]")
					}));
					Data data = new Data(listaPoint.ToArray());
					chart = GerarGraficoBarra(data, categories, "Posicao");
				}
				else if (Grafico.Equals("DesAcao"))
				{
					var lista = defeitos.Where(w => !string.IsNullOrWhiteSpace(w.DesAcao)).GroupBy(a => a.DesAcao).Select(s => new { DesAcao = s.Key, Count = s.Count() });
					var listaVazia = defeitos.Where(w => string.IsNullOrWhiteSpace(w.DesAcao)).GroupBy(a => a.DesAcao).Select(s => new { DesAcao = "AÇÃO NÃO INFORMADA", Count = s.Count() });
					lista = lista.Union(listaVazia);

					categories = lista.Select(s => s.DesAcao).ToArray();
					listaPoint = new List<DotNet.Highcharts.Options.Point>();
					lista.ToList().ForEach(f => listaPoint.Add(new DotNet.Highcharts.Options.Point
					{
						Name = f.DesAcao,
						Y = f.Count
					}));
					Data data = new Data(listaPoint.ToArray());
					chart = GerarGraficoPizza(data, categories, "Descrição Ação");
				}

				return View(chart);

			}
			catch (Exception ex)
			{
				//Log.AppLog().ErrorException("\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name, ex);
				Log.AppLog().Error(ex, "\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name);
				ViewBag.Erro = "Erro: " + ex.Message;
			}

			return View();
		}

		/// <summary>
		///  Método ListFamilia lista as familias existentes
		/// </summary>
		public static IEnumerable<SelectListItem> ListFamilia
		{
			get
			{
				return new[]
				{
					new SelectListItem { Value = "LCD", Text = "LCD" },
					new SelectListItem { Value = "DVD", Text = "DVD/AUD" }
				};
			}
		}

		/// <summary>
		/// Método ListFabricas lista as fabricas existente.
		/// </summary>
		public static IEnumerable<SelectListItem> ListFabricas
		{
			get
			{
				return new[]
				{
					new SelectListItem { Value = "IAC", Text = "IAC" },
					new SelectListItem { Value = "IMC", Text = "IMC" },
					new SelectListItem { Value = "FEC", Text = "MF" },
				};
			}
		}

		/// <summary>
		///  Método que gera um gráfico de barras.
		/// </summary>
		/// <param name="data">Parâmetro data, contém as informações que serão geradas.</param>
		/// <param name="categories">Parâmetro categories contém o nome das categorias.</param>
		/// <param name="titulo">Parâmetro titutlo recebe o nome do gráfico que será gerado.</param>
		/// <returns>Retorna o objeto do tipo Highcharts</returns>
		public Highcharts GerarGraficoBarra(Data data, string[] categories, string titulo)
		{
			try
			{

				Highcharts chart = new Highcharts("chart")
					.InitChart(new Chart { DefaultSeriesType = ChartTypes.Column })
					.SetTitle(new Title { Text = titulo })
					.SetXAxis(new XAxis { Categories = categories })
					.SetYAxis(new YAxis { Title = new YAxisTitle { Text = "" } })
					.SetLegend(new Legend { Enabled = false })
					.SetTooltip(new Tooltip { Formatter = "TooltipFormatter" })
					.SetPlotOptions(new PlotOptions
					{
						Column = new PlotOptionsColumn
						{
							Cursor = Cursors.Pointer,
							Point = new PlotOptionsColumnPoint { Events = new PlotOptionsColumnPointEvents { Click = "ColumnPointClick" } },
							DataLabels = new PlotOptionsColumnDataLabels
							{
								Enabled = true,
								Color = Color.FromName("colors[0]"),
								Formatter = "function() { return this.y +''; }",
								Style = "fontWeight: 'bold'"
							}
						}
					})
					.SetSeries(new Series
					{
						Name = titulo,
						Data = data,
						Color = Color.White
					})
					.SetExporting(new Exporting { Enabled = true })
					.AddJavascripFunction(
						"TooltipFormatter",
						@"var point = this.point, s = this.x +':<b>'+ this.y +'</b><br/>';
					  if (point.drilldown) {
						s += 'Click to view '+ point.category +' versions';
					  } else {
						s += '';
					  }
					  return s;"
					)
					.AddJavascripFunction(
						"ColumnPointClick",
						@"var drilldown = this.drilldown;
					  if (drilldown) { // drill down
						setChart(drilldown.name, drilldown.categories, drilldown.data.data, drilldown.color);
					  } else { // restore
						setChart(name, categories, data.data);
					  }"
					)
					.AddJavascripFunction(
						"setChart",
						@"chart.xAxis[0].setCategories(categories);
					  chart.series[0].remove();
					  chart.addSeries({
						 name: name,
						 data: data,
						 color: color || 'white'
					  });",
						"name", "categories", "data", "color"
					)
					.AddJavascripVariable("colors", "Highcharts.getOptions().colors")
					.AddJavascripVariable("categories", JsonSerializer.Serialize(categories))
					.AddJavascripVariable("data", JsonSerializer.Serialize(data));

				return chart;

			}
			catch (Exception ex)
			{
				//Log.AppLog().ErrorException("\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name, ex);
				Log.AppLog().Error(ex, "\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name);
				ViewBag.Erro = "Erro: " + ex.Message;
			}

			return null;

		}

		/// <summary>
		/// Método que gera um gráfico de pizza
		/// </summary>
		/// <param name="data">Parâmetro data, contém as informações que serão geradas.</param>
		/// <param name="categories">Parâmetro categories contém o nome das categorias.</param>
		/// <param name="titulo">Parâmetro titutlo recebe o nome do gráfico que será gerado.</param>
		/// <returns>Retorna o objeto do tipo Highcharts</returns>
		public Highcharts GerarGraficoPizza(Data data, string[] categories, string titulo)
		{

			try
			{
				Highcharts chart = new Highcharts("chart")
					.InitChart(new Chart { PlotShadow = false })
					.SetTitle(new Title { Text = titulo })
					.SetTooltip(new Tooltip { Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.y +''; }" })
					.SetPlotOptions(new PlotOptions
					{
						Pie = new PlotOptionsPie
						{
							AllowPointSelect = true,
							Cursor = Cursors.Pointer,
							DataLabels = new PlotOptionsPieDataLabels
							{
								Color = ColorTranslator.FromHtml("#000000"),
								ConnectorColor = ColorTranslator.FromHtml("#000000"),
								Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.y +''; }"
							}
						}
					})
					.SetSeries(new Series
					{
						Type = ChartTypes.Pie,
						Name = titulo,
						Data = data
					});

				return chart;
			}
			catch (Exception ex)
			{
				//Log.AppLog().ErrorException("\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name, ex);
				Log.AppLog().Error(ex, "\r\n" + ex.StackTrace + "\r\n" + ex.TargetSite.Name);
				ViewBag.Erro = "Erro: " + ex.Message;
			}
			return null;
		}

	}
}
