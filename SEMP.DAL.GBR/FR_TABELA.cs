namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_TABELA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FR_TABELA()
        {
            FR_CAMPO = new HashSet<FR_CAMPO>();
        }

        [Key]
        [StringLength(96)]
        public string TAB_NOME { get; set; }

        [Required]
        [StringLength(96)]
        public string TAB_DESCRICAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_CAMPO> FR_CAMPO { get; set; }
    }
}
