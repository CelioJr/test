using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class viw_PlanoItemModeloMap : EntityTypeConfiguration<viw_PlanoItemModelo>
    {
        public viw_PlanoItemModeloMap()
        {
            // Primary Key
            this.HasKey(t => t.CodModelo);

            // Properties
            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("viw_PlanoItemModelo");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
        }
    }
}
