namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_CAD_IMEI_BACK
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int COD_IMEI { get; set; }

        public int? COD_SMT { get; set; }

        [StringLength(16)]
        public string IMEI { get; set; }

        public int? ORDEM { get; set; }

        public bool? FLAG_IMEI { get; set; }

        public DateTime? DATA { get; set; }

        [Key]
        [Column(Order = 1)]
        public DateTime DatEnvio { get; set; }
    }
}
