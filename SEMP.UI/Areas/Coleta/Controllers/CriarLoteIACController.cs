﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.AccessControl;
using System.Text.RegularExpressions;
using System.Timers;
using System.Web;
using System.Web.Mvc;
using RAST.BO.Embalagem;
using RAST.BO.Rastreabilidade;
using SEMP.BO;
using SEMP.Model;
using SEMP.Model.DTO;
using SEMP.Model.VO;
using SEMP.Model.VO.Rastreabilidade;

namespace SEMP.Areas.Coleta.Controllers
{
    public class CriarLoteIacController : Controller
    {


        public JsonResult InserirEmbalagem()
        {
            var fabrica = Request.Form["Fabrica"];
            var codDrt = Request.Form["CodDRT"];
            var numSerie = Request.Form["NumSerie"];


            CBEmbaladaDTO produtoEmbalar = null;
            List<CBAmarraDTO> listaAmarra = new List<CBAmarraDTO>();
            Mensagem retorno = new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "LIME", DesMensagem = "LEITURA OK. LEIA PRÓXIMO." };
            var error = false;
            var sessao = Session;

            try
            {
                if (sessao == null)
                {
                    error = true;
                    retorno = Defeito.ObterMensagem(Constantes.CLIENT_ERROR);                    
                }

                if (!error)
                {
                    CBPostoDTO posto = (CBPostoDTO)Session["Posto"];
                    CBLinhaDTO linhaAtual = ((CBLinhaDTO)Session["Linha"]);
                    Passagem passagem = (Passagem)Session["passagem"];
                    List<viw_CBPerfilAmarraDTO> perfils = (List<viw_CBPerfilAmarraDTO>)Session["GetPerfilPosto"];
                    int turno = (Session["Turno"] != null ? (int)Session["Turno"] : Geral.RecuperarTurnoAtual(linhaAtual.CodLinha));
                    IEmbalagem embalagem = Embalagem.Recuperar_Posto_Embalagem((CBPostoDTO)Session["Posto"]);

                    var perfilMasc = (from p in perfils
                                      where new Regex(p.Mascara).IsMatch(numSerie)
                                      select p).FirstOrDefault();

                    if (perfilMasc == null)
                    {
                        error = true;
                        retorno = Defeito.ObterMensagem(Constantes.RAST_REGISTRO_NAO_ENCONTRADO);
                        retorno.DesMensagem = "Máscara não encontrada";
                    }

                    if (!error)
                    {
                        produtoEmbalar = (new CBEmbaladaDTO
                        {
                            CodFab = fabrica,
                            CodLinha = passagem.posto.CodLinha,
                            CodModelo = perfilMasc.CodModelo,
                            CodOperador = codDrt,
                            DatLeitura = DateTime.Now,
                            DatReferencia = DateTime.Now.ToShortDateString(),
                            NumECB = numSerie,
                            NumAP = "00000",
                            NumPosto = passagem.posto.NumPosto,
                            Linha = linhaAtual,
                            Turno = turno
                        });

                        retorno = embalagem.Embalar_Produto(produtoEmbalar, listaAmarra, passagem, posto);

                        if (retorno.CodMensagem != Constantes.RAST_OK)
                            error = true;
                    }
                } 
            }
            catch (Exception ex)
            {
                error = true;
                retorno.CodMensagem = Constantes.DB_ERRO;
                retorno.DesMensagem = ex.Message;
            }

            var jsonData = new
            {
                error = error,
                message = retorno.DesMensagem,
                retorno = retorno
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AmarraLoteConfigIac(CBEmbaladaDTO embalada)
        {
            var sessao = Session;
            bool error = false;

            try
            {
                string codModelo = string.Empty;
                if (sessao != null)
                {
                    var postoCookie = Request.Cookies["Posto"];
                    var posto = Geral.ObterPosto(int.Parse(postoCookie.Values["codLinha"]), int.Parse(postoCookie.Values["numPosto"]));
                    var linha = Geral.ObterLinha(int.Parse(postoCookie.Values["codLinha"]));
                    var dataReferenciaValida = true;

                    embalada.NumAP = "00000";

                    ViewBag.Posto = posto.DesPosto;
                    ViewBag.Linha = linha.DesLinha;

                    #region Validando datas e modelo
                    try
                    {
                        codModelo = Configuracao.ObterDescricaoNE(embalada.CodModelo).CodItem;

                        if (string.IsNullOrEmpty(codModelo))
                        {
                            error = true;
                        }
                        if (Convert.ToDateTime(embalada.DatReferencia) != System.DateTime.Today)
                            dataReferenciaValida = false;
                    }
                    catch
                    {
                        dataReferenciaValida = false;
                        error = true;
                    }


                    #endregion

                    if (ModelState.IsValid && !error)
                    {
                        int codFabrica = int.Parse(embalada.CodFab);
                        embalada.DescricaoModelo = Geral.ObterDescricaoItem(embalada.CodModelo);


                        var resumoDadosAps = " <b> Estação: </b> " + linha.DesLinha + " | " + posto.DesPosto + " </br>";
                        resumoDadosAps += " <b> Modelo: </b> " + embalada.CodModelo + " - " + embalada.DescricaoModelo + " </br>";

                        var qtdLote = Geral.ObterQuantidadeLotePorLinha(linha.CodLinha);
                        var qtdCc = Geral.ObterQuantidadeCaixaColetivaPorLinha(linha.CodLinha);
                        var turno = Geral.RecuperarTurnoAtual(linha.CodLinha);

                        var passagem = new Passagem((DadosPosto)Session["DadosPosto"]);

                        var perfil = new List<viw_CBPerfilAmarraDTO>
                        {
                            new viw_CBPerfilAmarraDTO()
                            {
                                CodLinha = passagem.posto.CodLinha,
                                NumPosto = passagem.posto.NumPosto,
                                CodModelo = codModelo,
                                CodTipoAmarra = 6,
                                CodItem = codModelo,
                                DesItem = Geral.ObterDescricaoItem(codModelo),
                                Mascara = "^" + codModelo + "(\\d{7}|\\d{6})$",
                                DesTipoAmarra = "Produto"
                            }
                        };

                        Session["Posto"] = posto;
                        Session["Turno"] = turno;
                        Session["Linha"] = linha;
                        Session["passagem"] = passagem;
                        Session["GetPerfilPosto"] = perfil;

                        #region Preparando Grid de Histórico
                        var colunas = new List<string> { "Produto" };
                        colunas.AddRange(perfil.Where(a => a.DesTipoAmarra != "EAN13" && a.DesTipoAmarra != "Produto").Select(s => s.DesTipoAmarra).ToList());
                        colunas.Add("Data Leitura");
                        colunas.Add("Magazine");
                        colunas.Add("Lote");
                        ViewBag.ColunasHistorico = string.Join(",", colunas.ToArray());
                        #endregion

                        ViewBag.CodTipoPosto = posto.CodTipoPosto;
                        ViewBag.CodigoFabrica = codFabrica.ToString("00");
                        ViewBag.ResumoDadosAps = resumoDadosAps;
                        ViewBag.Turno = turno;
                        ViewBag.DataReferencia = embalada.DatReferencia;
                        ViewBag.Perfil = string.Join(",", perfil.Select(s => s.DesTipoAmarra).ToArray());
                        ViewBag.QtdeLote = qtdLote;
                        ViewBag.qtdeCC = qtdCc;
                        ViewBag.CodPosto = posto.NumPosto;
                        ViewBag.CodLinha = linha.CodLinha;
                        ViewBag.TempoLeitura = posto.TempoLeitura == null ? 5 : posto.TempoLeitura.Value;
                        ViewBag.ImprimeEtq = posto.flgImprimeEtq;
                        ViewBag.CodLinhaT1 = linha.CodLinhaT1;
						ViewBag.Modelo = embalada.CodModelo;

                        return View("AmarraLoteEntrepostoIAC");
                    }
                    else
                    {
                        if (embalada.CodFab == null)
                        {
                            ModelState.AddModelError("CodFab", "Informe o código da fábrica.");
                        }
                        else if (embalada.CodModelo == null)
                        {
                            ModelState.AddModelError("CodModelo", "Informe o código do Modelo.");
                        }
                        else if (string.IsNullOrEmpty(embalada.DatReferencia))
                        {
                            ModelState.AddModelError("DatReferencia", "Informe a data de referência.");
                        }

                        if (dataReferenciaValida == false)
                        {
                            ModelState.AddModelError("", "Data de referência inválida.");
                        }
                        if (string.IsNullOrEmpty(codModelo))
                        {
                            ModelState.AddModelError("", "Informe um Modelo válido.");
                        }


                        //if (!string.IsNullOrWhiteSpace(validarPostoImpressao))
                        //{
                        //    ModelState.AddModelError("", validarPostoImpressao);
                        //}


                        return View(embalada);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Sessão esgotada, faça novamente o login no sistema.");
                    return View(embalada);
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(embalada);
            }
        }
        //
        // GET: /Coleta/CriarLoteIAC/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ConfigTela()
        {
            return View("ConfigLoteIACSemAP");
        }

        public ActionResult AmarraLoteConfigIac()
        {
            try
            {
                var postoCookie = Request.Cookies["Posto"];

                var posto = Geral.ObterPosto(int.Parse(postoCookie.Values["codLinha"]), int.Parse(postoCookie.Values["numPosto"]));
                var linha = Geral.ObterLinha(int.Parse(postoCookie.Values["codLinha"]));

                ViewBag.Fabrica = postoCookie.Values["Fabrica"];
                ViewBag.Posto = posto.DesPosto;
                ViewBag.Linha = linha.DesLinha;
                ViewBag.Setor = linha.Setor;

            }
            catch
            {
                return RedirectToAction("Index");
            }

            CBEmbaladaDTO embalada = new CBEmbaladaDTO()
            {
                DatReferencia = DateTime.Now.ToShortDateString()
            };
            return View(embalada);
        }

        /// <summary>
        /// Método para fazer a validação de cada item que é lido no posto de embalagem
        /// </summary>
        /// <param name="fabrica"></param>  
        /// <param name="leitura"></param>
        /// <param name="codDrt"></param>        
        /// <returns></returns>
        public JsonResult ValidarLeituraEmbalagem(string fabrica, string leitura, string codDrt)
        {
            var bar = leitura.ToUpper(); //Numero de serie
            var error = false; // inicia sem erros                      
            Mensagem retorno = null;
            var sessao = Session;

            if (sessao != null)
            {
                var passagem = (Passagem)Session["passagem"];
                var perfils = (List<viw_CBPerfilAmarraDTO>)Session["GetPerfilPosto"];

                var perfilMasc = (from p in perfils
                                  where new Regex(p.Mascara).IsMatch(bar)
                                  select p).FirstOrDefault();

                if (perfilMasc == null)
                {
                    error = true;
                    retorno = Defeito.ObterMensagem(Constantes.RAST_REGISTRONAOENCONTRADO);
                }

                if (!error)
                {                 
                    retorno = passagem.ValidaPassagem(leitura, codDrt, passagem.posto.CodLinha, passagem.posto.NumPosto, "");
                    if (retorno.CodMensagem != Constantes.RAST_OK)
                        error = true;
                }
            }
            else
            {
                error = true;
                retorno = Defeito.ObterMensagem(Constantes.CLIENT_ERROR);
            }

            var jsonData = new
            {
                error = error,
                message = retorno.DesMensagem,
                serial = bar,
                retorno = retorno,
                indice = 1

            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

		public JsonResult FecharMagazine(string codFabrica, string codDrt, string justificativa, string codModelo)
		{
			var erro = false;
			var linha = (CBLinhaDTO)Session["Linha"];
			var posto = (CBPostoDTO)Session["Posto"];
			var retorno = Defeito.ObterMensagem(Constantes.RAST_OK);
			var embalagem = Embalagem.Recuperar_Posto_Embalagem(posto);

			string numCC = embalagem.RecuperarNumeroCaixaColetivaAberta("00000", linha.CodLinha, codFabrica);

			if (string.IsNullOrEmpty(numCC))
			{
				erro = true;
				retorno = Defeito.ObterMensagem(Constantes.RAST_LOTE_NAOEXISTE);
			}
			else
			{
				retorno = embalagem.FecharCaixaColetiva(numCC, codDrt, justificativa);

				if (retorno.CodMensagem != Constantes.RAST_OK)
				{
					erro = true;
					retorno = Defeito.ObterMensagem(Constantes.RAST_LOTE_NAOFINALIZADO);
				}

				embalagem.ImprimirCaixaColetiva(numCC, linha, posto);
			}

			var jsonData = new
			{
				error = erro,
				message = retorno.DesMensagem,
				retorno = retorno
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ValidarCrachaReimpressaoEtiqueta(string cracha, string codDrt, bool statusAtual, string modelo, string codFabrica)
		{
			var erro = false;
			var messageErro = "";
			var statusAt = false;

			try
			{
				var passagem = (Passagem)Session["passagem"];
				if (!statusAtual) //está desativado. tem que ativar, por isso valida o crachá
				{

					var retorno = Login.ValidaCracha(cracha);

					if (retorno.Status.CodMensagem == Constantes.DB_ERRO)
					{
						erro = true;
						messageErro = "Crachá Inválido";
					}

					if (!(retorno.Cargo.Equals("069") || retorno.Cargo.Equals("079"))) //reserva de linha ou supervisora de producao
					{
						erro = true;
						messageErro = "Crachá sem permissão para autorizar este tipo de operação";
					}
				}

				if (erro == false)
				{
                    statusAt = Embalagem.Atualizar_Modo_Retrabalho(passagem.posto.CodLinha, passagem.posto.NumPosto, Convert.ToInt32(codDrt));

                    //statusAt = Embalagem.Recuperar_Status_Retrabalho(passagem.posto.CodLinha, passagem.posto.NumPosto);
					 
				}
			}

			catch
			{
				erro = true;
				messageErro = "Crachá inválido. Favor tentar novamente";
			}

			var jsonData = new
			{
				error = erro,
				message = messageErro,
				statusAtual = statusAt,
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		public JsonResult AlterarTamanhoMagazine(int tamanho)
		{
			var erro = false;
			var messageErro = "";

			try
			{
				var passagem = (Passagem)Session["passagem"];

				var embalagem = Embalagem.Recuperar_Posto_Embalagem((CBPostoDTO)Session["Posto"]);

				//se voltar true, significa que deu tudo certo, então a função retorna false, pois não deu erro.
				erro = !embalagem.AlterarTamanhoCaixaColetiva(passagem.posto.CodLinha, tamanho);
			}

			catch
			{
				erro = true;
				messageErro = "Erro ao alterar tamanho do Magazine";
			}

			var jsonData = new
			{
				error = erro,
				valor = tamanho,
				message = messageErro,
				novoTamanho = tamanho
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);

		}

		public JsonResult AlterarTamanhoLote(int tamanho)
		{
			var erro = false;
			var messageErro = "";

			try
			{
				var passagem = (Passagem)Session["passagem"];

				var embalagem = Embalagem.Recuperar_Posto_Embalagem((CBPostoDTO)Session["Posto"]);

				//se voltar true, significa que deu tudo certo, então a função retorna false, pois não deu erro.
				erro = !embalagem.AlterarTamanhoLote(passagem.posto.CodLinha, tamanho);
			}

			catch
			{
				erro = true;
				messageErro = "Erro ao alterar tamanho do lote";
			}

			var jsonData = new
			{
				error = erro,
				valor = tamanho,
				message = messageErro,
				novoTamanho = tamanho
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);

		}

		public JsonResult FecharLote(string codFabrica, string codModelo, string codDrt, string justificativa)
		{
			var erro = false;
			var linha = (CBLinhaDTO)Session["Linha"];
			var posto = (CBPostoDTO)Session["Posto"];
			var retorno = Defeito.ObterMensagem(Constantes.RAST_OK);
			var embalagem = Embalagem.Recuperar_Posto_Embalagem(posto);

			string numLote = embalagem.RecuperarLoteAberto("00000", codModelo, linha.CodLinha, codFabrica);
			string numMagazine = embalagem.RecuperarNumeroCaixaColetivaAberta("00000",codModelo,linha.CodLinha,codFabrica);

			if (string.IsNullOrEmpty(numLote))
			{
				erro = true;
				retorno = Defeito.ObterMensagem(Constantes.RAST_LOTE_NAOEXISTE);
			}
			else
			{
				retorno = embalagem.FecharCaixaColetiva(numMagazine, codDrt, "Lote Finalizado");
				
				if (retorno.CodMensagem != Constantes.RAST_OK)
				{
					erro = true;
					retorno.DesMensagem = "Magazine não pode ser Finalizado!"; 
				}
				else
				{
					retorno = Embalagem.FecharLote(numLote, codDrt, justificativa);

					if (retorno.CodMensagem != Constantes.RAST_OK)
					{
						erro = true;
						retorno = Defeito.ObterMensagem(Constantes.RAST_LOTE_NAOFINALIZADO);
					}
					else
					{
						retorno.DesMensagem = "Lote e Magazine Finalizado!";

						int totalCaixa = Convert.ToInt32(embalagem.ObterTotalProduzidoCaixaColetiva(codFabrica, "00000", linha.CodLinha, codModelo));
						

						if (totalCaixa != 0)
						embalagem.ImprimirCaixaColetiva(numLote, linha, posto);
					}
				}

				
			}

			var jsonData = new
			{
				error = erro,
				message = retorno.DesMensagem,
				retorno = retorno
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterTotalCaixaColetiva(string codFabrica, string codModelo)
		{
			bool error = false;
			string total = "0";
			var totalCaixa="";
			var numAP = "00000";
			
			var sessao = Session;
			Hashtable lista = new Hashtable();

			try
			{
				if (sessao != null)
				{
					Passagem passagem = new Passagem((DadosPosto)Session["DadosPosto"]);
					var linhaAtual = ((CBLinhaDTO)Session["Linha"]);
					var embalagem = Embalagem.Recuperar_Posto_Embalagem((CBPostoDTO)Session["Posto"]);


					totalCaixa = embalagem.ObterTotalProduzidoCaixaColetiva(codFabrica, numAP, linhaAtual.CodLinha, codModelo);

				}
				else
				{
					error = true;
				}
			}
			catch
			{
				error = true;
			}

			var jsonData = new
			{
				error = error,
				total = total,
				ArrayAPRetorno = totalCaixa
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterTotalLote(string codFabrica, string codModelo)
		{
			bool error = false;
			string total = "0";
			int totalLoteAtual = 0 ;
			string NumLoteAbreto = "0";
			
			
			
			var sessao = Session;
			Hashtable lista = new Hashtable();

			try
			{
				if (sessao != null)
				{
					Passagem passagem = new Passagem((DadosPosto)Session["DadosPosto"]);
					var linhaAtual = ((CBLinhaDTO)Session["Linha"]);
					var embalagem = Embalagem.Recuperar_Posto_Embalagem((CBPostoDTO)Session["Posto"]);

					NumLoteAbreto = embalagem.RecuperarLoteAberto(codModelo,linhaAtual.CodLinha,codFabrica);


					totalLoteAtual = embalagem.QtdeCaixasColetivasNoLote(NumLoteAbreto);

					
				}
				else
				{
					error = true;
				}
			}
			catch
			{
				error = true;
			}

			var jsonData = new
			{
				error = error,
				total = total,
				ArrayAPRetorno = totalLoteAtual
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}
		
		public bool RegistrarDefeito()
	    {
		    return true;
	    }

		public JsonResult PreencherArrayAP()
		{
			var perfil = (List<viw_CBPerfilAmarraDTO>)Session["GetPerfilPosto"];

			var listaApsLeitura = (from p in perfil
								   group p by p.CodModelo into grupo
								   select new { Modelo = grupo.Key, QtdEsp = grupo.Count(), QtdeLido = 0 }).ToList();

			return Json(listaApsLeitura, JsonRequestBehavior.AllowGet);

		}
    }
}
