namespace SEMP.Model.DTO
{
	using System.ComponentModel.DataAnnotations;

	public partial class tbl_TAREFA_DTO
	{

        public int ID { get; set; }

        [Required]
        [StringLength(20)]
        public string NOME { get; set; }

        [Required]
        [StringLength(150)]
        public string DESCRICAO { get; set; }

        public int ID_CRITICIDADE { get; set; }

        [Required]
        [StringLength(50)]
        public string ACTION { get; set; }

        public int ID_TIPO { get; set; }

        [Required]
        [StringLength(150)]
        public string COMANDO { get; set; }

        [Required]
        [StringLength(300)]
        public string EMAILRESPONSAVEIS { get; set; }

        public int TENTATIVAS { get; set; }

        public bool ATIVO { get; set; }

        public bool DELETADO { get; set; }
    }
}
