﻿using System;

namespace SEMP.Model.VO.SAP
{
	public class ZSPP_ITEM_APONTAMENTO_MOB
	{
		public string NECOD1 { get; set; }
		public string NECOD2 { get; set; }
		public string SERIAL { get; set; }
		public string IMEICOD1 { get; set; }
		public string IMEICOD2 { get; set; }
		public DateTime DATA_REAL_PROD { get; set; }
		public string HORA_REAL_PROD { get; set; }
		public string CPF { get; set; }
		public string PESO { get; set; }

	}
}
