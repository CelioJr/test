﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using RAST.BO.Rastreabilidade;
using SEMP.Model;
using SEMP.DAL.DAO.Embalagem;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.VO;
using SEMP.Model.DTO;
using SEMP.Model.VO.Embalagem;

namespace RAST.BO.Embalagem
{
	public class EmbalagemNotebook : Embalagem, IEmbalagem
	{
		Mensagem _mensagem;

		/// <summary>
		/// Método que faz a embalagem do produto Notebook
		/// </summary>
		/// <param name="embalagem"></param> e atualiza os dados do banco DTB_SSA
		/// <param name="listaAmarra"></param>
		/// <param name="passagem"></param>
		/// <returns></returns>
		public override Mensagem Embalar_Produto(CBEmbaladaDTO embalagem, List<CBAmarraDTO> listaAmarra, Passagem passagem, CBPostoDTO posto, bool gravarBancoSemp = true)
		{
			var retorno = new Mensagem { CodMensagem = Constantes.RAST_OK };

			retorno = Update_KitAcessorio_DtbCB(listaAmarra);
			if (retorno.CodMensagem != Constantes.RAST_OK)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				retorno.DesMensagem = "Erro ao atualizar Kit Acessório";
			}
			else
			{
				retorno = base.Embalar_Produto(embalagem, listaAmarra, passagem, posto, true);

				if (retorno.CodMensagem == Constantes.RAST_OK)
				{
					if (!Atualizar_DtbSSA_Notebook(embalagem, listaAmarra))
					{
						retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
						retorno.DesMensagem = "Erro ao atualizar Banco SSA";
					}

				}
			}

			return retorno;
		}

		/// <summary>
		/// Método que faz as validações da leitura dos itens que são amarrados ao produto
		/// </summary>
		/// <param name="tipoAmarra"></param>
		/// <param name="leitura"></param>
		/// <param name="numAp"></param>
		/// <param name="numSerie"></param>
		/// <param name="numKitProvisorio"></param>
		/// <returns></returns>
		public override Mensagem ValidarLeitura(int tipoAmarra, string leitura, string numAp, string numSerie, string numKitProvisorio, CBLinhaDTO linha)
		{
			var retorno = new Mensagem();

			if (tipoAmarra == Constantes.AMARRA_KIT)
			{   //validações para kit de embalagem
				retorno = Validar_kit_Embalagem(leitura, numAp, numSerie);
			}
			else if (tipoAmarra == Constantes.AMARRA_LICENCA_NERO)
			{
				retorno = Validar_Licenca_Nero(leitura, numSerie, numKitProvisorio);
			}

			return retorno;
		}

		/// <summary>
		/// Método que faz a validação do produto lido
		/// </summary>
		/// <param name="passagem"></param>
		/// <param name="codDrt"></param>
		/// <param name="numAp"></param>
		/// <param name="leitura"></param>
		/// <param name="modelo"></param>
		/// <param name="codFab"></param>
		/// <param name="linha"></param>
		/// <returns></returns>
		public override Mensagem ValidarProduto(Passagem passagem, String codDrt, string numAp, string leitura, string modelo, string codFab, string codigoCinescopio, CBLinhaDTO linha)
		{
			_mensagem = base.ValidarProduto(passagem, codDrt, numAp, leitura, modelo, codFab, codigoCinescopio, linha);

			if (_mensagem.CodMensagem != Constantes.RAST_OK) return _mensagem;

			if (String.IsNullOrEmpty(Recuperar_Numero_Serie_Toshiba(leitura))) return _mensagem;       //se retornar aqui é porque não é toshiba

			ObterSerialFiltroPrivacidade();
			if (_mensagem.CodMensagem != Constantes.RAST_OK)
				return _mensagem;

			Validar_Mac(leitura);
			if (_mensagem.CodMensagem != Constantes.RAST_OK)
				return _mensagem;

			Validar_Imagem_Toshiba(leitura);

			return _mensagem;
		}

		private static String Recuperar_Numero_Serie_Toshiba(string numeroSerie)
		{
			return DaoEmbalagem.Recuperar_Numero_Serie_Toshiba(numeroSerie);
		}

		/// <summary>
		/// Valida o número do kit acessorio criado no sistema EtqProduto e a respectiva associação com o número de série do produto
		/// </summary>
		/// <param name="strNumEcb"></param>
		/// <param name="numAp"></param>
		/// <param name="numSerie"></param>
		/// <returns></returns>
		private Mensagem Validar_kit_Embalagem(string strNumEcb, string numAp, string numSerie)
		{

			if (String.IsNullOrEmpty(strNumEcb))
			{
				_mensagem = new Mensagem
				{
					CodMensagem = Constantes.KIT_LEITURA_INVALIDA,
					DesMensagem = "Informe um número de kit válido.",
					DesCor = "red"
				};
				return _mensagem;
			}

			if (!Regex.IsMatch(strNumEcb, @"^\d+$"))
			{
				_mensagem = new Mensagem
				{
					CodMensagem = Constantes.KIT_LEITURA_INVALIDA,
					DesMensagem = "Serial do kit não pode conter letras.",
					DesCor = "red"
				};
				return _mensagem;
			}

			KitAcessorio kit = DaoEmbalagem.RecuperarDadosKitAcessorio(strNumEcb);

			if (kit.CodKit == 0)
			{
				_mensagem = new Mensagem
				{
					CodMensagem = Constantes.KIT_LEITURA_INVALIDA,
					DesMensagem = "O n° do kit não foi gerado no banco de dados.",
					DesCor = "red"
				};
			}
			else if (kit.CodOF.ToString(CultureInfo.InvariantCulture) == numAp)
			{
				_mensagem = new Mensagem
				{
					CodMensagem = Constantes.KIT_LEITURA_INVALIDA,
					DesMensagem = "O kit informado não pertence a AP do n° de série lido.",
					DesCor = "red"
				};
			}
			else if (!String.IsNullOrEmpty(kit.NumSerie))
			{
				if (kit.NumSerie.Equals(numSerie))
				{
					_mensagem = new Mensagem
					{
						CodMensagem = Constantes.KIT_LEITURA_INVALIDA,
						DesMensagem = "O n° do kit informado já está associado ao n° de série lido.",
						DesCor = "red"
					};
				}
				else
				{
					_mensagem = new Mensagem
					{
						CodMensagem = Constantes.KIT_LEITURA_INVALIDA,
						DesMensagem = "O n° do kit informado já está associado ao n° de série: " + kit.NumSerie,
						DesCor = "red"
					};
				}
			}
			else
			{
				var codkit = DaoEmbalagem.Verificar_NumeroSerie_Associado_kitEmbalagem(numSerie);

				if (!string.IsNullOrEmpty(codkit))
				{
					_mensagem = new Mensagem
					{
						CodMensagem = Constantes.KIT_LEITURA_INVALIDA,
						DesMensagem = "O n° de série informado já foi associado ao kit de n°: " + codkit,
						DesCor = "red"
					};
				}
			}

			return _mensagem;
		}

		private void Validar_Imagem_Toshiba(string numSerie)
		{
			//recuperando codigo da imagem 
			var codImagem = String.IsNullOrEmpty(numSerie) ? DaoEmbalagem.Recuperar_Codigo_Imagem(numSerie) : DaoEmbalagem.Recuperar_Codigo_Imagem_Toshiba(numSerie);

			var imagemToshiba = DaoEmbalagem.Validar_Imagem_Toshiba(codImagem);

			if (String.IsNullOrEmpty(imagemToshiba))
			{
				_mensagem = new Mensagem
				{
					CodMensagem = Constantes.IMAGEM_TOSHIBA_INVALIDA,
					DesMensagem = "O código da imagem Toshiba não foi encontrado. Por favor entre em contato com a ENG.",
					DesCor = "red"
				};
			}
		}

		private void Validar_Mac(string numSerie)
		{
			string tipo = "LAN_OnBoard";
			Validar_Mac_Toshiba(tipo, numSerie);
			if (_mensagem.CodMensagem != Constantes.RAST_OK)
				return;

			tipo = "Bluetooth";
			Validar_Mac_Toshiba(tipo, numSerie);
			if (_mensagem.CodMensagem != Constantes.RAST_OK)
				return;

			tipo = "Wireless";
			Validar_Mac_Toshiba(tipo, numSerie);
		}

		private void Validar_Mac_Toshiba(String tipo, string numSerie)
		{
			switch (tipo)
			{
				case "LAN_OnBoard":
					var sMacLanOnboard = DaoEmbalagem.Validar_Mac(tipo, numSerie);
					if (String.IsNullOrEmpty(sMacLanOnboard) || sMacLanOnboard.Equals("FFFFFFFFFFFF"))
					{
						_mensagem.CodMensagem = Constantes.NE_FILTRO_PRIVACIDADE_INVALIDO;
						_mensagem.DesMensagem = "O MAC address da placa de rede on-board não foi encontrado. Por favor entre em contato com a ENG.";
						_mensagem.DesCor = "red";
					}
					break;
				case "Bluetooth":
					var sMacBluetooth = DaoEmbalagem.Validar_Mac(tipo, numSerie);
					if (String.IsNullOrEmpty(sMacBluetooth) || sMacBluetooth.Equals("FFFFFFFFFFFF"))
					{
						_mensagem.CodMensagem = Constantes.NE_FILTRO_PRIVACIDADE_INVALIDO;
						_mensagem.DesMensagem = "O MAC address do bluetooth não foi encontrado. Por favor entre em contato com a ENG.";
						_mensagem.DesCor = "red";
					}
					break;
				case "Wireless":
					var sMacWireless = DaoEmbalagem.Validar_Mac(tipo, numSerie);
					if (String.IsNullOrEmpty(sMacWireless) || sMacWireless.Equals("FFFFFFFFFFFF"))
					{
						_mensagem.CodMensagem = Constantes.NE_FILTRO_PRIVACIDADE_INVALIDO;
						_mensagem.DesMensagem = "O MAC address da placa de rede wireless não foi encontrado. Por favor entre em contato com a ENG.";
						_mensagem.DesCor = "red";
					}
					break;
			}
		}

		private static void ObterSerialFiltroPrivacidade()
		{
			////Método responsável por obter e validar o serial do filtro de privacidade quando existe um NE da familia especifica na estrutura da AP
			//FiltroPrivacidade filtro = (new EmbalagemBo()).Obter_Ne_Filtro_Privacidade(_embalagemDto.NumFab);
			//int numProjeto = 0;

			//try
			//{
			//    if (!String.IsNullOrEmpty(filtro.Ne))
			//    {
			//        if (String.IsNullOrEmpty(filtro.DispositivoId))
			//        {
			//            _mensagem.CodMensagem = Constantes.NE_FILTRO_PRIVACIDADE_INVALIDO;
			//            _mensagem.DesMensagem = "NE ( " + filtro.Ne + " ) do filtro não está associado ao dispositivo. Antes de prosseguir contacte a Engenharia.";
			//            _mensagem.DesCor = "red";

			//            return;
			//        }

			//        if (!Regex.IsMatch(_embalagemDto.NumFiltro, @"^\d+$"))
			//        {
			//            _mensagem.CodMensagem = Constantes.NE_FILTRO_PRIVACIDADE_INVALIDO;
			//            _mensagem.DesMensagem = "O serial do filtro de privacidade informado não contem um padrão válido.";
			//            _mensagem.DesCor = "red";
			//            return;
			//        }

			//        if (_embalagemDto.NumFiltro.Trim().Length != 12)
			//        {
			//            _mensagem.CodMensagem = Constantes.NE_FILTRO_PRIVACIDADE_INVALIDO;
			//            _mensagem.DesMensagem = "O serial do filtro de privacidade informado não contem um padrão válido.";
			//            _mensagem.DesCor = "red";
			//            return;
			//        }

			//        if (filtro.Ne != _embalagemDto.NumFiltro.Substring(1, 6))
			//        {
			//            _mensagem.CodMensagem = Constantes.NE_FILTRO_PRIVACIDADE_INVALIDO;
			//            _mensagem.DesMensagem = "O serial do filtro de privacidade informado não pertence ao NE ( " + _embalagemDto.NumFiltro + " ) do filtro presente na estrutura da máquina.";
			//            _mensagem.DesCor = "red";
			//            return;
			//        }

			//        numProjeto = daoEmbalagem.Verificar_Serial_Filtro_Privacidade(filtro.Ne);

			//        if (numProjeto.Equals(0))
			//        {
			//            _mensagem.CodMensagem = Constantes.NE_FILTRO_PRIVACIDADE_INVALIDO;
			//            _mensagem.DesMensagem = "Nenhum serial foi gerado para o NE ( " + filtro.Ne + " ) do filtro.";
			//            _mensagem.DesCor = "red";
			//            return;
			//        }
			//        if (Convert.ToInt32(_embalagemDto.NumFiltro.Substring(7, 12)) > numProjeto)
			//        {
			//            _mensagem.CodMensagem = Constantes.NE_FILTRO_PRIVACIDADE_INVALIDO;
			//            _mensagem.DesMensagem = "Os 6 ultimos digitos do serial do filtro de privacidade são inválidos.";
			//            _mensagem.DesCor = "red";
			//            return;
			//        }

			//        if (daoEmbalagem.Verificar_Serial_Filtro_Associado_Varios_NSs(filtro.Ne, _embalagemDto.NumFiltro, filtro.DispositivoId))
			//        {
			//            _mensagem.CodMensagem = Constantes.NE_FILTRO_PRIVACIDADE_INVALIDO;
			//            _mensagem.DesMensagem = "O serial do filtro de privacidade informado já está associado a outra máquina.";
			//            _mensagem.DesCor = "red";
			//            return;
			//        }
			//    }

			//    _numFiltroPrivacidade = numProjeto;

			//}
			//catch (Exception)
			//{
			//    _mensagem.CodMensagem = Constantes.NE_FILTRO_PRIVACIDADE_INVALIDO;
			//    _mensagem.DesMensagem = "Erro ao consultar Filtro de Privacidade";
			//    _mensagem.DesCor = "red";
			//}
		}

		/// <summary>
		/// Atualiza os dados do kit acessorio nas tabelas do banco dtb_cb
		/// </summary>
		/// <param name="listaAmarra"></param>
		/// <returns></returns>
		private static Mensagem Update_KitAcessorio_DtbCB(List<CBAmarraDTO> listaAmarra)
		{
			var retorno = new Mensagem() { CodMensagem = Constantes.DB_OK };

			try
			{
				var numKit = listaAmarra.FirstOrDefault(t => t.FlgTipo.Equals(Constantes.AMARRA_KIT)).NumECB;
				var numNero = listaAmarra.FirstOrDefault(t => t.FlgTipo.Equals(Constantes.AMARRA_LICENCA_NERO)).NumECB;
				var numSerie = listaAmarra.FirstOrDefault(t => t.FlgTipo.Equals(Constantes.AMARRA_KIT)).NumSerie;

				if (!String.IsNullOrEmpty(numKit))
					DaoEmbalagem.Update_KitAcessorio_DtbCB(Convert.ToInt32(numKit), numSerie, numNero, true);
			}
			catch (Exception ex)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				retorno.StackErro = ex.InnerException.Message;
				retorno.DesMensagem = "Erro ao atualizar Kit Acessório - DTB_CB";
				return retorno;
			}
			return retorno;
		}

		/// <summary>
		/// Método que faz a validação da licença nero com o número de série 
		/// </summary>
		/// <param name="licencaNero"></param>
		/// <param name="numSerie"></param>
		/// <param name="numKit"></param>
		/// <returns></returns>
		private static Mensagem Validar_Licenca_Nero(string licencaNero, string numSerie, string numKit)
		{
			var retorno = new Mensagem();

			if (String.IsNullOrEmpty(licencaNero))
			{
				retorno = new Mensagem()
				{
					CodMensagem = Constantes.RAST_REGISTRONAOENCONTRADO,
					DesMensagem = "Código da etiqueta NERO não é válido."
				};
			}
			else if (!licencaNero.Contains(numSerie) || !licencaNero.Contains(numKit))
			{
				retorno = new Mensagem()
				{
					CodMensagem = Constantes.RAST_REGISTRONAOENCONTRADO,
					DesMensagem = "Código da etiqueta NERO informado não foi gerado com o kit: " + numKit
				};
			}

			return retorno;
		}

		/// <summary>
		/// Métod que faz a chamada para atualização das tabelas do banco DTB_SSA
		/// </summary>
		/// <param name="embalagem"></param>
		/// <param name="listaAmarra"></param>
		/// <returns></returns>
		private static bool Atualizar_DtbSSA_Notebook(CBEmbaladaDTO embalagem, IEnumerable<CBAmarraDTO> listaAmarra)
		{
			return DaoEmbalagem.Atualizar_tabela_dtbSSA_Notebook(embalagem, listaAmarra); ;
		}

	}
}

