using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_USUARIO_SISTEMA_DTO
    {
        public int USR_CODIGO { get; set; }
        public string SIS_CODIGO { get; set; }
        public string USS_ACESSO_EXTERNO { get; set; }
        public string USS_ADMINISTRADOR { get; set; }
        public string USS_ACESSO_MAKER { get; set; }
        public string USS_CRIAR_FORMULARIO { get; set; }
        public string USS_CRIAR_RELATORIO { get; set; }
        public string USS_ACESSAR { get; set; }
        public string USS_CRIAR_REGRA { get; set; }
        public virtual FR_USUARIO_DTO FR_USUARIO { get; set; }
    }
}
