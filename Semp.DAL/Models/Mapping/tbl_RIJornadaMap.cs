using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_RIJornadaMap : EntityTypeConfiguration<tbl_RIJornada>
    {
        public tbl_RIJornadaMap()
        {
            // Primary Key
            this.HasKey(t => t.CodJornada);

            // Properties
            this.Property(t => t.CodJornada)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.flgRegra1)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.flgRegra2)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.flgRegra3)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.flgRegra4)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.flgRegra5)
                .IsFixedLength()
                .HasMaxLength(2);

            // Table & Column Mappings
            this.ToTable("tbl_RIJornada");
            this.Property(t => t.CodJornada).HasColumnName("CodJornada");
            this.Property(t => t.NumCarga).HasColumnName("NumCarga");
            this.Property(t => t.HEntrada).HasColumnName("HEntrada");
            this.Property(t => t.HSaida).HasColumnName("HSaida");
            this.Property(t => t.HSaidaRefeicao).HasColumnName("HSaidaRefeicao");
            this.Property(t => t.HEntradaRefeicao).HasColumnName("HEntradaRefeicao");
            this.Property(t => t.HEntradaAntes).HasColumnName("HEntradaAntes");
            this.Property(t => t.HEntradaDepois).HasColumnName("HEntradaDepois");
            this.Property(t => t.HSaidaAntes).HasColumnName("HSaidaAntes");
            this.Property(t => t.HSaidaDepois).HasColumnName("HSaidaDepois");
            this.Property(t => t.HAdicionalNoturnoIni).HasColumnName("HAdicionalNoturnoIni");
            this.Property(t => t.HAdicionalNoturnoFim).HasColumnName("HAdicionalNoturnoFim");
            this.Property(t => t.HoraExtra1).HasColumnName("HoraExtra1");
            this.Property(t => t.NumHoraExtra1).HasColumnName("NumHoraExtra1");
            this.Property(t => t.HoraExtra2).HasColumnName("HoraExtra2");
            this.Property(t => t.NumHoraExtra2).HasColumnName("NumHoraExtra2");
            this.Property(t => t.HoraExtra3).HasColumnName("HoraExtra3");
            this.Property(t => t.NumHoraExtra3).HasColumnName("NumHoraExtra3");
            this.Property(t => t.HoraExtra4).HasColumnName("HoraExtra4");
            this.Property(t => t.NumHoraExtra4).HasColumnName("NumHoraExtra4");
            this.Property(t => t.HoraExtra5).HasColumnName("HoraExtra5");
            this.Property(t => t.NumHoraExtra5).HasColumnName("NumHoraExtra5");
            this.Property(t => t.flgRegra1).HasColumnName("flgRegra1");
            this.Property(t => t.flgRegra2).HasColumnName("flgRegra2");
            this.Property(t => t.flgRegra3).HasColumnName("flgRegra3");
            this.Property(t => t.flgRegra4).HasColumnName("flgRegra4");
            this.Property(t => t.flgRegra5).HasColumnName("flgRegra5");
        }
    }
}
