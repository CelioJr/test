using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class alc_api_user_DTO
    {
        public alc_api_user_DTO()
        {
            this.alc_api_role = new List<alc_api_role_DTO>();
            this.alc_api_role1 = new List<alc_api_role_DTO>();
        }

        public long id { get; set; }
        public string password { get; set; }
        public string username { get; set; }
        public virtual ICollection<alc_api_role_DTO> alc_api_role { get; set; }
        public virtual ICollection<alc_api_role_DTO> alc_api_role1 { get; set; }
    }
}
