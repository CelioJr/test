namespace SEMP.Model.DTO
{
	using System;
	using System.ComponentModel.DataAnnotations;

	public partial class tbl_historico_DTO
	{
        public int ID { get; set; }

        public int ID_TAREFA { get; set; }

        public int ID_ACAO { get; set; }

        [Required]
        [StringLength(100)]
        public string USUARIO { get; set; }

        public DateTime DATAACAO { get; set; }

        [Required]
        [StringLength(800)]
        public string OBS { get; set; }

    }
}
