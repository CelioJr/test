using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_ACPTIPO_DTO
    {
        public FR_ACPTIPO_DTO()
        {
            this.FR_ACAOPARAMETRO = new List<FR_ACAOPARAMETRO_DTO>();
        }

        public int ACT_CODIGO { get; set; }
        public string ACT_DESCRICAO { get; set; }
        public virtual ICollection<FR_ACAOPARAMETRO_DTO> FR_ACAOPARAMETRO { get; set; }
    }
}
