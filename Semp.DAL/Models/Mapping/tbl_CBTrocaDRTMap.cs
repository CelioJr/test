using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBTrocaDRTMap : EntityTypeConfiguration<tbl_CBTrocaDRT>
    {
        public tbl_CBTrocaDRTMap()
        {
            // Primary Key
            this.HasKey(t => t.CodLinha);

            // Properties
            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodDRT)
                .IsFixedLength()
                .HasMaxLength(8);

            this.Property(t => t.CodDRTAnt)
                .IsFixedLength()
                .HasMaxLength(8);

            // Table & Column Mappings
            this.ToTable("tbl_CBTrocaDRT");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.NumPosto).HasColumnName("NumPosto");
            this.Property(t => t.CodDRT).HasColumnName("CodDRT");
            this.Property(t => t.DatTroca).HasColumnName("DatTroca");
            this.Property(t => t.CodDRTAnt).HasColumnName("CodDRTAnt");


        }
    }
}
