using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_ACAOCOMPONENTE_DTO
    {
        public FR_ACAOCOMPONENTE_DTO()
        {
            this.FR_PARAMETRO = new List<FR_PARAMETRO_DTO>();
        }

        public int FRM_CODIGO { get; set; }
        public int COM_CODIGO { get; set; }
        public int ACO_CODIGO { get; set; }
        public string ACC_MOMENTO { get; set; }
        public string ACC_CONDICAO { get; set; }
        public virtual FR_ACAO_DTO FR_ACAO { get; set; }
        public virtual FR_COMPONENTE_DTO FR_COMPONENTE { get; set; }
        public virtual ICollection<FR_PARAMETRO_DTO> FR_PARAMETRO { get; set; }
    }
}
