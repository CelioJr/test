﻿using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAST.BO.Rastreabilidade
{
	public class ModeloBO
	{
		public static List<CBModeloDTO> ObterModelos(string codItem = "", string desItem = "") {

			var dados = String.IsNullOrEmpty(codItem) ?
									daoModelo.ObterModelos()
									: daoModelo.ObterModelos(x => x.CodModelo == codItem);

			if (!String.IsNullOrEmpty(desItem))
			{
				dados = dados.Where(x => x.DesItem.Contains(desItem)).ToList();
			}
			return dados;

		}

		public static CBModeloDTO ObterModelo(string codModelo)
		{
			return daoModelo.ObterModelo(codModelo);
		}

		public static bool AtualizaModelo(CBModeloDTO modelo) {
			return daoModelo.AtualizaModelo(modelo);

		}
		public static bool ExcluirModelo(CBModeloDTO modelo) {
			return daoModelo.ExcluirModelo(modelo);

		}

	}
}
