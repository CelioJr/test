namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ms_up
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ms_up()
        {
            ms_msicup = new HashSet<ms_msicup>();
            ms_upihm = new HashSet<ms_upihm>();
        }

        [Key]
        public long id_up { get; set; }

        [StringLength(30)]
        public string cd_up { get; set; }

        public int? revisao { get; set; }

        [StringLength(100)]
        public string ds_up { get; set; }

        public DateTime? dthr_stativo { get; set; }

        public DateTime? dthr_revisao { get; set; }

        public int? st_ativo { get; set; }

        [StringLength(1)]
        public string is_Licenciada { get; set; }

        public long? id_usr { get; set; }

        public long? id_evtInicioCiclo { get; set; }

        public long? id_evtInicioParada { get; set; }

        [StringLength(30)]
        public string iduppdba { get; set; }

        [StringLength(8)]
        public string cd_bc { get; set; }

        public int? sequencial { get; set; }

        [StringLength(40)]
        public string nrop { get; set; }

        public short? tp_up { get; set; }

        public virtual ms_evt ms_evt { get; set; }

        public virtual ms_evt ms_evt1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_msicup> ms_msicup { get; set; }

        public virtual ms_usr ms_usr { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_upihm> ms_upihm { get; set; }
    }
}
