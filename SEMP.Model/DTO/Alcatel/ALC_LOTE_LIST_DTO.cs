using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_LOTE_LIST_DTO
    {
        public int ID_LIST { get; set; }
        public Nullable<System.DateTime> dtPallet { get; set; }
        public Nullable<int> LINE { get; set; }
        public string PACKING { get; set; }
        public string ID_LOTE { get; set; }
        public string ORDER_ID { get; set; }
        public Nullable<int> RESP_PALLET { get; set; }
        public virtual ALC_ORDER_DTO ALC_ORDER { get; set; }
        public virtual ALC_SYS_USER_DTO ALC_SYS_USER { get; set; }
    }
}
