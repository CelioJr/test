using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_LSAATNotaRodapeMap : EntityTypeConfiguration<tbl_LSAATNotaRodape>
    {
        public tbl_LSAATNotaRodapeMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.NumAT, t.Sequencia, t.DatPedido });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NumAT)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.Sequencia)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.Especificacao)
                .HasMaxLength(25)
                .IsUnicode(false);

            this.Property(t => t.NumNota)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodEspecie)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.TipNota)
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.CodSerie)
                .HasMaxLength(2)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_LSAATNotaRodape");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.NumAT).HasColumnName("NumAT");
            this.Property(t => t.Sequencia).HasColumnName("Sequencia");
            this.Property(t => t.DatPedido).HasColumnName("DatPedido");
            this.Property(t => t.QtdVolume).HasColumnName("QtdVolume");
            this.Property(t => t.PesoLiquido).HasColumnName("PesoLiquido");
            this.Property(t => t.PesoBruto).HasColumnName("PesoBruto");
            this.Property(t => t.Especificacao).HasColumnName("Especificacao");
            this.Property(t => t.NumNota).HasColumnName("NumNota");
            this.Property(t => t.CodEspecie).HasColumnName("CodEspecie");
            this.Property(t => t.TipNota).HasColumnName("TipNota");
            this.Property(t => t.CodSerie).HasColumnName("CodSerie");
        }
    }
}
