﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.DTO
{
	public class BarrasPainelDTO
	{

		[Display(Name = "Linha")]
		public string Linha { get; set; }
		[Display(Name = "")]
		public string StatusLinha { get; set; }
		[Display(Name = "")]
		public string FlgValida { get; set; }
		[Display(Name = "Modelo")]
		public string CodModelo { get; set; }
		[Display(Name = "Descrição")]
		public string DesModelo { get; set; }
		[Display(Name = "Plano")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? Producao { get; set; }
		[Display(Name = "Embal.")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? Embalados { get; set; }
		[Display(Name = "Difer.")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? DifModelo { get; set; }
		[Display(Name = "Apont.")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? QtdApontado { get; set; }
		[Display(Name = "Prev.")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public int? ProdEsperada { get; set; }
		[Display(Name = "Difer. (H)")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public int? Diferenca { get; set; }
		[Display(Name = "AP")]
		public string NumAP { get; set; }

		public decimal? AparHora { get; set; }
		public decimal? AparMin { get; set; }
		public decimal? HorasTrab { get; set; }
		public decimal? MinutosDia { get; set; }
		public decimal? QtdMinReal { get; set; }
		public int? QtdLoteAP { get; set; }
		public int? QtdEmbaladoAP { get; set; }
		public int? DifUltLeitura { get; set; }
		public string FlgDivida { get; set; }
		public string CodFam { get; set; }
		public DateTime? DatValida { get; set; }
		public DateTime? HoraInicio { get; set; }
		public DateTime? HoraFim { get; set; }
		public string CodLinha { get; set; }
		public DateTime? UltLeitura { get; set; }
		public decimal? TempoParaAcabar { get; set; }
		public DateTime? HoraPrevista { get; set; }
		public decimal? RitmoAtual { get; set; }
		public float? IntervaloTrans { get; set; }
		public decimal? QtdCapacidade { get; set; }
		public DateTime? DataHoraCongelada { get; set; }

		public int? QtdTotalAparelhos { get; set; }

	}
}
