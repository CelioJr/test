using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBTAModeloMap : EntityTypeConfiguration<tbl_CBTAModelo>
    {
        public tbl_CBTAModeloMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodModelo, t.Data });

            // Properties
            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.Observacao)
                .HasMaxLength(300)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_CBTAModelo");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.Data).HasColumnName("Data");
            this.Property(t => t.QtdAparelhos).HasColumnName("QtdAparelhos");
            this.Property(t => t.QtdDefeitos).HasColumnName("QtdDefeitos");
            this.Property(t => t.TempoIAC).HasColumnName("TempoIAC");
            this.Property(t => t.TempoIMC).HasColumnName("TempoIMC");
            this.Property(t => t.TempoMF).HasColumnName("TempoMF");
            this.Property(t => t.WIP1).HasColumnName("WIP1");
            this.Property(t => t.WIP2).HasColumnName("WIP2");
            this.Property(t => t.TempoAtravessa).HasColumnName("TempoAtravessa");
            this.Property(t => t.TempoCorrido).HasColumnName("TempoCorrido");
			this.Property(t => t.TempoWIP).HasColumnName("TempoWIP");
			this.Property(t => t.TempoProducao).HasColumnName("TempoProducao");
			this.Property(t => t.TempoTecnico).HasColumnName("TempoTecnico");
			this.Property(t => t.Observacao).HasColumnName("Observacao");
        }
    }
}
