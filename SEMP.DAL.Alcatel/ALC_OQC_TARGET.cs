namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_OQC_TARGET")]
    public partial class ALC_OQC_TARGET
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ALC_OQC_TARGET()
        {
            ALC_OQC_INSPECTION = new HashSet<ALC_OQC_INSPECTION>();
        }

        public int id { get; set; }

        public int? quantity { get; set; }

        [StringLength(80)]
        public string sku { get; set; }

        public DateTime? dtCreated { get; set; }

        public DateTime? dtFinished { get; set; }

        public bool? activated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_OQC_INSPECTION> ALC_OQC_INSPECTION { get; set; }
    }
}
