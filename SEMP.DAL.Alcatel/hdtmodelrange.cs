namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.hdtmodelrange")]
    public partial class hdtmodelrange
    {
        public long id { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? dateCreated { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? dateEnded { get; set; }

        public int imeiEnd { get; set; }

        public int imeiNext { get; set; }

        public int imeiStart { get; set; }

        public int imeiTac { get; set; }

        public int macEnd { get; set; }

        public int macNext { get; set; }

        public int macStart { get; set; }

        public int macTac { get; set; }

        [StringLength(255)]
        public string status { get; set; }

        [StringLength(255)]
        public string userNameCreated { get; set; }

        [StringLength(255)]
        public string userNameEnded { get; set; }

        public long? hdtModel_id { get; set; }

        public virtual hdtmodel hdtmodel { get; set; }
    }
}
