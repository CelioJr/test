using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBValidaModelo
    {
        public int NumPosto { get; set; }
        public string CodModelo { get; set; }
        public int CodLinha { get; set; }
        public string Setor { get; set; }
    }
}
