namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_operacaopredecessora
    {
        [Key]
        public long id_operacaopredecessora { get; set; }

        public long? id_operacao { get; set; }

        public long? id_operacaoAnterior { get; set; }

        public virtual dw_operacao dw_operacao { get; set; }

        public virtual dw_operacao dw_operacao1 { get; set; }
    }
}
