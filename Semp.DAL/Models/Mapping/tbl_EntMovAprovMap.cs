using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_EntMovAprovMap : EntityTypeConfiguration<tbl_EntMovAprov>
    {
        public tbl_EntMovAprovMap()
        {
            // Primary Key
            this.HasKey(t => new { t.NumRomaneio, t.DatAprov, t.UsuAprov, t.MaqAprov, t.CodStatus });

            // Properties
            this.Property(t => t.NumRomaneio)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.UsuAprov)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);

            this.Property(t => t.MaqAprov)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);

            this.Property(t => t.CodStatus)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_EntMovAprov");
            this.Property(t => t.NumRomaneio).HasColumnName("NumRomaneio");
            this.Property(t => t.DatAprov).HasColumnName("DatAprov");
            this.Property(t => t.UsuAprov).HasColumnName("UsuAprov");
            this.Property(t => t.MaqAprov).HasColumnName("MaqAprov");
            this.Property(t => t.CodStatus).HasColumnName("CodStatus");
        }
    }
}
