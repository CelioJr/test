using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_ProdAmplitudeMap : EntityTypeConfiguration<tbl_ProdAmplitude>
    {
        public tbl_ProdAmplitudeMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.CodDepto, t.NomUsuario });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodDepto)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.NomUsuario)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(30)
                .IsUnicode(false);

            this.Property(t => t.TipUsuario)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.NomEmail)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(60)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_ProdAmplitude");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.CodDepto).HasColumnName("CodDepto");
            this.Property(t => t.NomUsuario).HasColumnName("NomUsuario");
            this.Property(t => t.TipUsuario).HasColumnName("TipUsuario");
            this.Property(t => t.NomEmail).HasColumnName("NomEmail");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
        }
    }
}
