﻿using SAP.Middleware.Connector;
using System;
using System.Data;
using System.Linq;

namespace SEMP.SAPConnector
{
	public static class IRfcFunctionExtensions
	{
		public static DataTable ToDataTable(this IRfcFunction dados)
		{
			DataTable dataTable = new DataTable();
			IRfcTable colunas = dados.GetTable("FIELDS");

			for(int j = 0; j < colunas.Count; j++)
			{
				var col = colunas[j].GetValue(0).ToString();
				var tipo = colunas[j].GetValue("TYPE").ToString();
				DataColumn dataColumn = new DataColumn(col, GetDataType(tipo));
				dataTable.Columns.Add(dataColumn);

			}

			IRfcTable linhas = dados.GetTable("DATA");
			foreach (var row in linhas)
			{
				DataRow dataRow = dataTable.NewRow();
				var celulas = row.GetValue("WA").ToString().Split('|');

				for(int i = 0; i < celulas.Count(); i++)
				{
					var tipo = colunas[i].GetValue("TYPE").ToString();
					if (tipo.Equals("P") || tipo.Equals("B") || tipo.Equals("N"))
					{
						dataRow[i] = celulas[i].Replace(".", ",").Trim();
					}
					else
					{
						dataRow[i] = celulas[i].Trim();
					}
					
				}

				dataTable.Rows.Add(dataRow);
			}
			return dataTable;
		}

		private static Type GetDataType(RfcDataType rfcDataType)
		{
			switch (rfcDataType)
			{
				case RfcDataType.DATE:
					return typeof(string);
				case RfcDataType.CHAR:
					return typeof(string);
				case RfcDataType.STRING:
					return typeof(string);
				case RfcDataType.BCD:
					return typeof(decimal);
				case RfcDataType.INT2:
					return typeof(int);
				case RfcDataType.INT4:
					return typeof(int);
				case RfcDataType.FLOAT:
					return typeof(double);
				default:
					return typeof(string);
			}
		}

		private static Type GetDataType(string rfcDataType)
		{
			switch (rfcDataType)
			{
				case "D":
					return typeof(string);
				case "C":
					return typeof(string);
				case "S":
					return typeof(string);
				case "B":
					return typeof(decimal);
				case "I":
					return typeof(int);
				case "P":
					return typeof(double);
				case "N":
					return typeof(double);
				default:
					return typeof(string);
			}
		}

	}
}
