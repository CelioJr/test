namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_regras_tags
    {
        [Key]
        public long id_regras_tags { get; set; }

        [StringLength(40)]
        public string complemento { get; set; }

        public long? id_regras_nscb { get; set; }

        public long? id_tags { get; set; }

        public int? ordem { get; set; }

        public virtual om_regras_nscb om_regras_nscb { get; set; }

        public virtual om_tags om_tags { get; set; }
    }
}
