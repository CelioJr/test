using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DOC_RELATORIO_DTO
    {
        public int DOC_CODIGO { get; set; }
        public int REL_CODIGO { get; set; }
        public string REL_DESCRICAO { get; set; }
        public string REL_OBJETIVO { get; set; }
        public byte[] REL_IMAGEM { get; set; }
        public virtual FR_DOC_PRINCIPAL_DTO FR_DOC_PRINCIPAL { get; set; }
        public virtual FR_RELATORIO_DTO FR_RELATORIO { get; set; }
    }
}
