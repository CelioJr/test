using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.DTB_SSA.Models.Mapping
{
    public class viw_STI_NEMap : EntityTypeConfiguration<viw_STI_NE>
    {
        public viw_STI_NEMap()
        {
            // Primary Key
            this.HasKey(t => new { t.coditem, t.desItem, t.CodUnid, t.CodSupFamilia, t.CodSubFamilia, t.Tipo_Familia });

            // Properties
            this.Property(t => t.coditem)
                .IsRequired()
                .HasMaxLength(15);

            this.Property(t => t.desItem)
                .IsRequired()
                .HasMaxLength(40);

            this.Property(t => t.CodUnid)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(4);

            this.Property(t => t.CodFamDBF)
                .HasMaxLength(6);

            this.Property(t => t.CodEstrutura)
                .HasMaxLength(12);

            this.Property(t => t.CodSupFamilia)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.CodFamilia)
                .HasMaxLength(6);

            this.Property(t => t.CodSubFamilia)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6);

            this.Property(t => t.CodPartNumber)
                .HasMaxLength(40);

            this.Property(t => t.Tipo_Familia)
                .IsRequired()
                .HasMaxLength(2);

            this.Property(t => t.DesResumida)
                .HasMaxLength(16);

            this.Property(t => t.CodModeloEtq)
                .HasMaxLength(15);

            this.Property(t => t.CodAnatelEtq)
                .HasMaxLength(15);

            this.Property(t => t.CodEanEtq)
                .HasMaxLength(20);

            this.Property(t => t.NomFactorClass)
                .HasMaxLength(30);

            this.Property(t => t.NomSubFactorClass)
                .HasMaxLength(30);

            this.Property(t => t.DesTouch)
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("viw_STI_NE");
            this.Property(t => t.coditem).HasColumnName("coditem");
            this.Property(t => t.desItem).HasColumnName("desItem");
            this.Property(t => t.CodUnid).HasColumnName("CodUnid");
            this.Property(t => t.CodFamDBF).HasColumnName("CodFamDBF");
            this.Property(t => t.CodEstrutura).HasColumnName("CodEstrutura");
            this.Property(t => t.CodSupFamilia).HasColumnName("CodSupFamilia");
            this.Property(t => t.CodFamilia).HasColumnName("CodFamilia");
            this.Property(t => t.CodSubFamilia).HasColumnName("CodSubFamilia");
            this.Property(t => t.CodPartNumber).HasColumnName("CodPartNumber");
            this.Property(t => t.Tipo_Familia).HasColumnName("Tipo_Familia");
            this.Property(t => t.DesResumida).HasColumnName("DesResumida");
            this.Property(t => t.CodModeloEtq).HasColumnName("CodModeloEtq");
            this.Property(t => t.CodAnatelEtq).HasColumnName("CodAnatelEtq");
            this.Property(t => t.CodEanEtq).HasColumnName("CodEanEtq");
            this.Property(t => t.NomFactorClass).HasColumnName("NomFactorClass");
            this.Property(t => t.NomSubFactorClass).HasColumnName("NomSubFactorClass");
            this.Property(t => t.NumScreenSize).HasColumnName("NumScreenSize");
            this.Property(t => t.DesTouch).HasColumnName("DesTouch");
        }
    }
}
