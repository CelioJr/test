﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade
{
	public class ResumoContingencia
	{

		[Display(Name = "Linha")]
		public int CodLinha { get; set; }
		public string DesLinha { get; set; }
		[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
		public DateTime Data { get; set; }
		[Display(Name = "Embalado")]
		public int QtdEmbalado { get; set; }
		[Display(Name = "SAP")]
		public int QtdSAP { get; set; }
		[Display(Name = "SISAP")]
		public int QtdSISAP { get; set; }
		[Display(Name = "Expedido")]
		public int QtdExpedido { get; set; }

	}
}
