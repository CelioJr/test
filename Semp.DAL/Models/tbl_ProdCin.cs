using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_ProdCin
    {
        public System.DateTime DatBase { get; set; }
        public string CodCinescopio { get; set; }
        public Nullable<decimal> QtdPlanejada { get; set; }
        public Nullable<decimal> QtdProduzida { get; set; }
        public System.Guid rowguid { get; set; }
    }
}
