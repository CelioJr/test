using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_TARGET_HOUR_DTO
    {
        public ALC_TARGET_HOUR_DTO()
        {
            this.ALC_ORDER = new List<ALC_ORDER_DTO>();
            this.ALC_PROD_PLAN = new List<ALC_PROD_PLAN_DTO>();
        }

        public int ID_TARGET { get; set; }
        public Nullable<int> VALUE { get; set; }
        public Nullable<System.DateTime> DT_END { get; set; }
        public Nullable<System.DateTime> DT_START { get; set; }
        public int COD_LINE { get; set; }
        public int COD_SKU { get; set; }
        public byte[] line { get; set; }
        public virtual ALC_HOUR_SHIFT_DTO ALC_HOUR_SHIFT { get; set; }
        public virtual ICollection<ALC_ORDER_DTO> ALC_ORDER { get; set; }
        public virtual ICollection<ALC_PROD_PLAN_DTO> ALC_PROD_PLAN { get; set; }
    }
}
