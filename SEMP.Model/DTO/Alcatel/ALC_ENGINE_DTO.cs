using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_ENGINE_DTO
    {
        public string SERIAL { get; set; }
        public Nullable<System.DateTime> DT_PASS { get; set; }
        public string ORDER_ENG { get; set; }
        public Nullable<System.DateTime> DT_CHECK { get; set; }
        public Nullable<int> COD_SERIAL { get; set; }
        public string OP_PA { get; set; }
        public string MAGAZINE { get; set; }
        public Nullable<System.DateTime> DT_MAGAZINE { get; set; }
        public string BENCH { get; set; }
        public string JIG_SN { get; set; }
        public Nullable<int> LINE { get; set; }
        public virtual ALC_ORDER_DTO ALC_ORDER { get; set; }
        public virtual ALC_MAGAZINE_DTO ALC_MAGAZINE { get; set; }
        public virtual ALC_ORDER_DTO ALC_ORDER1 { get; set; }
    }
}
