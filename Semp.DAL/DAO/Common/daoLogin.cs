﻿using System.Linq;
using SEMP.DAL.Models;
using SEMP.Model.DTO;

namespace SEMP.DAL.Common
{
	public class daoLogin
	{
        //static daoLogin()
        //{
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<tbl_RIFuncionario, RIFuncionarioDTO>();

        //    });
        //}

        public static RIFuncionarioDTO ObterFuncionarioByCPF(string CPF)
		{

			try
			{
				using (DTB_SIMContext dtbSim = new DTB_SIMContext())
				{
					tbl_RIFuncionario funcionario = dtbSim.tbl_RIFuncionario.FirstOrDefault(x => x.NumCPF == CPF && x.DatDemissao == null);

					var RIFuncionarioDTO = new RIFuncionarioDTO();

					AutoMapper.Mapper.Map(funcionario, RIFuncionarioDTO);

					return RIFuncionarioDTO;
				}
			}
			catch
			{
				return null;
			}

		}

		public static RIFuncionarioDTO ObterFuncionarioByLogin(string userName)
		{

			try
			{
				using (DTB_SIMContext dtbSim = new DTB_SIMContext())
				{
					var funcionario = dtbSim.tbl_RIFuncionario.FirstOrDefault(x => x.NomLogin == userName && x.DatDemissao == null);

					if (funcionario == null)
					{
						return null;
					}

					RIFuncionarioDTO RIFuncionarioDTO = new RIFuncionarioDTO();

					AutoMapper.Mapper.Map(funcionario, RIFuncionarioDTO);

					return RIFuncionarioDTO;
				}
			}
			catch
			{
				return null;

			}

		}

		public static UsuarioDTO ObterUsuarioByLogin(string userName)
		{

			try
			{
				using (DTB_SIMContext dtbSim = new DTB_SIMContext())
				{
					var usuario = (from u in dtbSim.tbl_Usuario
								   where u.NomUsuario == userName && u.FlgAtivo == "S"
								   select new UsuarioDTO()
								   {
									   CodFab = u.CodFab,
									   Cpf = u.Cpf,
									   Email = u.Email,
									   FlgAtivo = u.FlgAtivo,
									   FullName = u.FullName,
									   NomDepto = u.NomDepto,
									   NomDeptoAD = u.NomDeptoAD,
									   NomUsuario = u.NomUsuario
								   }).FirstOrDefault();

					return usuario;
				}
			}
			catch
			{
				return null;

			}

		}

		public static RIFuncionarioDTO ObterFuncionarioByDRT(string CodDRT)
		{

			try
			{
				using (DTB_SIMContext dtbSim = new DTB_SIMContext())
				{
					tbl_RIFuncionario funcionario = dtbSim.tbl_RIFuncionario.FirstOrDefault(x => x.CodDRT == CodDRT);

					RIFuncionarioDTO RIFuncionarioDTO = new RIFuncionarioDTO();

					AutoMapper.Mapper.Map(funcionario, RIFuncionarioDTO);

					return RIFuncionarioDTO;
				}
			}
			catch
			{
				return null;
			}

		}



		public static UsuarioMobileDTO ObterFuncionarioDRT(string drt)
		{ 
			UsuarioMobileDTO user = new UsuarioMobileDTO();

			RIFuncionarioDTO funcionario = daoLogin.ObterFuncionarioByDRT(drt);
			user.CodFab = funcionario.CodFab;
			user.Cpf = funcionario.NumCPF;
			//user.Email = "";
			user.FlgAtivo = funcionario.FlgAtivo;
			user.NomUsuario = funcionario.NomLogin;
			user.FullName = funcionario.NomFuncionario;
		   // user.NomDepto = "";
		   // user.NomDeptoAD = "";


			return user;
		}


	}
}
