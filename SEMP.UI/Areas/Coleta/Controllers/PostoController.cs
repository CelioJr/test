﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Diagnostics;
using RAST.BO.Embalagem;
using RAST.BO.Rastreabilidade;
using RAST.BO.Lote;
using SEMP.Model.VO;
using SEMP.Model.VO.Rastreabilidade;
using SEMP.Model;
using SEMP.Model.DTO;
using SEMP.BO;

namespace SEMP.Areas.Coleta.Controllers
{
	/// <summary>
	/// Classe que controla as Action da funcionalidade Posto
	/// </summary>
	public class PostoController : Controller
	{
		/// <summary>
		/// Método GetLinhas retorna as linhas de acordo com a fábrica informada.
		/// </summary>
		/// <param name="fabrica">Parâmetro que recebe a fabrica onde será feito o filtro das linhas.</param>
		/// <returns>Retorna uma lista das linhas em formato JSON.</returns>
		public JsonResult GetLinhas(string fabrica)
		{
			return Json(Geral.ObterLinhaPorFabrica(fabrica).Where(x => x.FlgAtivo.Equals("1")), JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetLinhasRecebimento(string fabrica)
		{
			return Json(Geral.ObterLinhasRecebimento(fabrica), JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Método que retorno todos os tipos de Postos
		/// </summary>
		/// <returns></returns>
		public JsonResult GetTipoPosto()
		{
			return Json(Geral.ObterTipoPostos(), JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Método GetPosto retorna os postos de acordo com a linha informada.
		/// </summary>
		/// <param name="codLinha">Parâmetro que recebe a linha onde será feito o filtro dos postos.</param>
		/// <returns>Retorna uma lista dos postos em formato JSON.</returns>
		public JsonResult GetPostos(int codLinha)
		{
			return Json(Geral.ObterPostoPorLinha(codLinha), JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Método GetValida, valira o numero ECB e o coódigo do DRT.
		/// </summary>
		/// <param name="numECB">Parâmetro numECB.</param>
		/// <param name="codDRT">Parâmetro codDRT.</param>
		/// <returns>Retorna os dados validados.</returns>
		public JsonResult GetValida(string numECB, string codDRT, string codModelo, string numAP = "")
		{
			var sessao = Session;//HttpContext.Current.Session;

			Mensagem retorno = new Mensagem();

			// Verifica se a sessão existe, caso contrário tem que pedir pra logar de novo
			if (sessao != null)
			{
				// recupera os dados do posto na sessão
				DadosPosto dados = (DadosPosto)sessao["DadosPosto"];

				// inicializa o objeto da Passagem
				Passagem posto = new Passagem(dados);

				var validarAP = "";

				if (!String.IsNullOrEmpty(numAP)){
					// validar se a AP permite a leitura
					var linha = Geral.ObterLinha(posto.posto.CodLinha);
					var dataRef = Convert.ToDateTime(DateTime.Now.ToShortDateString());
					validarAP = Geral.ValidarAP("62", numAP, DateTime.Now, linha);
				}

				if (!String.IsNullOrEmpty(validarAP))
				{
					retorno = Defeito.ObterMensagem(Constantes.RAST_AP_FINALIZADA);
				}
				else
				{
					// faz a validação da passagem da etiqueta
					retorno = posto.ValidaPassagem(numECB, codDRT, posto.posto.CodLinha, posto.posto.NumPosto, numAP);

					// Se estiver tudo OK com o número de série continua a validação, caso contrário retorna mensagem de erro pro cliente
					if (retorno.CodMensagem == Constantes.RAST_OK)
					{

						//var dadosposto = (DadosPosto)Session["DadosPosto"];

						if (dados.flgValidaOrigem == true)
						{

							retorno = Amarracao.ValidaLeituraLinhasAnteriores(dados, numECB, codDRT);

							if (retorno.CodMensagem == Constantes.RAST_LEITURA_REPETIDA)
							{
								//se der leitura repetida, quer dizer que a placa passou no ultimo posto obrigatório da linha anterior, logo, está apta a seguir o processo
								retorno = new Mensagem() { CodMensagem = "R0001", DesCor = "lime", DesMensagem = "LEITURA OK. LEIA PRÓXIMO." };
							}
							else
							{

								if (retorno.CodMensagem == Constantes.RAST_OK)
								{
									//nesta caso a placa não passou pelo último posto obrigatório anterior, por isso não poderia ser em frente no processo
									retorno = Defeito.ObterMensagem(Constantes.RAST_REGISTRONAOENCONTRADO);
									retorno.DesMensagem = "Placa não passou nos postos obrigatórios da linha anterior";
								}
								else
								{
									//nesta caso a placa possui algum erro de rastreabilidade
									retorno.DesMensagem = "Problema na PLACA: " + retorno.DesMensagem;
								}
							}
						}
					}
				}
			}
			else
			{
				retorno = Defeito.ObterMensagem(Constantes.CLIENT_ERROR);
			}

			return Json(retorno, JsonRequestBehavior.AllowGet);
		}

		public JsonResult RegistrarPassagemTablet(int CodLinha, int NumPosto, string numSerie, string codDRT)
		{

			Mensagem retorno = new Mensagem();
			return Json(retorno, JsonRequestBehavior.AllowGet);

		}

		public JsonResult RegistrarPassagem(string numECB, string codDRT)
		{
			var sessao = Session;

			Mensagem retorno = new Mensagem();

			try
			{
				if (sessao != null)
				{
					DadosPosto dados = (DadosPosto)sessao["DadosPosto"];

					Passagem posto = new Passagem(dados);

					//retorno = posto.GravarPassagem(numECB, codDRT);
					retorno = posto.GravaPassagemProcedure(numECB, codDRT, posto.posto.CodLinha, posto.posto.NumPosto, null, null);
				}
				else
				{
					retorno = Defeito.ObterMensagem(Constantes.CLIENT_ERROR);
				}
			}
			catch (Exception)
			{
				retorno = Defeito.ObterMensagem(Constantes.CLIENT_ERROR);
			}

			return Json(retorno, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterDesDefeito(string codDefeito)
		{
			var sessao = Session;//HttpContext.Current.Session;

			String retorno = "";

			if (sessao != null)
			{
				DadosPosto dados = (DadosPosto)sessao["DadosPosto"];

				if (Defeito.VerificaDefeitoAtivo(codDefeito))
				{

					retorno = Defeito.ObterDesDefeito(codDefeito);

					if (retorno != "")
						retorno = codDefeito + " - " + retorno;

				}
				else
				{
					var erro = Defeito.ObterMensagem(Constantes.RAST_DEFEITO_INEXISTENTE);
					retorno = erro.DesMensagem;
				}
			}
			else
			{
				retorno = Constantes.DB_ERRO;
			}

			return Json(retorno, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ValidaErrosTablet(int CodLinha, int NumPosto, string numSerie, string codDRT)
		{
			Mensagem retorno =  new Mensagem();
			Passagem passagem = new Passagem(CodLinha, NumPosto);

			retorno = passagem.ValidaPassagemTablet(numSerie, codDRT, CodLinha, NumPosto);

			if (retorno.CodMensagem == Constantes.RAST_OK || retorno.CodMensagem == "R0003")
			{
				retorno = passagem.GravarPassagem(numSerie, codDRT);
			}

			return Json(retorno, JsonRequestBehavior.AllowGet);


		}

		public JsonResult InserirDefeitoTablet(int CodLinha, int NumPosto, string codDefeito, string numSerie, string codDRT)
		{

			Mensagem retorno = new Mensagem();

			retorno = Defeito.GravarDefeito(CodLinha, NumPosto, numSerie, codDefeito, codDRT, "");

			return Json(retorno, JsonRequestBehavior.AllowGet);
		}

		public JsonResult InserirDefeito(string codDefeito, string numSerie, string codDRT)
		{
			var sessao = Session;//HttpContext.Current.Session;

			Mensagem retorno = new Mensagem();

			if (sessao != null)
			{
				DadosPosto dados = (DadosPosto)sessao["DadosPosto"];

				retorno = Defeito.GravarDefeito(dados.CodLinha, dados.NumPosto, numSerie, codDefeito, codDRT, "");
			}
			else
			{
				retorno = Defeito.ObterMensagem(Constantes.CLIENT_ERROR);
			}

			return Json(retorno, JsonRequestBehavior.AllowGet);
		}

		public JsonResult InserirDefeitoPosicao(string posicao, string codDefeito, string numSerie, string codDRT, string codFab, string numAP)
		{
			var sessao = Session;//HttpContext.Current.Session;

			Mensagem retorno = new Mensagem();

			if (sessao != null)
			{
				DadosPosto dados = (DadosPosto)sessao["DadosPosto"];

				retorno = Defeito.GravarDefeito(dados.CodLinha, dados.NumPosto, numSerie, codDefeito, codDRT, "");
				retorno = Defeito.GravarPosicao(numSerie, posicao);

				try
				{
					if (dados.Setor == "IAC")
					{
						Boolean passouNoPosto = false;
						
						passouNoPosto = Passagem.PassouNoPosto(numSerie, dados.CodLinha, dados.NumPosto);

						if (!passouNoPosto)
						{

							CBLinhaDTO linhaAtual = ((CBLinhaDTO)Session["Linha"]);
							var ap = Geral.ObterAP(codFab, numAP);

							var emb = new CBEmbaladaDTO()
							{
								NumECB = numSerie,
								CodFab = codFab,
								CodLinha = dados.CodLinha,
								CodModelo = (ap != null) ? ap.CodModelo : numSerie.Substring(0,6),
								CodOperador = codDRT,
								DatLeitura = DateTime.Now,
								DatReferencia = DateTime.Now.ToShortDateString(),
								NumAP = numAP,
								NumCC = "",
								NumLote = "",
								NumPosto = dados.NumPosto,
								Turno = 1,
								CodLinhaFec = "",
								Linha = linhaAtual
							};

							var passagem = new Passagem(dados);
							passagem.GravaPassagemProcedure(numSerie, codDRT, dados.CodLinha, dados.NumPosto, codFab, numAP);

							var retEmb = Embalagem.SU_GravaSTA(emb);
							if (retEmb.CodMensagem == Constantes.RAST_OK)
							{
								Embalagem.Inserir_Embalada(emb, true);
							}

						}
					}
				}
				catch
				{

				}

			}
			else
			{
				retorno = Defeito.ObterMensagem(Constantes.CLIENT_ERROR);
			}

			return Json(retorno, JsonRequestBehavior.AllowGet);
		}

		public JsonResult InserirPosicao(string NumSerie, string Posicao)
		{
			var sessao = Session;//HttpContext.Current.Session;

			Mensagem retorno = new Mensagem();

			if (sessao != null)
			{
				retorno = Defeito.GravarPosicao(NumSerie, Posicao);
			}

			return Json(retorno, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterDesModelo(string codItem)
		{
			var sessao = Session;//HttpContext.Current.Session;

			string retorno = "";

			if (sessao != null)
			{
				//DadosPosto dados = (DadosPosto)sessao["DadosPosto"];

				retorno = Geral.ObterDescricaoItem(codItem);
			}
			else
			{
				retorno = Constantes.DB_ERRO;
			}

			return Json(retorno, JsonRequestBehavior.AllowGet);
		}

		public JsonResult CancelarLeitura(string numECB, string codDRT)
		{
			var sessao = Session;//HttpContext.Current.Session;

			Mensagem retorno = new Mensagem();

			if (sessao != null)
			{
				DadosPosto dados = (DadosPosto)sessao["DadosPosto"];

				retorno = Geral.CancelarLeitura(numECB, dados.CodLinha, dados.NumPosto);
			}
			else
			{
				retorno = Defeito.ObterMensagem(Constantes.CLIENT_ERROR);
			}

			return Json(retorno, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Json: /Coleta/Posto/CancelarLeituraPosto
		/// </summary>
		/// <param name="numECB"></param>
		/// <param name="codLinha"></param>
		/// <param name="numPosto"></param>
		/// <returns></returns>

		public JsonResult CancelarLeituraPosto(string numECB, int codLinha, int numPosto)
		{

			Mensagem retorno = new Mensagem();

			retorno = Geral.CancelarLeitura(numECB, codLinha, numPosto);

			return Json(retorno, JsonRequestBehavior.AllowGet);

		}

		/// <summary>
		/// Valida se a amarração é valida (Ex.: Se o produto ou item já foi lido, se o item faz parte do produto, etc.).
		/// </summary>
		/// <param name="Fabrica"></param>
		/// <param name="NumAP"></param>
		/// <param name="Modelo"></param>
		/// <param name="Leitura"></param>
		/// <param name="CodDRT"></param>
		/// <param name="UltimoIndiceLido"></param>
		/// <returns></returns>
		public JsonResult ValidarLeituraAmarracao(string Fabrica, string NumAP, string Modelo, string Leitura, string CodDRT, int UltimoIndiceLido, bool padrao, int idxPerfil)
		{
			Stopwatch stopWatch = new Stopwatch();
			stopWatch.Start();
			string bar = Leitura.ToUpper().ToString();
			string message = "";
			bool error = false;
			bool reLeitura = false;
			int indice = idxPerfil;
			int codTipoAmarra = 0;
			string codItem = string.Empty;
			Mensagem retorno = null;
			string barcode = bar;
			List<CBAmarraDTO> listaItens = null;

			if (padrao == true)
				barcode = "12345";

			try
			{
				var dadosPosto = (DadosPosto)Session["DadosPosto"];

				Passagem passagem = new Passagem(dadosPosto);

				var perfils = (List<viw_CBPerfilAmarraDTO>)Session["GetPerfilPosto"];

				var perfilMasc = perfils[idxPerfil];

				//Se for produto valida passagem
				if (perfilMasc.CodTipoAmarra == Constantes.AMARRA_PRODUTO)
				{
					retorno = passagem.ValidaPassagem(bar, CodDRT, passagem.posto.CodLinha, passagem.posto.NumPosto, NumAP);

					message = retorno.DesMensagem;

					if (retorno.CodMensagem != "R0001")
					{
						error = true;
					}
					else if (passagem.posto.CodTipoPosto == Constantes.POSTO_AMARRA_NOTEBOOK ||
							 passagem.posto.CodTipoPosto == Constantes.POSTO_AMARRA_TABLET ||
							 passagem.posto.CodTipoPosto == Constantes.POSTO_PASSAGEM_TABLET)
					{
						if (!Amarracao.VerificaSeProdutoExisteInformatica(bar, NumAP))
						{
							error = true;
							message = "NÚMERO DE SÉRIE NÃO PERTENCE A AP: " + NumAP + ".";
							retorno.DesCor = "RED";
							retorno.DesMensagem = message;
						}

						if (!Amarracao.ValidarSeProdutoInformaticaPassouSeparacao(bar))
						{
							error = true;
							message = "PRODUTO NÃO PASSOU NA SEPARAÇÃO";
							retorno.DesCor = "RED";
							retorno.DesMensagem = message;
						}
					}

					if (!error)
					{
						listaItens = Amarracao.RetornaItensAmarrados(passagem.posto.CodLinha, passagem.posto.NumPosto, bar);

						if (listaItens.Count > 0)
						{
							reLeitura = true;
						}
					}
				}
				else if ((passagem.posto.CodTipoPosto == Constantes.POSTO_AMARRA_NOTEBOOK || passagem.posto.CodTipoPosto == Constantes.POSTO_AMARRA_TABLET) && perfilMasc.CodTipoAmarra == Constantes.AMARRA_PLACA_MAE_TABLET || perfilMasc.CodTipoAmarra == Constantes.AMARRA_PLACA_AUX_TABLET)
				{
					if (!Amarracao.VerificaSePlacaMaeExisteInformatica(bar))
					{
						retorno = new Mensagem();
						error = true;
						message = "ITEM NÃO FOI ENCONTRADO NA EMBALAGEM.";
						retorno.DesCor = "RED";
						retorno.DesMensagem = message;
					}
					else if (Amarracao.VerificaSeItemFoiAmarrado(bar))
					{
						message = "JÁ LIDO NESTA ETAPA";
						error = true;
						retorno = Defeito.ObterMensagem(Constantes.RAST_LEITURA_REPETIDA);
					}
					else if (Amarracao.VerificaSeItemTemDefeito(bar))
					{
						message = "DEFEITO EM ABERTO";
						error = true;
						retorno = Defeito.ObterMensagem(Constantes.RAST_NAOLIBERADO_DEFEITO);
					}
					else
					{
						message = "LEITURA OK. LEIA PRÓXIMO.";
						retorno = Defeito.ObterMensagem(Constantes.RAST_OK);
					}
				}
				else
				{
					var retornoValidacao = Amarracao.ValidarItem(bar);
					retorno = new Mensagem
					{
						CodMensagem = retornoValidacao.CodMensagem,
						DesMensagem = retornoValidacao.DesMensagem,
						DesCor = retornoValidacao.DesCor
					};

					message = retornoValidacao.DesMensagem;
					error = retornoValidacao.erro;


					// verifica se o posto é do tipo AMARRA PCI e se a placa é um item importado
					var itemImportado = false;
					/*if (perfilMasc.CodTipoAmarra == Constantes.AMARRA_PLACA_PCI_PRINCIPAL)
					{
						var item = Geral.ObterItem(perfilMasc.CodItem);
						if (item != null && item.Origem.StartsWith("6"))
						{
							itemImportado = true;
						}
					}*/

					if (retorno.CodMensagem != "R0001")
					{
						error = true;
					}
					else if ((perfilMasc.flgPlacaPainel != null && (perfilMasc.flgPlacaPainel == 1 || perfilMasc.flgPlacaPainel == 2)) && passagem.posto.flgValidaOrigem == true && itemImportado == false)
					{

						//deve ser verificado se a placa ou painel passou em todos os postos da linha anterior
						retorno = Amarracao.ValidaLeituraLinhasAnteriores(passagem.posto, bar, CodDRT);

						if (retorno.CodMensagem == Constantes.RAST_LEITURA_REPETIDA)
						{
							//se der leitura repetida, quer dizer que a placa passou no ultimo posto obrigatório da linha anterior, logo, está apta a seguir o processo
							error = false;
							retorno = new Mensagem() { CodMensagem = "R0001", DesCor = "lime", DesMensagem = "LEITURA OK. LEIA PRÓXIMO." };
						}
						else
						{
							error = true;

							if (retorno.CodMensagem == Constantes.RAST_OK)
							{
								//nesta caso a placa não passou pelo último posto obrigatório anterior, por isso não poderia ser em frente no processo
								retorno = Defeito.ObterMensagem(Constantes.RAST_REGISTRONAOENCONTRADO);
								retorno.DesMensagem = "Placa não passou nos postos obrigatórios da linha anterior";
								message = retorno.DesMensagem;
							}
							else
							{
								//nesta caso a placa possui algum erro de rastreabilidade
								message = "Problema na PLACA: " + retorno.DesMensagem;
							}
						}
					}
				}
				codTipoAmarra = perfilMasc.CodTipoAmarra;
				codItem = perfilMasc.CodItem;
			}
			catch
			{
				error = true;
				retorno = Defeito.ObterMensagem(Constantes.CLIENT_ERROR);
				message = retorno.DesMensagem;
			}

			stopWatch.Stop();

			TimeSpan ts = stopWatch.Elapsed;

			// Format and display the TimeSpan value.
			string tempoExecucaoMetodo = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
				ts.Hours, ts.Minutes, ts.Seconds,
				ts.Milliseconds / 10);

			var jsonData = new
			{
				error = error,
				message = message,
				serial = bar,
				indice = indice,
				tipo = codTipoAmarra,
				codItem = codItem,
				retorno = retorno,
				reLeitura = reLeitura,
				tempo = tempoExecucaoMetodo,
				itensAmarrados = listaItens
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Este método é responsável por inserir os dados referentes a amarração no banco de dados.
		/// </summary>
		/// <returns></returns>
		[HttpPost]
		public JsonResult InserirAmarracao()
		{
			Stopwatch stopWatch = new Stopwatch();
			stopWatch.Start();
			string fabrica = Request.Form["Fabrica"];
			string numAP = Request.Form["NumeroAP"];
			string modelo = Request.Form["Modelo"];
			string codDRT = Request.Form["CodDRT"];
			string numSerie = Request.Form["NumSerie"];
			bool error = false;
			string message = string.Empty;

			var listaAmarra = new List<CBAmarraDTO>();

			var sessao = Session;

			try
			{

				if (sessao != null)
				{
					Passagem passagem = new Passagem((DadosPosto)Session["DadosPosto"]);
					var perfils = (List<viw_CBPerfilAmarraDTO>)Session["GetPerfilPosto"];//RAST.BO.Rastreabilidade.Amarracao.GetPerfilPosto(passagem.posto.CodLinha, passagem.posto.NumPosto, modelo, fabrica, numAP);

					foreach (var item in perfils.Where(a => a.DesTipoAmarra != "Produto" && a.DesTipoAmarra != "EAN13").OrderBy(o => o.CodTipoAmarra).ToList())
					{
						//listaAmarra.Add(new CBAmarraDTO { 
						//    NumSerie = numSerie,
						//    NumECB = Request.Form[item.DesTipoAmarra.Replace("\"","")],
						//    CodModelo = modelo,
						//    FlgTipo = item.CodTipoAmarra,
						//    DatAmarra = DateTime.Now,
						//    CodLinha =  passagem.posto.CodLinha,
						//    NumPosto = passagem.posto.NumPosto,
						//    CodFab = fabrica,
						//    NumAP = numAP,                      
						//    CodItem = item.CodItem,
						//    DipositivoID = item.DipositivoID,
						//    IsDispositivoIntegrado = item.IsDispositivoIntegrado
						//});        
						var valor = Request.Form[item.DesTipoAmarra].Split(',');

						for (int x = 0; x < valor.Length; x++)
						{
							if (!listaAmarra.Exists(a => a.NumECB.Equals(valor[x])))
							{
								CBAmarraDTO amarra = new CBAmarraDTO()
								{
									NumSerie = numSerie,
									NumECB = valor[x],
									CodModelo = modelo,
									FlgTipo = item.CodTipoAmarra,
									DatAmarra = DateTime.Now,
									CodLinha = passagem.posto.CodLinha,
									NumPosto = passagem.posto.NumPosto,
									CodFab = fabrica,
									NumAP = numAP,
									CodItem = item.CodItem,
									DipositivoID = item.DipositivoID,
									IsDispositivoIntegrado = item.IsDispositivoIntegrado
								};

								listaAmarra.Add(amarra);
							}
						}
					}

					Mensagem retorno = null;

					if (listaAmarra.Count == 0)
					{
						retorno = passagem.GravarPassagem(numSerie, codDRT);
					}
					else
					{
						if (passagem.posto.CodTipoPosto == 20 || passagem.posto.CodTipoPosto == 23)
						{
							retorno = Amarracao.InserirAmarracaoInformatica(listaAmarra, codDRT);
						}
						else
						{
							retorno = Amarracao.InserirAmarracao(listaAmarra, codDRT, passagem.posto);
						}
					}

					if (retorno.CodMensagem != "R0001")
					{
						error = true;
						message = retorno.DesMensagem;
					}
					else
					{
						message = retorno.DesMensagem;
					}
				}
				else
				{
					error = true;
				}
			}
			catch (Exception ex)
			{
				error = true;
				message = "ERRO AO PROCESSAR : " + ex.Message;
			}

			stopWatch.Stop();
			TimeSpan ts = stopWatch.Elapsed;

			string tempoExecucaoMetodo = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
				ts.Hours, ts.Minutes, ts.Seconds,
				ts.Milliseconds / 10);

			var jsonData = new
			{
				error = error,
				message = message,
				tempo = tempoExecucaoMetodo
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterTotal(string ap, string codFab)
		{
			bool error = false;
			int total = 0;
			int totalAP = 0;
			int totalLoteAP = 0;
			int totalModelo = 0;

			var sessao = Session;
			try
			{
				if (sessao != null)
				{
					Passagem passagem = new Passagem((DadosPosto)Session["DadosPosto"]);

					if (!string.IsNullOrEmpty(ap))
					{
						var lsaap = Geral.ObterAP(codFab, ap);

						var codmodelo = lsaap.CodModelo;
						if (lsaap.CodProcesso.StartsWith("01"))
						{
							var modAux = Geral.ObterCodModeloIAC_IAC(codmodelo);
							if (modAux != null)
							{
								codmodelo = modAux;
							}

							codmodelo = Geral.ObterCodModeloIAC_IMC(codmodelo);
						}

						totalModelo = Geral.ObterTotalAprovadasModelo(passagem.posto.CodLinha, passagem.posto.NumPosto, codmodelo);
						totalLoteAP = lsaap.QtdLoteAP ?? 0;

						totalAP = Geral.ObterTotalAmarradoAP(passagem.posto.CodLinha, passagem.posto.NumPosto, codFab, ap);
					}

					total = Geral.ObterTotalAprovadas(passagem.posto.CodLinha, passagem.posto.NumPosto);



				}
				else
				{
					error = true;
				}
			}
			catch
			{
				error = true;
			}

			var jsonData = new
			{
				error = error,
				total = total,
				totalAP = totalAP,
				totalLoteAP = totalLoteAP,
				totalModelo = totalModelo
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ValidarSerialPostoTecnico(string Leitura, string CodDRT, string codDefeito, string alterarDefeito)
		{
			string serial = Leitura.ToUpper();
			bool error = false;
			bool exibeDefeitoAnterior = false;

			bool verificaSerial = Tecnico.VerificaSeExisteDefeitoAberto(serial);
			bool verificaDefeitoFechado = Tecnico.VerificaSeDefeitoFechado(serial);
			string message = string.Empty;
			string defeitoAnterior = string.Empty;
			string defeitoEncontrado = string.Empty;
			List<TecnicoPosicao> listaPosicao = new List<TecnicoPosicao>();
			Mensagem retorno = null;
			CBDefeitoDTO defeito = new CBDefeitoDTO();
			List<CBCadSubDefeitoDTO> subDefeitoDTO = new List<CBCadSubDefeitoDTO>();

			var sessao = Session;

			DadosPosto dados = (DadosPosto)sessao["DadosPosto"];
			Passagem posto = new Passagem(dados);

			if (verificaSerial == false && alterarDefeito == "false")
			{
				error = true;
				retorno = Defeito.ObterMensagem(Constantes.RAST_PLACANAOEXISTE);
				message = "NÃO HÁ DEFEITOS REGISTRADOS PARA ESTE PRODUTO. LEIA A PRÓXIMA PLACA.";
			}
			else if (alterarDefeito == "true" && verificaDefeitoFechado == false)
			{
				error = true;
				retorno = Defeito.ObterMensagem(Constantes.RAST_PLACANAOEXISTE);
				message = "NÃO É POSSIVEL ALTERAR UM DEFEITO EM ABERTO. LEIA A PRÓXIMA PLACA.";
			}
			else
			{
				listaPosicao = Tecnico.GetPosicao(Leitura, posto.posto.CodLinha);

				if (verificaDefeitoFechado && alterarDefeito == "true")
				{
					defeito = Tecnico.GetDefeito(serial, 1);
				}
				else
				{
					defeito = Tecnico.GetDefeito(serial);
				}

				if (!string.IsNullOrWhiteSpace(codDefeito) && codDefeito != defeito.CodDefeito && alterarDefeito == "false")
				{
					if (!string.IsNullOrWhiteSpace(Defeito.ObterDesDefeito(codDefeito)))
					{

						//Verifica se o defeito é igual ao defeito anterior.
						if (codDefeito.Trim() == defeito.CodDefeito.Trim())
						{
							error = true;
							retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
							message = "CÓDIGO DE DEFEITO NÃO PODE SER IGUAL.";
							throw new Exception(message);
						}
						else
						{
							var defeitoExistenteCodDefeito = Tecnico.GetDefeitoByCodDefeitoExistente(defeito.NumECB, codDefeito);

							if (defeitoExistenteCodDefeito.NumECB != null)
							{
								defeito.FlgRevisado = Constantes.DEF_CANCELADO;
								defeito.CodDRT = CodDRT;
								Tecnico.SalvarDefeito(defeito);

								defeitoExistenteCodDefeito.FlgRevisado = Constantes.DEF_ABERTO;
								//defeitoExistenteCodDefeito.CodDRT = CodDRT;
								Tecnico.SalvarDefeitoCodDefeitoExistente(defeitoExistenteCodDefeito, 2);
							}
							else
							{
								defeitoAnterior = defeito.CodDefeito + "-" + defeito.DescricaoDefeito;
								defeitoEncontrado = codDefeito + "-" + Defeito.ObterDesDefeito(codDefeito);
								exibeDefeitoAnterior = true;

								defeito.FlgRevisado = Constantes.DEF_CANCELADO;
								defeito.CodDRT = CodDRT;
								Tecnico.SalvarDefeito(defeito);
								retorno = Defeito.GravarDefeito(defeito.CodLinha, defeito.NumPosto, defeito.NumECB, defeito.DatEvento, codDefeito, CodDRT, defeito.NumSeriePai);
								defeito = Tecnico.GetDefeito(defeito.NumECB);
							}
						}
					}
					else
					{
						error = true;
						retorno = Defeito.ObterMensagem(Constantes.RAST_PLACANAOEXISTE);
						message = "CÓDIGO DE DEFEITO NÃO EXISTE.";
					}
				}
				subDefeitoDTO = Defeito.ObterSubDefeitos(defeito.CodDefeito);
			}

			var jsonData = new
			{
				error = error,
				message = message,
				defeito = defeito,
				retorno = retorno,
				alterarDefeito = alterarDefeito,
				listaPosicao = listaPosicao,
				defeitoEncontrado = defeitoEncontrado,
				defeitoAnterior = defeitoAnterior,
				exibeDefeitoAnterior = exibeDefeitoAnterior,
				subDefeito = subDefeitoDTO,
				subDefeitoSelecionado = defeito.CodSubDefeito
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		public JsonResult RetornaPosicoes(string Leitura, int acao)
		{
			var sessao = Session;
			DadosPosto dados = (DadosPosto)sessao["DadosPosto"];
			Passagem posto = new Passagem(dados);
			List<TecnicoPosicao> listaPosicao = null;

			if (acao == 13 || acao == 14)
			{
				listaPosicao = Tecnico.GetPosicaoPlaca(Leitura, posto.posto.CodLinha);
			}
			else
			{
				if (acao == 15)
				{
					listaPosicao = Tecnico.GetPosicaoPainel(Leitura);
				}
				else
				{
					listaPosicao = Tecnico.GetPosicao(Leitura, posto.posto.CodLinha);
				}
			}

			var jsonData = new
			{
				listaPosicao = listaPosicao
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult SalvarDefeitoPostoTecnico()
		{
			bool error = false;
			string message = string.Empty;
			Mensagem retorno = null;
			int totalDefeitosResolvidos = 0;
			Passagem passagem = null;

			try
			{
				passagem = new Passagem((DadosPosto)Session["DadosPosto"]);

				string numSerie = Request.Form["NumSerie"];
				string origem = Request.Form["origem"];
				string acao = Request.Form["acao"];
				string causa = Request.Form["causa"];
				string romaneio = Request.Form["romaneio"];
				string tipo = Request.Form["tipo"];
				string posicaoForm = Request.Form["posicao"];
				string observacao = Request.Form["obs"];
				string codDRTRevisor =  Request.Form["CodDRTRevisor"];
				string defeitoIncorreto = Request.Form["defeitoIncorreto"];
				string defeitoEncontrado = Request.Form["defeitoEncontrado"];
				string alterardefeito = Request.Form["hdAlterarDefeito"];
				string subDefeito = Request.Form["subDefeito"];

				string posicao = string.Empty;
				string codigoItem = string.Empty;

				if (!string.IsNullOrWhiteSpace(posicaoForm) && posicaoForm.IndexOf("|") != -1)
				{
					posicao = posicaoForm.Split('|')[0];
					codigoItem = posicaoForm.Split('|')[1];
				}
				else
				{
					if (!string.IsNullOrWhiteSpace(posicaoForm))
					{
						throw new Exception("POSIÇÃO INFORMADA NÃO EXISTE. INFORME UMA POSIÇÃO VÁLIDA.");
					}
					else
					{
						posicao = posicaoForm;
					}
				}

				if (!String.IsNullOrEmpty(defeitoEncontrado))
					ValidarSerialPostoTecnico(numSerie, codDRTRevisor, defeitoEncontrado, alterardefeito);

				CBDefeitoDTO defeito = null;

				if (alterardefeito == "true")
				{
					defeito = Tecnico.GetDefeito(numSerie.ToUpper(), 1);
				}
				else
				{
					defeito = Tecnico.GetDefeito(numSerie.ToUpper());
				}

				if (!string.IsNullOrWhiteSpace(posicao))// && posicao.IndexOf("|") != -1)
				{
					if (!Tecnico.GetPosicao(numSerie, passagem.posto.CodLinha).Any(a => a.Posicao.Contains(posicao.Split('|')[0])))
					{
						throw new Exception("POSIÇÃO INFORMADA NÃO EXISTE. INFORME UMA POSIÇÃO VÁLIDA.");
					}
				}

				if (!String.IsNullOrEmpty(acao) && Defeito.ObterSubDefeitos(defeito.CodDefeito).Count > 0 && String.IsNullOrEmpty(subDefeito))
				{
					throw new Exception("DEFEITO ESPECÍFICO NÃO INFORMADO");
				}

				defeito.CodCausa = causa;
				defeito.DesAcao = acao;
				defeito.CodOrigem = origem;
				defeito.FlgTipo = tipo;
				defeito.Posicao = posicao == null ? "" : posicao.ToUpper();
				defeito.CodItem = codigoItem;
				defeito.ObsDefeito = observacao == null ? "" : observacao.ToUpper();
				defeito.CodDRT = codDRTRevisor;
				if (!String.IsNullOrEmpty(subDefeito))
					defeito.CodSubDefeito = int.Parse(subDefeito);

				if (!string.IsNullOrWhiteSpace(romaneio))
				{
					defeito.Romaneio = int.Parse(romaneio);
				}

				retorno = Defeito.ObterMensagem(Constantes.RAST_OK);

				if (string.IsNullOrWhiteSpace(acao))
				{
					if (alterardefeito == "true")
					{
						error = true;
						retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
						message = "AÇÃO NÃO INFORMADA. INFORME UMA AÇÃO.";
					}
					else
					{
						Tecnico.SalvarDefeito(defeito);
						message = "EDIÇÃO REALIZADA COM SUCESSO.";
					}

				}
				else if (defeito.CodDefeito == Constantes.DEFEITO_PADRAO)
				{
					error = true;
					retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
					message = "A OCORRÊNCIA NÃO PODE SER SALVA COM DEFEITO PADRÃO (SF882).";
				}
				else if ((
					   acao == "13" || acao == "14" || acao == "15")
					&& (numSerie.Length != Constantes.TAM_PRODUTO && numSerie.Length != Constantes.TAM_PRODUTO_INFORMATICA))
				{
					error = true;
					retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
					message = "Desassociação incorreta: Somente Produtos podem ser desassociados.".ToUpper();
				}
				else if (acao == "12")
				{
					//defeito.CodCausa = "";
					//defeito.CodOrigem = "";
					//defeito.FlgTipo = "";
					//defeito.Posicao = "";
					defeito.FlgRevisado = Constantes.DEF_SCRAP;
					defeito.NumPostoRevisado = passagem.posto.NumPosto;
					defeito.CodLinhaRevisado = passagem.posto.CodLinha;
					Tecnico.SalvarDefeito(defeito);
					message = "Manutenção finalizada com sucesso. Leia a próxima placa.".ToUpper();
				}
				/* DESASSOCIAÇÃO DE PLACA OU PAINEL */
				#region desassociacao placa ou painel
				else if (acao == "13" || acao == "14" || acao == "15" || acao == "19")
				{
					if (Embalagem.VerificaSeNumeroSerieExisteNaEmbalagem(numSerie))
					{
						error = true;
						retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
						message = "Produto não pode ser desassociado pois já foi embalado.".ToUpper();
					}
					else
					{
						bool desassociarPlacaEPainel = acao.ToUpper().Equals(Constantes.ACAO_DESASSOCIAR_PLACA_E_PAINEL.ToString());
						bool desassociarPlaca = acao.ToUpper().Equals(Constantes.ACAO_DESASSOCIAR_PLACA.ToString());
						bool desassociarPainel = acao.ToUpper().Equals(Constantes.ACAO_DESASSOCIAR_PAINEL.ToString());
						bool desassociarItem = acao.ToUpper().Equals(Constantes.ACAO_DESASSOCIAR_ITEM.ToString());

						//string placa = Geral.GetPlaca(defeito.NumECB, codigoItem);
						//string painel = Geral.GetPainel(defeito.NumECB, codigoItem);
						var item = Geral.GetItem(defeito.NumECB, codigoItem);
						var amarra = Geral.GetItemAmarrado(item);

						string NumeroSeriePai = defeito.NumECB;
						int transferir = 0;

						/*if (string.IsNullOrWhiteSpace(placa) && (desassociarPlacaEPainel || desassociarPlaca))
						{
							error = true;
							retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
							message = "Nenhuma placa associada ao produto.".ToUpper(); 
						}
						else if (string.IsNullOrWhiteSpace(painel) && (desassociarPlacaEPainel || desassociarPainel))
						{
							error = true;
							retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
							message = "Nenhum painel associado ao produto.".ToUpper(); 
						}*/
						if (string.IsNullOrWhiteSpace(item))
						{
							error = true;
							retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
							message = "Item não associado ao produto.".ToUpper();
						}

						if (error == false)
						{
							//Tranfere o defeito para a placa
							/*if ((desassociarPlacaEPainel == true || desassociarPlaca == true))
							{
								defeito.NumSeriePai = NumeroSeriePai;
								defeito.NumECB = placa;								
								defeito.DesAcao = null;						
								transferir = Tecnico.TransferirDefeito(defeito);
							}

							//Transfere o defeito para o painel
							if ((desassociarPlacaEPainel == true || desassociarPainel == true))
							{
								defeito.NumSeriePai = NumeroSeriePai;
								defeito.NumECB = painel;								
								defeito.DesAcao = null;	
								transferir = Tecnico.TransferirDefeito(defeito);
							}*/

							defeito.NumSeriePai = NumeroSeriePai;
							defeito.NumECB = item;
							defeito.DesAcao = null;
							transferir = Tecnico.TransferirDefeito(defeito);

							//Transfere a passagem
							bool transPassagem = false;
							if (!desassociarItem)
							{
								transPassagem = Tecnico.TranferirPassagem(defeito.CodLinha, NumeroSeriePai);
							}
							else
							{
								transPassagem = Tecnico.TranferirPassagemItem(amarra.CodLinha ?? 0, amarra.NumPosto ?? 0, NumeroSeriePai);
							}

							if (!transPassagem)
							{
								error = true;
								retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
								message = "Erro ao tranferir Passagem.".ToUpper();
							}

							if (error == false)
							{
								//Transfere a amarração
								bool transAmarracao = false;
								if (!desassociarItem)
								{
									transAmarracao = Tecnico.TranferirAmarracao(defeito.CodLinha, NumeroSeriePai);
								}
								else
								{
									transAmarracao = Tecnico.TranferirAmarracaoItem(NumeroSeriePai, item);
								}

								if (!transAmarracao)
								{
									error = true;
									retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
									message = "Erro ao tranferir Amarração.".ToUpper();
								}

								if (error == false)
								{
									if (alterardefeito == "true")
									{
										defeito.FlgRevisado = Constantes.DEF_REVISADO;
										defeito.NumPostoRevisado = passagem.posto.NumPosto;
										defeito.CodLinhaRevisado = passagem.posto.CodLinha;

										AlterarDefeitoPostoTecnico(defeito, Request.Form["hdCodDRTSupervisor"], Request.Form["hdJustificativa"], defeitoEncontrado);
									}
									else
									{
										defeito = Tecnico.GetDefeito(NumeroSeriePai);

										//Atualiza a Situacao do Defeit para o FlgRevisado = 1, Fechado
										defeito.CodCausa = causa;
										defeito.DesAcao = acao;
										defeito.CodOrigem = origem;
										defeito.FlgTipo = tipo;
										defeito.Posicao = posicao.ToUpper();
										defeito.CodItem = codigoItem;
										defeito.ObsDefeito = observacao.ToUpper();
										defeito.FlgRevisado = Constantes.DEF_REVISADO;
										defeito.NumPostoRevisado = passagem.posto.NumPosto;
										defeito.CodLinhaRevisado = passagem.posto.CodLinha;
										//Correcao para salvar o CodSubDefeito.
										if (!string.IsNullOrWhiteSpace(subDefeito))
											defeito.CodSubDefeito = int.Parse(subDefeito);

										defeito.CodDRT = codDRTRevisor;

										Tecnico.SalvarDefeito(defeito);
									}
									message = "Manutenção finalizada com sucesso. Leia a próxima placa.".ToUpper();
								}
							}
						}

					}
				}
				#endregion
				else
				{
					if (alterardefeito == "true")
					{
						defeito.FlgRevisado = Constantes.DEF_REVISADO;
						defeito.NumPostoRevisado = passagem.posto.NumPosto;
						defeito.CodLinhaRevisado = passagem.posto.CodLinha;
						AlterarDefeitoPostoTecnico(defeito, Request.Form["hdCodDRTSupervisor"], Request.Form["hdJustificativa"], defeitoEncontrado);
					}
					else
					{
						defeito.FlgRevisado = Constantes.DEF_REVISADO;
						defeito.NumPostoRevisado = passagem.posto.NumPosto;
						defeito.CodLinhaRevisado = passagem.posto.CodLinha;
						Tecnico.SalvarDefeito(defeito);
					}
					message = "Manutenção finalizada com sucesso. Leia a próxima placa.".ToUpper();
				}
			}
			catch (Exception ex)
			{
				error = true;
				retorno = Defeito.ObterMensagem(Constantes.CLIENT_ERROR);
				message = ex.Message.ToUpper();
			}

			totalDefeitosResolvidos = Tecnico.GetTotalDefeitoResolvido(passagem.posto.CodLinha, passagem.posto.NumPosto, passagem.posto.Turno);

			var jsonData = new
			{
				error = error,
				message = message,
				retorno = retorno,
				totalDefeitoResolvidos = totalDefeitosResolvidos
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterItemAmarrado(string numSerie)
		{
			var tipo = 0;
			var descricao = "";
			var familia = "";

			try
			{

				var dado = Geral.GetItemAmarrado(numSerie);

				if (dado != null)
				{
					familia = Geral.ObterLinha(dado.CodLinha).Familia;

					Configuracao config = new Configuracao();

					var tipoamarra = config.ObterTipoAmarra(dado.FlgTipo ?? 0);
					if (tipoamarra != null) tipo = tipoamarra.FlgPlacaPainel ?? 0;

					descricao = Geral.ObterDescricaoItem(dado.CodItem);
				}

				var resultado = new { CodItem = dado.CodItem, DesItem = descricao, Familia=familia, NumSerie = dado.NumSerie, NumECB = dado.NumECB, tipo = tipo };

				return Json(resultado, JsonRequestBehavior.AllowGet);
			}
			catch (Exception)
			{
				var erro = new {error = true, message = "Item lido incorreto"};
				return Json(erro, JsonRequestBehavior.AllowGet);
			}
		}

		public JsonResult GetTotalDefeitosResolvidos()
		{
			Passagem passagem = null;
			int total = 0;
			try
			{
				passagem = new Passagem((DadosPosto)Session["DadosPosto"]);

				total = Tecnico.GetTotalDefeitoResolvido(passagem.posto.CodLinha, passagem.posto.NumPosto, passagem.posto.Turno);
			}
			catch { }

			var jsonData = new
			{
				totalDefeitoResolvidos = total
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ValidarInseriDefeitoPostoTecnico(string numECB, string codDRT)
		{
			Mensagem retorno = new Mensagem();
			string validarRetorno = Tecnico.ValidarInserirDefeito(numECB);

			if (string.IsNullOrWhiteSpace(validarRetorno))
			{
				retorno = Defeito.ObterMensagem(Constantes.RAST_OK);
			}
			else
			{
				retorno = Defeito.ObterMensagem(Constantes.CLIENT_ERROR);
				retorno.DesMensagem = validarRetorno.ToUpper();
			}

			return Json(retorno, JsonRequestBehavior.AllowGet);
		}

		public JsonResult RastrearPassagem(string NumSerie)
		{
			var lista = Geral.RastrearPassagem(NumSerie)
				.Select(a => new
				{
					Linha = a.CodLinha +" - "+ a.DescricaoLinha,
					Posto = a.NumPosto + " - " + a.DescricaoPosto,
					Data = a.DatEvento.ToString(),
					NomeOperador = a.NomeOperador
				}).ToList();

			return Json(lista, JsonRequestBehavior.AllowGet);
		}


		public JsonResult RastrearPassagemHist(string NumSerie)
		{
			var lista = Geral.RastrearPassagemHist(NumSerie)
				.Select(a => new
				{
					Linha = a.CodLinha + " - " + a.DescricaoLinha,
					Posto = a.NumPosto + " - " + a.DescricaoPosto,
					Data = a.DatEvento.ToString(),
					NomeOperador = a.NomeOperador
				}).ToList();

			return Json(lista, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Este método é responsável por retornar os dados referentes a amarração.
		/// </summary>
		/// <param name="NumSerie"></param>
		/// <returns></returns>
		public JsonResult RastrearAmarracao(string NumSerie)
		{
			var lista = Geral.RastrearAmarracao(NumSerie)
				.Select(a => new
				{
					NumSerie = a.NumSerie,
					Item = a.NumECB,
					Tipo = a.DesTipoAmarra,
					Linha = a.CodLinha + " - " + a.DescricaoLinha,
					Posto = a.NumPosto + " - " + a.DescricaoPosto,
					Data = a.DatAmarra.ToString()
				});

			return Json(lista, JsonRequestBehavior.AllowGet);
		}

		public JsonResult RastrearAmarracaoHist(string NumSerie)
		{
			var lista = Geral.RastrearAmarracaoHist(NumSerie)
				.Select(a => new
				{
					NumSerie = a.NumSerie,
					Item = a.NumECB,
					Tipo = a.DesTipoAmarra,
					Linha = a.CodLinha + " - " + a.DescricaoLinha,
					Posto = a.NumPosto + " - " + a.DescricaoPosto,
					Data = a.DatAmarra.ToString()
				});

			return Json(lista, JsonRequestBehavior.AllowGet);
		}

		public JsonResult RastrearDefeito(string NumSerie)
		{
			var lista = Geral.RastrearDefeito(NumSerie)
				.Select(a => new
				{
					Linha = a.CodLinha + " - "+a.DescricaoLinha,
					Posto = a.NumPosto + " - " + a.DescricaoPosto,
					DataDefeito = a.DatEvento.ToString(),
					CodDefeito = a.CodDefeito+"-"+Defeito.ObterDesDefeito(a.CodDefeito),
					Status = a.FlgRevisado == 0 ? "Defeito em aberto" : a.FlgRevisado == 1 ? "Revisado" : a.FlgRevisado == 2 ? "Defeito cancelado" : "Scrap",
					a.FlgRevisado,
					Acao = string.IsNullOrWhiteSpace(a.DesAcao) ? "" : a.DesAcao,
					DataReparo = a.DatRevisado.ToString(),
					OperadorTecnico = a.OperadorTecnico,
					OperadorApontouDefeito = a.OperadorApontouDefeito,
					Posicao = a.Posicao
				}).ToList();

			return Json(lista, JsonRequestBehavior.AllowGet);
		}

		public JsonResult RastrearEmbalagem(string NumSerie)
		{
			var lista = Geral.RastrearEmbalagem(NumSerie)
				.Select(a => new
				{
					Linha = a.CodLinha + " - " + a.DescricaoLinha,
					Posto = a.NumPosto + " - " + a.DescricaoPosto,
					Data = a.DatLeitura.ToString(),
					Lote = a.NumLote,
					Modelo = a.CodModelo,
					Fabrica = a.CodFab,
					Operador = a.Operador
				});

			return Json(lista, JsonRequestBehavior.AllowGet);
		}

		public JsonResult VerificaPostoDefeito(string CodDefeito)
		{
			Boolean verifica = false;

			try
			{
				var sessao = Session;//HttpContext.Current.Session;

				DadosPosto dados = (DadosPosto)sessao["DadosPosto"];

				Passagem posto = new Passagem(dados);

				verifica = Defeito.VerificaPostoDefeito(posto.posto.CodLinha, posto.posto.NumPosto, CodDefeito);
			}
			catch (Exception)
			{

			}

			return Json(verifica, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetLinhasPorSetor()
		{
			var sessao = Session;//HttpContext.Current.Session;			

			if (sessao != null)
			{
				DadosPosto dados = (DadosPosto)sessao["DadosPosto"];

				string fabrica = Geral.ObterFabricaPorLinha(dados.CodLinha);

				switch (fabrica)
				{
					case ("IAC"):
						fabrica = "IMC";
						break;

					case ("IMC"):
						fabrica = "FEC";
						break;

					case ("FEC"):
						return Json("SEM DESTINO", JsonRequestBehavior.AllowGet);
				}

				return Json(Geral.ObterLinhaPorFabrica(fabrica), JsonRequestBehavior.AllowGet);
			}

			return Json("SEM DESTINO", JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterModeloDisponivel()
		{
			var sessao = Session;//HttpContext.Current.Session;

			string fabrica = "";

			if (sessao != null)
			{
				DadosPosto dados = (DadosPosto)sessao["DadosPosto"];

				fabrica = Geral.ObterFabricaPorLinha(dados.CodLinha);

				switch (fabrica)
				{
					case ("IAC"):
						fabrica = "1";
						break;

					case ("IMC"):
						fabrica = "2";
						break;

					case ("FEC"):
						fabrica = "3";
						break;
				}
			}

			return Json(TransferenciaLote.ObterModeloDisponivel(fabrica), JsonRequestBehavior.AllowGet);
		}

		public JsonResult ValidarFabrica()
		{
			var sessao = Session;//HttpContext.Current.Session;

			string fabrica = "";

			if (sessao != null)
			{
				DadosPosto dados = (DadosPosto)sessao["DadosPosto"];

				fabrica = Geral.ObterFabricaPorLinha(dados.CodLinha);

				switch (fabrica)
				{
					case ("IAC"):
						fabrica = "1";
						break;

					case ("IMC"):
						fabrica = "2";
						break;

					case ("FEC"):
						fabrica = "3";
						break;
				}
			}

			return Json(fabrica, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ValidarCracharSupervisor(string cracha)
		{
			RIFuncionarioDTO funcionario = null;

			string cra = cracha.Substring(0, 11);

			funcionario = Login.ObterFuncionarioByCPF(cra);

			bool retorno = true;
			if (funcionario != null && funcionario.CodCargo != null)
			{
				var codcargo = int.Parse(funcionario.CodCargo);
				if (codcargo != 79 && codcargo != 21)
				{
					retorno = false;
				}
			}
			else
			{
				retorno = false;
			}

			var data = new
			{
				retorno = retorno,
				codDRT = funcionario.CodDRT
			};



			return Json(data, JsonRequestBehavior.AllowGet);
		}

		public bool AlterarDefeitoPostoTecnico(CBDefeitoDTO defeito, string supervisor, string justificativa, string defeitoEncontrado)
		{
			var defeitoAntigoAlterado = defeito;
			var defeitoAntigo = Tecnico.GetDefeito(defeito.NumECB, 1);

			Tecnico.ExcluirDefeito(defeitoAntigo);

			defeitoAntigo.FlgRevisado = Constantes.DEF_ALTERADO;
			defeitoAntigo.CodDRTRevisor = supervisor;
			defeitoAntigo.ObsDefeito = justificativa;

			Tecnico.AdicionarDefeito(defeitoAntigo);

			defeitoAntigoAlterado.FlgRevisado = Constantes.DEF_REVISADO;
			if (!string.IsNullOrWhiteSpace(defeitoEncontrado))
			{
				defeitoAntigoAlterado.CodDefeito = defeitoEncontrado;
			}

			Tecnico.AdicionarDefeito(defeitoAntigoAlterado, false);

			return true;
		}

		public JsonResult ObeterRealPrevisto()
		{
			Passagem passagem = new Passagem((DadosPosto)Session["DadosPosto"]);

			RealPrevisto realPrevisto = Passagem.ObterQuantidadeRealEPrevisto(passagem.posto.CodLinha);

			var data = new
			{
				Real = realPrevisto.QuantidadeReal,
				Previsto = realPrevisto.QuantidadePrevista
			};

			return Json(data, JsonRequestBehavior.AllowGet);
		}

		public JsonResult RecuperarMascaras()
		{
			var perfils = (List<viw_CBPerfilAmarraDTO>)Session["GetPerfilPosto"];

			var mascaras = perfils.Select(s => new { Tipo = s.DesTipoAmarra, Mascara = s.Mascara, Ap = s.numAP }).ToList();

			var jsonData = new
			{
				mascara = mascaras
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterAP(string codFab, string numAP)
		{
			var dadosAP = Geral.ObterAP(codFab, numAP);

			return Json(dadosAP, JsonRequestBehavior.AllowGet);

		}


		#region Recebimento/Transferência de Lote
		public JsonResult Transferir()
		{
			var sessao = Session;//HttpContext.Current.Session;

			Mensagem retorno = new Mensagem();

			if (sessao == null)
			{
				ViewBag.JS = "alert('Linha editada com sucesso.'); location.href ='/Login/'";
				return Json(null, JsonRequestBehavior.AllowGet);
			}

			DadosPosto dados = (DadosPosto)sessao["DadosPosto"];

			var transferindo = TransferenciaLote.ObterTransferenciaAberta(dados.CodLinha, dados.NumPosto);
			var disponivel = TransferenciaLote.ObterLoteDisponivel(TransferenciaLote.ObterModeloTransferencia(dados.CodLinha, dados.NumPosto));

			string numLoteMax = transferindo.Max(x => x.NumLote);
			string NumLoteMin = disponivel.Min(x => x.NumLote);

			if (numLoteMax != null && NumLoteMin != null)
			{
				if (int.Parse(NumLoteMin.Substring(13, 5)) < int.Parse(numLoteMax.Substring(13, 5)))
				{
					retorno = Defeito.ObterMensagem(SEMP.Model.Constantes.RAST_ERRO_FIFO);
					retorno.DesMensagem = retorno.DesMensagem + " - Falta ler: " + NumLoteMin;
					return Json(retorno, JsonRequestBehavior.AllowGet);
				}
			}


			retorno = TransferenciaLote.Trasnferir(dados.CodLinha, dados.NumPosto);

			return Json(retorno, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterNumeroLoteCC(string NumCC)
		{
			var sessao = Session;//HttpContext.Current.Session;

			if (sessao == null)
			{
				ViewBag.JS = "alert('Linha editada com sucesso.'); location.href ='/Login/'";
				return Json(null, JsonRequestBehavior.AllowGet);
			}

			DadosPosto dados = (DadosPosto)sessao["DadosPosto"];

			string NumLote = TransferenciaLote.ObterNumeroLoteCC(NumCC);

			return Json(NumLote, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterTotalTransferencia()
		{
			var sessao = Session;//HttpContext.Current.Session;

			int totalLote = 0;
			int totalCC = 0;

			if (sessao != null)
			{
				DadosPosto dados = (DadosPosto)sessao["DadosPosto"];

				totalLote = TransferenciaLote.ObterTotalLoteTransf(dados.CodLinha, dados.NumPosto);
				totalCC = TransferenciaLote.ObterTotalCCTransf(dados.CodLinha, dados.NumPosto);
			}

			var jsonData = new
			{
				totalLote = totalLote,
				totalCC = totalCC
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		public JsonResult DesfazTransferencia()
		{
			var sessao = Session;//HttpContext.Current.Session;

			if (sessao == null)
			{
				ViewBag.JS = "alert('Linha editada com sucesso.'); location.href ='/Login/'";
				return Json(null, JsonRequestBehavior.AllowGet);
			}

			DadosPosto dados = (DadosPosto)sessao["DadosPosto"];

			Boolean resultado = TransferenciaLote.DesfazTransferencia(dados.CodLinha, dados.NumPosto);

			return Json(resultado, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterLinhaDestino()
		{
			var sessao = Session;//HttpContext.Current.Session;						

			if (sessao == null)
			{
				ViewBag.JS = "alert('Linha editada com sucesso.'); location.href ='/Login/'";
				return Json(null, JsonRequestBehavior.AllowGet);
			}

			DadosPosto dados = (DadosPosto)sessao["DadosPosto"];

			int CodLinhaDestino = TransferenciaLote.ObterLinhaDestino(dados.CodLinha, dados.NumPosto);

			return Json(CodLinhaDestino, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ValidarLote(string NumSerie, string CodDRT, string CodLinhaDestino)
		{
			Boolean resultado = false;
			Mensagem retorno = new Mensagem();
			var sessao = Session;//HttpContext.Current.Session;	

			if (NumSerie.Substring(0, 1).Equals("C"))
			{
				NumSerie = TransferenciaLote.ObterNumeroLoteCC(NumSerie);
			}

			if (sessao == null)
			{
				retorno = Defeito.ObterMensagem(Constantes.ERRO_SESSAO);
				return Json(retorno, JsonRequestBehavior.AllowGet);
			}

			retorno = Defeito.ObterMensagem(Constantes.RAST_OK);

			if (NumSerie.Length == Constantes.TAM_LOTE)
				resultado = TransferenciaLote.VerificaLoteFechado(NumSerie);
			else
			{
				string NumLote = TransferenciaLote.ObterNumeroLote(NumSerie);
				resultado = TransferenciaLote.VerificaLoteFechado(NumLote);
			}

			if (resultado == false)
			{
				retorno = Defeito.ObterMensagem(Constantes.RAST_LOTE_NAOFINALIZADO);
				return Json(retorno, JsonRequestBehavior.AllowGet);
			}

			DadosPosto dados = (DadosPosto)sessao["DadosPosto"];

			retorno = TransferenciaLote.GravarTransferencia(NumSerie, CodDRT, dados.CodLinha, dados.NumPosto, int.Parse(CodLinhaDestino));

			return Json(retorno, JsonRequestBehavior.AllowGet);
		}

		public JsonResult AtualizarValoresRecebimento(int codLinha)
		{
			/*int totalRecebido = RecebimentoLote.ObterTotalRecebido(codLinha);
			int totalRecebidoCC = RecebimentoLote.ObterTotalRecebidoCC(codLinha);

			String ultimoLote = RecebimentoLote.ObterUltimoLoteRecebido(codLinha);
			int totalPlacas = RecebimentoLote.ObterTotalLote(ultimoLote);

			String ultimoCC = RecebimentoLote.ObterUltimoLoteRecebidoCC(codLinha);
			int totalPlacasCC = RecebimentoLote.ObterTotalCC(ultimoCC);

			string lotePai = RecebimentoLote.ObterLotePaiCC(ultimoCC);
			var qtdRecebidoLote = RecebimentoLote.GetRecebidoLote(lotePai);
			var totalCCLote = RecebimentoLote.GetTotalCCLote(lotePai);*/

			var dados = RecebimentoLote.ObterDadosRecebimento(codLinha).FirstOrDefault();

			var jsonData = new
			{
				totalRecebido = dados.QtdLoteDia,
				totalRecebidoCC = dados.QtdCCDia,
				ultimoLote = dados.NumUltLote,
				totalPlacas = dados.QtdUltLote,
				ultimoCC = dados.NumUltCC,
				totalPlacasCC = dados.QtdUltCC,
				lotePai = dados.NumLotePaiCC,
				totalCCLoteRecebidos = dados.QtdRecPaiCC,
				totalCCLote = dados.QtdTotalPaiCC
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ValidarCaixaColetiva(string codigoBar, string codDRT)
		{
			bool erro = false;
			string msg = "LEITURA OK. LEIA O PRÓXIMO.";
			string cor = "green";

			var caixa = RecebimentoLote.GetCaixaColetiva(codigoBar);

			if (caixa.NumLote == null)
			{
				msg = "NÚMERO DE SÉRIE INVÁLIDO.";
				erro = true;
				cor = "red";
			}
			else
			{
				if (caixa.NumLotePai == null)
				{
					erro = true;
					cor = "red";
					msg = "LEIA UM NÚMERO DE SÉRIE DA CAIXA COLETIVA.";
				}
				else if (caixa.DatRecebimento != null)
				{
					erro = true;
					cor = "yellow";
					msg = "JÁ LIDO NESTA ETAPA.";
				}
				else if (caixa.DatFechamento == null)
				{
					erro = true;
					cor = "red";
					msg = "ESSA CAIXA COLETIVA PERTENCE A UM LOTE EM ABERTO.";
				}
				else
				{
					var retorno = RecebimentoLote.ValidarFIFOLote(caixa);
					if (retorno.Error == true)
					{
						erro = true;
						msg = "O LOTE ESTÁ FORA DA ORDEM DO FIFO. LEIA UM MAGAZINE DO LOTE : " + retorno.PrimeiroLoteASair.NumLote;
						cor = "red";
					}
					else
					{
						Passagem passagem = new Passagem((DadosPosto)Session["DadosPosto"]);
						var gravar = RecebimentoLote.GravaPassagemRecebimentoLote(caixa, passagem.posto, codDRT);

						if (!gravar)
						{
							erro = true;
							cor = "red";
							msg = "ERRO AO GRAVAR NA PASSAGEM.";
						}
					}
				}
			}

			var jsonData = new
			{
				erro = erro,
				msg = msg,
				cor = cor
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetTotalRecebidoLote()
		{
			Passagem passagem = new Passagem((DadosPosto)Session["DadosPosto"]);
			var totalRecebido = RecebimentoLote.GetTotalRecebido(passagem.posto);

			var jsonData = new
			{
				total = totalRecebido
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetUltimosRegistroLidosLote()
		{
			Passagem passagem = new Passagem((DadosPosto)Session["DadosPosto"]);

			var lista = RecebimentoLote.GetUltimosLido(passagem.posto);

			return Json(lista, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ReceberLote(string NumLote, string CodDRT, int CodLinhaDest)
		{
			var sessao = Session;//HttpContext.Current.Session;

			if (sessao == null)
			{
				ViewBag.JS = "alert('Sessão expirada: Faça Login novamente.'); location.href ='/Login/'";
				return Json(null, JsonRequestBehavior.AllowGet);
			}

			DadosPosto dados = (DadosPosto)sessao["DadosPosto"];

			Mensagem retorno = new Mensagem();

			var lote = RecebimentoLote.GetCaixaColetiva(NumLote);

			// verifica se foi lido um magazine ou uma placa
			if (lote == null || lote.NumLote == null)
			{
				retorno = Defeito.ObterMensagem(Constantes.RAST_REGISTRO_NAO_ENCONTRADO);
				return Json(retorno, JsonRequestBehavior.AllowGet);

			}

			//var postoReceb = Geral.ObterPostoPorLinha(CodLinhaDest);
			var codLinha = dados.CodLinha;
			var numPosto = dados.NumPosto;

			retorno = RecebimentoLote.GravarRecebimentoLote(lote.NumLote, codLinha, numPosto, CodDRT, CodLinhaDest);

			return Json(retorno, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ReceberLoteAlt(string NumLote, string CodDRT, int CodLinhaDest)
		{
			var retorno = "";

			try
			{
				var lote = RecebimentoLote.GetCaixaColetiva(NumLote);

				// verifica se foi lido um magazine ou uma placa
				if (lote == null || lote.NumLote == null)
				{
					retorno = Defeito.ObterMensagem(Constantes.RAST_REGISTRO_NAO_ENCONTRADO).DesMensagem;
					return Json(retorno, JsonRequestBehavior.AllowGet);
				}

				//var postoReceb = Geral.ObterPostoPorLinha(CodLinhaDest);

				var recebimento = RecebimentoLote.GravarRecebimentoLote(lote.NumLote, CodLinhaDest, 1, CodDRT, CodLinhaDest);

				retorno = recebimento != null ? recebimento.DesMensagem : "Erro: Lote não foi recebido. Informe ao DTI";
			}
			catch (Exception ex)
			{
				retorno = "Erro: " + ex.Message;
				throw;
			}

			return Json(retorno, JsonRequestBehavior.AllowGet);
		}

		#endregion


		#region Novas rotinas de lote
		public JsonResult ObterDadosLote()
		{
			var sessao = Session;
			DadosPosto dados = (DadosPosto)sessao["DadosPosto"];

			var ultimoLote = Lote.UltimoLote(dados.CodLinha, dados.NumPosto);
			var ultimoLoteCC = Lote.UltimoLoteCC(dados.CodLinha, dados.NumPosto);

			var resultado = new {ultimoLote, ultimoLoteCC};

			return Json(resultado, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterDadosCC(string numcc)
		{
			var cc = Lote.ObterLote(numcc);
			var lote = Lote.ObterLote(cc.NumLotePai);

			var resultado = new { cc, lote};

			return Json(resultado, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterDadosCCSerie(string numserie)
		{
			var cc = Lote.ObterLoteCCSerie(numserie);
			var lote = Lote.ObterLote(cc.NumLotePai);
			var qtdAprovados = Lote.QtdAprovadoLoteCC(cc.NumLote);

			var resultado = new { cc, lote, qtdAprovados };

			return Json(resultado, JsonRequestBehavior.AllowGet);
		}

		public JsonResult RegistrarPassagemLote(string numECB, string codDRT, string numLote, string numCC)
		{
			var sessao = Session;

			Mensagem retorno = new Mensagem();

			try
			{
				if (sessao != null)
				{
					DadosPosto dados = (DadosPosto)sessao["DadosPosto"];

					retorno = Lote.GravaPassagemProcedureLote(numECB, codDRT, dados.CodLinha, dados.NumPosto, null, null, numLote, numCC);
				}
				else
				{
					retorno = Defeito.ObterMensagem(Constantes.CLIENT_ERROR);
				}
			}
			catch (Exception)
			{
				retorno = Defeito.ObterMensagem(Constantes.CLIENT_ERROR);
			}

			return Json(retorno, JsonRequestBehavior.AllowGet);
		}

		public JsonResult RegistrarAprovadoItemLote(string numECB, string codDRT, string numCC)
		{
			var sessao = Session;

			Mensagem retorno = new Mensagem();

			try
			{
				if (sessao != null)
				{
					DadosPosto dados = (DadosPosto)sessao["DadosPosto"];
					Passagem pposto = new Passagem(dados);

					var posto = (CBPostoDTO)Session["Posto"];
					var linha = (CBLinhaDTO)Session["Linha"];

					// grava a passagem normal no sisap
					retorno = pposto.GravaPassagemProcedure(numECB, codDRT, pposto.posto.CodLinha, pposto.posto.NumPosto, null, null);

					if (retorno.CodMensagem == Constantes.RAST_OK || retorno.CodMensagem == Constantes.DB_OK)
					{
						//registra a aprovação do item
						Lote.AprovaLeituraItemLote(numECB, numCC);
					}

					// verifica se já completaram as amostras
					if (Lote.LoteFechadoAmostras(numCC))
					{
						var gravar = Lote.AprovarLoteOBA(numCC, codDRT, posto.CodLinha, posto.NumPosto);
						retorno = Defeito.ObterMensagem(Constantes.RAST_LOTE_FECHADO);

						Lote.ImprimirCaixaColetiva(numCC, linha, posto);
					}
					else
					{
						retorno = Defeito.ObterMensagem(Constantes.RAST_OK);
					}

				}
				else
				{
					retorno = Defeito.ObterMensagem(Constantes.CLIENT_ERROR);
				}
			}
			catch (Exception)
			{
				retorno = Defeito.ObterMensagem(Constantes.CLIENT_ERROR);
			}

			return Json(retorno, JsonRequestBehavior.AllowGet);
		}

		public JsonResult HistoricoPassagemLote(string sidx, string sord, int page, int rows)
		{
			Passagem passagem = new Passagem((DadosPosto)Session["DadosPosto"]);
			var ultimoLote = Lote.UltimoLote(passagem.posto.CodLinha, passagem.posto.NumPosto);

			var context = Lote.ObterPassagemPostoLote(passagem.posto.CodLinha, passagem.posto.NumPosto);

			int pageIndex = Convert.ToInt32(page) - 1;
			int pageSize = rows;
			int totalRecords = context.Count();
			int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

			var authors = context.OrderBy(a => sord).Skip(pageIndex * pageSize).Take(pageSize).ToList();

			var jsonData = new
			{
				total = totalPages,
				page,
				records = totalRecords,
				rows = (from n in authors
						select new
						{
							i = n.NumECB,
							cell = new string[] {
								"" + n.NumECB,
								n.DatInclusao.Value.ToString("HH:mm:ss"),
								Geral.ObterDescricaoItem(n.NumECB.Substring(0, 6).ToString())
							}
						}).ToArray()
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		public JsonResult CarregarItensLote(string sidx, string sord, int page, int rows, string numLote)
		{
			var context = Lote.ObterPassagemPostoLote(numLote);

			int pageIndex = Convert.ToInt32(page) - 1;
			int pageSize = rows;
			int totalRecords = context.Count();
			int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

			var authors = context.OrderBy(a => sord).Skip(pageIndex * pageSize).Take(pageSize).ToList();

			var jsonData = new
			{
				total = totalPages,
				page,
				records = totalRecords,
				rows = (from n in authors
						select new
						{
							i = n.NumECB,
							cell = new string[] {
								"" + n.NumECB,
								Geral.ObterDescricaoItem(n.NumECB.Substring(0, 6).ToString()),
								n.DatInclusao.Value.ToString("HH:mm:ss"),
								(n.DatInspecao != null) ? n.DatInspecao.Value.ToString("HH:mm:ss") : ""
							}
						}).ToArray()
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		public JsonResult CarregarItensLoteSerie(string sidx, string sord, int page, int rows, string numECB)
		{
			var context = Lote.ObterItensLoteSerie(numECB);

			int pageIndex = Convert.ToInt32(page) - 1;
			int pageSize = rows;
			int totalRecords = context.Count();
			int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

			var authors = context.OrderBy(a => sord).Skip(pageIndex * pageSize).Take(pageSize).ToList();

			var jsonData = new
			{
				total = totalPages,
				page,
				records = totalRecords,
				rows = (from n in authors
						select new
						{
							i = n.NumECB,
							cell = new string[] {
								"" + n.NumECB,
								Geral.ObterDescricaoItem(n.NumECB.Substring(0, 6).ToString()),
								n.DatInclusao.Value.ToString("HH:mm:ss"),
								(n.DatInspecao != null) ? n.DatInspecao.Value.ToString("HH:mm:ss") : ""
							}
						}).ToArray()
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);
		}

		public JsonResult FecharLote(string numLote, string codDRT, string justificativa)
		{

			var resultado = Lote.FecharLote(numLote, codDRT, justificativa);

			var retorno = Defeito.ObterMensagem(Constantes.RAST_OK);

			var jsonData = new
			{
				error = resultado,
				message = retorno.DesMensagem,
				retorno
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);

		}

		public JsonResult AlterarTamanhoCaixaColetiva(int tamanho)
		{
			bool erro;
			var messageErro = "";

			try
			{
				Passagem passagem = new Passagem((DadosPosto)Session["DadosPosto"]);

				//var embalagem = Embalagem.Recuperar_Posto_Embalagem((CBPostoDTO)Session["Posto"]);

				//se voltar true, significa que deu tudo certo, então a função retorna false, pois não deu erro.
				erro = !Lote.AlterarTamanhoCaixaColetiva(passagem.posto.CodLinha, tamanho);
			}

			catch (Exception)
			{
				erro = true;
				messageErro = "Erro ao alterar tamanho do lote";
			}

			var jsonData = new
			{
				error = erro,
				valor = tamanho,
				message = messageErro
			};

			return Json(jsonData, JsonRequestBehavior.AllowGet);

		}

		public JsonResult GetValidaItemLote(string numECB, string numcc, string codDRT)
		{
			var sessao = Session;//HttpContext.Current.Session;

			Mensagem retorno = new Mensagem();

			if (sessao != null)
			{
				DadosPosto dados = (DadosPosto)sessao["DadosPosto"];

				Passagem posto = new Passagem(dados);

				retorno = Lote.ValidaPassagemItemLote(numECB, numcc, posto.posto.CodLinha, posto.posto.NumPosto, codDRT);

			}
			else
			{
				retorno = Defeito.ObterMensagem(Constantes.CLIENT_ERROR);
			}

			return Json(retorno, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ReprovarItemOBA(string codDefeito, string numSerie, string codDRT, string numLote)
		{
			var sessao = Session;//HttpContext.Current.Session;

			Mensagem retorno = new Mensagem();

			if (sessao != null)
			{
				DadosPosto dados = (DadosPosto)sessao["DadosPosto"];

				retorno = Lote.ReprovarItemOBA(codDefeito, numSerie, codDRT, dados.CodLinha, dados.NumPosto, numLote);
			}
			else
			{
				retorno = Defeito.ObterMensagem(Constantes.CLIENT_ERROR);
			}

			return Json(retorno, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ReimprimirEtiquetaCC(string numCC)
		{

			var posto = (CBPostoDTO)Session["Posto"];
			var linha = (CBLinhaDTO)Session["Linha"];

			var impressao = Lote.ImprimirCaixaColetiva(numCC, linha, posto);

			return Json(impressao, JsonRequestBehavior.AllowGet);

		}

		public JsonResult LiberarReimpressaoLote(string leitura)
		{

			var posto = (CBPostoDTO)Session["Posto"];

			var existe = Lote.ObterLoteCCSerie(leitura, posto.CodLinha);

			if (existe.NumLote != null) {
				leitura = existe.NumLote;
			}

			var impressao = Lote.LiberarReimpressaoLote(leitura);

			return Json(impressao, JsonRequestBehavior.AllowGet);

		}

		#endregion

	}
}
