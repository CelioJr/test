﻿namespace SEMP.Testes
{
	partial class frmPrincipal
	{
		/// <summary>
		/// Variável de designer necessária.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Limpar os recursos que estão sendo usados.
		/// </summary>
		/// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Código gerado pelo Windows Form Designer

		/// <summary>
		/// Método necessário para suporte ao Designer - não modifique 
		/// o conteúdo deste método com o editor de código.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnRFCReadTable = new System.Windows.Forms.Button();
			this.btnTestesGerais = new System.Windows.Forms.Button();
			this.btnApontamentoMOB = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnRFCReadTable
			// 
			this.btnRFCReadTable.Location = new System.Drawing.Point(12, 12);
			this.btnRFCReadTable.Name = "btnRFCReadTable";
			this.btnRFCReadTable.Size = new System.Drawing.Size(132, 23);
			this.btnRFCReadTable.TabIndex = 0;
			this.btnRFCReadTable.Text = "Consultar Tabela SAP";
			this.btnRFCReadTable.UseVisualStyleBackColor = true;
			this.btnRFCReadTable.Click += new System.EventHandler(this.btnRFCReadTable_Click);
			// 
			// btnTestesGerais
			// 
			this.btnTestesGerais.Location = new System.Drawing.Point(150, 12);
			this.btnTestesGerais.Name = "btnTestesGerais";
			this.btnTestesGerais.Size = new System.Drawing.Size(92, 23);
			this.btnTestesGerais.TabIndex = 1;
			this.btnTestesGerais.Text = "Testes Gerais";
			this.btnTestesGerais.UseVisualStyleBackColor = true;
			this.btnTestesGerais.Click += new System.EventHandler(this.btnTestesGerais_Click);
			// 
			// btnApontamentoMOB
			// 
			this.btnApontamentoMOB.Location = new System.Drawing.Point(248, 12);
			this.btnApontamentoMOB.Name = "btnApontamentoMOB";
			this.btnApontamentoMOB.Size = new System.Drawing.Size(138, 23);
			this.btnApontamentoMOB.TabIndex = 2;
			this.btnApontamentoMOB.Text = "Apontamento MOB";
			this.btnApontamentoMOB.UseVisualStyleBackColor = true;
			this.btnApontamentoMOB.Click += new System.EventHandler(this.btnApontamentoMOB_Click);
			// 
			// frmPrincipal
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(740, 47);
			this.Controls.Add(this.btnApontamentoMOB);
			this.Controls.Add(this.btnTestesGerais);
			this.Controls.Add(this.btnRFCReadTable);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "frmPrincipal";
			this.Text = "Sistema para testes de classes";
			this.TopMost = true;
			this.Load += new System.EventHandler(this.frmPrincipal_Load);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnRFCReadTable;
		private System.Windows.Forms.Button btnTestesGerais;
		private System.Windows.Forms.Button btnApontamentoMOB;
	}
}

