﻿using System;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace SEMP.Models
{
	internal static class Log
	{
		public static Logger AppLog()
		{
			if (LogManager.Configuration != null)
			{
				var config = new LoggingConfiguration();
				DateTime now = DateTime.Now;

				var target =
				new FileTarget
				{
					FileName = @"C:\Log\server_" + now.Year + "_" + now.Month + "_" + now.Day + ".log"
				};

				config.AddTarget("logfile", target);

				//var ruleDebug = new LoggingRule("*", target);
				var ruleError = new LoggingRule("*", LogLevel.Error, target);
				//var ruleWarn = new LoggingRule("*", LogLevel.Warn, target);

				/*if (!config.LoggingRules.Contains(ruleDebug))
				{
					config.LoggingRules.Add(ruleDebug);
				}*/
				if (!config.LoggingRules.Contains(ruleError))
				{
					config.LoggingRules.Add(ruleError);
				}/*
                if (!config.LoggingRules.Contains(ruleWarn))
                {
                    config.LoggingRules.Add(ruleWarn);
                } */

				LogManager.Configuration = config;
			}

			return LogManager.GetCurrentClassLogger();
		}
	}

}