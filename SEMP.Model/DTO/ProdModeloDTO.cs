using System;

namespace SEMP.Model.DTO
{
    public class ProdModeloDTO
    {
        public string CodModelo { get; set; }
        public string DesResumida { get; set; }
        public string Projeto { get; set; }
        public Nullable<int> NumProjeto { get; set; }
        public string NumMacAddress { get; set; }
        public string CodAcessorio { get; set; }
        public string CodEAN { get; set; }
        public string CodEAN13a { get; set; }
        public string CodEAN13b { get; set; }
        public string CodEAN128 { get; set; }
        public Nullable<int> ImpModelo { get; set; }
        public string PotModelo { get; set; }
        public string RefCinescopio { get; set; }
        public string DesCP1 { get; set; }
        public string DesCor { get; set; }
        public Nullable<double> EmbModelo { get; set; }
        public string flgAtivo { get; set; }
        public string NumIMEI { get; set; }
        public string NumAnatel { get; set; }
        public int LotePadrao { get; set; }
        public Nullable<int> NumIMEIDOA { get; set; }
        public string VersaoSoft { get; set; }
        public string FlgLeitura { get; set; }
        public string Mascara { get; set; }
    }
}
