﻿using System;
using System.Collections.Generic;
using System.Linq;
using SEMP.DAL.Models;
using SEMP.Model.DTO;

namespace SEMP.DAL.DAO.Rastreabilidade
{
    public class daoResumoMF
    {
        //static daoResumoMF()
        //{
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<tbl_CBLinha, CBLinhaDTO>();
        //        cfg.CreateMap<tbl_CapacLinha, CapacLinhaDTO>();
        //        cfg.CreateMap<tbl_ProdDia, ProdDiaDTO>();
        //        cfg.CreateMap<CBLinhaObservacaoDTO, tbl_CBLinhaObservacao>();
        //    });
        //}

        public static List<CBLinhaDTO> LinhasMF()
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var linhas = (from l in dtbCb.tbl_CBLinha
                              where l.Setor == "FEC" && l.CodLinhaT1 != null
                              select l).OrderBy(x => new { x.Familia, x.DesLinha }).ToList();

                List<CBLinhaDTO> CBLinhaDTO = new List<CBLinhaDTO>();

                AutoMapper.Mapper.Map(linhas, CBLinhaDTO);

                return CBLinhaDTO;
            }
        }

        public static List<CBLinhaDTO> LinhasMF(DateTime DatProducao)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var linhas = (from l in dtbCb.tbl_CBLinha
                              join o in dtbCb.tbl_CBLinhaObservacao on new { l.CodLinha, DatProducao = DatProducao } equals new { o.CodLinha, o.DatProducao } into o2
                              from leftObs in o2.DefaultIfEmpty()
                              where l.Setor == "FEC" && l.CodLinhaT1 != null
                              select new CBLinhaDTO()
                              {
                                  CodLinha = l.CodLinha,
                                  CodFam = l.CodFam,
                                  CodLinhaT1 = l.CodLinhaT1,
                                  CodModelo = l.CodModelo,
                                  CodPlaca = l.CodPlaca,
                                  DesLinha = l.DesLinha,
                                  DesObs = l.DesObs,
                                  Observacao = leftObs.Observacao,
                                  Familia = l.Familia,
                                  Setor = l.Setor,
                                  FlgAtivo = l.flgAtivo
                              }).OrderBy(x => new { x.Familia, x.DesLinha }).ToList();

                return linhas;
            }
        }

        public static List<CBLinhaDTO> LinhasMF(string Familia)
        {
            if (String.IsNullOrEmpty(Familia))
            {
                Familia = "LCD";
            }

            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var linhas = (from l in dtbCb.tbl_CBLinha
                              where l.Setor == "FEC" && l.Familia == Familia
                              select l).ToList();

                List<CBLinhaDTO> CBLinhaDTO = new List<CBLinhaDTO>();

                AutoMapper.Mapper.Map(linhas, CBLinhaDTO);

                return CBLinhaDTO;
            }
        }

        public static List<CBLinhaDTO> ObterLinhas(string Familia)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var linhas = (from l in dtbCb.tbl_CBLinha
                              where l.Setor == "FEC" && l.CodLinhaT1.Trim().Substring(0, 3) == Familia
                              select l).OrderBy(x => x.DesLinha).ToList();

                List<CBLinhaDTO> CBLinhaDTO = new List<CBLinhaDTO>();

                AutoMapper.Mapper.Map(linhas, CBLinhaDTO);

                return CBLinhaDTO;
            }
        }

        public static List<CBLinhaDTO> ObterLinhas(string Familia, DateTime DatProducao)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var linhas = (from l in dtbCb.tbl_CBLinha
                              join o in dtbCb.tbl_CBLinhaObservacao on new { l.CodLinha, DatProducao } equals new { o.CodLinha, o.DatProducao } into o2
                              from leftObs in o2.DefaultIfEmpty()
                              where l.Setor == "FEC" && l.CodLinhaT1.Trim().Substring(0, 3) == Familia
                              select new CBLinhaDTO()
                              {
                                  CodLinha = l.CodLinha,
                                  CodFam = l.CodFam,
                                  CodLinhaT1 = l.CodLinhaT1,
                                  CodModelo = l.CodModelo,
                                  CodPlaca = l.CodPlaca,
                                  DesLinha = l.DesLinha,
                                  DesObs = l.DesObs,
                                  Observacao = leftObs.Observacao,
                                  Familia = l.Familia,
                                  Setor = l.Setor,
                                  FlgAtivo = l.flgAtivo
                              }).OrderBy(x => x.DesLinha).ToList();

                return linhas;
            }
        }

        public static List<ProgDiaJitDTO> ObterProgramaByLinha(string CodLinha, int Turno, DateTime DatInicio, DateTime DatFim)
        {
            try
            {
                using (DTB_SIMContext dtbSim = new DTB_SIMContext())
                {
                    var programa = (from p in dtbSim.tbl_ProgDiaJit
                                    join i in dtbSim.tbl_Item on p.CodModelo equals i.CodItem
                                    where p.CodLinha == CodLinha && p.NumTurno == Turno &&
                                            p.DatProducao >= DatInicio && p.DatProducao <= DatFim
                                    group p by new { p.CodLinha, p.NumTurno, p.CodModelo, i.DesItem } into g
                                    select new
                                    {
                                        g.Key.CodLinha,
                                        g.Key.NumTurno,
                                        g.Key.CodModelo,
                                        QtdProdPM = g.Sum(s => s.QtdProdPm),
                                        DesModelo = g.Key.DesItem
                                    });

                    List<ProgDiaJitDTO> prog = programa.ToList().Select(i => new ProgDiaJitDTO()
                    {
                        CodLinha = i.CodLinha,
                        NumTurno = i.NumTurno,
                        CodModelo = i.CodModelo,
                        QtdProdPm = i.QtdProdPM,
                        DesModelo = i.DesModelo
                    }).ToList();

                    return prog;
                }
            }
            catch
            {
                return null;
            }
        }

        public static List<CapacLinhaDTO> ObterCapacidadeByLinha(string CodLinha, int Turno, DateTime DatInicio, DateTime DatFim)
        {
            try
            {
                using (DTB_SIMContext dtbSim = new DTB_SIMContext())
                {
                    var programa = (from p in dtbSim.tbl_CapacLinha
                                    where p.CodLinha == CodLinha && p.NumTurno == Turno &&
                                            p.DatProducao >= DatInicio && p.DatProducao <= DatFim
                                    group p by new { p.CodLinha, p.NumTurno, p.CodModelo, p.DatProducao } into g
                                    select new
                                    {
                                        g.Key.CodLinha,
                                        g.Key.NumTurno,
                                        g.Key.CodModelo,
                                        g.Key.DatProducao,
                                        QtdCapacLinha = g.Sum(s => s.QtdCapacLinha)
                                    });

                    List<tbl_CapacLinha> prog = programa.ToList().Select(i => new tbl_CapacLinha
                    {
                        CodLinha = i.CodLinha,
                        NumTurno = i.NumTurno,
                        CodModelo = i.CodModelo,
                        QtdCapacLinha = i.QtdCapacLinha
                    }).ToList();

                    List<CapacLinhaDTO> capacLinhaDTO = new List<CapacLinhaDTO>();

                    AutoMapper.Mapper.Map(prog, capacLinhaDTO);

                    return capacLinhaDTO;
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Obter a quantidade produzida por linha e agrupado por Linha, Turno e Modelo
        /// </summary>
        /// <param name="CodLinha"></param>
        /// <param name="Turno"></param>
        /// <param name="DatInicio"></param>
        /// <param name="DatFim"></param>
        /// <returns></returns>
        public static List<ProdDiaDTO> ObterProduzidoByLinha(string CodLinha, int Turno, DateTime DatInicio, DateTime DatFim)
        {
            try
            {
                using (DTB_SIMContext dtbSim = new DTB_SIMContext())
                {
                    var produzido = (from p in dtbSim.tbl_ProdDia
                                     join i in dtbSim.tbl_Item on p.CodModelo equals i.CodItem
                                     where p.CodLinha == CodLinha && p.NumTurno == Turno && (p.DatProducao >= DatInicio && p.DatProducao <= DatFim)
                                     group p by new { p.CodLinha, p.NumTurno, p.CodModelo, i.DesItem } into g
                                     select new
                                     {
                                         g.Key.CodModelo,
                                         g.Key.CodLinha,
                                         g.Key.NumTurno,
                                         QtdProduzida = g.Sum(s => s.QtdProduzida),
                                         QtdApontada = g.Sum(s => s.QtdApontada),
                                         g.Key.DesItem
                                     }).ToList();

                    List<ProdDiaDTO> prod = produzido.Select(i => new ProdDiaDTO()
                    {
                        CodLinha = i.CodLinha,
                        NumTurno = i.NumTurno,
                        CodModelo = i.CodModelo,
                        QtdProduzida = i.QtdProduzida,
                        QtdApontada = i.QtdApontada,
                        DesModelo = i.DesItem
                    }).ToList();

                    return prod;
                }
            }
            catch
            {
                return null;
            }
        }

        public static List<ProdDiaDTO> ObterProduzidoLinhaTotal(string CodLinha, int Turno, DateTime DatInicio, DateTime DatFim)
        {
            try
            {
                using (DTB_SIMContext dtbSim = new DTB_SIMContext())
                {
                    var produzido = (from p in dtbSim.tbl_ProdDia
                                     where p.CodLinha == CodLinha && p.NumTurno == Turno && (p.DatProducao >= DatInicio && p.DatProducao <= DatFim)
                                     group p by new { p.CodLinha, p.NumTurno } into g
                                     select new
                                     {
                                         CodLinha = g.Key.CodLinha,
                                         NumTurno = g.Key.NumTurno,
                                         QtdProduzida = g.Sum(s => s.QtdProduzida),
                                         QtdApontada = g.Sum(s => s.QtdApontada)
                                     }).ToList();

                    List<tbl_ProdDia> prod = produzido.Select(i => new tbl_ProdDia
                    {
                        CodLinha = i.CodLinha,
                        NumTurno = i.NumTurno,
                        QtdProduzida = i.QtdProduzida,
                        QtdApontada = i.QtdApontada
                    }).ToList();

                    List<ProdDiaDTO> prodDiaDTO = new List<ProdDiaDTO>();

                    AutoMapper.Mapper.Map(prod, prodDiaDTO);


                    return prodDiaDTO;
                }
            }
            catch
            {
                return null;
            }
        }

        public static CBLinhaObservacaoDTO GravaObservacaoLinha(CBLinhaObservacaoDTO observacao)
        {
            tbl_CBLinhaObservacao obs = new tbl_CBLinhaObservacao();
            
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {

                if (dtbCb.tbl_CBLinhaObservacao.Any(a => a.CodLinha == observacao.CodLinha && a.DatProducao == observacao.DatProducao))
                {
                    obs = dtbCb.tbl_CBLinhaObservacao.FirstOrDefault(a => a.CodLinha == observacao.CodLinha && a.DatProducao == observacao.DatProducao);
                    AutoMapper.Mapper.Map(observacao, obs);
                }
                else
                {
                    AutoMapper.Mapper.Map(observacao, obs);
                    dtbCb.tbl_CBLinhaObservacao.Add(obs);
                }

                dtbCb.SaveChanges();

                return observacao;
            }

        }

        public static List<CBLinhaObservacaoDTO> ObterObservacaoDia(DateTime DatProducao)
        {
            try
            {
                using (DTB_CBContext dtbCb = new DTB_CBContext())
                {
                    var resultado = dtbCb.tbl_CBLinhaObservacao.Where(x => x.DatProducao == DatProducao).ToList();

                    List<CBLinhaObservacaoDTO> observacao = new List<CBLinhaObservacaoDTO>();

                    AutoMapper.Mapper.Map(resultado, observacao);

                    return observacao;
                }
            }
            catch (Exception)
            {
                return new List<CBLinhaObservacaoDTO>();
            }

        }

    }
}
