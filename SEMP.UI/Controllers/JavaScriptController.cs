﻿using SEMP.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEMP.Controllers
{
    public class JavaScriptController : Controller
    {
		[ExternalJavaScriptFile]
		public ActionResult Common()
		{
			return PartialView();
		}


		[ExternalJavaScriptFile]
        public ActionResult TelaColeta()
        {
            return PartialView();
        }

		[ExternalJavaScriptFile]
		public ActionResult TelaOba()
		{
			return PartialView();
		}


		[ExternalJavaScriptFile]
		public ActionResult TelaAmarraLote()
		{
			return PartialView();
		}

		[ExternalJavaScriptFile]
		public ActionResult TelaAmarraLoteJS() {
			return PartialView();
		}
    }
}