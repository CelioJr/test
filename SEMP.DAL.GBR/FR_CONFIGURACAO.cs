namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_CONFIGURACAO
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CNF_CODIGO { get; set; }

        public int CNF_VERSIONA_FORMULARIO { get; set; }

        public int CNF_VERSIONA_RELATORIO { get; set; }

        public int CNF_VERSIONA_REGRA { get; set; }

        [Required]
        [StringLength(20)]
        public string CNF_MAKER_VERSION { get; set; }
    }
}
