using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_gpaboletosMap : EntityTypeConfiguration<tbl_gpaboletos>
    {
        public tbl_gpaboletosMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.numgrim, t.CodItem, t.CodLocal, t.CodGalpao, t.CodRua, t.CodBloco, t.CodApto, t.SubApto, t.CodNivel, t.SubNivel, t.QtdEnderecar, t.DatEmissao, t.tipendent });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.numgrim)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.CodItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodLocal)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.CodGalpao)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodRua)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodBloco)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.CodApto)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.SubApto)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.CodNivel)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.SubNivel)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.QtdEnderecar)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.numcontrole)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.usuario)
                .HasMaxLength(128);

            this.Property(t => t.tipendent)
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_gpaboletos");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.numgrim).HasColumnName("numgrim");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.CodLocal).HasColumnName("CodLocal");
            this.Property(t => t.CodGalpao).HasColumnName("CodGalpao");
            this.Property(t => t.CodRua).HasColumnName("CodRua");
            this.Property(t => t.CodBloco).HasColumnName("CodBloco");
            this.Property(t => t.CodApto).HasColumnName("CodApto");
            this.Property(t => t.SubApto).HasColumnName("SubApto");
            this.Property(t => t.CodNivel).HasColumnName("CodNivel");
            this.Property(t => t.SubNivel).HasColumnName("SubNivel");
            this.Property(t => t.QtdEnderecar).HasColumnName("QtdEnderecar");
            this.Property(t => t.numcontrole).HasColumnName("numcontrole");
            this.Property(t => t.DatEmissao).HasColumnName("DatEmissao");
            this.Property(t => t.usuario).HasColumnName("usuario");
            this.Property(t => t.tipendent).HasColumnName("tipendent");
            this.Property(t => t.QtdReserva).HasColumnName("QtdReserva");
        }
    }
}
