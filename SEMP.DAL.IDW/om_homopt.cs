namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_homopt
    {
        [Key]
        public long id_homo { get; set; }

        public long id_usrhomologado { get; set; }

        public long id_usr { get; set; }

        public long id_pt { get; set; }

        public DateTime? dthr_homopt { get; set; }

        public short? tp_homopt { get; set; }

        public virtual om_pt om_pt { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }
    }
}
