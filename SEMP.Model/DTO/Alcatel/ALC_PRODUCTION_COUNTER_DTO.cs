using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_PRODUCTION_COUNTER_DTO
    {
        public int idCounter { get; set; }
        public Nullable<System.DateTime> dtPallet { get; set; }
        public string order_idOrder { get; set; }
        public Nullable<int> product_COD_SMT { get; set; }
        public Nullable<int> RESP_PALLET { get; set; }
        public Nullable<int> line_COD_LINHAS { get; set; }
        public string packing { get; set; }
        public string volume { get; set; }
        public string ID_LOTE { get; set; }
        public Nullable<System.DateTime> dt_pallet { get; set; }
        public virtual ALC_ORDER_DTO ALC_ORDER { get; set; }
        public virtual ALC_ORDER_DTO ALC_ORDER1 { get; set; }
        public virtual ALC_VOLUME_DTO ALC_VOLUME { get; set; }
        public virtual ALC_SYS_USER_DTO ALC_SYS_USER { get; set; }
    }
}
