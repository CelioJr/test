using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBTipoPosto
    {
        public int CodTipoPosto { get; set; }
        public string DesTipoPosto { get; set; }
        public string DesAcao { get; set; }
		public string DesCor { get; set; }
		public string FlgEntradaSaida { get; set; }
	}
}
