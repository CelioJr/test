using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_POST_USER_DTO
    {
        public ALC_POST_USER_DTO()
        {
            this.ALC_SYS_USER = new List<ALC_SYS_USER_DTO>();
            this.ALC_SYS_USER1 = new List<ALC_SYS_USER_DTO>();
            this.ALC_REPAIR = new List<ALC_REPAIR_DTO>();
        }

        public int idPost { get; set; }
        public string description { get; set; }
        public virtual ICollection<ALC_SYS_USER_DTO> ALC_SYS_USER { get; set; }
        public virtual ICollection<ALC_SYS_USER_DTO> ALC_SYS_USER1 { get; set; }
        public virtual ICollection<ALC_REPAIR_DTO> ALC_REPAIR { get; set; }
    }
}
