using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBLoteConfigMap : EntityTypeConfiguration<tbl_CBLoteConfig>
    {
        public tbl_CBLoteConfigMap()
        {
            // Primary Key
            this.HasKey(t => t.idLinha);

            // Properties
            this.Property(t => t.idLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodDRT)
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_CBLoteConfig");
            this.Property(t => t.idLinha).HasColumnName("idLinha");
            this.Property(t => t.Qtde).HasColumnName("Qtde");
            this.Property(t => t.CodDRT).HasColumnName("CodDRT");
            this.Property(t => t.QtdeCC).HasColumnName("QtdeCC");
			this.Property(t => t.PercAmostras).HasColumnName("PercAmostras");
			this.Property(t => t.QtdAmostras).HasColumnName("QtdAmostras");
        }
    }
}
