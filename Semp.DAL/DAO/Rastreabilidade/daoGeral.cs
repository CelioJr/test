﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.VO;
using SEMP.DAL.Models;
using SEMP.Model.DTO;
using System.Globalization;
using System.Data.SqlClient;
using SEMP.Model.VO.Embalagem;
using SEMP.Model;
using SEMP.Model.VO.Rastreabilidade;
using System.Data;

namespace SEMP.DAL.DAO.Rastreabilidade
{
	public class daoGeral
	{
		#region Banco SISAP
		public static List<CBLinhaDTO> ObterLinhaPorFabrica(string fabrica)
		{

			using (var dtbCb = new DTB_CBContext())
			{
				var linhasDto = new List<CBLinhaDTO>();

				try
				{
					var linhas = (from l in dtbCb.tbl_CBLinha
								  let qtdPostos = dtbCb.tbl_CBPosto.Count(x => x.CodLinha == l.CodLinha)
								  where l.Setor == fabrica && l.CodLinhaT1 != null
								  orderby l.CodLinhaT1
								  select new { l, qtdPostos }).ToList();
					linhasDto = linhas.Select(x => new CBLinhaDTO()
					{
						CodAuxiliar = x.l.CodAuxiliar,
						CodFam = x.l.CodFam,
						CodLinha = x.l.CodLinha,
						CodLinhaOrigem = x.l.CodLinhaOrigem,
						CodLinhaT1 = x.l.CodLinhaT1,
						CodLinhaT2 = x.l.CodLinhaT2,
						CodLinhaT3 = x.l.CodLinhaT3,
						CodModelo = x.l.CodModelo,
						CodPlaca = x.l.CodPlaca,
						DesLinha = x.l.DesLinha,
						DesObs = x.l.DesObs,
						Familia = x.l.Familia,
						FamiliaOrigem = x.l.FamiliaOrigem,
						FlgAtivo = x.l.flgAtivo,
						Setor = x.l.Setor,
						SetorOrigem = x.l.SetorOrigem,
						QtdPostos = x.qtdPostos
					}).ToList();
				}
				catch
				{

					throw;
				}


				return linhasDto;

			}

		}

		public static List<CBLinhaDTO> ObterLinhaPorData(DateTime datProducao)
		{

			using (var dtbCb = new DTB_CBContext())
			{
				var linhasDto = new List<CBLinhaDTO>();

				var dFim = datProducao.AddDays(1).AddMinutes(-1);

				try
				{
					var codLinhas = dtbCb.tbl_CBPassagem.Where(x => x.DatEvento >= datProducao && x.DatEvento <= dFim).Select(x => x.CodLinha).Distinct().ToList();

					var linhas = (from l in dtbCb.tbl_CBLinha
								  where codLinhas.Contains(l.CodLinha)
								  orderby l.CodLinhaT1
								  select l).ToList();

					AutoMapper.Mapper.Map(linhas, linhasDto);
				}
				catch
				{

					throw;
				}


				return linhasDto;

			}

		}

		public static List<CBLinhaDTO> ObterLinhasRecebimento(string fabrica)
		{

			using (var dtbCb = new DTB_CBContext())
			{
				var linhasDTO = new List<CBLinhaDTO>();

				try
				{
					linhasDTO = (from l in dtbCb.tbl_CBLinha
								 let qtdPosto = dtbCb.tbl_CBPosto.Count(p => l.CodLinha == p.CodLinha && p.CodTipoPosto == Constantes.POSTO_RECEBIMENTO && p.FlgAtivo == "S")
								 where l.Setor == fabrica &&
										 l.CodLinhaT1 != null &&
										 qtdPosto > 0 &&
										 l.flgAtivo == "1"
								 orderby l.CodLinhaT1
								 select new CBLinhaDTO()
								 {
									 CodLinha = l.CodLinha,
									 DesLinha = l.DesLinha
								 }).ToList();


				}
				catch
				{

					throw;
				}


				return linhasDTO;

			}

		}

		public static string ObterLinhaCliente(int codLinha)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var firstOrDefault = (from linha in dtbCb.tbl_CBLinha
									  where linha.CodLinha == codLinha
									  select new { LinhaCliente = linha.CodLinhaT1 }
					).FirstOrDefault();
				if (firstOrDefault != null)
				{
					var query = firstOrDefault.LinhaCliente;

					return string.IsNullOrWhiteSpace(query) ? "" : query;
				}

				return "";
			}
		}

		public static CBLinhaDTO ObterLinha(int codLinha)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var linha = (from i in dtbCb.tbl_CBLinha
							 where i.CodLinha == codLinha
							 select i).FirstOrDefault();

				var linhaDTO = new CBLinhaDTO();

				AutoMapper.Mapper.Map(linha, linhaDTO);

				return linhaDTO;
			}
		}

		public static CBLinhaDTO ObterLinhaT1(string codLinha)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var linha = (from i in dtbCb.tbl_CBLinha
							 where i.CodLinhaT1 == codLinha
							 select i).FirstOrDefault();

				var linhaDTO = new CBLinhaDTO();

				AutoMapper.Mapper.Map(linha, linhaDTO);

				return linhaDTO;
			}
		}

		public static List<CBPostoDTO> ObterPostoPorLinha(int codLinha)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var postos = (from p in dtbCb.tbl_CBPosto
							  where p.CodLinha == codLinha
							  select p).ToList();

				var postosDto = new List<CBPostoDTO>();

				AutoMapper.Mapper.Map(postos, postosDto);

				return postosDto;
			}
		}

		public static CBPostoDTO ObterPostoPorDRT(string codDrt)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var posto = (from i in dtbCb.tbl_CBPosto
							 join l in dtbCb.tbl_CBLinha on i.CodLinha equals l.CodLinha
							 join t in dtbCb.tbl_CBTipoPosto on i.CodTipoPosto equals t.CodTipoPosto
							 where i.CodDRT == codDrt
							 select new CBPostoDTO()
							 {
								 CodLinha = i.CodLinha,
								 NumPosto = i.NumPosto,
								 CodTipoPosto = i.CodTipoPosto,
								 DesPosto = i.DesPosto,
								 NumIP = i.NumIP,
								 CodDRT = i.CodDRT,
								 FlgTipoTerminal = i.FlgTipoTerminal,
								 FlgAtivo = i.FlgAtivo,
								 NumSeq = i.NumSeq,
								 flgObrigatorio = i.flgObrigatorio,
								 flgDRTObrig = i.flgDRTObrig,
								 TipoAmarra = i.TipoAmarra,
								 flgPedeAP = i.flgPedeAP,
								 flgImprimeEtq = i.flgImprimeEtq,
								 TempoLeitura = i.TempoLeitura,
								 TipoEtiqueta = i.TipoEtiqueta,
								 TempoCiclo = i.TempoCiclo,
								 FlgGargalo = i.FlgGargalo,
								 NumIPBalanca = i.NumIPBalanca,
								 NomFuncionario = "",
								 PostoBloqueado = i.PostoBloqueado,
								 TempoBloqueio = i.TempoBloqueio,
								 flgValidaOrigem = i.flgValidaOrigem,
								 MontarLote = i.MontarLote,
								 flgBaixaEstoque = i.flgBaixaEstoque,
								 DesCor = t.DesCor,
								 DesTipoPosto = t.DesTipoPosto,
								 DesLinha = l.DesLinha.Trim(),
								 Setor = l.Setor,
								 DesAcao = t.DesAcao
							 }).FirstOrDefault();

				return posto;
			}
		}

		public static CBPostoDTO ObterPostoPorIP(string numIP)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var posto = (from i in dtbCb.tbl_CBPosto
							 join l in dtbCb.tbl_CBLinha on i.CodLinha equals l.CodLinha
							 join t in dtbCb.tbl_CBTipoPosto on i.CodTipoPosto equals t.CodTipoPosto
							 where i.NumIP == numIP
							 select new CBPostoDTO()
							 {
								 CodLinha = i.CodLinha,
								 NumPosto = i.NumPosto,
								 CodTipoPosto = i.CodTipoPosto,
								 DesPosto = i.DesPosto,
								 NumIP = i.NumIP,
								 CodDRT = i.CodDRT,
								 FlgTipoTerminal = i.FlgTipoTerminal,
								 FlgAtivo = i.FlgAtivo,
								 NumSeq = i.NumSeq,
								 flgObrigatorio = i.flgObrigatorio,
								 flgDRTObrig = i.flgDRTObrig,
								 TipoAmarra = i.TipoAmarra,
								 flgPedeAP = i.flgPedeAP,
								 flgImprimeEtq = i.flgImprimeEtq,
								 TempoLeitura = i.TempoLeitura,
								 TipoEtiqueta = i.TipoEtiqueta,
								 TempoCiclo = i.TempoCiclo,
								 FlgGargalo = i.FlgGargalo,
								 NumIPBalanca = i.NumIPBalanca,
								 NomFuncionario = "",
								 PostoBloqueado = i.PostoBloqueado,
								 TempoBloqueio = i.TempoBloqueio,
								 flgValidaOrigem = i.flgValidaOrigem,
								 MontarLote = i.MontarLote,
								 flgBaixaEstoque = i.flgBaixaEstoque,
								 DesCor = t.DesCor,
								 DesTipoPosto = t.DesTipoPosto,
								 DesLinha = l.DesLinha.Trim(),
								 Setor = l.Setor,
								 DesAcao = t.DesAcao
							 }).FirstOrDefault();

				return posto;
			}
		}

		public static CBPostoDTO ObterPosto(int codLinha, int numPosto)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var posto = (from i in dtbCb.tbl_CBPosto
							 join l in dtbCb.tbl_CBLinha on i.CodLinha equals l.CodLinha
							 join t in dtbCb.tbl_CBTipoPosto on i.CodTipoPosto equals t.CodTipoPosto
							 where i.CodLinha == codLinha && i.NumPosto == numPosto
							 select new CBPostoDTO()
							 {
								 CodLinha = i.CodLinha,
								 NumPosto = i.NumPosto,
								 CodTipoPosto = i.CodTipoPosto,
								 DesPosto = i.DesPosto,
								 NumIP = i.NumIP,
								 CodDRT = i.CodDRT,
								 FlgTipoTerminal = i.FlgTipoTerminal,
								 FlgAtivo = i.FlgAtivo,
								 NumSeq = i.NumSeq,
								 flgObrigatorio = i.flgObrigatorio,
								 flgDRTObrig = i.flgDRTObrig,
								 TipoAmarra = i.TipoAmarra,
								 flgPedeAP = i.flgPedeAP,
								 flgImprimeEtq = i.flgImprimeEtq,
								 TempoLeitura = i.TempoLeitura,
								 TipoEtiqueta = i.TipoEtiqueta,
								 TempoCiclo = i.TempoCiclo,
								 FlgGargalo = i.FlgGargalo,
								 NumIPBalanca = i.NumIPBalanca,
								 NomFuncionario = "",
								 PostoBloqueado = i.PostoBloqueado,
								 TempoBloqueio = i.TempoBloqueio,
								 flgValidaOrigem = i.flgValidaOrigem,
								 MontarLote = i.MontarLote,
								 flgBaixaEstoque = i.flgBaixaEstoque,
								 DesCor = t.DesCor,
								 DesTipoPosto = t.DesTipoPosto,
								 DesLinha = l.DesLinha.Trim(),
								 Setor = l.Setor,
								 DesAcao = t.DesAcao
							 }).FirstOrDefault();

				return posto;
			}
		}

		public static CBTipoPostoDTO ObterTipoPosto(int codTipoPosto)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var tipoPosto = (from i in dtbCb.tbl_CBTipoPosto
								 where i.CodTipoPosto == codTipoPosto
								 select i).FirstOrDefault();

				var tipoPostoDTO = new CBTipoPostoDTO();

				AutoMapper.Mapper.Map(tipoPosto, tipoPostoDTO);

				return tipoPostoDTO;
			}
		}

		public static List<CBTipoPostoDTO> ObterTipoPostos()
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var tipoPosto = (from i in dtbCb.tbl_CBTipoPosto
								 select i).ToList();

				var tipoPostoDTO = new List<CBTipoPostoDTO>();

				AutoMapper.Mapper.Map(tipoPosto, tipoPostoDTO);

				return tipoPostoDTO;
			}
		}

		public static CBPostoDTO ObterPostoLote(int codLinha)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var postos = (from p in dtbCb.tbl_CBPosto
							  where p.CodLinha == codLinha && p.MontarLote == true
							  select p).FirstOrDefault();

				if (postos == null)
				{
					postos = (from p in dtbCb.tbl_CBPosto
							  where p.CodLinha == codLinha &&
								(
									p.CodTipoPosto == Constantes.POSTO_EMBALAGEM ||
									p.CodTipoPosto == Constantes.POSTO_TESTE_FECHAMENTO_DE_LOTE
								)
							  select p).FirstOrDefault();
				}
				var postosDto = new CBPostoDTO();

				AutoMapper.Mapper.Map(postos, postosDto);

				return postosDto;
			}
		}

		public static int ObterUltimoPostoObrigatorio(int codLinha)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				var posto = (from p in dtbCb.tbl_CBPosto
							 where p.CodLinha == codLinha && p.flgObrigatorio == true
							 select p).OrderByDescending(X => X.NumSeq).FirstOrDefault();

				return posto.NumPosto;
			}
		}

		public static List<CBHorarioDTO> ObterHorarios()
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				List<tbl_CBHorario> horarios = dtbCb.tbl_CBHorario.ToList();

				List<CBHorarioDTO> horariosDTO = new List<CBHorarioDTO>();

				AutoMapper.Mapper.Map(horarios, horariosDTO);

				return horariosDTO;
			}
		}

		public static CBHorarioDTO ObterHorariosPorID(int idHorario)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				tbl_CBHorario horario = dtbCb.tbl_CBHorario.Where(x => x.idHorario == idHorario).FirstOrDefault();

				CBHorarioDTO horariosDTO = new CBHorarioDTO();

				AutoMapper.Mapper.Map(horario, horariosDTO);

				return horariosDTO;
			}
		}

		public static List<CBHorarioDTO> ObterHorariosFabrica(String Fabrica)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				List<tbl_CBHorario> horarios = (
					from h in dtbCb.tbl_CBHorario
					join hl in dtbCb.tbl_CBHorarioLinha on new { h.idHorario, Turno = (int)h.Turno } equals
						new { hl.idHorario, hl.Turno }
					where hl.DatFim == null
					select h
					).ToList();

				List<CBHorarioDTO> horariosDTO = new List<CBHorarioDTO>();

				AutoMapper.Mapper.Map(horarios, horariosDTO);

				return horariosDTO;
			}
		}

		public static List<CBHorarioDTO> ObterHorariosFabrica(String Fabrica, int CodLinha)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				List<tbl_CBHorario> horarios = (
					from h in dtbCb.tbl_CBHorario
					join hl in dtbCb.tbl_CBHorarioLinha on new { h.idHorario, Turno = (int)h.Turno } equals
						new { hl.idHorario, hl.Turno }
					where hl.DatFim == null && hl.CodLinha == CodLinha
					select h
					).ToList();

				List<CBHorarioDTO> horariosDTO = new List<CBHorarioDTO>();

				AutoMapper.Mapper.Map(horarios, horariosDTO);

				return horariosDTO;
			}
		}

		public static DateTime ObterDataInicio(int CodLinha, TimeSpan Hora)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				DateTime DatInicio = new DateTime();
				TimeSpan HoraFimDia = new TimeSpan(23, 59, 59);
				TimeSpan HoraInicioDia = new TimeSpan(0, 0, 0);

				List<int> idHorario = new List<int>();
				idHorario = (from p in dtbCb.tbl_CBHorarioLinha
							 where CodLinha == p.CodLinha && p.DatFim == null
							 select p.idHorario).ToList();

				if (idHorario != null)
				{

					foreach (int registro in idHorario)
					{
						var hora = (from p in dtbCb.tbl_CBHorario
									where p.idHorario == registro
									select new { p.HoraInicio, p.HoraFim, p.flgViraDia }).ToArray();

						if ((Hora >= daoParadas.ToTimeSpan(hora[0].HoraInicio) && Hora <= daoParadas.ToTimeSpan(hora[0].HoraFim)) ||
							(hora[0].flgViraDia == "S" &&
							 ((Hora >= daoParadas.ToTimeSpan(hora[0].HoraInicio) && Hora <= HoraFimDia) ||
							  (Hora >= HoraInicioDia && Hora <= daoParadas.ToTimeSpan(hora[0].HoraFim)))))
						{
							DatInicio = Convert.ToDateTime(hora[0].HoraInicio);
							return DatInicio;
						}
					}
				}

				return DatInicio;
			}
		}

		public static DateTime ObterDataFim(int CodLinha, TimeSpan Hora)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				DateTime DatFim = new DateTime();
				TimeSpan HoraFimDia = new TimeSpan(23, 59, 59);
				TimeSpan HoraInicioDia = new TimeSpan(0, 0, 0);

				List<int> idHorario = new List<int>();
				idHorario = (from p in dtbCb.tbl_CBHorarioLinha
							 where CodLinha == p.CodLinha && p.DatFim == null
							 select p.idHorario).ToList();

				if (idHorario != null)
				{

					foreach (int registro in idHorario)
					{
						var hora = (from p in dtbCb.tbl_CBHorario
									where p.idHorario == registro
									select new { p.HoraInicio, p.HoraFim, p.flgViraDia }).ToArray();

						if ((Hora >= daoParadas.ToTimeSpan(hora[0].HoraInicio) && Hora <= daoParadas.ToTimeSpan(hora[0].HoraFim)) ||
							(hora[0].flgViraDia == "S" &&
							 ((Hora >= daoParadas.ToTimeSpan(hora[0].HoraInicio) && Hora <= HoraFimDia) ||
							  (Hora >= HoraInicioDia && Hora <= daoParadas.ToTimeSpan(hora[0].HoraFim)))))
						{
							DatFim = Convert.ToDateTime(hora[0].HoraFim);
							return DatFim;
						}
					}
				}

				return DatFim;
			}
		}

		public static int ObterTotalAprovadas(int CodLinha, int NumPosto, DateTime DatInicio, DateTime DatFim)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				var contador = (from p in dtbCb.tbl_CBPassagem
								where p.CodLinha == CodLinha && p.NumPosto == NumPosto && p.DatEvento >= DatInicio && p.DatEvento <= DatFim
								select p).Count();

				return contador;
			}
		}

		public static int ObterTotalAprovadasModelo(int CodLinha, int NumPosto, DateTime DatInicio, DateTime DatFim, string codModelo)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				var contador = (from p in dtbCb.tbl_CBPassagem
								where p.CodLinha == CodLinha && p.NumPosto == NumPosto && p.DatEvento >= DatInicio && p.DatEvento <= DatFim &&
										p.NumECB.StartsWith(codModelo)
								select p).Count();

				return contador;
			}
		}

		public static int ObterTotalAmarradoAP(string AP)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				var contador = (from p in dtbCb.tbl_CBAmarra
								where p.NumAP == AP
								select p.NumSerie).Distinct().Count();

				return contador;
			}
		}

		public static int ObterTotalAmarradoAP(int codLinha, int numPosto, string codFab, string numAP)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				/*var contador = (from p in dtbCb.tbl_CBAmarra
								where p.NumAP == AP && p.CodLinha == codLinha && p.NumPosto == numPosto
								select p.NumSerie).Distinct().Count();*/

				//var contador = dtbCb.tbl_CBAmarra.Count(p => p.NumAP == AP && p.CodLinha == codLinha && p.NumPosto == numPosto);
				var contador = dtbCb.spc_CBContaAmarraAP(codLinha, numPosto, codFab, numAP).FirstOrDefault();

				return contador;
			}
		}

		public static int ObterTotalReprovadas(int CodLinha, int NumPosto, DateTime DatInicio, DateTime DatFim)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				var contador = (from p in dtbCb.tbl_CBDefeito
								where p.CodLinha == CodLinha && p.NumPosto == NumPosto && p.DatEvento >= DatInicio && p.DatEvento <= DatFim && String.IsNullOrEmpty(p.NumSeriePai)
								&& (p.CodCausa != "SF882" && (p.FlgRevisado < 2 || p.FlgRevisado == 3))
								select p.NumECB).Count();

				return contador;
			}
		}

		public static int ObterTotal(int CodLinha, int NumPosto, DateTime DatInicio, DateTime DatFim)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{

				var contador = (from A in
									(from A in
										 (
											 ((
											 from Tbl_CBPassagem in dtbCb.tbl_CBPassagem
											 where
												 Tbl_CBPassagem.CodLinha == CodLinha &&
												 Tbl_CBPassagem.NumPosto == NumPosto &&
												 (Tbl_CBPassagem.DatEvento) >= DatInicio &&
												 (Tbl_CBPassagem.DatEvento) <= DatFim
											 select new
											 {
												 Tbl_CBPassagem.NumECB
											 }
											 ).Concat
											 (
										  (from Tbl_CBDefeito in dtbCb.tbl_CBDefeito
										   where
											(Tbl_CBDefeito.FlgRevisado < 2 || Tbl_CBDefeito.FlgRevisado == 3) &&
											   Tbl_CBDefeito.CodLinha == CodLinha &&
											   Tbl_CBDefeito.NumPosto == NumPosto &&
											   (Tbl_CBDefeito.DatEvento) >= DatInicio &&
											   (Tbl_CBDefeito.DatEvento) <= DatFim &&
											   !
												   (from Tbl_CBPassagem in dtbCb.tbl_CBPassagem
													where
														Tbl_CBPassagem.CodLinha == CodLinha &&
														Tbl_CBPassagem.NumPosto == NumPosto &&
														(Tbl_CBPassagem.DatEvento) >= DatInicio &&
														(Tbl_CBPassagem.DatEvento) <= DatFim
													select new
													{
														Tbl_CBPassagem.NumECB
													}).Contains(new { NumECB = Tbl_CBDefeito.NumECB })
										   select new
										   {
											   NumECB = Tbl_CBDefeito.NumECB
										   }).Distinct()
											 )))
									 select new
									 {
										 A.NumECB,
										 Dummy = "x"
									 })
								group A by new { A.Dummy }
									into g
								select new
								{
									Column1 = g.Count(p => p.NumECB != null)
								}).ToList();


				return !contador.Any() ? 0 : contador.FirstOrDefault().Column1;
			}
		}

		public static string ObterModeloPorECB(String NumEcb)
		{
			using (DTB_CBContext dtbCb = new DTB_CBContext())
			{
				var modelo = dtbCb.tbl_CBIdentifica.FirstOrDefault(x => x.NumECB == NumEcb);
				if (modelo != null)
				{
					return modelo.CodModelo ?? modelo.CodPlacaIMC;
				}
				else
				{
					return "";
				}
			}
		}

		public static string ObterNumAP(string NumECB)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var NumAP = dtb_CB.tbl_CBAmarra.Where(x => x.NumSerie == NumECB).Select(x => x.NumAP).FirstOrDefault();

				return NumAP.ToString();
			}
		}

		public static string ObterCodFab(string NumECB)
		{
			using (DTB_CBContext dtb_CB = new DTB_CBContext())
			{
				var CodFab = dtb_CB.tbl_CBAmarra.Where(x => x.NumSerie == NumECB).Select(x => x.CodFab).FirstOrDefault();

				return CodFab.ToString();
			}
		}

		public static int ObterQuantidadeLotePorLinha(int codlinha)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var res = dtbCb.tbl_CBLoteConfig.FirstOrDefault(x => x.idLinha == codlinha);

				return res == null ? 0 : res.Qtde ?? 0;
			}
		}

		public static int ObterQuantidadeCaixaColetivaPorLinha(int codlinha)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var res = dtbCb.tbl_CBLoteConfig.FirstOrDefault(x => x.idLinha == codlinha);

				return res == null ? 0 : res.QtdeCC ?? 0;
			}
		}

		public static int QtdeCaixasColetivasNoLote(string numLote)
		{
			int valor = 0;
			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				valor = contexto.tbl_CBLote.Count(x => x.NumLotePai == numLote);
			}

			return valor;
		}

		public static int QtdeCaixasColetivasNoLote(int codLinha)
		{
			using (DTB_CBContext contexto = new DTB_CBContext())
			{

				var resultado = contexto.Database.SqlQuery<int>(
							"EXEC spc_CBLoteQtdePai @CodLinha",
							new SqlParameter("CodLinha", codLinha)
							);

				contexto.SaveChanges();

				var retornoProc = resultado.FirstOrDefault();

				return retornoProc;

				/*var embalado = contexto.tbl_CBEmbalada.Where(x => x.CodLinha == codLinha).OrderByDescending(x => x.DatLeitura).FirstOrDefault();
				var numLote = embalado != null ? embalado.NumLote : "";

				valor = contexto.tbl_CBLote.Count(x => x.NumLotePai == numLote);*/
			}

		}

		public static tbl_CBLoteConfig Obter_tblCBLoteConfig(int codlinha)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var res = dtbCb.tbl_CBLoteConfig.FirstOrDefault(x => x.idLinha == codlinha);

				return res;
			}
		}

		public static CBAmarraDTO GetItemAmarrado(string NumSerie)
		{
			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				var amarra = contexto.tbl_CBAmarra.Select(x => new CBAmarraDTO()
				{
					CodFab = x.CodFab,
					CodItem = x.CodItem,
					CodLinha = x.CodLinha,
					CodModelo = x.CodModelo,
					DatAmarra = x.DatAmarra,
					FlgTipo = x.FlgTipo,
					NumAP = x.NumAP,
					NumECB = x.NumECB,
					NumPosto = x.NumPosto,
					NumSerie = x.NumSerie
				}).FirstOrDefault(a => a.NumECB == NumSerie);

				return amarra;
			}
		}

		public static string GetPlaca(string NumSerie, string codItem)
		{
			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				var amarra = (from a in contexto.tbl_CBAmarra
							  join tipo in contexto.tbl_CBTipoAmarra on a.FlgTipo equals tipo.CodTipoAmarra
							  where tipo.FlgPlacaPainel == 1 && a.NumSerie == NumSerie && a.CodItem == codItem
							  select a.NumECB).FirstOrDefault();

				return amarra ?? "";
			}
		}

		public static string GetPainel(string NumSerie, string codItem)
		{
			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				var amarra = (from a in contexto.tbl_CBAmarra
							  join tipo in contexto.tbl_CBTipoAmarra on a.FlgTipo equals tipo.CodTipoAmarra
							  where tipo.FlgPlacaPainel == 2 && a.NumSerie == NumSerie && a.CodItem == codItem
							  select a.NumECB).FirstOrDefault();

				return amarra ?? "";

			}
		}

		public static string GetItem(string NumSerie, string codItem)
		{
			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				var amarra = (from a in contexto.tbl_CBAmarra
							  join tipo in contexto.tbl_CBTipoAmarra on a.FlgTipo equals tipo.CodTipoAmarra
							  where a.NumSerie == NumSerie && a.CodItem == codItem
							  select a.NumECB).FirstOrDefault();

				return amarra ?? "";
			}
		}

		public static List<CBPassagemDTO> RastrearPassagem(string NumSerie)
		{
			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				var listaPassagem = contexto.tbl_CBPassagem.Where(a => a.NumECB == NumSerie)
					.OrderBy(a => a.DatEvento)
					.ToList()
					.Select(s => new CBPassagemDTO
					{
						NumECB = s.NumECB,
						CodLinha = s.CodLinha,
						NumPosto = s.NumPosto,
						DatEvento = s.DatEvento,
						CodDRT = s.CodDRT,
						NomeOperador = GetFuncionarioByDRT(s.CodDRT).NomFuncionario,
						DescricaoLinha = contexto.tbl_CBLinha.Where(a => a.CodLinha == s.CodLinha).FirstOrDefault().DesLinha,
						DescricaoPosto = contexto.tbl_CBPosto.Where(a => a.CodLinha == s.CodLinha && a.NumPosto == s.NumPosto).FirstOrDefault().DesPosto
					}).ToList();

				return listaPassagem;
			}
		}

		public static List<CBDefeitoDTO> RastrearDefeito(string NumSerie)
		{
			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				List<tbl_CBDefeito> lista = contexto.tbl_CBDefeito.Where(a => a.NumECB == NumSerie).ToList();

				List<CBDefeitoDTO> listaDTO = new List<CBDefeitoDTO>();

				AutoMapper.Mapper.Map(lista, listaDTO);

				foreach (CBDefeitoDTO item in listaDTO)
				{
					item.DescricaoLinha = contexto.tbl_CBLinha.Where(a => a.CodLinha == item.CodLinha).FirstOrDefault().DesLinha;
					item.DescricaoPosto = contexto.tbl_CBPosto.Where(a => a.CodLinha == item.CodLinha && a.NumPosto == item.NumPosto).FirstOrDefault().DesPosto;
					if (!string.IsNullOrWhiteSpace(item.DesAcao))
					{
						var codAcao = int.Parse(item.DesAcao);
						item.DesAcao = contexto.tbl_CBAcao.Where(a => a.CodAcao == codAcao).Select(a => a.Descricao).FirstOrDefault();
					}
					item.OperadorTecnico = GetFuncionarioByDRT(item.CodDRT).NomFuncionario;
					item.OperadorApontouDefeito = GetFuncionarioByDRT(item.CodDRTTest).NomFuncionario;
				}

				return listaDTO;
			}
		}

		public static List<CBAmarraDTO> RastrearAmarracao(string NumSerie)
		{
			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				List<tbl_CBAmarra> lista = null;

				if (NumSerie.Length == 16 || NumSerie.Length == 9)
				{
					lista = contexto.tbl_CBAmarra.Where(a => a.NumSerie == NumSerie).ToList();

					if (lista == null || lista.Count() == 0){
						lista = contexto.tbl_CBAmarra.Where(a => a.NumECB == NumSerie).ToList();
					}
				}
				else
				{
					lista = contexto.tbl_CBAmarra.Where(a => a.NumECB == NumSerie).ToList();
				}

				List<CBAmarraDTO> listaDTO = new List<CBAmarraDTO>();

				AutoMapper.Mapper.Map(lista, listaDTO);

				return listaDTO.Select(a => new CBAmarraDTO
				{
					CodFab = a.CodFab,
					CodItem = a.CodItem,
					CodLinha = a.CodLinha,
					CodModelo = a.CodModelo,
					DatAmarra = a.DatAmarra,
					DesTipoAmarra = contexto.tbl_CBTipoAmarra.Where(x => x.CodTipoAmarra == a.FlgTipo).FirstOrDefault().DesTipoAmarra,
					DipositivoID = a.DipositivoID,
					FlgTipo = a.FlgTipo,
					IsDispositivoIntegrado = a.IsDispositivoIntegrado,
					NumAP = a.NumAP,
					NumECB = a.NumECB,
					NumPosto = a.NumPosto,
					NumSerie = a.NumSerie,
					DescricaoLinha = contexto.tbl_CBLinha.Where(c => c.CodLinha == a.CodLinha).FirstOrDefault().DesLinha,
					DescricaoPosto = contexto.tbl_CBPosto.Where(c => c.CodLinha == a.CodLinha && c.NumPosto == a.NumPosto).FirstOrDefault().DesPosto
				}).ToList();
			}
		}

		public static List<CBPassagem_HistDTO> RastrearPassagemHist(string NumSerie)
		{
			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				var listaPassagem = contexto.tbl_CBPassagem_Hist.Where(a => a.NumECB == NumSerie)
					.OrderBy(a => a.DatEvento)
					.ToList()
					.Select(s => new CBPassagem_HistDTO
					{
						NumECB = s.NumECB,
						CodLinha = s.CodLinha,
						NumPosto = s.NumPosto,
						DatEvento = s.DatEvento,
						CodDRT = s.CodDRT,
						NomeOperador = GetFuncionarioByDRT(s.CodDRT).NomFuncionario,
						DescricaoLinha = contexto.tbl_CBLinha.Where(a => a.CodLinha == s.CodLinha).FirstOrDefault().DesLinha,
						DescricaoPosto = contexto.tbl_CBPosto.Where(a => a.CodLinha == s.CodLinha && a.NumPosto == s.NumPosto).FirstOrDefault().DesPosto
					}).ToList();

				return listaPassagem;
			}
		}

		public static List<CBAmarra_HistDTO> RastrearAmarracaoHist(string NumSerie)
		{
			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				List<tbl_CBAmarra_Hist> lista = null;

				if (NumSerie.Length == 16)
				{
					lista = contexto.tbl_CBAmarra_Hist.Where(a => a.NumSerie == NumSerie).ToList();
				}
				else
				{
					lista = contexto.tbl_CBAmarra_Hist.Where(a => a.NumECB == NumSerie).ToList();
				}

				List<CBAmarra_HistDTO> listaDTO = new List<CBAmarra_HistDTO>();

				AutoMapper.Mapper.Map(lista, listaDTO);

				return listaDTO.Select(s => new CBAmarra_HistDTO
				{
					CodFab = s.CodFab,
					CodItem = s.CodItem,
					CodLinha = s.CodLinha,
					CodModelo = s.CodModelo,
					DatAmarra = s.DatAmarra,
					DesTipoAmarra = contexto.tbl_CBTipoAmarra.Where(x => x.CodTipoAmarra == s.FlgTipo).FirstOrDefault().DesTipoAmarra,
					DipositivoID = s.DipositivoID,
					FlgTipo = s.FlgTipo,
					IsDispositivoIntegrado = s.IsDispositivoIntegrado,
					NumAP = s.NumAP,
					NumECB = s.NumECB,
					NumPosto = s.NumPosto,
					NumSerie = s.NumSerie,
					DescricaoLinha = contexto.tbl_CBLinha.Where(a => a.CodLinha == s.CodLinha).FirstOrDefault().DesLinha,
					DescricaoPosto = contexto.tbl_CBPosto.Where(a => a.CodLinha == s.CodLinha && a.NumPosto == s.NumPosto).FirstOrDefault().DesPosto
				}).ToList();
			}
		}

		public static int RecuperarTurnoAtual(int codLinha)
		{
			int turno = 0;
			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				var resultado = contexto.Database.SqlQuery<int>
								(@"select Hl.turno
									from tbl_CBHorarioLinha Hl
										, tbl_CBHorario h
									where (GETDATE() <= hl.DatFim or hl.DatFim is null)
									and h.idHorario = Hl.idHorario
									and Hl.Turno = h.Turno 
									and Hl.CodLinha = {0}
									and ( CONVERT(VARCHAR(10),GETDATE(),108) >= h.HoraInicio)
									and ( CONVERT(VARCHAR(10),GETDATE(),108) <= h.HoraFim)", codLinha
									).FirstOrDefault<int>();
				turno = resultado;
			}
			return turno;
		}

		public static List<CBEmbaladaDTO> RastrearEmbalagem(string NumSerie)
		{
			using (DTB_CBContext contexto = new DTB_CBContext())
			{
				var lista = contexto.tbl_CBEmbalada.Where(a => a.NumECB == NumSerie).ToList()
					.Select(a => new CBEmbaladaDTO
					{
						CodFab = a.CodFab,
						CodLinha = a.CodLinha,
						CodLinhaFec = a.CodLinhaFec,
						CodModelo = a.CodModelo,
						CodOperador = a.CodOperador,
						CodTestador = a.CodTestador,
						DatLeitura = a.DatLeitura,
						DatReferencia = a.DatReferencia.ToString(),
						NumAP = a.NumAP,
						NumECB = a.NumECB,
						NumLote = a.NumLote,
						NumPosto = a.NumPosto,
						DescricaoLinha = contexto.tbl_CBLinha.Where(b => b.CodLinha == a.CodLinha).FirstOrDefault().DesLinha,
						DescricaoPosto = contexto.tbl_CBPosto.Where(b => b.CodLinha == a.CodLinha && b.NumPosto == a.NumPosto).FirstOrDefault().DesPosto,
						Operador = GetFuncionarioByDRT(a.CodOperador).NomFuncionario
					}).ToList();

				return lista;
			}
		}

		public static CBEmbaladaDTO ObterEmbaladoSerieLinha(string numECB, int codLinha)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				return dtbCB.tbl_CBEmbalada
							.Where(x => x.NumECB == numECB && x.CodLinha == codLinha)
							.Select(x => new CBEmbaladaDTO()
							{
								CodFab = x.CodFab,
								CodLinha = x.CodLinha,
								CodLinhaFec = x.CodLinhaFec,
								CodModelo = x.CodModelo,
								CodOperador = x.CodOperador,
								CodTestador = x.CodTestador,
								DatLeitura = x.DatLeitura,
								NumAP = x.NumAP,
								NumCC = x.NumCC,
								NumECB = x.NumECB,
								NumLote = x.NumLote,
								NumPosto = x.NumPosto
							})
							.FirstOrDefault();
			}
		}

		public static List<ResumoEmbalada> ObterResumoEmbalagem(DateTime datReferencia)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = (from e in contexto.tbl_CBEmbalada
							 where e.DatReferencia == datReferencia
							 group e by new { e.CodModelo, NumECB = e.NumECB.Substring(0, 6), e.DatReferencia } into grupo
							 select new ResumoEmbalada()
							 {
								 CodModelo = grupo.Key.CodModelo,
								 NumECB = grupo.Key.NumECB,
								 DatProducao = grupo.Key.DatReferencia,
								 QtdProduzida = grupo.Count()
							 });

				return dados.ToList();
			}

		}

		public static List<ResumoEmbalada> ObterResumoEmbalagem(DateTime datInicio, DateTime datFim)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = (from e in contexto.tbl_CBEmbalada
							 where e.DatReferencia >= datInicio && e.DatReferencia <= datFim
							 group e by new { e.CodModelo, NumECB = e.NumECB.Substring(0, 6), e.DatReferencia } into grupo
							 select new ResumoEmbalada()
							 {
								 CodModelo = grupo.Key.CodModelo,
								 NumECB = grupo.Key.NumECB,
								 DatProducao = grupo.Key.DatReferencia,
								 QtdProduzida = grupo.Count()
							 });

				return dados.ToList();
			}

		}

		/// <summary>
		/// Fazer o resumo da produção que foi embalada na fase
		/// </summary>
		/// <param name="datReferencia">Data a ser avaliada</param>
		/// <param name="fabrica">Fase de produção (IAC/IMC/FEC)</param>
		/// <returns>Quantidade embaladas por modelo na data</returns>
		public static List<ResumoEmbalada> ObterResumoEmbalagem(DateTime datReferencia, string fabrica)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = (from e in contexto.tbl_CBEmbalada
							 join l in contexto.tbl_CBLinha on e.CodLinha equals l.CodLinha
							 where e.DatReferencia == datReferencia && l.Setor == fabrica
							 group e by new { e.CodModelo, NumECB = e.NumECB.Substring(0, 6), e.DatReferencia } into grupo
							 select new ResumoEmbalada()
							 {
								 CodModelo = grupo.Key.CodModelo,
								 NumECB = grupo.Key.NumECB,
								 DatProducao = grupo.Key.DatReferencia,
								 QtdProduzida = grupo.Count()
							 });

				return dados.ToList();
			}

		}

		public static List<ResumoModelo> ObterResumoModelo(DateTime datInicio, DateTime datFim)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = (from e in contexto.tbl_CBEmbalada
							 join i in contexto.tbl_Item on e.CodModelo equals i.CodItem
							 join l in contexto.tbl_CBLinha on e.CodLinha equals l.CodLinha
							 where e.DatReferencia >= datInicio && e.DatReferencia <= datFim && l.Setor == "FEC"
							 group e by new { e.CodModelo, i.DesItem } into grupo
							 select new ResumoModelo()
							 {
								 CodModelo = grupo.Key.CodModelo,
								 DesModelo = grupo.Key.DesItem.Trim(),
								 Data = datInicio,
								 QtdProduzido = grupo.Count(),
								 Ordem = 1
							 }).ToList();

				var plano = (from pp in contexto.tbl_ProgDiaJit
							 join i in contexto.tbl_Item on pp.CodModelo equals i.CodItem
							 where pp.DatProducao >= datInicio && pp.DatProducao <= datFim
							 group pp by new { pp.CodModelo, i.DesItem } into grupo
							 select new ResumoModelo()
							 {
								 CodModelo = grupo.Key.CodModelo,
								 DesModelo = grupo.Key.DesItem.Trim(),
								 QtdPlanejado = (int)grupo.Sum(x => x.QtdProducao ?? x.QtdProdPm),
								 Ordem = 2
							 }).ToList();

				dados.ForEach(x =>
				{
					var item = plano.FirstOrDefault(p => p.CodModelo == x.CodModelo);
					if (item != null)
					{
						x.QtdPlanejado = (int)item.QtdPlanejado;

						//x.Eficiencia = Math.Round((double)x.QtdProduzido / x.QtdPlanejado, 2);
						x.Eficiencia = (double)x.QtdProduzido / x.QtdPlanejado;
					}
					plano.Remove(item);
				});

				dados.AddRange(plano);

				return dados.OrderBy(x => x.Ordem).ThenBy(x => x.Eficiencia).ToList();
			}

		}

		public static CBEmbaladaDTO ObterUltimoEmbaladoLinha(int codLinha)
		{
			using (var dtbCB = new DTB_CBContext())
			{
				return dtbCB.tbl_CBEmbalada.Select(x => new CBEmbaladaDTO()
				{
					CodFab = x.CodFab,
					CodLinha = x.CodLinha,
					CodLinhaFec = x.CodLinhaFec,
					CodModelo = x.CodModelo,
					CodOperador = x.CodOperador,
					CodTestador = x.CodTestador,
					DatLeitura = x.DatLeitura,
					NumAP = x.NumAP,
					NumCC = x.NumCC,
					NumECB = x.NumECB,
					NumLote = x.NumLote,
					NumPosto = x.NumPosto
				}).OrderByDescending(x => x.DatLeitura).FirstOrDefault(x => x.CodLinha == codLinha);
			}
		}

		public static int ObterIDPostoGargalo(int codLinha)
		{

			using (var dtbCB = new DTB_CBContext())
			{
				var dados = dtbCB.tbl_CBPosto.FirstOrDefault(x => x.CodLinha == codLinha && x.FlgGargalo == "S");


				if (dados != null)
				{
					return dados.NumPosto;
				}
				else
				{
					return 1;
				}

			}

		}

		public static DateTime ObterUltimaDataProducao()
		{

			using (var contexto = new DTB_CBContext())
			{

				var datInicio = DateTime.Now.AddDays(-1);

				var data = contexto.tbl_CBEmbalada.Where(x => x.DatReferencia < datInicio).OrderByDescending(x => x.DatReferencia).FirstOrDefault();

				if (data != null)
				{

					return data.DatReferencia.Value;

				}
				else
				{
					return DateTime.Now;
				}

			}

		}

		public static List<ResumoEmbaladoLinha> ObterResumoEmbaladoLinha(DateTime datInicio, DateTime datFim)
		{


			using (var dtbCB = new DTB_CBContext())
			using (var dtbSIM = new DTB_SIMContext())
			{

				var linhas = dtbCB.tbl_CBLinha.Where(x => x.flgAtivo == "1").Select(x => new ResumoEmbaladoLinha()
				{
					CodLinha = x.CodLinha,
					CodLinhaERP = x.CodLinhaT1,
					DesLinha = x.DesLinha.Trim(),
					Setor = x.Setor
				}).ToList();

				var programaFEC = dtbSIM.tbl_ProgDiaJit.Where(x => x.DatProducao >= datInicio && x.DatProducao <= datFim).ToList();
				var programaPCI = dtbSIM.tbl_ProgDiaPlaca.Where(x => x.DatProducao >= datInicio && x.DatProducao <= datFim).ToList();

				var produzidoFEC = dtbSIM.tbl_ProdDia.Where(x => x.DatProducao >= datInicio && x.DatProducao <= datFim).ToList();
				var produzidoPCI = dtbSIM.tbl_ProdDiaPlaca.Where(x => x.DatProducao >= datInicio && x.DatProducao <= datFim).ToList();

				foreach (var linha in linhas)
				{
					decimal? programa;
					decimal? produzido;
					int? qtdProdutos;

					if (linha.Setor == "FEC")
					{
						programa = programaFEC.Where(x => x.CodLinha == linha.CodLinhaERP).Sum(x => x.QtdProducao ?? x.QtdProdPm);
						produzido = produzidoFEC.Where(x => x.CodLinha == linha.CodLinhaERP).Sum(x => x.QtdProduzida);
						qtdProdutos = programaFEC.Count();
					}
					else
					{
						programa = programaPCI.Where(x => x.CodLinha == linha.CodLinhaERP).Sum(x => x.QtdProducao ?? x.QtdProdPM);
						produzido = produzidoPCI.Where(x => x.CodLinha == linha.CodLinhaERP).Sum(x => x.QtdProduzida);
						qtdProdutos = programaPCI.Count();
					}

					linha.QtdPrograma = programa ?? 0;
					linha.QtdProduzido = produzido ?? 0;
					linha.QtdProdutos = qtdProdutos ?? 0;

				}

				return linhas;
			}

		}

		public static List<VelocidadeLinha> ObterVelocidades()
		{

			using (var contexto = new DTB_CBContext())
			{
				var hora = DateTime.Now.AddMinutes(-10);

				var dados = (from emb in contexto.tbl_CBEmbalada
							 where emb.DatLeitura >= hora
							 group emb by emb.CodLinha into grupo
							 select new VelocidadeLinha()
							 {
								 CodLinha = grupo.Key,
								 QtdProduzido = grupo.Count(),
								 Velocidade = grupo.Count() / 10m
							 }
							).ToList();

				return dados;
			}


		}

		public static List<ResumoContingencia> ObterResumoContingencia(DateTime datInicio, DateTime datFim) 
		{
			try
			{

				using (var contexto = new DTB_CBContext())
				{
					var dados = (from p in contexto.viw_CBPassagemTurno
								join l in contexto.tbl_CBLinha on p.CodLinha equals l.CodLinha
								join posto in contexto.tbl_CBPosto on new { p.CodLinha, p.NumPosto } equals new { posto.CodLinha, posto.NumPosto }
								where l.Setor == "FEC" && l.flgAtivo == "1" && 
									(posto.CodTipoPosto == Constantes.POSTO_AMARRAFINAL || posto.CodTipoPosto == Constantes.POSTO_LEITURA_EXPED) &&
									p.DatEvento >= datInicio  && p.DatEvento <= datFim
								select new { p, posto.CodTipoPosto, l.DesLinha });

					var selecao = dados.GroupBy(x => new { x.p.CodLinha, DatReferencia = x.p.DatReferencia.Value, x.DesLinha }).Select(x => new ResumoContingencia()
					{
						CodLinha = x.Key.CodLinha,
						DesLinha = x.Key.DesLinha,
						Data = x.Key.DatReferencia,
						QtdEmbalado = x.Count(z => z.CodTipoPosto == Constantes.POSTO_AMARRAFINAL),
						QtdSAP = x.Count(z => z.CodTipoPosto == Constantes.POSTO_AMARRAFINAL && z.p.CodDRT.Length < 8),
						QtdSISAP = x.Count(z => z.CodTipoPosto == Constantes.POSTO_AMARRAFINAL && z.p.CodDRT.Length == 8),
						QtdExpedido = x.Count(z => z.CodTipoPosto == Constantes.POSTO_LEITURA_EXPED)
					})
					.OrderBy(x => x.Data)
					.ThenBy(x => x.CodLinha)
					.ToList();

					return selecao;

				}

			}
			catch (Exception)
			{
				return null;
			}
		
		}

		public static List<CBDestinatarioEmailDTO> ObterDestinatariosEmail(string codRotina){

			using (var contexto = new DTB_CBContext())
			{
				var dados = contexto.tbl_CBDestinatarioEmail.Where(x => x.CodRotina == codRotina).ToList();

				return dados.Select(x => new CBDestinatarioEmailDTO() { 
								CodRotina = x.CodRotina,
								EmailDestinatario = x.EmailDestinatario,
								NomDestinatario = x.NomDestinatario
							}).ToList();
			}


		}
		#endregion

		#region Banco Cliente

		public static LinhaDTO ObterLinha(string codLinha)
		{
			using (var dtbCb = new DTB_SIMContext())
			{
				var linha = (from i in dtbCb.tbl_Linha
							 where i.CodLinha == codLinha
							 select new
							 {
								 CodLinha = i.CodLinha,
								 DesDep = i.DesDep,
								 DesLinha = i.DesLinha,
								 Usuario = i.Usuario,
								 FlgTipo = i.FlgTipo,
								 FlgAtivo = i.FlgAtivo,
								 rowguid = i.rowguid
							 }).FirstOrDefault();

				var dado = new LinhaDTO()
				{
					CodLinha = linha.CodLinha,
					DesDep = linha.DesDep,
					DesLinha = linha.DesLinha,
					Usuario = linha.Usuario,
					FlgTipo = linha.FlgTipo,
					FlgAtivo = linha.FlgAtivo,
					rowguid = linha.rowguid,
					rowguid6 = Guid.NewGuid()
				};

				return dado;
			}
		}

		public static FabricaDTO ObterFabrica(string codFab)
		{
			using (var dtbCb = new DTB_SIMContext())
			{
				var fabrica = (from i in dtbCb.tbl_Fabrica
							   where i.CodFab == codFab
							   select i).FirstOrDefault();

				var fabricaDTO = new FabricaDTO();

				AutoMapper.Mapper.Map(fabrica, fabricaDTO);

				return fabricaDTO;
			}
		}

		public static ProgDiaJitDTO ObterPrograma(string codLinha, DateTime datProducao, string codModelo)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var prog = (from p in dtbSim.tbl_ProgDiaJit
							where p.CodLinha == codLinha &&
								  p.DatProducao == datProducao &&
								  p.CodModelo == codModelo
							select p).FirstOrDefault();

				var progDiaJitDTO = new ProgDiaJitDTO();

				AutoMapper.Mapper.Map(prog, progDiaJitDTO);

				return progDiaJitDTO;
			}
		}

		public static List<ProgDiaJitDTO> ObterPrograma(DateTime datInicio, DateTime datFim)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var prog = (from p in dtbSim.tbl_ProgDiaJit
							where p.DatProducao <= datInicio && p.DatProducao <= datFim
							select p).ToList();

				var progDiaJitDTO = new List<ProgDiaJitDTO>();

				AutoMapper.Mapper.Map(prog, progDiaJitDTO);

				return progDiaJitDTO;
			}
		}

		public static ProdDiaDTO ObterProduzido(string codLinha, DateTime datProducao, string codModelo)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var prod = (from p in dtbSim.tbl_ProdDia
							where p.CodLinha == codLinha &&
								  p.DatProducao == datProducao &&
								  p.CodModelo == codModelo
							select p).FirstOrDefault();

				var prodDiaDTO = new ProdDiaDTO();

				AutoMapper.Mapper.Map(prod, prodDiaDTO);

				return prodDiaDTO;
			}
		}

		public static string ObterDescricaoItem(string CodItem)
		{
			if (String.IsNullOrEmpty(CodItem)) return "";

			using (var dtbSim = new DTB_SIMContext())
			{
				var firstOrDefault = (from i in dtbSim.tbl_Item
									  where i.CodItem == CodItem
									  select i.DesItem).FirstOrDefault();

				return firstOrDefault != null ? firstOrDefault.Trim() : "";
			}
		}

		public static string ObterDescResumidaItem(string CodItem)
		{
			if (String.IsNullOrEmpty(CodItem)) return "";

			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var firstOrDefault = (from i in dtbSim.tbl_ProdModelo
									  where i.CodModelo == CodItem
									  select i.DesResumida).FirstOrDefault();

				return firstOrDefault != null ? firstOrDefault.Trim() : "";
			}
		}

		public static ItemDTO ObterItem(String CodItem)
		{
			if (!String.IsNullOrEmpty(CodItem))
			{
				using (DTB_SIMContext dtbSim = new DTB_SIMContext())
				{
					var item = (from i in dtbSim.tbl_Item
								where i.CodItem == CodItem
								select i).FirstOrDefault();

					ItemDTO itemDTO = new ItemDTO();

					AutoMapper.Mapper.Map(item, itemDTO);

					return itemDTO;
				}
			}

			return new ItemDTO();

		}

		public static string ObterDesDepartamento(string codFab, string codDepto)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var dado = dtbSim.tbl_Departamento.FirstOrDefault(x => x.CodDepto.Equals(codDepto) && x.CodFab.Equals(codFab));

				return dado != null ? dado.NomDepto : "";

			}
		}

		public static List<viw_ItemDTO> ObterItens()
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var item = (from i in dtbCb.viw_Item
							select i).ToList();

				var itemDTO = new List<viw_ItemDTO>();

				AutoMapper.Mapper.Map(item, itemDTO);

				return itemDTO;
			}

		}

		/// <summary>
		/// Pesquisa no banco de dados os modelos que iniciam com 9 para tela de pesquisa
		/// </summary>
		/// <returns>Lista de itens (Código e Descrição)</returns>
		public static List<viw_ItemDTO> ObterModelos()
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var item = (from i in dtbCb.viw_Item
							where i.CodItem.StartsWith("9")
							select i).ToList();

				var itemDTO = new List<viw_ItemDTO>();
                
				AutoMapper.Mapper.Map(item, itemDTO);

				return itemDTO;
			}

		}

		/// <summary>
		/// Retorna uma lista de modelos filtrados pela descrição
		/// </summary>
		/// <param name="descricao">Descrição do modelo</param>
		/// <returns>Lista de modelos</returns>
		public static List<viw_ItemDTO> ObterModelos(string descricao, int? pagina = 1)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var item = (from i in dtbCb.viw_Item
							where i.CodItem.StartsWith("9") &&
									(i.DesItem.Contains(descricao) ||
									i.CodItem.Contains(descricao))
							orderby i.CodItem
							select i).Skip((pagina.Value - 1) * 10).Take(10).ToList();


				var itemDTO = new List<viw_ItemDTO>();
                
				AutoMapper.Mapper.Map(item, itemDTO);

				return itemDTO;
			}

		}

		public static List<viw_ItemDTO> ObterModelosPCI(string descricao, int? pagina = 1)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var item = (from i in dtbCb.viw_Item
							where !i.CodItem.StartsWith("9") &&
									i.Origem.StartsWith("2") &&
									(i.DesItem.Contains(descricao) ||
									i.CodItem.Contains(descricao))
							orderby i.CodItem
							select i).Skip((pagina.Value - 1) * 10).Take(10).ToList();


				var itemDTO = new List<viw_ItemDTO>();
                
				AutoMapper.Mapper.Map(item, itemDTO);

				return itemDTO;
			}

		}

		/// <summary>
		/// Pesquisa no banco de dados os modelos a partir da tabela de plano
		/// </summary>
		/// <returns>Lista de itens (Código e Descrição)</returns>
		public static List<ItemDTO> ObterModelosAll()
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var item = (from i in dtbCb.tbl_Item
							join pcp in dtbCb.tbl_ItemPCP on i.CodItem equals pcp.CodItem
							join p in dtbCb.tbl_PlanoItem.Select(x => new { x.CodModelo }).Distinct() on i.CodItem equals p.CodModelo
							where i.FlgAtivoItem == "S" && new[] { "01", "02", "03" }.Contains(pcp.CodProcesso.Substring(0, 2)) && pcp.MovEstoque == "S"
							select i).ToList();

				var itemDTO = new List<ItemDTO>();
                
				AutoMapper.Mapper.Map(item, itemDTO);

				return itemDTO;
			}

		}

		/// <summary>
		/// Pesquisa no banco de dados os modelos a partir da tabela de plano
		/// </summary>
		/// <returns>Lista de itens (Código e Descrição)</returns>
		public static List<viw_ItemDTO> ObterModelosPrograma(string localizar, DateTime data)
		{
			using (var contexto = new DTB_SIMContext())
			{
				//var data = Common.daoUtil.GetDate().AddMonths(-2);

				var item = (from i in contexto.tbl_Item
							join p in contexto.tbl_ProgDiaJit.Where(p => p.DatProducao == data).Select(x => new { x.CodModelo }).Distinct() on i.CodItem equals p.CodModelo
							where i.DesItem.Contains(localizar)
							select new viw_ItemDTO()
							{
								CodItem = i.CodItem,
								DesItem = i.DesItem.Trim()
							}).ToList();

				return item;
			}

		}

		public static List<viw_ItemDTO> ObterModelosProgramaMes(string localizar, DateTime data)
		{
			using (var contexto = new DTB_SIMContext())
			{
				var datIni = data.AddDays(-data.Day).AddDays(1);
				var datFim = datIni.AddMonths(1).AddDays(-1);

				localizar = localizar ?? "";

				var item = (from i in contexto.tbl_Item
							join p in contexto.tbl_ProgDiaJit.Where(p => p.DatProducao >= datIni /*&& p.DatProducao <= datFim*/).Select(x => new { x.CodModelo }).Distinct() on i.CodItem equals p.CodModelo
							where i.DesItem.Contains(localizar) || i.CodItem.Contains(localizar)
							select new viw_ItemDTO()
							{
								CodItem = i.CodItem,
								DesItem = i.DesItem.Trim()
							}).ToList();

				var itemPCI = (from i in contexto.tbl_Item
							   join p in contexto.tbl_ProgDiaPlaca.Where(p => p.DatProducao >= datIni /*&& p.DatProducao <= datFim*/).Select(x => new { x.CodPlaca }).Distinct() on i.CodItem equals p.CodPlaca
							   where i.DesItem.Contains(localizar) || i.CodItem.Contains(localizar)
							   select new viw_ItemDTO()
							   {
								   CodItem = i.CodItem,
								   DesItem = i.DesItem.Trim()
							   }).ToList();

				var itemProdDia = (from i in contexto.tbl_Item
								   join p in contexto.tbl_ProdDia.Where(p => p.DatProducao >= datIni /*&& p.DatProducao <= datFim*/).Select(x => new { x.CodModelo }).Distinct() on i.CodItem equals p.CodModelo
								   where i.DesItem.Contains(localizar) || i.CodItem.Contains(localizar)
								   select new viw_ItemDTO()
								   {
									   CodItem = i.CodItem,
									   DesItem = i.DesItem.Trim()
								   }).ToList();

				var itemProdDiaPlaca = (from i in contexto.tbl_Item
									   join p in contexto.tbl_ProdDiaPlaca.Where(p => p.DatProducao >= datIni /*&& p.DatProducao <= datFim*/).Select(x => new { x.CodPlaca }).Distinct() on i.CodItem equals p.CodPlaca
									   where i.DesItem.Contains(localizar) || i.CodItem.Contains(localizar)
									   select new viw_ItemDTO()
									   {
										   CodItem = i.CodItem,
										   DesItem = i.DesItem.Trim()
									   }).ToList();

				item.AddRange(itemPCI);

				item.AddRange(itemProdDia.Where(x => !item.Contains(x)).ToList());
				
				item.AddRange(itemProdDiaPlaca.Where(x => !item.Contains(x)).ToList());
				
				return item;
			}

		}

		public static String ObterFabricaPorLinha(int CodLinha)
		{
			try
			{
				using (DTB_CBContext dtbCb = new DTB_CBContext())
				{
					var Fabrica = (from i in dtbCb.tbl_CBLinha
								   where i.CodLinha == CodLinha
								   select i.Setor).FirstOrDefault();
					return Fabrica;
				}
			}
			catch
			{
				return "";
			}
		}

		public static String ObterCodModeloIAC(string CodModelo)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var modelo = (from p in dtbSim.tbl_PlanoItem
							  join i in dtbSim.tbl_ItemPCP on p.CodItem equals i.CodItem
							  where p.CodModelo == CodModelo && i.CodProcesso.StartsWith("01") && i.MovEstoque == "S"
							  select i.CodItem).FirstOrDefault();

				return modelo;
			}
		}

		public static String ObterCodModeloSMD(string CodModelo)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var modelo = (from p in dtbSim.tbl_PlanoItem
							  join i in dtbSim.tbl_ItemPCP on p.CodItem equals i.CodItem
							  where p.CodModelo == CodModelo && i.CodProcesso.StartsWith("01") && i.MovEstoque == "S"
							  select i.CodItem).FirstOrDefault();

				return modelo;
			}
		}

		public static String ObterCodModeloIMC(string CodModelo)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var modelo = (from p in dtbSim.tbl_PlanoItem
							  join i in dtbSim.tbl_ItemPCP on p.CodItem equals i.CodItem
							  where p.CodModelo == CodModelo && i.CodProcesso.StartsWith("02") && i.MovEstoque == "S"
							  select i.CodItem).FirstOrDefault();

				return modelo;
			}
		}

		public static String ObterCodModeloIAC_IMC(string CodModelo)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var modelo = (from p in dtbSim.tbl_PlanoItem
							  join i in dtbSim.tbl_ItemPCP on p.CodModelo equals i.CodItem
							  where p.CodItem == CodModelo && i.CodProcesso.StartsWith("02") && i.MovEstoque == "S"
							  select i.CodItem).FirstOrDefault();

				return modelo;
			}
		}

		public static String ObterCodModeloIAC_IAC(string CodModelo)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var modelo = (from p in dtbSim.tbl_PlanoItem
							  join i in dtbSim.tbl_ItemPCP on p.CodModelo equals i.CodItem
							  where p.CodItem == CodModelo && i.CodProcesso.StartsWith("01") && i.MovEstoque == "S"
							  select i.CodItem).FirstOrDefault();

				return modelo;
			}
		}

		public static string ObterCodigoModelo(string codFabrica, string numeroAP)
		{
			try
			{
				using (DTB_SIMContext dtbSim = new DTB_SIMContext())
				{
					string cod = dtbSim.tbl_LSAAP.FirstOrDefault(a => a.CodFab == codFabrica && a.NumAP == numeroAP)
							.CodModelo;
					return cod;
				}
			}
			catch
			{
				throw new Exception("Modelo não encontrado para AP informada.");
			}
		}

		public static string ValidarAP(string codFabrica, string numeroAP, DateTime dataReferencia)
		{
			try
			{
				using (DTB_SIMContext contexto = new DTB_SIMContext())
				{
					int fabrica = int.Parse(codFabrica);
					var ap = numeroAP.Trim();

					var resultado = contexto.Database.SqlQuery<spc_Coleta001>(
							"EXEC spc_Coleta001 @CodFab, @NumAP, @DatReferencia",
							new SqlParameter("CodFab", fabrica.ToString("00")),
							new SqlParameter("NumAP", String.Format("{000000:0}", ap)),
							new SqlParameter("DatReferencia", dataReferencia)
							).ToList<spc_Coleta001>();

					contexto.SaveChanges();

					var query = resultado.FirstOrDefault();

					if (query != null)
					{
						return string.Empty;
					}
					else
					{
						return "Ap informada é inválida.";
					}
				}
			}
			catch (Exception ex)
			{
				return ex.Message;
			}
		}

		public static string RecuperarInformacoes_ValidarAP(string codFabrica, string numeroAP, DateTime dataReferencia)
		{
			try
			{
				using (DTB_SIMContext contexto = new DTB_SIMContext())
				{
					int fabrica = int.Parse(codFabrica);
					string ap = numeroAP;

					var resultado = contexto.Database.SqlQuery<spc_Coleta001>(
							"EXEC spc_Coleta001 @CodFab, @NumAP, @DatReferencia",
							new SqlParameter("CodFab", fabrica.ToString("00")),
							new SqlParameter("NumAP", ap),
							new SqlParameter("DatReferencia", dataReferencia.ToString("yyyy-MM-dd"))
							).ToList<spc_Coleta001>();

					contexto.SaveChanges();

					var query = resultado.FirstOrDefault();

					if (query != null)
					{
						return query.CodCin;
					}
					else
					{
						return "";
					}
				}
			}
			catch
			{
				return "";
			}
		}

		public static RIFuncionarioDTO GetFuncionarioByDRT(string codDRT)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var func = dtbSim.tbl_RIFuncionario.Where(a => a.CodDRT == codDRT).FirstOrDefault();

				var funcDTO = new RIFuncionarioDTO();

				AutoMapper.Mapper.Map(func, funcDTO);

				return funcDTO;
			}
		}

		public static int ObterTotalAP(string CodFab, string NumAP)
		{
			if (NumAP == null) return 0;

			using (DTB_SIMContext contexto = new DTB_SIMContext())
			{
				contexto.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

				var ap = contexto.tbl_LSAAP.Where(x => x.CodFab == CodFab && x.NumAP == NumAP).FirstOrDefault();

				if (ap != null && ap.QtdLoteAP.HasValue)
					return ap.QtdLoteAP.GetValueOrDefault();
				else
					return 0;
			}
		}

		public static LSAAPDTO ObterAP(string CodFab, string NumAP)
		{
			if (NumAP == null) return new LSAAPDTO();

			using (DTB_SIMContext contexto = new DTB_SIMContext())
			{
				contexto.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

				var ap = contexto.tbl_LSAAP.Where(x => x.CodFab == CodFab && x.NumAP == NumAP).FirstOrDefault();

				if (ap != null)
				{
					var lsaDTO = new LSAAPDTO();

					AutoMapper.Mapper.Map(ap, lsaDTO);

					return lsaDTO;
				}
				else
					return new LSAAPDTO();
			}
		}

		public static LSAAPDTO ObterAP(string NumOP)
		{
			if (NumOP == null) return new LSAAPDTO();

			using (DTB_SIMContext contexto = new DTB_SIMContext())
			{
				contexto.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

				var ap = contexto.tbl_LSAAP.Where(x => x.NumOP == NumOP).FirstOrDefault();

				if (ap != null)
				{
					var lsaDTO = new LSAAPDTO();

					AutoMapper.Mapper.Map(ap, lsaDTO);

					return lsaDTO;
				}
				else
					return new LSAAPDTO();
			}
		}

		public static List<LSAAPDTO> ObterAPModeloData(string codModelo, DateTime datReferencia)
		{
			if (codModelo == null) return new List<LSAAPDTO>();

			using (DTB_SIMContext contexto = new DTB_SIMContext())
			{
				contexto.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

				var ap = contexto.tbl_LSAAP.Where(x => x.CodModelo == codModelo && x.DatReferencia == datReferencia).FirstOrDefault();

				if (ap != null)
				{
					var lsaDTO = new List<LSAAPDTO>();

					AutoMapper.Mapper.Map(ap, lsaDTO);

					return lsaDTO;
				}
				else
					return new List<LSAAPDTO>();
			}
		}

		public static List<LSAAPItemDTO> ObterAPItem(string NumOP)
		{
			if (NumOP == null) return new List<LSAAPItemDTO>();

			using (DTB_SIMContext contexto = new DTB_SIMContext())
			{
				contexto.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

				var ap = contexto
							.tbl_LSAAPItem
							.Join(contexto.tbl_Item, apItem => apItem.CodItemAlt, item => item.CodItem, (apItem, item) => new { apItem, item })
							.Where(x => x.apItem.NumOP == NumOP)
							.Select(y => new LSAAPItemDTO() {
								CodAplAlt = y.apItem.CodAplAlt,
								CodItem = y.apItem.CodItem,
								CodItemAlt = y.apItem.CodItemAlt,
								CodFab = y.apItem.CodFab,
								CodIdentificaItem = y.apItem.CodIdentificaItem,
								CodIdentificaRej = y.apItem.CodIdentificaRej,
								CodModelo = y.apItem.CodModelo,
								CodProcesso = y.apItem.CodProcesso,
								CodProcesso2 = y.apItem.CodProcesso2,
								DesAlt = y.item.DesItem,
								FlgApontado = y.apItem.FlgApontado,
								FlgItemOK = y.apItem.FlgItemOK,
								NumAP = y.apItem.NumAP,
								NumNa = y.apItem.NumNa,
								NumOP = y.apItem.NumOP,
								OrdConsumo = y.apItem.OrdConsumo,
								QtdDevolucao = y.apItem.QtdDevolucao,
								QtdEmpenho = y.apItem.QtdEmpenho,
								QtdEmpenho2 = y.apItem.QtdEmpenho2,
								QtdFrequencia = y.apItem.QtdFrequencia,
								QtdNecessaria = y.apItem.QtdNecessaria,
								QtdPaga = y.apItem.QtdPaga,
								QtdProduzida = y.apItem.QtdProduzida,
								QtdRecebida = y.apItem.QtdRecebida,
								QtdRejeito = y.apItem.QtdRejeito,
								QtdSaldoProd = y.apItem.QtdSaldoProd,
								rowguid = y.apItem.rowguid
							})
							.ToList();


				return ap;
			}
		}

		public static UsuarioDTO ObterUsuario(string codFab, string nomUsuario)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				return dtbSim.tbl_Usuario.Where(x => x.NomUsuario == nomUsuario && x.CodFab == codFab).Select(x => new UsuarioDTO()
				{
					CodFab = x.CodFab,
					Cpf = x.Cpf,
					Email = x.Email,
					FlgAtivo = x.FlgAtivo,
					FullName = x.FullName,
					NomDepto = x.NomDepto,
					NomDeptoAD = x.NomDeptoAD,
					NomUsuario = x.NomUsuario
				}).FirstOrDefault();

			}
		}

		/// <summary>
		/// Procedimento para enviar e-mail através do banco de dados
		/// </summary>
		/// <param name="de"></param>
		/// <param name="deNome"></param>
		/// <param name="para"></param>
		/// <param name="assunto"></param>
		/// <param name="mensagem"></param>
		/// <param name="cc"></param>
		/// <returns></returns>
		public static bool EnviarEmail(string de, string deNome, string para, string assunto, string mensagem, string cc = "")
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				try
				{
					/*var resumo = dtbSim.Database.ExecuteSqlCommand(
					//var resumo = dtbSim.Database.SqlQuery<>(
								"EXEC master.dbo.xp_smtp_sendmail @FROM, @FROM_NAME, @TO, @CC, @subject, @message, @type, @server",
								new SqlParameter("FROM", de),
								new SqlParameter("FROM_NAME", deNome),
								new SqlParameter("TO", para),
								new SqlParameter("CC", cc),
								new SqlParameter("subject", assunto),
								new SqlParameter("message", mensagem),
								new SqlParameter("type", "text/html"),
								new SqlParameter("server", "smtp.semptoshiba.mao")
								);*/
					var resumo = dtbSim.Database.ExecuteSqlCommand(
								"EXEC spc_BarrasEmail @De, @DeNome, @Para, @Assunto, @Conteudo, @CC, @flgEmitido, @flgHTML",
								new SqlParameter("De", de),
								new SqlParameter("DeNome", deNome),
								new SqlParameter("Para", para),
								new SqlParameter("Assunto", assunto),
								new SqlParameter("Conteudo", mensagem),
								new SqlParameter("CC", cc),
								new SqlParameter("flgEmitido", "N"),
								new SqlParameter("flgHTML", "N")
								);

					return true;
				}
				catch
				{
					return false;
				}

			}
		}

		/// <summary>
		/// Procedimento para enviar e-mail através do banco de dados
		/// </summary>
		/// <param name="de"></param>
		/// <param name="deNome"></param>
		/// <param name="para"></param>
		/// <param name="assunto"></param>
		/// <param name="mensagem"></param>
		/// <param name="cc"></param>
		/// <returns></returns>
		public static string EnviarEmail(string de, string deNome, string para, string assunto, string mensagem, string cc = "", bool retornaErro = true)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				try
				{
					var pMensagem = new SqlParameter("Conteudo", mensagem);
					pMensagem.SqlDbType = SqlDbType.VarChar;

					var resumo = dtbSim.Database.ExecuteSqlCommand(
								"EXEC spc_BarrasEmail @De, @DeNome, @Para, @Assunto, @Conteudo, @CC, @flgEmitido, @flgHTML",
								new SqlParameter("De", de),
								new SqlParameter("DeNome", deNome),
								new SqlParameter("Para", para),
								new SqlParameter("Assunto", assunto),
								pMensagem,
								new SqlParameter("CC", cc),
								new SqlParameter("flgEmitido", "N"),
								new SqlParameter("flgHTML", "N")
								);

					return "";
				}
				catch (Exception ex)
				{
					return ex.Message;
				}

			}
		}

		public static string VerificarAPDestino(string codFab, string numAP)
		{
			using (DTB_SIMContext contexto = new DTB_SIMContext())
			{
				var ap = contexto.tbl_LSAAP.Where(x => x.CodFab == codFab && x.NumAP == numAP).FirstOrDefault();

				if (ap != null)
					return ap.NumAPDestino;
				else
					return "";
			}
		}

		/// <summary>
		/// Metodo utilizado para verificar quais Aps disponivel na producao atraves do modelo/NE
		/// </summary>
		/// <param name="codModelo"></param>
		/// <returns></returns>
		public static List<ClassesAuxiliaresEmbalagem.ResultProcGetApsModelo> GetApsModelo(string codProcesso, string codModelo)
		{
			var modelo = String.IsNullOrWhiteSpace(codModelo) ? "" : codModelo;
			var processo = String.IsNullOrWhiteSpace(codModelo) ? "" : codProcesso;

			using (var contexto = new DTB_SIMContext())
			{
				var resultado = contexto.Database.SqlQuery<ClassesAuxiliaresEmbalagem.ResultProcGetApsModelo>(
							"EXEC spc_SISAP_ObterAPsModelo  @CodModelo, @CodProcesso",
							new SqlParameter("CodModelo", modelo),
							new SqlParameter("CodProcesso", processo)
							).ToList();

				contexto.SaveChanges();


				return resultado;
			}

		}


		public static List<ItensRastreaveisAP> ObterItensRastreaveis(string codFab, string numAP)
		{

			using (var contexto = new DTB_SIMContext())
			{

				var dadosap = (from ap in contexto.tbl_LSAAPItem
							   join item in contexto.tbl_Item on ap.CodItemAlt equals item.CodItem
							   join familia in contexto.tbl_Familia on item.CodFam equals familia.CodFam
							   where ap.CodFab == codFab && ap.NumAP == numAP && familia.FlgRastreavel == 1
							   orderby familia.NomFam
							   select new ItensRastreaveisAP()
							   {
								   CodFab = ap.CodFab,
								   NumAP = ap.NumAP,
								   CodModelo = ap.CodModelo,
								   CodItem = ap.CodItem,
								   CodItemAlt = ap.CodItemAlt,
								   DesItemAlt = item.DesItem.Trim(),
								   QtdFrequencia = ap.QtdFrequencia,
								   QtdNecessaria = ap.QtdNecessaria,
								   QtdProduzida = ap.QtdProduzida ?? 0,
								   CodFam = item.CodFam,
								   NomFam = familia.NomFam.Trim(),
								   Origem = item.Origem
							   }).ToList();

				return dadosap;

			}

		}

		public static int ObterQuantidadePontos(string CodModelo)
		{

			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var datHoje = DateTime.Now;

				var itens = dtbSim.spc_BarrasQtdPontos(CodModelo, datHoje);

				return itens.FirstOrDefault();
			}
		}

		public static BarrasProducaoDTO ObterProdutoMilhao()
		{

			using (var contexto = new DTB_SIMContext())
			{
				var milhao = contexto.tbl_BarrasProducao.Where(x => x.DatExpedicao != null).OrderByDescending(x => x.DatLeitura).FirstOrDefault();

				var milhaoDTO = new BarrasProducaoDTO();

				AutoMapper.Mapper.Map(milhao, milhaoDTO);


				if (milhao != null)
				{
					return milhaoDTO;
				}
				else
				{
					return new BarrasProducaoDTO();
				}

			}

		}

		public static List<LinhaDTO> ObterLinhas(string descricao, int? pagina = 1)
		{
			using (var contexto = new DTB_SIMContext())
			{
				var item = (from i in contexto.tbl_Linha
							where (i.DesLinha.Contains(descricao) ||
									i.CodLinha.Contains(descricao))
							orderby i.CodLinha
							select i).Skip((pagina.Value - 1) * 10).Take(10).ToList();


				var itemDTO = new List<LinhaDTO>();

				AutoMapper.Mapper.Map(item, itemDTO);

				return itemDTO;
			}

		}

		public static List<CBLinhaDTO> ObterLinhasCB()
		{
			using (var contexto = new DTB_CBContext())
			{
				var item = (from i in contexto.tbl_CBLinha
							orderby i.CodLinha
							select i).ToList();


				var itemDTO = new List<CBLinhaDTO>();

				AutoMapper.Mapper.Map(item, itemDTO);

				return itemDTO;
			}

		}

		#endregion

		#region AcessoMAO

		public static List<AcessoMaoDTO> ObterAcessos() {

			using (var contexto = new DTB_SIMContext())
			{
				var dados = contexto.tbl_AcessoMao.ToList()
					.Select(x => new AcessoMaoDTO()
					{
						Acesso = x.Acesso,
						Codigo = x.Codigo,
						CodigoPai = x.CodigoPai,
						rowguid = x.rowguid,
						Sistema = x.Sistema
					}).ToList();

				return dados;
			}
		}

		public static List<AcessoMaoDTO> ObterAcessosCodigo(string codigo)
		{

			using (var contexto = new DTB_SIMContext())
			{
				var dados = contexto.tbl_AcessoMao.Where(x => x.Codigo == codigo).ToList()
					.Select(x => new AcessoMaoDTO()
					{
						Acesso = x.Acesso,
						Codigo = x.Codigo,
						CodigoPai = x.CodigoPai,
						rowguid = x.rowguid,
						Sistema = x.Sistema
					}).ToList();

				return dados;
			}
		}

		public static List<AcessoMaoDTO> ObterAcessosAcesso(string acesso)
		{

			using (var contexto = new DTB_SIMContext())
			{
				var dados = contexto.tbl_AcessoMao.Where(x => x.Acesso.Contains(acesso)).ToList()
					.Select(x => new AcessoMaoDTO()
					{
						Acesso = x.Acesso,
						Codigo = x.Codigo,
						CodigoPai = x.CodigoPai,
						rowguid = x.rowguid,
						Sistema = x.Sistema
					}).ToList();

				return dados;
			}
		}

		public static bool ObterAcessosBarApontamento()
		{

			using (var contexto = new DTB_SIMContext())
			{
				var dados = contexto.tbl_AcessoMao.Where(x => x.Codigo == "BAREnviaApontamento").FirstOrDefault();
				if (dados != null)
				{
					return (dados.Acesso != null && dados.Acesso == "True");
				}
				else
				{
					return false;
				}


			}
		}

		public static bool SetarAcessosBarApontamento(string acesso)
		{

			using (var contexto = new DTB_SIMContext())
			{
				var dados = contexto.tbl_AcessoMao.Where(x => x.Codigo == "BAREnviaApontamento").FirstOrDefault();
				if (dados != null)
				{

					dados.Acesso = acesso;
					contexto.SaveChanges();

					return true;
				}
				else
				{
					return false;
				}


			}
		}

		#endregion
	}
}
