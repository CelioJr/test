﻿using System;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
	public class PassagemSerie
	{
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public string NumSerie { get; set; }
		public int CodLinha { get; set; }
		public int NumPosto { get; set; }
		public String DatLeitura { get; set; }

		public String SoSerie
		{
			get
			{
				return String.IsNullOrEmpty(NumSerie) ? "" : NumSerie.Substring(NumSerie.Length-8);
			}
		}
	}
}
