using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.DTB_SSA.Models.Mapping
{
    public class tbl_MFG_Build_dMap : EntityTypeConfiguration<tbl_MFG_Build_d>
    {
        public tbl_MFG_Build_dMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Model_Number, t.Serial_Number });

            // Properties
            this.Property(t => t.Model_Number)
                .IsRequired()
                .HasMaxLength(15);

            this.Property(t => t.Serial_Number)
                .IsRequired()
                .HasMaxLength(15);

            this.Property(t => t.MFG_Country_Code)
                .HasMaxLength(2);

            this.Property(t => t.Sold_Country_Code)
                .HasMaxLength(2);

            this.Property(t => t.Asset_Tag_Number)
                .HasMaxLength(15);

            this.Property(t => t.Production_Date)
                .HasMaxLength(8);

            this.Property(t => t.Ship_Date)
                .HasMaxLength(8);

            this.Property(t => t.Sales_Out_Date)
                .HasMaxLength(8);

            // Table & Column Mappings
            this.ToTable("tbl_MFG_Build_d");
            this.Property(t => t.Model_Number).HasColumnName("Model_Number");
            this.Property(t => t.Serial_Number).HasColumnName("Serial_Number");
            this.Property(t => t.MFG_Country_Code).HasColumnName("MFG_Country_Code");
            this.Property(t => t.Sold_Country_Code).HasColumnName("Sold_Country_Code");
            this.Property(t => t.Asset_Tag_Number).HasColumnName("Asset_Tag_Number");
            this.Property(t => t.Production_Date).HasColumnName("Production_Date");
            this.Property(t => t.Ship_Date).HasColumnName("Ship_Date");
            this.Property(t => t.Sales_Out_Date).HasColumnName("Sales_Out_Date");
            this.Property(t => t.Data_Registro).HasColumnName("Data_Registro");
        }
    }
}
