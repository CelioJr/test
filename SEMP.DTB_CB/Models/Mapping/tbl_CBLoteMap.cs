using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DTB_CB.Models.Mapping
{
    public class tbl_CBLoteMap : EntityTypeConfiguration<tbl_CBLote>
    {
        public tbl_CBLoteMap()
        {
            // Primary Key
            this.HasKey(t => t.NumLote);

            // Properties
            this.Property(t => t.NumLote)
                .IsRequired()
                .HasMaxLength(18);

            this.Property(t => t.Status)
                .IsFixedLength()
                .HasMaxLength(10);

            //this.Property(t => t.CodDRTFechamento)
            //    .HasMaxLength(8);

            //this.Property(t => t.JustFechamento)
            //    .HasMaxLength(200);

            //this.Property(t => t.CodDRTRecebimento)
            //    .HasMaxLength(8);

            //this.Property(t => t.CodModelo)
            //    .IsRequired()
            //    .IsFixedLength()
            //    .HasMaxLength(6);

            //this.Property(t => t.CodFab)
            //    .IsFixedLength()
            //    .HasMaxLength(2);

            //this.Property(t => t.NumAP)
            //    .IsFixedLength()
            //    .HasMaxLength(6);

            // Table & Column Mappings
            this.ToTable("tbl_CBLote");
            this.Property(t => t.NumLote).HasColumnName("NumLote");
            this.Property(t => t.Qtde).HasColumnName("Qtde");
            this.Property(t => t.DatAbertura).HasColumnName("DatAbertura");
            this.Property(t => t.DatFechamento).HasColumnName("DatFechamento");
            this.Property(t => t.Status).HasColumnName("Status");
            //this.Property(t => t.DatTransferencia).HasColumnName("DatTransferencia");
            //this.Property(t => t.DatRecebimento).HasColumnName("DatRecebimento");
            //this.Property(t => t.CodDRTFechamento).HasColumnName("CodDRTFechamento");
            //this.Property(t => t.JustFechamento).HasColumnName("JustFechamento");
            //this.Property(t => t.CodDRTRecebimento).HasColumnName("CodDRTRecebimento");
            //this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            //this.Property(t => t.CodFab).HasColumnName("CodFab");
            //this.Property(t => t.NumAP).HasColumnName("NumAP");
            //this.Property(t => t.TamLote).HasColumnName("TamLote");
        }
    }
}
