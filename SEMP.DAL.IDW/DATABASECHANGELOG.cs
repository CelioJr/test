namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DATABASECHANGELOG")]
    public partial class DATABASECHANGELOG
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(63)]
        public string ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(63)]
        public string AUTHOR { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(200)]
        public string FILENAME { get; set; }

        public DateTime DATEEXECUTED { get; set; }

        public int ORDEREXECUTED { get; set; }

        [Required]
        [StringLength(10)]
        public string EXECTYPE { get; set; }

        [StringLength(35)]
        public string MD5SUM { get; set; }

        [StringLength(255)]
        public string DESCRIPTION { get; set; }

        [StringLength(255)]
        public string COMMENTS { get; set; }

        [StringLength(255)]
        public string TAG { get; set; }

        [StringLength(20)]
        public string LIQUIBASE { get; set; }
    }
}
