using System;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBHistoricoRetPosto
    {
        public int codLinha { get; set; }
        public int numPosto { get; set; }
        public System.DateTime DatAtivacao { get; set; }
        public int CodOpeAtivacao { get; set; }
        public DateTime? DatDesativacao { get; set; }
        public int? CodOpeDesativacao { get; set; }
    }
}
