using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_RMAMap : EntityTypeConfiguration<tbl_RMA>
    {
        public tbl_RMAMap()
        {
            // Primary Key
            this.HasKey(t => t.CodRMA);

            // Properties
            this.Property(t => t.CodUser)
                .HasMaxLength(50);

            this.Property(t => t.CodStatus)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.NotaFiscal)
                .IsFixedLength()
                .HasMaxLength(9);

            this.Property(t => t.NumGrim)
                .IsFixedLength()
                .HasMaxLength(6);

            this.Property(t => t.CodFab)
                .IsFixedLength()
                .HasMaxLength(2);

            // Table & Column Mappings
            this.ToTable("tbl_RMA");
            this.Property(t => t.CodRMA).HasColumnName("CodRMA");
            this.Property(t => t.DatReg).HasColumnName("DatReg");
            this.Property(t => t.CodUser).HasColumnName("CodUser");
            this.Property(t => t.CodStatus).HasColumnName("CodStatus");
            this.Property(t => t.NotaFiscal).HasColumnName("NotaFiscal");
            this.Property(t => t.NumGrim).HasColumnName("NumGrim");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
        }
    }
}
