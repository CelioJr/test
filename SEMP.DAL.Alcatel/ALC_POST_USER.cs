namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_POST_USER")]
    public partial class ALC_POST_USER
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ALC_POST_USER()
        {
            ALC_SYS_USER = new HashSet<ALC_SYS_USER>();
            ALC_SYS_USER1 = new HashSet<ALC_SYS_USER>();
            ALC_REPAIR = new HashSet<ALC_REPAIR>();
        }

        [Key]
        public int idPost { get; set; }

        [StringLength(255)]
        public string description { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_SYS_USER> ALC_SYS_USER { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_SYS_USER> ALC_SYS_USER1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_REPAIR> ALC_REPAIR { get; set; }
    }
}
