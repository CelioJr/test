namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tt_tmg_con
    {
        [Key]
        public long id_tmgcon { get; set; }

        [StringLength(30)]
        public string conhecimento { get; set; }

        [StringLength(30)]
        public string globalcode { get; set; }

        public decimal? qt_material { get; set; }

        public DateTime? dt_prevista_entrada { get; set; }

        public short? is_transito { get; set; }

        public DateTime? dthr_referencia { get; set; }

        public short? is_importado { get; set; }

        [StringLength(256)]
        public string ds_erro { get; set; }

        [StringLength(30)]
        public string centro { get; set; }
    }
}
