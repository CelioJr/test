﻿using System.Collections.Generic;
using System.Linq;
using SEMP.DAL.Models;
using SEMP.Model.DTO;

namespace SEMP.DAL.DAO.PM
{
	public class daoMaodeObra
	{

		/// <summary>
		/// Rotina para buscar no banco de dados os registros cadastrados
		/// </summary>
		/// <returns></returns>
		public static List<CapacMdoLinhaDTO> ObterLinhas()
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from c in dtbSim.tbl_CapacMdoLinha
								 join l in dtbSim.tbl_Linha on c.CodLinha equals l.CodLinha
								 select new CapacMdoLinhaDTO()
								 {
									 CodLinha = c.CodLinha,
									 DatFim = c.DatFim,
									 DatInicio = c.DatInicio,
									 NumTurno = c.NumTurno,
									 Quantidade = c.Quantidade,
									 DesLinha = l.DesLinha
								 });

				return resultado.ToList();
			}
		}

		/// <summary>
		/// Rotina para buscar no banco de dados um determinado registro
		/// </summary>
		/// <param name="codLinha"></param>
		/// <returns></returns>
		public static CapacMdoLinhaDTO ObterLinha(string codLinha)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var resultado = (from c in dtbSim.tbl_CapacMdoLinha
								join l in dtbSim.tbl_Linha on c.CodLinha equals l.CodLinha
								 where c.CodLinha.Equals(codLinha)
								 select new CapacMdoLinhaDTO()
								 {
									 CodLinha = c.CodLinha,
									 DesLinha = l.DesLinha,
									 DatInicio = c.DatInicio,
									 DatFim = c.DatFim,
									 NumTurno = c.NumTurno,
									 Quantidade = c.Quantidade
								 }).FirstOrDefault();

				return resultado;

			}
		}

		/// <summary>
		/// Rotina de manuntenção de dados, para inclusão e alteração
		/// </summary>
		/// <param name="linha"></param>
		public static void GravarLinha(CapacMdoLinhaDTO linha)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var existe =
					dtbSim.tbl_CapacMdoLinha.FirstOrDefault(
						x => x.CodLinha.Equals(linha.CodLinha) &&
							x.NumTurno == linha.NumTurno
						);

				if (existe != null)
				{
					existe.DatInicio = linha.DatInicio;
					existe.DatFim = linha.DatFim;
					existe.Quantidade = linha.Quantidade;

					dtbSim.SaveChanges();
				}
				else
				{
					var nlinha = new tbl_CapacMdoLinha()
					{
						CodLinha = linha.CodLinha,
						DatInicio = linha.DatInicio,
						DatFim = linha.DatFim,
						NumTurno = linha.NumTurno,
						Quantidade = linha.Quantidade
					};

					dtbSim.tbl_CapacMdoLinha.Add(nlinha);
					dtbSim.SaveChanges();
				}
			}
		}

		/// <summary>
		/// Rotina para excluir um registro do banco de dados
		/// </summary>
		/// <param name="codLinha"></param>
		/// <param name="numTurno"></param>
		public static void RemoverLinha(string codLinha, int numTurno)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				var existe =
					dtbSim.tbl_CapacMdoLinha.FirstOrDefault(
						x => x.CodLinha.Equals(codLinha) && x.NumTurno == numTurno);

				if (existe == null) return;

				dtbSim.tbl_CapacMdoLinha.Remove(existe);
				dtbSim.SaveChanges();
			}
		}

	}
}
