namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_proreausr
    {
        [Key]
        public long id_proreausr { get; set; }

        public long? id_prorea { get; set; }

        public long? id_usr { get; set; }

        public virtual dw_prorea dw_prorea { get; set; }

        public virtual om_usr om_usr { get; set; }
    }
}
