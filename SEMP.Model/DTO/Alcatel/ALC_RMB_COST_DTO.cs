using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_RMB_COST_DTO
    {
        public ALC_RMB_COST_DTO()
        {
            this.ALC_RMB_REGISTER = new List<ALC_RMB_REGISTER_DTO>();
        }

        public decimal COST_ID { get; set; }
        public Nullable<int> COST_MONTH { get; set; }
        public Nullable<double> COST_VALUE { get; set; }
        public Nullable<int> COST_YEAR { get; set; }
        public string PART_NUMBER { get; set; }
        public virtual ICollection<ALC_RMB_REGISTER_DTO> ALC_RMB_REGISTER { get; set; }
        public virtual ALC_RMB_PART_NUMBER_DTO ALC_RMB_PART_NUMBER { get; set; }
    }
}
