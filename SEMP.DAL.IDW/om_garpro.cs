namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_garpro
    {
        [Key]
        public long id_garpro { get; set; }

        public long id_garantia { get; set; }

        public long id_produto { get; set; }

        public DateTime? dthr_iniciovalidade { get; set; }

        public DateTime? dthr_fimvalidade { get; set; }

        [StringLength(30)]
        public string ns { get; set; }

        public short? tp_licenca { get; set; }

        public decimal? qt_licencasvendidas { get; set; }

        public decimal? qt_licencascontratomanu { get; set; }

        public virtual om_garantia om_garantia { get; set; }

        public virtual om_produto om_produto { get; set; }
    }
}
