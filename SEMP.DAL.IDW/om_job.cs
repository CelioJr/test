namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_job
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_job()
        {
            om_jobdet = new HashSet<om_jobdet>();
            om_joblog = new HashSet<om_joblog>();
        }

        [Key]
        public long id_job { get; set; }

        [StringLength(60)]
        public string cd_job { get; set; }

        public int? revisao { get; set; }

        [StringLength(100)]
        public string ds_job { get; set; }

        public short? st_ativo { get; set; }

        public DateTime? dt_stativo { get; set; }

        public DateTime? dt_revisao { get; set; }

        public long id_usrrevisao { get; set; }

        public long id_usrstativo { get; set; }

        [StringLength(60)]
        public string padrao_schedule { get; set; }

        public short? tp_integracao { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_jobdet> om_jobdet { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_joblog> om_joblog { get; set; }
    }
}
