namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ms_usr
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ms_usr()
        {
            ms_cfg = new HashSet<ms_cfg>();
            ms_detectorusr = new HashSet<ms_detectorusr>();
            ms_detectorusr1 = new HashSet<ms_detectorusr>();
            ms_ic = new HashSet<ms_ic>();
            ms_ihm = new HashSet<ms_ihm>();
            ms_ms = new HashSet<ms_ms>();
            ms_perfilandon = new HashSet<ms_perfilandon>();
            ms_perfilandon1 = new HashSet<ms_perfilandon>();
            ms_up = new HashSet<ms_up>();
        }

        [Key]
        public long id_usr { get; set; }

        [StringLength(30)]
        public string cd_usr { get; set; }

        public int? revisao { get; set; }

        [StringLength(100)]
        public string nm_usr { get; set; }

        public DateTime? dthr_revisao { get; set; }

        public DateTime? dthr_stativo { get; set; }

        public int? st_ativo { get; set; }

        [StringLength(30)]
        public string login { get; set; }

        [StringLength(40)]
        public string senha { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_cfg> ms_cfg { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_detectorusr> ms_detectorusr { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_detectorusr> ms_detectorusr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_ic> ms_ic { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_ihm> ms_ihm { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_ms> ms_ms { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_perfilandon> ms_perfilandon { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_perfilandon> ms_perfilandon1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_up> ms_up { get; set; }
    }
}
