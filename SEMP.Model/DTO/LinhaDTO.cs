using System;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
	public class LinhaDTO
	{
		[Display(Name = "Linha")]
		public string CodLinha { get; set; }
		[Display(Name = "Des. Depart.")]
		public string DesDep { get; set; }
		[Display(Name = "Descri��o")]
		public string DesLinha { get; set; }

		[Display(Name = "Usu�rio")]
		public string Usuario { get; set; }
		public System.Guid? rowguid { get; set; }
		public System.Guid rowguid6 { get; set; }
		[Display(Name = "Tipo")]
		public string FlgTipo { get; set; }
		[Display(Name = "Status")]
		public string FlgAtivo { get; set; }
	}
}
