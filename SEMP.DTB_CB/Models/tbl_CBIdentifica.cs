using System;
using System.Collections.Generic;

namespace SEMP.DTB_CB.Models
{
    public partial class tbl_CBIdentifica
    {
        public tbl_CBIdentifica()
        {
            this.tbl_CBPassagem = new List<tbl_CBPassagem>();
        }

        public string NumECB { get; set; }
        public string CodPlacaIAC { get; set; }
        public string CodPlacaIMC { get; set; }
        public string CodModelo { get; set; }
        public Nullable<System.DateTime> DatIdentifica { get; set; }
        public Nullable<System.DateTime> DatIMC { get; set; }
        public Nullable<System.DateTime> DatModelo { get; set; }
        public string CodLinhaIAC { get; set; }
        public string CodLinhaIMC { get; set; }
        public string CodLinhaFEC { get; set; }
        public string MacAddress { get; set; }
        public virtual ICollection<tbl_CBPassagem> tbl_CBPassagem { get; set; }
    }
}
