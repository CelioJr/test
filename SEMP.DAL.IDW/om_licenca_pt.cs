namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_licenca_pt
    {
        [Key]
        public long id_licencapt { get; set; }

        public long id_licenca { get; set; }

        public long id_pt { get; set; }

        public virtual om_licenca om_licenca { get; set; }

        public virtual om_pt om_pt { get; set; }
    }
}
