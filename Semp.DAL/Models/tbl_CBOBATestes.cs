using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBOBATestes
    {
		public string NumECB { get; set; }
		public int CodLinha { get; set; }
		public int NumPosto { get; set; }
		public string CodFab { get; set; }
		public string NumRPA { get; set; }
		public string CodDefeito { get; set; }
		public int CodSubDefeito { get; set; }
		public string FlgStatus { get; set; }
		public string Observacao { get; set; }
	}
}
