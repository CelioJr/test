using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_SGQ_DOCUMENT_DTO
    {
        public string ID_DOCUMENT { get; set; }
        public string DESCRIPTION { get; set; }
        public string LINK { get; set; }
        public Nullable<int> process_ID_PROCESS { get; set; }
        public string DOC_TYPE { get; set; }
        public byte[] DOC_BYTES { get; set; }
        public virtual ALC_SGQ_PROCESS_DTO ALC_SGQ_PROCESS { get; set; }
    }
}
