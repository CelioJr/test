﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web.Mvc;
using DotNet.Highcharts;
using DotNet.Highcharts.Helpers;
using Microsoft.Ajax.Utilities;
using RAST.BO.PM;
using RAST.BO.Rastreabilidade;
using RAST.BO.Relatorios;
using SEMP.Model.VO;
using SEMP.Model.VO.PM;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using SEMP.Models;
using SEMP.Model.VO.Rastreabilidade;
using SEMP.Model.DTO;

namespace SEMP.Controllers
{
	/// <summary>
	/// Classe Menu, responsável por controlar o Menu.
	/// </summary>
	public class MenuController : Controller
	{
		public ActionResult Index_Old()
		{
			try
			{
				var resumo = MenuCockpit.Lista("");

				ViewBag.grafico = GerarGrafico(resumo, "Percentual de defeitos hora", "chartDefeito");
			}
			catch
			{
				ViewBag.grafico = "";
			}

			var retornologin = (SEMP.Model.VO.RetornoLogin)Session["RetornoLogin"] ?? new SEMP.Model.VO.RetornoLogin { CodDRT = "", UsuarioCracha = false, NomFuncionario = "", NomUsuario = "", Depto = "", Cargo = "" };
			if (SEMP.Helpers.Extensions.Permissao("NovoInicio", retornologin))
			{
				return RedirectToAction("Index_Novo", "Menu");
			}
			return View();

		}

		public ActionResult Index()
		{
			try
			{
				var datInicio = DateTime.Now.AddDays(-DateTime.Now.Day).AddDays(1);
				datInicio = DateTime.Parse(datInicio.ToShortDateString());
				var datFim = datInicio.AddMonths(1).AddDays(-1);

				ViewBag.resumo = Geral.ObterResumoModelo(datInicio, datFim);

			}
			catch
			{
				ViewBag.grafico = "";
			}

			return View();

		}

		public ActionResult Cockpit(string fase)
		{

			var painel = new List<BarrasPainelDTO>();
			var grafico1 = new Highcharts("chartDefeito");
			var grafico2 = new Highcharts("chartDefeito2");
			var grafico3 = new Highcharts("chartDefeito3");

			try
			{

				painel = fase == "IMC" ? PainelProducao.ObterPainelProducaoIMC() :
								 fase == "IAC" ? PainelProducao.ObterPainelProducaoIAC() :
												PainelProducao.ObterPainelProducao();

				ViewBag.painel = painel;
			}
			catch
			{
				ViewBag.painel = painel;
			}

			try
			{
				grafico1 = GerarGrafico(MenuCockpit.Lista(fase), "PPM por hora", "chartDefeito");
				ViewBag.grafico = grafico1;
			}
			catch { }

			try
			{
				grafico2 = GerarGrafico2(MenuCockpit.ListaFabrica(), "PPM por fase", "chartDefeito2");
				ViewBag.grafico2 = grafico2;
			}
			catch { }

			try
			{
				grafico3 = GerarGrafico3(MenuCockpit.ListaTop5(fase), "Top 5 de Defeitos", "chartDefeito3");
				ViewBag.grafico3 = grafico3;
			}
			catch { }

			return View();

		}

		public PartialViewResult ListaHora(string fase)
		{
			try
			{
				return PartialView();
			}
			catch (Exception)
			{
				var erro = new List<ProducaoLinhaMinuto>();
				return PartialView(erro);

			}

		}

		public JsonResult ObterIndice(String fase)
		{
			try
			{

				var indice = 0m;
				var dataHoje = DateTime.Parse(DateTime.Now.ToShortDateString());

				var defeitos = Defeito.ObterDefeitosFabrica(dataHoje, dataHoje, fase).Where(x => x.FlgRevisado == 1 && !x.CodCausa.StartsWith("WX")).ToList();
				var produzido = new List<ProgramaDiario>();
				var qtdPrograma = 0m;

				if (fase != null && fase == "FEC")
				{
					produzido = ProducaoDiaria.ObterProducaoDia(dataHoje);
					var programa = ProducaoDiaria.ObterProgramaDia(dataHoje);//.Where(x => x.CodLinha.StartsWith("101")).ToList();
					qtdPrograma = programa.Sum(x => x.QtdProdPm) ?? 0;
				}
				else if (fase == "IMC")
				{
					produzido = ProducaoDiaria.ObterProducaoDiaPlaca(dataHoje, "M");
					var programa = ProducaoDiaria.ObterProgramaDiaPlaca(dataHoje, "M");
					qtdPrograma = programa.Sum(x => x.QtdProdPM);
				}
				else
				{
					//var prodIAC = Indicadores.ObterTotalIAC(dataHoje, dataHoje.AddHours(12));
					produzido = ProducaoDiaria.ObterProducaoDiaPlaca(dataHoje, "I");
					//produzido.Add(new ProgramaDiario(){QtdProdDia = prodIAC, NomFam = "LCD"});

					var programa = ProducaoDiaria.ObterProgramaDiaPlaca(dataHoje, "I");//.Where(x => x.CodLinha.Contains("SMD-09")).ToList();
					qtdPrograma = programa.Sum(x => x.QtdProdPM);
				}

				var qtdDefeitos = 0m;
				var qtdProduzido = 0m;

				if (produzido != null)
				{
					//produzido = produzido.Where(x => x.NomFam.Contains("LCD")).ToList();

					qtdProduzido = produzido.Sum(x => x.QtdProdDia) ?? 0;
				}

				if (defeitos.Any())
				{
					qtdDefeitos = defeitos.Count();

					if (qtdProduzido > 0)
					{
						indice = (qtdDefeitos / qtdProduzido) * 100;
					}
				}

				var dados = new RetornoTelaInicial()
				{
					produzido = qtdProduzido,
					defeitos = qtdDefeitos,
					qtdPlano = qtdPrograma,
					indice = indice
				};

				return Json(dados, JsonRequestBehavior.AllowGet);
			}
			catch (Exception)
			{
				var dados = new RetornoTelaInicial()
				{
					produzido = 0,
					defeitos = 0,
					qtdPlano = 0,
					indice = -1
				};

				return Json(dados, JsonRequestBehavior.AllowGet);

			}

		}

		public RetornoTelaInicial ObterIndice2(String fase)
		{
			try
			{

				var indice = 0m;
				var dataHoje = DateTime.Parse(DateTime.Now.ToShortDateString());

				var defeitos = Defeito.ObterDefeitosFabrica(dataHoje, dataHoje, fase).Where(x => x.FlgRevisado == 1 && !x.CodCausa.StartsWith("WX")).ToList();
				var produzido = new List<ProgramaDiario>();
				var qtdPrograma = 0m;

				if (fase != null && fase == "FEC")
				{
					produzido = ProducaoDiaria.ObterProducaoDia(dataHoje);
					var programa = ProducaoDiaria.ObterProgramaDia(dataHoje).Where(x => x.CodLinha.StartsWith("101")).ToList();
					qtdPrograma = programa.Sum(x => x.QtdProdPm) ?? 0;
				}
				else if (fase == "IMC")
				{
					produzido = ProducaoDiaria.ObterProducaoDiaPlaca(dataHoje, "M");
					var programa = ProducaoDiaria.ObterProgramaDiaPlaca(dataHoje, "M").ToList();
					qtdPrograma = programa.Sum(x => x.QtdProdPM);
				}
				else
				{
					var prodIAC = Indicadores.ObterTotalIAC(dataHoje, dataHoje);
					produzido.Add(new ProgramaDiario() { QtdProdDia = prodIAC, NomFam = "LCD" });

					var programa = ProducaoDiaria.ObterProgramaDiaPlaca(dataHoje, "I").Where(x => x.CodLinha.Contains("SMD-09")).ToList();
					qtdPrograma = programa.Sum(x => x.QtdProdPM);
				}

				var qtdDefeitos = 0m;
				var qtdProduzido = 0m;

				if (produzido != null)
				{
					produzido = produzido.Where(x => x.NomFam.Contains("LCD")).ToList();

					qtdProduzido = produzido.Sum(x => x.QtdProdDia) ?? 0;
				}

				if (defeitos.Any())
				{
					qtdDefeitos = defeitos.Count();

					if (qtdProduzido > 0)
					{
						indice = (qtdDefeitos / qtdProduzido) * 100;
					}
				}

				var dados = new RetornoTelaInicial()
				{
					produzido = qtdProduzido,
					defeitos = qtdDefeitos,
					qtdPlano = qtdPrograma,
					indice = indice
				};

				return dados;
			}
			catch (Exception)
			{
				var dados = new RetornoTelaInicial()
				{
					produzido = 0,
					defeitos = 0,
					qtdPlano = 0,
					indice = -1
				};

				return dados;

			}

		}


		public JsonResult ObterDadosProducao()
		{

			var dados = PainelProducao.ObterPainelProducao();

			List<object> retorno = new List<object>();

			retorno.Add(dados.Select(x => x.DesModelo).ToList());
			retorno.Add(dados.Select(x => x.Producao).ToList());
			retorno.Add(dados.Select(x => x.Embalados).ToList());
			retorno.Add(dados.Select(x => x.Producao > 0 ? x.Embalados / (decimal)x.Producao * 100 : 0).ToList());

			return Json(retorno, JsonRequestBehavior.AllowGet);

		}

		public ActionResult ObterDadosProducaoCSV()
		{
			//Response.AddHeader("Content-Disposition", "attachment; filename=adressenbestand.csv");
			//Response.ContentType = "text/csv";

			var dados = PainelProducao.ObterPainelProducao();

			//List<object> retorno = new List<object>();

			//retorno.Add(dados.Select(x => x.DesModelo).ToList());
			//retorno.Add(dados.Select(x => x.Embalados).ToList());
			//retorno.Add(dados.Select(x => x.Producao).ToList());
			//retorno.Add(dados.Select(x => x.Producao > 0 ? x.Embalados / (decimal)x.Producao * 100 : 0).ToList());

			return View(dados);

		}


		private Highcharts GerarGrafico(List<DefeitosHora> lista, string titulo, string idGrafico)
		{

			var categories = lista.Select(s => s.Hora.ToStringInvariant()).ToArray();

			var listaDefeitos = new List<DotNet.Highcharts.Options.Point>();
			var listaAcumulados = new List<DotNet.Highcharts.Options.Point>();
			var listaIndice = new List<DotNet.Highcharts.Options.Point>();
			var listaIndiceAcum = new List<DotNet.Highcharts.Options.Point>();

			foreach (var defeitosHora in lista)
			{
				listaIndice.Add(new DotNet.Highcharts.Options.Point { Y = (int)defeitosHora.PPM, Color = Color.CadetBlue });
				listaDefeitos.Add(new DotNet.Highcharts.Options.Point { Y = defeitosHora.QtdDefeitos, Color = Color.Red });
				listaAcumulados.Add(new DotNet.Highcharts.Options.Point { Y = defeitosHora.QtdAcumulado, Color = Color.BlueViolet });
				listaIndiceAcum.Add(new DotNet.Highcharts.Options.Point { Y = (int)defeitosHora.PPMAcum, Color = Color.Brown });
			}

			var dados = new List<DadosGrafico>
			{
				new DadosGrafico()
				{
					Cor = Color.BlueViolet,
					Dados = new Data(listaIndice.ToArray()),
					Titulo = "PPM hora"
				},
				new DadosGrafico()
				{
					Cor = Color.Brown,
					Dados = new Data(listaIndiceAcum.ToArray()),
					Titulo = "PPM acumulado"
				}
			};

			var chart = Grafico.GerarGraficoLinhaPPM(dados, categories, titulo, idGrafico, 300, 280, "Hora", "Índice");

			return chart;
		}

		private Highcharts GerarGrafico2(List<DefeitosFabrica> lista, string titulo, string idGrafico)
		{

			var categories = lista.Select(s => s.Fabrica).ToArray();

			var listaIndice = new List<DotNet.Highcharts.Options.Point>();
			var listaIndiceMeta = new List<DotNet.Highcharts.Options.Point>();

			lista.ForEach(x =>
			{
				listaIndice.Add(new DotNet.Highcharts.Options.Point { Y = x.PPM });
				listaIndiceMeta.Add(new DotNet.Highcharts.Options.Point { Y = x.PPMMeta });
			});

			var dados = new Data(listaIndice.ToArray());
			var dados2 = new Data(listaIndiceMeta.ToArray());

			var chart = Grafico.GerarGraficoBarra(dados, dados2, categories, titulo, "Metas", idGrafico, 300, 280, Color.Red, Color.Blue);

			return chart;
		}

		private Highcharts GerarGrafico3(List<DefeitosFabrica> lista, string titulo, string idGrafico)
		{

			var categories = lista.Select(s => s.Fabrica).ToArray();

			var listaIndice = new List<DotNet.Highcharts.Options.Point>();

			lista.ForEach(x =>
			{
				Random rnd = new Random();
				Color randomColor = Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));

				listaIndice.Add(new DotNet.Highcharts.Options.Point { Y = x.QtdDefeito, Color = randomColor });
			});

			var dados = new Data(listaIndice.ToArray());

			var chart = Grafico.GerarGraficoBarra(dados, categories, titulo, idGrafico, 300, 280);

			return chart;
		}
	}
}
