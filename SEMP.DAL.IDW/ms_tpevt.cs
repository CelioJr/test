namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ms_tpevt
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ms_tpevt()
        {
            ms_trigger = new HashSet<ms_trigger>();
        }

        [Key]
        public long id_tpevt { get; set; }

        [StringLength(100)]
        public string ds_tpevt { get; set; }

        public int? depara_pdba { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_trigger> ms_trigger { get; set; }
    }
}
