using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
	public class tbl_CBPlanoAcaoCausaMap : EntityTypeConfiguration<tbl_CBPlanoAcaoCausa>
	{
		public tbl_CBPlanoAcaoCausaMap()
		{
			// Primary Key
			this.HasKey(t => new { t.CodPlano, t.CodPlanoCausa });

			// Properties
			this.Property(t => t.CodPlano)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.CodPlanoCausa)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

			this.Property(t => t.DesCausa)
				.HasMaxLength(200);

			this.Property(t => t.NomResponsavel)
				.HasMaxLength(50);

			// Table & Column Mappings
			this.ToTable("tbl_CBPlanoAcaoCausa");
			this.Property(t => t.CodPlano).HasColumnName("CodPlano");
			this.Property(t => t.CodPlanoCausa).HasColumnName("CodPlanoCausa");
			this.Property(t => t.DesCausa).HasColumnName("DesCausa");
			this.Property(t => t.CodTipoCausa).HasColumnName("CodTipoCausa");
			this.Property(t => t.FlgStatus).HasColumnName("FlgStatus");
			this.Property(t => t.DatInicio).HasColumnName("DatInicio");
			this.Property(t => t.DatFim).HasColumnName("DatFim");
			this.Property(t => t.NomResponsavel).HasColumnName("NomResponsavel");


		}
	}
}
