﻿using System;
using System.Web.Mvc;
using RAST.BO.CNQ;
using SEMP.Model.DTO;

namespace SEMP.Areas.Outros.Controllers
{
	public class CustoRetrabalhoController : Controller
	{
		//
		// GET: /Coleta/CustoRetrabalho/
		public ActionResult Index()
		{
			return View();
		}

		//
		// GET: /Coleta/CustoRetrabalho/Details/5
		public ActionResult Lista(string mesRef, string codFam)
		{

			var mes = DateTime.Parse(DateTime.Now.AddDays(-DateTime.Now.Day).AddDays(1).ToShortDateString());
			if (!String.IsNullOrEmpty(mesRef))
			{
				mes = DateTime.Parse(mesRef);
			}

			var custoret = CustoRestrabalhoBO.ObterCustosRet(mes, codFam);

			return View(custoret);
		}

		//
		// GET: /Coleta/CustoRetrabalho/Create
		public ActionResult Create(string mesRef)
		{
			ViewBag.mesRef = mesRef;
			return View();
		}

		//
		// POST: /Coleta/CustoRetrabalho/Create
		[HttpPost]
		public ActionResult Create(CnqCustoRetDTO custoret)
		{
			try
			{
				CustoRestrabalhoBO.GravarCustoRef(custoret);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}

		//
		// GET: /Coleta/CustoRetrabalho/Edit/5
		public ActionResult Edit(string mesRef, string codFam, string codFab)
		{
			var mes = DateTime.Parse(mesRef);
			var custoret = CustoRestrabalhoBO.ObterCustoRet(mes, codFam, codFab);

			return View(custoret);
		}

		//
		// POST: /Coleta/CustoRetrabalho/Edit/5
		[HttpPost]
		public ActionResult Edit(CnqCustoRetDTO custoret)
		{
			try
			{
				CustoRestrabalhoBO.GravarCustoRef(custoret);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}

		//
		// GET: /Coleta/CustoRetrabalho/Delete/5
		public ActionResult Delete(string mesRef, string codFam, string codFab)
		{
			var mes = DateTime.Parse(mesRef);
			var custoret = CustoRestrabalhoBO.ObterCustoRet(mes, codFam, codFab);

			return View(custoret);
		}

		//
		// POST: /Coleta/CustoRetrabalho/Delete/5
		[HttpPost]
		public ActionResult Delete(string mesRef, string codFam, string codFab, string post)
		{
			try
			{
				var mes = DateTime.Parse(mesRef);
				var custoret = CustoRestrabalhoBO.ObterCustoRet(mes, codFam, codFab);

				CustoRestrabalhoBO.RemoverCustoRef(custoret);

				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}
		}
	}
}
