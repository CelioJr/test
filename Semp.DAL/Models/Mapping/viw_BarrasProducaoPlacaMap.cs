using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class viw_BarrasProducaoPlacaMap : EntityTypeConfiguration<viw_BarrasProducaoPlaca>
    {
        public viw_BarrasProducaoPlacaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.CodLinha, t.DatReferencia, t.DatLeitura, t.CodOperador, t.CodApontador, t.CodModelo, t.NumSerieModelo });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodLinha)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.CodOperador)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.CodApontador)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumSerieModelo)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.NomEstacao)
                .HasMaxLength(30)
                .IsUnicode(false);

            this.Property(t => t.NumAP)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("viw_BarrasProducaoPlaca");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.DatReferencia).HasColumnName("DatReferencia");
            this.Property(t => t.DatLeitura).HasColumnName("DatLeitura");
            this.Property(t => t.CodOperador).HasColumnName("CodOperador");
            this.Property(t => t.CodApontador).HasColumnName("CodApontador");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.NumSerieModelo).HasColumnName("NumSerieModelo");
            this.Property(t => t.NomEstacao).HasColumnName("NomEstacao");
            this.Property(t => t.NumAP).HasColumnName("NumAP");
			this.Property(t => t.NumTurno).HasColumnName("NumTurno");
        }
    }
}
