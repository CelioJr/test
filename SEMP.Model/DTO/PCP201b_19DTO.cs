﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.DTO
{
	public class PCP201b_19DTO
	{
		[Display(Name = "Fab.")]
		public string CodFab { get; set; }
		[Display(Name = "Descrição")]
		public string Modelo { get; set; }

		[Display(Name = "Total Prog.")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? QtdTotalProg { get; set; }

		[Display(Name = "Total AP")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? QtdTotalAP { get; set; }

		[Display(Name = "Total Apont.")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? QtdTotalApont { get; set; }

		[Display(Name = "Saldo Inicial")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? SaldoInicial { get; set; }

		[Display(Name = "006")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? P006 { get; set; }

		[Display(Name = "007")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? P007 { get; set; }

		[Display(Name = "008")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? P008 { get; set; }

		[Display(Name = "701")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? P701 { get; set; }

		[Display(Name = "801")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? P801 { get; set; }

		[Display(Name = "702")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? P702 { get; set; }

		[Display(Name = "802")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? P802 { get; set; }

		[Display(Name = "703")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? P703 { get; set; }

		[Display(Name = "803")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? P803 { get; set; }

		[Display(Name = "704")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? P704 { get; set; }

		[Display(Name = "804")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? P804 { get; set; }

		[Display(Name = "805")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? P805 { get; set; }

		[Display(Name = "806")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? P806 { get; set; }

		[Display(Name = "807")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? P807 { get; set; }

		[Display(Name = "822")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? P822 { get; set; }

		[Display(Name = "Família")]
		public string NomFam { get; set; }

		[Display(Name = "Fase")]
		public string Fase { get; set; }
		[Display(Name = "Modelo")]
		public string CodModelo { get; set; }
		[Display(Name = "Aparelho")]
		public string CodAparelho { get; set; }
		[Display(Name = "Ordem")]
		public int Ordem { get; set; }

		[Display(Name = "Total")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? Total { get; set; }
	}
}
