using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class TBL_GpaRecItem
    {
        public long IdRegistro { get; set; }
        public string CodLinha { get; set; }
        public string CodItem { get; set; }
        public Nullable<int> CodPallet { get; set; }
        public string NumSerie { get; set; }
        public System.DateTime DataLeitura { get; set; }
        public Nullable<System.DateTime> DataReferencia { get; set; }
        public string Operador { get; set; }
        public string Status { get; set; }
        public string Situacao { get; set; }
        public string NumAP { get; set; }
        public string TipoAP { get; set; }
    }
}
