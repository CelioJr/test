namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ms_cfg
    {
        [Key]
        public long id_cfg { get; set; }

        public int? revisao { get; set; }

        public int? tp_calculoAndon { get; set; }

        public int? st_ativo { get; set; }

        public DateTime? dthr_stativo { get; set; }

        public DateTime? dthr_revisao { get; set; }

        public long id_usr { get; set; }

        public virtual ms_usr ms_usr { get; set; }
    }
}
