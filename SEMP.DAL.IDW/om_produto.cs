namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_produto
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_produto()
        {
            dw_consolperdamplog = new HashSet<dw_consolperdamplog>();
            dw_consolpr = new HashSet<dw_consolpr>();
            dw_estpro = new HashSet<dw_estpro>();
            dw_estsalma = new HashSet<dw_estsalma>();
            dw_estsalma1 = new HashSet<dw_estsalma>();
            dw_expcvs = new HashSet<dw_expcvs>();
            dw_folhaemb = new HashSet<dw_folhaemb>();
            dw_folhaiac = new HashSet<dw_folhaiac>();
            dw_folhaiac1 = new HashSet<dw_folhaiac>();
            dw_folhamon = new HashSet<dw_folhamon>();
            dw_folhamoncomp = new HashSet<dw_folhamoncomp>();
            dw_folharapcom = new HashSet<dw_folharapcom>();
            dw_folhateste = new HashSet<dw_folhateste>();
            dw_folhatv = new HashSet<dw_folhatv>();
            dw_ft_sub = new HashSet<dw_ft_sub>();
            dw_nserie = new HashSet<dw_nserie>();
            dw_operacao = new HashSet<dw_operacao>();
            dw_operacao1 = new HashSet<dw_operacao>();
            dw_operacaocomp = new HashSet<dw_operacaocomp>();
            dw_passcau = new HashSet<dw_passcau>();
            dw_passmon = new HashSet<dw_passmon>();
            dw_produtoconjugado = new HashSet<dw_produtoconjugado>();
            dw_produtoconjugado1 = new HashSet<dw_produtoconjugado>();
            dw_rota = new HashSet<dw_rota>();
            dw_t_defeito = new HashSet<dw_t_defeito>();
            ip_balanceamento = new HashSet<ip_balanceamento>();
            ip_balanceamento1 = new HashSet<ip_balanceamento>();
            om_cfg = new HashSet<om_cfg>();
            om_garpro = new HashSet<om_garpro>();
            om_mapa = new HashSet<om_mapa>();
            om_mapapa = new HashSet<om_mapapa>();
            om_papro = new HashSet<om_papro>();
            om_prg = new HashSet<om_prg>();
            om_prgpos = new HashSet<om_prgpos>();
            om_proaltglo = new HashSet<om_proaltglo>();
            om_proaltglo1 = new HashSet<om_proaltglo>();
            om_proaltglo2 = new HashSet<om_proaltglo>();
            om_proaltglo3 = new HashSet<om_proaltglo>();
            om_procomest = new HashSet<om_procomest>();
            om_procomest1 = new HashSet<om_procomest>();
            om_produto1 = new HashSet<om_produto>();
            om_promidia = new HashSet<om_promidia>();
            om_propaihomo = new HashSet<om_propaihomo>();
            om_propaihomo1 = new HashSet<om_propaihomo>();
            om_proturno = new HashSet<om_proturno>();
            pp_cmcom = new HashSet<pp_cmcom>();
            pp_cmcom1 = new HashSet<pp_cmcom>();
            pp_cmcom2 = new HashSet<pp_cmcom>();
            pp_cmcom3 = new HashSet<pp_cmcom>();
            pp_cmcom4 = new HashSet<pp_cmcom>();
            pp_cpproduto = new HashSet<pp_cpproduto>();
            pp_nec = new HashSet<pp_nec>();
            pp_planeccron = new HashSet<pp_planeccron>();
            pp_cpfaltamp = new HashSet<pp_cpfaltamp>();
        }

        [Key]
        public long id_produto { get; set; }

        public long? id_produtoAgru { get; set; }

        [StringLength(30)]
        public string cd_produto { get; set; }

        public int? revisao { get; set; }

        [StringLength(256)]
        public string ds_produto { get; set; }

        public DateTime? dt_revisao { get; set; }

        public DateTime? dt_stativo { get; set; }

        public short? st_ativo { get; set; }

        public long id_usrstativo { get; set; }

        public long id_usrrevisao { get; set; }

        public long? id_cc { get; set; }

        public decimal? min_valposalim { get; set; }

        public long? id_for { get; set; }

        public long? id_progrp { get; set; }

        [StringLength(100)]
        public string ds_complemento { get; set; }

        [StringLength(256)]
        public string depara { get; set; }

        public short? tp_produto { get; set; }

        public int? hr_leadtimeEntrada { get; set; }

        public int? hr_leadtimeSaida { get; set; }

        public short? tp_producao { get; set; }

        public long? id_est { get; set; }

        public long? id_cliente { get; set; }

        [StringLength(10)]
        public string ds_curta { get; set; }

        public short? tp_origem { get; set; }

        public decimal? vl_custounit { get; set; }

        public long? id_unidmedida { get; set; }

        public short? tp_semiacabado { get; set; }

        public short? tp_semiacabado2 { get; set; }

        public decimal? qt_loteminprod { get; set; }

        public short? is_DAT { get; set; }

        public short? is_consumido { get; set; }

        public decimal? ind_perdaproducao { get; set; }

        public decimal? g_peso_bruto { get; set; }

        public decimal? g_peso_liquido { get; set; }

        public short? tp_classeabc { get; set; }

        [StringLength(20)]
        public string cd_sap { get; set; }

        [StringLength(20)]
        public string cd_caixa { get; set; }

        public short? qt_empilhamento { get; set; }

        [StringLength(60)]
        public string cd_modelo { get; set; }

        [StringLength(60)]
        public string cd_partnumber { get; set; }

        public decimal? ind_defeito { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolperdamplog> dw_consolperdamplog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolpr> dw_consolpr { get; set; }

        public virtual dw_est dw_est { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_estpro> dw_estpro { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_estsalma> dw_estsalma { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_estsalma> dw_estsalma1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_expcvs> dw_expcvs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhaemb> dw_folhaemb { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhaiac> dw_folhaiac { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhaiac> dw_folhaiac1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhamon> dw_folhamon { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhamoncomp> dw_folhamoncomp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folharapcom> dw_folharapcom { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhateste> dw_folhateste { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhatv> dw_folhatv { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_ft_sub> dw_ft_sub { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_nserie> dw_nserie { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_operacao> dw_operacao { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_operacao> dw_operacao1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_operacaocomp> dw_operacaocomp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_passcau> dw_passcau { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_passmon> dw_passmon { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_produtoconjugado> dw_produtoconjugado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_produtoconjugado> dw_produtoconjugado1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rota> dw_rota { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_t_defeito> dw_t_defeito { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ip_balanceamento> ip_balanceamento { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ip_balanceamento> ip_balanceamento1 { get; set; }

        public virtual om_cc om_cc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg { get; set; }

        public virtual om_for om_for { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_garpro> om_garpro { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_mapa> om_mapa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_mapapa> om_mapapa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_papro> om_papro { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_prg> om_prg { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_prgpos> om_prgpos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_proaltglo> om_proaltglo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_proaltglo> om_proaltglo1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_proaltglo> om_proaltglo2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_proaltglo> om_proaltglo3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_procomest> om_procomest { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_procomest> om_procomest1 { get; set; }

        public virtual pp_cliente pp_cliente { get; set; }

        public virtual om_progrp om_progrp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_produto> om_produto1 { get; set; }

        public virtual om_produto om_produto2 { get; set; }

        public virtual om_unidmedida om_unidmedida { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_promidia> om_promidia { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_propaihomo> om_propaihomo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_propaihomo> om_propaihomo1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_proturno> om_proturno { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cmcom> pp_cmcom { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cmcom> pp_cmcom1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cmcom> pp_cmcom2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cmcom> pp_cmcom3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cmcom> pp_cmcom4 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cpproduto> pp_cpproduto { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_nec> pp_nec { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_planeccron> pp_planeccron { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cpfaltamp> pp_cpfaltamp { get; set; }
    }
}
