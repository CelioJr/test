using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_OQC_TARGET_DTO
    {
        public ALC_OQC_TARGET_DTO()
        {
            this.ALC_OQC_INSPECTION = new List<ALC_OQC_INSPECTION_DTO>();
        }

        public int id { get; set; }
        public Nullable<int> quantity { get; set; }
        public string sku { get; set; }
        public Nullable<System.DateTime> dtCreated { get; set; }
        public Nullable<System.DateTime> dtFinished { get; set; }
        public Nullable<bool> activated { get; set; }
        public virtual ICollection<ALC_OQC_INSPECTION_DTO> ALC_OQC_INSPECTION { get; set; }
    }
}
