﻿using RAST.BO.CNQ;
using RAST.BO.Relatorios;
using SEMP.Model.DTO;
using SEMP.Model.VO.CNQ;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEMP.Areas.Coleta.Controllers
{
    public class RelatorioInspecaoIACController : Controller
    {

        public ActionResult Index(string Mensagem)
        {
            ViewBag.mensagem = Mensagem;
            return View();
        }

        public JsonResult GetModelos(string datInicio, string datFim, int codlinha, int? turno)
        {
            DateTime dIni = Convert.ToDateTime(datInicio);
            DateTime dFim = Convert.ToDateTime(datFim).AddDays(1);

            int t = 1;
            if (turno != null && turno != 0)
            {
                t = (int)turno;
            }

            var linha = PainelProducao.ObterCodLinha(codlinha);

            var Modelos = RelatorioProducaoIAC.ObterResumoIAC(dIni, dFim, linha.CodLinhaT1, t);

            return Json(Modelos, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GerarRelatorio(string DatInicio, string DatFim, string Turno, string CodLinha, string CodModelo)
        {

            List<CBLinhaDTO> linhas = null;
            DateTime dateStart = DateTime.Now;
            DateTime dateEnd = DateTime.Now;
            DateTime dataFimInicioAux = DateTime.Now;

            if (!string.IsNullOrWhiteSpace(DatInicio))
            {
                dateStart = DateTime.Parse(DatInicio);
            }

            if (!string.IsNullOrWhiteSpace(DatFim))
            {
                dateEnd = DateTime.Parse(DatFim);
            }
            
            dateEnd = dateEnd.AddDays(1).AddSeconds(-1);

            List<MaioresDefeitos> listaMaioresDefeitos = new List<MaioresDefeitos>();

            if (string.IsNullOrWhiteSpace(CodLinha))
            {
                linhas = RAST.BO.Rastreabilidade.Geral.ObterLinhaPorFabrica("IAC").OrderBy(o => o.DesLinha).ToList();

                foreach (var item in linhas)
                {
                    var listagemDefeitos = QualidadeBO.GetMaioresDefeitosIACPorLinha(dateStart, dateEnd, item.CodLinha);
                    listaMaioresDefeitos.AddRange(listagemDefeitos);
                }
            }
            else
            {
                linhas = new List<CBLinhaDTO>();
                linhas.Add(RAST.BO.Rastreabilidade.Geral.ObterLinha(int.Parse(CodLinha)));

                foreach (var item in linhas)
                {
                    listaMaioresDefeitos = QualidadeBO.GetMaioresDefeitosIACPorLinha(dateStart, dateEnd, item.CodLinha);
                }
            }

            var horarios = GetHorarios(int.Parse(Turno));
            var total = new IndicadorPostoHoraHora() { 
                Titulo = "TOTAL ACUMULADO",
                Hora = "TOTAL ACUMULADO",
                Quantidade = 0
            };

            horarios.Add(total);

            var rel = new RelatorioInspecaoIACVO();
            
            rel.Linhas = new List<RelatorioInspecaoIACLinhasVO>();

            foreach (var item in linhas)
            {
                var l = new RelatorioInspecaoIACLinhasVO();

                l.CodLinha = item.CodLinha;
                l.Descricao = item.DesLinha;

                l.Postos = new List<RelatorioInspecaoIACPostos>();
                l.HorasLinha = new List<RelatorioInspecaoIACHorarios>();

                int quantDefeitoAcumulado = 0;
                int quantInspecionadaAcumulada = 0;
                
                foreach (var h in horarios)
                {
                    RelatorioInspecaoIACHorarios horariosLinha = null;
                    var obj = listaMaioresDefeitos
                        .Where(a => a.DatEvento.TimeOfDay >= h.HoraInicio && a.DatEvento.TimeOfDay <= h.HoraFim && a.CodLinha == item.CodLinha)
                        .GroupBy(g => new { Posicao = g.Posicao.ToString().Trim(), CodDefeito = g.CodDefeito.ToString().Trim()
                            //, DesDefeito = g.DesDefeito.ToString().Trim() 
                        })
                        .Select(s => new { Posicao = s.Key.Posicao, CodDefeito = s.Key.CodDefeito, Quantidade = s.Count()
                            //,DesDefeito = s.Key.DesDefeito 
                        }).ToList();

                    int quantDefeito = 0;
                    
                    if (obj.Count > 0)
                    {
                        foreach (var itemObj in obj)
                        {
                            horariosLinha = new RelatorioInspecaoIACHorarios();

                            horariosLinha.HoraInicio = h.HoraInicio;
                            horariosLinha.HoraFim = h.HoraFim;
                            horariosLinha.Hora = h.Hora;

                            horariosLinha.Posicao = itemObj.Posicao;
                            horariosLinha.CodDefeito = itemObj.CodDefeito;
                            //horariosLinha.DesDefeito = itemObj.DesDefeito;
                            horariosLinha.Quantidade = itemObj.Quantidade;
                            horariosLinha.CodLinha = item.CodLinha;

                            quantDefeito = quantDefeito + horariosLinha.Quantidade;
                            
                            l.HorasLinha.Add(horariosLinha);
                        }
                    }
                    else
                    {
                        horariosLinha = new RelatorioInspecaoIACHorarios();

                        horariosLinha.HoraInicio = h.HoraInicio;
                        horariosLinha.HoraFim = h.HoraFim;
                        horariosLinha.Hora = h.Hora;

                        horariosLinha.Posicao = "";
                        horariosLinha.CodDefeito = "";
                        horariosLinha.Quantidade = 0;

                        horariosLinha.CodLinha = item.CodLinha;
                        l.HorasLinha.Add(horariosLinha);
                    }

                    //
                    horariosLinha = new RelatorioInspecaoIACHorarios();
                    horariosLinha.HoraInicio = h.HoraInicio;
                    horariosLinha.HoraFim = h.HoraFim;
                    horariosLinha.Hora = h.Hora;
                    horariosLinha.Posicao = "QTD. INSPECIONADA";
                    horariosLinha.CodDefeito = "QTD. INSPECIONADA";
                    horariosLinha.CodLinha = item.CodLinha;
                    var quantidadeInspecionada = (int)QualidadeBO.GetQuantidadeProduzidaPorLinhaEHora(dateStart, dateEnd, h.HoraInicio, h.HoraFim, int.Parse(Turno), item.CodLinha); ;
                    horariosLinha.Quantidade = quantidadeInspecionada;

                    quantInspecionadaAcumulada = quantInspecionadaAcumulada + horariosLinha.Quantidade;

                    l.HorasLinha.Add(horariosLinha);

                    //
                    horariosLinha = new RelatorioInspecaoIACHorarios();
                    horariosLinha.HoraInicio = h.HoraInicio;
                    horariosLinha.HoraFim = h.HoraFim;
                    horariosLinha.Hora = h.Hora;
                    horariosLinha.Posicao = "QTD. INSPECIONADA ACUMULADA";
                    horariosLinha.CodDefeito = "QTD. INSPECIONADA ACUMULADA";
                    horariosLinha.CodLinha = item.CodLinha;
                    horariosLinha.Quantidade = quantInspecionadaAcumulada;
                    l.HorasLinha.Add(horariosLinha);

                    //
                    horariosLinha = new RelatorioInspecaoIACHorarios();
                    horariosLinha.HoraInicio = h.HoraInicio;
                    horariosLinha.HoraFim = h.HoraFim;
                    horariosLinha.Hora = h.Hora;
                    horariosLinha.Posicao = "QTD. DEFEITO";
                    horariosLinha.CodDefeito = "QTD. DEFEITO";
                    horariosLinha.CodLinha = item.CodLinha;
                    horariosLinha.Quantidade = quantDefeito;
                    l.HorasLinha.Add(horariosLinha);

                    quantDefeitoAcumulado = quantDefeitoAcumulado + quantDefeito;

                    //
                    horariosLinha = new RelatorioInspecaoIACHorarios();
                    horariosLinha.HoraInicio = h.HoraInicio;
                    horariosLinha.HoraFim = h.HoraFim;
                    horariosLinha.Hora = h.Hora;
                    horariosLinha.Posicao = "QTD. DEFEITO ACUMULADA";
                    horariosLinha.CodDefeito = "QTD. DEFEITO ACUMULADA";
                    horariosLinha.CodLinha = item.CodLinha;
                    horariosLinha.Quantidade = quantDefeitoAcumulado;
                    l.HorasLinha.Add(horariosLinha);

                    //
                    horariosLinha = new RelatorioInspecaoIACHorarios();
                    horariosLinha.HoraInicio = h.HoraInicio;
                    horariosLinha.HoraFim = h.HoraFim;
                    horariosLinha.Hora = h.Hora;
                    horariosLinha.Posicao = "QTD. PPM ACUMULADO";
                    horariosLinha.CodDefeito = "QTD. PPM ACUMULADO";
                    horariosLinha.CodLinha = item.CodLinha;

                    var quantidadePontos = QualidadeBO.GetQuantidadeDePontosPorLinhaEHora(dateStart, dateEnd, h.HoraInicio, h.HoraFim, int.Parse(Turno), item.CodLinha);

                    var calculo = quantidadeInspecionada > 0 ? (double)quantDefeito / (double)(quantidadeInspecionada * quantidadePontos) : 0;

                    var ppm = Math.Round((double)calculo * 1000000, 1);

                    horariosLinha.Quantidade = (int)ppm;

                    l.HorasLinha.Add(horariosLinha);
                }

                l.ModelosLinha = QualidadeBO.GetModelosLinhasIAC(dateStart, dateEnd, item.CodLinha);
                
                rel.Linhas.Add(l);
            }

            return PartialView(rel);
        }

        public List<IndicadorPostoHoraHora> GetHorarios(int t)
        {
            List<IndicadorPostoHoraHora> horarios = new List<IndicadorPostoHoraHora>();
            CBHorarioDTO turno = Indicadores.GetTurno(t);
            DateTime data = DateTime.Now;

            TimeSpan time = data.TimeOfDay;

            // se o inicio for maior que o fim quer dizer que o turno começa em um dia e termina em outro dia
            if (TimeSpan.Parse(turno.HoraInicio) > TimeSpan.Parse(turno.HoraFim))
            {
                time = DateTime.Parse(data.AddDays(1).ToShortDateString() + " " + turno.HoraFim.ToString()).Subtract(DateTime.Parse(data.ToShortDateString() + " " + turno.HoraInicio.ToString()));
            }
            else
            {
                time = DateTime.Parse(data.ToShortDateString() + " " + turno.HoraFim.ToString()).Subtract(DateTime.Parse(data.ToShortDateString() + " " + turno.HoraInicio.ToString()));
            }


            int hora = 0;
            int total = (int)Math.Round(time.TotalHours);
            int horasInicial = DateTime.Parse(data.ToShortDateString() + " " + turno.HoraInicio.ToString()).TimeOfDay.Hours;
            int horasFinal = DateTime.Parse(data.ToShortDateString() + " " + turno.HoraFim.ToString()).TimeOfDay.Hours;

            for (int i = 0; i <= total; i++)
            {
                hora = horasInicial + i;

                if (hora == 24)
                {
                    for (int j = 0; j <= horasFinal; j++)
                    {
                        hora = j;

                        horarios.Add(new IndicadorPostoHoraHora
                        {
                            Hora = new TimeSpan(hora, 0, 0).ToString() + " - " + new TimeSpan(hora + 1, 0, 0).ToString(),
                            HoraInicio = new TimeSpan(hora, hora == horasInicial ? TimeSpan.Parse(turno.HoraInicio).Minutes : 0, 0),
                            HoraFim = new TimeSpan(hora == horasFinal ? horasFinal : hora + 1, hora == horasFinal ? TimeSpan.Parse(turno.HoraFim).Minutes : 0, 0)
                        });
                    }
                    break;
                }
                else
                {
                    horarios.Add(new IndicadorPostoHoraHora
                    {
                        Hora = new TimeSpan(hora, 0, 0).ToString() + " - " + new TimeSpan(hora == 23 ? 0 : hora + 1, 0, 0).ToString(),
                        HoraInicio = new TimeSpan(hora, hora == horasInicial ? TimeSpan.Parse(turno.HoraInicio).Minutes : 0, 0),
                        HoraFim = hora == 23 ?
                        new TimeSpan(hora == horasFinal ? horasFinal : 23, hora == horasFinal ? TimeSpan.Parse(turno.HoraFim).Minutes : 59, 59)
                        :
                        new TimeSpan(hora == horasFinal ? horasFinal : hora + 1, hora == horasFinal ? TimeSpan.Parse(turno.HoraFim).Minutes : 0, 0)
                    });
                }
            }

            return horarios;
        }
    }
}