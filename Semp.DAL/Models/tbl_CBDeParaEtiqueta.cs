using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBDeParaEtiqueta
    {
        public string CodModelo { get; set; }
        public string CodEtiqueta { get; set; }
    }
}
