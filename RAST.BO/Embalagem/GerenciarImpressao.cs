﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using SEMP.Model.DTO;

namespace RAST.BO.Embalagem
{
    public static class GerenciarImpressao
    {

        public static void ImprimirEtiqueta (Hashtable dados, CBEtiquetaDTO etiqueta)
        {            
			TcpClient tcpclnt = new TcpClient();
            string dadosEnviar = string.Empty;
            ICollection chaves = dados.Keys;

            foreach (var chave in chaves)
            {
                dadosEnviar += chave.ToString() + "$" + dados[chave] + "|";            
            }
            dadosEnviar += "NomeImpressora$" + etiqueta.NomeImpressora + "|";
            dadosEnviar += "CaminhoArquivo$" + etiqueta.CaminhoEtiqueta + etiqueta.NomeEtiqueta + "|";

            // IP do servidor de impressão 
            tcpclnt.Connect(etiqueta.NumIP_Impressora, Convert.ToInt32(etiqueta.Porta_Impressora)); 
            
            //recupera o Stream de leitura
            Stream stm = tcpclnt.GetStream();

            //transforma em bytes a string
            ASCIIEncoding asen = new ASCIIEncoding();
            byte[] ba = asen.GetBytes(dadosEnviar);

            //envia a string via stream
            stm.Write(ba, 0, ba.Length);

            //recupera o retorno do servidor
            byte[] bb=new byte[100];
			int k=stm.Read(bb,0,100);

            string retorno = "";
            for (int i = 0; i < k; i++)
                retorno += Convert.ToChar(bb[i]);

            if (!string.IsNullOrEmpty(retorno))
                if (retorno != "OK")
                    throw new Exception(retorno);
 
            //fecha a conexao
            tcpclnt.Close();

        }

    }
}
