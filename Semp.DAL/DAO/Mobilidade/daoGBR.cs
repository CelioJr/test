﻿using SEMP.DAL.GBR;
using SEMP.Model.DTO;
using SEMP.Model.VO.Mobilidade;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEMP.DAL.DAO.Mobilidade
{
	public class daoGBR
	{

		public static List<ResumoProducaoGBR> ObterResumoGBR(DateTime datInicio, DateTime datFim)
		{

			try
			{
				using (var contexto = new GBRContext())
				{

					var resumo = (from A in contexto.GBR_LIST_PACK
								  join B in contexto.GBR_CAD_SKU on A.COD_SKU equals B.COD_SKU
								  where A.DATA >= datInicio && A.DATA <= datFim
								  select new
								  {
									  B.COD_SKU,
									  B.SKU,
									  B.DATA
								  })
								  .ToList()
								  .GroupBy(B => new { B.COD_SKU, B.SKU, DATA = B.DATA.Value.Date })
								  .Select(x => new ResumoProducaoGBR()
								  {
									  COD_SKU = x.Key.COD_SKU,
									  SKU = x.Key.SKU,
									  DATA = x.Key.DATA,
									  QTD_PROD = x.Count()
								  })
								  .ToList();

					return resumo;
				}
			}
			catch (Exception)
			{

				return null;
			}

		}

		public static List<GBR_CAD_LINHAS> ObterLinhas()
		{
			using (var contexto = new GBRContext())
			{
				return contexto.GBR_CAD_LINHAS.ToList();
			}
		}

		public static GBR_CAD_LINHAS ObterLinha(int codLinha)
		{
			using (var contexto = new GBRContext())
			{
				var dados = contexto.GBR_CAD_LINHAS.FirstOrDefault(x => x.COD_LINHAS == codLinha);

				return dados;
			}
		}

		public static List<GBR_LIST_PACK_DTO> ObterPack(string packString){

			using (var contexto = new GBRContext())
			{
				var dados = contexto.GBR_LIST_PACK.Where(x => x.PACK_STRING == packString)
								.Select(z => new GBR_LIST_PACK_DTO() { 
									PACK_STRING = z.PACK_STRING,
									COD_LIST_PACK = z.COD_LIST_PACK,
									COD_MODELO = z.COD_MODELO,
									COD_SKU = z.COD_SKU,
									COD_USUARIO = z.COD_USUARIO,
									CONFERIDO_PACK = z.CONFERIDO_PACK,
									DATA = z.DATA,
									FECHADO = z.FECHADO,
									IP = z.IP,
									ORDEM = z.ORDEM,
									PESO = z.PESO,
									PPB = z.PPB,
									SERIAL = z.SERIAL
								})
								.ToList();
				return dados;
			}

		}

		public static GBR_CAD_SMT_SERIAL_DTO ObterSerial(string numSerie) 
		{

			using (var contexto = new GBRContext())
			{
				var dados = contexto.GBR_CAD_SMT_SERIAL
								.Select(x => new GBR_CAD_SMT_SERIAL_DTO() { 
									SERIAL = x.SERIAL,
									IMEI_1 = x.IMEI_1,
									IMEI_2 = x.IMEI_2,
									IMEI_3 = x.IMEI_3,
									IMEI_4 = x.IMEI_4,
									PESO = x.PESO
								})
								.FirstOrDefault(x => x.SERIAL == numSerie);
				return dados;
			}
		
		}

	}
}
