using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBValidaModeloMap : EntityTypeConfiguration<tbl_CBValidaModelo>
    {
        public tbl_CBValidaModeloMap()
        {
            // Primary Key
            this.HasKey(t => new { t.NumPosto, t.CodModelo, t.CodLinha, t.Setor });

            // Properties
            this.Property(t => t.NumPosto)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6);

            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Setor)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3);

            // Table & Column Mappings
            this.ToTable("tbl_CBValidaModelo");
            this.Property(t => t.NumPosto).HasColumnName("NumPosto");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.Setor).HasColumnName("Setor");
        }
    }
}
