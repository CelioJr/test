﻿using SEMP.DAL.Models;
using SEMP.Model.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.DAO.Rastreabilidade
{
    public class daoAcao
    {

        //static daoAcao() {
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<tbl_CBAcao, CBAcaoDTO>();
        //    });

        //}

        public static List<CBAcaoDTO> Listar()
        {
            using (DTB_CBContext contexto = new DTB_CBContext())
            {
                List<CBAcaoDTO> listaDTO = new List<CBAcaoDTO>();

                var lista = (from l in contexto.tbl_CBAcao
                              select l).ToList();

                AutoMapper.Mapper.Map(lista, listaDTO);

                return listaDTO;
            }
        }

        public static List<CBAcaoDTO> ObterAcoes(string descricao, string tipo)
        {
            using (var contexto = new DTB_CBContext())
            {
                var resultado = (from c in contexto.tbl_CBAcao
                                 //where c.Descricao.Contains(descricao) || c.Tipo.Contains(tipo)
                                 select new CBAcaoDTO()
                                 {
                                     CodAcao = c.CodAcao,
                                     Descricao = c.Descricao,
                                     Tipo = c.Tipo,
                                     FlgAtivo = c.FlgAtivo
                                 }).ToList();

                return resultado;

            }
        }

        public static CBAcaoDTO ObterAcao(int codAcao)
        {
            using (var contexto = new DTB_CBContext())
            {
                var resultado = (from c in contexto.tbl_CBAcao
                                 where c.CodAcao == codAcao
                                 select new CBAcaoDTO()
                                 {
                                     CodAcao = c.CodAcao,
                                     Descricao = c.Descricao,
                                     Tipo = c.Tipo,
                                     FlgAtivo = c.FlgAtivo
                                 }).FirstOrDefault();

                return resultado;

            }
        }

        public static void Gravar(CBAcaoDTO acao)
        {
            using (var contexto = new DTB_CBContext())
            {
                var existe = contexto.tbl_CBAcao.FirstOrDefault(
                        x => x.CodAcao == acao.CodAcao);

                if (existe != null)
                {
                    existe.Descricao = acao.Descricao;
                    existe.Tipo = acao.Tipo;
                    existe.FlgAtivo = acao.FlgAtivo;
                    contexto.SaveChanges();
                }
                else
                {
                    var acaoCB = new tbl_CBAcao()
                    {
                        CodAcao = acao.CodAcao,
                        Descricao = acao.Descricao,
                        Tipo = acao.Tipo,
                        FlgAtivo = acao.FlgAtivo
                    };

                    contexto.tbl_CBAcao.Add(acaoCB);
                    contexto.SaveChanges();
                }
            }
        }

        public static void Remover(CBAcaoDTO acao)
        {
            using (var contexto = new DTB_CBContext())
            {
                var existe = contexto.tbl_CBAcao.FirstOrDefault(
                        x => x.CodAcao == acao.CodAcao);

                if (existe == null) return;

                contexto.tbl_CBAcao.Remove(existe);
                contexto.SaveChanges();
            }
        }
    }
}
