using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
	public partial class CBCadOrigemDTO
	{
		[Display(Name = "C�digo")]
		public string CodOrigem { get; set; }
		[Display(Name = "Descri��o")]
		public string DesOrigem { get; set; }

        [Display(Name = "Ativo")]
        public Nullable<bool> FlgAtivo { get; set; }
	}
}
