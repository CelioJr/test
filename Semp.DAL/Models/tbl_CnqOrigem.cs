using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CnqOrigem
    {
        public string CodOrigem { get; set; }
        public string DesOrigem { get; set; }
    }
}
