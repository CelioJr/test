using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
	public class tbl_EntMovItemMap : EntityTypeConfiguration<tbl_EntMovItem>
	{
		public tbl_EntMovItemMap()
		{
			// Primary Key
			this.HasKey(t => new { t.CodItem, t.NumRomaneio });

			// Properties
			this.Property(t => t.CodItem)
				.IsRequired()
				.HasMaxLength(15)
                .IsUnicode(false);

			this.Property(t => t.NumRomaneio)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.CodModelo)
				.HasMaxLength(15)
                .IsUnicode(false);

			this.Property(t => t.CodDefeito)
				.HasMaxLength(15)
                .IsUnicode(false);

			this.Property(t => t.CodCausa)
				.IsFixedLength()
				.HasMaxLength(4)
                .IsUnicode(false);

			this.Property(t => t.OrigemDefeito)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			this.Property(t => t.DetalheDefeito)
				.HasMaxLength(100)
                .IsUnicode(false);

			this.Property(t => t.NomResp)
				.HasMaxLength(50)
                .IsUnicode(false);

			this.Property(t => t.flgDevolvido)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			this.Property(t => t.DesDevMotivo)
				.HasMaxLength(255)
                .IsUnicode(false);

			this.Property(t => t.NumGrim)
				.IsFixedLength()
				.HasMaxLength(6)
                .IsUnicode(false);

			this.Property(t => t.FlgCritico)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			this.Property(t => t.FlgNecReposicao)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			// Table & Column Mappings
			this.ToTable("tbl_EntMovItem");
			this.Property(t => t.CodItem).HasColumnName("CodItem");
			this.Property(t => t.QtdItem).HasColumnName("QtdItem");
			this.Property(t => t.NumRomaneio).HasColumnName("NumRomaneio");
			this.Property(t => t.CodModelo).HasColumnName("CodModelo");
			this.Property(t => t.CodDefeito).HasColumnName("CodDefeito");
			this.Property(t => t.CodCausa).HasColumnName("CodCausa");
			this.Property(t => t.OrigemDefeito).HasColumnName("OrigemDefeito");
			this.Property(t => t.DetalheDefeito).HasColumnName("DetalheDefeito");
			this.Property(t => t.NomResp).HasColumnName("NomResp");
			this.Property(t => t.flgDevolvido).HasColumnName("flgDevolvido");
			this.Property(t => t.DesDevMotivo).HasColumnName("DesDevMotivo");
			this.Property(t => t.NumGrim).HasColumnName("NumGrim");
			this.Property(t => t.FlgCritico).HasColumnName("FlgCritico");
			this.Property(t => t.FlgNecReposicao).HasColumnName("FlgNecReposicao");
		}
	}
}
