using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_LSAAPItemMap : EntityTypeConfiguration<tbl_LSAAPItem>
    {
        public tbl_LSAAPItemMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.NumAP, t.CodModelo, t.CodItem, t.CodItemAlt });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NumAP)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodItemAlt)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodProcesso)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.CodAplAlt)
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.QtdFrequencia)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.QtdNecessaria)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodIdentificaItem)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodIdentificaRej)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.FlgItemOK)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.FlgApontado)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.NumNa)
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.CodProcesso2)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_LSAAPItem");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.NumAP).HasColumnName("NumAP");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.CodItemAlt).HasColumnName("CodItemAlt");
            this.Property(t => t.CodProcesso).HasColumnName("CodProcesso");
            this.Property(t => t.CodAplAlt).HasColumnName("CodAplAlt");
            this.Property(t => t.QtdFrequencia).HasColumnName("QtdFrequencia");
            this.Property(t => t.QtdNecessaria).HasColumnName("QtdNecessaria");
            this.Property(t => t.QtdProduzida).HasColumnName("QtdProduzida");
            this.Property(t => t.QtdEmpenho).HasColumnName("QtdEmpenho");
            this.Property(t => t.QtdEmpenho2).HasColumnName("QtdEmpenho2");
            this.Property(t => t.QtdSaldoProd).HasColumnName("QtdSaldoProd");
            this.Property(t => t.QtdPaga).HasColumnName("QtdPaga");
            this.Property(t => t.QtdDevolucao).HasColumnName("QtdDevolucao");
            this.Property(t => t.QtdRejeito).HasColumnName("QtdRejeito");
            this.Property(t => t.CodIdentificaItem).HasColumnName("CodIdentificaItem");
            this.Property(t => t.CodIdentificaRej).HasColumnName("CodIdentificaRej");
            this.Property(t => t.FlgItemOK).HasColumnName("FlgItemOK");
            this.Property(t => t.FlgApontado).HasColumnName("FlgApontado");
            this.Property(t => t.NumNa).HasColumnName("NumNa");
            this.Property(t => t.CodProcesso2).HasColumnName("CodProcesso2");
            this.Property(t => t.QtdRecebida).HasColumnName("QtdRecebida");
            this.Property(t => t.OrdConsumo).HasColumnName("OrdConsumo");
        }
    }
}
