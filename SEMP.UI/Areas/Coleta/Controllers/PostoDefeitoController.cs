﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.Rastreabilidade;
using SEMP.Model.DTO;

namespace SEMP.Areas.Coleta.Controllers
{
	public class PostoDefeitoController : Controller
	{
		//
		// GET: /Coleta/PostoDefeito/

		public ActionResult Index(int? linha, int? posto)
		{
			return View();
		}

		public ActionResult Lista(int? linha, int? posto)
		{

			var postodefeito = Defeito.ObterPostosDefeito(linha, posto);

			return View(postodefeito);

		}

		public ActionResult Create()
		{
			var linhas = ObterLinhas();

			ViewBag.CodLinha = new SelectList(linhas, "CodLinha", "DesLinha", null);
			return View();
		}

		[HttpPost]
		public ActionResult Create(CBPostoDefeitoDTO postodefeito)
		{
			try
			{
				var linhas = ObterLinhas();

				ViewBag.CodLinha = new SelectList(linhas, "CodLinha", "DesLinha", postodefeito.CodLinha);

				Defeito.GravarPostoDefeito(postodefeito);

				//return RedirectToAction("Index");
				return Content("Salvo com sucesso");
			}
			catch
			{
				return RedirectToAction("Index");
			}
		}

		private List<CBLinhaDTO> ObterLinhas()
		{
			var linhas = new List<CBLinhaDTO>();
			linhas.Add(new CBLinhaDTO() { DesLinha = "SELECIONE UMA LINHA" });
			linhas.AddRange(Geral.ObterLinhaPorFabrica("FEC"));
			linhas.AddRange(Geral.ObterLinhaPorFabrica("IMC"));
			linhas.AddRange(Geral.ObterLinhaPorFabrica("IAC"));
            linhas.AddRange(Geral.ObterLinhaPorFabrica("LCM"));

			return linhas;
		}

		private List<CBPostoDTO> ObterPostos(int codLinha)
		{
			var postos = new List<CBPostoDTO>();
			postos = Geral.ObterPostoPorLinha(codLinha);

			return postos;
		}

		public ActionResult Delete(int codLinha, int numPosto, string codDefeito)
		{

			var postodefeito = Defeito.ObterPostoDefeito(codLinha, numPosto, codDefeito);

			return View(postodefeito);
		}

		[HttpPost]
		public ActionResult Delete(CBPostoDefeitoDTO postodefeito)
		{
			try
			{
				Defeito.RemoverPostoDefeito(postodefeito);

				return RedirectToAction("Index", new {linha = postodefeito.CodLinha, posto = postodefeito.NumPosto});
			}
			catch
			{
				return View();
			}
		}

		public ActionResult Print(int? codLinha, int? numPosto)
		{
			var defeitos = Defeito.ObterPostosDefeito();

			if (codLinha != null && codLinha != 0) defeitos = defeitos.Where(x => x.CodLinha == codLinha).ToList();

			if (numPosto != null && numPosto != 0) defeitos = defeitos.Where(x => x.NumPosto == numPosto).ToList();

			if (defeitos == null || !defeitos.Any())
				return RedirectToAction("Index");

			return View(defeitos);

		}

		
		public ActionResult GravarPostoDefeito(int CodLinha, int NumPosto, string CodDefeito)
		{
			bool resultado;
			try
			{
				CBPostoDefeitoDTO postodefeito = new CBPostoDefeitoDTO();

				postodefeito.CodLinha = CodLinha;
				postodefeito.NumPosto = NumPosto;
				postodefeito.CodDefeito = CodDefeito;

				Defeito.GravarPostoDefeito(postodefeito);

				resultado = true;
			}
			catch (Exception)
			{
				resultado = false;
			}
			
			return Json(resultado, JsonRequestBehavior.AllowGet);
		}
	}
}
