﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.CNQ
{
	public class Ppm
	{
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public int QuantidadePontos { get; set; }
		public int QuantidadeDefeito { get; set; }
		public string Sinalizacao { get; set; }
		public Double Calculo { get; set; }
		public int QtdFalsaFalha { get; set; }
	}
}
