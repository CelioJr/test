﻿using System;
using System.Collections.Generic;
using RAST.BO.Rastreabilidade;
using SEMP.Model.DTO;
using SEMP.Model.VO;

namespace RAST.BO.Embalagem
{
	public interface IEmbalagem
	{
		Mensagem Embalar_Produto(CBEmbaladaDTO embalagem, List<CBAmarraDTO> listaAmarra, Passagem passagem, CBPostoDTO posto, bool gravarBancoSemp = true);

		Mensagem ValidarLeitura(int tipoAmarra, string leitura, string numAp, string numSerie, string numKitProvisorio, CBLinhaDTO linha);

		Mensagem ValidarProduto(Passagem passagem, String codDrt, string numAp, string leitura, string modelo, string codFab, string codigoCinescopio, CBLinhaDTO linha);

		String AbrirCaixaColetiva(int idLinha, string codModelo, string codFab, string umAP);

		Mensagem FecharCaixaColetiva(string numLote, string codDrt, string justificativa);

		List<CBEmbaladaDTO> ProdutosDeLoteEmAberto(string numAp, string codModelo, int codLinha, string codFab);

		List<CBEmbaladaDTO> RecuperarItensEmbaladosDaCaixaColetivaAberta(string numAp, string codModelo, int codLinha, string codFab);

		Mensagem ImprimirEtiquetasIndividuais(string numAp, string leitura, string codModelo, CBPostoDTO posto, double PesoCaixa);

		Mensagem ImprimirCaixaColetiva(string numeroCaixaColetiva, CBLinhaDTO linha, CBPostoDTO posto);

		int QtdeCaixasColetivasNoLote(string numLote);

		bool AlterarTamanhoCaixaColetiva(int codLinha, int tamanho);

		bool AlterarTamanhoLote(int codLinha, int tamanho);

		String RecuperarLoteAberto(string codFab, string numap, int codLinha);

		String RecuperarLoteAberto(string numAp, string codModelo, int codLinha, string codFab);

		String RecuperarLoteAberto(string codModelo, int codLinha, string codFab);

		String RecuperarNumeroCaixaColetivaAberta(string numAp, string modelo, int codLinha, string codFab);

		String RecuperarNumeroCaixaColetivaAberta(string numAp, int codLinha, string codFab);

		String ObterTotalProduzidoCaixaColetiva(string codFabrica, string numAp, int codLinha);

		String ObterTotalProduzidoCaixaColetiva(string codFabrica, string numAp, int codLinha, string codModelo);
	}
}
