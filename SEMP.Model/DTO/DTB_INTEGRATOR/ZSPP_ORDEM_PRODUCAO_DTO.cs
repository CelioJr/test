namespace SEMP.Model.DTO
{
	using System;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;

	public partial class ZSPP_ORDEM_PRODUCAO_DTO
	{
        [Key]
        [Column(Order = 0)]
        [StringLength(16)]
        public string PACKNO { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        public string EXTROW { get; set; }

        [StringLength(4)]
        public string WERKS { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(12)]
        public string AUFNR { get; set; }

        [StringLength(4)]
        public string TYPE { get; set; }

        [StringLength(40)]
        public string MATNR { get; set; }

        [StringLength(8)]
        public string ARBPL { get; set; }

        public decimal? OQTY { get; set; }

        public DateTime? DTREF { get; set; }

        [StringLength(12)]
        public string ITEM { get; set; }
    }
}
