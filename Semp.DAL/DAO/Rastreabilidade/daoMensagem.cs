﻿using System;
using System.Linq;
using SEMP.DAL.Common;
using SEMP.DAL.Models;
using SEMP.Model.DTO;
using SEMP.Model.VO;

namespace SEMP.DAL.DAO.Rastreabilidade
{
    public class daoMensagem
    {
        //static daoMensagem()
        //{
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<tbl_CBMensagens, CBMensagensDTO>();
        //        cfg.CreateMap<CBDefeitoRastreamentoDTO, tbl_CBDefeitoRastreamento>();
        //    });
        //}

        #region Consultas

        public static CBMensagensDTO ObterMensagensByCodigo(string codigo)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var msg = (from i in dtbCb.tbl_CBMensagens
                           where i.CodMensagem == codigo
                           select i).FirstOrDefault();

                CBMensagensDTO cBMensagensDTO = new CBMensagensDTO();

                AutoMapper.Mapper.Map(msg, cBMensagensDTO);

                return cBMensagensDTO;
            }

        }

        public static Mensagem ObterMensagem(string codMensagem)
        {
            using (DTB_CBContext dtbCb = new DTB_CBContext())
            {
                var m = (from i in dtbCb.tbl_CBMensagens
                         where i.CodMensagem == codMensagem
                         select i).FirstOrDefault();

                Mensagem mensagem = new Mensagem();

                mensagem.CodMensagem = codMensagem;
                mensagem.DesMensagem = m.DesMensagem;
                mensagem.DesCor = m.DesCor;
                mensagem.DesSom = m.DesSom;
                mensagem.DatStatus = daoUtil.GetDate();

                return mensagem;
            }

        }

        #endregion

        #region Gravações

        public static string InsertDefeitoRastreamento(CBDefeitoRastreamentoDTO dados)
        {

            try
            {
                tbl_CBDefeitoRastreamento defeito = new tbl_CBDefeitoRastreamento();

                AutoMapper.Mapper.Map(dados, defeito);

                using (DTB_CBContext dtbCb = new DTB_CBContext())
                {
                    dtbCb.tbl_CBDefeitoRastreamento.Add(defeito);

                    dtbCb.SaveChanges();
                    return "";
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                string erro = "";
                foreach (var eve in e.EntityValidationErrors)
                {
                    erro = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        erro += String.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }

                return erro;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion
    }
}
