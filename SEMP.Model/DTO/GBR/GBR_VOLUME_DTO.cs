using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_VOLUME_DTO
	{
        public int COD_VOLUME { get; set; }
        public Nullable<int> COD_SKU { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
    }
}
