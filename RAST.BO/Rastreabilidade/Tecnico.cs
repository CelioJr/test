﻿using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.DTO;
using SEMP.Model.VO.Rastreabilidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RAST.BO.Rastreabilidade
{
    public class Tecnico
    {
        public static List<CnqOrigemDTO> ListaOrigem()
        {
            return daoTecnico.ListaOrigem();
        }

        public static List<CBCadDefeitoDTO> CodListDefeitos()
        {
            return daoTecnico.CodListDefeitos();
        }

        public static List<CnqCausaDTO> ListaCausa()
        {
            return daoTecnico.ListaCausa();
        }

        public static bool VerificaSeExisteDefeitoAberto(string serial)
        {
            return daoTecnico.VerificaSeExisteDefeitoAberto(serial);
        }

        public static bool VerificaSeDefeitoFechado(string serial)
        {
            return daoTecnico.VerificaSeDefeitoFechado(serial);
        }

        public static CBDefeitoDTO GetDefeito(string serial)
        {
            return daoTecnico.GetDefeito(serial);
        }

        public static CBDefeitoDTO GetDefeito(string serial, int flgRevisado)
        {
            return daoTecnico.GetDefeito(serial, flgRevisado);
        }

        public static void SalvarDefeito(CBDefeitoDTO defeito)
        {
            daoTecnico.SalvarDefeito(defeito);
        }

        public static void SalvarDefeitoCodDefeitoExistente(CBDefeitoDTO defeitoDTO, int flgRevisado)
        {
            daoTecnico.SalvarDefeitoCodDefeitoExistente(defeitoDTO, flgRevisado);
        }

        public static void SalvarDefeito(CBDefeitoDTO defeito, int flgRevisado)
        {
            daoTecnico.SalvarDefeito(defeito, flgRevisado);
        }

        public static void AdicionarDefeito(CBDefeitoDTO defeito, bool alterarData = true)
        {
            daoTecnico.AdicionarDefeito(defeito, alterarData);
        }

        public static void ExcluirDefeito(CBDefeitoDTO defeitoDTO)
        {
            daoTecnico.ExcluirDefeito(defeitoDTO);
        }

        public static int TransferirDefeito(CBDefeitoDTO defeitoDTO)
        {
            return daoTecnico.TransferirDefeito(defeitoDTO);
        }

        public static bool TranferirPassagem(int CodLinha, string NumSerie)
        {
            return daoTecnico.TranferirPassagem(CodLinha, NumSerie);
        }

		public static bool TranferirPassagemItem (int codLinha, int numPosto, string numSerie)
		{
			return daoTecnico.TranferirPassagemItem(codLinha, numPosto, numSerie);
		}

        public static bool TranferirAmarracao(int CodLinha, string NumSerie)
        {
            return daoTecnico.TranferirAmarracao(CodLinha, NumSerie);
        }

		public static bool TranferirAmarracaoItem(string numSerie, string numECB)
		{
			return daoTecnico.TranferirAmarracaoItem(numSerie, numECB);
		}

        public static int GetTotalDefeitoResolvido(int codLinha, int numPosto, int turno)
        {
            return daoTecnico.GetTotalDefeitoResolvido(codLinha, numPosto, turno);
        }

        public static string ValidarInserirDefeito(string serial)
        {
            string regexProduto = @"^\d{6}[A-Za-z]{1}\d{1}[A-Za-z]{2}\d{6}$";
            string regexPlaca = @"^\d{9}|d{12}$";

            //verifica se é produto e placa
            if (ValidarExpressaoRegular(serial, regexProduto) == false
                &&
                ValidarExpressaoRegular(serial, regexPlaca) == false)
            {
                return "código inserido é inválido.";
            }

            return daoTecnico.ValidarInserirDefeito(serial);
        }

        public static bool ValidarExpressaoRegular(string valor, string expressao)
        {
            var regex = new Regex(expressao);
            bool retorno = regex.IsMatch(valor);
            return retorno;
        }

        public static List<TecnicoPosicao> GetPosicao(string NumSerie, int Codlinha)
        {
            return daoTecnico.GetPosicao(NumSerie, Codlinha);
        }

        public static List<TecnicoPosicao> GetPosicaoPlaca(string NumSerie, int Codlinha)
        {
            return daoTecnico.GetPosicaoPlaca(NumSerie, Codlinha);
        }

		public static List<TecnicoPosicao> GetPosicaoPainel(string NumSerie)
		{
			return daoTecnico.GetPosicaoPainel(NumSerie);
		}

        public static CBDefeitoDTO GetDefeitoByCodDefeitoExistente(string serial, string codDefeito)
        {
            return daoTecnico.GetDefeitoByCodDefeitoExistente(serial, codDefeito);
        }
    }
}
