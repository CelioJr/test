﻿using System;
using System.Collections.Generic;
using System.Linq;
using SEMP.DAL.Models;
using SEMP.Model.DTO;
using SEMP.Model.VO.Rastreabilidade.Relatorios;

namespace SEMP.DAL.DAO.Apontaprod
{
	public class daoAcompanhamento
	{

		#region Universal
		public static List<LeiturasMF> ObterLeituras(string codFab, string numAP, DateTime? Datinicio, DateTime? DatFim, String CodLinha)
		{
			using (var dtbCB = new DTB_CBContext())
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{

				var ap = dtbSim.tbl_LSAAP.FirstOrDefault(x => x.CodFab == codFab && x.NumAP == numAP);

				IQueryable<LeiturasMF> retorno;

				if (ap != null && ap.CodProcesso.StartsWith("03"))
				{

					retorno = (from p in dtbSim.viw_BarrasProducao
								   join modelo in dtbSim.tbl_Item on p.CodModelo equals modelo.CodItem
								   join acessorio in dtbSim.tbl_Item on p.CodControle equals acessorio.CodItem into i1
								   join painel in dtbSim.tbl_Item on p.CodCinescopio equals painel.CodItem into i2
								   join outro in dtbSim.tbl_Item on p.CodOutro equals outro.CodItem into i3
								   from leftAcessorio in i1.DefaultIfEmpty()
								   from leftPainel in i2.DefaultIfEmpty()
								   from leftOutro in i3.DefaultIfEmpty()
								   where p.CodFab == codFab && p.NumAP == numAP
								   select new LeiturasMF()
								   {
									   CodFab = p.CodFab,
									   NumAP = p.NumAP,
									   CodLinha = p.CodLinha,
									   NumTurno = (int)p.NumTurno,
									   Data = p.DatLeitura,
									   DatReferencia = p.DatReferencia,
									   CodModelo = p.CodModelo,
									   DesModelo = modelo.DesItem,
									   NumSerieModelo = p.NumSerieModelo,
									   CodAcessorio = p.CodControle,
									   DesAcessorio = leftAcessorio.DesItem,
									   NumSerieAcessorio = p.NumSerieControle,
									   CodPainel = p.CodCinescopio,
									   DesPainel = leftPainel.DesItem,
									   NumSeriePainel = p.NumSerieCinescopio,
									   CodOutro = p.CodOutro,
									   DesOutro = leftOutro.DesItem,
									   NumSerieOutro = p.NumSerieOutro,
									   CodOperador = p.CodOperador,
									   CodTestador = p.CodTestador,
									   NumLote = p.NumPal,
									   DigVerificador = p.DigVerificador
								   }).OrderBy(x => x.Data);
				}
				else
				{
					retorno = (from p in dtbSim.viw_BarrasProducaoPlaca
							   join modelo in dtbSim.tbl_Item on p.CodModelo equals modelo.CodItem
							   where p.CodFab == codFab && p.NumAP == numAP
							   select new LeiturasMF()
							   {
								   CodFab = p.CodFab,
								   NumAP = p.NumAP,
								   CodLinha = p.CodLinha,
								   NumTurno = (int)p.NumTurno,
								   Data = p.DatLeitura,

								   DatReferencia = p.DatReferencia,
								   CodModelo = p.CodModelo,
								   DesModelo = modelo.DesItem,
								   NumSerieModelo = p.NumSerieModelo,
								   DatProcessadoSAP = p.DatProcessadoSAP,

								   CodAcessorio = "",
								   DesAcessorio = "",
								   NumSerieAcessorio = "",
								   CodPainel = "",
								   DesPainel = "",
								   NumSeriePainel = "",
								   CodOutro = "",
								   DesOutro = "",
								   NumSerieOutro = "",
								   CodOperador = p.CodOperador,
								   CodTestador = "",
								   NumLote = "",
								   DigVerificador = ""
							   }).OrderBy(x => x.Data);
				}



				var ret = new List<LeiturasMF>();

				if (!String.IsNullOrEmpty(CodLinha )) {
					retorno = retorno.Where(x => x.CodLinha == CodLinha);
				}

				if (Datinicio != null && DatFim != null) {
					retorno = retorno.Where(x => x.DatReferencia >= Datinicio && x.DatReferencia <= DatFim);

				}
				ret = retorno.ToList();

				return ret;
			
			}
		}


		public static List<ProducaoLinhaMinuto> ObterLeiturasSimplesFase(DateTime datReferencia, string fase)
		{
			using (var contexto = new DTB_CBContext())
			{

					var retorno = (from p in contexto.tbl_CBEmbalada
								   join l in contexto.tbl_CBLinha on p.CodLinha equals l.CodLinha
								   where p.DatReferencia == datReferencia && l.Setor == fase
								   group p by new { Hora = p.DatLeitura.Hour, Minuto = p.DatLeitura.Minute, p.DatReferencia, p.CodLinha, l.DesLinha } into grupo
								   select new ProducaoLinhaMinuto()
								   {
									   CodLinha = grupo.Key.CodLinha.ToString(),
									   DesLinha = grupo.Key.DesLinha,
									   DatReferencia = grupo.Key.DatReferencia.Value,
									   Hora = grupo.Key.Hora,
									   Minuto = grupo.Key.Minuto,
									   QtdLeitura = grupo.Count()
								   }).OrderBy(x => x.Hora).ToList();

					return retorno;
			}
		}
		#endregion

		#region Montagem Final
		public static List<LeiturasMF> ObterLeituras(DateTime Datinicio, DateTime DatFim)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext()) {
				DateTime menorLeitura = dtbSim.tbl_BarrasProducao.Min(x => x.DatLeitura);

				if (Datinicio < menorLeitura)
				{

					var retorno = (from p in dtbSim.viw_BarrasProducao
						join modelo in dtbSim.tbl_Item on p.CodModelo equals modelo.CodItem
						join acessorio in dtbSim.tbl_Item on p.CodControle equals acessorio.CodItem into i1
						join painel in dtbSim.tbl_Item on p.CodCinescopio equals painel.CodItem into i2
						join outro in dtbSim.tbl_Item on p.CodOutro equals outro.CodItem into i3
						from leftAcessorio in i1.DefaultIfEmpty()
						from leftPainel in i2.DefaultIfEmpty()
						from leftOutro in i3.DefaultIfEmpty()
						where p.DatLeitura >= Datinicio && p.DatLeitura <= DatFim
						select new LeiturasMF()
						{
							CodFab = p.CodFab,
							NumAP = p.NumAP,
							CodLinha = p.CodLinha,
							NumTurno = (int) p.NumTurno,
							Data = p.DatLeitura,
							DatReferencia = p.DatReferencia,
							CodModelo = p.CodModelo,
							DesModelo = modelo.DesItem,
							NumSerieModelo = p.NumSerieModelo,
							CodAcessorio = p.CodControle,
							DesAcessorio = leftAcessorio.DesItem,
							NumSerieAcessorio = p.NumSerieControle,
							CodPainel = p.CodCinescopio,
							DesPainel = leftPainel.DesItem,
							NumSeriePainel = p.NumSerieCinescopio,
							CodOutro = p.CodOutro,
							DesOutro = leftOutro.DesItem,
							NumSerieOutro = p.NumSerieOutro,
							CodOperador = p.CodOperador,
							CodTestador = p.CodTestador,
							NumLote = p.NumPal,
							DigVerificador = p.DigVerificador
						}).OrderBy(x => x.Data).ToList();

					return retorno;
				}
				else
				{
					var retorno = (from p in dtbSim.tbl_BarrasProducao
						join modelo in dtbSim.tbl_Item on p.CodModelo equals modelo.CodItem
						join acessorio in dtbSim.tbl_Item on p.CodControle equals acessorio.CodItem into i1
						join painel in dtbSim.tbl_Item on p.CodCinescopio equals painel.CodItem into i2
						join outro in dtbSim.tbl_Item on p.CodOutro equals outro.CodItem into i3
						from leftAcessorio in i1.DefaultIfEmpty()
						from leftPainel in i2.DefaultIfEmpty()
						from leftOutro in i3.DefaultIfEmpty()
						where p.DatLeitura >= Datinicio && p.DatLeitura <= DatFim
						select new LeiturasMF()
						{
							CodFab = p.CodFab,
							NumAP = p.NumAP,
							CodLinha = p.CodLinha,
							NumTurno = (int) p.NumTurno,
							Data = p.DatLeitura,
							DatReferencia = p.DatReferencia,
							CodModelo = p.CodModelo,
							DesModelo = modelo.DesItem,
							NumSerieModelo = p.NumSerieModelo,
							CodAcessorio = p.CodControle,
							DesAcessorio = leftAcessorio.DesItem,
							NumSerieAcessorio = p.NumSerieControle,
							CodPainel = p.CodCinescopio,
							DesPainel = leftPainel.DesItem,
							NumSeriePainel = p.NumSerieCinescopio,
							CodOutro = p.CodOutro,
							DesOutro = leftOutro.DesItem,
							NumSerieOutro = p.NumSerieOutro,
							CodOperador = p.CodOperador,
							CodTestador = p.CodTestador,
							NumLote = p.NumPal,
							DigVerificador = p.DigVerificador
						}).OrderBy(x => x.Data).ToList();

					return retorno;
				}
			}
		}

		public static List<viw_BarrasSAPContingenciaDTO> ObterLeiturasSAP(DateTime Datinicio, DateTime DatFim)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var retorno = (from p in dtbSim.viw_BarrasSAPContingencia
								where p.DatReferencia >= Datinicio && p.DatReferencia <= DatFim
								select new viw_BarrasSAPContingenciaDTO()
								{
								Werks	  = p.Werks,
								AUART	  = p.AUART,
								AUFNR	  = p.AUFNR,
								MATNR1	  = p.MATNR1,
								EAN13	  = p.EAN13,
								SERIAL1	  = p.SERIAL1,
								CREDATE1  = p.CREDATE1,
								MATNR2	  = p.MATNR2,
								SERIAL2	  = p.SERIAL2,
								CREDATE2  = p.CREDATE2,
								PRUEFLOS  = p.PRUEFLOS,
								CRETIME2  = p.CRETIME2,
								CRETIME1  = p.CRETIME1,
								ARBPL	  = p.ARBPL,
								CODLINHA  = p.CODLINHA,
								CODPOSTO  = p.CODPOSTO,
								DRT		  = p.DRT,
								TPAMAR	  = p.TPAMAR,
								GESME	  = p.GESME,
								STATCONF  = p.STATCONF,
								DTCONF	  = p.DTCONF,
								MBLNR	  = p.MBLNR,
								STATVD	  = p.STATVD,
								STBCKFLUSH= p.STBCKFLUSH,
								DTBCKFLUSH= p.DTBCKFLUSH,
								USERNAME  = p.USERNAME,
								SISAP	  = p.SISAP,
								PACKNO	  = p.PACKNO,
								VCODE	  = p.VCODE,
								OBA		  = p.OBA,
								DatReferencia = p.DatReferencia
								}).OrderBy(x => x.DatReferencia).ToList();

				return retorno;
			
			}
		}

		public static string ObterSeriePainel(string NumSerieProduto, string CodPainel) {
			using (var contexto = new DTB_CBContext()) {

				var dado = contexto.tbl_CBAmarra.FirstOrDefault(x => x.NumSerie == NumSerieProduto && x.CodItem == CodPainel);

				if (dado != null) {
					return dado.NumECB;
				}

				return "";

			}

		}

		public static List<LeiturasMF> ObterLeituras(DateTime Datinicio, DateTime DatFim, String CodLinha)
		{
			using (var dtbCB = new DTB_CBContext())
			using (DTB_SIMContext dtbSim = new DTB_SIMContext()) {
				DateTime menorLeitura = dtbSim.tbl_BarrasProducao.Min(x => x.DatLeitura);

				if (Datinicio < menorLeitura)
				{

					var retorno = (from p in dtbSim.viw_BarrasProducao
								   join modelo in dtbSim.tbl_Item on p.CodModelo equals modelo.CodItem
								   join acessorio in dtbSim.tbl_Item on p.CodControle equals acessorio.CodItem into i1
								   join painel in dtbSim.tbl_Item on p.CodCinescopio equals painel.CodItem into i2
								   join outro in dtbSim.tbl_Item on p.CodOutro equals outro.CodItem into i3
								   from leftAcessorio in i1.DefaultIfEmpty()
								   from leftPainel in i2.DefaultIfEmpty()
								   from leftOutro in i3.DefaultIfEmpty()
								   where p.DatLeitura >= Datinicio && p.DatLeitura <= DatFim && p.CodLinha == CodLinha
								   select new LeiturasMF()
								   {
									   CodFab = p.CodFab,
									   NumAP = p.NumAP,
									   CodLinha = p.CodLinha,
									   NumTurno = (int)p.NumTurno,
									   Data = p.DatLeitura,
									   DatReferencia = p.DatReferencia,
									   CodModelo = p.CodModelo,
									   DesModelo = modelo.DesItem,
									   NumSerieModelo = p.NumSerieModelo,
									   CodAcessorio = p.CodControle,
									   DesAcessorio = leftAcessorio.DesItem,
									   NumSerieAcessorio = p.NumSerieControle,
									   CodPainel = p.CodCinescopio,
									   DesPainel = leftPainel.DesItem,
									   NumSeriePainel = p.NumSerieCinescopio,
									   CodOutro = p.CodOutro,
									   DesOutro = leftOutro.DesItem,
									   NumSerieOutro = p.NumSerieOutro,
									   CodOperador = p.CodOperador,
									   CodTestador = p.CodTestador,
									   NumLote = p.NumPal,
									   DigVerificador = p.DigVerificador
								   }).OrderBy(x => x.Data).ToList();

					//retorno.ForEach(x => x.NumSeriePainel = ObterSeriePainel(x.CodModelo + x.DigVerificador + x.NumSerieModelo, x.CodPainel));

					return retorno;
				}
				else
				{
					var retorno = (from p in dtbSim.tbl_BarrasProducao
								   join modelo in dtbSim.tbl_Item on p.CodModelo equals modelo.CodItem
								   join acessorio in dtbSim.tbl_Item on p.CodControle equals acessorio.CodItem into i1
								   join painel in dtbSim.tbl_Item on p.CodCinescopio equals painel.CodItem into i2
								   join outro in dtbSim.tbl_Item on p.CodOutro equals outro.CodItem into i3
								   from leftAcessorio in i1.DefaultIfEmpty()
								   from leftPainel in i2.DefaultIfEmpty()
								   from leftOutro in i3.DefaultIfEmpty()
								   where p.DatLeitura >= Datinicio && p.DatLeitura <= DatFim && p.CodLinha == CodLinha
								   select new LeiturasMF()
								   {
									   CodFab = p.CodFab,
									   NumAP = p.NumAP,
									   CodLinha = p.CodLinha,
									   NumTurno = (int)p.NumTurno,
									   Data = p.DatLeitura,
									   DatReferencia = p.DatReferencia,
									   CodModelo = p.CodModelo,
									   DesModelo = modelo.DesItem,
									   NumSerieModelo = p.NumSerieModelo,
									   CodAcessorio = p.CodControle,
									   DesAcessorio = leftAcessorio.DesItem,
									   NumSerieAcessorio = p.NumSerieControle,
									   CodPainel = p.CodCinescopio,
									   DesPainel = leftPainel.DesItem,
									   NumSeriePainel = p.NumSerieCinescopio,
									   CodOutro = p.CodOutro,
									   DesOutro = leftOutro.DesItem,
									   NumSerieOutro = p.NumSerieOutro,
									   CodOperador = p.CodOperador,
									   CodTestador = p.CodTestador,
									   NumLote = p.NumPal,
									   DigVerificador = p.DigVerificador
								   }).OrderBy(x => x.Data).ToList();

					//retorno.ForEach(x => x.NumSeriePainel = ObterSeriePainel(x.CodModelo + x.DigVerificador + x.NumSerieModelo, x.CodPainel));

					return retorno;
				}
			}
		}

		public static List<LeiturasMF> ObterLeituras(DateTime Datinicio, DateTime DatFim, String CodLinha, String CodModelo)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext()) {
				DateTime menorLeitura = dtbSim.tbl_BarrasProducao.Min(x => x.DatLeitura);

				if (Datinicio < menorLeitura)
				{

					var retorno = (from p in dtbSim.viw_BarrasProducao
						join modelo in dtbSim.tbl_Item on p.CodModelo equals modelo.CodItem
						join acessorio in dtbSim.tbl_Item on p.CodControle equals acessorio.CodItem into i1
						join painel in dtbSim.tbl_Item on p.CodCinescopio equals painel.CodItem into i2
						join outro in dtbSim.tbl_Item on p.CodOutro equals outro.CodItem into i3
						from leftAcessorio in i1.DefaultIfEmpty()
						from leftPainel in i2.DefaultIfEmpty()
						from leftOutro in i3.DefaultIfEmpty()
						where p.DatLeitura >= Datinicio && p.DatLeitura <= DatFim && p.CodLinha == CodLinha && p.CodModelo == CodModelo
						select new LeiturasMF()
						{
							CodFab = p.CodFab,
							NumAP = p.NumAP,
							CodLinha = p.CodLinha,
							NumTurno = (int) p.NumTurno,
							Data = p.DatLeitura,
							DatReferencia = p.DatReferencia,
							CodModelo = p.CodModelo,
							DesModelo = modelo.DesItem,
							NumSerieModelo = p.NumSerieModelo,
							CodAcessorio = p.CodControle,
							DesAcessorio = leftAcessorio.DesItem,
							NumSerieAcessorio = p.NumSerieControle,
							CodPainel = p.CodCinescopio,
							DesPainel = leftPainel.DesItem,
							NumSeriePainel = p.NumSerieCinescopio,
							CodOutro = p.CodOutro,
							DesOutro = leftOutro.DesItem,
							NumSerieOutro = p.NumSerieOutro,
							CodOperador = p.CodOperador,
							CodTestador = p.CodTestador,
							NumLote = p.NumPal,
							DigVerificador = p.DigVerificador
						}).OrderBy(x => x.Data).ToList();

					return retorno;
				}
				else
				{
					var retorno = (from p in dtbSim.tbl_BarrasProducao
						join modelo in dtbSim.tbl_Item on p.CodModelo equals modelo.CodItem
						join acessorio in dtbSim.tbl_Item on p.CodControle equals acessorio.CodItem into i1
						join painel in dtbSim.tbl_Item on p.CodCinescopio equals painel.CodItem into i2
						join outro in dtbSim.tbl_Item on p.CodOutro equals outro.CodItem into i3
						from leftAcessorio in i1.DefaultIfEmpty()
						from leftPainel in i2.DefaultIfEmpty()
						from leftOutro in i3.DefaultIfEmpty()
						where p.DatLeitura >= Datinicio && p.DatLeitura <= DatFim && p.CodLinha == CodLinha && p.CodModelo == CodModelo
						select new LeiturasMF()
						{
							CodFab = p.CodFab,
							NumAP = p.NumAP,
							CodLinha = p.CodLinha,
							NumTurno = (int) p.NumTurno,
							Data = p.DatLeitura,
							DatReferencia = p.DatReferencia,
							CodModelo = p.CodModelo,
							DesModelo = modelo.DesItem,
							NumSerieModelo = p.NumSerieModelo,
							CodAcessorio = p.CodControle,
							DesAcessorio = leftAcessorio.DesItem,
							NumSerieAcessorio = p.NumSerieControle,
							CodPainel = p.CodCinescopio,
							DesPainel = leftPainel.DesItem,
							NumSeriePainel = p.NumSerieCinescopio,
							CodOutro = p.CodOutro,
							DesOutro = leftOutro.DesItem,
							NumSerieOutro = p.NumSerieOutro,
							CodOperador = p.CodOperador,
							CodTestador = p.CodTestador,
							NumLote = p.NumPal,
							DigVerificador = p.DigVerificador
						}).OrderBy(x => x.Data).ToList();

					return retorno;
				}
			}
		}

		public static List<LeiturasMF> ObterLeiturasPorModelo(String Codigo, String NumSerie)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var retorno = (from p in dtbSim.viw_BarrasProducao
					join modelo in dtbSim.tbl_Item on p.CodModelo equals modelo.CodItem
					join acessorio in dtbSim.tbl_Item on p.CodControle equals acessorio.CodItem into i1
					join painel in dtbSim.tbl_Item on p.CodCinescopio equals painel.CodItem into i2
					join outro in dtbSim.tbl_Item on p.CodOutro equals outro.CodItem into i3
					join linha in dtbSim.tbl_Linha on p.CodLinha equals linha.CodLinha into l1
					from leftAcessorio in i1.DefaultIfEmpty()
					from leftPainel in i2.DefaultIfEmpty()
					from leftOutro in i3.DefaultIfEmpty()
					from leftLinha in l1.DefaultIfEmpty()
					where p.CodModelo == Codigo && p.NumSerieModelo == NumSerie
					select new LeiturasMF()
					{
						CodFab = p.CodFab,
						NumAP = p.NumAP,
						CodLinha = p.CodLinha,
						DesLinha = leftLinha.DesLinha,
						NumTurno = (int) p.NumTurno,
						Data = p.DatLeitura,
						DatReferencia = p.DatReferencia,
						CodModelo = p.CodModelo,
						DesModelo = modelo.DesItem,
						NumSerieModelo = p.NumSerieModelo,
						CodAcessorio = p.CodControle,
						DesAcessorio = leftAcessorio.DesItem,
						NumSerieAcessorio = p.NumSerieControle,
						CodPainel = p.CodCinescopio,
						DesPainel = leftPainel.DesItem,
						NumSeriePainel = p.NumSerieCinescopio,
						CodOutro = p.CodOutro,
						DesOutro = leftOutro.DesItem,
						NumSerieOutro = p.NumSerieOutro,
						CodOperador = p.CodOperador,
						CodTestador = p.CodTestador,
						NumLote = p.NumPal,
						DigVerificador = p.DigVerificador
					});

				return retorno.ToList();
			}
		}

		public static List<LeiturasMF> ObterLeiturasPorAcessorio(String Codigo, String NumSerie)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var retorno = (from p in dtbSim.viw_BarrasProducao
					join modelo in dtbSim.tbl_Item on p.CodModelo equals modelo.CodItem
					join acessorio in dtbSim.tbl_Item on p.CodControle equals acessorio.CodItem into i1
					join painel in dtbSim.tbl_Item on p.CodCinescopio equals painel.CodItem into i2
					join outro in dtbSim.tbl_Item on p.CodOutro equals outro.CodItem into i3
					from leftAcessorio in i1.DefaultIfEmpty()
					from leftPainel in i2.DefaultIfEmpty()
					from leftOutro in i3.DefaultIfEmpty()
					where p.CodControle == Codigo && p.NumSerieControle == NumSerie
					select new LeiturasMF()
					{
						CodFab = p.CodFab,
						NumAP = p.NumAP,
						CodLinha = p.CodLinha,
						NumTurno = (int) p.NumTurno,
						Data = p.DatLeitura,
						DatReferencia = p.DatReferencia,
						CodModelo = p.CodModelo,
						DesModelo = modelo.DesItem,
						NumSerieModelo = p.NumSerieModelo,
						CodAcessorio = p.CodControle,
						DesAcessorio = leftAcessorio.DesItem,
						NumSerieAcessorio = p.NumSerieControle,
						CodPainel = p.CodCinescopio,
						DesPainel = leftPainel.DesItem,
						NumSeriePainel = p.NumSerieCinescopio,
						CodOutro = p.CodOutro,
						DesOutro = leftOutro.DesItem,
						NumSerieOutro = p.NumSerieOutro,
						CodOperador = p.CodOperador,
						CodTestador = p.CodTestador,
						NumLote = p.NumPal,
						DigVerificador = p.DigVerificador
					}).OrderBy(x => x.Data).ToList();

				return retorno;
			}
		}

		public static List<LeiturasMF> ObterLeiturasPorPainel(String Codigo, String NumSerie)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var retorno = (from p in dtbSim.viw_BarrasProducao
					join modelo in dtbSim.tbl_Item on p.CodModelo equals modelo.CodItem
					join acessorio in dtbSim.tbl_Item on p.CodControle equals acessorio.CodItem into i1
					join painel in dtbSim.tbl_Item on p.CodCinescopio equals painel.CodItem into i2
					join outro in dtbSim.tbl_Item on p.CodOutro equals outro.CodItem into i3
					from leftAcessorio in i1.DefaultIfEmpty()
					from leftPainel in i2.DefaultIfEmpty()
					from leftOutro in i3.DefaultIfEmpty()
					where p.CodCinescopio == Codigo && p.NumSerieCinescopio == NumSerie
					select new LeiturasMF()
					{
						CodFab = p.CodFab,
						NumAP = p.NumAP,
						CodLinha = p.CodLinha,
						NumTurno = (int) p.NumTurno,
						Data = p.DatLeitura,
						DatReferencia = p.DatReferencia,
						CodModelo = p.CodModelo,
						DesModelo = modelo.DesItem,
						NumSerieModelo = p.NumSerieModelo,
						CodAcessorio = p.CodControle,
						DesAcessorio = leftAcessorio.DesItem,
						NumSerieAcessorio = p.NumSerieControle,
						CodPainel = p.CodCinescopio,
						DesPainel = leftPainel.DesItem,
						NumSeriePainel = p.NumSerieCinescopio,
						CodOutro = p.CodOutro,
						DesOutro = leftOutro.DesItem,
						NumSerieOutro = p.NumSerieOutro,
						CodOperador = p.CodOperador,
						CodTestador = p.CodTestador,
						NumLote = p.NumPal,
						DigVerificador = p.DigVerificador
					}).OrderBy(x => x.Data).ToList();

				return retorno;
			}
		}

		public static List<LeiturasMF> ObterLeiturasPorOutro(String Codigo, String NumSerie)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var retorno = (from p in dtbSim.viw_BarrasProducao
					join modelo in dtbSim.tbl_Item on p.CodModelo equals modelo.CodItem
					join acessorio in dtbSim.tbl_Item on p.CodControle equals acessorio.CodItem into i1
					join painel in dtbSim.tbl_Item on p.CodCinescopio equals painel.CodItem into i2
					join outro in dtbSim.tbl_Item on p.CodOutro equals outro.CodItem into i3
					from leftAcessorio in i1.DefaultIfEmpty()
					from leftPainel in i2.DefaultIfEmpty()
					from leftOutro in i3.DefaultIfEmpty()
					where p.CodOutro == Codigo && p.NumSerieOutro == NumSerie
					select new LeiturasMF()
					{
						CodFab = p.CodFab,
						NumAP = p.NumAP,
						CodLinha = p.CodLinha,
						NumTurno = (int) p.NumTurno,
						Data = p.DatLeitura,
						DatReferencia = p.DatReferencia,
						CodModelo = p.CodModelo,
						DesModelo = modelo.DesItem,
						NumSerieModelo = p.NumSerieModelo,
						CodAcessorio = p.CodControle,
						DesAcessorio = leftAcessorio.DesItem,
						NumSerieAcessorio = p.NumSerieControle,
						CodPainel = p.CodCinescopio,
						DesPainel = leftPainel.DesItem,
						NumSeriePainel = p.NumSerieCinescopio,
						CodOutro = p.CodOutro,
						DesOutro = leftOutro.DesItem,
						NumSerieOutro = p.NumSerieOutro,
						CodOperador = p.CodOperador,
						CodTestador = p.CodTestador,
						NumLote = p.NumPal,
						DigVerificador = p.DigVerificador
					}).OrderBy(x => x.Data).ToList();

				return retorno;
			}
		}

		/// <summary>
		/// Rotina para buscar as leituras de um período, retornando uma coleção mais simples de dados para depois fazer cálculos
		/// </summary>
		/// <param name="Datinicio"></param>
		/// <param name="DatFim"></param>
		/// <returns></returns>
		public static List<ProducaoLinhaMinuto> ObterLeiturasSimples(DateTime datReferencia)
		{
			using (var dtbSim = new DTB_SIMContext())
			{
				DateTime menorLeitura = dtbSim.tbl_BarrasProducao.Min(x => x.DatLeitura);

				if (datReferencia < menorLeitura)
				{

					var retorno = (from p in dtbSim.viw_BarrasProducao
									join l in dtbSim.tbl_Linha on p.CodLinha equals l.CodLinha
								   where p.DatReferencia == datReferencia
								   group p by new { Hora = p.DatLeitura.Hour, Minuto = p.DatLeitura.Minute, p.DatReferencia, p.CodLinha, l.DesLinha } into grupo
								   select new ProducaoLinhaMinuto()
								   {
									   CodLinha = grupo.Key.CodLinha,
									   DesLinha = grupo.Key.DesLinha,
									   DatReferencia = grupo.Key.DatReferencia ?? datReferencia,
									   Hora = grupo.Key.Hora,
									   Minuto = grupo.Key.Minuto,
									   QtdLeitura = grupo.Count()
								   }).OrderBy(x => x.Hora).ToList();

					return retorno;
				}
				else
				{
					var mesRef = datReferencia.AddDays(-datReferencia.Day).AddDays(1);

					var retorno = (from p in dtbSim.tbl_BarrasProducao
								   join l in dtbSim.tbl_Linha on p.CodLinha equals l.CodLinha
								   join ap in dtbSim.tbl_LSAAP on p.NumAP equals ap.NumAP
								   from prog in dtbSim.tbl_ProgDiaJit.Where(x => x.CodModelo == p.CodModelo && x.DatProducao == datReferencia && x.CodLinha == p.CodLinha && x.FlagTP == (ap.TipAP == "N" ? "" : ap.TipAP)).DefaultIfEmpty()
								   from capac in dtbSim.tbl_CapacLinha.Where(x => x.CodModelo == p.CodModelo && x.DatProducao == mesRef && x.CodLinha == p.CodLinha).DefaultIfEmpty()
								   where p.DatReferencia == datReferencia
								   group new { p, capac, prog } by new { Hora = p.DatLeitura.Hour, Minuto = p.DatLeitura.Minute, p.DatReferencia, p.CodLinha, l.DesLinha, p.CodModelo, prog.QtdProdPm, capac.QtdCapacLinha } into grupo
								   select new {
									   CodLinha = grupo.Key.CodLinha,
									   DesLinha = grupo.Key.DesLinha,
									   DatReferencia = grupo.Key.DatReferencia,
									   CodModelo = grupo.Key.CodModelo,
									   Hora = grupo.Key.Hora,
									   Minuto = grupo.Key.Minuto,
									   QtdLeitura = grupo.Count(),
									   QtdCapacLinha = grupo.Key.QtdCapacLinha,
									   QtdProdPm = grupo.Key.QtdProdPm
								   })
								   .ToList()
								   .GroupBy(x => new { x.CodLinha, x.DesLinha, x.DatReferencia, x.Hora, x.Minuto, x.CodModelo })
								   .Select(x => new ProducaoLinhaMinuto()
								   {
									   CodLinha = x.Key.CodLinha,
									   DesLinha = x.Key.DesLinha,
									   CodModelo = x.Key.CodModelo,
									   DatReferencia = x.Key.DatReferencia,
									   Hora = x.Key.Hora,
									   Minuto = x.Key.Minuto,
									   QtdLeitura = x.Sum(y => y.QtdLeitura),
									   QtdMeta = x.Average(y => y.QtdCapacLinha ?? y.QtdProdPm ?? 0)
								   }).OrderBy(x => x.Hora).ToList();

					return retorno;
				}
			}
		}

		public static List<ProducaoDataHora> ObterProducaoDataHora(DateTime datIni, DateTime datFim, int codLinha, int horaIni, int horaFim)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				/*var retorno = (from p in dtbCb.viw_CBPassagemTurno
					join l in dtbCb.tbl_CBLinha on p.CodLinha equals l.CodLinha
					join posto in dtbCb.tbl_CBPosto on p.NumPosto equals posto.NumPosto
					where
						p.DatReferencia >= datIni && p.DatReferencia <= datFim && p.CodLinha == codLinha && posto.CodLinha == p.CodLinha
					group p by
						new {Hora = p.DatEvento.Hour, p.DatReferencia, p.CodLinha, l.DesLinha, p.NumPosto, posto.DesPosto, posto.NumSeq}
					into grupo
					orderby grupo.Key.Hora
					orderby grupo.Key.NumSeq
					select grupo);

				return retorno.ToList().Select(x => new ProducaoDataHora()
														{
															CodLinha = x.Key.CodLinha,
															DesLinha = x.Key.DesLinha,
															NumPosto = x.Key.NumPosto,
															DesPosto = x.Key.DesPosto,
															SeqPosto = x.Key.NumSeq,
															Data = x.Key.DatReferencia ?? datIni,
															Hora = x.Key.Hora,
															QtdProduzido = x.Count()
														}).ToList();*/
				var retorno = dtbCb.spc_CBBuscaProducaoHora(datIni, datFim, codLinha).ToList();
				return retorno;

			}
		}

		public static List<ProducaoData> ObterProducaoData(DateTime datIni, DateTime datFim, int codLinha)
		{

			using (var dtbCb = new DTB_CBContext())
			using (var dtbSim = new DTB_SIMContext())
			{

				datFim = datFim.AddDays(1).AddMinutes(-1);

				var retorno = dtbCb.spc_CBBuscaProducaoPeriodo(datIni, datFim, codLinha).ToList();

				var linha = dtbCb.tbl_CBLinha.FirstOrDefault(x => x.CodLinha == codLinha);
				if (linha != null)
				{
					var linhaCliente = linha.CodLinhaT1;
					
					var programa = linha.Setor.Equals("FEC") ?
						dtbSim.tbl_ProgDiaJit.Where(x => x.DatProducao >= datIni && x.DatProducao <= datFim && x.CodLinha == linhaCliente)
							.GroupBy(g => g.CodModelo).Select(p => new { CodModelo = p.Key, QtdPrograma = p.Sum(x => x.QtdProdPm??0) }) :
						dtbSim.tbl_ProgDiaPlaca.Where(x => x.DatProducao >= datIni && x.DatProducao <= datFim && x.CodLinha == linhaCliente)
							.GroupBy(g => g.CodModelo).Select(p => new { CodModelo = p.Key, QtdPrograma = p.Sum(x => x.QtdProdPM) });

					retorno.ForEach(x =>
					{
						var firstOrDefault = programa.FirstOrDefault(p => p.CodModelo == x.CodModelo);
						if (firstOrDefault != null)
							x.QtdPlano = (firstOrDefault.QtdPrograma);
					});
				}

				return retorno;
			}
		}

		#endregion

		#region IMC
		public static List<LeiturasMF> ObterLeiturasIMC(DateTime Datinicio, DateTime DatFim)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				DateTime menorLeitura = dtbSim.tbl_BarrasProducaoPlaca.Min(x => x.DatLeitura);

				if (Datinicio < menorLeitura)
				{

					var retorno = (from p in dtbSim.viw_BarrasProducaoPlaca
						join modelo in dtbSim.tbl_Item on p.CodModelo equals modelo.CodItem
						join lsaap in dtbSim.tbl_LSAAP on p.NumAP equals lsaap.NumAP
						where p.DatLeitura >= Datinicio && p.DatLeitura <= DatFim
						select new LeiturasMF()
						{
							CodFab = p.CodFab,
							NumAP = p.NumAP,
							CodLinha = p.CodLinha,
							NumTurno = (int) p.NumTurno,
							Data = p.DatLeitura,
							DatReferencia = p.DatReferencia,
							CodModelo = p.CodModelo,
							DesModelo = modelo.DesItem,
							NumSerieModelo = p.NumSerieModelo,
							CodOperador = p.CodOperador,
							CodTestador = p.CodApontador,
							DatProcessadoSAP = p.DatProcessadoSAP,
							NumOP = lsaap.NumOP
						}).OrderBy(x => x.Data).ToList();

					return retorno;
				}
				else
				{
					var retorno = (from p in dtbSim.tbl_BarrasProducaoPlaca
						join modelo in dtbSim.tbl_Item on p.CodModelo equals modelo.CodItem
						join lsaap in dtbSim.tbl_LSAAP on p.NumAP equals lsaap.NumAP
						where p.DatLeitura >= Datinicio && p.DatLeitura <= DatFim
						select new LeiturasMF()
						{
							CodFab = p.CodFab,
							NumAP = p.NumAP,
							CodLinha = p.CodLinha,
							NumTurno = (int) p.NumTurno,
							Data = p.DatLeitura,
							DatReferencia = p.DatReferencia,
							CodModelo = p.CodModelo,
							DesModelo = modelo.DesItem,
							NumSerieModelo = p.NumSerieModelo,
							CodOperador = p.CodOperador,
							CodTestador = p.CodApontador,
							DatProcessadoSAP = p.DatProcessadoSAP,
							NumOP = lsaap.NumOP
						}).OrderBy(x => x.Data).ToList();

					return retorno;
				}
			}
		}

		public static List<LeiturasMF> ObterLeiturasIMC(DateTime Datinicio, DateTime DatFim, String CodLinha)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				DateTime menorLeitura = dtbSim.tbl_BarrasProducaoPlaca.Min(x => x.DatLeitura);

				if (Datinicio < menorLeitura)
				{

					var retorno = (from p in dtbSim.viw_BarrasProducaoPlaca
						join modelo in dtbSim.tbl_Item on p.CodModelo equals modelo.CodItem
						join lsaap in dtbSim.tbl_LSAAP on p.NumAP equals lsaap.NumAP
						where p.DatLeitura >= Datinicio && p.DatLeitura <= DatFim && p.CodLinha == CodLinha
						select new LeiturasMF()
						{
							CodFab = p.CodFab,
							NumAP = p.NumAP,
							CodLinha = p.CodLinha,
							NumTurno = (int) p.NumTurno,
							Data = p.DatLeitura,
							DatReferencia = p.DatReferencia,
							CodModelo = p.CodModelo,
							DesModelo = modelo.DesItem,
							NumSerieModelo = p.NumSerieModelo,
							CodOperador = p.CodOperador,
							CodTestador = p.CodApontador,
							DatProcessadoSAP = p.DatProcessadoSAP,
							NumOP = lsaap.NumOP
						}).OrderBy(x => x.Data).ToList();

					return retorno;
				}
				else
				{
					var retorno = (from p in dtbSim.tbl_BarrasProducaoPlaca
						join modelo in dtbSim.tbl_Item on p.CodModelo equals modelo.CodItem
						join lsaap in dtbSim.tbl_LSAAP on p.NumAP equals lsaap.NumAP
						where p.DatLeitura >= Datinicio && p.DatLeitura <= DatFim && p.CodLinha == CodLinha
						select new LeiturasMF()
						{
							CodFab = p.CodFab,
							NumAP = p.NumAP,
							CodLinha = p.CodLinha,
							NumTurno = (int) p.NumTurno,
							Data = p.DatLeitura,
							DatReferencia = p.DatReferencia,
							CodModelo = p.CodModelo,
							DesModelo = modelo.DesItem,
							NumSerieModelo = p.NumSerieModelo,
							CodOperador = p.CodOperador,
							CodTestador = p.CodApontador,
							DatProcessadoSAP = p.DatProcessadoSAP,
							NumOP = lsaap.NumOP
						}).OrderBy(x => x.Data).ToList();

					return retorno;
				}
			}
		}

		public static List<LeiturasMF> ObterLeiturasIMC(DateTime Datinicio, DateTime DatFim, String CodLinha, String CodModelo)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				DateTime menorLeitura = dtbSim.tbl_BarrasProducaoPlaca.Min(x => x.DatLeitura);

				if (Datinicio < menorLeitura)
				{

					var retorno = (from p in dtbSim.viw_BarrasProducaoPlaca
						join modelo in dtbSim.tbl_Item on p.CodModelo equals modelo.CodItem
						join lsaap in dtbSim.tbl_LSAAP on p.NumAP equals lsaap.NumAP
						where p.DatLeitura >= Datinicio && p.DatLeitura <= DatFim && p.CodLinha == CodLinha && p.CodModelo == CodModelo
						select new LeiturasMF()
						{
							CodFab = p.CodFab,
							NumAP = p.NumAP,
							CodLinha = p.CodLinha,
							NumTurno = (int) p.NumTurno,
							Data = p.DatLeitura,
							DatReferencia = p.DatReferencia,
							CodModelo = p.CodModelo,
							DesModelo = modelo.DesItem,
							NumSerieModelo = p.NumSerieModelo,
							CodOperador = p.CodOperador,
							CodTestador = p.CodApontador,
							DatProcessadoSAP = p.DatProcessadoSAP,
							NumOP = lsaap.NumOP
						}).OrderBy(x => x.Data).ToList();

					return retorno;
				}
				else
				{
					var retorno = (from p in dtbSim.tbl_BarrasProducaoPlaca
						join modelo in dtbSim.tbl_Item on p.CodModelo equals modelo.CodItem
						join lsaap in dtbSim.tbl_LSAAP on p.NumAP equals lsaap.NumAP
						where p.DatLeitura >= Datinicio && p.DatLeitura <= DatFim && p.CodLinha == CodLinha && p.CodModelo == CodModelo
						select new LeiturasMF()
						{
							CodFab = p.CodFab,
							NumAP = p.NumAP,
							CodLinha = p.CodLinha,
							NumTurno = (int) p.NumTurno,
							Data = p.DatLeitura,
							DatReferencia = p.DatReferencia,
							CodModelo = p.CodModelo,
							DesModelo = modelo.DesItem,
							NumSerieModelo = p.NumSerieModelo,
							CodOperador = p.CodOperador,
							CodTestador = p.CodApontador,
							DatProcessadoSAP = p.DatProcessadoSAP,
							NumOP = lsaap.NumOP
						}).OrderBy(x => x.Data).ToList();

					return retorno;
				}
			}
		}

		public static List<LeiturasMF> ObterLeiturasPorModeloIMC(String Codigo, String NumSerie)
		{
			using (DTB_SIMContext dtbSim = new DTB_SIMContext())
			{
				var retorno = (from p in dtbSim.viw_BarrasProducaoPlaca
					join modelo in dtbSim.tbl_Item on p.CodModelo equals modelo.CodItem
					join lsaap in dtbSim.tbl_LSAAP on p.NumAP equals lsaap.NumAP
					where p.CodModelo == Codigo && p.NumSerieModelo == NumSerie
					select new LeiturasMF()
					{
						CodFab = p.CodFab,
						NumAP = p.NumAP,
						CodLinha = p.CodLinha,
						NumTurno = (int) p.NumTurno,
						Data = p.DatLeitura,
						DatReferencia = p.DatReferencia,
						CodModelo = p.CodModelo,
						DesModelo = modelo.DesItem,
						NumSerieModelo = p.NumSerieModelo,
						CodOperador = p.CodOperador,
						CodTestador = p.CodApontador,
						DatProcessadoSAP = p.DatProcessadoSAP,
						NumOP = lsaap.NumOP
					}).OrderBy(x => x.Data).ToList();

				return retorno;
			}
		}

		/// <summary>
		/// Rotina para buscar as leituras de um período, retornando uma coleção mais simples de dados para depois fazer cálculos
		/// </summary>
		/// <param name="Datinicio"></param>
		/// <param name="DatFim"></param>
		/// <returns></returns>
		public static List<ProducaoLinhaMinuto> ObterLeiturasSimplesIMC(DateTime datReferencia, string fase = "IMC")
		{
			using (var dtbSim = new DTB_SIMContext())
			{

				DateTime menorLeitura = dtbSim.tbl_BarrasProducaoPlaca.Min(x => x.DatLeitura);

				if (datReferencia < menorLeitura)
				{

					var retorno = (from p in dtbSim.viw_BarrasProducaoPlaca
								   join l in dtbSim.tbl_Linha on p.CodLinha equals l.CodLinha
								   join bl in dtbSim.tbl_BarrasLinha on p.CodLinha equals bl.CodLinhaRel
								   where p.DatReferencia == datReferencia && bl.Setor == fase
								   group p by new { Hora = p.DatLeitura.Hour, Minuto = p.DatLeitura.Minute, p.DatReferencia, p.CodLinha, l.DesLinha } into grupo
								   select new ProducaoLinhaMinuto()
								   {
									   CodLinha = grupo.Key.CodLinha,
									   DesLinha = grupo.Key.DesLinha,
									   DatReferencia = grupo.Key.DatReferencia,
									   Hora = grupo.Key.Hora,
									   Minuto = grupo.Key.Minuto,
									   QtdLeitura = grupo.Count()
								   }).OrderBy(x => x.Hora).ToList();

					return retorno;
				}
				else
				{
					var mesRef = datReferencia.AddDays(-datReferencia.Day).AddDays(1);

					var retorno = (from p in dtbSim.tbl_BarrasProducaoPlaca
								   join l in dtbSim.tbl_Linha on p.CodLinha equals l.CodLinha
								   join bl in dtbSim.tbl_BarrasLinha on p.CodLinha equals bl.CodLinhaRel
								   from prog in dtbSim.tbl_ProgDiaPlaca.Where(x => x.CodPlaca == p.CodModelo && x.DatProducao == datReferencia && x.CodLinha == p.CodLinha).DefaultIfEmpty()
								   from capac in dtbSim.tbl_CapacLinha.Where(x => x.CodModelo == p.CodModelo && x.DatProducao == mesRef && x.CodLinha == p.CodLinha).DefaultIfEmpty()
								   where p.DatReferencia == datReferencia && bl.Setor == fase
								   group new { p, capac, prog } by new { Hora = p.DatLeitura.Hour, Minuto = p.DatLeitura.Minute, p.DatReferencia, p.CodLinha, l.DesLinha, prog.QtdProdPM, capac.QtdCapacLinha } into grupo
								   //group p by new { Hora = p.DatLeitura.Hour, Minuto = p.DatLeitura.Minute, p.DatReferencia, p.CodLinha, l.DesLinha } into grupo
								   select new ProducaoLinhaMinuto()
								   {
									   CodLinha = grupo.Key.CodLinha,
									   DesLinha = grupo.Key.DesLinha,
									   DatReferencia = grupo.Key.DatReferencia,
									   Hora = grupo.Key.Hora,
									   Minuto = grupo.Key.Minuto,
									   QtdLeitura = grupo.Count(),
									   QtdMeta = grupo.Average(y => y.capac.QtdCapacLinha ?? y.prog.QtdProdPM)
								   }).OrderBy(x => x.Hora).ToList();

					return retorno;
				}
			}
		}


		#endregion

		#region DAT
		public static List<LeiturasMF> ObterLeiturasDAT(DateTime Datinicio, DateTime DatFim, string CodLinha = null, String CodModelo = null)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var retorno = (from p in dtbCb.tbl_CBEmbalada
							   join modelo in dtbCb.tbl_Item on p.CodModelo equals modelo.CodItem
							   join linha in dtbCb.tbl_CBLinha on p.CodLinha equals linha.CodLinha
								where p.DatLeitura >= Datinicio && p.DatLeitura <= DatFim && linha.Setor == "DAT"
								select new LeiturasMF()
								{
									CodFab = p.CodFab,
									NumAP = p.NumAP,
									CodLinha = linha.CodLinhaT1,
									NumTurno = 1,
									Data = p.DatLeitura,
									DatReferencia = p.DatReferencia,
									CodModelo = p.CodModelo,
									DesModelo = modelo.DesItem,
									NumSerieModelo = p.NumECB,
									CodOperador = p.CodOperador,
									CodTestador = p.CodTestador,
									NumLote = p.NumCC
								});

				if (!String.IsNullOrEmpty(CodLinha))
				{
					retorno = retorno.Where(x => x.CodLinha == CodLinha);
				}

				if (!String.IsNullOrEmpty(CodModelo) && !String.IsNullOrEmpty(CodLinha))
				{
					retorno = retorno.Where(x => x.CodLinha == CodLinha && x.CodModelo == CodModelo);
				}

				return retorno.OrderBy(x => x.Data).ToList();
			}
		}

		/*
		public static List<LeiturasMF> ObterLeiturasDAT(DateTime Datinicio, DateTime DatFim, String CodLinha)
		{
			using (var dtbCb = new DTB_CBContext())
			{

				var retorno = (from p in dtbCb.tbl_CBEmbalada
							   join modelo in dtbCb.tbl_Item on p.CodModelo equals modelo.CodItem
							   join linha in dtbCb.tbl_CBLinha on p.CodLinha equals linha.CodLinha
								where p.DatLeitura >= Datinicio && p.DatLeitura <= DatFim && linha.CodLinhaT1 == CodLinha
								select new LeiturasMF()
								{
									CodFab = p.CodFab,
									NumAP = p.NumAP,
									CodLinha = linha.CodLinhaT1,
									NumTurno = 1,
									Data = p.DatLeitura,
									DatReferencia = p.DatReferencia,
									CodModelo = p.CodModelo,
									DesModelo = modelo.DesItem,
									NumSerieModelo = p.NumECB,
									CodOperador = p.CodOperador,
									CodTestador = p.CodTestador,
									NumLote = p.NumCC
								}).OrderBy(x => x.Data).ToList();

				return retorno;
			}
		}

		public static List<LeiturasMF> ObterLeiturasDAT(DateTime Datinicio, DateTime DatFim, String CodLinha, String CodModelo)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var retorno = (from p in dtbCb.tbl_CBEmbalada
							   join modelo in dtbCb.tbl_Item on p.CodModelo equals modelo.CodItem
							   join linha in dtbCb.tbl_CBLinha on p.CodLinha equals linha.CodLinha
							   where p.DatLeitura >= Datinicio && p.DatLeitura <= DatFim && linha.CodLinhaT1 == CodLinha && p.CodModelo == CodModelo
								select new LeiturasMF()
								{
									CodFab = p.CodFab,
									NumAP = p.NumAP,
									CodLinha = linha.CodLinhaT1,
									NumTurno = 1,
									Data = p.DatLeitura,
									DatReferencia = p.DatReferencia,
									CodModelo = p.CodModelo,
									DesModelo = modelo.DesItem,
									NumSerieModelo = p.NumECB,
									CodOperador = p.CodOperador,
									CodTestador = p.CodTestador,
									NumLote = p.NumCC
								}).OrderBy(x => x.Data).ToList();

				return retorno;

			}
		}
		*/
		public static List<LeiturasMF> ObterLeiturasPorModeloDAT(String Codigo, String NumSerie)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var retorno = (from p in dtbCb.tbl_CBEmbalada
							   join modelo in dtbCb.tbl_Item on p.CodModelo equals modelo.CodItem
							   join linha in dtbCb.tbl_CBLinha on p.CodLinha equals linha.CodLinha
							   where p.CodModelo == Codigo && p.NumECB == NumSerie
							   select new LeiturasMF()
							   {
								   CodFab = p.CodFab,
								   NumAP = p.NumAP,
								   CodLinha = linha.CodLinhaT1,
								   NumTurno = 1,
								   Data = p.DatLeitura,
								   DatReferencia = p.DatReferencia,
								   CodModelo = p.CodModelo,
								   DesModelo = modelo.DesItem,
								   NumSerieModelo = p.NumECB,
								   CodOperador = p.CodOperador,
								   CodTestador = p.CodTestador,
								   NumLote = p.NumCC
							   }).OrderBy(x => x.Data).ToList();

				return retorno;
			}
		}

		/// <summary>
		/// Rotina para buscar as leituras de um período, retornando uma coleção mais simples de dados para depois fazer cálculos
		/// </summary>
		/// <param name="Datinicio"></param>
		/// <param name="DatFim"></param>
		/// <returns></returns>
		public static List<ProducaoLinhaMinuto> ObterLeiturasSimplesDAT(DateTime datReferencia)
		{
			using (var dtbSim = new DTB_SIMContext())
			{

				DateTime menorLeitura = dtbSim.tbl_BarrasProducaoPlaca.Min(x => x.DatLeitura);

				if (datReferencia < menorLeitura)
				{

					var retorno = (from p in dtbSim.viw_BarrasProducaoPlaca
								   join l in dtbSim.tbl_Linha on p.CodLinha equals l.CodLinha
								   where p.DatReferencia == datReferencia
								   group p by new { Hora = p.DatLeitura.Hour, Minuto = p.DatLeitura.Minute, p.DatReferencia, p.CodLinha, l.DesLinha } into grupo
								   select new ProducaoLinhaMinuto()
								   {
									   CodLinha = grupo.Key.CodLinha,
									   DesLinha = grupo.Key.DesLinha,
									   DatReferencia = grupo.Key.DatReferencia,
									   Hora = grupo.Key.Hora,
									   Minuto = grupo.Key.Minuto,
									   QtdLeitura = grupo.Count()
								   }).OrderBy(x => x.Hora).ToList();

					return retorno;
				}
				else
				{
					var retorno = (from p in dtbSim.tbl_BarrasProducaoPlaca
								   join l in dtbSim.tbl_Linha on p.CodLinha equals l.CodLinha
								   where p.DatReferencia == datReferencia
								   group p by new { Hora = p.DatLeitura.Hour, Minuto = p.DatLeitura.Minute, p.DatReferencia, p.CodLinha, l.DesLinha } into grupo
								   select new ProducaoLinhaMinuto()
								   {
									   CodLinha = grupo.Key.CodLinha,
									   DesLinha = grupo.Key.DesLinha,
									   DatReferencia = grupo.Key.DatReferencia,
									   Hora = grupo.Key.Hora,
									   Minuto = grupo.Key.Minuto,
									   QtdLeitura = grupo.Count()
								   }).OrderBy(x => x.Hora).ToList();

					return retorno;
				}
			}
		}


		#endregion


		#region Perfil de Modelo por Posto

		public static List<CBLinhaPostoPerfilDTO> ObterPerfilLinhaPosto(int? codLinha, string codModelo)
		{
			using (var dtbCb = new DTB_CBContext())
			{

				var query = (from l in dtbCb.tbl_CBLinhaPostoPerfil
							 join i in dtbCb.tbl_Item on l.CodModelo equals i.CodItem
							 join linha in dtbCb.tbl_CBLinha on l.CodLinha equals linha.CodLinha
							 join posto in dtbCb.tbl_CBPosto on l.NumPosto equals posto.NumPosto
							 where posto.CodLinha == l.CodLinha
							 orderby new { l.CodModelo, l.CodLinha, l.NumSeq }
							 select new
							 {
								 CodModelo = l.CodModelo,
								 DesModelo = i.DesItem,
								 CodLinha = l.CodLinha,
								 DesLinha = linha.DesLinha,
								 NumPosto = l.NumPosto,
								 DesPosto = posto.DesPosto,
								 FlgAtivo = l.FlgAtivo,
								 NumSeq = l.NumSeq,
								 flgObrigatorio = l.flgObrigatorio,
								 TempoCiclo = l.TempoCiclo,
								 QtdJigs = l.QtdJigs,
								 AparelhosMinuto = l.AparelhosMinuto

							 });

				if (codLinha != null && codLinha != 0)
				{
					query = query.Where(x => x.CodLinha == (codLinha ?? 0));
				}

				if (!String.IsNullOrEmpty(codModelo))
				{
					query = query.Where(x => x.CodModelo == codModelo);
				}

				var resultado = query.ToList().Select(x => new CBLinhaPostoPerfilDTO()
				{
					CodModelo = x.CodModelo,
					DesModelo = x.DesModelo,
					CodLinha = x.CodLinha,
					DesLinha = x.DesLinha,
					NumPosto = x.NumPosto,
					DesPosto = x.DesPosto,
					FlgAtivo = x.FlgAtivo,
					NumSeq = x.NumSeq,
					flgObrigatorio = x.flgObrigatorio,
					TempoCiclo = x.TempoCiclo,
					QtdJigs = x.QtdJigs,
					AparelhosMinuto = x.AparelhosMinuto
				}).ToList();

				return resultado;
			}
		}

		public static List<CBLinhaPostoPerfilDTO> ObterPerfilLinhaPosto(int codLinha)
		{
			using (var dtbCb = new DTB_CBContext())
			{

				var query = (from l in dtbCb.tbl_CBLinhaPostoPerfil
							 join i in dtbCb.tbl_Item on l.CodModelo equals i.CodItem
							 join linha in dtbCb.tbl_CBLinha on l.CodLinha equals linha.CodLinha
							 join posto in dtbCb.tbl_CBPosto on l.NumPosto equals posto.NumPosto
							 where l.CodLinha == codLinha && posto.CodLinha == l.CodLinha
							 orderby new { l.CodModelo, l.NumSeq}
							 select new
							 {
								 CodModelo = l.CodModelo,
								 DesModelo = i.DesItem,
								 CodLinha = l.CodLinha,
								 DesLinha = linha.DesLinha,
								 NumPosto = l.NumPosto,
								 DesPosto = posto.DesPosto,
								 FlgAtivo = l.FlgAtivo,
								 NumSeq = l.NumSeq,
								 flgObrigatorio = l.flgObrigatorio,
								 TempoCiclo = l.TempoCiclo,
								 QtdJigs = l.QtdJigs,
								 AparelhosMinuto = l.AparelhosMinuto

							 });

				var resultado = query.ToList().Select(x => new CBLinhaPostoPerfilDTO()
				{
					CodModelo = x.CodModelo,
					DesModelo = x.DesModelo,
					CodLinha = x.CodLinha,
					DesLinha = x.DesLinha,
					NumPosto = x.NumPosto,
					DesPosto = x.DesPosto,
					FlgAtivo = x.FlgAtivo,
					NumSeq = x.NumSeq,
					flgObrigatorio = x.flgObrigatorio,
					TempoCiclo = x.TempoCiclo,
					QtdJigs = x.QtdJigs,
					AparelhosMinuto = x.AparelhosMinuto
				}).ToList();

				return resultado;
			}
		}

		public static CBLinhaPostoPerfilDTO ObterPerfilLinhaPosto(int codLinha, int numPosto, string codModelo)
		{
			using (var dtbCb = new DTB_CBContext())
			{

				var query = (from l in dtbCb.tbl_CBLinhaPostoPerfil
							 join i in dtbCb.tbl_Item on l.CodModelo equals i.CodItem
							 join linha in dtbCb.tbl_CBLinha on l.CodLinha equals linha.CodLinha
							 join posto in dtbCb.tbl_CBPosto on l.NumPosto equals posto.NumPosto
							 where l.CodLinha == codLinha && posto.CodLinha == l.CodLinha && l.NumPosto == numPosto && l.CodModelo == codModelo
							 orderby l.NumSeq
							 select new CBLinhaPostoPerfilDTO()
							 {
								 CodModelo = l.CodModelo,
								 DesModelo = i.DesItem,
								 CodLinha = l.CodLinha,
								 DesLinha = linha.DesLinha,
								 NumPosto = l.NumPosto,
								 DesPosto = posto.DesPosto,
								 FlgAtivo = l.FlgAtivo,
								 NumSeq = l.NumSeq,
								 flgObrigatorio = l.flgObrigatorio,
								 TempoCiclo = l.TempoCiclo,
								 QtdJigs = l.QtdJigs,
								 AparelhosMinuto = l.AparelhosMinuto

							 });

				var resultado = query.FirstOrDefault();

				return resultado;
			}
		}

		public static void GravarPerfilLinhaPosto(CBLinhaPostoPerfilDTO dado)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var existe =
					dtbCb.tbl_CBLinhaPostoPerfil.FirstOrDefault(
						x => x.CodLinha == dado.CodLinha && x.CodModelo == dado.CodModelo && x.NumPosto == dado.NumPosto);
				if (existe != null)
				{
					existe.flgObrigatorio = dado.flgObrigatorio;
					existe.FlgAtivo = dado.FlgAtivo;
					existe.NumSeq = dado.NumSeq;
					existe.TempoCiclo = dado.TempoCiclo;
					existe.QtdJigs = dado.QtdJigs;
				}
				else
				{
					var perfil = new tbl_CBLinhaPostoPerfil()
					{
						CodLinha = dado.CodLinha,
						CodModelo = dado.CodModelo,
						flgObrigatorio = dado.flgObrigatorio,
						FlgAtivo = dado.FlgAtivo,
						NumPosto = dado.NumPosto,
						NumSeq = dado.NumSeq,
						TempoCiclo = dado.TempoCiclo,
						QtdJigs = dado.QtdJigs
					};
					dtbCb.tbl_CBLinhaPostoPerfil.Add(perfil);
				}
				dtbCb.SaveChanges();
			}
		}

		public static void RemoverPerfilLinhaPosto(CBLinhaPostoPerfilDTO dado)
		{
			using (var dtbCb = new DTB_CBContext())
			{
				var existe =
					dtbCb.tbl_CBLinhaPostoPerfil.FirstOrDefault(
						x => x.CodLinha == dado.CodLinha && x.CodModelo == dado.CodModelo && x.NumPosto == dado.NumPosto);
				
				if (existe == null) return;
				dtbCb.tbl_CBLinhaPostoPerfil.Remove(existe);
				dtbCb.SaveChanges();
			}
		}

		#endregion
	}
}
