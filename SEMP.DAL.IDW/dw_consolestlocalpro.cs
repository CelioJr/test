namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolestlocalpro
    {
        [Key]
        public long id_consolestlocalpro { get; set; }

        public long id_turno { get; set; }

        public long id_cal { get; set; }

        public long id_estlocalpro { get; set; }

        public DateTime dt_referencia { get; set; }

        public DateTime dthr_iturno { get; set; }

        public DateTime dthr_fturno { get; set; }

        public decimal ano { get; set; }

        public decimal mes { get; set; }

        public decimal qt_entrada { get; set; }

        public decimal qt_saida { get; set; }

        public decimal qt_perda { get; set; }

        public decimal qt_consumida { get; set; }

        public decimal qt_ajuste { get; set; }

        public decimal qt_total { get; set; }

        public virtual dw_cal dw_cal { get; set; }

        public virtual dw_estlocalpro dw_estlocalpro { get; set; }

        public virtual dw_turno dw_turno { get; set; }
    }
}
