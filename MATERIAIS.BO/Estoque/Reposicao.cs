﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.Materiais;
using SEMP.Model;
using SEMP.Model.DTO;
using SEMP.Model.VO.Materiais;

namespace MATERIAIS.BO.Estoque
{
	public class Reposicao
	{

		public static List<PainelReposicao> ObterDadosPainel()
		{
			return daoReposicao.ObterDadosPainel();
		}

		public static List<RelReposicaoDTO> ObterReposicoes(DateTime datIni, DateTime datFim, string status)
		{
			return daoReposicao.ObterReposicao(datIni, datFim, status, Constantes.FASE_MF);
		}

		public static List<PainelGestao> ObterDadosGestao(int fase)
		{
			return daoReposicao.ObterDadosGestao(fase);
		}

		public static ConsultaApPagamento ConsultaAPPagamento(string CodItem)
		{
			return daoReposicao.ConsultaAPPagamento(CodItem);
		}

		public static ResumoMesReposicao ObterResumoMesResposicao(int fase)
		{

			DateTime datIni = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
			DateTime datFim = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
			datFim = datFim.AddDays(1);

			var reposicoes = daoReposicao.ObterReposicao(datIni, datFim, "", fase);

			ResumoMesReposicao resumo = new ResumoMesReposicao();

			resumo.QtdTotalRomaneio = reposicoes.Select(x => x.Romaneio).Distinct().Count();
			resumo.QtdTotalItens = reposicoes.Count();
			resumo.QtdAprovada = reposicoes.Count(x => x.DatAprovacao != null);
			resumo.QtdRecebido = reposicoes.Count(x => x.DatRecebimento != null);
			resumo.QtdPaga = reposicoes.Count(x => x.DatPagamento != null);

			return resumo;
		}

		public static List<RelReposicaoDTO> ConsultaPendencia(string id)
		{
			DateTime datIni = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
			DateTime datFim = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
			datFim = datFim.AddDays(1);
			
			var reposicoes = daoReposicao.ObterReposicao(datIni, datFim, "", Constantes.FASE_MF);

			List<RelReposicaoDTO> filtro = new List<RelReposicaoDTO>();

			switch (id)
			{
					case "prod":
					filtro = reposicoes.Where(x => x.Status == "GERADO").OrderBy(x => x.Dia).ToList();
					break;

					case "sup":
					filtro = reposicoes.Where(x => x.DatPagamento == null && x.DatAprovacao != null).OrderBy(x => x.DatAprovacao).ToList();
					break;

					case "qual1":
					filtro = reposicoes.Where(x => x.DatRecebimento == null && x.DatAprovacao != null).OrderBy(x => x.DatAprovacao).ToList();
					break;
			}

			return filtro;
		}


		public static List<Pendencias> ConsultaPendencia2(string id, int fase)
		{
			//var datIni = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);		
			var datIni = new DateTime(2015, 7, 1);		
			var datFim = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
			datFim = datFim.AddDays(1);

			var filtro = new List<Pendencias>();

			switch (id)
			{
				case "prod":
					filtro = daoReposicao.ObterPendAprovacao(datIni, datFim, fase);
					break;

				case "sup":
					filtro = daoReposicao.ObterPendPagamento(datIni, datFim, fase);
					break;

				case "qual1":
					filtro = daoReposicao.ObterPendRecebimento(datIni, datFim, fase);
					break;

				case "qual2":
					filtro = daoReposicao.ObterPendLaudo(datIni, datFim, fase);
					break;

				case "supm2":
					filtro = daoReposicao.ObterPendPagamento2(datIni, datFim, fase);
					break;
			}

			return filtro;
		}

		public static ConsultaRomaneio ObterRomaneio(string NumRomaneio)
		{
			return daoReposicao.ObterRomaneio(Int32.Parse(NumRomaneio));
		}

	}
}
