using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_FORMULARIO_DTO
    {
        public FR_FORMULARIO_DTO()
        {
            this.FR_COMPONENTE = new List<FR_COMPONENTE_DTO>();
            this.FR_CONSULTA_AVANCADA = new List<FR_CONSULTA_AVANCADA_DTO>();
            this.FR_FONTEDADOS = new List<FR_FONTEDADOS_DTO>();
            this.FR_MENU = new List<FR_MENU_DTO>();
            this.FR_PERMISSAO = new List<FR_PERMISSAO_DTO>();
            this.FR_CATEGORIA = new List<FR_CATEGORIA_DTO>();
            this.FR_SISTEMA = new List<FR_SISTEMA_DTO>();
        }

        public int FRM_CODIGO { get; set; }
        public Nullable<int> IMG_INCLUIR { get; set; }
        public Nullable<int> IMG_ALTERAR { get; set; }
        public Nullable<int> IMG_EXCLUIR { get; set; }
        public Nullable<int> IMG_GRAVAR { get; set; }
        public Nullable<int> IMG_GRAVAR_MAIS { get; set; }
        public Nullable<int> IMG_CANCELAR { get; set; }
        public Nullable<int> IMG_ATUALIZAR { get; set; }
        public Nullable<int> IMG_VALORES_PADRAO { get; set; }
        public Nullable<int> IMG_UTILITARIO { get; set; }
        public Nullable<int> IMG_LOG { get; set; }
        public Nullable<int> IMG_SAIR { get; set; }
        public Nullable<int> IMG_IMPRIMIR { get; set; }
        public Nullable<int> IMG_AJUDA { get; set; }
        public Nullable<int> IMG_PROXIMO { get; set; }
        public Nullable<int> IMG_ULTIMO { get; set; }
        public Nullable<int> IMG_PRIMEIRO { get; set; }
        public Nullable<int> IMG_ANTERIOR { get; set; }
        public string FRM_DESCRICAO { get; set; }
        public string FRM_TIPO { get; set; }
        public Nullable<int> FRM_POSICAOX { get; set; }
        public Nullable<int> FRM_POSICAOY { get; set; }
        public Nullable<int> FRM_TAMANHO { get; set; }
        public Nullable<int> FRM_ALTURA { get; set; }
        public string FRM_TIPO_CRIACAO { get; set; }
        public string FRM_GUID { get; set; }
        public Nullable<int> REL_CODIGO { get; set; }
        public Nullable<int> USR_CODIGO { get; set; }
        public string FRM_LOG { get; set; }
        public virtual ICollection<FR_COMPONENTE_DTO> FR_COMPONENTE { get; set; }
        public virtual ICollection<FR_CONSULTA_AVANCADA_DTO> FR_CONSULTA_AVANCADA { get; set; }
        public virtual ICollection<FR_FONTEDADOS_DTO> FR_FONTEDADOS { get; set; }
        public virtual FR_RELATORIO_DTO FR_RELATORIO { get; set; }
        public virtual ICollection<FR_MENU_DTO> FR_MENU { get; set; }
        public virtual ICollection<FR_PERMISSAO_DTO> FR_PERMISSAO { get; set; }
        public virtual ICollection<FR_CATEGORIA_DTO> FR_CATEGORIA { get; set; }
        public virtual ICollection<FR_SISTEMA_DTO> FR_SISTEMA { get; set; }
    }
}
