namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_LOTE_LIST")]
    public partial class ALC_LOTE_LIST
    {
        [Key]
        public int ID_LIST { get; set; }

        public DateTime? dtPallet { get; set; }

        public int? LINE { get; set; }

        [StringLength(255)]
        public string PACKING { get; set; }

        [StringLength(255)]
        public string ID_LOTE { get; set; }

        [StringLength(20)]
        public string ORDER_ID { get; set; }

        public int? RESP_PALLET { get; set; }

        public virtual ALC_ORDER ALC_ORDER { get; set; }

        public virtual ALC_SYS_USER ALC_SYS_USER { get; set; }
    }
}
