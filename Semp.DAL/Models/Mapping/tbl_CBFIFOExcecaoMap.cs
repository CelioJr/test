using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBFIFOExcecaoMap : EntityTypeConfiguration<tbl_CBFIFOExcecao>
    {
        public tbl_CBFIFOExcecaoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.NumLote, t.NumCC });

            // Properties
            this.Property(t => t.NumLote)
                .IsRequired()
                .HasMaxLength(18)
                .IsUnicode(false);

            this.Property(t => t.NumCC)
                .IsRequired()
                .HasMaxLength(18)
                .IsUnicode(false);

            this.Property(t => t.Observacao)
                .HasMaxLength(500)
                .IsUnicode(false);

            this.Property(t => t.NomUsuario)
                .HasMaxLength(20)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_CBFIFOExcecao");
            this.Property(t => t.NumLote).HasColumnName("NumLote");
            this.Property(t => t.NumCC).HasColumnName("NumCC");
            this.Property(t => t.Data).HasColumnName("Data");
            this.Property(t => t.Observacao).HasColumnName("Observacao");
            this.Property(t => t.NomUsuario).HasColumnName("NomUsuario");
        }
    }
}
