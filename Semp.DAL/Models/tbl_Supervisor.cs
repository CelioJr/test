using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_Supervisor
    {
        public string CodDRT { get; set; }
        public string CodSeguranca { get; set; }
    }
}
