namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_RMB_COST")]
    public partial class ALC_RMB_COST
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ALC_RMB_COST()
        {
            ALC_RMB_REGISTER = new HashSet<ALC_RMB_REGISTER>();
        }

        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal COST_ID { get; set; }

        public int? COST_MONTH { get; set; }

        public double? COST_VALUE { get; set; }

        public int? COST_YEAR { get; set; }

        [StringLength(50)]
        public string PART_NUMBER { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ALC_RMB_REGISTER> ALC_RMB_REGISTER { get; set; }

        public virtual ALC_RMB_PART_NUMBER ALC_RMB_PART_NUMBER { get; set; }
    }
}
