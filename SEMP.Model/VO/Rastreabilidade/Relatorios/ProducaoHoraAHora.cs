﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
	public class ProducaoHoraAHora
	{
		[Display(Name = "Modelo")]
		public String CodModelo { get; set; }
		[Display(Name = "Descrição")]
		public String DesModelo { get; set; }
		[Display(Name = "Linha")]
		public String CodLinha { get; set; }
		[Display(Name = "Descrição")]
		public String Linha { get; set; }
		[Display(Name = "Plano T1")]
		public int? QtdProdPMT1 { get; set; }
		[Display(Name = "Plano T2")]
		public int? QtdProdPMT2 { get; set; }
		[Display(Name = "Plano")]
		public int QtdPlano {
			get
			{
				int valor1 = QtdProdPMT1 ?? 0;
				int valor2 = QtdProdPMT2 ?? 0;

				return valor1 + valor2;
			}
		}
		[Display(Name = "Prod.")]
		public int QtdProducao { get; set; }
		[Display(Name = "06:00 07:00")]
		public int Hora6 { get; set; }
		[Display(Name = "07:00 08:00")]
		public int Hora7 { get; set; }
		[Display(Name = "08:00 09:00")]
		public int Hora8 { get; set; }
		[Display(Name = "09:00 10:00")]
		public int Hora9 { get; set; }
		[Display(Name = "10:00 11:00")]
		public int Hora10 { get; set; }
		[Display(Name = "11:00 12:00")]
		public int Hora11 { get; set; }
		[Display(Name = "12:00 13:00")]
		public int Hora12 { get; set; }
		[Display(Name = "13:00 14:00")]
		public int Hora13 { get; set; }
		[Display(Name = "14:00 15:00")]
		public int Hora14 { get; set; }
		[Display(Name = "15:00 16:00")]
		public int Hora15 { get; set; }
		[Display(Name = "16:00 17:00")]
		public int Hora16 { get; set; }
		[Display(Name = "17:00 17:08")]
		public int Hora17 { get; set; }
		[Display(Name = "17:08 18:00")]
		public int Hora17_2 { get; set; }
		[Display(Name = "18:00 19:00")]
		public int Hora18 { get; set; }
		[Display(Name = "19:00 20:00")]
		public int Hora19 { get; set; }
		[Display(Name = "20:00 21:00")]
		public int Hora20 { get; set; }
		[Display(Name = "21:00 22:00")]
		public int Hora21 { get; set; }
		[Display(Name = "22:00 23:00")]
		public int Hora22 { get; set; }
		[Display(Name = "23:00 24:00")]
		public int Hora23 { get; set; }
		[Display(Name = "00:00 01:00")]
		public int Hora0 { get; set; }
		[Display(Name = "01:00 02:00")]
		public int Hora1 { get; set; }
		[Display(Name = "02:00 02:38")]
		public int Hora2 { get; set; }
		[Display(Name = "02:39 03:00")]
		public int Hora2_2 { get; set; }
		[Display(Name = "03:00 04:00")]
		public int Hora3 { get; set; }
		[Display(Name = "04:00 05:00")]
		public int Hora4 { get; set; }
		[Display(Name = "05:00 06:00")]
		public int Hora5 { get; set; }

		public int? PHora6 { get; set; }
		public int? PHora7 { get; set; }
		public int? PHora8 { get; set; }
		public int? PHora9 { get; set; }
		public int? PHora10 { get; set; }
		public int? PHora11 { get; set; }
		public int? PHora12 { get; set; }
		public int? PHora13 { get; set; }
		public int? PHora14 { get; set; }
		public int? PHora15 { get; set; }
		public int? PHora16 { get; set; }
		public int? PHora17 { get; set; }
		public int? PHora17_2 { get; set; }
		public int? PHora18 { get; set; }
		public int? PHora19 { get; set; }
		public int? PHora20 { get; set; }
		public int? PHora21 { get; set; }
		public int? PHora22 { get; set; }
		public int? PHora23 { get; set; }
		public int? PHora0 { get; set; }
		public int? PHora1 { get; set; }
		public int? PHora2 { get; set; }
		public int? PHora2_2 { get; set; }
		public int? PHora3 { get; set; }
		public int? PHora4 { get; set; }
		public int? PHora5 { get; set; }
				   
		public ProducaoHoraAHora()
		{
			QtdProdPMT1 = 0;
			QtdProdPMT2 = 0;
			QtdProducao = 0;
			Hora0 = 0;
			Hora1 = 0;
			Hora2 = 0;
			Hora2_2 = 0;
			Hora3 = 0;
			Hora4 = 0;
			Hora5 = 0;
			Hora6 = 0;
			Hora7 = 0;
			Hora8 = 0;
			Hora9 = 0;
			Hora10 = 0;
			Hora11 = 0;
			Hora12 = 0;
			Hora13 = 0;
			Hora14 = 0;
			Hora15 = 0;
			Hora16 = 0;
			Hora17 = 0;
			Hora17_2 = 0;
			Hora18 = 0;
			Hora19 = 0;
			Hora20 = 0;
			Hora21 = 0;
			Hora22 = 0;
			Hora23 = 0;


			PHora0 = 0;
			PHora1 = 0;
			PHora2 = 0;
			PHora2_2 = 0;
			PHora3 = 0;
			PHora4 = 0;
			PHora5 = 0;
			PHora6 = 0;
			PHora7 = 0;
			PHora8 = 0;
			PHora9 = 0;
			PHora10 = 0;
			PHora11 = 0;
			PHora12 = 0;
			PHora13 = 0;
			PHora14 = 0;
			PHora15 = 0;
			PHora16 = 0;
			PHora17 = 0;
			PHora17_2 = 0;
			PHora18 = 0;
			PHora19 = 0;
			PHora20 = 0;
			PHora21 = 0;
			PHora22 = 0;
			PHora23 = 0;
		}
	}
}
