namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_rap
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_rap()
        {
            dw_consolperdamplog = new HashSet<dw_consolperdamplog>();
            dw_folharap = new HashSet<dw_folharap>();
            dw_rap1 = new HashSet<dw_rap>();
            dw_rap_grupo = new HashSet<dw_rap_grupo>();
            om_cfg = new HashSet<om_cfg>();
            om_prgpos = new HashSet<om_prgpos>();
            om_prgpos1 = new HashSet<om_prgpos>();
            pp_indisp_rappt = new HashSet<pp_indisp_rappt>();
        }

        [Key]
        public long id_rap { get; set; }

        public long? id_rapTipo { get; set; }

        [StringLength(30)]
        public string cd_rap { get; set; }

        public int? revisao { get; set; }

        public DateTime? dt_stativo { get; set; }

        public DateTime? dt_revisao { get; set; }

        public short? st_ativo { get; set; }

        [StringLength(100)]
        public string ds_rap { get; set; }

        public decimal? qt_total { get; set; }

        public decimal? qt_alocada { get; set; }

        [StringLength(100)]
        public string depara { get; set; }

        public decimal? seg_tempoliberacao { get; set; }

        public long id_usrRevisao { get; set; }

        public long id_usrStativo { get; set; }

        public int? tp_rap { get; set; }

        public long? id_cliente { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolperdamplog> dw_consolperdamplog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folharap> dw_folharap { get; set; }

        public virtual pp_cliente pp_cliente { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rap> dw_rap1 { get; set; }

        public virtual dw_rap dw_rap2 { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rap_grupo> dw_rap_grupo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_prgpos> om_prgpos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_prgpos> om_prgpos1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_indisp_rappt> pp_indisp_rappt { get; set; }
    }
}
