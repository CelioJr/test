using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_FeriadosMap : EntityTypeConfiguration<tbl_Feriados>
    {
        public tbl_FeriadosMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Setor, t.DatFeriado });

            // Properties
            this.Property(t => t.Setor)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.DesFeriado)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(25)
                .IsUnicode(false);

            this.Property(t => t.DiaUtil)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_Feriados");
            this.Property(t => t.Setor).HasColumnName("Setor");
            this.Property(t => t.DatFeriado).HasColumnName("DatFeriado");
            this.Property(t => t.DesFeriado).HasColumnName("DesFeriado");
            this.Property(t => t.DiaUtil).HasColumnName("DiaUtil");
        }
    }
}
