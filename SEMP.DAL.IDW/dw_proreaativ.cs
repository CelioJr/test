namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_proreaativ
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_proreaativ()
        {
            dw_proreaativobs = new HashSet<dw_proreaativobs>();
        }

        [Key]
        public long id_proreaativ { get; set; }

        public DateTime? dthr_inicio { get; set; }

        public DateTime? dthr_fim { get; set; }

        public long? id_prorea { get; set; }

        public long? id_procativ { get; set; }

        public long? id_usr { get; set; }

        public short? st_proreaativ { get; set; }

        public virtual dw_procativ dw_procativ { get; set; }

        public virtual dw_prorea dw_prorea { get; set; }

        public virtual om_usr om_usr { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_proreaativobs> dw_proreaativobs { get; set; }
    }
}
