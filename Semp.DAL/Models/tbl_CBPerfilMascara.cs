using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBPerfilMascara
    {
        public string CodItem { get; set; }
        public string Mascara { get; set; }
    }
}
