using System;
using System.Collections.Generic;


namespace SEMP.DAL.DTB_SSA.Models
{
    public partial class tbl_Item_Padrao_Serial
    {
       public string NE { get; set; }
        public int dispositivo_id { get; set; }
        public int tamMax { get; set; }
        public int tamMin { get; set; }
        public byte utilizarRange { get; set; }
        public string ExpressaoRegular { get; set; }
        public int CodOpeCriacao { get; set; }
        public System.DateTime DatCriacao { get; set; }
        public int? CodOpeAlt { get; set; }
        public DateTime? DatAlt { get; set; }       
    }
}
