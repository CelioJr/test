using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_CONFIGURACAO_DTO
    {
        public int CNF_CODIGO { get; set; }
        public int CNF_VERSIONA_FORMULARIO { get; set; }
        public int CNF_VERSIONA_RELATORIO { get; set; }
        public int CNF_VERSIONA_REGRA { get; set; }
        public string CNF_MAKER_VERSION { get; set; }
    }
}
