namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolatlog
    {
        [Key]
        public long id_consolatlog { get; set; }

        public DateTime? dthr_anterior { get; set; }

        public int? ms_dthranterior { get; set; }

        public DateTime? dthr_nova { get; set; }

        public int? ms_dthrnova { get; set; }

        public long? id_pt { get; set; }

        [StringLength(255)]
        public string url_conexao { get; set; }

        public virtual om_pt om_pt { get; set; }
    }
}
