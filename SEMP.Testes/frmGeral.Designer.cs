﻿namespace SEMP.Testes
{
	partial class frmGeral
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnImportarSaldoPCI = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnImportarSaldoPCI
			// 
			this.btnImportarSaldoPCI.Location = new System.Drawing.Point(12, 12);
			this.btnImportarSaldoPCI.Name = "btnImportarSaldoPCI";
			this.btnImportarSaldoPCI.Size = new System.Drawing.Size(138, 23);
			this.btnImportarSaldoPCI.TabIndex = 0;
			this.btnImportarSaldoPCI.Text = "Importar Saldo PCI SAP";
			this.btnImportarSaldoPCI.UseVisualStyleBackColor = true;
			this.btnImportarSaldoPCI.Click += new System.EventHandler(this.btnImportarSaldoPCI_Click);
			// 
			// frmGeral
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(483, 184);
			this.Controls.Add(this.btnImportarSaldoPCI);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "frmGeral";
			this.Text = "Testes Gerais";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnImportarSaldoPCI;
	}
}