using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
	public class tbl_CBPassagemMap : EntityTypeConfiguration<tbl_CBPassagem>
	{
		public tbl_CBPassagemMap()
		{
			// Primary Key
			this.HasKey(t => new { t.NumECB, t.CodLinha, t.NumPosto, t.DatEvento });

			// Properties
			this.Property(t => t.NumECB)
				.IsRequired()
				.HasMaxLength(20)
                .IsUnicode(false);

			this.Property(t => t.CodLinha)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.NumPosto)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

			this.Property(t => t.CodDRT)
				.IsFixedLength()
				.HasMaxLength(8)
                .IsUnicode(false);

			this.Property(t => t.CodFab)
				.IsFixedLength()
				.HasMaxLength(2)
                .IsUnicode(false);

			this.Property(t => t.NumAP)
				.IsFixedLength()
				.HasMaxLength(6)
                .IsUnicode(false);

			// Table & Column Mappings
			this.ToTable("tbl_CBPassagem");
			this.Property(t => t.NumECB).HasColumnName("NumECB");
			this.Property(t => t.CodLinha).HasColumnName("CodLinha");
			this.Property(t => t.NumPosto).HasColumnName("NumPosto");
			this.Property(t => t.DatEvento).HasColumnName("DatEvento");
			this.Property(t => t.CodDRT).HasColumnName("CodDRT");
			this.Property(t => t.CodFab).HasColumnName("CodFab");
			this.Property(t => t.NumAP).HasColumnName("NumAP");

		}
	}
}
