namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tt_sap_estmppa
    {
        [Key]
        public long id_sapestmppa { get; set; }

        [StringLength(30)]
        public string deposito { get; set; }

        [StringLength(30)]
        public string globalcode { get; set; }

        public decimal? qt_estoque { get; set; }

        public DateTime? dthr_referencia { get; set; }

        [StringLength(30)]
        public string centro { get; set; }

        public short? is_importado { get; set; }

        [StringLength(256)]
        public string ds_erro { get; set; }
    }
}
