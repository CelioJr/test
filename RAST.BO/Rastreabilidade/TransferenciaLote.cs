﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.Model.DTO;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.DAL.Models;
using SEMP.Model.VO;
using SEMP.Model;
using SEMP.Model.VO.Rastreabilidade;
using SEMP.BO;
using System.Globalization;
using System.Text.RegularExpressions;

namespace RAST.BO.Rastreabilidade
{
	public class TransferenciaLote
	{
		#region Validações

		public static Boolean VerificaLoteFechado(string numLote)
		{
			return daoTransferencia.VerificaLoteFechado(numLote);
		}

		public static string ObterNumeroLote(string NumECB)
		{
			return daoTransferencia.ObterNumeroLote(NumECB);
		}

		public static List<CBTransfLoteDTO> ObterTransferenciaAberta(int CodLinha, int NumPosto)
		{
			return daoTransferencia.ObterTransferenciaAberta(CodLinha, NumPosto);
		}

		public static List<String> ObterModeloDisponivel(string fabrica)
		{
			return daoTransferencia.ObterModeloDisponivel(fabrica);
		}

		public static List<CBLoteDTO> ObterLoteDisponivel(string CodModelo)
		{
			return daoTransferencia.ObterLoteDisponivel(CodModelo);
		}

		public static String ObterModeloTransferencia(int CodLinha, int NumPosto)
		{
			return daoTransferencia.ObterModeloTransferencia(CodLinha, NumPosto);
		}

		public static string ObterNumeroLoteCC(string NumCC)
		{
			return daoTransferencia.ObterNumeroLoteCC(NumCC);
		}

		public static int ObterLinhaDestino(int CodLinha, int NumPosto)
		{
			return daoTransferencia.ObterLinhaDestino(CodLinha, NumPosto);
		}

		public static Boolean DesfazTransferencia(int CodLinha, int NumPosto)
		{
			return daoTransferencia.DesfazTransferencia(CodLinha, NumPosto);
		}

		public static Mensagem Trasnferir(int CodLinha, int NumPosto)
		{
			Mensagem retorno = new Mensagem();

			Boolean resultado = false;

			resultado = daoTransferencia.Trasnferir(ObterTransferenciaAberta(CodLinha, NumPosto));

			if (resultado == false)
			{
				retorno = Defeito.ObterMensagem(SEMP.Model.Constantes.RAST_ERRO_TRANSF);
			}
			else
			{
				retorno = Defeito.ObterMensagem(SEMP.Model.Constantes.RAST_TRANSF_OK);
			}

			return retorno;
		}

		public static Mensagem GravarTransferencia(string NumLote, string CodDRT, int CodLinha, int NumPosto, int CodLinhaDestino)
		{
			Mensagem retorno = new Mensagem();

			Boolean resultado = false;

			CBLoteDTO lote = new CBLoteDTO();

			lote = daoTransferencia.ObterLote(NumLote);

			resultado = daoTransferencia.GravaDatRecebimento(lote);

			if (resultado == true)
			{
				CBTransfLoteDTO transflote = new CBTransfLoteDTO();
				transflote.CodDRT = CodDRT;
				transflote.CodLinhaDest = CodLinhaDestino;
				transflote.CodLinhaOri = CodLinha;
				transflote.NumPostoOri = NumPosto;
				transflote.NumLote = NumLote;
				transflote.FabDestino = int.Parse(NumLote.Substring(0, 1));

				resultado = daoTransferencia.VerificaTransferencia(transflote);

				if (resultado == false)
				{
					resultado = daoTransferencia.GravarTransLote(transflote);
					if (resultado == true)
						retorno = Defeito.ObterMensagem(Constantes.RAST_OK);
					else
						retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
				}
				else
				{
					retorno = Defeito.ObterMensagem(Constantes.RAST_LEITURA_REPETIDA);
				}
				
			}
			else
			{
				retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
			}

			return retorno;
		}

		public static int ObterTotalLoteTransf(int CodLinha, int NumPosto)
		{
			List<CBTransfLoteDTO> transfLote = new List<CBTransfLoteDTO>();
			transfLote = daoTransferencia.ObterTransferenciaAberta(CodLinha, NumPosto);

			return transfLote.Count();
		}

		public static int ObterTotalCCTransf(int CodLinha, int NumPosto)
		{
			int contador = 0;
			List<CBTransfLoteDTO> transfLote = new List<CBTransfLoteDTO>();
			transfLote = daoTransferencia.ObterTransferenciaAberta(CodLinha, NumPosto);

			foreach(CBTransfLoteDTO item in transfLote)
			{
				contador = contador + daoTransferencia.ContadorCaixaColetiva(item.NumLote);
			}

			return contador;
		}

		public static Mensagem MovimentaINS(string CodItem, string DocInterno1, string DocInterno2, int QtdTra, string DocExterno, string Empresa,
											int PrecoUnitario, string UnidPreco, string LocFixoCr, string LocFixoDb, string EmpDebito,
											string flgNegativo, string EndCr, string EndDb, string NomUsuario)
		{
			Mensagem retorno = new Mensagem();

			string CodTra = "301";
			DateTime DatTra = Util.GetDate();
			string AgeExterno = "0688";

			Boolean resultado = daoTransferencia.MovimentaINS(CodTra,DatTra, CodItem, DocInterno1, DocInterno2, AgeExterno, QtdTra,
												DocExterno, Empresa, PrecoUnitario, UnidPreco, LocFixoCr, LocFixoDb, EmpDebito, flgNegativo,
												EndCr, EndDb, NomUsuario);

			if (resultado == false)
			{
				retorno = Defeito.ObterMensagem(SEMP.Model.Constantes.RAST_ERRO_TRANSF);
			}
			else
			{
				retorno = Defeito.ObterMensagem(SEMP.Model.Constantes.RAST_TRANSF_OK);
			}

			return retorno;
		}

		#endregion
	}
}
