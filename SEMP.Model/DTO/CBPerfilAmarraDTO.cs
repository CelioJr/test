namespace SEMP.Model.DTO
{
    /// <summary>
    /// Classe respons�vel por recuperar os itens que ser�o exibidos no posto de amarra��o.
    /// </summary>
    public class CBPerfilAmarraDTO
    {
        public int CodLinha { get; set; }
        public int NumPosto { get; set; }
        public string CodModelo { get; set; }
        public int CodTipoAmarra { get; set; }
        public string CodItem { get; set; }
    }
}
