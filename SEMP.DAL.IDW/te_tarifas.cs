namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class te_tarifas
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public te_tarifas()
        {
            te_tarifasemanal = new HashSet<te_tarifasemanal>();
        }

        [Key]
        public long id_tarifas { get; set; }

        public long? id_concessionaria { get; set; }

        public long? id_tipo_consumidor { get; set; }

        public long? id_lei { get; set; }

        public DateTime? dt_inicioTarifa { get; set; }

        public DateTime? dt_fimTarifa { get; set; }

        public virtual te_concessionaria te_concessionaria { get; set; }

        public virtual te_lei te_lei { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<te_tarifasemanal> te_tarifasemanal { get; set; }

        public virtual te_tipo_consumidor te_tipo_consumidor { get; set; }
    }
}
