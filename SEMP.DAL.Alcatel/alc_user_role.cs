namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.alc_user_role")]
    public partial class alc_user_role
    {
        [Key]
        public int id_role { get; set; }

        [StringLength(20)]
        public string authority { get; set; }

        public int? id_user { get; set; }
    }
}
