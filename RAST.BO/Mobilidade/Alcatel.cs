﻿using SEMP.DAL.DAO.Mobilidade;
using SEMP.Model.DTO;
using SEMP.Model.VO.Mobilidade;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAST.BO.Mobilidade
{
	public class Alcatel
	{
		public static List<ResumoProducaoAlcatel> ObterResumoAlcatel(DateTime datInicio, DateTime datFim)
		{

			return daoAlcatel.ObterResumoAlcatel(datInicio, datFim);

		}

		public static List<ResumoProducaoAlcatel> ObterResumoAlcatelEngine(DateTime datInicio, DateTime datFim)
		{

			return daoAlcatel.ObterResumoAlcatelEngine(datInicio, datFim);

		}

		public static List<ResumoProducaoAlcatel> ObterResumoAlcatelEngineFT(DateTime datInicio, DateTime datFim)
		{

			return daoAlcatel.ObterResumoAlcatelEngineFT(datInicio, datFim);

		}

		public static List<ProducaoHoraAHora> ObterProducaoHoraAHora(DateTime Data, string CodLinha = null)
		{
			return daoAlcatel.ObterProducaoHoraAHora(Data, CodLinha);
		}

		public static List<BarrasPainelDTO> ObterPainelProducaoMOB(string codLinha)
		{

			var alcatel = daoAlcatel.ObterPainelProducaoALCATEL(codLinha);

			return alcatel ?? new List<BarrasPainelDTO>();

		}

		public static List<ResumoProducao> ObterResumoMFLinha(int codLinha)
		{

			return daoAlcatel.ObterResumoMF(codLinha);

		}

		public static List<ALC_VOLUME_DTO> ObterLotes(DateTime datInicio, DateTime datFim) {
			return daoAlcatel.ObterLotes(datInicio, datFim);
		}

		public static List<ALC_LOT_LIST_DTO> ObterLotesItens(string numLote) {
			return daoAlcatel.ObterLotesItens(numLote);
		}

		public static ALC_LOT_LIST_DTO ObterLoteItem(string numGRP)
		{
			return daoAlcatel.ObterLoteItem(numGRP);
		}

	}
}
