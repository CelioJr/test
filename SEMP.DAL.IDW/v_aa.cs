namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class v_aa
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id_folha { get; set; }

        [StringLength(30)]
        public string cd_folha { get; set; }

        [StringLength(30)]
        public string cd_tppt { get; set; }

        [StringLength(38)]
        public string nova { get; set; }
    }
}
