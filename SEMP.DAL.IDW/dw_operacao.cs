namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_operacao
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_operacao()
        {
            dw_folhaoperacao = new HashSet<dw_folhaoperacao>();
            dw_operacaocomp = new HashSet<dw_operacaocomp>();
            dw_operacaomidia = new HashSet<dw_operacaomidia>();
            dw_operacaorap = new HashSet<dw_operacaorap>();
            dw_operacaopredecessora = new HashSet<dw_operacaopredecessora>();
            dw_operacaopredecessora1 = new HashSet<dw_operacaopredecessora>();
        }

        [Key]
        public long id_operacao { get; set; }

        public long id_produtoAcabado { get; set; }

        public long id_produtoSemiacabado { get; set; }

        public long id_toperacao { get; set; }

        public decimal? seg_ciclopadrao { get; set; }

        [StringLength(60)]
        public string cd_operacao { get; set; }

        public int? revisao { get; set; }

        public short? st_ativo { get; set; }

        public DateTime? dt_stativo { get; set; }

        public DateTime? dt_revisao { get; set; }

        [StringLength(256)]
        public string ds_operacao { get; set; }

        public long? id_tppt { get; set; }

        public long? id_gtFase { get; set; }

        public int? grupoOperacoes { get; set; }

        public long? id_usrStativo { get; set; }

        public long? id_usrRevisao { get; set; }

        public long? id_pt { get; set; }

        public decimal? x { get; set; }

        public decimal? y { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhaoperacao> dw_folhaoperacao { get; set; }

        public virtual om_gt om_gt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_operacaocomp> dw_operacaocomp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_operacaomidia> dw_operacaomidia { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_operacaorap> dw_operacaorap { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_operacaopredecessora> dw_operacaopredecessora { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_operacaopredecessora> dw_operacaopredecessora1 { get; set; }

        public virtual om_produto om_produto { get; set; }

        public virtual om_produto om_produto1 { get; set; }

        public virtual om_pt om_pt { get; set; }

        public virtual dw_t_operacao dw_t_operacao { get; set; }

        public virtual om_tppt om_tppt { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }
    }
}
