﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.Models
{
	public partial class tbl_CBResumoIAC
	{
		public System.DateTime Data { get; set; }
		public int Turno { get; set; }
		public string Linha { get; set; }
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		public Nullable<int> QtdPlano { get; set; }
		public Nullable<int> QtdReal { get; set; }
		public Nullable<int> QtdDefeitos { get; set; }

        public Nullable<int> QtdDefeitosReal { get; set; }

        public Nullable<decimal> PPM{ get; set; }

        public Nullable<decimal> PPMReal { get; set; }

		public string OcorrenciaParada { get; set; }
		public Nullable<int> Apontado { get; set; }
		public Nullable<int> ApontEstoque { get; set; }
		public string OcorrenciaFaltaApont { get; set; }
		public Nullable<int> QtdPontos { get; set; }
		public Nullable<decimal> TempoPadrao { get; set; }
		public Nullable<int> GargaloReal { get; set; }
		public string CM_1_PICKUP { get; set; }
		public string CM_1_MOUNT { get; set; }
		public string CM_2_PICKUP { get; set; }
		public string CM_2_MOUNT { get; set; }
		public Nullable<int> TempoDisp { get; set; }
		public string EficienciaRealizacao { get; set; }
		public string Produtividade { get; set; }
		public string FlagTP { get; set; }
	}
}
