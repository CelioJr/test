namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_operacaocomp
    {
        [Key]
        public long id_operacaocomp { get; set; }

        public long? id_operacao { get; set; }

        public long? id_produto { get; set; }

        public virtual dw_operacao dw_operacao { get; set; }

        public virtual om_produto om_produto { get; set; }
    }
}
