﻿namespace SEMP.Testes
{
	partial class frmSAPRFCReadTable
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.grdDados = new System.Windows.Forms.DataGridView();
			this.btnConsultar = new System.Windows.Forms.Button();
			this.txtQueryTable = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.txtDelimiter = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.txtNoData = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.txtRowSkips = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.txtRowCount = new System.Windows.Forms.TextBox();
			this.cmbExistentes = new System.Windows.Forms.ComboBox();
			this.label6 = new System.Windows.Forms.Label();
			this.txtOptions = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.lblFields = new System.Windows.Forms.Label();
			this.txtFields = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.chkPing = new System.Windows.Forms.CheckBox();
			this.label9 = new System.Windows.Forms.Label();
			this.txtQtdRegistros = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.grdDados)).BeginInit();
			this.SuspendLayout();
			// 
			// grdDados
			// 
			this.grdDados.AllowUserToAddRows = false;
			this.grdDados.AllowUserToDeleteRows = false;
			this.grdDados.AllowUserToOrderColumns = true;
			this.grdDados.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.grdDados.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
			this.grdDados.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
			this.grdDados.BackgroundColor = System.Drawing.Color.White;
			this.grdDados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.grdDados.Location = new System.Drawing.Point(12, 147);
			this.grdDados.Name = "grdDados";
			this.grdDados.ReadOnly = true;
			this.grdDados.RowHeadersVisible = false;
			this.grdDados.Size = new System.Drawing.Size(849, 155);
			this.grdDados.TabIndex = 0;
			this.grdDados.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.grdDados_DataBindingComplete);
			// 
			// btnConsultar
			// 
			this.btnConsultar.Location = new System.Drawing.Point(786, 12);
			this.btnConsultar.Name = "btnConsultar";
			this.btnConsultar.Size = new System.Drawing.Size(75, 23);
			this.btnConsultar.TabIndex = 1;
			this.btnConsultar.Text = "Consultar";
			this.btnConsultar.UseVisualStyleBackColor = true;
			this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
			// 
			// txtQueryTable
			// 
			this.txtQueryTable.Location = new System.Drawing.Point(95, 15);
			this.txtQueryTable.Name = "txtQueryTable";
			this.txtQueryTable.Size = new System.Drawing.Size(116, 20);
			this.txtQueryTable.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 18);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(85, 13);
			this.label1.TabIndex = 3;
			this.label1.Text = "QUERY_TABLE";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(26, 44);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(65, 13);
			this.label2.TabIndex = 5;
			this.label2.Text = "DELIMITER";
			// 
			// txtDelimiter
			// 
			this.txtDelimiter.Location = new System.Drawing.Point(95, 41);
			this.txtDelimiter.Name = "txtDelimiter";
			this.txtDelimiter.Size = new System.Drawing.Size(22, 20);
			this.txtDelimiter.TabIndex = 4;
			this.txtDelimiter.Text = "|";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(127, 44);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(58, 13);
			this.label3.TabIndex = 7;
			this.label3.Text = "NO_DATA";
			// 
			// txtNoData
			// 
			this.txtNoData.Location = new System.Drawing.Point(189, 41);
			this.txtNoData.Name = "txtNoData";
			this.txtNoData.Size = new System.Drawing.Size(22, 20);
			this.txtNoData.TabIndex = 6;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(26, 70);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(65, 13);
			this.label4.TabIndex = 9;
			this.label4.Text = "ROWSKIPS";
			// 
			// txtRowSkips
			// 
			this.txtRowSkips.Location = new System.Drawing.Point(95, 67);
			this.txtRowSkips.Name = "txtRowSkips";
			this.txtRowSkips.Size = new System.Drawing.Size(48, 20);
			this.txtRowSkips.TabIndex = 8;
			this.txtRowSkips.Text = "0";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(19, 96);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(72, 13);
			this.label5.TabIndex = 11;
			this.label5.Text = "ROWCOUNT";
			// 
			// txtRowCount
			// 
			this.txtRowCount.Location = new System.Drawing.Point(95, 93);
			this.txtRowCount.Name = "txtRowCount";
			this.txtRowCount.Size = new System.Drawing.Size(48, 20);
			this.txtRowCount.TabIndex = 10;
			this.txtRowCount.Text = "500";
			// 
			// cmbExistentes
			// 
			this.cmbExistentes.FormattingEnabled = true;
			this.cmbExistentes.Items.AddRange(new object[] {
            "",
            "ObterEstoquePCI"});
			this.cmbExistentes.Location = new System.Drawing.Point(659, 14);
			this.cmbExistentes.Name = "cmbExistentes";
			this.cmbExistentes.Size = new System.Drawing.Size(121, 21);
			this.cmbExistentes.TabIndex = 12;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(549, 18);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(104, 13);
			this.label6.TabIndex = 13;
			this.label6.Text = "Consultas Existentes";
			// 
			// txtOptions
			// 
			this.txtOptions.Location = new System.Drawing.Point(277, 15);
			this.txtOptions.Multiline = true;
			this.txtOptions.Name = "txtOptions";
			this.txtOptions.Size = new System.Drawing.Size(266, 68);
			this.txtOptions.TabIndex = 14;
			this.txtOptions.WordWrap = false;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(217, 18);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(55, 13);
			this.label7.TabIndex = 15;
			this.label7.Text = "OPTIONS";
			// 
			// lblFields
			// 
			this.lblFields.AutoSize = true;
			this.lblFields.Location = new System.Drawing.Point(217, 92);
			this.lblFields.Name = "lblFields";
			this.lblFields.Size = new System.Drawing.Size(44, 13);
			this.lblFields.TabIndex = 17;
			this.lblFields.Text = "FIELDS";
			// 
			// txtFields
			// 
			this.txtFields.Location = new System.Drawing.Point(277, 89);
			this.txtFields.Multiline = true;
			this.txtFields.Name = "txtFields";
			this.txtFields.Size = new System.Drawing.Size(266, 51);
			this.txtFields.TabIndex = 16;
			this.txtFields.WordWrap = false;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(12, 131);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(36, 13);
			this.label8.TabIndex = 18;
			this.label8.Text = "DATA";
			// 
			// chkPing
			// 
			this.chkPing.AutoSize = true;
			this.chkPing.Enabled = false;
			this.chkPing.Location = new System.Drawing.Point(781, 44);
			this.chkPing.Name = "chkPing";
			this.chkPing.Size = new System.Drawing.Size(76, 17);
			this.chkPing.TabIndex = 19;
			this.chkPing.Text = "PING SAP";
			this.chkPing.UseVisualStyleBackColor = true;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(737, 124);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(74, 13);
			this.label9.TabIndex = 21;
			this.label9.Text = "Qtd. Registros";
			// 
			// txtQtdRegistros
			// 
			this.txtQtdRegistros.Location = new System.Drawing.Point(813, 121);
			this.txtQtdRegistros.Name = "txtQtdRegistros";
			this.txtQtdRegistros.Size = new System.Drawing.Size(48, 20);
			this.txtQtdRegistros.TabIndex = 20;
			this.txtQtdRegistros.Text = "0";
			// 
			// frmSAPRFCReadTable
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(873, 314);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.txtQtdRegistros);
			this.Controls.Add(this.chkPing);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.lblFields);
			this.Controls.Add(this.txtFields);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.txtOptions);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.cmbExistentes);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.txtRowCount);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.txtRowSkips);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.txtNoData);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.txtDelimiter);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.txtQueryTable);
			this.Controls.Add(this.btnConsultar);
			this.Controls.Add(this.grdDados);
			this.Name = "frmSAPRFCReadTable";
			this.Text = "Consulta tabela SAP";
			this.Load += new System.EventHandler(this.frmSAPRFCReadTable_Load);
			((System.ComponentModel.ISupportInitialize)(this.grdDados)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.DataGridView grdDados;
		private System.Windows.Forms.Button btnConsultar;
		private System.Windows.Forms.TextBox txtQueryTable;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtDelimiter;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtNoData;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtRowSkips;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox txtRowCount;
		private System.Windows.Forms.ComboBox cmbExistentes;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox txtOptions;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label lblFields;
		private System.Windows.Forms.TextBox txtFields;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.CheckBox chkPing;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox txtQtdRegistros;
	}
}