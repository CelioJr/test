namespace SEMP.DAL.DTB_SSA.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_VersaoEstrutura
    {
        public int CodVersao { get; set; }
        public int CodOF { get; set; }
        public string ne { get; set; }
        public System.DateTime datAtualizacao { get; set; }
        public int CodOperador { get; set; }
        public decimal QtdFrequencia { get; set; }
        public string flgRecente { get; set; }
    }
}
