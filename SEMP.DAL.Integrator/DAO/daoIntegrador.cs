﻿using SEMP.Model.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace SEMP.DAL.Integrator.DAO
{
	public class daoIntegrador
	{
		public static string ObterStringConexao()
		{
			var contexto = new dtb_Integrator();
			
			var config = contexto.Database.Connection.ConnectionString;
			//ConfigurationManager.ConnectionStrings["dtb_Integrator"].ConnectionString;
			return config;
		}

		public static List<ZSPP_APONTAMENTO_PCI_DTO> ObterApontamentoPendenteEnvio()
		{
			using (var contexto = new dtb_Integrator())
			{

				var dados = contexto.ZSPP_APONTAMENTO_PCI.Where(x => x.PACKNO.Equals("")).ToList();

				if (dados != null)
				{

					return dados.Select(x => new ZSPP_APONTAMENTO_PCI_DTO() { 
						AUFNR = x.AUFNR,
						DatEnvio = x.DatEnvio,
						EXTROW = x.EXTROW,
						MEINS = x.MEINS,
						PACKNO = x.PACKNO,
						YIELD = x.YIELD
					}).ToList();

				}

				return null;
			}

		}

		public static List<ZSPP_APONTAMENTO_PCI_DTO> ObterApontamentoPorPacote(string packno)
		{
			using (var contexto = new dtb_Integrator())
			{

				var dados = contexto.ZSPP_APONTAMENTO_PCI.Where(x => x.PACKNO.Equals(packno)).ToList();

				if (dados != null)
				{

					return dados.Select(x => new ZSPP_APONTAMENTO_PCI_DTO()
					{
						AUFNR = x.AUFNR,
						DatEnvio = x.DatEnvio,
						EXTROW = x.EXTROW,
						MEINS = x.MEINS,
						PACKNO = x.PACKNO,
						YIELD = x.YIELD
					}).ToList();

				}

				return null;
			}

		}

		public static List<ZSPP_APONTAMENTO_PCI_DTO> ObterApontamento(DateTime datInicio, DateTime datFim, string numOrdem = null, string packno = null)
		{
			using (var contexto = new dtb_Integrator())
			{

				var query = contexto.ZSPP_APONTAMENTO_PCI.Where(x => x.DatEnvio >= datInicio && x.DatEnvio <= datFim);

				if (!String.IsNullOrEmpty(numOrdem)){
					query = query.Where(x => x.AUFNR == numOrdem);
				}

				if (!String.IsNullOrEmpty(packno))
				{
					query = query.Where(x => x.PACKNO == packno);
				}

				var dados = query.ToList();

				if (dados != null)
				{

					return dados.Select(x => new ZSPP_APONTAMENTO_PCI_DTO()
					{
						AUFNR = x.AUFNR,
						DatEnvio = x.DatEnvio,
						EXTROW = x.EXTROW,
						MEINS = x.MEINS,
						PACKNO = x.PACKNO,
						YIELD = x.YIELD
					}).ToList();

				}

				return null;
			}

		}

		public static bool AtualizarApontamentoPendenteEnvio(string PackNo)
		{
			using (var contexto = new dtb_Integrator())
			{

				var dados = contexto.ZSPP_APONTAMENTO_PCI.Where(x => x.PACKNO.Equals("")).ToList();

				if (dados != null)
				{

					//dados.ForEach(x => x.PACKNO = PackNo);
					foreach (var item in dados)
					{
						item.PACKNO = PackNo;
					}
					contexto.SaveChanges();

					return true;

				}

				return false;
			}

		}

		public static bool AtualizarPackNOApontamento(string packno)
		{
			using (var contexto = new dtb_Integrator())
			{
				// assign the return code to the new output parameter and pass it to the sp
				var data = contexto.Database.SqlQuery<object>("UPDATE ZSPP_APONTAMENTO_PCI SET PACKNO = '" + packno + "' WHERE PACKNO = ''");
				var item = data.FirstOrDefault();

				if (item != null)
				{
					return true;
				}

				return false;
			}

		}

		public static bool DesfazerPackNOApontamento(string packno)
		{
			using (var contexto = new dtb_Integrator())
			{
				// assign the return code to the new output parameter and pass it to the sp
				var data = contexto.Database.SqlQuery<object>("UPDATE ZSPP_APONTAMENTO_PCI SET PACKNO = '' WHERE PACKNO = '" + packno + "'");
				var item = data.FirstOrDefault();

				if (item != null)
				{
					return true;
				}

				return false;
			}

		}

		public static string ObterPackNumber()
		{

			using (var contexto = new dtb_Integrator())
			{
				var returnCode = new SqlParameter("@PACKNO", SqlDbType.VarChar, 16);
				returnCode.Direction = ParameterDirection.Output;

				var tipo = new SqlParameter("@Tipo", SqlDbType.VarChar, 20);
				tipo.Value = "OUTBOUND";

				// assign the return code to the new output parameter and pass it to the sp
				var data = contexto.Database.SqlQuery<object>("exec spc_Numeracao2 @Tipo, @PACKNO OUT", tipo, returnCode);
				var item = data.FirstOrDefault();

				if (item != null || returnCode != null)
				{
					return returnCode.Value.ToString();
				}

				return "";
			}

		}

		public static bool AtualizarStatus(bool status)
		{

			try
			{
				using (var contexto = new dtb_Integrator())
				{
					var dado = contexto.tbl_status.Where(x => x.codapp == "APT").FirstOrDefault();

					if (dado != null)
					{
						dado.date = DateTime.Now;
						dado.status = status;

						contexto.SaveChanges();
					}
					else
					{
						return false;
					}
				}

				return true;
			}
			catch
			{
				return false;
			}

		}

		public static bool Gravar_ZSPP_AP(List<ZSPP_AP> dados){

			try
			{
				using (var contexto = new dtb_Integrator())
				{
					contexto.Entry(dados).State = System.Data.Entity.EntityState.Modified;
					contexto.SaveChanges();
				}

				return true;
			}
			catch
			{
				return false;
			}
		}

		public static bool Inserir_ZSPP_AP(List<ZSPP_AP> dados)
		{

			try
			{
				using (var contexto = new dtb_Integrator())
				{
					contexto.ZSPP_AP.AddRange(dados);
					contexto.SaveChanges();
				}

				return true;
			}
			catch
			{
				return false;
			}


		}

		public static bool ExecutarProcedureAtualizacao()
		{
			using (var contexto = new dtb_Integrator())
			{
				contexto.Database.CommandTimeout = 320;

				// assign the return code to the new output parameter and pass it to the sp
				var data = contexto.Database.SqlQuery<object>("EXEC spc_ProcessaOrdens");
				var item = data.FirstOrDefault();

				if (item != null)
				{
					return true;
				}

				return false;
			}

		}

	}
}
