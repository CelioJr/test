﻿using System.ComponentModel.DataAnnotations;


namespace SEMP.DAL.Models
{
	public class tbl_CBAcesso
	{

		[Key]
		public string CodAcesso { get; set; }
		public string AcessoDepto { get; set; }
		public string AcessoUser { get; set; }

	}
}
