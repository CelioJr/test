﻿using SEMP.DAL.DAO.Materiais;
using System;
using System.Collections.Generic;
using SEMP.Model.DTO;
using SEMP.Model.VO.SAP;
using SEMP.SAPConnector;
using System.Linq;
using SEMP.DAL.Integrator;
using SEMP.DAL.Integrator.DAO;

namespace MATERIAIS.BO.Estoque
{
	public class AP
	{
		#region Capa da AP

		public static LSAAPDTO ObterAP(string codFab, string numAP)
		{
			return daoAP.ObterAP(codFab, numAP);
		}

		public static LSAAPDTO ObterAP(string numOP)
		{
			return daoAP.ObterAP(numOP);

		}

		public static List<LSAAPDTO> ObterAPs(List<string> numOP)
		{
			return daoAP.ObterAPs(numOP);

		}

		public static List<LSAAPDTO> ObterAPs(string codModelo, DateTime datReferencia)
		{
			return daoAP.ObterAPs(codModelo, datReferencia);
		}

		public static List<LSAAPDTO> ObterAPs(DateTime datReferencia)
		{
			return daoAP.ObterAPs(datReferencia);
		}

		public static List<LSAAPDTO> ObterAPs(DateTime datReferencia, string codFase)
		{
			return daoAP.ObterAPs(datReferencia, codFase);
		}

		public static List<LSAAPDTO> ObterAPsMes(String codModelo, DateTime datReferencia)
		{
			return daoAP.ObterAPsMes(codModelo, datReferencia);
		}

		public static bool AlterarModeloIMC_AP(string codFab, string numAP, string codModelo)
		{
			return daoAP.AlterarModeloIMC_AP(codFab, numAP, codModelo);

		}

		public static bool AlterarLinha_AP(string codFab, string numAP, string codLinha)
		{
			return daoAP.AlterarLinha_AP(codFab, numAP, codLinha);

		}

		public static bool AlteraIdentificacaoItem(string codFab, string numAP, string codModelo, string codItem, string codIdentifica)
		{
			return daoAP.AlteraIdentificacaoItem(codFab, numAP, codModelo, codItem, codIdentifica);
		}

		public static bool AlterarAP(LSAAPDTO ap)
		{
			return daoAP.AlterarAP(ap);
		}
		#endregion

		#region Modelos da AP
		public static List<LSAAPModeloDTO> ObterAPModelos(string codFab, string numAP)
		{
			return daoAP.ObterAPModelos(codFab, numAP);
		}

		public static LSAAPModeloDTO ObterAPModelo(string codFab, string numAP, string codModelo)
		{
			return daoAP.ObterAPModelo(codFab, numAP, codModelo);
		}
		#endregion

		#region Itens da AP
		public static List<LSAAPItemDTO> ObterAPItens(string codFab, string numAP)
		{
			return daoAP.ObterAPItens(codFab, numAP);
		}

		public static List<LSAAPItemDTO> ObterAPItens(string codFab, string numAP, string codModelo)
		{
			return daoAP.ObterAPItens(codFab, numAP, codModelo);
		}

		public static List<LSAAPItemDTO> ObterAPItens(string numOP)
		{
			return daoAP.ObterAPItens(numOP);
		}

		public static LSAAPItemDTO ObterAPItem(string codFab, string numAP, string codModelo, string codItem, string codItemAlt)
		{
			return daoAP.ObterAPItem(codFab, numAP, codModelo, codItem, codItemAlt);
		}
		#endregion

		#region Posição de Itens da AP
		public static List<LSAAPItemPosicaoDTO> ObterAPItensPosicoes(string codFab, string numAP)
		{
			return daoAP.ObterAPItensPosicoes(codFab, numAP);
		}

		public static List<LSAAPItemPosicaoDTO> ObterAPItensPosicoes(string codFab, string numAP, string codModelo)
		{
			return daoAP.ObterAPItensPosicoes(codFab, numAP, codModelo);
		}

		public static List<LSAAPItemPosicaoDTO> ObterAPItensPosicoes(string numOP)
		{
			return daoAP.ObterAPItensPosicoes(numOP);
		}

		public static LSAAPItemPosicaoDTO ObterAPItemPosicao(string codFab, string numAP, string codModelo, string codItem, string numPosicao)
		{
			return daoAP.ObterAPItemPosicao(codFab, numAP, codModelo, codItem, numPosicao);

		}
		#endregion

		#region Ordens de Produção SAP
		public static ZTPP_OPMAO ObterOP(string numOP)
		{

			if (String.IsNullOrEmpty(numOP))
			{
				return null;
			}

			var dados = SAPFunctions.Obter_ZTPP_OPMAO(numOP);

			var ap = ObterAP(dados.AUFNR);

			var retorno = new ZTPP_OPMAO()
			{
				MANDT = dados.MANDT,
				ID = dados.ID,
				WERKS = dados.WERKS,
				AUFNR = dados.AUFNR,
				TYPE = dados.TYPE,
				MATNR = dados.MATNR,
				ARBPL = dados.ARBPL,
				OQTY = dados.OQTY,
				WEMNG = dados.WEMNG,
				DTCV = dados.DTCV,
				DTIN = dados.DTIN,
				DTFM = dados.DTFM,
				DTREF = dados.DTREF,
				DTMOD = dados.DTMOD,
				SYSTEM_STATUS = dados.SYSTEM_STATUS,
				STAT1 = dados.STAT1,
				DTPI = dados.DTPI,
				STAT2 = dados.STAT2,
				DTSISAP = dados.DTSISAP,
				PACKNO = dados.PACKNO
			};

			if (ap != null){
				retorno.AP = ap;
			}


			return retorno;

		}

		public static List<ZTPP_OPMAO> ObterOP(string datInicio, string datFim)
		{

			if (!String.IsNullOrEmpty(datInicio))
			{
				var bPassou = DateTime.TryParse(datInicio, out DateTime data);
				if (bPassou)
				{
					datInicio = data.ToString("yyyyMMdd");
				}
				bPassou = DateTime.TryParse(datFim, out data);
				if (bPassou)
				{
					datFim = data.ToString("yyyyMMdd");
				}
			}

			var dados = String.IsNullOrEmpty(datInicio) ? SAPFunctions.Obter_ZTPP_OPMAO() : SAPFunctions.Obter_ZTPP_OPMAO(datInicio, datFim);
			var ordens = dados.Select(x => x.AUFNR).ToList();

			var aps = ObterAPs(ordens);

			var retorno = dados.Select(x => new ZTPP_OPMAO()
			{
				MANDT = x.MANDT,
				ID = x.ID,
				WERKS = x.WERKS,
				AUFNR = x.AUFNR,
				TYPE = x.TYPE,
				MATNR = x.MATNR,
				ARBPL = x.ARBPL,
				OQTY = x.OQTY,
				WEMNG = x.WEMNG,
				DTCV = x.DTCV,
				DTIN = x.DTIN,
				DTFM = x.DTFM,
				DTREF = x.DTREF,
				DTMOD = x.DTMOD,
				SYSTEM_STATUS = x.SYSTEM_STATUS,
				STAT1 = x.STAT1,
				DTPI = x.DTPI,
				STAT2 = x.STAT2,
				DTSISAP = x.DTSISAP,
				PACKNO = x.PACKNO
			}
			).ToList();

			foreach (var item in retorno)
			{
				var ap = aps.FirstOrDefault(x => x.NumOP == item.AUFNR);
				if (ap != null)
				{
					item.AP = ap;
				}
			}

			return retorno;

		}

		public static bool SAP_AtualizaOrdens(string datInicio, string datFim)
		{

			try
			{

				var dados = ObterOP(datInicio, datFim);

				if (dados != null)
				{
					dados = dados.Where(x => x.bDivergencia).ToList();

					foreach (var item in dados)
					{
						SAP_AtualizaOrdem(item.AUFNR);
						#region movido para outra função
						/*
						var alterar = false;

						if (item.AP != null)
						{
							if (!item.MATNR.Contains(item.AP.CodModelo))
							{
								// TRATAR AQUI QUANDO O MODELO FOR DIFERENTE. TEM QUE REIMPORTAR A ORDEM
								//item.AP.CodModelo = double.Parse(item.MATNR).ToString();
								daoAP.ApagarAP(item.AP.CodFab, item.AP.NumAP);
							}
							else
							{
								// Quantidade LOTE diferente
								if (item.OQTY != item.AP.QtdLoteAP)
								{
									item.AP.QtdLoteAP = (int)item.OQTY;
									alterar = true;
								}
								// Ordem encerrada com status diferente de 04
								if (item.SYSTEM_STATUS.StartsWith("EN") && item.AP.CodStatus != "04")
								{
									item.AP.CodStatus = "04";
									alterar = true;
								}
								// Ordem aberta com status diferente de 03
								if (!item.SYSTEM_STATUS.StartsWith("EN") && item.AP.CodStatus != "03")
								{
									item.AP.CodStatus = "03";
									alterar = true;
								}
								// Data de referência diferente
								if (item.DTREF != item.AP.DatReferencia)
								{
									item.AP.DatReferencia = item.DTREF;
									alterar = true;
								}

								// realiza as correções necessárias na AP
								if (alterar)
								{
									AlterarAP(item.AP);
								}
							}
						}*/
						#endregion
					}
				}

				return true;
			}
			catch
			{
				return false;
			}
		}

		public static bool SAP_AtualizaOrdem(string numOP)
		{

			try
			{

				var dados = AP.ObterOP(numOP);

				if (dados != null)
				{
					var alterar = false;

					if (dados.AP != null)
					{
						if (!dados.MATNR.Contains(dados.AP.CodModelo))
						{
							// TRATAR AQUI QUANDO O MODELO FOR DIFERENTE. TEM QUE REIMPORTAR A ORDEM
							//item.AP.CodModelo = double.Parse(item.MATNR).ToString();
							daoAP.ApagarAP(dados.AP.CodFab, dados.AP.NumAP);
						}
						else
						{
							// Quantidade LOTE diferente
							if (dados.OQTY != dados.AP.QtdLoteAP)
							{
								dados.AP.QtdLoteAP = (int)dados.OQTY;
								alterar = true;
							}
							// Ordem encerrada com status diferente de 04
							if (dados.SYSTEM_STATUS.StartsWith("EN") && dados.AP.CodStatus != "04")
							{
								dados.AP.CodStatus = "04";
								alterar = true;
							}
							// Ordem aberta com status diferente de 03
							if (!dados.SYSTEM_STATUS.StartsWith("EN") && dados.AP.CodStatus != "03")
							{
								dados.AP.CodStatus = "03";
								alterar = true;
							}
							// Data de referência diferente
							if (dados.DTREF != dados.AP.DatReferencia)
							{
								dados.AP.DatReferencia = dados.DTREF;
								alterar = true;
							}

							// realiza as correções necessárias na AP
							if (alterar)
							{
								AlterarAP(dados.AP);
							}
						}
					}

				}

				return true;
			}
			catch
			{
				return false;
			}
		}

		public static bool SAP_ImportarOrdens(string datInicio, string datFim)
		{

			try
			{

				var ordens = ObterOP(datInicio, datFim);

				if (ordens == null) return false;

				ordens = ordens.Where(x => x.AP == null).ToList();

				var extrow = 0;
				var packno = daoIntegrador.ObterPackNumber();

				foreach (var ordem in ordens)
				{
					var dados = new BAPI_PRODORD_GET_DETAIL
					{
						NUMBER = ordem.AUFNR
					};
					dados.ORDER_OBJECTS.COMPONENTS = "X";
					dados.ORDER_OBJECTS.HEADER = "X";
                    dados.ORDER_OBJECTS.OPERATIONS = "X";
                    dados.ORDER_OBJECTS.POSITIONS = "X";

                    var retorno = SAPFunctions.BAPI_PRODORD_GET_DETAIL(ref dados);
					var resb = SAPFunctions.ObterRESB(ordem.AUFNR);

					var zspp_ap = new List<ZSPP_AP>();

					var header = dados.HEADER.FirstOrDefault();
					var position = dados.POSITION.FirstOrDefault();
                    var operation = dados.OPERATION.FirstOrDefault();

                    extrow++;

					foreach (var item in dados.COMPONENT)
					{
						var ap = new ZSPP_AP();
						var posicao = resb.FirstOrDefault(x => x.rspos == item.RESERVATION_ITEM);

						ap.PACKNO = packno;
						ap.EXTROW = extrow.ToString("0000000000");

                        ap.WERKS = header.PRODUCTION_PLANT;
                        ap.AUFNR = header.ORDER_NUMBER;
                        ap.TYPE = header.ORDER_TYPE;
                        ap.MATNR = header.MATERIAL;
                        ap.ARBPL = operation.WORK_CENTER;
                        ap.OQTY = header.TARGET_QUANTITY;
                        ap.DTREF = header.START_DATE.AddDays(-header.START_DATE.Day).AddDays(1);
                        ap.SYSTEM_STATUS = header.SYSTEM_STATUS;
                        //ap.WEMNG = header.CONFIRMED_QUANTITY.ToString().Replace(',', '.');
                        if (position.DELIVERED_QUANTITY != null)
                        {
                            ap.WEMNG = position.DELIVERED_QUANTITY.ToString().Replace(',', '.');
                        }

                        ap.IAUFNR = header.ORDER_NUMBER;
                        ap.IWERKS = header.PRODUCTION_PLANT;
                        ap.IMATNR = item.MATERIAL;
                        ap.BDMNG = item.REQ_QUAN;
                        ap.MEINS = item.BASE_UOM;
                        ap.SORTF = posicao.sortf;
                        ap.MATKL = posicao.matkl;
                        ap.BAUGR = posicao.baugr;

                        zspp_ap.Add(ap);

					}

					daoIntegrador.Inserir_ZSPP_AP(zspp_ap);

				}

				daoIntegrador.ExecutarProcedureAtualizacao();

				return true;
			}
			
			catch
			{
				return false;
			}
		}

		public static bool SAP_ImportarOrdem(string numOP)
		{

			try
			{
				var extrow = 0;
				var packno = daoIntegrador.ObterPackNumber();


				var dados = new BAPI_PRODORD_GET_DETAIL
				{
					NUMBER = numOP
				};
				dados.ORDER_OBJECTS.COMPONENTS = "X";
				dados.ORDER_OBJECTS.HEADER = "X";
				dados.ORDER_OBJECTS.OPERATIONS = "X";
				dados.ORDER_OBJECTS.POSITIONS = "X";

				var retorno = SAPFunctions.BAPI_PRODORD_GET_DETAIL(ref dados);
				var resb = SAPFunctions.ObterRESB(numOP);

				var zspp_ap = new List<ZSPP_AP>();

				var header = dados.HEADER.FirstOrDefault();
				var position = dados.POSITION.FirstOrDefault();
				var operation = dados.OPERATION.FirstOrDefault();

				extrow++;

				foreach (var item in dados.COMPONENT)
				{
					var ap = new ZSPP_AP();
					var posicao = resb.FirstOrDefault(x => x.rspos == item.RESERVATION_ITEM);

					ap.PACKNO = packno;
					ap.EXTROW = extrow.ToString("0000000000");

					ap.WERKS = header.PRODUCTION_PLANT;
					ap.AUFNR = header.ORDER_NUMBER;
					ap.TYPE = header.ORDER_TYPE;
					ap.MATNR = header.MATERIAL;
					ap.ARBPL = operation.WORK_CENTER;
					ap.OQTY = header.TARGET_QUANTITY;
					ap.DTREF = header.START_DATE.AddDays(-header.START_DATE.Day).AddDays(1);
					ap.SYSTEM_STATUS = header.SYSTEM_STATUS;
					//ap.WEMNG = header.CONFIRMED_QUANTITY.ToString().Replace(',', '.');
					if (position.DELIVERED_QUANTITY != null)
					{
						ap.WEMNG = position.DELIVERED_QUANTITY.ToString().Replace(',', '.');
					}


					ap.IAUFNR = header.ORDER_NUMBER;
					ap.IWERKS = header.PRODUCTION_PLANT;
					ap.IMATNR = item.MATERIAL;
					ap.BDMNG = item.REQ_QUAN;
					ap.MEINS = item.BASE_UOM;
					ap.SORTF = posicao.sortf;
					ap.MATKL = posicao.matkl;
					ap.BAUGR = posicao.baugr;

					zspp_ap.Add(ap);

				}

				daoIntegrador.Inserir_ZSPP_AP(zspp_ap);

				daoIntegrador.ExecutarProcedureAtualizacao();

				return true;
			}
			catch
			{
				return false;
			}


		}
		#endregion
	}
}
