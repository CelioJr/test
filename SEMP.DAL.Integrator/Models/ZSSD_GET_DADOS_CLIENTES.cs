namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ZSSD_GET_DADOS_CLIENTES
    {
        public int ID { get; set; }

        [StringLength(16)]
        public string PACKNO { get; set; }

        [StringLength(10)]
        public string EXTROW { get; set; }

        [StringLength(4)]
        public string ORGANIZACAOVENDAS { get; set; }

        [StringLength(2)]
        public string CANALDISTRIBUICAO { get; set; }

        [StringLength(2)]
        public string SETORATIVIDADE { get; set; }

        [StringLength(10)]
        public string CODIGOSAP { get; set; }

        [StringLength(16)]
        public string CNPJ { get; set; }

        [StringLength(11)]
        public string CPF { get; set; }

        [StringLength(18)]
        public string INSCRICAOESTADUAL { get; set; }

        [StringLength(35)]
        public string NOME { get; set; }

        [StringLength(60)]
        public string RUA { get; set; }

        [StringLength(10)]
        public string NUMERO { get; set; }

        [StringLength(10)]
        public string COMPLEMENTO { get; set; }

        [StringLength(40)]
        public string CIDADE { get; set; }

        [StringLength(40)]
        public string BAIRRO { get; set; }

        [StringLength(10)]
        public string CEP { get; set; }

        [StringLength(3)]
        public string UF { get; set; }

        [StringLength(3)]
        public string PAIS { get; set; }

        [StringLength(3)]
        public string RELEVANTEST { get; set; }

        [StringLength(1)]
        public string BLOQUEADO { get; set; }

        [StringLength(9)]
        public string CODIGOSUFRAMA { get; set; }

        [StringLength(1)]
        public string PESSOAFISICA { get; set; }

        [StringLength(30)]
        public string TELEFONE1 { get; set; }

        [StringLength(30)]
        public string TELEFONE2 { get; set; }

        [StringLength(2)]
        public string TIPODECLARACAOEMPRESA { get; set; }

        [StringLength(2)]
        public string REGIMEPISCOFINS { get; set; }

        [StringLength(3)]
        public string VENDEDOR { get; set; }

        [StringLength(4)]
        public string GERENTE { get; set; }

        [StringLength(4)]
        public string GRUPOCONTASCLIENTE { get; set; }

        [StringLength(241)]
        public string EMAIL { get; set; }

        [StringLength(10)]
        public string HIERARQUIACLIENTE { get; set; }

        [StringLength(2)]
        public string ICMSTAXPAY { get; set; }

        [StringLength(1)]
        public string ZAHLS { get; set; }

        [StringLength(4)]
        public string BUKRS { get; set; }

        public DateTime? UPDATE_TIME_SAP { get; set; }

        public DateTime? UPDATE_TIME_LEGADO { get; set; }

        [StringLength(500)]
        public string OBS_INTEGRATOR { get; set; }
    }
}
