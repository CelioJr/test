using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_GPANotaMap : EntityTypeConfiguration<tbl_GPANota>
    {
        public tbl_GPANotaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodSerie, t.NumNota, t.CodModelo });

            // Properties
            this.Property(t => t.CodSerie)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NumNota)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodLocal)
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.NomUsuario)
                .HasMaxLength(30)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_GPANota");
            this.Property(t => t.CodSerie).HasColumnName("CodSerie");
            this.Property(t => t.NumNota).HasColumnName("NumNota");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.DatEntrada).HasColumnName("DatEntrada");
            this.Property(t => t.CodLocal).HasColumnName("CodLocal");
            this.Property(t => t.QtdNota).HasColumnName("QtdNota");
            this.Property(t => t.QtdRecebido).HasColumnName("QtdRecebido");
            this.Property(t => t.NomUsuario).HasColumnName("NomUsuario");
        }
    }
}
