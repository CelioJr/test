﻿using System;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
    public class IndicadorPostos
    {
        public string Titulo { get; set; }
        public decimal Quantidade { get; set; }
        public int NumPosto { get; set; }

        public DateTime dataEvento { get; set; }
        public IndicadorPostos() { }

        public IndicadorPostos(string titulo) 
        {
            this.Titulo = titulo;
        }

    }
}
