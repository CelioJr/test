using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_Movimentacao
    {
        public string CodFab { get; set; }
        public System.Guid NumSeq { get; set; }
        public string CreDeb { get; set; }
        public string CodTra { get; set; }
        public System.DateTime DatTra { get; set; }
        public string CodItem { get; set; }
        public string DocInterno1 { get; set; }
        public string DocInterno2 { get; set; }
        public string AgeExterno { get; set; }
        public string CodLocal { get; set; }
        public System.DateTime DatDig { get; set; }
        public decimal QtdTra { get; set; }
        public string DocExterno { get; set; }
        public Nullable<decimal> PrecoUnitario { get; set; }
        public string UnidPreco { get; set; }
        public string NomUsuario { get; set; }
    }
}
