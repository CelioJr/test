namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_folhacic
    {
        [Key]
        public long id_folhacic { get; set; }

        public decimal? seg_ciclopadrao { get; set; }

        public long? id_folha { get; set; }

        public long? id_pt { get; set; }

        public long? id_gt { get; set; }

        public int? qt_pacoteciclo { get; set; }

        public decimal? qt_fatorcontagem { get; set; }

        public virtual dw_folha dw_folha { get; set; }

        public virtual om_gt om_gt { get; set; }

        public virtual om_pt om_pt { get; set; }
    }
}
