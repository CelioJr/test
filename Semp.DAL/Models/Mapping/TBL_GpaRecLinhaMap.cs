using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class TBL_GpaRecLinhaMap : EntityTypeConfiguration<TBL_GpaRecLinha>
    {
        public TBL_GpaRecLinhaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdRegistro, t.CodLinha, t.QtdPorLinha, t.CodItem });

            // Properties
            this.Property(t => t.IdRegistro)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodLinha)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.QtdPorLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.Status)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("TBL_GpaRecLinha");
            this.Property(t => t.IdRegistro).HasColumnName("IdRegistro");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.QtdPorLinha).HasColumnName("QtdPorLinha");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.QtdLida).HasColumnName("QtdLida");
            this.Property(t => t.UltimoCodPalletLido).HasColumnName("UltimoCodPalletLido");
            this.Property(t => t.Status).HasColumnName("Status");
        }
    }
}
