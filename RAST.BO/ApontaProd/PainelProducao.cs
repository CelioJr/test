﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using SEMP.Model.DTO;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using SEMP.DAL.DAO.CNQ;
using RAST.BO.CNQ;
using SEMP.Model.VO.CNQ;
using SEMP.DAL.Models;

namespace RAST.BO.Relatorios
{
	public class PainelProducao
	{

		public static List<BarrasPainelDTO> ObterPainelProducao()
		{

			var dados = daoPainelProducao.ObterPainelProducao();

			var alcatel = daoPainelProducao.ObterPainelProducaoALCATEL();
			if (alcatel != null && alcatel.Count() > 0){
				dados.AddRange(alcatel);
			}

			return dados ?? new List<BarrasPainelDTO>();

		}

		public static List<BarrasPainelDTO> ObterPainelProducao(string codLinha)
		{

			return daoPainelProducao.ObterPainelProducao(codLinha) ?? new List<BarrasPainelDTO>();

		}

		public static List<BarrasPainel_STIDTO> ObterPainelProducaoSTI()
		{

			return daoPainelProducao.ObterPainelProducaoSTI() ?? new List<BarrasPainel_STIDTO>();

		}

		public static List<BarrasPainelDTO> ObterPainelProducaoIMC()
		{

			return daoPainelProducao.ObterPainelProducaoIMC() ?? new List<BarrasPainelDTO>();

		}

		public static List<BarrasPainelDTO> ObterPainelProducaoIMC(string codLinha)
		{

			return daoPainelProducao.ObterPainelProducaoIMC(codLinha) ?? new List<BarrasPainelDTO>();

		}

		public static List<BarrasPainelDTO> ObterPainelProducaoIAC()
		{

			return daoPainelProducao.ObterPainelProducaoIAC() ?? new List<BarrasPainelDTO>();

		}

		public static List<BarrasPainelDTO> ObterPainelProducaoIAC(string codLinha)
		{

			return daoPainelProducao.ObterPainelProducaoIAC(codLinha) ?? new List<BarrasPainelDTO>();

		}


		public static List<BarrasPainelDTO> ObterPainelProducaoIAC(int codLinha)
		{

			return daoPainelProducao.ObterPainelProducaoIAC(codLinha) ?? new List<BarrasPainelDTO>();

		}

		public static List<MensagemPainelDTO> ObterMensagens()
		{

			return daoPainelProducao.ObterMensagens();

		}

		public static List<BarrasPainelLinhaMSG> ObterMensagensLinha(string codLinha)
		{

			return daoPainelProducao.ObterMensagensLinha(codLinha);

		}

		public static List<ResumoProducao> ObterResumoMFLinha(DateTime datInicio, DateTime datFim, string codLinha)
		{

			return daoResumoProducao.ObterResumoMF(datInicio, datFim).Where(x => x.CodLinha == codLinha).ToList();

		}

		public static List<ResumoProducao> ObterResumoMFLinha(string codLinha)
		{

			return daoPainelProducao.ObterResumoMF(codLinha);

		}

		public static List<ResumoProducao> ObterResumoIMCLinha(DateTime datInicio, DateTime datFim, string codLinha)
		{

			return daoResumoProducao.ObterResumoIMC(datInicio, datFim).Where(x => x.CodLinha == codLinha).ToList();

		}

		public static List<ResumoProducao> ObterResumoIMCLinha(string codLinha)
		{

			return daoPainelProducao.ObterResumoIMC(codLinha);

		}

		public static List<ResumoProducao> ObterResumoIACLinha(string codLinha)
		{

			return daoPainelProducao.ObterResumoIAC(codLinha).ToList();

		}

		public static List<ResumoProducao> ObterResumoIACLinha(DateTime datInicio, DateTime datFim, string codLinha)
		{

			return daoResumoProducao.ObterResumoIAC(datInicio, datFim).Where(x => x.CodLinha.Trim() == codLinha).ToList();

		}

		public static List<v_paradasDTO> ObterParadasIDW(DateTime datInicio, DateTime datFim, string codLinha)
		{

			return daoIDW.ObterParadasIDW(datInicio, datFim, codLinha).Take(3).ToList();

		}

		public static CBLinhaDTO ObterCodLinha(int codLinha)
		{

			return daoConfiguracao.ObterLinha(codLinha);

		}

		public static CBObterHorasTrab ObterHorasTrab(int codLinha)
		{

			return daoPainelProducao.ObterHorasTrab(codLinha);

		}

		public static Ppm ObterPPM(DateTime datInicio, DateTime datFim, int codLinha, string codModelo, string desModelo, decimal quantidadeProduzida, string numAP)
		{

			var quantidadePontos = daoResumoIAC.ObterQuantidadePontos(codModelo);

			var modeloIMC = "";
			if (String.IsNullOrEmpty(numAP))
			{
				modeloIMC = daoPainelProducao.ObterModeloFase(codModelo, "02");
			}
			else
			{
				var ap = daoGeral.ObterAP("62", numAP);
				if (ap != null){
					modeloIMC = ap.CodRadial;
				}

			}

			int quantidadeDefeito = daoQualidade.GetQuantidadeDefeitoIACPorLinha(datInicio, datFim, codLinha, modeloIMC);
			var calculo = quantidadeProduzida > 0 && quantidadePontos > 0 ? (double)quantidadeDefeito / (double)(quantidadeProduzida * quantidadePontos) : 0;
			var calculoPpm = Math.Round((double)calculo * 1000000, 1);
			string sinalizacao;

			if (quantidadePontos != 0)
			{
				if (calculoPpm <= 30)
				{
					sinalizacao = "sinalVerde";
				}
				else
					if (calculoPpm > 30 && calculoPpm < 50)
					{
						sinalizacao = "sinalAmarelo";
					}
					else
					{
						sinalizacao = "sinalVermelho";
					}
			}
			else
			{
				sinalizacao = "sinalAzul";
			}

			Ppm ppm = new Ppm
			{
				CodModelo = codModelo,
				DesModelo = desModelo,
				QuantidadePontos = quantidadePontos,
				QuantidadeDefeito = quantidadeDefeito,
				Sinalizacao = sinalizacao,
				Calculo = calculoPpm
			};

			return ppm;
		}

		public static Ppm ObterPPMReal(DateTime datInicio, DateTime datFim, int codLinha, string codModelo, string desModelo, decimal quantidadeProduzida, string numAP)
		{

			var quantidadePontos = daoResumoIAC.ObterQuantidadePontos(codModelo);
			var modeloIMC = "";
			if (String.IsNullOrEmpty(numAP))
			{
				modeloIMC = daoPainelProducao.ObterModeloFase(codModelo, "02");
			}
			else
			{
				var ap = daoGeral.ObterAP("62", numAP);
				if (ap != null)
				{
					modeloIMC = ap.CodRadial;
				}

			}
			int quantidadeDefeito = daoQualidade.GetQuantidadeDefeitoIACPorLinhaReal(datInicio, datFim, codLinha, modeloIMC);
			int quantidadeFalsaFalha = daoQualidade.GetQuantidadeDefeitoIACPorLinhaFalsaFalha(datInicio, datFim, codLinha, modeloIMC);
			var calculo = quantidadeDefeito > 0  && quantidadeProduzida > 0 && quantidadePontos > 0 ? (double)(quantidadeDefeito-quantidadeFalsaFalha) / (double)(quantidadeProduzida * quantidadePontos) : 0;
			var calculoPpm = Math.Round((double)calculo * 1000000, 1);
			string sinalizacao;

			if (quantidadePontos != 0)
			{
				if (calculoPpm <= 20)
				{
					sinalizacao = "sinalVerde";
				}
				else
					if (calculoPpm > 20 && calculoPpm < 30)
				{
					sinalizacao = "sinalAmarelo";
				}
				else
				{
					sinalizacao = "sinalVermelho";
				}
			}
			else
			{
				sinalizacao = "sinalAzul";
			}

			Ppm ppm = new Ppm
			{
				CodModelo = codModelo,
				DesModelo = desModelo,
				QuantidadePontos = quantidadePontos,
				QuantidadeDefeito = quantidadeDefeito,
				Sinalizacao = sinalizacao,
				Calculo = calculoPpm,
				QtdFalsaFalha = quantidadeFalsaFalha
			};

			return ppm;
		}

		#region Mobilidade
		public static List<BarrasPainelDTO> ObterPainelProducaoMOB()
		{

			var alcatel = daoPainelProducao.ObterPainelProducaoALCATEL();

			return alcatel ?? new List<BarrasPainelDTO>();

		}
		#endregion

		#region Manutenção mensagem painel
		public static List<MensagemPainelDTO> ObterMensagemPainel()
		{

			return daoPainelProducao.ObterMensagemPainel();

		}
		public static MensagemPainelDTO ObterMensagemPainel(long codMensagem)
		{
			return daoPainelProducao.ObterMensagemPainel(codMensagem);
		}
		public static void GravarMensagemPainel(MensagemPainelDTO mensagem)
		{
			daoPainelProducao.GravarMensagemPainel(mensagem);
		}
		public static void RemoverMensagemPainel(long codMensagem)
		{
			daoPainelProducao.RemoverMensagemPainel(codMensagem);
		}


		#endregion

	}
}
