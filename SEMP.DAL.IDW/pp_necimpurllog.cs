namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_necimpurllog
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pp_necimpurllog()
        {
            pp_nec = new HashSet<pp_nec>();
        }

        [Key]
        public long id_necimpurllog { get; set; }

        public DateTime? dthr_iimportacao { get; set; }

        public DateTime? dthr_fimportacao { get; set; }

        public long id_necimplog { get; set; }

        public long id_necimpurl { get; set; }

        public short? st_imp { get; set; }

        [StringLength(256)]
        public string ds_erro { get; set; }

        [StringLength(255)]
        public string url_arquivo { get; set; }

        [StringLength(40)]
        public string aba { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_nec> pp_nec { get; set; }

        public virtual pp_necimplog pp_necimplog { get; set; }

        public virtual pp_necimpurl pp_necimpurl { get; set; }
    }
}
