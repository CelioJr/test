namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ms_perfilandon
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ms_perfilandon()
        {
            ms_ic = new HashSet<ms_ic>();
            ms_perfilregras = new HashSet<ms_perfilregras>();
        }

        [Key]
        public long id_perfilandon { get; set; }

        [StringLength(60)]
        public string cd_perfilandon { get; set; }

        public int? revisao { get; set; }

        [StringLength(100)]
        public string ds_perfilandon { get; set; }

        public int? st_ativo { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? dt_revisao { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? dt_stativo { get; set; }

        public long? id_usrRevisao { get; set; }

        public long? id_usrStAtivo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_ic> ms_ic { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ms_perfilregras> ms_perfilregras { get; set; }

        public virtual ms_usr ms_usr { get; set; }

        public virtual ms_usr ms_usr1 { get; set; }
    }
}
