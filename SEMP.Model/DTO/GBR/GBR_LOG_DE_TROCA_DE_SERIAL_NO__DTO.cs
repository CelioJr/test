using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_LOG_DE_TROCA_DE_SERIAL_NO__DTO
    {
        public int GBR_LOG_ID { get; set; }
        public string IMEI_BASE { get; set; }
        public string SERIAL_ANTERIOR { get; set; }
        public string SERIAL_NOVO { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
    }
}
