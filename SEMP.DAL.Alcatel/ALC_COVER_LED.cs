namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_COVER_LED")]
    public partial class ALC_COVER_LED
    {
        [Key]
        [StringLength(255)]
        public string ID_COVER { get; set; }

        public DateTime? PROD_DATE { get; set; }

        [StringLength(20)]
        public string OP { get; set; }

        public int? COD_LINE { get; set; }

        public int? COD_SKU { get; set; }

        public virtual ALC_ORDER ALC_ORDER { get; set; }
    }
}
