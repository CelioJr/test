using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_RMB_REGISTER_DTO
    {
        public decimal ID_RMB { get; set; }
        public Nullable<int> QUANTITY { get; set; }
        public string SERIAL { get; set; }
        public string RMB_STATUS { get; set; }
        public Nullable<decimal> TOTAL_RMB { get; set; }
        public Nullable<decimal> RMB_COST { get; set; }
        public string DEFECT { get; set; }
        public string PART_NUMBER { get; set; }
        public string REASON { get; set; }
        public Nullable<System.DateTime> CREATION_DATE { get; set; }
        public Nullable<System.DateTime> EDITION_DATE { get; set; }
        public virtual ALC_DEFECT_DTO ALC_DEFECT { get; set; }
        public virtual ALC_REPAIR_REASON_DTO ALC_REPAIR_REASON { get; set; }
        public virtual ALC_RMB_COST_DTO ALC_RMB_COST { get; set; }
        public virtual ALC_RMB_PART_NUMBER_DTO ALC_RMB_PART_NUMBER { get; set; }
    }
}
