﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.PM;
using SEMP.Model.DTO;

namespace RAST.BO.PM
{
	public class ProgramaProducao
	{

		public static List<ProgDiaJitDTO> ObterProgramacao(DateTime mes, string fase, string codlinha, string familia)
		{
            #region Fechamento
            if (fase.Equals("F"))
			{
                var retorno = daoProgramaProducao.ObterProgramacao(mes, codlinha, familia).OrderBy(x => x.CodLinha).ThenBy(x => x.DatProducao).ThenBy(x => x.CodModelo).ToList();

				var aps = daoProgramaProducao.ObterAPs(mes);
                var apsResumo = aps.GroupBy(x => new { x.CodModelo, x.CodStatus }).Select(x => new SaldoModelo(){ CodModelo = x.Key.CodModelo, CodStatus = x.Key.CodStatus, QtdSaldo = x.Sum(z => z.QtdLoteAP) ?? 0 }).ToList();

				foreach (var item in retorno)
				{
                    item.CorPrograma = "red";

                    // se existir AP para o produto
                    if (apsResumo.Any(x => x.CodModelo == item.CodModelo))
                    {
                        // grava a quantidade do programa do dia
                        var progDia = (int)(item.QtdProducao ?? item.QtdProdPm);

                        // apura o saldo de APs liberadas para apontamento
                        var saldo = apsResumo.FirstOrDefault(x => x.CodModelo == item.CodModelo && x.CodStatus == "03" && x.QtdSaldo > 0);

                        // se encontrou o saldo continua
                        if (saldo != null)
                        {
                            item.CorPrograma = "green";

                            saldo.QtdSaldo = saldo.QtdSaldo - progDia;

                        }
                        else // se no encontrou saldo de AP liberada para apontamento
                        {
                            var saldo02 = apsResumo.FirstOrDefault(x => x.CodModelo == item.CodModelo && x.CodStatus != "03" && x.QtdSaldo >= 0);

                            if (saldo02 != null)
                            {
                                item.CorPrograma = "blue";

                                saldo02.QtdSaldo -= progDia;

                            }
                        }
					}
                    
				}

				return retorno;
			}
            #endregion
            #region Placas
            else
			{
				familia = familia.Replace("TV ", "");
				familia = familia.Replace("AUDIO", "AUD");
				familia = familia.Replace("PA STI", "INF");

                var tipo = fase.ToUpper().Equals("I") ? "01" : "02";

                var retorno = daoProgramaProducao.ObterProgramacaoPlacas(mes, fase, codlinha, familia).OrderBy(x => x.CodLinha).ThenBy(x => x.DatProducao).ThenBy(x => x.CodModelo).ToList();

                var aps = daoProgramaProducao.ObterAPs(mes, tipo);

                var apsResumo = aps.GroupBy(x => new { x.CodModelo, x.CodStatus }).Select(x => new SaldoModelo() { CodModelo = x.Key.CodModelo, CodStatus = x.Key.CodStatus, QtdSaldo = x.Sum(z => z.QtdLoteAP) ?? 0 }).ToList();

                foreach (var item in retorno)
                {
                    item.CorPrograma = "red";

                    // se existir AP para o produto
                    if (apsResumo.Any(x => x.CodModelo == item.CodModelo))
                    {
                        // grava a quantidade do programa do dia
                        var progDia = (int)(item.QtdProducao ?? item.QtdProdPm);

                        // apura o saldo de APs liberadas para apontamento
                        var saldo = apsResumo.FirstOrDefault(x => x.CodModelo == item.CodModelo && x.CodStatus == "03" && x.QtdSaldo >= 0);

                        // se encontrou o saldo continua
                        if (saldo != null)
                        {
                            item.CorPrograma = "green";

                            saldo.QtdSaldo = saldo.QtdSaldo - progDia;

                        }
                        else // se não encontrou saldo de AP liberada para apontamento
                        {
                            var saldo02 = apsResumo.FirstOrDefault(x => x.CodModelo == item.CodModelo && x.CodStatus != "03" && x.QtdSaldo >= 0);

                            if (saldo02 != null)
                            {
                                item.CorPrograma = "blue";

                                saldo02.QtdSaldo -= progDia;

                            }
                        }
                    }

                }

                return retorno;
            }
            #endregion
        }

		public static List<ProgDiaJitDTO> ObterProgramacaoSAP(DateTime datInicio){

			return daoProgramaProducao.ObterProgramacaoSAP(datInicio);

		}


		class SaldoModelo
        {
            public string CodModelo { get; set; }
            public string CodStatus { get; set; }
            public int QtdSaldo { get; set; }
        }

		public static List<String> ObterMesesPrograma(DateTime mesInicial)
		{
			return daoProgramaProducao.ObterMesesPrograma(mesInicial);
		}

		public static List<String> ObterMesesProgramaPlaca(DateTime mesInicial)
		{
			return daoProgramaProducao.ObterMesesProgramaPlacas(mesInicial);
		}

		public static List<LSAAPDTO> ObterAPs(string codModelo, DateTime datReferencia) {

			return daoProgramaProducao.ObterAPs(codModelo, datReferencia);
		
		}
	}
}
