﻿using SEMP.DAL.Models;
using SEMP.Model.DTO;
using SEMP.DAL.DTB_SSA.Models;
using SEMP.Model.VO.PM;
using SEMP.DAL.DAO.PM;
using SEMP.DAL.IDW;

namespace SEMP.DAL
{
    public class IniciarMapper
    {
        public static void Iniciar()
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<BarrasLinhaDTO, tbl_BarrasLinha>();
                cfg.CreateMap<CBCadCausaDTO, tbl_CBCadCausa>();
                cfg.CreateMap<CBCadDefeitoDTO, tbl_CBCadDefeito>();
                cfg.CreateMap<CBCadOrigemDTO, tbl_CBCadOrigem>();
                cfg.CreateMap<CBCadSubDefeitoDTO, tbl_CBCadSubDefeito>();
                cfg.CreateMap<CBDefeitoDTO, tbl_CBDefeito>();
                cfg.CreateMap<CBDefeitoRastreamentoDTO, tbl_CBDefeitoRastreamento>();
                cfg.CreateMap<CBLinhaDefeitoRelatorioDTO, tbl_CBLinhaDefeitoRelatorio>();
                cfg.CreateMap<CBLinhaObservacaoDTO, tbl_CBLinhaObservacao>();
                cfg.CreateMap<CBLoteDTO, tbl_CBLote>();
                cfg.CreateMap<CBOBAInspecaoDTO, tbl_CBOBAInspecao>();
                cfg.CreateMap<CBOBARegistroDTO, tbl_CBOBARegistro>();
                cfg.CreateMap<CBOBATestesDTO, tbl_CBOBATestes>();
                cfg.CreateMap<CBPostoDefeitoDTO, tbl_CBPostoDefeito>();
                cfg.CreateMap<CBQueimaInspecaoDTO, tbl_CBQueimaInspecao>();
                cfg.CreateMap<CBQueimaRegistroDTO, tbl_CBQueimaRegistro>();
                cfg.CreateMap<CBQueimaTestesDTO, tbl_CBQueimaTestes>();
                cfg.CreateMap<CBResumoIACDTO, tbl_CBResumoIAC>();
                cfg.CreateMap<CBTransfLoteDTO, tbl_CBTransfLote>();
                cfg.CreateMap<ProdParadaDTO, tbl_ProdParada>();
                cfg.CreateMap<spc_PM009, ResumoHorasParadas>();
                cfg.CreateMap<spc_PM019, ResumoDetalhesHorasParadas>();
                cfg.CreateMap<tbl_BarrasLinha, BarrasLinhaDTO>();
                cfg.CreateMap<tbl_BarrasProducao, BarrasProducaoDTO>();
                cfg.CreateMap<tbl_CapacLinha, CapacLinhaDTO>();
                cfg.CreateMap<tbl_CBAcao, CBAcaoDTO>();
                cfg.CreateMap<tbl_CBAmarra, CBAmarraDTO>();
                cfg.CreateMap<tbl_CBAmarra_Hist, CBAmarra_HistDTO>();
                cfg.CreateMap<tbl_CBCadCausa, CBCadCausaDTO>();
                cfg.CreateMap<tbl_CBCadCausa, CnqCausaDTO>();
                cfg.CreateMap<tbl_CBCadDefeito, CBCadDefeitoDTO>();
                cfg.CreateMap<tbl_CBCadOrigem, CBCadOrigemDTO>();
                cfg.CreateMap<tbl_CBCadOrigem, CnqOrigemDTO>();
                cfg.CreateMap<tbl_CBCadSubDefeito, CBCadSubDefeitoDTO>();
                cfg.CreateMap<tbl_CBDefeito, CBDefeitoDTO>();
                cfg.CreateMap<tbl_CBEmbalada, CBEmbaladaDTO>();
                cfg.CreateMap<tbl_CBEtiqueta, CBEtiquetaDTO>();
                cfg.CreateMap<tbl_CBHorario, CBHorarioDTO>();
                cfg.CreateMap<tbl_CBHorarioLinha, CBHorarioLinhaDTO>();
                cfg.CreateMap<tbl_CBLinha, CBLinhaDTO>();
                cfg.CreateMap<tbl_CBLinhaDefeitoRelatorio, CBLinhaDefeitoRelatorioDTO>();
                cfg.CreateMap<tbl_CBLote, CBLoteDTO>();
                cfg.CreateMap<tbl_CBLoteConfig, CBLoteConfigDTO>();
                cfg.CreateMap<tbl_CBMaquina, CBMaquinaDTO>();
                cfg.CreateMap<tbl_CBMensagens, CBMensagensDTO>();
                cfg.CreateMap<tbl_CBOBAInspecao, CBOBAInspecaoDTO>();
                cfg.CreateMap<tbl_CBOBATestes, CBOBATestesDTO>();
                cfg.CreateMap<tbl_CBPassagem, CBPassagemDTO>();
                cfg.CreateMap<tbl_CBPerfilAmarraPosto, CBPerfilAmarraPostoDTO>();
                cfg.CreateMap<tbl_CBPesoEmbalagem, CBPesoEmbalagemDTO>();
                cfg.CreateMap<tbl_CBPosto, CBPostoDTO>();
                cfg.CreateMap<tbl_CBPostoDefeito, CBPostoDefeitoDTO>();
                cfg.CreateMap<tbl_CBQueimaInspecao, CBQueimaInspecaoDTO>();
                cfg.CreateMap<tbl_CBQueimaTestes, CBQueimaTestesDTO>();
                cfg.CreateMap<tbl_CBResumoIAC, CBResumoIACDTO>();
                cfg.CreateMap<tbl_CBRetrabalho, CBRetrabalhoDTO>();
                cfg.CreateMap<tbl_CBTipoAmarra, CBTipoAmarraDTO>();
                cfg.CreateMap<tbl_CBTipoPosto, CBTipoPostoDTO>();
                cfg.CreateMap<tbl_CBTransfLote, CBTransfLoteDTO>();
                cfg.CreateMap<tbl_Fabrica, FabricaDTO>();
                cfg.CreateMap<TBL_GpaRecItem, GpaRecItemDTO>();
                cfg.CreateMap<tbl_HistoricoReimpressao, HistoricoReimpressaoDTO>();
                cfg.CreateMap<tbl_Item, ItemDTO>();
                cfg.CreateMap<tbl_Linha, LinhaDTO>();
                cfg.CreateMap<tbl_LSAAP, LSAAPDTO>();
                cfg.CreateMap<tbl_MAPScrapBaixa, MAPScrapBaixaDTO>();
				cfg.CreateMap<tbl_PlanoEtapa, PlanoEtapaDTO>();
				cfg.CreateMap<tbl_PlanoIAC, PlanoIACDTO>();
				cfg.CreateMap<tbl_PlanoIACItem, PlanoIACItemDTO>();
				cfg.CreateMap<tbl_ProdDia, ProdDiaDTO>();
                cfg.CreateMap<tbl_ProdDiaPlaca, ProdDiaPlacaDTO>();
                cfg.CreateMap<tbl_ProdParada, ProdParadaDTO>();
                cfg.CreateMap<tbl_ProdParadaDep, ProdParadaDepDTO>();
                cfg.CreateMap<tbl_ProgDiaJit, ProgDiaJitDTO>();
                cfg.CreateMap<tbl_ProgDiaPlaca, ProgDiaPlacaDTO>();
                cfg.CreateMap<tbl_ProgMesVPE, ProgMesVPEDTO>();
                cfg.CreateMap<tbl_RIFuncionario, RIFuncionarioDTO>();
                cfg.CreateMap<tbl_RPA, RPADTO>();
                cfg.CreateMap<v_paradas, v_paradasDTO>();
                cfg.CreateMap<v_producaoturno, v_producaoturnoDTO>();
                cfg.CreateMap<viw_CBDefeitoTurno, viw_CBDefeitoTurnoDTO>();
                cfg.CreateMap<viw_CBPerfilAmarra, viw_CBPerfilAmarraDTO>();
                cfg.CreateMap<viw_Item, viw_ItemDTO>();
                cfg.CreateMap<viw_STI_ModeloEtiqueta, viw_STI_ModeloEtiquetaDTO>();
            });
        }

    }
}
