using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_REGRAS_FUNCOES_TIPOS_DTO
    {
        public int FTP_COD { get; set; }
        public string FTP_NOME { get; set; }
    }
}
