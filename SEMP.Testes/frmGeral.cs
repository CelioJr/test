﻿using MATERIAIS.BO.Estoque;
using System;
using System.Windows.Forms;

namespace SEMP.Testes
{
	public partial class frmGeral : Form
	{
		public frmGeral()
		{
			InitializeComponent();
		}

		private void btnImportarSaldoPCI_Click(object sender, EventArgs e)
		{

			var dados = SAPConnector.SAPFunctions.ObterEstoquePCI();

			Estoque.GravarEstoquePCI(dados);

			MessageBox.Show("Pronto!");

		}

	}
}
