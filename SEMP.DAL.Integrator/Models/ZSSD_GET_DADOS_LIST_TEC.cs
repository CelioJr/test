namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ZSSD_GET_DADOS_LIST_TEC
    {
        public int ID { get; set; }

        [StringLength(16)]
        public string PACKNO { get; set; }

        [StringLength(10)]
        public string EXTROW { get; set; }

        [StringLength(4)]
        public string FABRICA { get; set; }

        [StringLength(40)]
        public string APARELHO { get; set; }

        [StringLength(40)]
        public string COMPONENTE { get; set; }

        [StringLength(1)]
        public string MARCAALTERNATIVO { get; set; }

        [StringLength(2)]
        public string GRUPOALTERNATIVO { get; set; }

        [StringLength(40)]
        public string MODELO { get; set; }

        [StringLength(10)]
        public string DATAINICIO { get; set; }

        [StringLength(10)]
        public string POSICAO { get; set; }

        [StringLength(13)]
        public string QUANTIDADE { get; set; }

        [StringLength(1)]
        public string FINALIDADE { get; set; }

        [StringLength(10)]
        public string MAST_ANDAT { get; set; }

        [StringLength(12)]
        public string MAST_ANNAM { get; set; }

        [StringLength(10)]
        public string MAST_AEDAT { get; set; }

        [StringLength(10)]
        public string MAST_AENAM { get; set; }

        [StringLength(2)]
        public string STKO_STLST { get; set; }

        [StringLength(10)]
        public string STKO_DATUV { get; set; }

        [StringLength(10)]
        public string STPO_ANDAT { get; set; }

        [StringLength(12)]
        public string STPO_ANNAM { get; set; }

        [StringLength(10)]
        public string STPO_AEDAT { get; set; }

        [StringLength(12)]
        public string STPO_AENAM { get; set; }

        [StringLength(10)]
        public string STPO_DATUV { get; set; }
    }
}
