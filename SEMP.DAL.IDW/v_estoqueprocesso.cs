namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class v_estoqueprocesso
    {
        [StringLength(30)]
        public string cd_produto { get; set; }

        public decimal? qt_total { get; set; }

        public decimal? qt_entrada { get; set; }

        public decimal? qt_saida { get; set; }

        public decimal? qt_ajuste { get; set; }

        [Key]
        public DateTime dthr { get; set; }
    }
}
