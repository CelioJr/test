﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade
{
    public class DefeitosFabrica
    {
        public string Fabrica { get; set; }
        public DateTime Data { get; set; }
        public double IndDefeito { get { return QtdDefeito / QtdProducao; } }
		public double IndMeta { get; set; }
		public int PPM { 
			get { 
				var ppm = (QtdDefeito / (QtdProducao * (double)QtdPontos)) * 1000000;

				return (int)ppm;
			}
		}
		public int PPMMeta { get; set; }
		public int QtdDefeito { get; set; }
		public int QtdProducao { get; set; }
		public int QtdPontos { get; set; }
		public int Ordem { get { 
			if (Fabrica == "IAC"){
					return 1;
			}
			else if (Fabrica == "IMC"){
					return 2;
			}
			else
			{
					return 3;
			}
		} }

	}
}
