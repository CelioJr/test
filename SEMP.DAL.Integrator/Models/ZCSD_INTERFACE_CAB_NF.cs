namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ZCSD_INTERFACE_CAB_NF
    {
        public int id { get; set; }

        [Required]
        [StringLength(16)]
        public string PACKNO { get; set; }

        [Required]
        [StringLength(10)]
        public string EXTROW { get; set; }

        [StringLength(22)]
        public string DOCUMENTOSAP { get; set; }

        [StringLength(13)]
        public string NUMERONF { get; set; }

        [StringLength(6)]
        public string SERIENF { get; set; }

        [StringLength(10)]
        public string DATAEMISSAO { get; set; }

        [StringLength(10)]
        public string DATALANCAMENTO { get; set; }

        [StringLength(6)]
        public string EMPRESA { get; set; }

        [StringLength(6)]
        public string LOCALNEGOCIO { get; set; }

        [StringLength(4)]
        public string TIPONF { get; set; }

        [StringLength(4)]
        public string ENTRADASAIDA { get; set; }

        [StringLength(16)]
        public string CLIENTE { get; set; }

        [StringLength(4)]
        public string CANCELADA { get; set; }

        [StringLength(10)]
        public string DATACANCELAMENTO { get; set; }

        [StringLength(8)]
        public string INCOTERMS { get; set; }
    }
}
