namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_passtfse
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_passtfse()
        {
            dw_passtfsepm_hist = new HashSet<dw_passtfsepm_hist>();
            dw_passtfsepm = new HashSet<dw_passtfsepm>();
        }

        [Key]
        public long id_passtfse { get; set; }

        public long id_passtf { get; set; }

        public short? st_subetapa { get; set; }

        public DateTime? dthr_isubetapa { get; set; }

        public int? ms_dthrisubetapa { get; set; }

        public DateTime? dthr_fsubetapa { get; set; }

        public int? ms_dthrfsubetapa { get; set; }

        public DateTime? dthr_iposfalha { get; set; }

        public int? ms_dthriposfalha { get; set; }

        public DateTime? dthr_fposfalha { get; set; }

        public int? ms_dthrfposfalha { get; set; }

        public long id_ft_sub { get; set; }

        public int? ordemSubetapa { get; set; }

        public virtual dw_ft_sub dw_ft_sub { get; set; }

        public virtual dw_passtf dw_passtf { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_passtfsepm_hist> dw_passtfsepm_hist { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_passtfsepm> dw_passtfsepm { get; set; }
    }
}
