namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_versaodb
    {
        [Key]
        public long id_versaodb { get; set; }

        public long? id_scriptdb { get; set; }

        public DateTime? dthr_atualizacaodb { get; set; }
    }
}
