﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Antlr.Runtime;
using RAST.BO.Rastreabilidade;
using SEMP.Model.DTO;

namespace SEMP.Areas.Outros.Controllers
{
	public class TipoCausaController : Controller
	{
		#region Listagem
		//
		// GET: /Outros/TipoCausa/
		public ActionResult Index()
		{
			return View();
		}

		//
		// GET: /Outros/TipoCausa/Details/5
		public ActionResult Lista()
		{
			var dados = PlanoAcao.ObterTipoCausaLista();

			return View(dados);
		}

		public ActionResult ObterLista()
		{
			return Json(PlanoAcao.ObterTipoCausaLista().Select(x => new {Id = x.CodTipoCausa, Value = x.DesTipoCausa}), 
						JsonRequestBehavior.AllowGet);
		}
		#endregion

		#region Create
		//
		// GET: /Outros/TipoCausa/Create
		public ActionResult Create()
		{
			return View();
		}

		//
		// POST: /Outros/TipoCausa/Create
		[HttpPost]
		public ActionResult Create(CBPlanoAcaoTipoCausaDTO dados)
		{
			try
			{
				PlanoAcao.GravarTipoCausa(dados);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json("Erro ao gravar. " + ex.Message, JsonRequestBehavior.AllowGet);
			}
		}
		#endregion

		#region Edit
		//
		// GET: /Outros/TipoCausa/Edit/5
		public ActionResult Edit(int id)
		{
			var dado = PlanoAcao.ObterTipoCausa(id);

			return View(dado);
		}

		//
		// POST: /Outros/TipoCausa/Edit/5
		[HttpPost]
		public ActionResult Edit(CBPlanoAcaoTipoCausaDTO dado)
		{
			try
			{
				PlanoAcao.GravarTipoCausa(dado);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}
		#endregion

		#region Delete
		//
		// GET: /Outros/TipoCausa/Delete/5
		public ActionResult Delete(int id)
		{
			var dado = PlanoAcao.ObterTipoCausa(id);

			return View(dado);
		}

		//
		// POST: /Outros/TipoCausa/Delete/5
		[HttpPost]
		public ActionResult Delete(CBPlanoAcaoTipoCausaDTO dado)
		{
			try
			{
				PlanoAcao.RemoverTipoCausa(dado.CodTipoCausa);

				return RedirectToAction("Index");
			}
			catch
			{
				return RedirectToAction("Index");
			}
		}
		#endregion
	}
}
