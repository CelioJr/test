﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade
{
	public class TempoAtravessamentoModelo
	{

		public string CodModelo { get; set; }
		public string NumSerieModelo { get; set; }
		public string NumSeriePCI { get; set; }
		public DateTime? DatEntradaIAC { get; set; }
		public DateTime? DatSaidaIAC { get; set; }
		public int? TempoIAC { get; set; }
		public int? TempoEsperaIMC { get; set; }
		public DateTime? DatEntradaIMC { get; set; }
		public DateTime? DatSaidaIMC { get; set; }
		public int? TempoIMC { get; set; }
		public int? TempoEsperaMF { get; set; }
		public DateTime? DatEntradaMF { get; set; }
		public DateTime? DatSaidaMF { get; set; }
		public int? TempoMF { get; set; }
		public int? TempoCorrido { get; set; }
		public int? TempoEspera { get; set; }
		public int? TempoAtravessamento { get; set; }
		public int? TempoTecnicoIAC { get; set; }
		public int? TempoTecnicoIMC { get; set; }
		public int? TempoTecnicoFEC { get; set; }
		public int? TempoTecnico { get; set; }
		public int? QtdDefeitosIAC { get; set; }
		public int? QtdDefeitosIMC { get; set; }
		public int? QtdDefeitosFEC { get; set; }
		public int? QtdDefeitos { get; set; }

	}
}
