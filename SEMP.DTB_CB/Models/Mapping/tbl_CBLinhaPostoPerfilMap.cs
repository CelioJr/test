using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DTB_CB.Models.Mapping
{
    public class tbl_CBLinhaPostoPerfilMap : EntityTypeConfiguration<tbl_CBLinhaPostoPerfil>
    {
        public tbl_CBLinhaPostoPerfilMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodModelo, t.CodLinha, t.NumPosto });

            // Properties
            this.Property(t => t.CodModelo)
                .IsRequired()
                .HasMaxLength(30);

            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumPosto)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FlgAtivo)
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("tbl_CBLinhaPostoPerfil");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.NumPosto).HasColumnName("NumPosto");
            this.Property(t => t.FlgAtivo).HasColumnName("FlgAtivo");
            this.Property(t => t.NumSeq).HasColumnName("NumSeq");
            this.Property(t => t.flgObrigatorio).HasColumnName("flgObrigatorio");

            // Relationships
            this.HasRequired(t => t.tbl_CBLinha)
                .WithMany(t => t.tbl_CBLinhaPostoPerfil)
                .HasForeignKey(d => d.CodLinha);

        }
    }
}
