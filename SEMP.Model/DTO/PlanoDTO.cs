using System;

namespace SEMP.Model.DTO
{
    public class PlanoDTO
    {
        public string CodPlano { get; set; }
        public string NomRespons { get; set; }
        public string CodStatus { get; set; }
        public string DesAprovacao { get; set; }
        public Nullable<System.DateTime> DatElab { get; set; }
        public Nullable<System.DateTime> DatInativo { get; set; }
        public string Usuario { get; set; }
        public Nullable<System.DateTime> DatDigitacao { get; set; }
        public Nullable<System.DateTime> DatEDP { get; set; }
        public Nullable<System.DateTime> DatPCPM { get; set; }
        public Nullable<System.DateTime> DatGEI { get; set; }
        public Nullable<System.DateTime> DatGMM { get; set; }
        public Nullable<System.DateTime> DatGEF { get; set; }
        public Nullable<System.DateTime> DatPreEDP { get; set; }
        public Nullable<System.DateTime> DatPreGEI { get; set; }
        public string CodFase { get; set; }
        public string CodFab { get; set; }
        public string FlgBreakdown { get; set; }
        public Nullable<System.DateTime> DatBreakdown { get; set; }
        public string NomUsuario { get; set; }
        public System.Guid rowguid { get; set; }
        public string Supplier { get; set; }
        public string CodODM { get; set; }
    }
}
