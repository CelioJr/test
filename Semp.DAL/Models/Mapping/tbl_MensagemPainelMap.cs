using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_MensagemPainelMap : EntityTypeConfiguration<tbl_MensagemPainel>
    {
        public tbl_MensagemPainelMap()
        {
            // Primary Key
            this.HasKey(t => t.CodMensagem);

            // Properties
            this.Property(t => t.Destino)
                .HasMaxLength(8);

            this.Property(t => t.Mensagem)
                .HasMaxLength(500);

            this.Property(t => t.flgAtivo)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.nomusuario)
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("tbl_MensagemPainel");
            this.Property(t => t.CodMensagem).HasColumnName("CodMensagem");
            this.Property(t => t.Destino).HasColumnName("Destino");
            this.Property(t => t.Mensagem).HasColumnName("Mensagem");
            this.Property(t => t.Data).HasColumnName("Data");
            this.Property(t => t.flgAtivo).HasColumnName("flgAtivo");
            this.Property(t => t.nomusuario).HasColumnName("nomusuario");
        }
    }
}
