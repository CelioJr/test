﻿using System;

namespace SEMP.SAPConnector
{
	public static class DateTimeExtensions
	{
		public static string TrataDataSAP(this DateTime Data)
		{
			var retorno = "";
			retorno = Data.ToString("yyyyMMdd");//.ToShortDateString().Replace("/", "");
			return retorno;

		}

	}
}
