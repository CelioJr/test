using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_ProdParadaObs
    {
        public string CodFab { get; set; }
        public System.DateTime DatParada { get; set; }
        public string FlgTipo { get; set; }
        public string DesObs { get; set; }
        public System.Guid rowguid { get; set; }
    }
}
