﻿using System;
using System.Net.Mail;

namespace SEMP.WindowsService
{
	public class Email
	{
		public static void EnviaEmail(string titulo, string mensagem)
		{
			var mail = new MailMessage("integrador@semptcl.com.br", "thiago.raheem@semptcl.com.br");
			var client = new SmtpClient
			{
				Port = 25,
				DeliveryMethod = SmtpDeliveryMethod.Network,
				UseDefaultCredentials = false,
				Host = "10.2.1.32"
			};

			mail.Subject = titulo;
			mail.Body = mensagem;
			mail.CC.Add("joao.mendes@semptcl.com.br");
			client.Send(mail);
		}

		public static void EnviaEmailPara(string titulo, string mensagem, string para)
		{
			var mail = new MailMessage();

			var client = new SmtpClient
			{
				Port = 25,
				DeliveryMethod = SmtpDeliveryMethod.Network,
				UseDefaultCredentials = false,
				Host = "10.2.1.32"
			};

			mail.From = new MailAddress("integrador@semptcl.com.br");

			if (String.IsNullOrEmpty(para))
			{
				mail.To.Add("thiago.raheem@semptcl.com.br");
			}
			else
			{
				if (para.Contains(";"))
				{
					var dests = para.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
					foreach (var dest in dests)
					{
						mail.To.Add(dest.Trim());
					}
				}
			}

			mail.Subject = titulo;
			mail.Body = mensagem;
			mail.CC.Add("thiago.raheem@semptcl.com.br");
			mail.CC.Add("joao.mendes@semptcl.com.br");
			client.Send(mail);
		}

	}
}
