using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
	public partial class CBPlanoAcaoCausaDTO
	{
		[Display(Name = "Plano de A��o")]
		public int CodPlano { get; set; }
		[Display(Name = "C�digo")]
		public int CodPlanoCausa { get; set; }
		[Display(Name = "Descri��o")]
		public string DesCausa { get; set; }
		[Display(Name = "Tipo")]
		public Nullable<int> CodTipoCausa { get; set; }
		[Display(Name = "Status")]
		public Nullable<int> FlgStatus { get; set; }
		[Display(Name = "In�cio")]
		public Nullable<System.DateTime> DatInicio { get; set; }
		[Display(Name = "Fim")]
		public Nullable<System.DateTime> DatFim { get; set; }
		[Display(Name = "Respons�vel")]
		public string NomResponsavel { get; set; }

		[Display(Name = "Descri��o")]
		public string DesTipoCausa { get; set; }
	}
}
