﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
	public class ArvoreModelo
	{
		public int Nivel { get; set; }
		[Display(Name = "AP")]
		public string CodFab { get; set ; }
		public string NumAP { get; set; }
		[Display(Name = "Modelo")]
		public string CodModelo { get; set; }
		public string DesModelo { get; set; }
		[Display(Name = "Status")]
		public string CodStatus { get; set; }
		[Display(Name = "Lote/Prog.")]
		public decimal? QtdLoteAP { get; set; }
		[Display(Name = "Embalado")]
		public decimal? QtdEmbalado { get; set; }
		[Display(Name = "Saldo AP")]
		public decimal? QtdSaldo { get { return QtdEmbalado - QtdLoteAP ?? 0; } }
		[Display(Name = "Apontado")]
		public decimal? QtdProduzida { get; set; }
		[Display(Name = "Diferença")]
		public decimal? QtdDiferenca { get { return QtdProduzida - QtdLoteAP ?? 0; } }
		[Display(Name = "Observação")]
		public string Observacao { get; set; }
		public string CodTipo { get; set; }
		public string TipAP { get; set; }

	}
}
