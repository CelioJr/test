using System;
using System.Collections.Generic;

namespace SEMP.DTB_CB.Models
{
    /// <summary>
    /// Classe responsável por representar a tabela tbl_CBPassagem_Hist do banco de dados.
    /// </summary>
    public partial class tbl_CBPassagem_Hist
    {
        public string NumECB { get; set; }
        public int CodLinha { get; set; }
        public int NumPosto { get; set; }
        public System.DateTime DatEvento { get; set; }
        public string CodDRT { get; set; }
    }
}
