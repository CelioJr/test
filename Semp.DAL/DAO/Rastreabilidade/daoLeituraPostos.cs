﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.Models;
using SEMP.Model.VO.Rastreabilidade.Relatorios;

namespace SEMP.DAL.DAO.Rastreabilidade
{
	public class daoLeituraPostos
	{
		public static List<LeiturasPostos> ObterLeiturasPostos(DateTime datInicio, DateTime datFim, int codLinha, int turno)
		{

			using (var dtbCb = new DTB_CBContext())
			{

				var resumo = dtbCb.Database.SqlQuery<LeiturasPostos>(
					"EXEC spc_CBAcompanhaPostos @DatInicio, @DatFim, @CodLinha, @Turno",
					new SqlParameter("DatInicio", datInicio),
					new SqlParameter("DatFim", datFim),
					new SqlParameter("CodLinha", codLinha),
					new SqlParameter("Turno", turno)
					).ToList();

				return resumo;
			}
		}

		public static List<LeiturasPostos> ObterLeiturasPostosAnalitico(DateTime datInicio, DateTime datFim, int codLinha, int turno)
		{

			using (var dtbCB = new DTB_CBContext())
			{

				var resumo = dtbCB.Database.SqlQuery<LeiturasPostos>(
					"EXEC spc_CBAcompanhaPostosAnalitico @DatInicio, @DatFim, @CodLinha, @Turno",
					new SqlParameter("DatInicio", datInicio),
					new SqlParameter("DatFim", datFim),
					new SqlParameter("CodLinha", codLinha),
					new SqlParameter("Turno", turno)
					).ToList();

				var codmodelo = ""; var desModelo = "";

				foreach (var leitura in resumo)
				{
					if (codmodelo != leitura.CodModelo)
					{
						desModelo = daoGeral.ObterDescricaoItem(leitura.CodModelo);
						codmodelo = leitura.CodModelo;
					}

					leitura.DesModelo = desModelo;

				}

				return resumo;
			}
		}

		public static List<ProducaoDiariaDAT> ObterProducaoDAT(DateTime datInicio, DateTime datFim, int codLinha)
		{

			using (var dtbSim = new DTB_SIMContext())
			{

				var resumo = dtbSim.Database.SqlQuery<ProducaoDiariaDAT>(
					"EXEC dtb_CB..spc_CBRelatorio_Prod_Diario_DAT @CodLinha, @DataInicial, @DataFinal",
					new SqlParameter("CodLinha", codLinha),
					new SqlParameter("DataInicial", datInicio),
					new SqlParameter("DataFinal", datFim)
					
					).ToList();

				return resumo;
			}
		}
	}
}
