﻿using System.Web.Mvc;
using RAST.BO.Rastreabilidade;
using SEMP.Model.DTO;

namespace SEMP.Areas.Coleta.Controllers
{
	public class OrigemController : Controller
	{
		//
		// GET: /Coleta/Origem/
		public ActionResult Index()
		{
			var origens = Defeito.ObterOrigens();

			return View(origens);
		}

		//
		// GET: /Coleta/Origem/Create
		public ActionResult Create()
		{
			return View();
		}

		//
		// POST: /Coleta/Origem/Create
		[HttpPost]
		public ActionResult Create(CBCadOrigemDTO origem)
		{
			try
			{
				var codigo = Defeito.ObterUltimaOrigem();

                var flgAtivo = Request.Form["FlgAtivo"];
                origem.FlgAtivo = flgAtivo == null ? false : true;

				codigo++;

				origem.CodOrigem = codigo.ToString();

				Defeito.GravarOrigem(origem);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}

		//
		// GET: /Coleta/Origem/Edit/5
		public ActionResult Edit(string codOrigem)
		{
			var origem = Defeito.ObterOrigem(codOrigem);

			return View(origem);
		}

		//
		// POST: /Coleta/Origem/Edit/5
		[HttpPost]
		public ActionResult Edit(CBCadOrigemDTO origem)
		{
			try
			{
                var flgAtivo = Request.Form["FlgAtivo"];
                origem.FlgAtivo = flgAtivo == null ? false : true;

				Defeito.GravarOrigem(origem);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}

		//
		// GET: /Coleta/Origem/Delete/5
		public ActionResult Delete(string codOrigem)
		{
			var origem = Defeito.ObterOrigem(codOrigem);

			return View(origem);
		}

		//
		// POST: /Coleta/Origem/Delete/5
		[HttpPost]
		public ActionResult Delete(CBCadOrigemDTO origem)
		{
			try
			{
				Defeito.RemoverOrigem(origem);

				return RedirectToAction("Index");
			}
			catch
			{
				return RedirectToAction("Index");
			}
		}
	}
}
