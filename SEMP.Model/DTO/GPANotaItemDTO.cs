using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GPANotaItemDTO
    {
        public string CodSerie { get; set; }
        public string NumNota { get; set; }
        public string CodModelo { get; set; }
        public string NumSerie { get; set; }
        public Nullable<System.DateTime> DatRecebimento { get; set; }
        public Nullable<System.DateTime> DatMovimentacao { get; set; }
        public Nullable<int> CodPallet { get; set; }
        public string NomUsuario { get; set; }
    }
}
