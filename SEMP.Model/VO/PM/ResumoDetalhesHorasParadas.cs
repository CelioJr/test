﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.PM
{
    public class ResumoDetalhesHorasParadas
    {
        public DateTime MesBase { get; set; }
        public decimal? QtdParada { get; set; }
        public decimal? HorParada { get; set; }
        public decimal? QtdTotal { get; set; }
        public decimal? HorTotal { get; set; }
        public string MesAno { get; set; }
        					
    }
}
