using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_MAPScrapBaixa
    {
        public int IDScrap { get; set; }
        public System.DateTime DatReferencia { get; set; }
        public string CodModelo { get; set; }
        public string CodItem { get; set; }
        public decimal QtdPerda { get; set; }
        public Nullable<decimal> QtdBaixa { get; set; }
        public string CodFabBxa { get; set; }
        public Nullable<decimal> QtdEstoque { get; set; }
        public Nullable<System.DateTime> DatBaixa { get; set; }
        public Nullable<System.DateTime> DatGeracao { get; set; }
        public string NomUsuGerou { get; set; }
    }
}
