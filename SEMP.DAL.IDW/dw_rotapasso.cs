namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_rotapasso
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_rotapasso()
        {
            dw_rotapasso1 = new HashSet<dw_rotapasso>();
            dw_rotapasso11 = new HashSet<dw_rotapasso>();
            dw_rotapasso_pt = new HashSet<dw_rotapasso_pt>();
            dw_rp_predecessora = new HashSet<dw_rp_predecessora>();
            dw_rp_predecessora1 = new HashSet<dw_rp_predecessora>();
            om_obj = new HashSet<om_obj>();
        }

        [Key]
        public long id_rotapasso { get; set; }

        public long? id_rotapassosucessoNC { get; set; }

        public long? id_rotapassosucessoraC { get; set; }

        public long id_rota { get; set; }

        public long? id_folha { get; set; }

        public short? is_liberacao { get; set; }

        [StringLength(40)]
        public string obs { get; set; }

        public long? id_pt { get; set; }

        public long? id_est { get; set; }

        public long? id_estConsumir { get; set; }

        public decimal? qt_movimentacao { get; set; }

        public short? tp_movimentacao { get; set; }

        public int? passo { get; set; }

        public virtual dw_est dw_est { get; set; }

        public virtual dw_est dw_est1 { get; set; }

        public virtual dw_folha dw_folha { get; set; }

        public virtual dw_rota dw_rota { get; set; }

        public virtual om_pt om_pt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rotapasso> dw_rotapasso1 { get; set; }

        public virtual dw_rotapasso dw_rotapasso2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rotapasso> dw_rotapasso11 { get; set; }

        public virtual dw_rotapasso dw_rotapasso3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rotapasso_pt> dw_rotapasso_pt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rp_predecessora> dw_rp_predecessora { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_rp_predecessora> dw_rp_predecessora1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_obj> om_obj { get; set; }
    }
}
