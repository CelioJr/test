﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
    public class InspecaoIACRelatorio
    {
        public string Modelo { get; set; }
        public string PosicaoMecanica { get; set; }

        public string CodigoDefeito { get; set; }

        public string Data { get; set; }

        public int Hora { get; set; }

        public int Quantidade { get; set; }    
         
        public InspecaoIACRelatorio()
        {            
            
        }

    }

    public class InspecaoIACRelatorioTela
    {
        public string Modelo { get; set; }
        public string PosicaoMecanica { get; set; }

        public string CodigoDefeito { get; set; }

        public string Data { get; set; }        
 
        public List<Acumuladores> dadosAcumuladores {get; set;}
 
        public InspecaoIACRelatorioTela()
        {
            dadosAcumuladores = new List<Acumuladores>(23);
        }

    }

    public class Acumuladores
    {
        public int Hora { get; set; }

        public int QtInspecionada { get; set; }

        public int QtInspecionadaAcomulada { get; set; }

        public int QtDefeito { get; set; }

        public int QtDefeitoAcomulado { get; set; }

        public int PpmAcomulado { get; set; }

        public Acumuladores()
        {
            
        }
    }
}
