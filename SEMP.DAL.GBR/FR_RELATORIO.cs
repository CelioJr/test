namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_RELATORIO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FR_RELATORIO()
        {
            FR_DOC_RELATORIO = new HashSet<FR_DOC_RELATORIO>();
            FR_FORMULARIO = new HashSet<FR_FORMULARIO>();
            FR_PERMISSAO = new HashSet<FR_PERMISSAO>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int REL_CODIGO { get; set; }

        [Required]
        [StringLength(3)]
        public string SIS_CODIGO { get; set; }

        [Required]
        [StringLength(196)]
        public string REL_NOME { get; set; }

        public string REL_CONTEUDO { get; set; }

        public DateTime? REL_MODIFICADO { get; set; }

        public int? REL_TAMANHO { get; set; }

        public int USR_CODIGO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_RELATORIO> FR_DOC_RELATORIO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_FORMULARIO> FR_FORMULARIO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_PERMISSAO> FR_PERMISSAO { get; set; }
    }
}
