using System;

namespace SEMP.Model.DTO
{
    public partial class PlanoIACDTO
    {
        public string CodModelo { get; set; }
        public string NumRevisao { get; set; }
        public string CodProcesso { get; set; }
        public string CodStatus { get; set; }
        public Nullable<System.DateTime> DatStatus { get; set; }
        public Nullable<System.DateTime> DatAtivo { get; set; }
        public Nullable<System.DateTime> DatInativo { get; set; }
        public Nullable<System.DateTime> DatUltAlt { get; set; }
        public string NomAprovacao { get; set; }
        public string NomEmissor { get; set; }
        public string NomDocLDC { get; set; }
        public string Amplitude { get; set; }
        public System.Guid rowguid { get; set; }
        public short FlgLeftRight { get; set; }

		public string DesModelo { get; set; }
	}
}
