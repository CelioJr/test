namespace SEMP.DAL.Models
{
    public partial class tbl_PlanoEtapa
    {
        public string CodModelo { get; set; }
        public string NumRevisao { get; set; }
        public string NumEtapa { get; set; }
        public string CodProcesso { get; set; }
        public string CodOrdAlim { get; set; }
        public string NumPriPosicao { get; set; }
        public string NumPadrao { get; set; }
        public System.Guid rowguid { get; set; }
        public string Comentario { get; set; }
        public string NomPrograma { get; set; }
        public short FlgLeftRight { get; set; }
        public string DesSkip { get; set; }
    }
}
