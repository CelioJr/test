namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_licmodrec
    {
        [Key]
        public long id_licmodrec { get; set; }

        public long id_licenca { get; set; }

        public long id_modulo_recurso { get; set; }

        [StringLength(512)]
        public string chave_verificacao_licenca { get; set; }

        [StringLength(512)]
        public string chave_verificacao { get; set; }

        public virtual om_licenca om_licenca { get; set; }

        public virtual om_modulo_recurso om_modulo_recurso { get; set; }
    }
}
