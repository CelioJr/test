namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_CHK_SMT_SERIAL
    {
        [Key]
        public int COD_SMT { get; set; }

        [StringLength(15)]
        public string OP { get; set; }

        [StringLength(30)]
        public string SKU { get; set; }

        [StringLength(30)]
        public string MODELO { get; set; }

        [StringLength(30)]
        public string SERIAL { get; set; }

        [StringLength(20)]
        public string PI_MAIN { get; set; }

        [StringLength(20)]
        public string PI_SUB1 { get; set; }

        [StringLength(20)]
        public string PI_SUB2 { get; set; }

        public bool? VERIFICADO { get; set; }

        [StringLength(30)]
        public string USUARIO { get; set; }

        [StringLength(20)]
        public string SESSAO { get; set; }

        [StringLength(20)]
        public string IMEI1 { get; set; }

        public DateTime? DATA { get; set; }
    }
}
