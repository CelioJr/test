using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_DCREstoque
    {
        public string CodFab { get; set; }
        public string CodItem { get; set; }
        public string NumDCR { get; set; }
        public string flgTipo { get; set; }
        public Nullable<decimal> QtdEstoque { get; set; }
        public Nullable<decimal> QtdPrevisao { get; set; }
        public Nullable<System.DateTime> DatEstoque { get; set; }
    }
}
