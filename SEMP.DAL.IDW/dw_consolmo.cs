namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolmo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_consolmo()
        {
            dw_consolalmo = new HashSet<dw_consolalmo>();
            dw_consolpamo = new HashSet<dw_consolpamo>();
            dw_consolmooco = new HashSet<dw_consolmooco>();
            dw_consolprmo = new HashSet<dw_consolprmo>();
            dw_consolremo = new HashSet<dw_consolremo>();
        }

        [Key]
        public long id_consolmo { get; set; }

        public long id_consol { get; set; }

        public decimal? seg_auto_tempologin { get; set; }

        public decimal? seg_manu_tempologin { get; set; }

        public long? pcs_auto_producaobruta { get; set; }

        public long? pcs_auto_producaorefugada { get; set; }

        public long? pcs_manu_producaobruta { get; set; }

        public long? pcs_manu_producaorefugada { get; set; }

        public decimal? seg_auto_parada { get; set; }

        public decimal? seg_auto_sp { get; set; }

        public long? pcs_auto_perda { get; set; }

        public decimal? seg_auto_tempotrabalhado { get; set; }

        public decimal? seg_auto_cicloprodutivo { get; set; }

        public decimal? seg_auto_cicloimprodutivo { get; set; }

        public long? pcs_auto_producaoprevista { get; set; }

        public long? pcs_manu_producaoprevista { get; set; }

        public decimal? seg_manu_cicloprodutivo { get; set; }

        public decimal? seg_manu_cicloimprodutivo { get; set; }

        public long? pcs_auto_qtinjnormal { get; set; }

        public long? pcs_manu_qtinjnormal { get; set; }

        public long id_usr { get; set; }

        public decimal? QT_AUTO_CICLOIMPRODUTIVO { get; set; }

        public decimal? QT_MANU_CICLOIMPRODUTIVO { get; set; }

        public decimal? QT_AUTO_CICLOPRODUTIVO { get; set; }

        public decimal? QT_MANU_CICLOPRODUTIVO { get; set; }

        public decimal? QT_AUTO_CICLOREGULAGEM { get; set; }

        public decimal? QT_MANU_CICLOREGULAGEM { get; set; }

        public decimal? SEG_AUTO_CICLOREGULAGEM { get; set; }

        public decimal? SEG_MANU_CICLOREGULAGEM { get; set; }

        public decimal? SEG_AUTO_TEMPOPARADA_CP { get; set; }

        public decimal? SEG_MANU_TEMPOPARADA_CP { get; set; }

        public decimal? SEG_AUTO_TEMPOPARADA_SP { get; set; }

        public decimal? SEG_MANU_TEMPOPARADA_SP { get; set; }

        public decimal? seg_auto_tempoparada_cp_vr { get; set; }

        public decimal? seg_auto_tempoparada_sp_vr { get; set; }

        public decimal? seg_manu_tempoparada_cp_vr { get; set; }

        public decimal? seg_manu_tempoparada_sp_vr { get; set; }

        public decimal? qt_auto_ocoparada_cp_vr { get; set; }

        public decimal? qt_auto_ocoparada_sp_vr { get; set; }

        public decimal? qt_manu_ocoparada_cp_vr { get; set; }

        public decimal? qt_manu_ocoparada_sp_vr { get; set; }

        public decimal? seg_auto_tempoparada_default { get; set; }

        public decimal? seg_auto_tempoparada_sem_op { get; set; }

        public decimal? seg_auto_tempoparada_sem_evt { get; set; }

        public decimal? seg_auto_tempoparada_sem_cnx { get; set; }

        public decimal? seg_manu_tempoparada_default { get; set; }

        public decimal? seg_manu_tempoparada_sem_op { get; set; }

        public decimal? seg_manu_tempoparada_sem_evt { get; set; }

        public decimal? seg_manu_tempoparada_sem_cnx { get; set; }

        public decimal? qt_auto_tempoparada_default { get; set; }

        public decimal? qt_auto_tempoparada_sem_op { get; set; }

        public decimal? qt_auto_tempoparada_sem_evt { get; set; }

        public decimal? qt_auto_tempoparada_sem_cnx { get; set; }

        public decimal? qt_manu_tempoparada_default { get; set; }

        public decimal? qt_manu_tempoparada_sem_op { get; set; }

        public decimal? qt_manu_tempoparada_sem_evt { get; set; }

        public decimal? qt_manu_tempoparada_sem_cnx { get; set; }

        public decimal? seg_auto_cicloprodutivo_cta { get; set; }

        public decimal? seg_auto_cicloimprodutivo_cta { get; set; }

        public decimal? seg_manu_cicloprodutivo_cta { get; set; }

        public decimal? seg_manu_cicloimprodutivo_cta { get; set; }

        public decimal? seg_auto_cta { get; set; }

        public decimal? seg_manu_cta { get; set; }

        public virtual dw_consol dw_consol { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolalmo> dw_consolalmo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolpamo> dw_consolpamo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolmooco> dw_consolmooco { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolprmo> dw_consolprmo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolremo> dw_consolremo { get; set; }

        public virtual om_usr om_usr { get; set; }
    }
}
