﻿using System;

namespace SEMP.Model.VO.SAP
{
	public class ZSPP_LOG_APONT
	{
		public string LOTE { get; set; }
		public string TYPE { get; set; }
		public string UNAME { get; set; }
		public DateTime DATUM { get; set; }
		public string UZEIT { get; set; }
		public string DESLOG { get; set; }

	}
}
