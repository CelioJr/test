﻿namespace SEMP.Model.VO.Rastreabilidade
{
	public class PainelResumoProd
	{
		public int CodLinha { get; set; }
		public int NumPosto { get; set; }
		public int QtdPrograma { get; set; }
		public int QtdProduzido { get; set; }
		public int QtdDifHora { get; set; }

		public PainelResumoProd(){
			QtdPrograma = 0;
			QtdProduzido = 0;
			QtdDifHora = 0;
		}

		public PainelResumoProd(int codLinha, int numPosto)
		{
			CodLinha = codLinha;
			NumPosto = numPosto;
			QtdPrograma = 0;
			QtdProduzido = 0;
			QtdDifHora = 0;
		}
	}
}
