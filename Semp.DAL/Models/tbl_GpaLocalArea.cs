using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_GpaLocalArea
    {
        public string CodLocal { get; set; }
        public string CodArea { get; set; }
    }
}
