﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade
{
	public class ItensLote
	{

		public string NumLotePai { get; set; }
		public string NumCC { get; set; }
		public string NumECB { get; set; }
		public string CodModelo { get; set; }
		public DateTime DatLeitura { get; set; }
		public DateTime? DatReferencia { get; set; }
		public string CodFab { get; set; }
		public string NumAP { get; set; }
		public string CodOperador { get; set; }
		public int CodLinha { get; set; }
		public int? NumPosto { get; set; }
		public DateTime? DatAmarraFinal { get; set; }

		public string DesModelo { get; set; }
		public string DesLinha { get; set; }
		public string DesPosto { get; set; }
	}
}
