using System;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
    public class CBHorarioIntervaloDTO
    {
		[Display(Name = "C�digo da Linha")]
        public int CodLinha { get; set; }
        public int Turno { get; set; }
		[Display(Name = "Tipo de Intervalo")]
		public string TipoIntervalo { get; set; }
		[Display(Name = "In�cio")]
		public string HoraInicio { get; set; }
		[Display(Name = "Fim")]
		public string HoraFim { get; set; }
		[Display(Name = "Tempo")]
		public Nullable<int> TempoIntervalo { get; set; }
    }
}
