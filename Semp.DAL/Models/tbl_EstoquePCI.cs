using System;

namespace SEMP.DAL.Models
{
    public partial class tbl_EstoquePCI
    {
        public string CodFab { get; set; }
        public string CodItem { get; set; }
        public string CodLocal { get; set; }
        public decimal? QtdEstoque { get; set; }
        public DateTime? DatAtualizacao { get; set; }
    }
}
