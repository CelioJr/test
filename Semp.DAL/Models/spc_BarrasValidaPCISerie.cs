﻿using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public class spc_BarrasValidaPCISerie
    {
        public string DesStatus {get; set;}
        public string Mensagem { get; set; }
        public string CodModelo { get; set; }
        public string NumSerieModelo { get; set; }
        public DateTime? DatLeitura { get; set; }
        public string CodLinha { get; set; }
        public string CodControle { get; set; }
        public string NumSerieControle { get; set; }
        public string CodCinescopio { get; set; }
        public string NumSerieCinescopio { get; set; }
    }
}
