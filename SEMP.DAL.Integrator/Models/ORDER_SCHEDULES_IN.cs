namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ORDER_SCHEDULES_IN
    {
        public Guid ID { get; set; }

        public Guid ORDER_ITEMS_IN_ID { get; set; }

        public int? ITM_NUMBER { get; set; }

        public decimal? REQ_QTY { get; set; }

        [StringLength(10)]
        public string REQ_DATE { get; set; }

        public virtual ORDER_ITEMS_IN ORDER_ITEMS_IN { get; set; }
    }
}
