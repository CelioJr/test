using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class TEST_TABLEs_DTO
    {
        public int id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public System.DateTime createdAt { get; set; }
        public System.DateTime updatedAt { get; set; }
    }
}
