﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RAST.BO.Rastreabilidade;
using SEMP.Model;
using SEMP.DAL.DAO.Embalagem;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.DTO;
using SEMP.Model.VO;
using System.Globalization;
using System.Diagnostics;

namespace RAST.BO.Embalagem
{
    public class EmbalagemTablet: Embalagem, IEmbalagem
    {
        /// <summary>
        /// Método que faz a embalagem do produto Tablet e chama a atualização da base DTB_SSA
        /// </summary>
        /// <param name="embalagem"></param>
        /// <param name="listaAmarra"></param>
        /// <param name="passagem"></param>
        /// <returns></returns>
        public override Mensagem Embalar_Produto (CBEmbaladaDTO embalagem, List<CBAmarraDTO> listaAmarra, Passagem passagem, CBPostoDTO posto, bool gravarBancoSemp = true)
        {
            Stopwatch sw = new Stopwatch();
            string tempoExecucaoMetodo = string.Empty;
            string TextoTempo = string.Empty;
            TimeSpan ts;

            Mensagem retorno = new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "LIME", DesMensagem = "LEITURA OK. LEIA PRÓXIMO." };

            try
            {
                switch (passagem.posto.CodTipoPosto)
                {
                    case Constantes.POSTO_AMARRA_LOTE_TABLET_IMPRESSAO_ETIQUETA_PESO: 
                    case Constantes.POSTO_AMARRA_LOTE_TABLET_IMPRESSAO_ETIQUETA:
                        sw.Start();
                        retorno = passagem.GravarPassagem(embalagem.NumECB, embalagem.CodOperador);
                        sw.Stop();
                        ts = sw.Elapsed;

                        tempoExecucaoMetodo = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
                        TextoTempo += "\n Tempo gravação passagem: " + tempoExecucaoMetodo;

                        retorno.MensagemAuxiliar = TextoTempo;                      
                        break;

                    default:
                        retorno = base.Embalar_Produto(embalagem, listaAmarra, passagem, posto, true);
                        break;                     
                }
            }
            catch (Exception ex)
            {
                if (retorno.CodMensagem == Constantes.RAST_IMPRESSAO_NAO_REALIZADA)
                {
                    retorno = daoMensagem.ObterMensagem(Constantes.RAST_IMPRESSAO_NAO_REALIZADA);
                }
                else
                {
                    base.DesfazerOperacoes(embalagem, listaAmarra);
                    retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
                }

                retorno.DesMensagem = ex.Message;
                retorno.StackErro = ex.InnerException.Message;
            }

            return retorno;
        }

        /// <summary>
        /// Faz a validação de itens que são amarrados ao tablet
        /// </summary>
        /// <param name="tipoAmarra"></param>
        /// <param name="leitura"></param>
        /// <param name="numAp"></param>
        /// <param name="numSerie"></param>
        /// <param name="numKitProvisorio"></param>
        /// <returns></returns>
        public override Mensagem ValidarLeitura (int tipoAmarra, string leitura, string numAp, string numSerie, string numKitProvisorio, CBLinhaDTO linha)
        {
            return new Mensagem() { CodMensagem = Constantes.RAST_OK };
        }

        /// <summary>
        /// Faz as validações necessárias para o produto que está sendo lido. 
        /// Valida se já foi embalado e se passou nos postos anteriores
        /// Valida se já foram impressas as etiquetas
        /// Valida se reimpressão está ativa
        /// </summary>
        /// <param name="passagem"></param>
        /// <param name="codDrt"></param>
        /// <param name="numAp"></param>
        /// <param name="leitura"></param>
        /// <param name="modelo"></param>
        /// <param name="codFab"></param>
        /// <param name="linha"></param>
        /// <returns></returns>
        public override Mensagem ValidarProduto (Passagem passagem, String codDrt, string numAp, string leitura, string modelo, string codFab, string codigoCinescopio, CBLinhaDTO linha)
        {
            var retorno = base.ValidarProduto(passagem, codDrt, numAp, leitura, modelo, codFab, codigoCinescopio , linha);

            //if (retorno.CodMensagem != Constantes.RAST_OK)
            //{
            //    //if (retorno.CodMensagem != Constantes.RAST_LEITURA_REPETIDA)
            //    //    return retorno; //deu erro e não é leitura repetida

            //    ////se erro é leitura repetida, verificar se o retrabalho ta ativo
            //    //if (Recuperar_Status_Retrabalho(passagem.posto.CodLinha, passagem.posto.NumPosto) == false)
            //    //    return retorno;
            //}
            //else
            //{
            //    //se o produto ta ok, quer dizer que ainda não foi lido. nesta condição, não pode usar se o retrabalho estiver ativado
            //    if (Recuperar_Status_Retrabalho(passagem.posto.CodLinha, passagem.posto.NumPosto) == true)
            //    {
            //        return new Mensagem
            //        {
            //            CodMensagem = Constantes.RAST_LOTE_ITEM_INEXISTENTE,
            //            DesMensagem = "Reimpressão não permitida para etiquetas não impressas",
            //            DesCor = "red"
            //        };      
            //    }
            //}

            if (retorno.CodMensagem != Constantes.RAST_OK)
            {               
                return retorno;
            }

            var msg = SerialValido(leitura, numAp);
            if (!string.IsNullOrEmpty(msg))
            {
                return new Mensagem
                {
                    CodMensagem = Constantes.RAST_REGISTRONAOENCONTRADO,
                    DesMensagem = msg,
                    DesCor = "red"
                };  
            }

            return retorno;
        }
            
        /// <summary>
        /// Verifica se o serial pertence a AP informada
        /// </summary>
        /// <param name="numSerie"></param>
        /// <param name="numAP"></param>
        /// <returns></returns>
        private static string SerialValido(string numSerie, string numAP)
        {            
            var numAp = DaoEmbalagem.Recuperar_AP_Serial(numSerie);

            if (String.IsNullOrEmpty(numAp.ToString()))
            {
                return "Número de série inexistente na base de dados. Favor verificar";
            }
            
            if (numAp.ToString("D6") != numAP)
            {
                return "Número de série não pertence a AP informada. Favor verificar";
            }

            return "";
        }
         
        #region Impressão Etiquetas
        /// <summary>
        /// Imprime as etiquetas individuais usadas no tablet
        /// </summary>
        /// <param name="numAp"></param>
        /// <param name="leitura"></param>
        /// <param name="etiqueta"></param>
        /// <param name="listaEtqImpressao"></param>
        /// <returns></returns>
        public override Mensagem ImprimirEtiquetasIndividuais (string numAp, string leitura, string codModelo, CBPostoDTO posto, double PesoCaixa)
        {          
            var etiquetasPorPosto = Embalagem.RecuperarEtiquetasPorPosto(posto.CodLinha, posto.NumPosto, true);

            if (etiquetasPorPosto == null)
                return new Mensagem { CodMensagem = Constantes.RAST_OK };

            var retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);

            foreach (var etiqueta in etiquetasPorPosto)
            { 
                switch (etiqueta.NomeEtiqueta)
	            {
                    case "etq_tablet_individual_traseira" :
                        retorno = ImprimirEtiquetaTraseira(leitura, etiqueta);
                        break;

                    case "etq_tablet_manual":
                        retorno = ImprimirEtiquetaCaixaIndividualMensagem(leitura, etiqueta);
                        break;

                    case "etq_tablet_caixa":
                        var dadosEtiqueta = Embalagem.RecuperarDadosEtiquetaDto(codModelo);
                        retorno = ImprimirEtiquetaCaixaIndividualDados(numAp, leitura, dadosEtiqueta, PesoCaixa, etiqueta);
                        break;
	            }
            }

            return retorno;
        }

        /// <summary>
        /// Imprime a etiqueta da caixa coletiva usada no tablet
        /// </summary>
        /// <param name="numAp"></param>
        /// <param name="etiqueta"></param>
        /// <param name="seriais"></param>
        /// <param name="qtdLote"></param>
        /// <returns></returns>                                        
        public override Mensagem ImprimirCaixaColetiva (string numeroCaixaColetiva, CBLinhaDTO linha, CBPostoDTO posto)
        {
            var retorno = new Mensagem { CodMensagem = Constantes.RAST_OK };

            var EtiquetasPorPosto = Embalagem.RecuperarEtiquetasPorPosto(posto.CodLinha, posto.NumPosto, true).ToList().FirstOrDefault();

            if (EtiquetasPorPosto == null)
                return retorno;

            retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);

            if (string.IsNullOrEmpty(numeroCaixaColetiva))
            {
                retorno = Defeito.ObterMensagem(Constantes.ERRO_AO_REALIZAR_IMPRESSAO);
                retorno.DesMensagem = "Número de Série não possui caixa coletiva";
                return retorno;
            }

            var itensLote = base.RecuperarItensEmbaladadosDaCaixaColetiva(numeroCaixaColetiva);
            var numerosSerie = itensLote.Select(x => x.NumECB).ToArray();
            var numAp = itensLote[0].NumAP;
            string modelo = itensLote[0].CodModelo;

            var dadosEtiqueta = Embalagem.RecuperarDadosEtiquetaDto(modelo);

            if (dadosEtiqueta.DesResumida == null)
            {
                retorno = Defeito.ObterMensagem(Constantes.RAST_REGISTRONAOENCONTRADO);
                return retorno;
            }
            
            var dados = new Hashtable
            {
                {"EanColetivo", dadosEtiqueta.CodEANCxColetiva},
                {"EanIndividual", dadosEtiqueta.CodEAN},
                {"modelo", dadosEtiqueta.CodItem},
                {"modeloBarCode", dadosEtiqueta.CodItem},
                {"modeloVPE", dadosEtiqueta.DesResumida},
                {"numAP", numAp}               
            };

            for (var i = 0; i <= 5; i++)
            {
                dados.Add(("serial" + (i + 1)).Trim(), "");
                dados.Add(("desc" + (i + 1)).Trim(), "");
            }

            int cont = 0;
            for (var i = 0; i<= numerosSerie.Length -1; i++)
            {
                cont++;

                dados["serial" + (i + 1)] = numerosSerie[i];
                dados["desc" + (i + 1)] = "SN " + (i + 1) + " :";
            }

            if (cont == 1)
                dados.Add("quantidade", "Qtde: " + cont + "Pc");
            else
                dados.Add("quantidade", "Qtde: " + cont + "Pcs");

            GerenciarImpressao.ImprimirEtiqueta(dados, EtiquetasPorPosto);

            retorno = Defeito.ObterMensagem(Constantes.RAST_OK);
            return retorno;
        }

        /// <summary>
        /// Imprime a etiqueta que vai na caixa individual e que contém apenas o número de série
        /// </summary>
        /// <param name="numSerie"></param>
        private Mensagem ImprimirEtiquetaCaixaIndividualMensagem(string numSerie, CBEtiquetaDTO dadosEtiqueta)
        {
            var retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);
            
            var dados = new Hashtable
            {
                {"barCode", numSerie}
            };

            try
            {
                GerenciarImpressao.ImprimirEtiqueta(dados, dadosEtiqueta);
            }
            catch (Exception)
            {
                retorno = daoMensagem.ObterMensagem(Constantes.ERRO_AO_REALIZAR_IMPRESSAO);
                retorno.DesMensagem = "Erro ao realizar impressão da etiqueta do manual do tablet";
            }

            return retorno;
        }

        /// <summary>
        /// Imprime as etiquetas da caixa individual que contém os dados do produto e os itens que vão na caixa do aparelho
        /// </summary>
        /// <param name="numAp"></param>
        /// <param name="numSerie"></param>
        /// <param name="dados"></param>
        private Mensagem ImprimirEtiquetaCaixaIndividualDados (string numAp, string numSerie, viw_STI_ModeloEtiquetaDTO dados, double PesoCaixa, CBEtiquetaDTO etiqueta)
        {
            var retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);
            var pathEtq = etiqueta.CaminhoEtiqueta + etiqueta.NomeEtiqueta; // @"C:\Etiquetas\etq_caixa_ind_2.Lab";            
            var pathPrinter = etiqueta.NomeImpressora; // "EtqKitTablet";            
           
            var cxTexto = "";

            if (dados.DesDetalhe == null)
            {
                retorno = daoMensagem.ObterMensagem(Constantes.ERRO_AO_REALIZAR_IMPRESSAO);          
                retorno.DesMensagem =  "Erro ao recuperar detalhes do Produto para Impressão da Etiqueta da Caixa do Tablet";
                return retorno;
            }

            var aux2 = dados.DesDetalhe.Replace(Convert.ToChar(10), Convert.ToChar(13));
            var auxArray = aux2.Split(Convert.ToChar(13));

            auxArray = auxArray.Where(val => val != "").ToArray();
            
            cxTexto = auxArray.Aggregate(cxTexto, (current, ar) => current + (ar == "" ? "- " + ar : Convert.ToChar(13) + "- " + ar));

            try
            {

                if (PesoCaixa != 0)
                {
                    cxTexto += Convert.ToChar(13) + " Peso Bruto: " + PesoCaixa.ToString();

                    var dadosImpressao = new Hashtable()
                    {
                        {"caixaTexto", cxTexto},
                        {"EAN", dados.CodEAN},
                        {"modelo", dados.CodItem},
                        {"modeloVPE", dados.DesResumida},
                        {"numAP", numAp},
                        {"numeroserie", numSerie}                       
                    };
                    GerenciarImpressao.ImprimirEtiqueta(dadosImpressao, etiqueta);
                }
                else
                {
                    var dadosImpressao = new Hashtable()
                    {
                        {"caixaTexto", cxTexto},
                        {"EAN", dados.CodEAN},
                        {"modelo", dados.CodItem},
                        {"modeloVPE", dados.DesResumida},
                        {"numAP", numAp},
                        {"numeroserie", numSerie}
                    };
                    GerenciarImpressao.ImprimirEtiqueta(dadosImpressao, etiqueta);
                }
            }
            catch (Exception)
            {
                retorno = daoMensagem.ObterMensagem(Constantes.ERRO_AO_REALIZAR_IMPRESSAO);
                retorno.DesMensagem = "Erro ao imprimir da Etiqueta da Caixa do Tablet ";                 
            }
            
            return retorno;
        }

        /// <summary>
        /// Imprime a etiqueta traseira que contém o número de série do produto
        /// </summary>
        /// <param name="numSerie"> número de série do produto que deve ser impresso</param>
        /// <param name="dadosEtiqueta">Etiqueta que deve ser impressa</param>
        private static Mensagem ImprimirEtiquetaTraseira(string numSerie, CBEtiquetaDTO dadosEtiqueta)
        {      

            var retorno = new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "Lime", DesMensagem = "LEITURA OK. LEIA PRÓXIMO." };
                          

            var dadosImpressao = new Hashtable
            {
                {"numSerie", numSerie}
            };

            try
            {
                GerenciarImpressao.ImprimirEtiqueta(dadosImpressao, dadosEtiqueta);
            }
            catch (Exception)
            {
                retorno = daoMensagem.ObterMensagem(Constantes.ERRO_AO_REALIZAR_IMPRESSAO);
                retorno.DesMensagem = "Erro ao realizar impressão da etiqueta traseira do tablet";
            }

            return retorno;
        }


        public static Mensagem AjustarImpressoraEtiquetaTraseiraTablet (int quantidade, CBEtiquetaDTO etiqueta)
        {
            var retorno = new Mensagem { CodMensagem = Constantes.RAST_OK, DesCor = "Lime", DesMensagem = "Etiqueta Impressa com Sucesso" };
           
            try
            {
                for (int x = 1; x <= quantidade; x++)
                    ImprimirEtiquetaTraseira("9999" + x.ToString("00"), etiqueta);                
            }
            catch (Exception)
            {
                retorno = daoMensagem.ObterMensagem(Constantes.ERRO_AO_REALIZAR_IMPRESSAO);
                return retorno;
            }
            return retorno;
        }
         
        #endregion            

        #region Peso
        /// <summary>
        /// Recupera o peso da balança
        /// </summary>
        /// <returns></returns>
        public static double RecuperarPesoBalanca (string CodModelo, string NumIPBalanca)
        { 
            double pesoMinimo = 0;
            double pesoMaximo = 0;

            try
            {
                var dadosPeso = Embalagem.RecuperarPesoEmbalagem(CodModelo);

                pesoMinimo = Convert.ToDouble(dadosPeso.PesoMin);
                pesoMaximo = Convert.ToDouble(dadosPeso.PesoMax);
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao recuperar Faixa de Peso da Embalagem. \n" + ex.Message);
            }

            string ret = ClientBalanca.RecuperarPeso(NumIPBalanca);
            if (ret == "Erro")
            {
                return -1;
                //error = true;
                //retorno = Defeito.ObterMensagem(Constantes.ERRO_AO_REALIZAR_IMPRESSAO);
                //retorno.DesMensagem = "Não foi possível conectar ao servidor da balança";
            }
            else
            {
                var PesoCaixa = Convert.ToDouble(ret);

                if (!(PesoCaixa >= pesoMinimo && PesoCaixa <= pesoMaximo))
                {
                    return -2;
                    //error = true;
                    //retorno = Defeito.ObterMensagem(Constantes.ERRO_AO_REALIZAR_IMPRESSAO);
                    //retorno.DesMensagem = "Peso do aparelho não está dentro da faixa permitida";
                }
                return PesoCaixa;
            }
        }
        #endregion

    }
}
