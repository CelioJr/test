namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ORDER_TEXT
    {
        public Guid ID { get; set; }

        public Guid ORDER_HEADER_IN_ID { get; set; }

        [StringLength(4)]
        public string TEXT_ID { get; set; }

        [Column(TypeName = "text")]
        public string TEXT_LINE { get; set; }

        public virtual ORDER_HEADER_IN ORDER_HEADER_IN { get; set; }
    }
}
