using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class RPADTO
    {
        public string CodFab { get; set; }
        public string NumRPA { get; set; }
        public System.DateTime DatEmissao { get; set; }
        public string CodDepto { get; set; }
        public string NomRequisitante { get; set; }
        public string CodMotivo { get; set; }
        public string CodItem { get; set; }
        public string NumSerieMod { get; set; }
        public decimal QtdRequisitado { get; set; }
        public Nullable<System.DateTime> DatRetornoExp { get; set; }
        public Nullable<decimal> QtdRetorno { get; set; }
        public string FlgReintegra { get; set; }
        public string ObsRPA { get; set; }
        public string Usuario { get; set; }
        public string NomRespSetor { get; set; }
        public Nullable<System.DateTime> DatRespSetor { get; set; }
        public string NomDiretor { get; set; }
        public Nullable<System.DateTime> DatDiretor { get; set; }
        public System.Guid rowguid { get; set; }
		public string DesItem { get; set; }
    }
}
