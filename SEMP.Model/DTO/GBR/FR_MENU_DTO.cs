using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_MENU_DTO
    {
        public string SIS_CODIGO { get; set; }
        public int MNU_CODIGO { get; set; }
        public string MNU_DESCRICAO { get; set; }
        public Nullable<int> MNU_CODIGO_PARENT { get; set; }
        public int MNU_INDICE { get; set; }
        public Nullable<int> FRM_CODIGO { get; set; }
        public string MNU_TECLA { get; set; }
        public Nullable<int> IMG_CODIGO { get; set; }
        public string MNU_SEPARADOR { get; set; }
        public string MNU_GUID { get; set; }
        public string MNU_TIPO { get; set; }
        public Nullable<int> REL_CODIGO { get; set; }
        public virtual FR_FORMULARIO_DTO FR_FORMULARIO { get; set; }
        public virtual FR_SISTEMA_DTO FR_SISTEMA { get; set; }
    }
}
