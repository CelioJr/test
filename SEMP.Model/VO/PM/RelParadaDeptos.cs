﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.PM
{
	public class RelParadaDeptos
	{
		public string CodDepto { get; set; }
		public string DesDepto { get; set; }
		public decimal? NumHorasTV { get; set; }
		public decimal? NumHorasAudio { get; set; }
		public decimal? NumHorasSTPCI { get; set; }
		public decimal? NumHorasDVD { get; set; }
		public decimal? NumHorasIAC { get; set; }
		public decimal? NumHorasIMC { get; set; }
		public decimal? NumHorasMSC { get; set; }
		public decimal? NumHorasTotal { get; set; }
	}
}
