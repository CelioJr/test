namespace SEMP.Model.DTO
{
    using System;

    public partial class HistoricoDTO
    {
        public int codOF { get; set; }
        public string desSerie { get; set; }
        public DateTime? datSepAlmox { get; set; }
        public DateTime? dat1 { get; set; }
        public int? codOpe1 { get; set; }
        public DateTime? dat2 { get; set; }
        public int? codOpe2 { get; set; }
        public DateTime? dat3 { get; set; }
        public int? codOpe3 { get; set; }
        public DateTime? dat4 { get; set; }
        public int? codOpe4 { get; set; }
        public DateTime? dat5 { get; set; }
        public int? codOpe5 { get; set; }
        public DateTime? dat6 { get; set; }
        public int? codOpe6 { get; set; }
        public DateTime? dat7 { get; set; }
        public int? codOpe7 { get; set; }
        public byte codAtual { get; set; }
        public byte? codOntem { get; set; }
        public byte codPointControl { get; set; }
        public bool flgSemiProduto { get; set; }
        public byte? codRetorno { get; set; }
        public string LicencaWindows { get; set; }
        public byte? codLinha { get; set; }
        public DateTime? dat10 { get; set; }
        public int? codOpe10 { get; set; }
        public bool cifrado { get; set; }
        public string NumPatrimonio { get; set; }
        public bool vinil { get; set; }
        public string LicencaBetwin { get; set; }
        public DateTime? datDownload { get; set; }
        public int? codOpeDownload { get; set; }
        public int? imagem_id { get; set; }
        public string ExpressaoHCL { get; set; }
        public int? codRegistroMulticast { get; set; }
        public int? codLinhaEmbal { get; set; }
        public DateTime? datEmbalPosto1 { get; set; }
        public int? codOpeEmbalPosto1 { get; set; }
        public DateTime? datImpEtiqMetalizada { get; set; }
        public int? CodOpeImpEtiqMetalizada { get; set; }
        public DateTime? datAprovacaoCQ { get; set; }
        public int? CodOpeAprovacaoCQ { get; set; }
        public DateTime? firstDat1 { get; set; }
        public DateTime? firstDat2 { get; set; }
        public DateTime? firstDat3 { get; set; }
        public DateTime? firstDat4 { get; set; }
        public DateTime? firstDat5 { get; set; }
        public DateTime? firstDat6 { get; set; }
        public DateTime? firstDat7 { get; set; }
        public DateTime? firstDat10 { get; set; }
        public DateTime? firstDatDownload { get; set; }
        public DateTime? firstDatEmbalPosto1 { get; set; }
        public DateTime? firstDatSepAlmox { get; set; }
        public DateTime? firstdatAprovacaoCQ { get; set; }
        public string LicencaSoftware { get; set; }
        public decimal? valPesoKit { get; set; }
    }
}
