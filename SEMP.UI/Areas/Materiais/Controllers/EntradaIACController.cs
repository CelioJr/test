﻿using MATERIAIS.BO.Estoque;
using SEMP.Model.DTO;
using SEMP.Model.VO.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SEMP.Areas.Materiais.Controllers
{
	public class EntradaIACController : Controller
	{
		// GET: Materiais/EntradaIAC
		public ActionResult Index()
		{
			return View();
		}

		[HttpPost]
		public JsonResult GravarEntrada(string numOP, string BarrasUD, string BarrasFornecedor, string nomUsuario)
		{
			try
			{
				var entrada = new CBEntradaIACDTO()
				{
					BarrasFornecedor = BarrasFornecedor,
					BarrasUD = BarrasUD,
					CodItem = BarrasUD.Substring(0, 6),
					DatLeitura = DateTime.Now,
					NomUsuario = nomUsuario,
					NumOP = numOP.TrimStart('0'),
					NumUD = BarrasUD.Substring(25),
					QtdItem = decimal.Parse(BarrasUD.Substring(6, 10).Replace('.', ','))
				};

				var gravar = EntradaIAC.GravarEntradaIAC(entrada);

				return Json(gravar, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{

				return Json(ex.Message, JsonRequestBehavior.AllowGet);
			}
		}

		public JsonResult ObterEntradas(string numOP, string datInicio, string datFim)
		{
			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni.AddDays(1).AddMinutes(-1);

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}
			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim).AddDays(1).AddMinutes(-1);
			}

			var dados = EntradaIAC.ObterEntradasIAC(numOP.TrimStart('0'), dIni, dFim);

			return Json(dados, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ObterDadosSAP(string numOP, string datInicio, string datFim)
		{
			var dIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var dFim = dIni.AddDays(1).AddMinutes(-1);

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
			}
			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim).AddDays(1).AddMinutes(-1);
			}

			var resultado = new List<LTAP>();
			var dados = SAPConnector.SAPFunctions.ObterLTAP(ref resultado, numOP.TrimStart('0'), "");

			var UDs = resultado.Select(x => x.VLENR).ToList();
			var entradas = EntradaIAC.ObterLeituraUD(UDs);

			foreach (var item in entradas)
			{
				var lidos = resultado.FirstOrDefault(x => item.NumUD == x.VLENR);
				if (lidos != null)
				{
					lidos.Lido = true;
					lidos.DatLido = item.DatLeitura;
				}
			}

			return Json(resultado.OrderByDescending(x => x.Lido).ThenBy(x => x.MATNR).ThenBy(x => x.QDATU).ToList(), JsonRequestBehavior.AllowGet);

		}

		public JsonResult ObterDadosUD(string numOP, string numUD)
		{

			var resultado = new List<LTAP>();
			var dados = SAPConnector.SAPFunctions.ObterLTAP(ref resultado, numOP, numUD);

			return Json(resultado, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ObterLeituraUD(string numUD)
		{

			var resultado = EntradaIAC.ObterLeituraUD(numUD);

			return Json(resultado, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ObterLeituraBarrasFornecedor(string BarrasFornecedor)
		{

			var resultado = EntradaIAC.ObterLeituraFornecedor(BarrasFornecedor);

			return Json(resultado, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ValidarLeituraUD(string numUD, string numOP)
		{

			// verificar se a etiqueta já foi lida
			var resultado = EntradaIAC.ObterLeituraUD(numUD);

			if (resultado != null)
			{
				var retorno = new
				{
					status = false,
					mensagem = "Etiqueta já lida."
				};

				return Json(retorno, JsonRequestBehavior.AllowGet);
			}

			// verificar no SAP se a UD existe e se foi movimentada para essa ordem
			var ltap = new List<LTAP>();
			var dados = SAPConnector.SAPFunctions.ObterLTAP(ref ltap, "", numUD.Substring(25)); //numOP.TrimStart('0'), numUD.Substring(25));

			if (ltap == null || ltap.Count() == 0)
			{
				var retorno = new
				{
					status = false,
					mensagem = "UD inválida ou ainda não movimentada."
				};

				return Json(retorno, JsonRequestBehavior.AllowGet);
			}
			else
			{
				var registro = ltap.FirstOrDefault();

				if (registro.NLPLA != numOP)
				{
					var retorno = new
					{
						status = false,
						mensagem = "UD não movimentada para essa Ordem."
					};

					return Json(retorno, JsonRequestBehavior.AllowGet);
				}
				else
				{
					var retorno = new
					{
						status = true,
						mensagem = ""
					};

					return Json(retorno, JsonRequestBehavior.AllowGet);
				}
			}

		}
	}
}