using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_ROLE_USER_DTO
    {
        public ALC_ROLE_USER_DTO()
        {
            this.ALC_SYS_USER = new List<ALC_SYS_USER_DTO>();
        }

        public string authority { get; set; }
        public virtual ICollection<ALC_SYS_USER_DTO> ALC_SYS_USER { get; set; }
    }
}
