using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBAcessoMap : EntityTypeConfiguration<tbl_CBAcesso>
    {
        public tbl_CBAcessoMap()
        {
            // Primary Key
            this.HasKey(t => t.CodAcesso);

            // Properties
            this.Property(t => t.CodAcesso)
                .IsFixedLength()
                .HasMaxLength(20)
                .IsUnicode(false);

            this.Property(t => t.AcessoDepto)
                .IsFixedLength()
                .HasMaxLength(8000)
                .IsUnicode(false);

			this.Property(t => t.AcessoUser)
				.IsFixedLength()
				.HasMaxLength(8000)
                .IsUnicode(false);

			// Table & Column Mappings
			this.ToTable("tbl_CBAcesso");
            this.Property(t => t.CodAcesso).HasColumnName("CodAcesso");
            this.Property(t => t.AcessoDepto).HasColumnName("AcessoDepto");
            this.Property(t => t.AcessoUser).HasColumnName("AcessoUser");
        }
    }
}
