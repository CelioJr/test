using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DEP_FORMULARIO_TABELA_DTO
    {
        public int FRM_CODIGO { get; set; }
        public string TAB_NOME { get; set; }
    }
}
