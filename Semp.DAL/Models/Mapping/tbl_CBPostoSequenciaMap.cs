using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBPostoSequenciaMap : EntityTypeConfiguration<tbl_CBPostoSequencia>
    {
        public tbl_CBPostoSequenciaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdRota);

            // Properties
            this.Property(t => t.IdRota)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6);

            this.Property(t => t.CodFab)
                .IsRequired()
                .HasMaxLength(5);

            this.Property(t => t.LetraLinha)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.DesPosto)
                .HasMaxLength(50);

            this.Property(t => t.NumIP)
                .HasMaxLength(15);

            this.Property(t => t.CodDRT)
                .IsFixedLength()
                .HasMaxLength(8);

            this.Property(t => t.FlgTipoTerminal)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.FlgAtivo)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.TipoAmarra)
                .IsFixedLength()
                .HasMaxLength(2);

            // Table & Column Mappings
            this.ToTable("tbl_CBPostoSequencia");
            this.Property(t => t.IdRota).HasColumnName("IdRota");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.LetraLinha).HasColumnName("LetraLinha");
            this.Property(t => t.NumPosto).HasColumnName("NumPosto");
            this.Property(t => t.CodTipoPosto).HasColumnName("CodTipoPosto");
            this.Property(t => t.DesPosto).HasColumnName("DesPosto");
            this.Property(t => t.NumIP).HasColumnName("NumIP");
            this.Property(t => t.CodDRT).HasColumnName("CodDRT");
            this.Property(t => t.FlgTipoTerminal).HasColumnName("FlgTipoTerminal");
            this.Property(t => t.FlgAtivo).HasColumnName("FlgAtivo");
            this.Property(t => t.NumSeq).HasColumnName("NumSeq");
            this.Property(t => t.flgObrigatorio).HasColumnName("flgObrigatorio");
            this.Property(t => t.flgDRTObrig).HasColumnName("flgDRTObrig");
            this.Property(t => t.TipoAmarra).HasColumnName("TipoAmarra");

        }
    }
}
