﻿using SEMP.BO;
using SEMP.Model;
using SEMP.Model.VO;
using System;
using System.Windows;
using System.Windows.Input;

namespace SISAP
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

			Util.IniciarMapper();
			//txtCracha.Focus();
			pwCracha.Focus();
        }

        private void btnEntrar(object sender, RoutedEventArgs e)
        {
            Login();
        }

        private void Login()
        {
            string username = pwCracha.Password, password = string.Empty;

            try
            {
                if ("devLogin".Equals(username) && "d3vL0g1n".Equals(password))
                {
                    RetornoLogin dev = new RetornoLogin();
                    dev.CodDRT = "99999";
                    dev.Cracha = "999999999999";
                    dev.Depto = "FUCAPI";
                    dev.Cargo = "Analista";
                    dev.NomFuncionario = "Desenv Fucapi";
                    dev.NomUsuario = username;
                    dev.Status = new Mensagem();
                    dev.UsuarioCracha = false;

                    new Passagem(dev).Show();
                    this.Close();

                }
                else if (password.Equals(String.Empty))
                {
                    if (username.Length == 12)
                    {
                        username = username.Substring(0, 11);
                    }

                    RetornoLogin retorno = SEMP.BO.Login.ValidaCracha(username);

                    if (retorno != null && retorno.Status.CodMensagem == Constantes.DB_OK)
                    {
                        new Passagem(retorno).Show();
                        this.Close();
                    }
                    else
                    {
                        pwCracha.Password = string.Empty;
                        MessageBox.Show("Usuário não encontrado", "ERRO");
                    }
                }
                else
                {
                    pwCracha.Password = string.Empty;
                    MessageBox.Show("Usuário não encontrado", "ERRO");
                }
            }

            catch (Exception ex)
            {
                pwCracha.Password = string.Empty;
                MessageBox.Show("Erro no servidor: " + ex.Message, "ERRO");
            }
        }

        private void txtCracha_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Login();
            }
        }

    }
}
