﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Embalagem
{
    public class KitAcessorio
    {
        public int CodKit { get; set; }
        public System.DateTime DatImpressao { get; set; }
        public int CodOperador { get; set; }
        public int CodOF { get; set; }
        public string NumSerie { get; set; }
        public string SerialAdaptador { get; set; }
        public string NumLicencaNero { get; set; }
    }
}
