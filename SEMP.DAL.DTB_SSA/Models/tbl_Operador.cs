using System;
using System.Collections.Generic;

namespace SEMP.DAL.DTB_SSA.Models
{
    public partial class tbl_Operador
    {
        public tbl_Operador()
        {
        }

        public int codOperador { get; set; }
        public string desLogin { get; set; }
        public string desNome { get; set; }
        public string desSenha { get; set; }
        public int codGrupo { get; set; }
        public string NivelFullControl { get; set; }
        public string NivelSistCQ { get; set; }
        public string nivel_sist_defeito { get; set; }
        public string nivel_sist_SGI { get; set; }
        public string nivel_sist_Ind { get; set; }
        public string nivel_sist_componente { get; set; }
        public string nivel_sist_sec { get; set; }
        public string nivel_sist_revisao { get; set; }
        public string nivel_sist_PSI { get; set; }
        public string nivel_sist_revisaoPeriodica { get; set; }
        public string nivel_sist_ExpedicaoNF { get; set; }
        public string flgRamalAtivo { get; set; }
        public string codFab { get; set; }
        public string codDepto { get; set; }
        public string nivel_sec_sem { get; set; }
        public string nivel_colex { get; set; }
        public string nivel_sist_MM { get; set; }
        public string nivel_sist_PlanProd { get; set; }
        public string nivel_sist_PcEmb { get; set; }
        public string nivel_sist_CheckFrete { get; set; }
        public string nivel_sist_gdtissa { get; set; }
        public DateTime? dataExpiracao { get; set; }
        public string nivel_sist_separacao { get; set; }
        public string nivel_sist_siet { get; set; }
        public DateTime? dataExpiracaoSIGA { get; set; }
        public string nivel_sist_rh { get; set; }
        public string miner_depto_PlanoAcao { get; set; }
        public string Email { get; set; }
    }
}
