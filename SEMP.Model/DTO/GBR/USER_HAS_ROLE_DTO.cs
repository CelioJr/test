using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class USER_HAS_ROLE_DTO
    {
        public int userId { get; set; }
        public string authority { get; set; }
        public int USER_ID { get; set; }
    }
}
