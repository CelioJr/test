using System;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
    public class CnqDefeitoDTO
    {
		[Display(Name = "F�brica")]
        public string CodFab { get; set; }
		[Display(Name = "Fam�lia")]
        public string CodFam { get; set; }
		[Display(Name = "Sub Fam�lia")]
        public string CodSubFam { get; set; }
		[Display(Name = "C�d. Defeito")]
        public string CodDefeito { get; set; }
		[Display(Name = "Descri��o")]
        public string DesDefeito { get; set; }
		[Display(Name = "IMC")]
        public Nullable<short> flgIMC { get; set; }
		[Display(Name = "FEC")]
        public Nullable<short> flgMONTAGEM { get; set; }
		[Display(Name = "IAC")]
        public Nullable<short> flgIAC { get; set; }
		[Display(Name = "Ativo")]
        public string flgAtivo { get; set; }
    }
}
