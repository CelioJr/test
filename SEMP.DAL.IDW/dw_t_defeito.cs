namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_t_defeito
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_t_defeito()
        {
            dw_consoldef = new HashSet<dw_consoldef>();
            dw_expcvs = new HashSet<dw_expcvs>();
            dw_expcvs1 = new HashSet<dw_expcvs>();
            dw_passcau = new HashSet<dw_passcau>();
            dw_passdef = new HashSet<dw_passdef>();
        }

        [Key]
        public long id_tdefeito { get; set; }

        [StringLength(30)]
        public string cd_tdefeito { get; set; }

        public int? revisao { get; set; }

        [StringLength(40)]
        public string ds_tdefeito { get; set; }

        public DateTime? dt_revisao { get; set; }

        public short? st_ativo { get; set; }

        public DateTime? dt_stativo { get; set; }

        public long id_usrstativo { get; set; }

        public long id_usrrevisao { get; set; }

        public long id_tppt { get; set; }

        public short? is_requeracao { get; set; }

        public long? id_produto { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consoldef> dw_consoldef { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_expcvs> dw_expcvs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_expcvs> dw_expcvs1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_passcau> dw_passcau { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_passdef> dw_passdef { get; set; }

        public virtual om_produto om_produto { get; set; }

        public virtual om_tppt om_tppt { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }
    }
}
