using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_HistoricoRetPostoMap : EntityTypeConfiguration<tbl_HistoricoRetPosto>
    {
        public tbl_HistoricoRetPostoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Posto_id, t.DatAtivacao });

            // Properties
            this.Property(t => t.Posto_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("tbl_HistoricoRetPosto");
            this.Property(t => t.Posto_id).HasColumnName("Posto_id");
            this.Property(t => t.DatAtivacao).HasColumnName("DatAtivacao");
            this.Property(t => t.CodOpeAtivacao).HasColumnName("CodOpeAtivacao");
            this.Property(t => t.DatDesativacao).HasColumnName("DatDesativacao");
            this.Property(t => t.CodOpeDesativacao).HasColumnName("CodOpeDesativacao");
            this.Property(t => t.flgPesagemKitAces).HasColumnName("flgPesagemKitAces");


        }
    }
}
