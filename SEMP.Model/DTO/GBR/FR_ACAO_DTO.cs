using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_ACAO_DTO
    {
        public FR_ACAO_DTO()
        {
            this.FR_ACAOCOMPONENTE = new List<FR_ACAOCOMPONENTE_DTO>();
            this.FR_ACAOPARAMETRO = new List<FR_ACAOPARAMETRO_DTO>();
        }

        public int ACO_CODIGO { get; set; }
        public string ACO_NOME { get; set; }
        public virtual ICollection<FR_ACAOCOMPONENTE_DTO> FR_ACAOCOMPONENTE { get; set; }
        public virtual ICollection<FR_ACAOPARAMETRO_DTO> FR_ACAOPARAMETRO { get; set; }
    }
}
