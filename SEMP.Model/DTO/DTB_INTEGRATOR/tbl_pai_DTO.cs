namespace SEMP.Model.DTO
{
	using System.ComponentModel.DataAnnotations;

	public partial class tbl_pai_DTO
	{
        [Key]
        public int id_pai { get; set; }

        [Required]
        [StringLength(50)]
        public string CODAPP { get; set; }

        [StringLength(100)]
        public string NAME { get; set; }
		
    }
}
