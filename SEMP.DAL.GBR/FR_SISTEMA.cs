namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_SISTEMA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FR_SISTEMA()
        {
            FR_DOC_PRINCIPAL = new HashSet<FR_DOC_PRINCIPAL>();
            FR_HISTORICO_SQL = new HashSet<FR_HISTORICO_SQL>();
            FR_MENU = new HashSet<FR_MENU>();
            FR_TAREFA = new HashSet<FR_TAREFA>();
            FR_FORMULARIO = new HashSet<FR_FORMULARIO>();
        }

        [Key]
        [StringLength(3)]
        public string SIS_CODIGO { get; set; }

        [Required]
        [StringLength(30)]
        public string SIS_DESCRICAO { get; set; }

        public int? IMG_CODIGO { get; set; }

        public int? IMG_CODIGO_ICONE { get; set; }

        [StringLength(2000)]
        public string SIS_SQLDATALIMITE { get; set; }

        [StringLength(2000)]
        public string SIS_SQLDADOSENTIDADE { get; set; }

        [StringLength(2000)]
        public string SIS_SQLINFORMACOES { get; set; }

        [StringLength(30)]
        public string SIS_CHECK { get; set; }

        public bool? SIS_ACESSOEXTERNO { get; set; }

        public int? SIS_GRUPOEXTERNO { get; set; }

        [StringLength(2000)]
        public string SIS_INFORMACAO { get; set; }

        [StringLength(1000)]
        public string SIS_RESUMO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_DOC_PRINCIPAL> FR_DOC_PRINCIPAL { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_HISTORICO_SQL> FR_HISTORICO_SQL { get; set; }

        public virtual FR_IMAGEM FR_IMAGEM { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_MENU> FR_MENU { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_TAREFA> FR_TAREFA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_FORMULARIO> FR_FORMULARIO { get; set; }
    }
}
