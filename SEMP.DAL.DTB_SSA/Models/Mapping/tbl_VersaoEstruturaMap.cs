using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.DTB_SSA.Models.Mapping
{
    public class tbl_VersaoEstruturaMap : EntityTypeConfiguration<tbl_VersaoEstrutura>
    {
        public tbl_VersaoEstruturaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodVersao, t.CodOF, t.ne });

            // Properties
            this.Property(t => t.CodVersao)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodOF)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ne)
                .IsRequired()
                .HasMaxLength(13);

            this.Property(t => t.flgRecente)
                .IsFixedLength()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("tbl_VersaoEstrutura");
            this.Property(t => t.CodVersao).HasColumnName("CodVersao");
            this.Property(t => t.CodOF).HasColumnName("CodOF");
            this.Property(t => t.ne).HasColumnName("ne");
            this.Property(t => t.datAtualizacao).HasColumnName("datAtualizacao");
            this.Property(t => t.CodOperador).HasColumnName("CodOperador");
            this.Property(t => t.QtdFrequencia).HasColumnName("QtdFrequencia");
            this.Property(t => t.flgRecente).HasColumnName("flgRecente");

        }
    }
}
