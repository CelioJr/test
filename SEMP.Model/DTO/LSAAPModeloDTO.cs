using System;

namespace SEMP.Model.DTO
{
    public class LSAAPModeloDTO
    {
        public string CodFab { get; set; }
        public string NumAP { get; set; }
        public string CodModelo { get; set; }
        public string CodModeloApont { get; set; }
        public string CodProcesso { get; set; }
        public Nullable<decimal> QtdEmbalado { get; set; }
        public Nullable<decimal> QtdApontado { get; set; }
        public Nullable<decimal> QtdRejeitado { get; set; }
        public Nullable<int> Nivel { get; set; }
        public Nullable<int> Ordem { get; set; }
        public Nullable<int> QtdItens { get; set; }
        public string FlgApontar { get; set; }
        public string CodLocalCRE { get; set; }
        public string CodLocalDEB { get; set; }
        public string CodLinha { get; set; }
        public Nullable<decimal> QtdNecessidade { get; set; }
        public Nullable<decimal> QtdSaldoAnt { get; set; }

		public string DesModelo { get; set; }
		public string DesModeloApont { get; set; }
		public string DesProcesso { get; set; }
		public string DesLocalCRE { get; set; }
		public string DesLocalDEB { get; set; }
		public string DesLinha { get; set; }
	}
}
