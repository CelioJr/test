using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEMP.Model.DTO
{
	[NotMapped]
	public class ProdParadaDTO
	{
		[Display(Name = "F�b.")]
		[Required]
		public string CodFab { get; set; }
		[Display(Name = "Linha")]
		public string CodLinha { get; set; }
		[Display(Name = "N. Parada")]
		public string NumParada { get; set; }
		[Display(Name = "Modelo")]
		public string CodModelo { get; set; }
		[Display(Name = "Data Parada")]
		[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
		public System.DateTime DatParada { get; set; }
		[Display(Name = "Motivo da Parada")]
		[Required]
		public string CodParada { get; set; }
		[Display(Name = "Item (N.E.)")]
		public string CodItem { get; set; }
		[Display(Name = "Hora In�cio")]
		[DisplayFormat(DataFormatString = "{0:hh:mm}")]
		[Required(ErrorMessage = "Informe a hora")]
		public string HorInicio { get; set; }
		[Display(Name = "Hora Fim")]
		public string HorFim { get; set; }
		[Display(Name = "Perda na Produ��o?")]
		[Required]
		public string StatusPerda { get; set; }
		[Display(Name = "Qtde p�s/ Aparelho")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? QtdItem { get; set; }
		[Display(Name = "Aps. N�o Prod.")]
		[DisplayFormat(DataFormatString = "{0:0}")]
		public decimal? QtdNaoProduzida { get; set; }
		[Display(Name = "Turno de Trabalho")]
		[Required]
		public string CodTurno { get; set; }
		[Display(Name = "Almo�o")]
		public string HorAlmoco { get; set; }
		[Display(Name = "No. Func.")]
		public int? NumFuncionarios { get; set; }
		[Display(Name = "Qtde Falta")]
		public decimal? QtdFalta { get; set; }
		[Display(Name = "Detalhes da Parada")]
		public string DesObs { get; set; }
		[Display(Name = "Emails Enviados?")]
		public string StatusEmail { get; set; }
		[Display(Name = "rowguid")]
		public System.Guid rowguid { get; set; }
		[Display(Name = "DRT")]
		public string NumDrt { get; set; }

		[Display(Name = "Descri��o")]
		public string DesModelo { get; set; }
		[Display(Name = "Descri��o")]
		public string DesItem { get; set; }
		[Display(Name = "Descri��o")]
		public string DesFabrica { get; set; }
		[Display(Name = "Descri��o")]
		public string DesParada { get; set; }

        [Display(Name = "Descri��o")]
        public string DesCausa { get; set; }

		[Display(Name = "Descri��o")]
		public string DesLinha { get; set; }

		[Display(Name = "Descri��o Depto Linha")]
		public string DesDepLinha { get; set; }

		[Display(Name = "Deptos da Parada")]
		public string Deptos { get; set; }

		[Display(Name="Resp.")]
		public int QtdRespostas { get; set; }

        [Display(Name = "Fam")]
        public string Familia { get; set; }

        [Display(Name = "Total Horas")]
        public double TotalHoras { get; set; }

        [Display(Name = "Total Horas Tipo")]
        public decimal TotalHorasTipo { get; set; }

        [Display(Name = "Tipo")]
        public string Tipo { get; set; }

	}
}
