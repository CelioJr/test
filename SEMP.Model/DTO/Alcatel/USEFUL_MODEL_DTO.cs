using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class USEFUL_MODEL_DTO
    {
        public Nullable<long> ID { get; set; }
        public int COD_SKU { get; set; }
        public Nullable<int> COD_MODELO { get; set; }
        public string SKU { get; set; }
        public string EAN { get; set; }
        public string MODEL { get; set; }
        public string MODEL_NAME { get; set; }
        public Nullable<int> QTY_BOX { get; set; }
        public Nullable<int> QTY_PALLET { get; set; }
        public Nullable<int> QTY_LOT { get; set; }
    }
}
