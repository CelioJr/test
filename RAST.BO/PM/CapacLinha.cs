﻿using SEMP.DAL.DAO.PM;
using SEMP.Model.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAST.BO.PM
{
	public class CapacLinha
	{
		public static List<CapacLinhaDTO> ObterCapacidades(DateTime mesRef, string codLinha, string codModelo)
		{
			return daoCapacLinha.ObterCapacidades(mesRef, codLinha, codModelo);
		}

		public static CapacLinhaDTO ObterCapacidade(DateTime mesRef, string codLinha, string codModelo)
		{
			return daoCapacLinha.ObterCapacidade(mesRef, codLinha, codModelo);
		}

		public static void Gravar(CapacLinhaDTO capacidade)
		{
			daoCapacLinha.Gravar(capacidade);
		}

		public static void Remover(CapacLinhaDTO capacidade)
		{
			daoCapacLinha.Remover(capacidade);
		}

		public static bool VerificaSeModeloExiste(string codigo)
		{
			return daoCapacLinha.VerificaSeModeloExiste(codigo);
		}

		public static bool VerificaSeLinhaExiste(string codigo)
		{
			return daoCapacLinha.VerificaSeLinhaExiste(codigo);
		}

		public static bool ClonarMes(DateTime mesRef)
		{
			return daoCapacLinha.ClonarMes(mesRef);
		}

        public static bool ClonarMes(DateTime mesRef, DateTime mesDest)
        {
            return daoCapacLinha.ClonarMes(mesRef, mesDest);
        }
	}
}
