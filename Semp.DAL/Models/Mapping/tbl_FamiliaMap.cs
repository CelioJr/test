﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
	class tbl_FamiliaMap : EntityTypeConfiguration<tbl_Familia>
	{
		public tbl_FamiliaMap()
		{
			// Primary Key
			this.HasKey(t => t.CodFam);

			// Properties
			this.Property(t => t.CodFam)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(6)
                .IsUnicode(false);

			this.Property(t => t.NomFam)
				.IsRequired()
				.HasMaxLength(25)
                .IsUnicode(false);

			this.Property(t => t.EspTecnica)
				.HasMaxLength(15)
                .IsUnicode(false);

			this.Property(t => t.FlgInspecao)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			this.Property(t => t.Usuario)
				.IsFixedLength()
				.HasMaxLength(25)
                .IsUnicode(false);

			this.Property(t => t.FlgKitAcessorio)
				.IsFixedLength()
				.HasMaxLength(1)
                .IsUnicode(false);

			this.Property(t => t.CodFamToshiba)
				.IsFixedLength()
				.HasMaxLength(3)
                .IsUnicode(false);

			// Table & Column Mappings
			this.ToTable("tbl_Familia");
			this.Property(t => t.CodFam).HasColumnName("CodFam");
			this.Property(t => t.NomFam).HasColumnName("NomFam");
			this.Property(t => t.EspTecnica).HasColumnName("EspTecnica");
			this.Property(t => t.FlgInspecao).HasColumnName("FlgInspecao");
			this.Property(t => t.Usuario).HasColumnName("Usuario");
			this.Property(t => t.rowguid).HasColumnName("rowguid");
			this.Property(t => t.FlgAtivo).HasColumnName("FlgAtivo");
			this.Property(t => t.FlgKitAcessorio).HasColumnName("FlgKitAcessorio");
			this.Property(t => t.CodFamToshiba).HasColumnName("CodFamToshiba");
			this.Property(t => t.FlgRastreavel).HasColumnName("FlgRastreavel");
		}
	}
}

