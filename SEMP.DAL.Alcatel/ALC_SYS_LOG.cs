namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_SYS_LOG")]
    public partial class ALC_SYS_LOG
    {
        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal ID_LOG { get; set; }

        [StringLength(255)]
        public string CODE { get; set; }

        [StringLength(255)]
        public string DESCRIPTION { get; set; }

        public DateTime? DT_MAKED { get; set; }

        public int? RESPONSIBLE { get; set; }

        [StringLength(255)]
        public string SYS_LOG { get; set; }

        public virtual ALC_SYS_USER ALC_SYS_USER { get; set; }
    }
}
