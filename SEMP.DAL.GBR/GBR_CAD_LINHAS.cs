namespace SEMP.DAL.GBR
{
	using System;
	using System.ComponentModel.DataAnnotations;

	public partial class GBR_CAD_LINHAS
    {
        [Key]
        public int COD_LINHAS { get; set; }

        [StringLength(20)]
        public string NOME_LINHA { get; set; }

        public DateTime? DATA { get; set; }
		
    }
}
