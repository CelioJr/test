using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_Parada
    {
        public string CodFab { get; set; }
        public string CodParada { get; set; }
        public string DesParada { get; set; }
        public string TipParada { get; set; }
        public System.Guid rowguid { get; set; }
    }
}
