using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class GBR_CQTF_LIST_HIST_DTO
    {
        public int COD_HIST { get; set; }
        public string ARQUIVO { get; set; }
        public string IMEI { get; set; }
        public Nullable<System.DateTime> DATA { get; set; }
        public string CABECALHO { get; set; }
        public string INPUT { get; set; }
    }
}
