namespace SEMP.Model.DTO
{
    public class CBTipoPostoDTO
    {
        public int CodTipoPosto { get; set; }
        public string DesTipoPosto { get; set; }
        public string DesAcao { get; set; }
		public string DesCor { get; set; }
		public string FlgEntradaSaida { get; set; }
	}
}
