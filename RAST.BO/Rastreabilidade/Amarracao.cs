﻿using SEMP.BO;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.DTO;
using SEMP.Model.VO;
using SEMP.Model.VO.Rastreabilidade;
using System;
using System.Collections.Generic;
using System.Linq;
using SEMP.Model;
using System.Text;
using System.Threading.Tasks;

namespace RAST.BO.Rastreabilidade
{
	public class Amarracao
	{
		public static List<CBAmarraDTO> HistoricoAmarracaoPostoSimples(int codLinha, int numPosto)
		{
			return daoAmarracao.HistoricoAmarracaoPostoSimples(codLinha, numPosto);
		}

		public static List<CBAmarraDTO> HistoricoAmarracaoPostoLote(int codLinha, int numPosto)
		{
			return daoAmarracao.HistoricoAmarracaoPostoLote(codLinha, numPosto);
		}

		public static List<CBAmarraDTO> HistoricoAmarracaoPostoLoteAtual(int codLinha, int numPosto, string codModelo)
		{
			return daoAmarracao.HistoricoAmarracaoPostoLoteAtual(codLinha, numPosto, codModelo);
		}

		public static List<CBAmarraDTO> HistoricoPostoPassagem(int codLinha, int numPosto)
		{
			return daoAmarracao.HistoricoPostoPassagem(codLinha, numPosto);
		}

		public static List<viw_CBPerfilAmarraDTO> GetPerfilPosto(int codLinha, int numPosto, string codModelo, string codFab, string numAP)
		{
			return daoAmarracao.GetPerfilPosto(codLinha, numPosto, codModelo, codFab, numAP);
		}

		public static List<string> GetFamiliaLeitura(int codLinha, int numPosto) {

			return daoAmarracao.GetFamiliaLeitura(codLinha, numPosto);

		}

		public static bool VerificaSeItemFoiAmarrado(string NumECB)
		{
			return daoAmarracao.VerificaSeItemFoiAmarrado(NumECB);
		}

		public static bool VerificaSeItemTemDefeito(string NumECB)
		{
			return daoAmarracao.VerificaSeItemTemDefeito(NumECB);
		}

		public static CBMensagensDTO ValidarItem(string NumECB)
		{
			return daoAmarracao.ValidarItem(NumECB);
		}

		public static Mensagem ValidaLeituraLinhasAnteriores(DadosPosto posto, string numECB, string codDRT)
		{
			return daoAmarracao.ValidaLeituraLinhasAnteriores(posto, numECB, codDRT);
		}

		public static Mensagem InserirAmarracao(List<CBAmarraDTO> listaAmarra, string CodDRT, DadosPosto posto)
		{
			Mensagem retorno = new Mensagem();

			try
			{
				//if (daoAmarracao.InserirAmarracao(listaAmarra, CodDRT, posto))
				if (daoAmarracao.InserirAmarracaoProcedure(listaAmarra, CodDRT))
				{
					retorno = new Mensagem
					{
						CodMensagem = "R0001",
						DesMensagem = "LEITURA OK. LEIA PRÓXIMO.",
						DesCor = "LIME"
					};
					//daoMensagem.ObterMensagem(Constantes.RAST_OK);
				}
				else
				{
					retorno = retorno = new Mensagem
					{
						CodMensagem = "E0002",
						DesMensagem = "ERRO NO BANCO DE DADOS",
						DesCor = "RED"
					};
					//daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				}
			}
			catch (Exception ex)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				retorno.StackErro = ex.InnerException.Message;
			}

			return retorno;
		}

		public static Mensagem InserirAmarracaoInformatica(List<CBAmarraDTO> listaAmarra, string CodDRT)
		{
			Mensagem retorno = new Mensagem();

			try
			{
				if (daoAmarracao.InserirAmarracaoInformatica(listaAmarra, CodDRT))
				{
					retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);
				}
				else
				{
					retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				}
			}
			catch (Exception ex)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				retorno.StackErro = ex.InnerException.Message;
			}

			return retorno;
		}

		public static bool VerificaSeProdutoExisteInformatica(string NumSerie, string NumAP)
		{
			return daoAmarracao.VerificaSeProdutoExisteInformatica(NumSerie, NumAP);
		}

		public static bool VerificaSePlacaMaeExisteInformatica(string NumSerie)
		{
			return daoAmarracao.VerificaSePlacaMaeExisteInformatica(NumSerie);
		}

		public static bool ValidarSeProdutoInformaticaPassouSeparacao(string numSerie)
		{
			return daoAmarracao.ValidarSeProdutoInformaticaPassouSeparacao(numSerie);
		}

		public static List<CBAmarraDTO> RetornaItensAmarrados(int codLinha, int numPosto, string numSerie)
		{
			return daoAmarracao.RetornaItensAmarrados(codLinha, numPosto, numSerie);
		}
	}
}
