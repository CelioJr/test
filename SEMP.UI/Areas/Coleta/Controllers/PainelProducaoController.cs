﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json;
using RAST.BO.ApontaProd;
using RAST.BO.Rastreabilidade;
using RAST.BO.Relatorios;
using SEMP.Model.DTO;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using SEMP.Models;
using SEMP.Model.VO.CNQ;
using SEMP.Model.VO.Rastreabilidade;

namespace SEMP.Areas.Coleta.Controllers
{
	public class PainelProducaoController : Controller
	{
		//
		// GET: /Coleta/PainelProducao/

		public ActionResult Index()
		{
			return View();
		}

		public ActionResult PainelProducaoRecarregar()
		{
			var painel = PainelProducao.ObterPainelProducao();

			//var painel2 = PainelProducao.ObterPainelProducaoSTI();
			var mensagens = Json(PainelProducao.ObterMensagens(), JsonRequestBehavior.AllowGet);//PainelProducao.ObterMensagens();

			ViewBag.mensagens = JsonConvert.SerializeObject(mensagens.Data);
			ViewBag.painelsti = new List<BarrasPainel_STIDTO>(); //painel2;
			return View(painel);
		}

		public ActionResult PainelProducaoEntrada()
		{
			var painel = PainelProducao.ObterPainelProducao();
			var painelIMC = PainelProducao.ObterPainelProducaoIMC();

			ViewBag.painelIMC = painelIMC;

			return View(painel);
		}

		//
		// GET: /Coleta/PainelProducao/PainelProducaoLinha?fase=<fase>&codlinha=<codlinha>

		public ActionResult PainelProducaoLinha(string fase, int codlinha)
		{
			ViewBag.Fase = fase;
			ViewBag.CodLinha = codlinha;
			return View();
		}

		public ActionResult PainelProducaoLinhaRecarregar(int codlinha)
		{

			BarrasPainelDTO painel;
			List<ResumoProducao> resumo;

			var linha = PainelProducao.ObterCodLinha(codlinha);

			if (linha.CodLinhaT1.StartsWith("111"))
			{
				painel = PainelProducao.ObterPainelProducaoIMC(linha.CodLinhaT1).FirstOrDefault();
				resumo = PainelProducao.ObterResumoIMCLinha(linha.CodLinhaT1);
			}

			else
			{
				painel = PainelProducao.ObterPainelProducao(linha.CodLinhaT1).FirstOrDefault();
				resumo = PainelProducao.ObterResumoMFLinha(linha.CodLinhaT1);
			}

			ViewBag.resumo = resumo;

			return PartialView(painel);
		}

		//Painel da IAC
		public ActionResult PainelProducaoIACRecarregar(int codlinha)
		{
			DateTime dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			DateTime dFim = dIni.AddDays(1).AddMinutes(-1);

			BarrasPainelDTO painel;

			painel = PainelProducao.ObterPainelProducaoIAC(codlinha).FirstOrDefault();

			if (painel != null)
			{
				var ppm = PainelProducao.ObterPPM(dIni, dFim, codlinha, painel.CodModelo, painel.DesModelo, (decimal)painel.Embalados, painel.NumAP);
				var ppmReal = PainelProducao.ObterPPMReal(dIni, dFim, codlinha, painel.CodModelo, painel.DesModelo, (decimal)painel.Embalados, painel.NumAP);

				var parada = new List<v_paradasDTO>();
				//parada = PainelProducao.ObterParadasIDW(dIni, dFim, painel.CodLinha);

				ViewBag.parada = parada;
				ViewBag.ppm = ppm;
				ViewBag.ppmReal = ppmReal;
			}
			return PartialView(painel);
		}

		//Painel da Informática 
		public ActionResult PainelProducaoINFRecarregar(int codlinha)
		{
			var linha = PainelProducao.ObterCodLinha(codlinha);
			RealPrevisto realPrevisto = Passagem.ObterQuantidadeRealEPrevisto(codlinha);
			var horasTrab = PainelProducao.ObterHorasTrab(codlinha);
			ViewBag.intervalo = horasTrab.Intervalo ?? "I";
			ViewBag.desLinha = linha.DesLinha ?? "";
			ViewBag.codLinhaT1 = linha.CodLinhaT1 ?? "";
			ViewBag.realPrevisto = realPrevisto;
			return PartialView();
		}

		public ActionResult Painel(int? codlinha)
		{
			ViewBag.CodLinha = codlinha;
			return View();
		}

		public ActionResult Painel2(int codlinha)
		{
			ViewBag.CodLinha = codlinha;
			return View();
		}

		//
		// GET: /Coleta/PainelProducao/IMC
		public ActionResult IMC()
		{
			return View();
		}

		public ActionResult PainelProducaoIMCRecarregar()
		{
			var painel = PainelProducao.ObterPainelProducaoIMC();
			var painel2 = new List<BarrasPainel_STIDTO>();
			var mensagens = Json(PainelProducao.ObterMensagens(), JsonRequestBehavior.AllowGet);//PainelProducao.ObterMensagens();

			ViewBag.mensagens = JsonConvert.SerializeObject(mensagens.Data);
			ViewBag.painelsti = painel2;
			return View(painel);
		}

		public ActionResult IAC()
		{
			return View();
		}

		public ActionResult PainelProducaoIAC()
		{
			var painel = PainelProducao.ObterPainelProducaoIAC();
			var painel2 = new List<BarrasPainel_STIDTO>();
			var mensagens = Json(PainelProducao.ObterMensagens(), JsonRequestBehavior.AllowGet);//PainelProducao.ObterMensagens();

			ViewBag.mensagens = JsonConvert.SerializeObject(mensagens.Data);
			ViewBag.painelsti = painel2;
			return View(painel);
		}

		public JsonResult Mensagens()
		{

			return Json(PainelProducao.ObterMensagens(), JsonRequestBehavior.AllowGet);

		}

		public JsonResult MensagensLinha(string codLinha)
		{

			return Json(PainelProducao.ObterMensagensLinha(codLinha), JsonRequestBehavior.AllowGet);

		}

		public ActionResult PainelGestao()
		{

			var linhas = RelatorioProducaoMF.ObterLinhasMF().Select(x => new Menu
			{
				Codigo = 1,
				CodPermissao = "",
				Endereco = String.Format("javascript:AlteraLinha(\"{0}\", \"{1}\")", x.CodLinhaT1, x.CodLinha),
				Legenda = String.Format("{0} - {1}", x.CodLinha, x.DesLinha),
				Titulo = x.DesLinha
			}).ToList();

			ViewBag.linhas = linhas;
			return View();
		}

		#region Rotinas de acesso a dados
		public JsonResult ObterProducao(string datIni, string datFim, int codLinha, string codModelo)
		{
			var dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			var dFim = dIni.AddDays(1).AddMinutes(-1);

			if (!String.IsNullOrEmpty(datIni))
			{
				dIni = DateTime.Parse(datIni);
			}
			if (!String.IsNullOrEmpty(datFim))
			{
				dFim = DateTime.Parse(datFim);
			}

			try
			{
				var dados = ProducaoHoraAHoraBO.ObterProducaoData(dIni, dFim, codLinha, codModelo);

				return Json(dados, JsonRequestBehavior.AllowGet);
			}
			catch (Exception)
			{
				return Json(new RetornoGraficoGargalo(), JsonRequestBehavior.AllowGet);

			}
		}

		public JsonResult ObterIndice(int codLinha)
		{

			var dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			var dFim = dIni.AddDays(1).AddMinutes(-1);

			var defeitos = Defeito.ObterDefeitosRel(dIni, dFim).Where(x => x.FlgRevisado == 1 && !x.CodCausa.StartsWith("WX")).ToList();
			var producaohora = ProducaoHoraAHoraBO.ObterProducaoLinhaMinuto(dIni);
			if (producaohora != null)
			{
				producaohora = producaohora.GroupBy(x => x.Hora).Select(x => new ProducaoLinhaMinuto() { Hora = x.Key, QtdLeitura = x.Sum(y => y.QtdLeitura) }).ToList();
			}

			if (defeitos.Any())
			{
				var resumo = defeitos.GroupBy(x => x.DatEvento.Hour).Select(x => new DefeitosHora()
				{
					Hora = x.Key,
					QtdDefeitos = x.Count(),
					Cor = Color.Brown
				}).OrderBy(x => x.Hora).ToList();

				for (var i = 7; i <= DateTime.Now.Hour; i++)
				{
					if (resumo.FirstOrDefault(x => x.Hora == i) == null)
					{
						resumo.Add(new DefeitosHora()
						{
							Hora = i,
							QtdProduzido = 0,
							QtdDefeitos = 0,
							QtdAcumulado = 0
						});
					}
				}

				foreach (var hora in resumo)
				{
					var anteriores = resumo.Where(x => x.Hora <= hora.Hora).Sum(x => x.QtdDefeitos);

					var produzido = producaohora.FirstOrDefault(x => x.Hora == hora.Hora);
					var produzidoacum = producaohora.Where(x => x.Hora <= hora.Hora).Sum(x => x.QtdLeitura);

					hora.QtdProduzido = produzido != null ? produzido.QtdLeitura : 0;

					hora.QtdProduzidoAcum = produzidoacum;

					hora.QtdAcumulado = anteriores;
				}

				return Json(resumo, JsonRequestBehavior.AllowGet);
			}
			return Json(new List<DefeitosHora>(), JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterPainel(int codlinha)
		{

			BarrasPainelDTO painel;

			var linha = PainelProducao.ObterCodLinha(codlinha);

			if (linha.Setor.Equals("IMC"))
			{
				painel = PainelProducao.ObterPainelProducaoIMC(linha.CodLinhaT1).FirstOrDefault();
			}
			else if (linha.Setor.Equals("IAC"))
			{
				painel = PainelProducao.ObterPainelProducaoIAC(linha.CodLinhaT3).FirstOrDefault();
			}
			else
			{
				painel = PainelProducao.ObterPainelProducao(linha.CodLinhaT1).FirstOrDefault();
			}

			return Json(painel, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ObterProdutoMilhao(){

			var dados = Geral.ObterProdutoMilhao();

			return Json(dados, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ObterResumoPainel(int codlinha)
		{

			List<ResumoProducao> resumo;
			var datIni = DateTime.Parse(DateTime.Now.ToShortDateString());
			var datFim = datIni.AddDays(1).AddMinutes(-1);

			var linha = PainelProducao.ObterCodLinha(codlinha);

			if (linha.Setor.Equals("IMC"))
			{
				resumo = PainelProducao.ObterResumoIMCLinha(linha.CodLinhaT1);
			}
			else if (linha.Setor.Equals("IAC"))
			{
				resumo = PainelProducao.ObterResumoIACLinha(linha.CodLinhaT3);
			}
			else
			{
				resumo = PainelProducao.ObterResumoMFLinha(linha.CodLinhaT1);
			}

			if (!linha.Setor.Equals("IAC"))
			{
				var defeitos = Defeito.ObterDefeitosRelRef(datIni, datFim, codlinha);

				var dados = defeitos.GroupBy(x => x.CodModelo)
									.Select(x => new
									{
										CodModelo = x.Key,
										QtdDefeitos = x.Count(),
										QtdFF = x.Count(y => y.CodCausa.StartsWith("WX"))
									})
									.ToList();

				foreach (var item in resumo)
				{
					var def = dados.FirstOrDefault(y => y.CodModelo == item.CodModelo);
					if (def != null)
					{
						item.QtdDefeito = def.QtdDefeitos;
						item.QtdFalsaFalha = def.QtdFF;
					}

				}
			}
			else
			{
				foreach (var item in resumo)
				{
					var ppm = PainelProducao.ObterPPM(datIni, datFim, codlinha, item.CodModelo, item.DesModelo, (decimal)item.QtdRealizado, item.NumAP);
					var ppmReal = PainelProducao.ObterPPMReal(datIni, datFim, codlinha, item.CodModelo, item.DesModelo, (decimal)item.QtdRealizado, item.NumAP);

					item.QtdDefeito = (decimal)ppm.Calculo;
					item.QtdFalsaFalha = (decimal)ppmReal.Calculo;
				}
			}

			return Json(resumo, JsonRequestBehavior.AllowGet);
		}

		#endregion

	}
}
