﻿using DotNet.Highcharts;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using RAST.BO.CNQ;
using SEMP.Model.DTO;
using SEMP.Model.VO.CNQ;
using SEMP.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEMP.Areas.Outros.Controllers
{
    public class QualidadeController : Controller
    {
        //
        // GET: /Outros/Qualidade/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PPMGeral()
        {
            return View();
        }

        public ActionResult GetPPMGeral(string DatInicio, string DatFim, int Meta, string Tipo)
        {
            #region Propriedades

            DateTime dateStart = DateTime.Now;
            DateTime dateEnd = DateTime.Now;
            DateTime dataFimInicioAux = DateTime.Now;

            if (!string.IsNullOrWhiteSpace(DatInicio))
            {
                dateStart = DateTime.Parse("01/" + DatInicio);
            }

            if (!string.IsNullOrWhiteSpace(DatFim))
            {
                dataFimInicioAux = DateTime.Parse("01/" + DatFim);
                dateEnd = DateTime.Parse(dateEnd.Day + "/" + DatFim);
            }

            List<string> months = GetMonthsBetweenTwoDates(dateStart, dateEnd);
            List<string> weeks = GetWeeksByMonth(dateEnd);
            List<string> categorias = GetNameMonths(months);

            categorias.AddRange(GetNameWeeks(weeks));
            months.AddRange(weeks);

            #endregion

            List<object> listaObjectInspecao = null, listaMeta = null;

            double calculoInspecao = 0;

            List<Series> series = new List<Series>();

            listaObjectInspecao = new List<object>();
            listaMeta = new List<object>();

            for (int i = 0; i < months.Count; i++)
            {
                var dataExplode = months[i].Split('|');
                var dataInicial = DateTime.Parse(dataExplode[0]);
                var dataFinal = DateTime.Parse(dataExplode[1]);

                int quantidadeDefeitoInspecao = QualidadeBO.GetQuantidadeDefeitoIAC(dataInicial, dataFinal, Tipo);
                int quantidadeProduzidaInspecao = QualidadeBO.GetQuantidadeProduzidaIAC(dataInicial, dataFinal, Tipo);
                int quantidadePontosGeral = QualidadeBO.GetQuantidadeDePontosGeral(dataInicial, dataFinal, Tipo);

                calculoInspecao = quantidadeProduzidaInspecao > 0 ? (double)quantidadeDefeitoInspecao / (double)(quantidadeProduzidaInspecao * quantidadePontosGeral) : 0;

                listaObjectInspecao.Add(Math.Round((calculoInspecao) * 1000000, 1));
                listaMeta.Add((double)Meta);
            }

            series.Add(new Series
            {
                Name = "PPM",
                Color = ColorTranslator.FromHtml("#6a95c7"),
                Data = new Data(listaObjectInspecao.ToArray())
            });

            series.Add(new Series
            {
                Name = "META",
                Color = ColorTranslator.FromHtml("#9bbb59"),
                Data = new Data(listaMeta.ToArray()),
                PlotOptionsLine = new PlotOptionsLine
                {
                    DataLabels = new PlotOptionsLineDataLabels {
                        Enabled = false,
                        Style = "color: '#ffffff'"
                    }
                }
            });

            Highcharts chart = Grafico.CombineGraph("PPM IAC GERAL", categorias, series, "chartIndice", 0, 0, "", "", false);
            //ViewBag.TableIndiceGeral = GetTableGeral(series, categorias);

            return PartialView(chart);
        }

        public ActionResult PPMLinhaGeral()
        {
            return View();
        }

        public ActionResult GetPPMLinhaGeral(string DatInicio, string DatFim, int Meta)
        {
            #region Propriedades

            DateTime dateStart = DateTime.Now;
            DateTime dateEnd = DateTime.Now;
            DateTime dataFimInicioAux = DateTime.Now;

            if (!string.IsNullOrWhiteSpace(DatInicio))
            {
                dateStart = DateTime.Parse("01/" + DatInicio);
            }

            if (!string.IsNullOrWhiteSpace(DatFim))
            {
                dataFimInicioAux = DateTime.Parse("01/" + DatFim);
                dateEnd = DateTime.Parse(dateEnd.Day + "/" + DatFim);
            }

            List<string> months = GetMonthsBetweenTwoDates(dateStart, dateEnd);
            List<string> weeks = GetWeeksByMonth(dateEnd);
            List<string> categorias = GetNameMonths(months);

            categorias.AddRange(GetNameWeeks(weeks));
            months.AddRange(weeks);

            #endregion

            List<CBLinhaDTO> linhas = RAST.BO.Rastreabilidade.Geral.ObterLinhaPorFabrica("IAC").OrderBy(o => o.DesLinha).ToList();

            List<GraficoLinhaIAC> graficosLinha = new List<GraficoLinhaIAC>();

            List<Series> series = new List<Series>();

            List<object> listaObject = null;
            List<object> listaMeta = null;

            double calculo = 0;

            for (int i = 0; i < months.Count; i++)
            {
                var dataExplode = months[i].Split('|');
                var dataInicial = DateTime.Parse(dataExplode[0]);
                var dataFinal = DateTime.Parse(dataExplode[1]);

                listaObject = new List<object>();
                listaMeta = new List<object>();

                foreach (var linha in linhas)
                {
                    int quantidadePontos = QualidadeBO.GetQuantidadeDePontos(dataInicial, dataFinal, linha.CodLinha);
                    int quantidadeDefeito = QualidadeBO.GetQuantidadeDefeitoIACPorLinha(dataInicial, dataFinal, linha.CodLinha);
                    int quantidadeProduzida = QualidadeBO.GetQuantidadeProduzidaIACPorLinha(dataInicial, dataFinal, linha.CodLinha);

                    calculo = quantidadeProduzida > 0 ? (double)(quantidadeDefeito / (double)(quantidadeProduzida * quantidadePontos)) : 0;

                    listaObject.Add(Math.Round((double)(calculo * 1000000), 1));
                    listaMeta.Add((double)Meta);
                }

                series.Add(new Series
                {
                    Name = categorias[i],
                    Type = ChartTypes.Column,
                    Data = new Data(listaObject.ToArray())
                });
            }

            series.Add(new Series
            {
                Name = "META",
                Data = new Data(listaMeta.ToArray()),
                PlotOptionsLine = new PlotOptionsLine
                {
                    DataLabels = new PlotOptionsLineDataLabels
                    {
                        Enabled = false,
                        Style = "color: '#ffffff'"
                    }
                }
            });

            var categories = linhas.Select(s => string.Format("{0}", s.DesLinha)).ToList();

            Highcharts grafico = Grafico.CombineGraph("PPM IAC POR LINHA", categories.ToList(), series, "PPMIACPORLINHA", 0, 0, "", "", false);

            return PartialView(grafico);
        }

        public ActionResult DefeitosOrigemIAC()
        {
            return View();
        }

        public ActionResult GetDefeitosOrigemIAC(string DatInicio, string DatFim, string Fase, string Familia)
        {
            DateTime dateStart = DateTime.Now;
            DateTime dateEnd = DateTime.Now;
            DateTime dataFimInicioAux = DateTime.Now;

            string DescricaoFamilia = string.Empty;

            if (Familia == "FEC")
            {
                DescricaoFamilia = "MF";
            }
            else if (Familia == "AUD")
            {
                DescricaoFamilia = "ÁUDIO";
            }
            else if (Familia == "TAB")
            {
                DescricaoFamilia = "INFO";
            }
            else
            {
                DescricaoFamilia = Familia;
            }

            if (!string.IsNullOrWhiteSpace(DatInicio))
            {
                dateStart = DateTime.Parse("01/" + DatInicio);
            }

            if (!string.IsNullOrWhiteSpace(DatFim))
            {
                dataFimInicioAux = DateTime.Parse("01/" + DatFim);
                dateEnd = DateTime.Parse(dateEnd.Day + "/" + DatFim);
            }

            List<string> months = GetMonthsBetweenTwoDates(dateStart, dateEnd);
            List<string> weeks = GetWeeksByMonth(dateEnd);
            List<string> categorias = GetNameMonths(months);

            categorias.AddRange(GetNameWeeks(weeks));
            months.AddRange(weeks);


            #region GERAL IMC

            List<object> listaIMC = GetDefeitos(Fase, Familia, months);

            Highcharts chartsIMC = Grafico.CombineGraph(
                Fase == "FEC" ? "MF-" + DescricaoFamilia + ": DEFEITOS DE ORIGEM IAC" :
                Fase + "-" + DescricaoFamilia + ": DEFEITOS DE ORIGEM IAC", categorias, new List<Series>()
            {
                new Series
                    {
                        Name = "% TOTAL",
                        Color = ColorTranslator.FromHtml("#89A54E"),
                        Data = new Data(listaIMC.ToArray())
                    }
            }, "chartIMC", null, 300);

            ViewBag.chartsIMC = chartsIMC;
            ViewBag.TableMF = GetTableGeral(new List<Series>() { new Series
                    {
                        Name = "% TOTAL",
                        Color = ColorTranslator.FromHtml("#89A54E"),
                        Data = new Data(listaIMC.ToArray())
                    } }, categorias);

            #endregion

            #region MAIORES DEFEITOS

            //Maiores Defeitos
            dateEnd = dateEnd.AddDays(1).AddSeconds(-1);
            List<MaioresDefeitos> listaMaioresDefeitos = QualidadeBO.GetMaioresDefeitos(dateStart, dateEnd, Fase, Familia);

            Highcharts chartMaioresDefeitos = GetSeriesMaioresDefeitos(listaMaioresDefeitos, months, categorias, Familia, Fase, null, 300);
            ViewBag.chartMaioresDefeitos = chartMaioresDefeitos;
            ViewBag.TableMaioresDefeitos = GetTableMaioresDefeitos(listaMaioresDefeitos, months, categorias, Fase, Familia);

            #endregion

            return PartialView();
        }

        public ActionResult MaioresDefeitos()
        {
            return View();
        }

        public ActionResult GetMaioresDefeitos(string DatInicio, string DatFim, string Fase, string Familia)
        {
            DateTime dateStart = DateTime.Now;
            DateTime dateEnd = DateTime.Now;
            DateTime dataFimInicioAux = DateTime.Now;

            string DescricaoFamilia = string.Empty;

            if (Familia == "FEC")
            {
                DescricaoFamilia = "MF";
            }
            else if (Familia == "AUD")
            {
                DescricaoFamilia = "ÁUDIO";
            }
            else if (Familia == "TAB")
            {
                DescricaoFamilia = "INFO";
            }
            else
            {
                DescricaoFamilia = Familia;
            }

            if (!string.IsNullOrWhiteSpace(DatInicio))
            {
                dateStart = DateTime.Parse("01/" + DatInicio);
            }

            if (!string.IsNullOrWhiteSpace(DatFim))
            {
                dataFimInicioAux = DateTime.Parse("01/" + DatFim);
                dateEnd = DateTime.Parse(dateEnd.Day + "/" + DatFim);
            }

            List<string> months = GetMonthsBetweenTwoDates(dateStart, dateEnd);
            List<string> weeks = GetWeeksByMonth(dateEnd);
            List<string> categorias = GetNameMonths(months);

            categorias.AddRange(GetNameWeeks(weeks));
            months.AddRange(weeks);

            //Maiores Defeitos
            dateEnd = dateEnd.AddDays(1).AddSeconds(-1);
            List<MaioresDefeitos> listaMaioresDefeitos = QualidadeBO.GetMaioresDefeitos(dateStart, dateEnd, Fase, Familia);

            //Maiores Defeitos com Posição
            Highcharts chartMaioresDefeitosPosicoes = GetSeriesMaioresDefeitosPosicoes(listaMaioresDefeitos, months, categorias, Familia, Fase, null, 400);
            ViewBag.Posicoes = chartMaioresDefeitosPosicoes;
            ViewBag.TablePosicoes = GetTablePosicoes(listaMaioresDefeitos, months, categorias);

            return PartialView();
        }

        public ActionResult LinhasIAC()
        {
            List<CBLinhaDTO> linhas = RAST.BO.Rastreabilidade.Geral.ObterLinhaPorFabrica("IAC").OrderBy(o => o.DesLinha).ToList();

            ViewBag.Linhas = linhas;

            return View();
        }

        public ActionResult GetLinhasIAC(string DatInicio, string DatFim, int Linha, int Meta)
        {
            #region Propriedades

            DateTime dateStart = DateTime.Now;
            DateTime dateEnd = DateTime.Now;
            DateTime dataFimInicioAux = DateTime.Now;

            if (!string.IsNullOrWhiteSpace(DatInicio))
            {
                dateStart = DateTime.Parse("01/" + DatInicio);
            }

            if (!string.IsNullOrWhiteSpace(DatFim))
            {
                dataFimInicioAux = DateTime.Parse("01/" + DatFim);
                dateEnd = DateTime.Parse(dateEnd.Day + "/" + DatFim);
            }

            List<string> months = GetMonthsBetweenTwoDates(dateStart, dateEnd);
            List<string> weeks = GetWeeksByMonth(dateEnd);
            List<string> categorias = GetNameMonths(months);

            categorias.AddRange(GetNameWeeks(weeks));
            months.AddRange(weeks);

            #endregion

            List<CBLinhaDTO> linhas = null;

            List<Highcharts> listaCharts = new List<Highcharts>();
            List<GraficoLinhaIAC> graficosLinha = new List<GraficoLinhaIAC>();

            //Maiores Defeitos
            dateEnd = dateEnd.AddDays(1).AddSeconds(-1);
            List<MaioresDefeitos> listaMaioresDefeitos = null;

            if (Linha == 0)
            {
                linhas = RAST.BO.Rastreabilidade.Geral.ObterLinhaPorFabrica("IAC").OrderBy(o => o.DesLinha).ToList();
                foreach (var item in linhas)
                {
                    listaMaioresDefeitos = QualidadeBO.GetMaioresDefeitosIACPorLinha(dateStart, dateEnd, item.CodLinha);

                    graficosLinha.Add(new GraficoLinhaIAC
                    {
                        Grafico = GetSeriesIndiceDefeitosIACPorLinha(months, categorias, item),
                        TabelaDefeito = GetTablePosicoes(listaMaioresDefeitos, months, categorias)
                    });
                }
            }
            else
            {
                linhas = new List<CBLinhaDTO>();
                linhas.Add(RAST.BO.Rastreabilidade.Geral.ObterLinha(Linha));

                foreach (var item in linhas)
                {
                    //listaCharts.Add(GetSeriesIndiceDefeitosIACPorLinha(months, categorias, item));

                    listaMaioresDefeitos = QualidadeBO.GetMaioresDefeitosIACPorLinha(dateStart, dateEnd, item.CodLinha);

                    graficosLinha.Add(new GraficoLinhaIAC
                    {
                        Grafico = GetSeriesIndiceDefeitosIACPorLinha(months, categorias, item),
                        TabelaDefeito = GetTablePosicoes(listaMaioresDefeitos, months, categorias)
                    });
                }
            }

            ViewBag.ListaCharts = graficosLinha;

            return PartialView();
        }

        public ActionResult IAC()
        {
            return View();
        }

        public ActionResult GetIAC(string DatInicio, string DatFim, string Tipo)
        {
            DateTime dateStart = DateTime.Now;
            DateTime dateEnd = DateTime.Now;
            DateTime dataFimInicioAux = DateTime.Now;

            if (!string.IsNullOrWhiteSpace(DatInicio))
            {
                dateStart = DateTime.Parse("01/" + DatInicio);
            }

            if (!string.IsNullOrWhiteSpace(DatFim))
            {
                dataFimInicioAux = DateTime.Parse("01/" + DatFim);
                dateEnd = DateTime.Parse(dateEnd.Day + "/" + DatFim);
            }

            List<string> months = GetMonthsBetweenTwoDates(dateStart, dateEnd);
            List<string> weeks = GetWeeksByMonth(dateEnd);
            List<string> categorias = GetNameMonths(months);

            categorias.AddRange(GetNameWeeks(weeks));
            months.AddRange(weeks);

            #region IAC GERAL

            Highcharts chartIndiceGeral = GetSeriesIndiceDefeitosInternoIAC(months, categorias, null, 300);
            ViewBag.chartIndiceGeral = chartIndiceGeral;

            #endregion

            #region MAIORES DEFEITOS

            //Maiores Defeitos
            dateEnd = dateEnd.AddDays(1).AddSeconds(-1);
            List<MaioresDefeitos> listaMaioresDefeitos = QualidadeBO.GetMaioresDefeitosIACINSPECAO(dateStart, dateEnd, Tipo);

            Highcharts chartMaioresDefeitos = GetSeriesMaioresDefeitosIAC(listaMaioresDefeitos, months, categorias, Tipo, null, 300);
            ViewBag.chartMaioresDefeitos = chartMaioresDefeitos;
            ViewBag.TableMaioresDefeitos = GetTableMaioresDefeitosIAC(listaMaioresDefeitos, months, categorias, Tipo);

            ////Maiores Defeitos com Posição
            //Highcharts chartMaioresDefeitosPosicoes = GetSeriesMaioresDefeitosPosicoes(listaMaioresDefeitos, months, categorias, "", "IAC");
            //ViewBag.Posicoes = chartMaioresDefeitosPosicoes;
            //ViewBag.TablePosicoes = GetTablePosicoes(listaMaioresDefeitos, months, categorias);

            #endregion

            return PartialView();
        }

        public ActionResult IMC()
        {
            return View();
        }

        public ActionResult GetIMC(string DatInicio, string DatFim, string Familia)
        {
            DateTime dateStart = DateTime.Now;
            DateTime dateEnd = DateTime.Now;
            DateTime dataFimInicioAux = DateTime.Now;

            string DescricaoFamilia = string.Empty;

            if (Familia == "FEC")
            {
                DescricaoFamilia = "MF";
            }
            else if (Familia == "AUD")
            {
                DescricaoFamilia = "ÁUDIO";
            }
            else if (Familia == "TAB")
            {
                DescricaoFamilia = "INFO";
            }
            else
            {
                DescricaoFamilia = Familia;
            }

            if (!string.IsNullOrWhiteSpace(DatInicio))
            {
                dateStart = DateTime.Parse("01/" + DatInicio);
            }

            if (!string.IsNullOrWhiteSpace(DatFim))
            {
                dataFimInicioAux = DateTime.Parse("01/" + DatFim);
                dateEnd = DateTime.Parse(dateEnd.Day + "/" + DatFim);
            }

            List<string> months = GetMonthsBetweenTwoDates(dateStart, dateEnd);
            List<string> weeks = GetWeeksByMonth(dateEnd);
            List<string> categorias = GetNameMonths(months);

            categorias.AddRange(GetNameWeeks(weeks));
            months.AddRange(weeks);

            #region GERAL IMC

            List<object> listaIMC = GetDefeitos("IMC", Familia, months);

            Highcharts chartsIMC = Grafico.CombineGraph("IMC-" + DescricaoFamilia + ": DEFEITOS DE ORIGEM IAC", categorias, new List<Series>()
            {
                new Series
                    {
                        Name = "% TOTAL",
                        Color = ColorTranslator.FromHtml("#89A54E"),
                        Data = new Data(listaIMC.ToArray())
                    }
            }, "chartIMC", null, 300);

            ViewBag.chartsIMC = chartsIMC;
            ViewBag.TableMF = GetTableGeral(new List<Series>() { new Series
                    {
                        Name = "% TOTAL",
                        Color = ColorTranslator.FromHtml("#89A54E"),
                        Data = new Data(listaIMC.ToArray())
                    } }, categorias);

            #endregion

            #region IMC FAMILIA

            List<object> listaIndiceM = GetIndices("IMC", Familia, "M", months);
            List<object> listaIndiceO = GetIndices("IMC", Familia, "O", months);
            List<object> listaIndiceP = GetIndices("IMC", Familia, "P", months);
            List<object> listaGeral = GetIndices("IMC", Familia, "", months, true);

            List<Series> series = new List<Series>();

            series.Add(new Series
            {
                Name = "MATERIAL",
                Type = ChartTypes.Column,
                YAxis = "1",
                Data = new Data(listaIndiceM.ToArray())
            });

            series.Add(new Series
            {
                Name = "MOB",
                Type = ChartTypes.Column,
                YAxis = "1",
                Data = new Data(listaIndiceO.ToArray())
            });

            series.Add(new Series
            {
                Name = "MÁQUINA",
                Type = ChartTypes.Column,
                YAxis = "1",
                Data = new Data(listaIndiceP.ToArray())
            });

            series.Add(new Series
            {
                Name = "TOTAL " + Familia,
                Color = ColorTranslator.FromHtml("#89A54E"),
                Data = new Data(listaGeral.ToArray())
            });

            Highcharts charts = Grafico.CombineGraph("IMC-" + DescricaoFamilia + " GERAL", categorias, series, "chart", null, 300);
            ViewBag.TableDefeitoGeral = GetTableGeral(series, categorias);


            #endregion

            #region MAIORES DEFEITOS

            //Maiores Defeitos
            dateEnd = dateEnd.AddDays(1).AddSeconds(-1);
            List<MaioresDefeitos> listaMaioresDefeitos = QualidadeBO.GetMaioresDefeitos(dateStart, dateEnd, "IMC", Familia);

            Highcharts chartMaioresDefeitos = GetSeriesMaioresDefeitos(listaMaioresDefeitos, months, categorias, Familia, "IMC", null, 300);
            ViewBag.chartMaioresDefeitos = chartMaioresDefeitos;
            ViewBag.TableMaioresDefeitos = GetTableMaioresDefeitos(listaMaioresDefeitos, months, categorias, "IMC", Familia);

            //Maiores Defeitos com Posição
            Highcharts chartMaioresDefeitosPosicoes = GetSeriesMaioresDefeitosPosicoes(listaMaioresDefeitos, months, categorias, Familia, "IMC", null, 300);
            ViewBag.Posicoes = chartMaioresDefeitosPosicoes;
            ViewBag.TablePosicoes = GetTablePosicoes(listaMaioresDefeitos, months, categorias);

            #endregion

            return PartialView(charts);
        }

        public ActionResult MF()
        {
            return View();
        }

        public ActionResult GetMF(string DatInicio, string DatFim, string Familia)
        {
            DateTime dateStart = DateTime.Now;
            DateTime dateEnd = DateTime.Now;
            DateTime dataFimInicioAux = DateTime.Now;

            string DescricaoFamilia = string.Empty;

            if (Familia == "FEC")
            {
                DescricaoFamilia = "MF";
            }
            else if (Familia == "AUD")
            {
                DescricaoFamilia = "ÁUDIO";
            }
            else if (Familia == "TAB")
            {
                DescricaoFamilia = "INFO";
            }
            else
            {
                DescricaoFamilia = Familia;
            }

            if (!string.IsNullOrWhiteSpace(DatInicio))
            {
                dateStart = DateTime.Parse("01/" + DatInicio);
            }

            if (!string.IsNullOrWhiteSpace(DatFim))
            {
                dataFimInicioAux = DateTime.Parse("01/" + DatFim);
                dateEnd = DateTime.Parse(dateEnd.Day + "/" + DatFim);
            }

            List<string> months = GetMonthsBetweenTwoDates(dateStart, dateEnd);
            List<string> weeks = GetWeeksByMonth(dateEnd);
            List<string> categorias = GetNameMonths(months);

            categorias.AddRange(GetNameWeeks(weeks));
            months.AddRange(weeks);

            #region GERAL MF

            List<object> listaMF = GetDefeitos("FEC", Familia, months);

            Highcharts chartsMF = Grafico.CombineGraph("MF-" + DescricaoFamilia + ": DEFEITOS DE ORIGEM IAC", categorias, new List<Series>()
            {
                new Series
                    {
                        Name = "% TOTAL",
                        Color = ColorTranslator.FromHtml("#89A54E"),
                        Data = new Data(listaMF.ToArray())
                    }
            }, "chartMF", null, 300);

            ViewBag.chartsMF = chartsMF;
            ViewBag.TableMF = GetTableGeral(new List<Series>() { new Series
                    {
                        Name = "% TOTAL",
                        Color = ColorTranslator.FromHtml("#89A54E"),
                        Data = new Data(listaMF.ToArray())
                    } }, categorias);

            #endregion

            #region MF FAMILIA

            List<object> listaIndiceM = GetIndices("FEC", Familia, "M", months);
            List<object> listaIndiceO = GetIndices("FEC", Familia, "O", months);
            List<object> listaIndiceP = GetIndices("FEC", Familia, "P", months);
            List<object> listaGeral = GetIndices("FEC", Familia, "", months, true);

            List<Series> series = new List<Series>();

            series.Add(new Series
            {
                Name = "MATERIAL",
                Type = ChartTypes.Column,
                YAxis = "1",
                Data = new Data(listaIndiceM.ToArray())
            });

            series.Add(new Series
            {
                Name = "MOB",
                Type = ChartTypes.Column,
                YAxis = "1",
                Data = new Data(listaIndiceO.ToArray())
            });

            series.Add(new Series
            {
                Name = "MÁQUINA",
                Type = ChartTypes.Column,
                YAxis = "1",
                Data = new Data(listaIndiceP.ToArray())
            });

            series.Add(new Series
            {
                Name = "TOTAL MF",
                Color = ColorTranslator.FromHtml("#89A54E"),
                Data = new Data(listaGeral.ToArray())
            });

            Highcharts charts = Grafico.CombineGraph("MF-" + DescricaoFamilia + " DEFEITOS GERAL", categorias, series, "chartMFFamilia", null, 300);
            ViewBag.TableDefeitoGeral = GetTableGeral(series, categorias);

            #endregion

            #region MAIORES DEFEITOS

            //Maiores Defeitos
            dateEnd = dateEnd.AddDays(1).AddSeconds(-1);
            List<MaioresDefeitos> listaMaioresDefeitos = QualidadeBO.GetMaioresDefeitos(dateStart, dateEnd, "FEC", Familia);

            Highcharts chartMaioresDefeitos = GetSeriesMaioresDefeitos(listaMaioresDefeitos, months, categorias, Familia, "FEC", null, 300);
            ViewBag.chartMaioresDefeitos = chartMaioresDefeitos;
            ViewBag.TableMaioresDefeitos = GetTableMaioresDefeitos(listaMaioresDefeitos, months, categorias, "FEC", Familia);

            //Maiores Defeitos com Posição
            Highcharts chartMaioresDefeitosPosicoes = GetSeriesMaioresDefeitosPosicoes(listaMaioresDefeitos, months, categorias, Familia, "MF", null, 300);
            ViewBag.Posicoes = chartMaioresDefeitosPosicoes;
            ViewBag.TablePosicoes = GetTablePosicoes(listaMaioresDefeitos, months, categorias);

            #endregion

            return PartialView(charts);
        }

        public ActionResult Fases()
        {
            return View();
        }

        public ActionResult GetFases(string DatInicio, string DatFim)
        {
            DateTime dateStart = DateTime.Now;
            DateTime dateEnd = DateTime.Now;
            DateTime dataFimInicioAux = DateTime.Now;

            if (!string.IsNullOrWhiteSpace(DatInicio))
            {
                dateStart = DateTime.Parse("01/" + DatInicio);
            }

            if (!string.IsNullOrWhiteSpace(DatFim))
            {
                dataFimInicioAux = DateTime.Parse("01/" + DatFim);
                dateEnd = DateTime.Parse(dateEnd.Day + "/" + DatFim);
            }

            List<string> months = GetMonthsBetweenTwoDates(dateStart, dateEnd);
            List<string> weeks = GetWeeksByMonth(dateEnd);
            List<string> categorias = GetNameMonths(months);

            categorias.AddRange(GetNameWeeks(weeks));
            months.AddRange(weeks);


            #region IAC

            Highcharts chartIndiceGeralIAC = GetSeriesIndiceDefeitosInternoIAC(months, categorias, 650, 300);
            ViewBag.chartIndiceGeralIAC = chartIndiceGeralIAC;

            #endregion

            #region IMC

            List<object> listaIMCLCD = GetDefeitos("IMC", "LCD", months);
            Highcharts chartsIMCLCD = Grafico.CombineGraph("IMC-LCD: DEFEITOS DE ORIGEM IAC", categorias, new List<Series>()
            {
                new Series
                    {
                        Name = "% TOTAL",
                        Color = ColorTranslator.FromHtml("#89A54E"),
                        Data = new Data(listaIMCLCD.ToArray())
                    }
            }, "chartIMCLCD", 650, 300);
            ViewBag.chartsIMCLCD = chartsIMCLCD;

            List<object> listaIMCINFO = GetDefeitos("IMC", "INF", months);
            Highcharts chartsIMCINFOR = Grafico.CombineGraph("IMC-INFORMÁTICA: DEFEITOS DE ORIGEM IAC", categorias, new List<Series>()
            {
                new Series
                    {
                        Name = "% TOTAL",
                        Color = ColorTranslator.FromHtml("#89A54E"),
                        Data = new Data(listaIMCINFO.ToArray())
                    }
            }, "chartIMCINFO", 650, 300);
            ViewBag.chartsIMCINFOR = chartsIMCINFOR;

            List<object> listaIMCAUDIO = GetDefeitos("IMC", "AUD", months);
            Highcharts chartsIMCAUDIO = Grafico.CombineGraph("IMC-ÁUDIO: DEFEITOS DE ORIGEM IAC", categorias, new List<Series>()
            {
                new Series
                    {
                        Name = "% TOTAL",
                        Color = ColorTranslator.FromHtml("#89A54E"),
                        Data = new Data(listaIMCAUDIO.ToArray())
                    }
            }, "chartIMCAUDIO", 650, 300);
            ViewBag.chartsIMCAUDIO = chartsIMCAUDIO;

            List<object> listaIMCDVD = GetDefeitos("IMC", "DVD", months);
            Highcharts chartsIMCDVD = Grafico.CombineGraph("IMC-DVD: DEFEITOS DE ORIGEM IAC", categorias, new List<Series>()
            {
                new Series
                    {
                        Name = "% TOTAL",
                        Color = ColorTranslator.FromHtml("#89A54E"),
                        Data = new Data(listaIMCDVD.ToArray())
                    }
            }, "chartIMCDVD", 650, 300);
            ViewBag.chartsIMCDVD = chartsIMCDVD;


            #endregion

            #region MF

            List<object> listaMFLCD = GetDefeitos("FEC", "LCD", months);
            Highcharts chartsMFLCD = Grafico.CombineGraph("MF-LCD: DEFEITOS DE ORIGEM IAC", categorias, new List<Series>()
            {
                new Series
                    {
                        Name = "% TOTAL",
                        Color = ColorTranslator.FromHtml("#89A54E"),
                        Data = new Data(listaMFLCD.ToArray())
                    }
            }, "chartMFLCD", 650, 300);
            ViewBag.chartsMFLCD = chartsMFLCD;

            List<object> listaMFAUDIO = GetDefeitos("FEC", "AUD", months);
            Highcharts chartsMFAUDIO = Grafico.CombineGraph("MF-ÁUDIO: DEFEITOS DE ORIGEM IAC", categorias, new List<Series>()
            {
                new Series
                    {
                        Name = "% TOTAL",
                        Color = ColorTranslator.FromHtml("#89A54E"),
                        Data = new Data(listaMFAUDIO.ToArray())
                    }
            }, "chartMFAUDIO", 650, 300);
            ViewBag.chartsMFAUDIO = chartsMFAUDIO;

            List<object> listaMFDVD = GetDefeitos("FEC", "DVD", months);
            Highcharts chartsMFDVD = Grafico.CombineGraph("MF-DVD: DEFEITOS DE ORIGEM IAC", categorias, new List<Series>()
            {
                new Series
                    {
                        Name = "% TOTAL",
                        Color = ColorTranslator.FromHtml("#89A54E"),
                        Data = new Data(listaMFDVD.ToArray())
                    }
            }, "chartMFDVD", 650, 300);
            ViewBag.chartsMFDVD = chartsMFDVD;

            #endregion

            return PartialView();
        }

        public ActionResult Geral(string DatInicio, string DatFim)
        {
            DateTime dateStart = DateTime.Now;
            DateTime dateEnd = DateTime.Now;
            DateTime dataFimInicioAux = DateTime.Now;

            if (!string.IsNullOrWhiteSpace(DatInicio))
            {
                dateStart = DateTime.Parse("01/" + DatInicio);
            }

            if (!string.IsNullOrWhiteSpace(DatFim))
            {
                dataFimInicioAux = DateTime.Parse("01/" + DatFim);
                dateEnd = DateTime.Parse(dateEnd.Day + "/" + DatFim);
            }

            List<string> months = GetMonthsBetweenTwoDates(dateStart, dateEnd);
            List<string> weeks = GetWeeksByMonth(dateEnd);
            List<string> categorias = GetNameMonths(months);

            categorias.AddRange(GetNameWeeks(weeks));
            months.AddRange(weeks);

            List<object> listaMFLCD = GetDefeitos("FEC", "LCD", months);
            List<object> listaMFAudio = GetDefeitos("FEC", "AUD", months);

            List<object> listaIMCLCD = GetDefeitos("IMC", "LCD", months);
            List<object> listaIMCAudio = GetDefeitos("IMC", "AUD", months);
            List<object> listaIMCINFO = GetDefeitos("IMC", "INF", months);

            List<object> listaIACINSPECAO = GetDefeitos("IAC", "INSPECAO", months);
            List<object> listaIACICT = GetDefeitos("IAC", "ICT", months);

            List<object> listaGeralIAC = GetDefeitosGeralIAC(months);

            List<Series> series = new List<Series>();

            series.Add(new Series
            {
                Name = "IAC INSPEÇÃO",
                Type = ChartTypes.Column,
                YAxis = "1",
                Data = new Data(listaIACINSPECAO.ToArray())
            });

            series.Add(new Series
            {
                Name = "IAC ICT",
                Type = ChartTypes.Column,
                YAxis = "1",
                Data = new Data(listaIACICT.ToArray())
            });

            series.Add(new Series
            {
                Name = "MF LCD",
                Type = ChartTypes.Column,
                YAxis = "1",
                Data = new Data(listaMFLCD.ToArray())
            });

            series.Add(new Series
            {
                Name = "MF ÁUDIO",
                Type = ChartTypes.Column,
                YAxis = "1",
                Data = new Data(listaMFAudio.ToArray())
            });

            series.Add(new Series
            {
                Name = "IMC LCD",
                Type = ChartTypes.Column,
                YAxis = "1",
                Data = new Data(listaIMCLCD.ToArray())
            });

            series.Add(new Series
            {
                Name = "IMC ÁUDIO",
                Type = ChartTypes.Column,
                YAxis = "1",
                Data = new Data(listaIMCAudio.ToArray())
            });

            series.Add(new Series
            {
                Name = "IMC INFO",
                Type = ChartTypes.Column,
                YAxis = "1",
                Data = new Data(listaIMCINFO.ToArray())
            });

            series.Add(new Series
            {
                Name = "GERAL IAC",
                Color = ColorTranslator.FromHtml("#89A54E"),
                Data = new Data(listaGeralIAC.ToArray())
            });

            Highcharts charts = Grafico.CombineGraph("DEFEITOS GERAL IAC", categorias, series, "chartGeral", 0, 0);
            ViewBag.TableGeral = GetTableGeral(series, categorias);

            return PartialView(charts);
        }

        private List<object> GetIndices(string fase, string familia, string tipo, List<string> periodos, bool geral = false)
        {
            List<object> listaObject = new List<object>();

            if (!geral)
            {
                for (int i = 0; i < periodos.Count; i++)
                {
                    var item = QualidadeBO.GetIndices(periodos[i], fase, familia, tipo);
                    listaObject.Add(Math.Round(item.Calculo, 2));
                }
            }
            else
            {
                for (int i = 0; i < periodos.Count; i++)
                {
                    var item = QualidadeBO.GetIndiceGeral(periodos[i], fase, familia, tipo);
                    listaObject.Add(Math.Round(item.Calculo, 2));
                }
            }

            return listaObject;
        }

        private List<object> GetDefeitos(string fase, string familia, List<string> periodos)
        {
            //List<DefeitosGeralIAC> lista = new List<DefeitosGeralIAC>();
            List<object> listaObject = new List<object>();

            if (fase == "IAC")
            {
                double calculo = 0;
                for (int i = 0; i < periodos.Count; i++)
                {
                    var dataExplode = periodos[i].Split('|');
                    var dataInicial = DateTime.Parse(dataExplode[0]);
                    var dataFinal = DateTime.Parse(dataExplode[1]);

                    int quantidadeDefeito = QualidadeBO.GetQuantidadeDefeitoIAC(dataInicial, dataFinal, familia);
                    int quantidadeProduzida = QualidadeBO.GetQuantidadeProduzidaIAC(dataInicial, dataFinal, familia);
                    calculo = quantidadeProduzida > 0 ? ((double)quantidadeDefeito / (double)quantidadeProduzida) * 100 : 0;
                    listaObject.Add(Math.Round(calculo, 2));
                }
            }
            else
            {
                for (int i = 0; i < periodos.Count; i++)
                {
                    var item = QualidadeBO.GetDefeitosPorFase(periodos[i], fase, familia);
                    listaObject.Add(Math.Round(item.Calculo, 2));
                    //lista.Add(item);
                }
            }

            return listaObject;
        }

        private List<object> GetDefeitosGeralIAC(List<string> periodos)
        {
            //List<DefeitosGeralIAC> lista = new List<DefeitosGeralIAC>();
            List<object> listaObject = new List<object>();

            for (int i = 0; i < periodos.Count; i++)
            {
                var item = QualidadeBO.GetDefeitoGeralIAC(periodos[i]);
                listaObject.Add(Math.Round(item.Calculo, 2));
                //lista.Add(QualidadeBO.GetDefeitoGeralIAC(periodos[i]));
            }

            return listaObject;
        }

        private List<string> GetMonthsBetweenTwoDates(DateTime dateStart, DateTime dateEnd)
        {
            int minDay = dateStart.Day;
            int minMonth = dateStart.Month;
            int minYear = dateStart.Year;
            int maxDay = dateEnd.Day;
            int maxMonth = dateEnd.Month;
            int maxYear = dateEnd.Year;
            int maxMonthAux = 0;

            DateTime dateAux = new DateTime(dateEnd.Year, dateEnd.Month, 1);

            List<string> listaDatas = new List<string>();

            for (int j = minYear; j <= maxYear; j++)
            {
                if (j == maxYear)
                {
                    maxMonthAux = maxMonth;
                }
                else
                {
                    maxMonthAux = 12;
                }

                for (int i = 1; i <= maxMonthAux; i++)
                {
                    int _day = 1;
                    int _month = i;
                    DateTime tempDate = new DateTime(j, _month, _day);

                    if ((tempDate >= dateStart) && (tempDate < dateEnd))
                    {
                        if (tempDate < dateAux)
                        {
                            listaDatas.Add(tempDate + "|" + tempDate.AddMonths(1).AddSeconds(-1));
                        }
                    }
                }
            }

            return listaDatas;
        }

        private List<string> GetWeeksByMonth(DateTime dateEnd)
        {
            var calendar = CultureInfo.CurrentCulture.Calendar;
            var firstDayOfWeek = CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek;

            int year = dateEnd.Year, month = dateEnd.Month;

            var weekPeriodsAux = Enumerable.Range(1, calendar.GetDaysInMonth(year, month))
                      .Select(d =>
                      {
                          var date = new DateTime(year, month, d);
                          var weekNumInYear = calendar.GetWeekOfYear(date, CalendarWeekRule.FirstDay, firstDayOfWeek);
                          return new { date, weekNumInYear };
                      })
                      .GroupBy(x => x.weekNumInYear)
                      .Select(x => new { DateFrom = x.First().date, To = x.Last().date })
                      .ToList();

            List<string> weekPeriods = new List<string>();

            for (int i = 0; i < weekPeriodsAux.Count; i++)
            {
                if ((i + 1) >= weekPeriodsAux.Count)
                {
                    if (weekPeriodsAux[i].To <= dateEnd)
                    {
                        weekPeriods.Add(weekPeriodsAux[i].DateFrom + "|" + dateEnd.AddDays(1).AddSeconds(-1));
                    }
                }
                else
                {
                    if (weekPeriodsAux[i].DateFrom <= dateEnd)
                    {
                        if (weekPeriodsAux[i + 1].DateFrom >= dateEnd)
                        {
                            weekPeriods.Add(weekPeriodsAux[i].DateFrom + "|" + dateEnd.AddDays(1).AddSeconds(-1));
                        }
                        else
                        {
                            weekPeriods.Add(weekPeriodsAux[i].DateFrom + "|" + weekPeriodsAux[i + 1].DateFrom.AddSeconds(-1));
                        }
                    }
                }
            }

            return weekPeriods;
        }

        private int GetWeeksInYear(DateTime date)
        {
            DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
            DateTime date1 = new DateTime(date.Year, date.Month, date.Day);
            Calendar cal = dfi.Calendar;

            return cal.GetWeekOfYear(date1, dfi.CalendarWeekRule, dfi.FirstDayOfWeek);
        }

        private List<string> GetNameMonths(List<string> horas)
        {
            List<string> retorno = new List<string>();
            System.Globalization.DateTimeFormatInfo mfi = new
            System.Globalization.DateTimeFormatInfo();

            foreach (var item in horas)
            {
                DateTime date = DateTime.Parse(item.Split('|')[0]);
                string strMonthName = GetNameMonthsBr(date.Date.Month).ToString();
                retorno.Add(strMonthName + "/" + date.ToString("yy"));
            }

            return retorno;
        }

        private string GetNameMonthsBr(int num)
        {
            string mes = string.Empty;

            switch (num)
            {
                case 1:
                    mes = "jan";
                    break;
                case 2:
                    mes = "fev";
                    break;
                case 3:
                    mes = "mar";
                    break;
                case 4:
                    mes = "abr";
                    break;
                case 5:
                    mes = "mai";
                    break;
                case 6:
                    mes = "jun";
                    break;
                case 7:
                    mes = "jul";
                    break;
                case 8:
                    mes = "ago";
                    break;
                case 9:
                    mes = "set";
                    break;
                case 10:
                    mes = "out";
                    break;
                case 11:
                    mes = "nov";
                    break;
                case 12:
                    mes = "dez";
                    break;
                default:
                    mes = "ERRO";
                    break;
            }

            return mes;
        }

        private List<string> GetNameWeeks(List<string> weeks)
        {
            List<string> retorno = new List<string>();

            foreach (var item in weeks)
            {
                DateTime date = DateTime.Parse(item.Split('|')[0]);
                retorno.Add("W" + GetWeeksInYear(date));
            }

            return retorno;
        }

        public Highcharts GetSeriesMaioresDefeitosIAC(List<MaioresDefeitos> listaMaioresDefeitos, List<string> months, List<string> categorias, string tipo, int? width = 0, int height = 0)
        {
            List<Series> seriesMaioresDefeitos = new List<Series>();

            var causas = (from a in listaMaioresDefeitos
                          group a by new { CodCausa = a.CodCausa, DesCausa = a.DesCausa } into grupo
                          orderby grupo.Count() descending
                          select new
                          {
                              CodCausa = grupo.Key.CodCausa,
                              DesCausa = grupo.Key.DesCausa
                          }).Take(5).ToList();

            List<object> listaObject = null;
            int QuantidadeProduzida = 0;
            int QuantidadeDefeito = 0;
            double calculo = 0;

            for (int i = 0; i < months.Count; i++)
            {
                var dataExplode = months[i].Split('|');
                var dataInicial = DateTime.Parse(dataExplode[0]);
                var dataFinal = DateTime.Parse(dataExplode[1]);

                QuantidadeProduzida = QualidadeBO.GetQuantidadeProduzidaIAC(dataInicial, dataFinal, tipo);

                listaObject = new List<object>();

                foreach (var item in causas)
                {
                    QuantidadeDefeito = listaMaioresDefeitos.Where(a => a.DatEvento >= dataInicial && a.DatEvento <= dataFinal && a.CodCausa == item.CodCausa).Count();
                    calculo = QuantidadeProduzida > 0 ? (double)QuantidadeDefeito / (double)QuantidadeProduzida : 0;
                    listaObject.Add(Math.Round(calculo, 2));
                }

                seriesMaioresDefeitos.Add(new Series
                {
                    Name = categorias[i],
                    Type = ChartTypes.Column,
                    YAxis = "1",
                    Data = new Data(listaObject.ToArray())
                });

            }

            Highcharts chartMaioresDefeitos = Grafico.CombineGraph("IAC MAIORES DEFEITOS", causas.Select(s => s.DesCausa).ToList(), seriesMaioresDefeitos, "chartMaiorDefeito", width, height);

            return chartMaioresDefeitos;
        }

        private Highcharts GetSeriesMaioresDefeitos(List<MaioresDefeitos> listaMaioresDefeitos, List<string> months, List<string> categorias, string Familia, string fase, int? width = 0, int? height = 0)
        {

            string DescricaoFamilia = string.Empty;

            if (Familia == "FEC")
            {
                DescricaoFamilia = "MF";
            }
            else if (Familia == "AUD")
            {
                DescricaoFamilia = "ÁUDIO";
            }
            else if (Familia == "TAB")
            {
                DescricaoFamilia = "INFO";
            }
            else
            {
                DescricaoFamilia = Familia;
            }

            List<Series> seriesMaioresDefeitos = new List<Series>();

            var causas = (from a in listaMaioresDefeitos
                          group a by new { CodCausa = a.CodCausa, DesCausa = a.DesCausa } into grupo
                          orderby grupo.Count() descending
                          select new
                          {
                              CodCausa = grupo.Key.CodCausa,
                              DesCausa = grupo.Key.DesCausa
                          }).Take(5).ToList();

            List<object> listaObject = null;
            int QuantidadeProduzida = 0;
            int QuantidadeDefeito = 0;
            double calculo = 0;

            for (int i = 0; i < months.Count; i++)
            {
                var dataExplode = months[i].Split('|');
                var dataInicial = DateTime.Parse(dataExplode[0]);
                var dataFinal = DateTime.Parse(dataExplode[1]);

                QuantidadeProduzida = QualidadeBO.GetTotalProduzido(dataInicial, dataFinal, fase, Familia);

                listaObject = new List<object>();

                foreach (var item in causas)
                {
                    QuantidadeDefeito = listaMaioresDefeitos.Where(a => a.DatEvento >= dataInicial && a.DatEvento <= dataFinal && a.CodCausa == item.CodCausa).Count();
                    calculo = QuantidadeProduzida > 0 ? (double)QuantidadeDefeito / (double)QuantidadeProduzida : 0;
                    listaObject.Add(Math.Round(calculo * 100, 2));
                }

                seriesMaioresDefeitos.Add(new Series
                {
                    Name = categorias[i],
                    Type = ChartTypes.Column,
                    YAxis = "1",
                    Data = new Data(listaObject.ToArray())
                });

            }

            if (fase == "FEC")
            {
                fase = "MF";
            }

            Highcharts chartMaioresDefeitos = Grafico.CombineGraph("MAIORES DEFEITOS IAC NA " + fase + "-" + DescricaoFamilia, causas.Select(s => s.DesCausa).ToList(), seriesMaioresDefeitos, "chartMaiorDefeito", width, height);

            return chartMaioresDefeitos;
        }

        private Highcharts GetSeriesMaioresDefeitosPosicoes(List<MaioresDefeitos> listaMaioresDefeitos, List<string> months, List<string> categorias, string Familia, string fase, int? width = 0, int? height = 0)
        {

            string DescricaoFamilia = string.Empty;

            if (Familia == "AUD")
            {
                DescricaoFamilia = "ÁUDIO";
            }
            else if (Familia == "TAB")
            {
                DescricaoFamilia = "INFO";
            }
            else
            {
                DescricaoFamilia = Familia;
            }

            List<Series> seriesMaioresDefeitos = new List<Series>();

            var causas = (from a in listaMaioresDefeitos
                          group a by new
                          {
                              CodCausa = a.CodCausa,
                              DesCausa = a.DesCausa,
                              Posicao = a.Posicao,
                              QuantidadePosicao = a.Posicao.Count(),
                              QuantidadeCausa = a.CodCausa.Count()
                          } into grupo
                          orderby grupo.Count() descending
                          select new
                          {
                              CodCausa = grupo.Key.CodCausa,
                              DesCausa = grupo.Key.DesCausa,
                              Posicao = grupo.Key.Posicao,
                              QuantidadePosicao = grupo.Key.QuantidadePosicao,
                              QuantidadeCausa = grupo.Key.QuantidadeCausa
                          }).Take(5).ToList();

            List<object> listaObject = null;

            for (int i = 0; i < months.Count; i++)
            {
                var dataExplode = months[i].Split('|');
                var dataInicial = DateTime.Parse(dataExplode[0]);
                var dataFinal = DateTime.Parse(dataExplode[1]);

                listaObject = new List<object>();

                foreach (var item in causas)
                {
                    var quantidade = listaMaioresDefeitos.Where(a => a.DatEvento >= dataInicial && a.DatEvento <= dataFinal && a.CodCausa == item.CodCausa && a.Posicao == item.Posicao).Count();
                    listaObject.Add(quantidade);
                }

                seriesMaioresDefeitos.Add(new Series
                {
                    Name = categorias[i],
                    Data = new Data(listaObject.ToArray())
                });
            }

            var categories = causas.Select(s => string.Format("{0}<br><br><b>POSIÇÃO</b>: {1}", s.DesCausa, s.Posicao)).ToList();

            string descricaoGrafico = fase == "FEC" ? "MF" + "-" + DescricaoFamilia + " MAIORES DEFEITOS" : fase + "-" + DescricaoFamilia + " MAIORES DEFEITOS";

            if (fase == "IAC")
            {
                descricaoGrafico = "IAC MAIORES DEFEITOS POSIÇÕES";
            }

            Highcharts chartMaioresDefeitos = Grafico.StackedColumn(descricaoGrafico, categories.ToList(), seriesMaioresDefeitos, "posicoes", width, height);

            return chartMaioresDefeitos;
        }

        private Highcharts GetSeriesIndiceDefeitosInternoIAC(List<string> months, List<string> categorias, int? width = 0, int height = 0)
        {

            List<object> listaObjectInspecao = null, listaObjectICT = null, listaObjectTotal = null;

            Highcharts chart = null;

            double calculoInspecao = 0, calculoICT = 0, total = 0;

            List<Series> series = new List<Series>();

            listaObjectInspecao = new List<object>();
            listaObjectICT = new List<object>();
            listaObjectTotal = new List<object>();

            for (int i = 0; i < months.Count; i++)
            {
                var dataExplode = months[i].Split('|');
                var dataInicial = DateTime.Parse(dataExplode[0]);
                var dataFinal = DateTime.Parse(dataExplode[1]);

                int quantidadeDefeitoInspecao = QualidadeBO.GetQuantidadeDefeitoIAC(dataInicial, dataFinal, "INSPECAO");
                int quantidadeProduzidaInspecao = QualidadeBO.GetQuantidadeProduzidaIAC(dataInicial, dataFinal, "INSPECAO");
                int quantidadePontosInspecao = QualidadeBO.GetQuantidadeDePontosGeral(dataInicial, dataFinal, "INSPECAO");

                int quantidadeDefeitoICT = QualidadeBO.GetQuantidadeDefeitoIAC(dataInicial, dataFinal, "ICT");
                int quantidadeProduzidaICT = QualidadeBO.GetQuantidadeProduzidaIAC(dataInicial, dataFinal, "ICT");
                int quantidadePontosICT = QualidadeBO.GetQuantidadeDePontosGeral(dataInicial, dataFinal, "ICT"); ;

                calculoInspecao = quantidadeProduzidaInspecao > 0 ? (double)quantidadeDefeitoInspecao / (double)(quantidadeProduzidaInspecao * quantidadePontosInspecao) : 0;

                calculoICT = quantidadeProduzidaICT > 0 ? (double)quantidadeDefeitoICT / (double)(quantidadeProduzidaICT * quantidadePontosICT) : 0;

                total = calculoInspecao + calculoICT;

                listaObjectInspecao.Add(Math.Round((double)calculoInspecao * 1000000, 1));
                listaObjectICT.Add(Math.Round((double)calculoICT * 1000000, 1));
                listaObjectTotal.Add(Math.Round((double)total * 1000000, 1));
            }

            series.Add(new Series
            {
                Name = "PPM INSPEÇÃO",
                Color = ColorTranslator.FromHtml("#6a95c7"),
                Data = new Data(listaObjectInspecao.ToArray())
            });

            series.Add(new Series
            {
                Name = "PPM ICT",
                Color = ColorTranslator.FromHtml("#c0504d"),
                Data = new Data(listaObjectICT.ToArray())
            });

            series.Add(new Series
            {
                Name = "PPM TOTAL",
                Color = ColorTranslator.FromHtml("#9bbb59"),
                Data = new Data(listaObjectTotal.ToArray())
            });

            chart = Grafico.CombineGraph("IAC ÍNDICE DE DEFEITOS (INTERNO)", categorias, series, "chartIndice", width, height, "", "", false);
            ViewBag.TableIndiceGeral = GetTableGeral(series, categorias);

            return chart;
        }

        private Highcharts GetSeriesIndiceDefeitosIACPorLinha(List<string> months, List<string> categorias, CBLinhaDTO linha)
        {
            List<object> listaObject = new List<object>();
            List<object> listaMeta = new List<object>();
            Highcharts chart = null;

            double calculo = 0;
            List<Series> series = new List<Series>();

            for (int i = 0; i < months.Count; i++)
            {
                var dataExplode = months[i].Split('|');
                var dataInicial = DateTime.Parse(dataExplode[0]);
                var dataFinal = DateTime.Parse(dataExplode[1]);

                int quantidadePontos = QualidadeBO.GetQuantidadeDePontos(dataInicial, dataFinal, linha.CodLinha);
                int quantidadeDefeito = QualidadeBO.GetQuantidadeDefeitoIACPorLinha(dataInicial, dataFinal, linha.CodLinha);
                int quantidadeProduzida = QualidadeBO.GetQuantidadeProduzidaIACPorLinha(dataInicial, dataFinal, linha.CodLinha);

                calculo = quantidadeProduzida > 0 ? (double)quantidadeDefeito / (double)(quantidadeProduzida * quantidadePontos) : 0;

                listaObject.Add(Math.Round((double)calculo * 1000000, 1));
                listaMeta.Add((double)30);
            }

            string name = linha.DesLinha.TrimStart().TrimEnd().Trim().Replace(" ", "");

            name = name.Replace("-", "");

            series.Add(new Series
            {
                Name = "PPM",
                Color = ColorTranslator.FromHtml("#CCCCCC"),
                Data = new Data(listaObject.ToArray())
            });

            series.Add(new Series
            {
                Name = "META",
                Color = ColorTranslator.FromHtml("#9bbb59"),
                Data = new Data(listaMeta.ToArray()),
                PlotOptionsLine = new PlotOptionsLine
                {
                    DataLabels = new PlotOptionsLineDataLabels
                    {
                        Enabled = false,
                        Style = "color: '#ffffff'"
                    }
                }
            });

            chart = Grafico.CombineGraph("PPM LINHA : " + linha.DesLinha.ToUpper(), categorias, series, "chart" + name, 0, 0, "", "", false);
            ViewData[name] = GetTableGeral(series, categorias);

            return chart;
        }

        private List<MaioresDefeitos> GetTablePosicoes(List<MaioresDefeitos> listaMaioresDefeitos, List<string> months, List<string> categorias)
        {

            List<MaioresDefeitos> MaioresDefeitos = new List<MaioresDefeitos>();

            var causas = (from a in listaMaioresDefeitos
                          group a by new
                          {
                              CodCausa = a.CodCausa,
                              DesCausa = a.DesCausa,
                              Posicao = a.Posicao,
                              QuantidadePosicao = a.Posicao.Count(),
                              QuantidadeCausa = a.CodCausa.Count()
                          } into grupo
                          orderby grupo.Count() descending
                          select new
                          {
                              CodCausa = grupo.Key.CodCausa,
                              DesCausa = grupo.Key.DesCausa,
                              Posicao = grupo.Key.Posicao,
                              QuantidadePosicao = grupo.Key.QuantidadePosicao,
                              QuantidadeCausa = grupo.Key.QuantidadeCausa
                          }).Take(5).ToList();

            for (int i = 0; i < months.Count; i++)
            {
                var dataExplode = months[i].Split('|');
                var dataInicial = DateTime.Parse(dataExplode[0]);
                var dataFinal = DateTime.Parse(dataExplode[1]);

                foreach (var item in causas)
                {
                    var quantidade = listaMaioresDefeitos.Where(a => a.DatEvento >= dataInicial && a.DatEvento <= dataFinal && a.CodCausa == item.CodCausa && a.Posicao == item.Posicao).Count();

                    MaioresDefeitos.Add(new MaioresDefeitos
                    {
                        DesCausa = item.DesCausa,
                        CodCausa = item.CodCausa,
                        QuantidadeDefeito = quantidade,
                        Posicao = item.Posicao,
                        Data = categorias[i]
                    });

                }
            }

            return MaioresDefeitos;
        }

        private List<MaioresDefeitos> GetTableMaioresDefeitosIAC(List<MaioresDefeitos> listaMaioresDefeitos, List<string> months, List<string> categorias, string tipo)
        {
            List<MaioresDefeitos> MaioresDefeitos = new List<MaioresDefeitos>();

            var causas = (from a in listaMaioresDefeitos
                          group a by new { CodCausa = a.CodCausa, DesCausa = a.DesCausa } into grupo
                          orderby grupo.Count() descending
                          select new
                          {
                              CodCausa = grupo.Key.CodCausa,
                              DesCausa = grupo.Key.DesCausa
                          }).Take(5).ToList();

            int QuantidadeProduzida = 0;
            int QuantidadeDefeito = 0;
            double calculo = 0;

            for (int i = 0; i < months.Count; i++)
            {
                var dataExplode = months[i].Split('|');
                var dataInicial = DateTime.Parse(dataExplode[0]);
                var dataFinal = DateTime.Parse(dataExplode[1]);

                QuantidadeProduzida = QualidadeBO.GetQuantidadeProduzidaIAC(dataInicial, dataFinal, tipo);

                foreach (var item in causas)
                {
                    QuantidadeDefeito = listaMaioresDefeitos.Where(a => a.DatEvento >= dataInicial && a.DatEvento <= dataFinal && a.CodCausa == item.CodCausa).Count();
                    calculo = QuantidadeProduzida > 0 ? (double)QuantidadeDefeito / (double)QuantidadeProduzida : 0;

                    MaioresDefeitos.Add(new MaioresDefeitos
                    {
                        DesCausa = item.DesCausa,
                        CodCausa = item.CodCausa,
                        Calculo = Math.Round(calculo, 2),
                        Data = categorias[i]
                    });
                }
            }

            return MaioresDefeitos;
        }

        private List<MaioresDefeitos> GetTableMaioresDefeitos(List<MaioresDefeitos> listaMaioresDefeitos, List<string> months, List<string> categorias, string fase, string Familia)
        {
            List<MaioresDefeitos> MaioresDefeitos = new List<MaioresDefeitos>();

            var causas = (from a in listaMaioresDefeitos
                          group a by new { CodCausa = a.CodCausa, DesCausa = a.DesCausa } into grupo
                          orderby grupo.Count() descending
                          select new
                          {
                              CodCausa = grupo.Key.CodCausa,
                              DesCausa = grupo.Key.DesCausa
                          }).Take(5).ToList();

            int QuantidadeProduzida = 0;
            int QuantidadeDefeito = 0;
            double calculo = 0;

            for (int i = 0; i < months.Count; i++)
            {
                var dataExplode = months[i].Split('|');
                var dataInicial = DateTime.Parse(dataExplode[0]);
                var dataFinal = DateTime.Parse(dataExplode[1]);

                QuantidadeProduzida = QualidadeBO.GetTotalProduzido(dataInicial, dataFinal, fase, Familia);

                foreach (var item in causas)
                {
                    QuantidadeDefeito = listaMaioresDefeitos.Where(a => a.DatEvento >= dataInicial && a.DatEvento <= dataFinal && a.CodCausa == item.CodCausa).Count();
                    calculo = QuantidadeProduzida > 0 ? (double)QuantidadeDefeito / (double)QuantidadeProduzida : 0;

                    MaioresDefeitos.Add(new MaioresDefeitos
                    {
                        DesCausa = item.DesCausa,
                        CodCausa = item.CodCausa,
                        Calculo = Math.Round(calculo, 2),
                        Data = categorias[i]
                    });
                }
            }

            return MaioresDefeitos;
        }

        private List<MaioresDefeitos> GetTableGeral(List<Series> series, List<string> categorias)
        {
            List<MaioresDefeitos> MaioresDefeitos = new List<MaioresDefeitos>();

            foreach (Series item in series)
            {
                for (int i = 0; i < categorias.Count; i++)
                {
                    MaioresDefeitos.Add(new MaioresDefeitos
                    {
                        DesCausa = item.Name,
                        Data = categorias[i],
                        Calculo = (double)item.Data.ArrayData[i]
                    });
                }
            }

            return MaioresDefeitos;
        }

        public JsonResult GetValoresPPMIACGERAL(int Meta)
        {
            LerExcel excel = new LerExcel(@"C:\temp\Arquivo.xlsx");

            var lista = excel.Consulta("SELECT data, quantidadeProduzida, quantidadeDefeito, quantidadePontos FROM [PPMIACGERAL$]");

            List<string> categoria = new List<string>();
            List<double> calculos = new List<double>();
            List<double> meta = new List<double>();

            double calculo = 0;

            foreach (var item in lista)
            {
                if (!string.IsNullOrWhiteSpace(item.ToString()))
                {
                    var exp = item.ToString().Split('|');
                    categoria.Add(exp[0]);

                    int quantidadeProduzida = int.Parse(exp[1].ToString());
                    int quantidadeDefeito = int.Parse(exp[2].ToString());
                    int quantidadePontos = int.Parse(exp[3].ToString());

                    calculo = quantidadeProduzida > 0 ? (double)quantidadeDefeito / (double)(quantidadeProduzida * quantidadePontos) : 0;
                    calculos.Add(Math.Round((calculo) * 1000000, 1));

                    meta.Add(Meta);
                }
            }

            var data = new
            {
                categoria = categoria,
                calculos = calculos,
                meta = meta,
                max = calculos.Max()
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetValoresPPMLINHA9(int Meta)
        {
            LerExcel excel = new LerExcel(@"C:\temp\Arquivo.xlsx");

            var lista = excel.Consulta("SELECT data, quantidadeProduzida, quantidadeDefeito, quantidadePontos FROM [PPMLINHA9$]");

            List<string> categoria = new List<string>();
            List<double> calculos = new List<double>();
            List<double> meta = new List<double>();
            
            double calculo = 0;

            foreach (var item in lista)
            {
                if (!string.IsNullOrWhiteSpace(item.ToString()))
                {
                    var exp = item.ToString().Split('|');
                    categoria.Add(exp[0]);

                    int quantidadeProduzida = int.Parse(exp[1].ToString());
                    int quantidadeDefeito = int.Parse(exp[2].ToString());
                    int quantidadePontos = int.Parse(exp[3].ToString());

                    calculo = quantidadeProduzida > 0 ? (double)quantidadeDefeito / (double)(quantidadeProduzida * quantidadePontos) : 0;
                    calculos.Add(Math.Round((calculo) * 1000000, 1));

                    meta.Add(Meta);
                }
            }

            var data = new
            {
                categoria = categoria,
                calculos = calculos,
                meta = meta,
                max = calculos.Max()
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }

    }
}
