﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEMP.UIColeta.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {

			return View();
			
        }

		[HttpPost]
		public ActionResult Index(FormCollection dados)
		{

			System.Web.Security.FormsAuthentication.SetAuthCookie(dados["username"], false);

			Session["Empresa"] = "dtb_SEMP";

			return RedirectToAction("Index", "Home");
		}

		public ActionResult Logout()
		{

			Session.RemoveAll();
			System.Web.Security.FormsAuthentication.SignOut();
			return RedirectToAction("Index", "Login");

		}
	}
}