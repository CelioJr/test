using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CnqCustoRetMap : EntityTypeConfiguration<tbl_CnqCustoRet>
    {
        public tbl_CnqCustoRetMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.MesRef, t.CodFam });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodFam)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_CnqCustoRet");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.MesRef).HasColumnName("MesRef");
            this.Property(t => t.CodFam).HasColumnName("CodFam");
            this.Property(t => t.ValReTrabalho).HasColumnName("ValReTrabalho");
            this.Property(t => t.ProdAmount).HasColumnName("ProdAmount");
        }
    }
}
