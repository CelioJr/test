namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_procomest
    {
        [Key]
        public long id_procomest { get; set; }

        public decimal? qt_usada { get; set; }

        public short? tp_procomest { get; set; }

        public long? id_produtoMP { get; set; }

        public long id_produto { get; set; }

        public int? conjunto { get; set; }

        public virtual om_produto om_produto { get; set; }

        public virtual om_produto om_produto1 { get; set; }
    }
}
