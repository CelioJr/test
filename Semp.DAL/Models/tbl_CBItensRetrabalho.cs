﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.Models
{
	public partial class tbl_CBItensRetrabalho
	{
		public int CodRetrabalho { get; set; }
		public string NumECB { get; set; }
		public virtual tbl_CBRetrabalho tbl_CBRetrabalho { get; set; }
	}
}
