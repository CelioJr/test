namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ORDER_ITEMS_IN
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ORDER_ITEMS_IN()
        {
            ORDER_CONDITIONS_IN = new HashSet<ORDER_CONDITIONS_IN>();
            ORDER_SCHEDULES_IN = new HashSet<ORDER_SCHEDULES_IN>();
        }

        public Guid ID { get; set; }

        public Guid ORDER_HEADER_IN_ID { get; set; }

        [StringLength(100)]
        public string ITM_NUMBER { get; set; }

        [StringLength(4)]
        public string ITEM_CATEG { get; set; }

        [StringLength(40)]
        public string MATERIAL { get; set; }

        public decimal? TARGET_QTY { get; set; }

        [StringLength(4)]
        public string PLANT { get; set; }

        [StringLength(4)]
        public string STORE_LOC { get; set; }

        [StringLength(12)]
        public string ORDERID { get; set; }

        [StringLength(10)]
        public string PURCH_DATE { get; set; }

        [StringLength(10)]
        public string PO_DAT_S { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDER_CONDITIONS_IN> ORDER_CONDITIONS_IN { get; set; }

        public virtual ORDER_HEADER_IN ORDER_HEADER_IN { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDER_SCHEDULES_IN> ORDER_SCHEDULES_IN { get; set; }
    }
}
