using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DTB_CB.Models.Mapping
{
    public class tbl_CBPassagemMap : EntityTypeConfiguration<tbl_CBPassagem>
    {
        public tbl_CBPassagemMap()
        {
            // Primary Key
            this.HasKey(t => new { t.NumECB, t.CodLinha, t.NumPosto, t.DatEvento });

            // Properties
            this.Property(t => t.NumECB)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.CodLinha)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NumPosto)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodDRT)
                .IsFixedLength()
                .HasMaxLength(8);

            // Table & Column Mappings
            this.ToTable("tbl_CBPassagem");
            this.Property(t => t.NumECB).HasColumnName("NumECB");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.NumPosto).HasColumnName("NumPosto");
            this.Property(t => t.DatEvento).HasColumnName("DatEvento");
            this.Property(t => t.CodDRT).HasColumnName("CodDRT");

            // Relationships
            this.HasRequired(t => t.tbl_CBIdentifica)
                .WithMany(t => t.tbl_CBPassagem)
                .HasForeignKey(d => d.NumECB);
            this.HasRequired(t => t.tbl_CBLinha)
                .WithMany(t => t.tbl_CBPassagem)
                .HasForeignKey(d => d.CodLinha);

        }
    }
}
