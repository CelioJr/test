namespace SEMP.Model.DTO
{
    public partial class viw_STI_NEDTO
    {
        public string coditem { get; set; }
        public string desItem { get; set; }
        public string CodUnid { get; set; }
        public string CodFamDBF { get; set; }
        public string CodEstrutura { get; set; }
        public string CodSupFamilia { get; set; }
        public string CodFamilia { get; set; }
        public string CodSubFamilia { get; set; }
        public string CodPartNumber { get; set; }
        public string Tipo_Familia { get; set; }
        public string DesResumida { get; set; }
        public string CodModeloEtq { get; set; }
        public string CodAnatelEtq { get; set; }
        public string CodEanEtq { get; set; }
        public string NomFactorClass { get; set; }
        public string NomSubFactorClass { get; set; }
        public decimal? NumScreenSize { get; set; }
        public string DesTouch { get; set; }
    }
}
