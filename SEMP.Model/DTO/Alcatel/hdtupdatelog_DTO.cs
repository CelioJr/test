using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class hdtupdatelog_DTO
    {
        public long id { get; set; }
        public int endNumber { get; set; }
        public string hdtId { get; set; }
        public string model { get; set; }
        public int startNumber { get; set; }
        public int tacNumber { get; set; }
        public string type { get; set; }
        public string userName { get; set; }
    }
}
