namespace SEMP.DAL.Alcatel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("db_accessadmin.ALC_CQTF")]
    public partial class ALC_CQTF
    {
        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal ID_CQTF { get; set; }

        [StringLength(50)]
        public string cqtfControl { get; set; }

        public DateTime? dtEntry { get; set; }

        public int? product { get; set; }

        public DateTime? dtCheck { get; set; }

        [StringLength(100)]
        public string remessa { get; set; }
    }
}
