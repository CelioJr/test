using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_PROPRIEDADE_DTO
    {
        public int FRM_CODIGO { get; set; }
        public int COM_CODIGO { get; set; }
        public string PRO_NOME { get; set; }
        public string PRO_VALOR { get; set; }
        public virtual FR_COMPONENTE_DTO FR_COMPONENTE { get; set; }
    }
}
