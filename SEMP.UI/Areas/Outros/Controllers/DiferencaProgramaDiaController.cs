﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAST.BO.PM;
using SEMP.Model.VO.PM;

namespace SEMP.Areas.Outros.Controllers
{
	public class DiferencaProgramaDiaController : Controller
	{
		//
		// GET: /Outros/ProgramaDia/
		public ActionResult Index()
		{
			return View();
		}

		//
		// GET: /Outros/ProgramaDia/Details/5
		public ActionResult Lista(string mesRef, string fase, string codlinha, string familia)
		{
			var dMes = DateTime.Parse(DateTime.Now.AddDays(-DateTime.Now.Day).AddDays(1).ToShortDateString());

			if (!String.IsNullOrEmpty(mesRef))
			{
				dMes = DateTime.Parse(mesRef);
			}

			var programa = ProgramaProducao.ObterProgramacao(dMes, fase, codlinha, familia);
			var modelos = programa.GroupBy(x => new {x.CodFab, x.CodModelo, x.DesModelo, x.CodLinha, x.DesLinha, x.NumTurno, x.Familia, x.QtdCapac, x.PlanoMes}).Select(x => new ProgModelos()
			{
				CodFab = x.Key.CodFab,
				CodLinha = x.Key.CodLinha,
				CodModelo = x.Key.CodModelo,
				DesModelo = x.Key.DesModelo,
				DesLinha = x.Key.DesLinha,
				NumTurno = x.Key.NumTurno,
				QtdPrograma = x.Sum(y => y.QtdProdPm),
				QtdProduzido = x.Sum(y => y.QtdProducao),
				Familia = x.Key.Familia,
				QtdCapacidade = x.Key.QtdCapac,
				QtdPlanoMes = x.Key.PlanoMes
			});

			ViewBag.Programa = programa;
			ViewBag.Data = dMes;
			return View(modelos);
		}

		public ActionResult Meses(string mesRef, string fase, string codlinha, string familia)
		{
			var dMes = DateTime.Parse(DateTime.Now.AddDays(-DateTime.Now.Day).AddDays(1).ToShortDateString());

			if (!String.IsNullOrEmpty(mesRef))
			{
				dMes = DateTime.Parse(mesRef);
			}

			var meses = fase.Equals("F") ? ProgramaProducao.ObterMesesPrograma(dMes) : ProgramaProducao.ObterMesesProgramaPlaca(dMes);

			ViewBag.mesRef = dMes.ToShortDateString();
			ViewBag.fase = fase;
			ViewBag.codlinha = codlinha;
			ViewBag.familia = familia;

			return View(meses);
		}

		public ActionResult ListaXLS(string mesRef, string fase, string codlinha, string familia)
		{

			Response.AddHeader("content-disposition", "attachment; filename=ProgramaDia.xls");
			Response.ContentType = "application/ms-excel";

			var dMes = DateTime.Parse(DateTime.Now.AddDays(-DateTime.Now.Day).AddDays(1).ToShortDateString());

			if (!String.IsNullOrEmpty(mesRef))
			{
				dMes = DateTime.Parse(mesRef);
			}

			var programa = ProgramaProducao.ObterProgramacao(dMes, fase, codlinha, familia);
			var modelos = programa.GroupBy(x => new { x.CodFab, x.CodModelo, x.DesModelo, x.CodLinha, x.DesLinha, x.NumTurno, x.Familia, x.QtdCapac, x.PlanoMes }).Select(x => new ProgModelos()
			{
				CodFab = x.Key.CodFab,
				CodLinha = x.Key.CodLinha,
				CodModelo = x.Key.CodModelo,
				DesModelo = x.Key.DesModelo,
				DesLinha = x.Key.DesLinha,
				NumTurno = x.Key.NumTurno,
				QtdPrograma = x.Sum(y => y.QtdProducao ?? y.QtdProdPm),
				Familia = x.Key.Familia,
				QtdCapacidade = x.Key.QtdCapac,
				QtdPlanoMes = x.Key.PlanoMes
			});

			ViewBag.Programa = programa;
			ViewBag.Data = dMes;
			return View(modelos);
		}
	}
}
