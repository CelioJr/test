using System;

namespace SEMP.Model.DTO
{
    public class PlanoItemDTO
    {
        public string CodModelo { get; set; }
        public string CodItem { get; set; }
        public string NumPosicao { get; set; }
        public Nullable<decimal> QtdItem { get; set; }
        public string CodProcesso { get; set; }
        public string CodPreForma { get; set; }
        public System.DateTime DatInicio { get; set; }
        public Nullable<System.DateTime> DatFim { get; set; }
        public string NumNAEnt { get; set; }
        public string NumNASai { get; set; }
        public string Quadrante { get; set; }
        public string FlgAtivo { get; set; }
        public string CodBreakdown { get; set; }
        public string NomUsuario { get; set; }
        public System.Guid rowguid { get; set; }
        public string CodBlocoCirc { get; set; }
    }
}
