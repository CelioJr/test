using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_WIPProducao
    {
        public string CodLocal { get; set; }
        public string CodItem { get; set; }
        public string Familia { get; set; }
        public Nullable<decimal> QtdEstoque { get; set; }
        public Nullable<decimal> Valor { get; set; }
    }
}
