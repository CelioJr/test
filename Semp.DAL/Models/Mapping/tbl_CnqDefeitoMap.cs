using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CnqDefeitoMap : EntityTypeConfiguration<tbl_CnqDefeito>
    {
        public tbl_CnqDefeitoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.CodDefeito });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodFam)
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.CodSubFam)
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.CodDefeito)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.DesDefeito)
                .HasMaxLength(60)
                .IsUnicode(false);

            this.Property(t => t.flgAtivo)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_CnqDefeito");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.CodFam).HasColumnName("CodFam");
            this.Property(t => t.CodSubFam).HasColumnName("CodSubFam");
            this.Property(t => t.CodDefeito).HasColumnName("CodDefeito");
            this.Property(t => t.DesDefeito).HasColumnName("DesDefeito");
            this.Property(t => t.flgIMC).HasColumnName("flgIMC");
            this.Property(t => t.flgMONTAGEM).HasColumnName("flgMONTAGEM");
            this.Property(t => t.flgIAC).HasColumnName("flgIAC");
            this.Property(t => t.flgAtivo).HasColumnName("flgAtivo");
        }
    }
}
