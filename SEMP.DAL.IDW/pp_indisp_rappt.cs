namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_indisp_rappt
    {
        [Key]
        public long id_indisp_rappt { get; set; }

        public DateTime? dthr_inicio { get; set; }

        public DateTime? dthr_final { get; set; }

        public short? tp_recurso { get; set; }

        public long? id_rap { get; set; }

        public long? id_pt { get; set; }

        public long id_indisp { get; set; }

        [StringLength(100)]
        public string ds_indisp_rappt { get; set; }

        public decimal? qt_indisp { get; set; }

        public virtual dw_rap dw_rap { get; set; }

        public virtual om_pt om_pt { get; set; }

        public virtual pp_indisp pp_indisp { get; set; }
    }
}
