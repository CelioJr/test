﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.DAL.Models;
using SEMP.Model.VO;
using SEMP.Model.VO.Rastreabilidade;
using SEMP.BO;
using SEMP.Model.DTO;
using SEMP.Model;

namespace RAST.BO.Rastreabilidade
{
	public class RecebimentoLote
	{
		#region Antigos
		public static int ObterTotalAP(string CodFab, string NumAP)
		{
			return daoGeral.ObterTotalAP(CodFab, NumAP);
		}

		public static int ObterTotalRecebido(string CodFab, string NumAP)
		{
			return daoRecebimento.ObterTotalRecebido(CodFab, NumAP);
		}

		public static String ObterUltimoLoteRecebido(string CodFab, string NumAP)
		{
			return daoRecebimento.ObterUltimoLoteRecebido(CodFab, NumAP);
		}

		public static int ObterTotalLote(string NumLote)
		{
			return daoRecebimento.ObterTotalLote(NumLote);
		}

		public static int ObterTotalCC(string NumLote)
		{
			return daoRecebimento.ObterTotalCC(NumLote);
		}

		/// <summary>
		/// Verifica se o lote já foi recebido na linha de produção
		/// </summary>
		/// <param name="NumLote"></param>
		/// <returns></returns>
		public static Mensagem ValidarRecebimentoLote(string NumLote)
		{
			Mensagem retorno = new Mensagem();

			try
			{
				//Caso o lote ainda não tenha sido recebido
				if (daoRecebimento.ValidaLoteRecebido(NumLote) == true)
					retorno = Defeito.ObterMensagem(Constantes.RAST_OK);
				//Caso o lote já tenha sido recebido na linha
				else
					retorno = Defeito.ObterMensagem(Constantes.RAST_LEITURA_REPETIDA);
			}
			catch (Exception)
			{
				retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
			}


			return retorno;
		}

		public static Mensagem GravarPassagem(string NumLote, int CodLinha, int NumPosto, string CodDRT, string CodFab, string NumAP)
		{
			Mensagem retorno = new Mensagem();
			Boolean resultado = false;
			Passagem passagem = new Passagem(CodLinha, NumPosto);

			try
			{
				retorno = ValidarRecebimentoLote(NumLote);

				if(retorno.CodMensagem == Constantes.RAST_LEITURA_REPETIDA)
					return retorno;
				else if(retorno.CodMensagem == Constantes.DB_ERRO)
					return retorno;

				List<String> listaNumECB = daoRecebimento.ObterItensLote(NumLote);

				if(listaNumECB == null)
				{
					retorno = Defeito.ObterMensagem(Constantes.RAST_LOTE_VAZIO);
					return retorno;
				}					

				foreach (String item in listaNumECB)
				{
					passagem.GravarPassagem(item, CodDRT, CodFab, NumAP);
				}
				
				CBTransfLoteDTO transfLoteDTO = new CBTransfLoteDTO();
				
				transfLoteDTO = daoTransferencia.ObterLoteTransf(NumLote);
				transfLoteDTO.CodFab = CodFab;
				transfLoteDTO.NumAP = NumAP;
				
				resultado = daoRecebimento.GravarRecebimentoTransf(transfLoteDTO);
				if (resultado == false)
				{
					retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
					return retorno;
				}				
					
				retorno = Defeito.ObterMensagem(Constantes.RAST_OK);
			}
			catch (Exception)
			{
				retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
			}
			

			return retorno;
		}

		public static Mensagem DevolverLote(string NumLote, int CodLinha, int NumPosto)
		{
			Mensagem retorno = new Mensagem();
			Boolean resultado = false;
			Passagem passagem = new Passagem(CodLinha, NumPosto);

			try
			{
				retorno = ValidarRecebimentoLote(NumLote);

				if (retorno.CodMensagem != Constantes.RAST_LEITURA_REPETIDA)
				{
					retorno = Defeito.ObterMensagem(Constantes.RAST_LOTE_NAO_RECEBIDO);
					return retorno;
				}
					
				List<String> listaNumECB = daoRecebimento.ObterItensLote(NumLote);

				if (listaNumECB == null)
				{
					retorno = Defeito.ObterMensagem(Constantes.RAST_LOTE_VAZIO);
					return retorno;
				}

				foreach (String item in listaNumECB)
				{
					resultado = daoPassagem.CancelamentoLeitura(item, CodLinha, NumPosto);

					if (resultado == false)
					{
						retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
						return retorno;
					}
				}
				
				CBTransfLoteDTO transfLoteDTO = new CBTransfLoteDTO();
				
				transfLoteDTO = daoTransferencia.ObterLoteTransf(NumLote);
				
				resultado = daoRecebimento.DesfazRecebimentoTransf(transfLoteDTO);
				if (resultado == false)
				{
					retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
					return retorno;
				}
				
				retorno = Defeito.ObterMensagem(Constantes.RAST_OK);
			}
			catch (Exception)
			{
				retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
			}

			return retorno;
		}

        public static CBLoteDTO GetCaixaColetiva(string serial)
        {
            return daoRecebimento.GetCaixaColetiva(serial);
        }

        public static RecebimentoLoteVO ValidarFIFOLote(CBLoteDTO caixaLote)
        {
            return daoRecebimento.ValidarFIFOLote(caixaLote);
        }

        public static bool GravaPassagemRecebimentoLote(CBLoteDTO caixaLote, DadosPosto posto, string codDRT)
        {
            return daoRecebimento.GravaPassagemRecebimentoLote(caixaLote, posto, codDRT);
        }

        public static List<CBLoteDTO> GetUltimosLido(DadosPosto posto)
        {
            return daoRecebimento.GetUltimosLido(posto);
        }

        public static int GetTotalRecebido(DadosPosto posto)
        {
            return daoRecebimento.GetTotalRecebido(posto);
        }

		#endregion

		#region Novos
		public static int ObterTotalRecebido(int codLinha)
		{
			return daoRecebimento.ObterTotalRecebido(codLinha);
		}

		public static int ObterTotalRecebidoCC(int codLinha)
		{
			return daoRecebimento.ObterTotalRecebidoCC(codLinha);
		}


		public static String ObterUltimoLoteRecebido(int codLinha)
		{
			return daoRecebimento.ObterUltimoLoteRecebido(codLinha);
		}

		public static String ObterUltimoLoteRecebidoCC(int codLinha)
		{
			return daoRecebimento.ObterUltimoLoteRecebidoCC(codLinha);
		}

		/// <summary>
		/// Verifica se o lote já foi recebido na linha de produção
		/// </summary>
		/// <param name="NumLote"></param>
		/// <returns></returns>
		public static Mensagem ValidarRecebimentoLoteCC(string NumLote, int codLinha, int numPosto)
		{
			Mensagem retorno = new Mensagem();

			try
			{
				//Caso o lote ainda não tenha sido recebido
				if (daoRecebimento.ValidaLoteRecebidoCC(NumLote) == false){
					var posto = daoGeral.ObterPosto(codLinha, numPosto);

					if (posto.flgValidaOrigem != null && posto.flgValidaOrigem == true)
					{ 

						if (daoRecebimento.ValidaLoteFIFO(NumLote) == false) { 
							retorno = Defeito.ObterMensagem(Constantes.RAST_OK);
						}
						else
						{
							retorno = Defeito.ObterMensagem(Constantes.RAST_ERRO_FIFO);
							retorno.DesMensagem = "LEITURA FORA DO FIFO: " + NumLote;

						}
					}
				}
				//Caso o lote já tenha sido recebido na linha
				else
					retorno = Defeito.ObterMensagem(Constantes.RAST_LEITURA_REPETIDA);
			}
			catch (Exception)
			{
				retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
			}


			return retorno;
		}

		public static Mensagem GravarPassagem(string NumLote, int CodLinha, int NumPosto, string CodDRT)
		{
			Mensagem retorno = new Mensagem();
			Boolean resultado = false;
			Passagem passagem = new Passagem(CodLinha, NumPosto);

			try
			{
				retorno = ValidarRecebimentoLoteCC(NumLote, CodLinha, NumPosto);

				if (retorno.CodMensagem != Constantes.RAST_OK)
					return retorno;

				List<String> listaNumECB = daoRecebimento.ObterItensLote(NumLote);

				if (listaNumECB == null)
				{
					retorno = Defeito.ObterMensagem(Constantes.RAST_LOTE_VAZIO);
					return retorno;
				}

				foreach (String item in listaNumECB)
				{
					passagem.GravarPassagem(item, CodDRT, "", "");
				}

				resultado = daoRecebimento.GravarRecebimento(NumLote, CodDRT);

				if (resultado == false)
				{
					retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
					return retorno;
				}

				resultado = daoRecebimento.GravarRecebimentoTransf(NumLote, CodDRT, CodLinha);

				if (resultado == false)
				{
					retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
					return retorno;
				}

				retorno = Defeito.ObterMensagem(Constantes.RAST_OK);
			}
			catch (Exception)
			{
				retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
			}


			return retorno;
		}

		public static Mensagem GravarRecebimentoLote(string NumLote, int CodLinha, int NumPosto, string CodDRT, int codLinhaDest)
		{
			Mensagem retorno = new Mensagem();
			//Passagem passagem = new Passagem(CodLinha, NumPosto);

			try
			{
				var ret = daoRecebimento.GravarRecebimentoLote(NumLote, CodDRT, codLinhaDest, CodLinha, NumPosto);

				retorno = Defeito.ObterMensagem(ret);

				if (retorno.CodMensagem == Constantes.RAST_ERRO_FIFO)
				{
					retorno.DesMensagem = "LEITURA FORA DO FIFO: " + NumLote;
				}

			}
			catch (Exception)
			{
				retorno = Defeito.ObterMensagem(Constantes.DB_ERRO);
			}


			return retorno;
		}

		public static string ObterLotePaiCC(string numCC)
		{
			return daoRecebimento.ObterLotePaiCC(numCC);

		}

		public static string ObterLotePaiSerie(string numECB)
		{
			return daoRecebimento.ObterLotePaiSerie(numECB);

		}

		public static Boolean VerificaLoteExiste(string NumLote)
		{
			return daoRecebimento.VerificaLoteExiste(NumLote);
		}

		public static int GetRecebidoLote(string numLote)
		{
			return daoRecebimento.GetRecebidoLote(numLote);

		}

		public static int GetTotalCCLote(string numLote)
		{
			return daoRecebimento.GetTotalCCLote(numLote);
		}

		public static List<DadosRecebLote> ObterDadosRecebimento(int codLinha) {

			return daoRecebimento.ObterDadosRecebimento(codLinha);

		}
		#endregion
	}
}
