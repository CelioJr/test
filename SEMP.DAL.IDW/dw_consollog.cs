namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consollog
    {
        [Key]
        public long id_consollog { get; set; }

        public long id_consol { get; set; }

        public DateTime? dthr_log { get; set; }

        public decimal? seg_corr_tempoativo { get; set; }

        public decimal? seg_corr_tempoparada { get; set; }

        public decimal? seg_corr_tempoparada_sp { get; set; }

        [StringLength(40)]
        public string ds_log { get; set; }

        public decimal? seg_corr_cicloprodutivo { get; set; }

        public decimal? seg_corr_cicloimprodutivo { get; set; }

        public decimal? seg_corr_tempotrabalhado { get; set; }

        public decimal? seg_corr_temporefugadas { get; set; }

        public decimal? seg_corr_ritmo { get; set; }

        public decimal? seg_corr_tempoprodutivas { get; set; }

        public decimal? seg_corr_tempodisponivel { get; set; }

        public decimal? seg_corr_perdaciclo { get; set; }

        public decimal? seg_corr_ciclomedio { get; set; }

        public decimal? seg_corr_ciclopadrao { get; set; }

        public decimal? seg_corr_tmpcicnormal { get; set; }

        public decimal? seg_corr_perdacav { get; set; }

        public decimal? seg_corr_boas { get; set; }

        public long? pcs_corr_perdaparada { get; set; }

        public long? pcs_corr_perdaparada_sp { get; set; }

        public long? pcs_corr_perdaciclo { get; set; }

        public long? pcs_corr_perdacavidades { get; set; }

        public long? pcs_corr_producaobruta { get; set; }

        public long? pcs_corr_producaorefugada { get; set; }

        public long? pcs_corr_producaoprevista { get; set; }

        public long? pcs_corr_qtinjnormal { get; set; }

        public decimal? seg_corr_tempocalendario { get; set; }

        public decimal? seg_corr_tempoalerta { get; set; }

        public decimal? seg_corr_tempocalsempeso { get; set; }

        public long id_usr { get; set; }

        public virtual dw_consol dw_consol { get; set; }

        public virtual om_usr om_usr { get; set; }
    }
}
