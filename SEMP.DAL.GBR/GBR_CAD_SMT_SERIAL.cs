namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_CAD_SMT_SERIAL
    {
        [Key]
        public int COD_SMT { get; set; }

        [StringLength(15)]
        public string SERIAL { get; set; }

        public int? COD_SKU { get; set; }

        [StringLength(30)]
        public string USUARIO { get; set; }

        public int? COD_MODELO { get; set; }

        public bool? VERIFICADO { get; set; }

        public bool? DEBUG_SAV { get; set; }

        public DateTime? DATA { get; set; }

        [StringLength(30)]
        public string INSPECAO_USER { get; set; }

        public DateTime? INSPECAO_DATA { get; set; }

        public bool? INSPECAO_VERIFICADO { get; set; }

        public int? INSPECAO_SESSAO { get; set; }

        public bool? IMEI_RECEBIDO { get; set; }

        public bool? CAXA_COLETIVA { get; set; }

        public bool? CAIXA_INDIVIDUAL { get; set; }

        public bool? PALLETIZADO { get; set; }

        [StringLength(15)]
        public string IMEI_1 { get; set; }

        [StringLength(15)]
        public string IMEI_2 { get; set; }

        [StringLength(15)]
        public string IMEI_3 { get; set; }

        [StringLength(15)]
        public string IMEI_4 { get; set; }

        public int? COD_LINHA { get; set; }

        [StringLength(15)]
        public string PACKSTRING { get; set; }

        [StringLength(15)]
        public string PALLETSTRING { get; set; }

        public int? COD_LINHA_IMEI { get; set; }

        public int? COD_LINHA_INDIVIDUAL { get; set; }

        public int? COD_LINHA_COLETIVA { get; set; }

        public int? COD_LINHA_PALLET { get; set; }

        public DateTime? DATA_LINHA_IMEI { get; set; }

        public DateTime? DATA_LINHA_INDIVIDUAL { get; set; }

        public DateTime? DATA_LINHA_COLETIVA { get; set; }

        public DateTime? DATA_LINHA_PALLET { get; set; }

        public bool? SERIAL_ENGINES { get; set; }

        public DateTime? ENGINES_EXPORTADO_DATA { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PESO { get; set; }

        [StringLength(11)]
        public string OP { get; set; }

        [StringLength(25)]
        public string COD_VOLUME { get; set; }

        [StringLength(17)]
        public string ORDEM { get; set; }

        public int? COD_LINHA_FT { get; set; }

        public DateTime? DATA_LINHA_FT { get; set; }

        [StringLength(20)]
        public string COMPUTER_NAME { get; set; }

        [StringLength(20)]
        public string SKU_SAV { get; set; }

        [StringLength(17)]
        public string ORDEM_IMEI { get; set; }

        [StringLength(17)]
        public string ORDEM_CX { get; set; }

        [StringLength(17)]
        public string ORDEM_COLETIVA { get; set; }
    }
}
