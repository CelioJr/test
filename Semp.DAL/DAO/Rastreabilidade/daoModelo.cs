﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.Model.DTO;
using SEMP.DAL.Models;
using System.Linq.Expressions;

namespace SEMP.DAL.DAO.Rastreabilidade
{
	public class daoModelo
	{

		public static List<CBModeloDTO> ObterModelos(Expression<Func<tbl_CBModelo, bool>> predicate)
		{

			using (var contexto = new DTB_CBContext())
			{

				var dados = contexto.tbl_CBModelo.Where(predicate).Join(contexto.tbl_Item, x => x.CodModelo, y => y.CodItem, (x, y) => new { Modelo = x, Item = y }).Select(x => new CBModeloDTO()
				{
					CodModelo = x.Modelo.CodModelo,
					Fase = x.Modelo.Fase,
					FlgControlaFifo = x.Modelo.FlgControlaFifo,
					Mascara = x.Modelo.Mascara,
					QtdAmostrasOBA = x.Modelo.QtdAmostrasOBA,
					QtdCaixa = x.Modelo.QtdCaixa,
					QtdLote = x.Modelo.QtdLote,
					Setor = x.Modelo.Setor,
					TempoCiclo = x.Modelo.TempoCiclo,
                    PostoEntrada = x.Modelo.PostoEntrada,
					DesItem = x.Item.DesItem.Trim()
				}).ToList();

				return dados;

			}
		}

		public static List<CBModeloDTO> ObterModelos()
		{

			using (var contexto = new DTB_CBContext())
			{

				var dados = contexto.tbl_CBModelo.Join(contexto.tbl_Item, x => x.CodModelo, y => y.CodItem, (x, y) => new { Modelo = x, Item = y }).Select(x => new CBModeloDTO()
				{
					CodModelo = x.Modelo.CodModelo,
					Fase = x.Modelo.Fase,
					FlgControlaFifo = x.Modelo.FlgControlaFifo,
					Mascara = x.Modelo.Mascara,
					QtdAmostrasOBA = x.Modelo.QtdAmostrasOBA,
					QtdCaixa = x.Modelo.QtdCaixa,
					QtdLote = x.Modelo.QtdLote,
					Setor = x.Modelo.Setor,
					TempoCiclo = x.Modelo.TempoCiclo,
                    PostoEntrada = x.Modelo.PostoEntrada,
                    DesItem = x.Item.DesItem.Trim()
				}).ToList();

				return dados;

			}
		}

		public static CBModeloDTO ObterModelo(string codModelo)
		{

			using (var contexto = new DTB_CBContext())
			{

				var dados = contexto.tbl_CBModelo.Join(contexto.tbl_Item, x => x.CodModelo, y => y.CodItem, (x, y) => new { Modelo = x, Item = y }).Where(x => x.Modelo.CodModelo == codModelo).Select(x => new CBModeloDTO()
				{
					CodModelo = x.Modelo.CodModelo,
					Fase = x.Modelo.Fase,
					FlgControlaFifo = x.Modelo.FlgControlaFifo,
					Mascara = x.Modelo.Mascara,
					QtdAmostrasOBA = x.Modelo.QtdAmostrasOBA,
					QtdCaixa = x.Modelo.QtdCaixa,
					QtdLote = x.Modelo.QtdLote,
					Setor = x.Modelo.Setor,
					TempoCiclo = x.Modelo.TempoCiclo,
                    PostoEntrada = x.Modelo.PostoEntrada,
                    DesItem = x.Item.DesItem.Trim()
				}).FirstOrDefault();

				return dados;

			}
		}


		public static bool AtualizaModelo(CBModeloDTO modelo)
		{

			try
			{

				using (var contexto = new DTB_CBContext())
				{
					var dado = contexto.tbl_CBModelo.FirstOrDefault(x => x.CodModelo == modelo.CodModelo);

					if (dado != null)
					{
						dado.TempoCiclo = modelo.TempoCiclo;
						dado.Fase = modelo.Fase;
						dado.FlgControlaFifo = modelo.FlgControlaFifo;
						dado.Mascara = modelo.Mascara;
						dado.QtdAmostrasOBA = modelo.QtdAmostrasOBA;
						dado.QtdCaixa = modelo.QtdCaixa;
						dado.QtdLote = modelo.QtdLote;
						dado.Setor = modelo.Setor;
                        dado.PostoEntrada = modelo.PostoEntrada;
					}
					else
					{
						contexto.tbl_CBModelo.Add(new tbl_CBModelo()
						{
							CodModelo = modelo.CodModelo,
							Fase = modelo.Fase,
							FlgControlaFifo = modelo.FlgControlaFifo,
							Mascara = modelo.Mascara,
							QtdAmostrasOBA = modelo.QtdAmostrasOBA,
							QtdCaixa = modelo.QtdCaixa,
							QtdLote = modelo.QtdLote,
							Setor = modelo.Setor,
							TempoCiclo = modelo.TempoCiclo,
                            PostoEntrada = modelo.PostoEntrada
						});
					}

					contexto.SaveChanges();
				}

				return true;

			}
			catch
			{
				return false;
			}
		}

		public static bool ExcluirModelo(CBModeloDTO modelo)
		{

			try
			{

				using (var contexto = new DTB_CBContext())
				{
					var dado = contexto.tbl_CBModelo.FirstOrDefault(x => x.CodModelo == modelo.CodModelo);

					if (dado != null)
					{
						contexto.tbl_CBModelo.Remove(dado);
					}

					contexto.SaveChanges();
				}

				return true;

			}
			catch
			{
				return false;
			}
		}


	}
}
