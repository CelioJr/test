namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_cp_data
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pp_cp_data()
        {
            pp_cp_turno = new HashSet<pp_cp_turno>();
        }

        [Key]
        public long id_cp_data { get; set; }

        public DateTime? dt_planejada { get; set; }

        public long? pcs_producaoPrevista { get; set; }

        public long id_cpproduto { get; set; }

        public virtual pp_cpproduto pp_cpproduto { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cp_turno> pp_cp_turno { get; set; }
    }
}
