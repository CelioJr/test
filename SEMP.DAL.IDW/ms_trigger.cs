namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ms_trigger
    {
        [Key]
        public long id_trigger { get; set; }

        public long id_detector { get; set; }

        public long id_tpevt { get; set; }

        public int? ordem { get; set; }

        public long? id_tppt { get; set; }

        public long? id_pt { get; set; }

        public long? id_ind { get; set; }

        public short? operadorLogico { get; set; }

        public decimal? vl_ind { get; set; }

        public int? qt_ciclos { get; set; }

        public long? id_folha { get; set; }

        public virtual ms_detector ms_detector { get; set; }

        public virtual ms_ind ms_ind { get; set; }

        public virtual ms_tpevt ms_tpevt { get; set; }

        public virtual om_pt om_pt { get; set; }

        public virtual om_tppt om_tppt { get; set; }
    }
}
