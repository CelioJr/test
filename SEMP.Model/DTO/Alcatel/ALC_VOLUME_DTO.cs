using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_VOLUME_DTO
    {
        public ALC_VOLUME_DTO()
        {
            this.ALC_LOT_LIST = new List<ALC_LOT_LIST_DTO>();
            this.ALC_OQC_INSPECTION = new List<ALC_OQC_INSPECTION_DTO>();
            this.ALC_PRODUCTION_COUNTER = new List<ALC_PRODUCTION_COUNTER_DTO>();
        }

        public string ID_VOL { get; set; }
        public Nullable<System.DateTime> DATE_CREATE { get; set; }
        public Nullable<System.DateTime> DATE_FINISH { get; set; }
        public byte[] orderVol { get; set; }
        public Nullable<int> QUANTITY { get; set; }
        public string STATUS { get; set; }
        public Nullable<int> QTY_PROD { get; set; }
        public string ORDER_VOL { get; set; }
        public Nullable<int> PRODUCTS_QTY { get; set; }
        public virtual ICollection<ALC_LOT_LIST_DTO> ALC_LOT_LIST { get; set; }
        public virtual ICollection<ALC_OQC_INSPECTION_DTO> ALC_OQC_INSPECTION { get; set; }
        public virtual ALC_ORDER_DTO ALC_ORDER { get; set; }
        public virtual ALC_ORDER_DTO ALC_ORDER1 { get; set; }
        public virtual ICollection<ALC_PRODUCTION_COUNTER_DTO> ALC_PRODUCTION_COUNTER { get; set; }
    }
}
