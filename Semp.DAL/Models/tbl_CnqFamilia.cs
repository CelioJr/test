using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CnqFamilia
    {
        public string CodFam { get; set; }
        public string NomFam { get; set; }
    }
}
