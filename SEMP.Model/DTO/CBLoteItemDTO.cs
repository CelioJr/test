﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
    public class CBLoteItemDTO
    {
        [Display(Name = "Número do Lote")]
        public string NumLote { get; set; }
        
        [Display(Name = "Número da Caixa Coletiva")]
        public string NumCC { get; set; }

        [Display(Name = "Número de Série")]
        public string NumECB { get; set; }

        [Display(Name = "Status do Número de Série")]
        public int Status { get; set; }

        [Display(Name = "Amostra do Lote")]
        public Nullable<bool> flgAmostra { get; set; }

        [Display(Name = "Data de Inclusão no Lote")]
        public Nullable<System.DateTime> DatInclusao { get; set; }

        [Display(Name = "Data de Inspeção do Número de Série do Lote")]
        public Nullable<System.DateTime> DatInspecao { get; set; }

        [Display(Name = "Comentários sobre a Inspeção")]
        public string ResultadoInspecao { get; set; }

    }
}
