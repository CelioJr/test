namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_clidiario
    {
        [Key]
        public long id_clidiario { get; set; }

        public DateTime? dthr_inicio { get; set; }

        public DateTime? dthr_fim { get; set; }

        [StringLength(256)]
        public string ds_diario { get; set; }

        public long id_cliente { get; set; }

        public virtual pp_cliente pp_cliente { get; set; }
    }
}
