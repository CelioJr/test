﻿using System;

namespace SEMP.SAPConnector.VO
{
	public class ZTPP_RASTREAB
	{

		public int MANDT { get; set; }
		public string WERKS { get; set; }
		public string AUART { get; set; }
		public string AUFNR { get; set; }
		public string MATNR1 { get; set; }
		public string EAN13 { get; set; }
		public string SERIAL1 { get; set; }
		public DateTime CREDATE1 { get; set; }
		public string MATNR2 { get; set; }
		public string SERIAL2 { get; set; }
		public DateTime CREDATE2 { get; set; }
		public decimal PRUEFLOS { get; set; }
		public string CRETIME2 { get; set; }
		public string CRETIME1 { get; set; }
		public string ARBPL { get; set; }
		public int CODLINHA { get; set; }
		public int CODPOSTO { get; set; }
		public string DRT { get; set; }
		public int TPAMAR { get; set; }
		public decimal GESME { get; set; }
		public string STATCONF { get; set; }
		public DateTime DTCONF { get; set; }
		public string MBLNR { get; set; }
		public string STATVD { get; set; }
		public string STBCKFLUSH { get; set; }
		public DateTime DTBCKFLUSH { get; set; }
		public string USERNAME { get; set; }
		public string SISAP { get; set; }
		public string PACKNO { get; set; }
		public string VCODE { get; set; }
		public string OBA { get; set; }


	}
}
