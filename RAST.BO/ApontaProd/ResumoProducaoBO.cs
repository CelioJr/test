﻿using System;
using System.Collections.Generic;
using System.Linq;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model.VO.Rastreabilidade.Relatorios;

namespace RAST.BO.Relatorios
{
	public class ResumoProducaoBO
	{
		#region Montagem Final
		public static List<ResumoProducao> ObterResumoMF(DateTime datInicio, DateTime datFim, String CodModelo, String Familia)
		{

			var resumo = new List<ResumoProducao>();

			if (String.IsNullOrEmpty(CodModelo) && String.IsNullOrEmpty(Familia))
			{
				resumo = daoResumoProducao.ObterResumoMF(datInicio, datFim);
			}
			else if (String.IsNullOrEmpty(CodModelo) && !String.IsNullOrEmpty(Familia))
			{
				resumo = daoResumoProducao.ObterResumoMFFam(datInicio, datFim, Familia);
			}
			else if (!String.IsNullOrEmpty(CodModelo) && String.IsNullOrEmpty(Familia))
			{
				resumo = daoResumoProducao.ObterResumoMF(datInicio, datFim, CodModelo);
			}
			else
			{
				resumo = daoResumoProducao.ObterResumoMF(datInicio, datFim, CodModelo, Familia);
			}

			return resumo;

		}

		public static List<ResumoProducao> ObterResumoModelo(DateTime datInicio, DateTime datFim, String CodModelo, String Familia)
		{

			List<ResumoProducao> resumo = new List<ResumoProducao>();

			if (String.IsNullOrEmpty(CodModelo) && String.IsNullOrEmpty(Familia))
			{
				resumo = daoResumoProducao.ObterResumoModelo(datInicio, datFim);
			}
			else if (String.IsNullOrEmpty(CodModelo) && !String.IsNullOrEmpty(Familia))
			{
				resumo = daoResumoProducao.ObterResumoModeloFam(datInicio, datFim, Familia);
			}
			else if (!String.IsNullOrEmpty(CodModelo) && String.IsNullOrEmpty(Familia))
			{
				resumo = daoResumoProducao.ObterResumoModelo(datInicio, datFim, CodModelo);
			}
			else
			{
				resumo = daoResumoProducao.ObterResumoModelo(datInicio, datFim, CodModelo, Familia);
			}

			return resumo;

		}

		public static string ObterLinhaTotal(ResumoProducao totais, String titulo, bool? flgIAC = false)
		{
			try
			{
				if (totais != null)
				{
					string difMes = totais.QtdDifMes < 0 ? "red" : "green",
						difApont = totais.QtdDiferenca < 0 ? "red" : "",
						difProducao = totais.QtdDifProducao < 0 ? "red" : "green";

					var linha = "<tr style='font-size: 11px; height: 15px;'><td></td><td></td><td style='text-align: right; font-weight: bold;'>" +
								titulo + "</td>" +
								"<td style='text-align: right;'>" + totais.QtdPlano + "</td>" +
								"<td style='text-align: right;'>" + totais.QtdRealizado + "</td>" +
								"<td style='text-align: right;' class='" + (flgIAC == null || flgIAC == false ? "hidden" : "") + "'>" + totais.QtdIDW + "</td>" +
								"<td style='text-align: right; font-weight: bold; color: " + difProducao + ";'>" + totais.QtdDifProducao +
								"</td>" +
								"<td style='text-align: right;'>" + totais.QtdApontado + "</td>" +
								"<td style='text-align: right; font-weight: bold; color: " + difApont + ";'>" + totais.QtdDiferenca + "</td>" +
								"<td style='text-align: right;'>" + totais.QtdPlanoVPE + "</td>" +
								"<td style='text-align: right;'>" + totais.QtdPlanoMes + "</td>" +
								"<td style='text-align: right;'>" + totais.QtdProduzidoMes + "</td>" +
								"<td style='text-align: right; font-weight: bold; color: " + difMes + ";'>" + totais.QtdDifMes + "</td></tr>";

					return linha;
				}
				else
				{
					return "<tr style='height: 15px;'><td></td><td></td><td style='text-align: right; font-weight: bold;'>" +
								titulo + "</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>" +
								"<td></td><td></td></tr>";
				}
			}
			catch (Exception)
			{
				return "<tr style='height: 15px;'><td></td><td></td><td style='text-align: right; font-weight: bold;'>" +
								titulo + "</td><td></td><td></td><td></td><td></td><td></td><td></td>" +
								"<td></td><td></td></tr>";
			}

		}
		#endregion

		#region IMC
		public static List<ResumoProducao> ObterResumoIMC(DateTime datInicio, DateTime datFim, String CodModelo)
		{

			List<ResumoProducao> resumo = new List<ResumoProducao>();

			if (String.IsNullOrEmpty(CodModelo))
			{
				resumo = daoResumoProducao.ObterResumoIMC(datInicio, datFim);
			}
			else
			{
				resumo = daoResumoProducao.ObterResumoIMC(datInicio, datFim, CodModelo);
			}

			return resumo;

		}

		public static List<ResumoProducao> ObterResumoIMCModelo(DateTime datInicio, DateTime datFim, String CodModelo)
		{

			List<ResumoProducao> resumo = daoResumoProducao.ObterResumoIMCModelo(datInicio, datFim, CodModelo);

			return resumo;

		}

		#endregion

		#region IAC
		public static List<ResumoProducao> ObterResumoIAC(DateTime datInicio, DateTime datFim, String CodModelo)
		{

			List<ResumoProducao> resumo = daoResumoProducao.ObterResumoIAC(datInicio, datFim);

			if (!String.IsNullOrEmpty(CodModelo))
			{
				resumo = resumo.Where(x => x.CodModelo == CodModelo).ToList();
			}

			return resumo;

		}

		public static List<ResumoProducao> ObterResumoIACModelo(DateTime datInicio, DateTime datFim, String CodModelo)
		{

			List<ResumoProducao> resumo = daoResumoProducao.ObterResumoIACModelo(datInicio, datFim, CodModelo);

			return resumo;

		}

		#endregion

		
	}
}
