namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_cp_hora
    {
        [Key]
        public long id_cphora { get; set; }

        public long? pcs_producaoPrevista { get; set; }

        public long id_cp_turno { get; set; }

        public long? id_calsem { get; set; }

        public long? id_calavu { get; set; }

        public virtual dw_calavu dw_calavu { get; set; }

        public virtual dw_calsem dw_calsem { get; set; }

        public virtual pp_cp_turno pp_cp_turno { get; set; }
    }
}
