using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_MAPScrapBaixaMap : EntityTypeConfiguration<tbl_MAPScrapBaixa>
    {
        public tbl_MAPScrapBaixaMap()
        {
            // Primary Key
            this.HasKey(t => t.IDScrap);

            // Properties
            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodFabBxa)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NomUsuGerou)
                .IsFixedLength()
                .HasMaxLength(20)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_MAPScrapBaixa");
            this.Property(t => t.IDScrap).HasColumnName("IDScrap");
            this.Property(t => t.DatReferencia).HasColumnName("DatReferencia");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.QtdPerda).HasColumnName("QtdPerda");
            this.Property(t => t.QtdBaixa).HasColumnName("QtdBaixa");
            this.Property(t => t.CodFabBxa).HasColumnName("CodFabBxa");
            this.Property(t => t.QtdEstoque).HasColumnName("QtdEstoque");
            this.Property(t => t.DatBaixa).HasColumnName("DatBaixa");
            this.Property(t => t.DatGeracao).HasColumnName("DatGeracao");
            this.Property(t => t.NomUsuGerou).HasColumnName("NomUsuGerou");
        }
    }
}
