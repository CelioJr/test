using System;

namespace SEMP.Model.DTO
{
    public class BarrasProducaoHDTO
    {
        public string CodFab { get; set; }
        public string CodLinha { get; set; }
        public string CodLinhaCB { get; set; }
        public Nullable<System.DateTime> DatReferencia { get; set; }
        public System.DateTime DatLeitura { get; set; }
        public string HoraLeitura { get; set; }
        public string CodOperador { get; set; }
        public string CodTestador { get; set; }
        public string CodTestador2 { get; set; }
        public string CodModelo { get; set; }
        public string NumSerieModelo { get; set; }
        public string CodControle { get; set; }
        public string NumSerieControle { get; set; }
        public string CodCinescopio { get; set; }
        public string NumSerieCinescopio { get; set; }
        public string CodOutro { get; set; }
        public string NumSerieOutro { get; set; }
        public Nullable<System.DateTime> DatExpedicao { get; set; }
        public string NumPal { get; set; }
        public string DigVerificador { get; set; }
        public string NomEstacao { get; set; }
        public string NumAP { get; set; }
		public Nullable<int> NumTurno { get; set; }
    }
}
