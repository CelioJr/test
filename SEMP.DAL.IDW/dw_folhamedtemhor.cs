namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_folhamedtemhor
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_folhamedtemhor()
        {
            dw_folhamedtemphorcfg = new HashSet<dw_folhamedtemphorcfg>();
        }

        [Key]
        public long id_folhamedtemphorario { get; set; }

        public long id_folhamedtemp { get; set; }

        public decimal hrini { get; set; }

        [Required]
        [StringLength(40)]
        public string hriniGUI { get; set; }

        public int diasemini { get; set; }

        public int diasemfim { get; set; }

        public decimal hrfim { get; set; }

        [StringLength(40)]
        public string hrfimGUI { get; set; }

        public long? id_ft_param { get; set; }

        public decimal? seg_intervalo_leitura { get; set; }

        public virtual dw_folhamedtemp dw_folhamedtemp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_folhamedtemphorcfg> dw_folhamedtemphorcfg { get; set; }

        public virtual dw_ft_param dw_ft_param { get; set; }
    }
}
