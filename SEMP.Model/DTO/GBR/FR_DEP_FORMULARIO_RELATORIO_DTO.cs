using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DEP_FORMULARIO_RELATORIO_DTO
    {
        public int FRM_CODIGO { get; set; }
        public int REL_CODIGO { get; set; }
    }
}
