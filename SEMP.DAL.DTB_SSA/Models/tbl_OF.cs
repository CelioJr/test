namespace SEMP.DAL.DTB_SSA.Models
{
    using System;
    
    public partial class tbl_OF
    {    
        public int codOF { get; set; }
        public int numQtde { get; set; }
        public string desCliente { get; set; }
        public string codModelo { get; set; }
        public string desSerieIni { get; set; }
        public string desSerieFim { get; set; }
        public string flgSoho { get; set; }
        public bool flgNotebook { get; set; }
        public bool flgMultipla { get; set; }
        public byte? validade { get; set; }
        public bool servidor { get; set; }
        public int? codOperador { get; set; }
        public DateTime? datAtualizacao { get; set; }
        public string sistema_operacional { get; set; }
        public int? imagem_id { get; set; }
        public string CodImagem { get; set; }
        public DateTime? DatImagem { get; set; }
        public int? CodOpeImagem { get; set; }
        public string flgEAN { get; set; }
        public string flgConfiguracao { get; set; }
        public string flgModelo { get; set; }
        public int? flgHCL { get; set; }
        public string flgApontamento { get; set; }
        public string flgTipoMaquina { get; set; }
        public string desHCL { get; set; }
        public int? codOfMae { get; set; }
        public string codSO { get; set; }
        public int? Qtd_Sep_Fis { get; set; }
        public int? Qtd_Emb { get; set; }
        public DateTime? datLiberacao { get; set; }
        public int? codOpeLiberacao { get; set; }
        public string flgSubTipoMaq { get; set; }

    }
}
