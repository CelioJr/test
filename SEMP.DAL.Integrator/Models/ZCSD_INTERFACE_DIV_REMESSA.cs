namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ZCSD_INTERFACE_DIV_REMESSA
    {
        public int id { get; set; }

        [Required]
        [StringLength(16)]
        public string PACKNO { get; set; }

        [Required]
        [StringLength(10)]
        public string EXTROW { get; set; }

        [StringLength(12)]
        public string NUMEROPEDIDO { get; set; }

        [StringLength(8)]
        public string NUMEROITEM { get; set; }

        [StringLength(10)]
        public string DATAREMESSA { get; set; }

        [StringLength(18)]
        public string QTDORDEM { get; set; }

        [StringLength(18)]
        public string QTDCONFIRMADA { get; set; }
    }
}
