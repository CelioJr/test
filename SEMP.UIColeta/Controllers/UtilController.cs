﻿using RAST.BO.Rastreabilidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEMP.UIColeta.Controllers
{
    public class UtilController : Controller
    {
        // GET: Util
        public ActionResult Index()
        {
            return View();
        }

		public JsonResult GetPostos(int codLinha)
		{
			var postos = Geral.ObterPostoPorLinha(codLinha);

			postos = postos.OrderBy(x => x.NumSeq).ToList();

			return Json(postos, JsonRequestBehavior.AllowGet);
		}
	}
}