﻿using System;
using System.Collections.Generic;
using System.Linq;
using SEMP.Model.DTO;
using SEMP.DAL.Models;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using SEMP.DAL.IDW;

namespace SEMP.DAL.DAO.Rastreabilidade
{
    public class daoIDW
    {
        //static daoIDW()
        //{
        //    AutoMapper.Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<v_paradas, v_paradasDTO>();
        //        cfg.CreateMap<v_producaoturno, v_producaoturnoDTO>();
        //    });
        //}

        public static List<v_paradasDTO> ObterParadasIDW(DateTime DatInicio, DateTime DatFim)
        {
            using (IDWContext idw = new IDWContext())
            {
                var paradas = (from i in idw.v_paradas
                               where i.inicio >= DatInicio && i.inicio <= DatFim && i.tempoComPeso > 5
                               select i).OrderByDescending(x => x.tempoComPeso).ToList();

                List<v_paradasDTO> newParadas = new List<v_paradasDTO>();

                AutoMapper.Mapper.Map(paradas, newParadas);

                return newParadas;
            }
        }

        public static List<v_producaoturnoDTO> ObterProducaoIDW(DateTime DatInicio, DateTime DatFim)
        {

            using (IDWContext idw = new IDWContext())
            {
                List<v_producaoturno> producao = idw.v_producaoturno.Where(x => x.dt_referencia >= DatInicio && x.dt_referencia <= DatFim).ToList();

                List<v_producaoturnoDTO> producaoDTO = new List<v_producaoturnoDTO>();

                AutoMapper.Mapper.Map(producao, producaoDTO);

                return producaoDTO;

            }
        }

        public static List<v_paradasDTO> ObterParadasIDW(DateTime DatInicio, DateTime DatFim, string codLinha)
        {
            using (IDWContext idw = new IDWContext())
            {
                //var paradas = (from i in idw.v_paradas
                //				join t in idw.v_producaoturno.Select(x => new { CodLinhaDE = x.cd_pt, CodLinhaPARA = x.cd_gt }).Distinct() on i.maquina equals t.CodLinhaDE
                //			   where i.inicio >= DatInicio && i.inicio <= DatFim && i.tempoComPeso > 5 && t.CodLinhaPARA.Trim() == codLinha
                //			   select new v_paradasDTO()
                //			   {
                //				   codigo = i.codigo,
                //				   descricao = i.descricao,
                //				   fim = i.fim,
                //				   inicio = i.inicio,
                //				   maquina = t.CodLinhaPARA,
                //				   tempoComPeso = i.tempoComPeso,
                //				   tempoSemPeso = i.tempoSemPeso
                //			   }).OrderByDescending(x => x.inicio);

                var paradas = (from i in idw.v_paradas
                               join t in idw.v_producaoturno on i.maquina equals t.cd_pt
                               where i.inicio >= DatInicio && i.inicio <= DatFim && i.tempoComPeso > 5 && t.cd_gt.Trim() == codLinha
                               select new v_paradasDTO()
                               {
                                   codigo = i.codigo,
                                   descricao = i.descricao,
                                   fim = i.fim,
                                   inicio = i.inicio,
                                   maquina = t.cd_gt,
                                   tempoComPeso = i.tempoComPeso,
                                   tempoSemPeso = i.tempoSemPeso
                               }).OrderByDescending(x => x.inicio);
                try
                {

                    var resultado = paradas.ToList();
                    return resultado;
                }
                catch (Exception)
                {
                    return new List<v_paradasDTO>();
                }
            }
        }

        public static List<LinhasDeParaIDW> ObterLinhasDePara()
        {
            using (IDWContext idw = new IDWContext())
            {
                var linhas = (from t in idw.v_producaoturno
                              select new LinhasDeParaIDW() { CodLinhaDE = t.cd_pt, CodLinhaPARA = t.cd_gt }).Distinct().ToList();
                return linhas;
            }
        }

    }
}
