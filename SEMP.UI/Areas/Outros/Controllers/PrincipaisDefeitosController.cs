﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DotNet.Highcharts;
using DotNet.Highcharts.Helpers;
using RAST.BO.Rastreabilidade;
using RAST.BO.Relatorios;
using SEMP.Model.VO.CNQ;
using SEMP.Model.VO.Rastreabilidade.Relatorios;
using SEMP.Models;
using SEMP.BO;


namespace SEMP.Areas.Outros.Controllers
{
	public class PrincipaisDefeitosController : Controller
	{
		//
		// GET: /Coleta/PrincipaisDefeitos/
		public ActionResult Index()
		{
			return View();
		}

		//
		// GET: /Coleta/PrincipaisDefeitos/Details/5
		public ActionResult Lista(
			String datInicio,
			String datFim,
			String modelo,
			string familia,
			string fase,
			string tipo)
		{

			var dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
			var dFim = dIni.AddDays(1).AddMinutes(-1);

			if (!String.IsNullOrEmpty(datInicio))
			{
				dIni = DateTime.Parse(datInicio);
				dFim = DateTime.Parse(datFim).AddDays(1).AddMinutes(-1);
			}

			var famOriginal = familia;

			if (!String.IsNullOrEmpty(familia))
			{
				switch (familia)
				{
					case "TV LCD":
						familia = "101";
						famOriginal = "LCD";
						break;
					case "AUDIO":
						familia = "104";
						famOriginal = "AUD";
						break;
					case "DVD":
						familia = "102";
						famOriginal = "DVD";
						break;
					case "PA STI":
						familia = "103";
						famOriginal = "INF";
						break;
				}
			}

			var defeitos = Defeito.ObterDefeitosCNQ(dIni, dFim, tipo, fase, familia, modelo);

			if (defeitos != null && defeitos.Any())
			{
				var resumo = defeitos.Select(x => new ResumoDefeito()
				{
					CodDefeito = x.DesCausa,
					DesDefeito = x.CodCausa,
					QtdDefeito = x.Qtde,
					Cor = Util.GetRandomColor(int.Parse(Guid.NewGuid().ToString().Substring(0, 8),
										System.Globalization.NumberStyles.HexNumber))
				}).ToList();

				ViewBag.grafDefeito = GerarGrafico(resumo, "", "chartDefeito");
				if (String.IsNullOrEmpty(familia))
				{
					ViewBag.producao = RelatorioTecnico.ObterProducaoFase(dIni, dFim, fase);
					ViewBag.resumoCNQ = Defeito.ObterResumoCNQ(dIni, dFim, fase);
				}
				else
				{
					ViewBag.producao = RelatorioTecnico.ObterProducaoFamilia(dIni, dFim, fase, famOriginal);
					ViewBag.resumoCNQ = Defeito.ObterResumoCNQ(dIni, dFim, fase, famOriginal);
				}


				return View(defeitos);
			}

			return View(new List<PrincipaisDefeitos>());
		}

		private Highcharts GerarGrafico(List<ResumoDefeito> Lista, string titulo, string idGrafico)
		{

			var listaPoint = new List<DotNet.Highcharts.Options.Point>();
			Highcharts chart = null;
			string[] categories = null;

			categories = Lista.Select(s => s.CodDefeito).ToArray();
			Lista.ForEach(f => listaPoint.Add(new DotNet.Highcharts.Options.Point { Y = f.QtdDefeito, Color = f.Cor }));
			var data = new Data(listaPoint.ToArray());
			chart = Grafico.GerarGraficoBarra(data, categories, titulo, idGrafico);

			return chart;
		}

	}
}
