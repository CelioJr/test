using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class usertable_DTO
    {
        public int id { get; set; }
        public Nullable<bool> isAdmin { get; set; }
        public Nullable<System.DateTime> lastAccess { get; set; }
        public string password { get; set; }
        public string userName { get; set; }
    }
}
