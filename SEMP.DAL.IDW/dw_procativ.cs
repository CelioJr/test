namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_procativ
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_procativ()
        {
            dw_detativ = new HashSet<dw_detativ>();
            dw_proreaativ = new HashSet<dw_proreaativ>();
        }

        [Key]
        public long id_procativ { get; set; }

        public int? ordem_grupo { get; set; }

        public int? ordem_procativ { get; set; }

        public long id_grpativ { get; set; }

        [StringLength(256)]
        public string ds_procativ { get; set; }

        public decimal? seg_tempopadrao { get; set; }

        public long id_procedimento { get; set; }

        public long? id_area { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_detativ> dw_detativ { get; set; }

        public virtual dw_grpativ dw_grpativ { get; set; }

        public virtual dw_t_area dw_t_area { get; set; }

        public virtual dw_procedimento dw_procedimento { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_proreaativ> dw_proreaativ { get; set; }
    }
}
