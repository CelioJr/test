using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_BarrasProducaoMap : EntityTypeConfiguration<tbl_BarrasProducao>
    {
        public tbl_BarrasProducaoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodLinha, t.DatReferencia, t.DatLeitura, t.CodOperador, t.CodTestador, t.CodModelo, t.NumSerieModelo, t.NomEstacao });

            // Properties
            this.Property(t => t.CodFab)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodLinha)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.CodLinhaCB)
                .IsFixedLength()
                .HasMaxLength(4)
                .IsUnicode(false);

            this.Property(t => t.HoraLeitura)
                .IsFixedLength()
                .HasMaxLength(4)
                .IsUnicode(false);

            this.Property(t => t.CodOperador)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.CodTestador)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.CodTestador2)
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumSerieModelo)
                .IsRequired()
                .HasMaxLength(13)
                .IsUnicode(false);

            this.Property(t => t.CodControle)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumSerieControle)
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.CodCinescopio)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumSerieCinescopio)
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.CodOutro)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumSerieOutro)
                .IsFixedLength()
                .HasMaxLength(8)
                .IsUnicode(false);

            this.Property(t => t.NumPal)
                .HasMaxLength(30)
                .IsUnicode(false);

            this.Property(t => t.DigVerificador)
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NomEstacao)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false);

            this.Property(t => t.NumAP)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_BarrasProducao");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.CodLinha).HasColumnName("CodLinha");
            this.Property(t => t.CodLinhaCB).HasColumnName("CodLinhaCB");
            this.Property(t => t.DatReferencia).HasColumnName("DatReferencia");
            this.Property(t => t.DatLeitura).HasColumnName("DatLeitura");
            this.Property(t => t.HoraLeitura).HasColumnName("HoraLeitura");
            this.Property(t => t.CodOperador).HasColumnName("CodOperador");
            this.Property(t => t.CodTestador).HasColumnName("CodTestador");
            this.Property(t => t.CodTestador2).HasColumnName("CodTestador2");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.NumSerieModelo).HasColumnName("NumSerieModelo");
            this.Property(t => t.CodControle).HasColumnName("CodControle");
            this.Property(t => t.NumSerieControle).HasColumnName("NumSerieControle");
            this.Property(t => t.CodCinescopio).HasColumnName("CodCinescopio");
            this.Property(t => t.NumSerieCinescopio).HasColumnName("NumSerieCinescopio");
            this.Property(t => t.CodOutro).HasColumnName("CodOutro");
            this.Property(t => t.NumSerieOutro).HasColumnName("NumSerieOutro");
            this.Property(t => t.DatExpedicao).HasColumnName("DatExpedicao");
            this.Property(t => t.NumPal).HasColumnName("NumPal");
            this.Property(t => t.DigVerificador).HasColumnName("DigVerificador");
            this.Property(t => t.NomEstacao).HasColumnName("NomEstacao");
            this.Property(t => t.NumAP).HasColumnName("NumAP");
			this.Property(t => t.NumTurno).HasColumnName("NumTurno");
        }
    }
}
