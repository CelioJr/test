using System;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
	public class ProdParadaDepItemDTO
	{
		[Display(Name="F�b.")]
		public string CodFab { get; set; }
		[Display(Name = "Num. Parada")]
		public string NumParada { get; set; }
		[Display(Name = "Departamento")]
		public string CodDepto { get; set; }
		[Display(Name = "Data Retorno")]
		public Nullable<System.DateTime> DatRetorno { get; set; }
		[Display(Name = "Causa da Parada")]
		public string DesCausa { get; set; }
		[Display(Name = "A��o Corretiva")]
		public string DesAcao { get; set; }
		[Display(Name = "Respons�vel")]
		public string NomResponsavel { get; set; }
		public System.Guid rowguid { get; set; }
		[Display(Name = "Data Email")]
		public Nullable<System.DateTime> DatEnvio { get; set; }
		[Display(Name = "Status")]
		public string FlgStatus { get; set; }
		[Display(Name = "Observa��es")]
		public string DesObs { get; set; }
		[Display(Name = "F�b.")]
		public string CodFabDepto { get; set; }

		[Display(Name = "Descri��o")]
		public string DesFabrica { get; set; }
		[Display(Name = "Descri��o")]
		public string DesDepartamento { get; set; }

	}
}
