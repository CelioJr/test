using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class TBL_GpaRecPalletMap : EntityTypeConfiguration<TBL_GpaRecPallet>
    {
        public TBL_GpaRecPalletMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IdRegistro, t.CodItem, t.DataCriacao, t.QtdPorPallet, t.QtdCarregamento });

            // Properties
            this.Property(t => t.IdRegistro)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CodItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.QtdPorPallet)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.QtdCarregamento)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Status)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("TBL_GpaRecPallet");
            this.Property(t => t.IdRegistro).HasColumnName("IdRegistro");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.DataCriacao).HasColumnName("DataCriacao");
            this.Property(t => t.QtdPorPallet).HasColumnName("QtdPorPallet");
            this.Property(t => t.QtdCarregamento).HasColumnName("QtdCarregamento");
            this.Property(t => t.Status).HasColumnName("Status");
        }
    }
}
