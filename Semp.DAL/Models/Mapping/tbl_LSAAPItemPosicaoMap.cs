using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_LSAAPItemPosicaoMap : EntityTypeConfiguration<tbl_LSAAPItemPosicao>
    {
        public tbl_LSAAPItemPosicaoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.NumAP, t.NumOP, t.CodModelo, t.CodItem, t.NumPosicao });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NumAP)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

			this.Property(t => t.NumOP)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(12)
				.IsUnicode(false);

			this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.NumPosicao)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10)
                .IsUnicode(false);

            this.Property(t => t.QtdItem)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
				.HasPrecision(13,4);

            // Table & Column Mappings
            this.ToTable("tbl_LSAAPItemPosicao");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.NumAP).HasColumnName("NumAP");
			this.Property(t => t.NumOP).HasColumnName("NumOP");
			this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.NumPosicao).HasColumnName("NumPosicao");
            this.Property(t => t.QtdItem).HasColumnName("QtdItem");
        }
    }
}
