using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_Item
    {
        public string CodItem { get; set; }
        public string DesItem { get; set; }
        public string CodDesenho { get; set; }
        public string Origem { get; set; }
        public string CodUnid { get; set; }
        public string CodFam { get; set; }
        public string CodNBM { get; set; }
        public string CodTEC { get; set; }
        public Nullable<decimal> PerIPI { get; set; }
        public string Formatacao { get; set; }
        public string FlgMecElet { get; set; }
        public string Usuario { get; set; }
        public System.Guid rowguid { get; set; }
        public string CodEmbalagem { get; set; }
        public string PartNumberTos { get; set; }
        public string FlgEmbPadrao { get; set; }
        public string CodTECSeq { get; set; }
        public string FlgMSD { get; set; }
        public string NumNivelMSD { get; set; }
        public string FlgVencto { get; set; }
        public string FlgAtivoItem { get; set; }
        public string CodTipoReceita { get; set; }
        public string CodPisCofinsReceita { get; set; }
        public string DesResumida { get; set; }
        public string FlgComercializado { get; set; }
        public string FlgItemVirtual { get; set; }
        public string CodClasPPB { get; set; }
        public Nullable<decimal> PesoLiqNac { get; set; }
    }
}
