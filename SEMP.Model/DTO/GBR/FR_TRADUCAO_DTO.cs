using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_TRADUCAO_DTO
    {
        public FR_TRADUCAO_DTO()
        {
            this.FR_TRADUCAO_IDIOMA = new List<FR_TRADUCAO_IDIOMA_DTO>();
        }

        public int TRA_CODIGO { get; set; }
        public string TRA_ITEM { get; set; }
        public string TRA_TEXTO { get; set; }
        public string TRA_TIPO { get; set; }
        public virtual ICollection<FR_TRADUCAO_IDIOMA_DTO> FR_TRADUCAO_IDIOMA { get; set; }
    }
}
