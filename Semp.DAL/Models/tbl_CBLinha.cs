using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBLinha
    {
        public int CodLinha { get; set; }
        public string DesLinha { get; set; }
        public string DesObs { get; set; }
        public string Setor { get; set; }
        public string Familia { get; set; }
        public string CodLinhaT1 { get; set; }
        public string CodLinhaT2 { get; set; }
        public string CodLinhaT3 { get; set; }
        public string CodPlaca { get; set; }
        public string CodModelo { get; set; }
        public string CodAuxiliar { get; set; }
        public string CodFam { get; set; }
        
        public int CodLinhaOrigem { get; set; }

        public string SetorOrigem { get; set; }
        public string flgAtivo { get; set; }
		public string FamiliaOrigem { get; set; }
    }
}
