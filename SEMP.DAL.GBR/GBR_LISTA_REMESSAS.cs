namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_LISTA_REMESSAS
    {
        [Key]
        public int COD_REMESSA { get; set; }

        public DateTime? DATA { get; set; }

        [StringLength(20)]
        public string USUARIO { get; set; }

        [StringLength(100)]
        public string REMESSA { get; set; }

        [StringLength(15)]
        public string IMEI { get; set; }

        [StringLength(15)]
        public string CODVENDA { get; set; }

        public int? QTY { get; set; }

        [StringLength(200)]
        public string FILE_PATH { get; set; }
    }
}
