using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_CBPerfilMascaraMap : EntityTypeConfiguration<tbl_CBPerfilMascara>
    {
        public tbl_CBPerfilMascaraMap()
        {
            // Primary Key
            this.HasKey(t => t.CodItem);

            // Properties
            this.Property(t => t.CodItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.Mascara)
                .HasMaxLength(200)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_CBPerfilMascara");
            this.Property(t => t.CodItem).HasColumnName("CodItem");
            this.Property(t => t.Mascara).HasColumnName("Mascara");
        }
    }
}
