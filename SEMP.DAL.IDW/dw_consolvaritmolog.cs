namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolvaritmolog
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_consolvaritmolog()
        {
            dw_consolvaritmologcau = new HashSet<dw_consolvaritmologcau>();
            dw_consolvaritmooco = new HashSet<dw_consolvaritmooco>();
        }

        [Key]
        public long id_consolvaritmolog { get; set; }

        public DateTime? dthr_ivaritmo { get; set; }

        public DateTime? dthr_fvaritmo { get; set; }

        public long? id_pt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolvaritmologcau> dw_consolvaritmologcau { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consolvaritmooco> dw_consolvaritmooco { get; set; }

        public virtual om_pt om_pt { get; set; }
    }
}
