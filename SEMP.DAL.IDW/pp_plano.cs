namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pp_plano
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pp_plano()
        {
            pp_cp = new HashSet<pp_cp>();
            pp_plancol = new HashSet<pp_plancol>();
            pp_planec = new HashSet<pp_planec>();
            pp_planeccron = new HashSet<pp_planeccron>();
            pp_planptgt = new HashSet<pp_planptgt>();
        }

        [Key]
        public long id_plano { get; set; }

        [StringLength(30)]
        public string cd_plano { get; set; }

        public int? revisao { get; set; }

        public short? is_modelo { get; set; }

        public DateTime? dt_revisao { get; set; }

        public DateTime? dt_stativo { get; set; }

        public short? st_ativo { get; set; }

        public short? st_plano { get; set; }

        public long id_tpplano { get; set; }

        public long? id_usrRevisao { get; set; }

        public long? id_usrstativo { get; set; }

        [StringLength(100)]
        public string ds_plano { get; set; }

        public DateTime? dthr_previsaoinicio { get; set; }

        public decimal? ind_oee { get; set; }

        public short? is_considerarRAP { get; set; }

        public short? is_considerarMP { get; set; }

        public short? is_considerarEst { get; set; }

        public short? is_considerarCal { get; set; }

        public short? is_considerarIndisp { get; set; }

        public short? is_considerarMO { get; set; }

        public short? is_determinadoCal { get; set; }

        public long? id_cal { get; set; }

        public short? is_considerarOEEFinalSerie { get; set; }

        public short? is_considerarProdutoTurno { get; set; }

        public short? is_considerarCM { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? is_simular { get; set; }

        public virtual dw_cal dw_cal { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_cp> pp_cp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_plancol> pp_plancol { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_planec> pp_planec { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_planeccron> pp_planeccron { get; set; }

        public virtual pp_tpplano pp_tpplano { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pp_planptgt> pp_planptgt { get; set; }
    }
}
