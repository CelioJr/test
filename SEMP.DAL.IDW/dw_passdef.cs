namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_passdef
    {
        [Key]
        public long id_passdef { get; set; }

        public long id_passagem { get; set; }

        public long id_tdefeito { get; set; }

        public long? id_passcau { get; set; }

        public long? id_area { get; set; }

        [StringLength(200)]
        public string ds_posicaomecanica { get; set; }

        public virtual dw_passagem dw_passagem { get; set; }

        public virtual dw_passcau dw_passcau { get; set; }

        public virtual dw_t_area dw_t_area { get; set; }

        public virtual dw_t_defeito dw_t_defeito { get; set; }
    }
}
