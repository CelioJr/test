namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dw_consolmedparamlog
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dw_consolmedparamlog()
        {
            dw_consol_parammed = new HashSet<dw_consol_parammed>();
        }

        [Key]
        public long id_consolmedparamlog { get; set; }

        public DateTime dthr_medicao { get; set; }

        public int? ms_dthrmedicao { get; set; }

        public decimal vlr_lido { get; set; }

        public long id_ft_param { get; set; }

        public long? id_rtcic { get; set; }

        public decimal? vl_monetario { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dw_consol_parammed> dw_consol_parammed { get; set; }

        public virtual dw_ft_param dw_ft_param { get; set; }

        public virtual dw_ft_param dw_ft_param1 { get; set; }

        public virtual dw_ft_param dw_ft_param2 { get; set; }

        public virtual dw_rtcic dw_rtcic { get; set; }
    }
}
