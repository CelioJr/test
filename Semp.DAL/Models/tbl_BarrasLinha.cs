using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_BarrasLinha
    {
        public string CodLinhaCB { get; set; }
        public string CodLinha { get; set; }
        public int flgFabrica { get; set; }
        public string DesLinha { get; set; }
        public string WStation { get; set; }
		public string CodLinhaRel { get; set; }
        public string CodLinhaT1 { get; set; }
        public string CodLinhaT2 { get; set; }
        public string CodLinhaT3 { get; set; }
        public string Setor { get; set; }
        public string Produto { get; set; }
        public string CodFam { get; set; }
        public string flgSeqLeit { get; set; }
        public string HoraSaiIntervalo1 { get; set; }
        public string HoraRetIntervalo1 { get; set; }
        public string HoraSaiIntervalo2 { get; set; }
        public string HoraRetIntervalo2 { get; set; }
        public string HoraSaiAlmoco { get; set; }
        public string HoraRetAlmoco { get; set; }
        public string HoraSaiJanta { get; set; }
        public string HoraRetJanta { get; set; }
        public string HoraEntrada { get; set; }
        public string HoraSaida { get; set; }
        public string flgAtivo { get; set; }
        public string flgStatus { get; set; }
        public string Familia { get; set; }
        public string HoraEntradaT2 { get; set; }
        public string HoraSaidaT2 { get; set; }
        public string NomeEstacaoExp { get; set; }
    }
}
