using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_DepartamentoMap : EntityTypeConfiguration<tbl_Departamento>
    {
        public tbl_DepartamentoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.CodDepto });

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.CodDepto)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.NomDepto)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(25)
                .IsUnicode(false);

            this.Property(t => t.NumDepto)
                .IsFixedLength()
                .HasMaxLength(3)
                .IsUnicode(false);

            this.Property(t => t.FlgAtivo)
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_Departamento");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.CodDepto).HasColumnName("CodDepto");
            this.Property(t => t.NomDepto).HasColumnName("NomDepto");
            this.Property(t => t.NumDepto).HasColumnName("NumDepto");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
            this.Property(t => t.FlgAtivo).HasColumnName("FlgAtivo");
        }
    }
}
