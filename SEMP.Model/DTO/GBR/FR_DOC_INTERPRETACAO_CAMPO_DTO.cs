using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DOC_INTERPRETACAO_CAMPO_DTO
    {
        public string CAM_PADRAO { get; set; }
        public string CAM_TIPO { get; set; }
        public int DOC_CODIGO { get; set; }
        public virtual FR_DOC_PRINCIPAL_DTO FR_DOC_PRINCIPAL { get; set; }
    }
}
