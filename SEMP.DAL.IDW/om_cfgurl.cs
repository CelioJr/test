namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_cfgurl
    {
        [Key]
        public long id_cfgurl { get; set; }

        [StringLength(255)]
        public string url_conexao { get; set; }

        public long? id_cfg { get; set; }

        [StringLength(100)]
        public string ds_url { get; set; }

        public int? utc { get; set; }

        public virtual om_cfg om_cfg { get; set; }
    }
}
