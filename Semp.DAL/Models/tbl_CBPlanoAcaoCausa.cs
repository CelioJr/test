using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBPlanoAcaoCausa
    {
        public int CodPlano { get; set; }
        public int CodPlanoCausa { get; set; }
        public string DesCausa { get; set; }
        public Nullable<int> CodTipoCausa { get; set; }
        public Nullable<int> FlgStatus { get; set; }
        public Nullable<System.DateTime> DatInicio { get; set; }
        public Nullable<System.DateTime> DatFim { get; set; }
        public string NomResponsavel { get; set; }
    }
}
