namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GBR_CAD_ETIQUETA_STRING
    {
        [Key]
        public int COD_ETIQUETA { get; set; }

        [StringLength(30)]
        public string DESCRICAO { get; set; }

        [StringLength(8000)]
        public string STRING { get; set; }

        public int? COD_SKU { get; set; }

        public DateTime? DATA { get; set; }

        [StringLength(10)]
        public string TIPO { get; set; }

        public int? COD_MODELO { get; set; }
    }
}
