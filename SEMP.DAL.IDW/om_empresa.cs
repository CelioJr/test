namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_empresa
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_empresa()
        {
            om_cfg = new HashSet<om_cfg>();
            om_licenca = new HashSet<om_licenca>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id_empresa { get; set; }

        [StringLength(60)]
        public string cd_empresa { get; set; }

        [StringLength(256)]
        public string ds_empresa { get; set; }

        [StringLength(256)]
        public string endereco { get; set; }

        [StringLength(256)]
        public string cidade { get; set; }

        [StringLength(60)]
        public string estado { get; set; }

        [StringLength(60)]
        public string pais { get; set; }

        [StringLength(512)]
        public string chave_verificacao { get; set; }

        public byte[] img_logotipo { get; set; }

        public byte[] img_background { get; set; }

        public byte[] img_rodape { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_cfg> om_cfg { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_licenca> om_licenca { get; set; }
    }
}
