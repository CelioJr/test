using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
	public partial class tbl_IAMCFamilia
	{
		public string NomFam { get; set; }
		public string CodFam { get; set; }
		public int? NumOrdem { get; set; }
	}
}
