using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class FR_DOC_CASO_USO_DTO
    {
        public FR_DOC_CASO_USO_DTO()
        {
            this.FR_DOC_CASO_USO_EXTENSAO = new List<FR_DOC_CASO_USO_EXTENSAO_DTO>();
            this.FR_DOC_CASO_USO_EXTENSAO1 = new List<FR_DOC_CASO_USO_EXTENSAO_DTO>();
            this.FR_DOC_CASO_USO_GENERALIZACAO = new List<FR_DOC_CASO_USO_GENERALIZACAO_DTO>();
            this.FR_DOC_CASO_USO_GENERALIZACAO1 = new List<FR_DOC_CASO_USO_GENERALIZACAO_DTO>();
            this.FR_DOC_CASO_USO_INCLUSAO = new List<FR_DOC_CASO_USO_INCLUSAO_DTO>();
        }

        public int DOC_CODIGO { get; set; }
        public int USO_CODIGO { get; set; }
        public string USO_NOME { get; set; }
        public string USO_DESCRICAO { get; set; }
        public string USO_REQUISITO { get; set; }
        public string USO_VALIDACAO { get; set; }
        public string USO_CENARIO { get; set; }
        public virtual ICollection<FR_DOC_CASO_USO_EXTENSAO_DTO> FR_DOC_CASO_USO_EXTENSAO { get; set; }
        public virtual ICollection<FR_DOC_CASO_USO_EXTENSAO_DTO> FR_DOC_CASO_USO_EXTENSAO1 { get; set; }
        public virtual ICollection<FR_DOC_CASO_USO_GENERALIZACAO_DTO> FR_DOC_CASO_USO_GENERALIZACAO { get; set; }
        public virtual ICollection<FR_DOC_CASO_USO_GENERALIZACAO_DTO> FR_DOC_CASO_USO_GENERALIZACAO1 { get; set; }
        public virtual ICollection<FR_DOC_CASO_USO_INCLUSAO_DTO> FR_DOC_CASO_USO_INCLUSAO { get; set; }
        public virtual FR_DOC_PRINCIPAL_DTO FR_DOC_PRINCIPAL { get; set; }
    }
}
