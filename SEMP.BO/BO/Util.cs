﻿using System;
using System.Drawing;
using System.Text.RegularExpressions;
using SEMP.DAL.Common;
using Microsoft.VisualBasic;
using System.Collections.Generic;
using SEMP.Model.DTO;
using System.Linq;

namespace SEMP.BO
{
	public class Util
	{
        public static void IniciarMapper()
        {
            DAL.IniciarMapper.Iniciar();
        }

		public static DateTime GetDate() {

			return daoUtil.GetDate();

		}

		public static bool IsCpf(string cpf)
		{
			int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
			int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
			string tempCpf;
			string digito;
			int soma;
			int resto;
			cpf = cpf.Trim();
			cpf = cpf.Replace(".", "").Replace("-", "");
			{
				Int64 d = 0;
				if (cpf.Length != 11 || !Int64.TryParse(cpf, out d))
					return false;
			}
			tempCpf = cpf.Substring(0, 9);
			soma = 0;

			for (int i = 0; i < 9; i++)
				soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
			resto = soma % 11;
			if (resto < 2)
				resto = 0;
			else
				resto = 11 - resto;
			digito = resto.ToString();
			tempCpf = tempCpf + digito;
			soma = 0;
			for (int i = 0; i < 10; i++)
				soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
			resto = soma % 11;
			if (resto < 2)
				resto = 0;
			else
				resto = 11 - resto;
			digito = digito + resto.ToString();
			return cpf.EndsWith(digito);
		}

		public static bool IsNumeric(string valor)
		{
			int valorInteiro;

			//Validando se é inteiro utilizando o recurso TryParse
			//No método passamos primeiro uma string, e depois a saída "out"
			return (int.TryParse(valor, out valorInteiro));

		}

		public static string ToNumerical(string str)
		{
			if (str != null & str.Length > 0)
				str = Regex.Replace(str, @"[^0-9]", "");
			return str;
		}

		public static string RetornaNomeCor()
		{
			Random random = new Random();
			int r = int.Parse(random.Next(255).ToString());
			int g = int.Parse(random.Next(255).ToString());
			int b = int.Parse(random.Next(255).ToString());
			int a = int.Parse(random.Next(255).ToString());
			string cor = string.Format("#{0}", Color.FromArgb(a, r, g, b).Name.ToUpper().Substring(0, 6));
			return cor;
		}

		public static Color GetRandomColor(int numero)
		{
			Random random = new Random(numero);
			return Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
			// The error is here
		}

		public static int NumberOfWeeks(DateTime dateFrom, DateTime dateTo)
		{
			TimeSpan Span = dateTo.Subtract(dateFrom);

			if (Span.Days <= 7)
			{
				if (dateFrom.DayOfWeek > dateTo.DayOfWeek)
				{
					return 2;
				}

				return 1;
			}

			int Days = Span.Days - 7 + (int)dateFrom.DayOfWeek;
			int WeekCount = 1;
			int DayCount = 0;

			for (WeekCount = 1; DayCount < Days; WeekCount++)
			{
				DayCount += 7;
			}

			return WeekCount;
		}

		public static int QtdSemanas(DateTime datIni, DateTime datFim)
		{
			var qtdSemanas = 0;

			var data = datIni;

			while (data < datFim)
			{
				data = data.AddDays(1);

				if (data.DayOfWeek == DayOfWeek.Sunday)
				{
					qtdSemanas++;
				}

			}

			return qtdSemanas;

		}

		public static long DateDiff(string interval, DateTime startDate, DateTime endDate)
		{
			return DateAndTime.DateDiff(interval, startDate, endDate);

		}

		public static List<CBAcessoDTO> ObterAcessos(string Acesso, string Depto, string User)
		{

			var dados = daoUtil.ObterAcessos(Acesso, Depto, User);

			return dados;

		}

		public static bool VerPermissaoAcesso(string Acesso, string Depto, string User) {

			var dados = daoUtil.ObterAcessos(Acesso);

			if (!String.IsNullOrEmpty(Depto))
			{
				var acessoDepto = dados.Where(x => x.AcessoDepto.Contains(Depto)).ToList();

				if (acessoDepto != null && acessoDepto.Count > 0)
				{

					return true;

				}
			}

			if (!String.IsNullOrEmpty(User))
			{
				var acessoUser = dados.Where(x => x.AcessoUser != null && x.AcessoUser.Contains(User)).ToList();

				if (acessoUser != null && acessoUser.Count > 0)
				{

					return true;

				}
			}
			return false;

		}

		public static decimal CalculaPPM(int QtdDefeitos, int QtdProduzido, int QtdPontos)
		{

			decimal ppm = ((decimal)QtdDefeitos / (QtdProduzido * QtdPontos) * 1000000);
			return ppm;

		}

	}

}
