namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_im
    {
        [Key]
        public long id_im { get; set; }

        public DateTime? dthr_im { get; set; }

        [StringLength(256)]
        public string msg { get; set; }

        public long id_usrsrc { get; set; }

        public long id_usrtrg { get; set; }

        public virtual om_usr om_usr { get; set; }

        public virtual om_usr om_usr1 { get; set; }
    }
}
