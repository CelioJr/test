using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.DTB_SSA.Models.Mapping
{
    public class tbl_DispositivoIntegradoMap : EntityTypeConfiguration<tbl_DispositivoIntegrado>
    {
        public tbl_DispositivoIntegradoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.NE, t.dispositivo_id });

            // Properties
            this.Property(t => t.NE)
                .IsRequired()
                .HasMaxLength(13);

            this.Property(t => t.dispositivo_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("tbl_DispositivoIntegrado");
            this.Property(t => t.NE).HasColumnName("NE");
            this.Property(t => t.dispositivo_id).HasColumnName("dispositivo_id");
            this.Property(t => t.frequencia).HasColumnName("frequencia");

        }
    }
}
