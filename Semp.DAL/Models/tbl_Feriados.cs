using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_Feriados
    {
        public string Setor { get; set; }
        public System.DateTime DatFeriado { get; set; }
        public string DesFeriado { get; set; }
        public string DiaUtil { get; set; }
    }
}
