namespace SEMP.Model.DTO
{
	using System;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;

	public partial class ZSSD_GET_CREATE_ORDER_DTO
	{
        [Key]
        [Column(Order = 0)]
        public Guid ORDER_HEADER_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        public DateTime UPDATE_TIME { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(16)]
        public string PACKNO { get; set; }

        [StringLength(5)]
        public string EXTROW { get; set; }

        [StringLength(5)]
        public string TYPE { get; set; }

        [StringLength(10)]
        public string ID { get; set; }

        [StringLength(10)]
        public string NUMBER { get; set; }

        [StringLength(100)]
        public string MESSAGE { get; set; }

        [StringLength(100)]
        public string LOG_NO { get; set; }

        [StringLength(100)]
        public string LOG_MSG_NO { get; set; }

        [StringLength(100)]
        public string MESSAGE_V1 { get; set; }

        [StringLength(100)]
        public string MESSAGE_V2 { get; set; }

        [StringLength(100)]
        public string MESSAGE_V3 { get; set; }

        [StringLength(100)]
        public string MESSAGE_V4 { get; set; }

        [StringLength(100)]
        public string PARAMETER { get; set; }

        [StringLength(100)]
        public string ROW { get; set; }

        [StringLength(100)]
        public string FIELD { get; set; }

        [StringLength(100)]
        public string SYSTEM { get; set; }
    }
}
