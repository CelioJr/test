﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.DTO
{
	public class CBTrocaItemDTO
	{
		public int CodRetrabalho { get; set; }
		public string CodItemAnterior { get; set; }
		public string CodItemNovo { get; set; }
		public Nullable<int> TipoAmarra { get; set; }
	}
}
