﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.Model.VO.Rastreabilidade.Relatorios
{
	public class DefeitosHora
	{
		public int Hora { get; set; }
		public int QtdDefeitos { get; set; }
		public int QtdAcumulado { get; set; }
		public int QtdProduzido { get; set; }
		public int QtdProduzidoAcum { get; set; }
		public int QtdPontos { get; set; }

		public double IndDefeito
		{
			get
			{
				if (QtdProduzido > 0 && QtdDefeitos > 0)
				{
					var indice = Math.Round( ((double)QtdDefeitos / QtdProduzido) * 100.0, 2);
					return indice;
				}
				else
				{
					return 0;
				}
			}
		}

		public double IndDefeitoAcum
		{
			get
			{
				if (QtdProduzidoAcum > 0 && QtdAcumulado > 0)
				{
					var indice = Math.Round(((double)QtdAcumulado / QtdProduzidoAcum) * 100.0, 2);
					return indice;
				}
				else
				{
					return 0;
				}
			}
		}

		public decimal PPM {
		get { 
				var ppm = ((decimal)QtdDefeitos / (QtdProduzido * QtdPontos) * 1000000);
				return ppm;
			} 
		}

		public double PPMAcum
		{
			get
			{
				if (QtdProduzidoAcum > 0 && QtdAcumulado > 0)
				{
					var indice = Math.Round(((double)QtdAcumulado / (QtdProduzidoAcum * QtdPontos)) * 1000000, 2);
					return indice;
				}
				else
				{
					return 0;
				}
			}
		}

		public System.Drawing.Color Cor { get; set; }
	}
}
