namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_GRUPO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FR_GRUPO()
        {
            FR_PERMISSAO = new HashSet<FR_PERMISSAO>();
            FR_USUARIO = new HashSet<FR_USUARIO>();
        }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int GRP_CODIGO { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(3)]
        public string SIS_CODIGO { get; set; }

        [Required]
        [StringLength(40)]
        public string GRP_NOME { get; set; }

        [StringLength(2000)]
        public string GRP_FILTRO_DICIONARIO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_PERMISSAO> FR_PERMISSAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FR_USUARIO> FR_USUARIO { get; set; }
    }
}
