namespace SEMP.Model.DTO
{
    public partial class FabricaDTO
    {
        public string CodFab { get; set; }
        public string NomFab { get; set; }
        public string Sigla { get; set; }
        public string SiglaEDP { get; set; }
        public string NumCGC { get; set; }
        public string CodLocalFat { get; set; }
        public string NumApolice { get; set; }
        public string RazaoSocial { get; set; }
        public string EndFabrica { get; set; }
        public string NumSUF { get; set; }
        public string NumCEX { get; set; }
        public string NomAtend { get; set; }
        public System.Guid rowguid { get; set; }
        public string NumInscEst { get; set; }
        public string DTB_Operation { get; set; }
        public string CodMatriz { get; set; }
    }
}
