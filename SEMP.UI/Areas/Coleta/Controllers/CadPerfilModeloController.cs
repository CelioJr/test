﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using RAST.BO.ApontaProd;
using SEMP.Model.DTO;
using SEMP.Model.VO.Rastreabilidade;

namespace SEMP.Areas.Coleta.Controllers
{
	public class CadPerfilModeloController : Controller
	{
		#region Consultas
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult Lista(string codLinha, string codModelo)
		{
			if (String.IsNullOrEmpty(codLinha))
			{
				codLinha = "0";
			}

			var dados = ProducaoHoraAHoraBO.ObterPerfilLinhaPosto(int.Parse(codLinha), codModelo) ??
			            new List<CBLinhaPostoPerfilDTO>();

			return View(dados);
		}

		public ActionResult ListaTempos(string datIni, string datFim, int? codLinha, string horaInicio, string horaFim)
		{

			try
			{
				var dIni = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
				var dFim = dIni.AddDays(1).AddMinutes(-1);

				if (!String.IsNullOrEmpty(datIni))
				{
					dIni = DateTime.Parse(datIni);
				}
				if (!String.IsNullOrEmpty(datFim))
				{
					dFim = DateTime.Parse(datFim);
				}

				var dados = codLinha != null ? ProducaoHoraAHoraBO.ObterTempoMedioLinha(dIni, dFim, codLinha.Value, horaInicio, horaFim) ??
							new List<CBTempoMedioLinha>() : new List<CBTempoMedioLinha>();

				return View(dados);
			}
			catch
			{
				return View(new List<CBTempoMedioLinha>());
			}
			
		}

		#endregion

		#region Manutenção
		//
		// GET: /Coleta/CadPerfilModelo/Create
		public ActionResult Create()
		{
			return View();
		}

		//
		// POST: /Coleta/CadPerfilModelo/Create
		[HttpPost]
		public ActionResult Create(CBLinhaPostoPerfilDTO dado)
		{
			try
			{
				ProducaoHoraAHoraBO.GravarPerfilLinhaPosto(dado);

				var json = new
				{
					message = "Capacidade inserida com sucesso.",
					retorno = true
				};
				return Json(json, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				var json = new
				{
					message = "Não foi possível gravar.",
					retorno = false
				};
				return Json(json, JsonRequestBehavior.AllowGet);
			}
		}

		//
		// GET: /Coleta/CadPerfilModelo/Edit/5
		public ActionResult Edit(int codLinha, int numPosto, string codModelo)
		{
			var dado = ProducaoHoraAHoraBO.ObterPerfilLinhaPosto(codLinha, numPosto, codModelo);

			return View(dado ?? new CBLinhaPostoPerfilDTO());
		}

		//
		// POST: /Coleta/CadPerfilModelo/Edit/5
		[HttpPost]
		public ActionResult Edit(CBLinhaPostoPerfilDTO dado)
		{
			try
			{
				ProducaoHoraAHoraBO.GravarPerfilLinhaPosto(dado);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}

		//
		// GET: /Coleta/CadPerfilModelo/Delete/5
		public ActionResult Delete(int codLinha, int numPosto, string codModelo)
		{
			var dado = ProducaoHoraAHoraBO.ObterPerfilLinhaPosto(codLinha, numPosto, codModelo);

			return View(dado ?? new CBLinhaPostoPerfilDTO());
		}

		//
		// POST: /Coleta/CadPerfilModelo/Delete/5
		[HttpPost]
		public ActionResult Delete(CBLinhaPostoPerfilDTO dado)
		{
			try
			{
				ProducaoHoraAHoraBO.RemoverPerfilLinhaPosto(dado);

				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}
		}

		#endregion

		#region Outros

		public ActionResult GravarTempo(int codLinha, int numPosto, string codModelo, decimal tempoMedio)
		{

			try
			{
				ProducaoHoraAHoraBO.GravarTempo(codLinha, numPosto, codModelo, tempoMedio);
				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json(false, JsonRequestBehavior.AllowGet);	
			}
			
			
		}

		#endregion

	}
}
