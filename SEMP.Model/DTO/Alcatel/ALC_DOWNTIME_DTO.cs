using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_DOWNTIME_DTO
    {
        public decimal ID_DOWNTIME { get; set; }
        public Nullable<System.DateTime> DT_ENTRY { get; set; }
        public string FAILURE { get; set; }
        public Nullable<int> HOUR_TARGET { get; set; }
        public string INTERVAL { get; set; }
        public Nullable<System.DateTime> LOST_MINUTES { get; set; }
        public Nullable<int> PRODUCED { get; set; }
        public Nullable<int> COD_LINE { get; set; }
        public string PPB { get; set; }
        public Nullable<int> COD_SKU { get; set; }
        public Nullable<int> DEPARTMENT { get; set; }
        public Nullable<int> MODEL { get; set; }
        public string SHIFT { get; set; }
        public Nullable<System.DateTime> DT_CREATED { get; set; }
        public string ID_FAILURE { get; set; }
        public string OBSERVATION { get; set; }
        public string ID_OP { get; set; }
        public virtual ALC_DEPARTMENT_DTO ALC_DEPARTMENT { get; set; }
        public virtual ALC_PPB_DTO ALC_PPB { get; set; }
        public virtual ALC_DOWNTIME_DTO ALC_DOWNTIME1 { get; set; }
        public virtual ALC_DOWNTIME_DTO ALC_DOWNTIME2 { get; set; }
        public virtual ALC_ORDER_DTO ALC_ORDER { get; set; }
        public virtual ALC_FAILURE_DTO ALC_FAILURE { get; set; }
    }
}
