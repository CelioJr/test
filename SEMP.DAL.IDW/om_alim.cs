namespace SEMP.DAL.IDW
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class om_alim
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public om_alim()
        {
            om_alimrea = new HashSet<om_alimrea>();
            om_pt = new HashSet<om_pt>();
            om_pt1 = new HashSet<om_pt>();
            om_pt2 = new HashSet<om_pt>();
        }

        [Key]
        public long id_alim { get; set; }

        [StringLength(30)]
        public string cd_alim { get; set; }

        [StringLength(256)]
        public string ds_alim { get; set; }

        public DateTime? dt_stativo { get; set; }

        public long? id_usrstativo { get; set; }

        public short? tp_alim { get; set; }

        public short? st_alim { get; set; }

        public long id_mapa { get; set; }

        public virtual om_mapa om_mapa { get; set; }

        public virtual om_usr om_usr { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_alimrea> om_alimrea { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_pt> om_pt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_pt> om_pt1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<om_pt> om_pt2 { get; set; }
    }
}
