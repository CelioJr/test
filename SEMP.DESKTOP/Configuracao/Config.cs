﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DESKTOP.Configuracao
{
    public static class Config
    {

        public static string porta
        {
            get 
            { 
                return Properties.Settings.Default.porta; 
            }
            set 
            {
                Properties.Settings.Default.porta = value;
                Properties.Settings.Default.Save();
            }
        }

        public static string velocidade
        {
            get
            {
                return Properties.Settings.Default.velocidade;
            }
            set
            {
                Properties.Settings.Default.velocidade = value;
                Properties.Settings.Default.Save();
            }
        }

        public static string bitDados
        {
            get
            {
                return Properties.Settings.Default.bitDados;
            }
            set
            {
                Properties.Settings.Default.bitDados = value;
                Properties.Settings.Default.Save();
            }
        }

        public static string paridade
        {
            get
            {
                return Properties.Settings.Default.paridade;
            }
            set
            {
                Properties.Settings.Default.paridade = value;
                Properties.Settings.Default.Save();
            }
        }

        public static string bitParada
        {
            get
            {
                return Properties.Settings.Default.bitParada;
            }
            set
            {
                Properties.Settings.Default.bitParada = value;
                Properties.Settings.Default.Save();
            }
        }

        public static string fluxo
        {
            get
            {
                return Properties.Settings.Default.fluxo;
            }
            set
            {
                Properties.Settings.Default.fluxo = value;
                Properties.Settings.Default.Save();
            }
        }

        public static string fabrica
        {
            get
            {
                return Properties.Settings.Default.fabrica;
            }
            set
            {
                Properties.Settings.Default.fabrica = value;
                Properties.Settings.Default.Save();
            }
        }

        public static string linha
        {
            get
            {
                return Properties.Settings.Default.linha;
            }
            set
            {
                Properties.Settings.Default.linha = value;
                Properties.Settings.Default.Save();
            }
        }

        public static string posto
        {
            get
            {
                return Properties.Settings.Default.posto;
            }
            set
            {
                Properties.Settings.Default.posto = value;
                Properties.Settings.Default.Save();
            }
        }

        public static string codigoFabrica
        {
            get
            {
                return Properties.Settings.Default.codigoFabrica;
            }
            set
            {
                Properties.Settings.Default.codigoFabrica = value;
                Properties.Settings.Default.Save();
            }
        }

        public static string numAP
        {
            get
            {
                return Properties.Settings.Default.numAP;
            }
            set
            {
                Properties.Settings.Default.numAP = value;
                Properties.Settings.Default.Save();
            }
        }

        public static string modelo
        {
            get
            {
                return Properties.Settings.Default.modelo;
            }
            set
            {
                Properties.Settings.Default.modelo = value;
                Properties.Settings.Default.Save();
            }
        }

    }
}
