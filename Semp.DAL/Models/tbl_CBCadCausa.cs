using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBCadCausa
    {
        public string CodCausa { get; set; }
        public string DesCausa { get; set; }
        public Nullable<short> flgIMC { get; set; }
        public Nullable<short> flgMONTAGEM { get; set; }
        public string flgAtivo { get; set; }
    }
}
