namespace SEMP.Model.DTO
{
	using System;
	using System.ComponentModel.DataAnnotations;

	public partial class ORDER_CONDITIONS_IN_DTO
    {
        public Guid ID { get; set; }

        public Guid ORDER_ITEMS_IN_ID { get; set; }

        [StringLength(100)]
        public string ITM_NUMBER { get; set; }

        [StringLength(4)]
        public string COND_TYPE { get; set; }

        public decimal? COND_VALUE { get; set; }

        [StringLength(5)]
        public string KOEIN { get; set; }
    }
}
