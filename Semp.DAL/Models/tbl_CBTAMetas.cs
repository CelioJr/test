using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBTAMetas
    {
        public string CodFase { get; set; }
        public string SubFase { get; set; }
        public Nullable<decimal> Meta { get; set; }
        public string CodModelo { get; set; }
        public int Ordem { get; set; }
    }
}
