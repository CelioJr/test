using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_TempoPadraoMap : EntityTypeConfiguration<tbl_TempoPadrao>
    {
        public tbl_TempoPadraoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodModelo, t.rowguid });

            // Properties
            this.Property(t => t.CodModelo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.CodPai)
                .IsFixedLength()
                .HasMaxLength(6)
                .IsUnicode(false);

            this.Property(t => t.Usuario)
                .IsFixedLength()
                .HasMaxLength(25)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_TempoPadrao");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.TempoPadrao).HasColumnName("TempoPadrao");
            this.Property(t => t.TempoLp).HasColumnName("TempoLp");
            this.Property(t => t.CodPai).HasColumnName("CodPai");
            this.Property(t => t.Usuario).HasColumnName("Usuario");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
        }
    }
}
