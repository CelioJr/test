using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class PACKING_SERIAL_VIEW_DTO
    {
        public int ID_SERIAL { get; set; }
        public Nullable<System.DateTime> DATE_COLETIVA { get; set; }
        public Nullable<System.DateTime> DATE_FT { get; set; }
        public Nullable<System.DateTime> DATE_IMEI { get; set; }
        public Nullable<System.DateTime> DATE_PALLET { get; set; }
        public Nullable<System.DateTime> DT_CREATION { get; set; }
        public string HOSTNAME { get; set; }
        public string IMEI_1 { get; set; }
        public string IMEI_2 { get; set; }
        public string IMEI_3 { get; set; }
        public string IMEI_4 { get; set; }
        public Nullable<int> LINE_COLETIVA { get; set; }
        public Nullable<int> LINE_FT { get; set; }
        public Nullable<int> LINE_IMEI { get; set; }
        public Nullable<int> LINE_INDIVIDUAL { get; set; }
        public Nullable<int> LINE_PALLET { get; set; }
        public string PACKING { get; set; }
        public string SERIAL { get; set; }
    }
}
