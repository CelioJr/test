﻿using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.Model;
using SEMP.Model.DTO;
using SEMP.Model.VO.Rastreabilidade;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RAST.BO.Rastreabilidade
{
	public class PostoQueima
	{

		#region Consultas Testes (Defeitos)

		public static List<CBCadDefeitoDTO> ObterTestesQueima()
		{

			return daoPostoQueima.ObterTestesQueima();

		}

		public static List<CBCadSubDefeitoDTO> ObterTestesEspecificosQueima()
		{

			return daoPostoQueima.ObterTestesEspecificosQueima();

		}

		#endregion


		#region Consultas Queima
		public static List<QueimaResumoInspecao> ObterResumoInspecao(DateTime datInicio, DateTime datFim)
		{
			return daoPostoQueima.ObterResumoInspecao(datInicio, datFim);
		
		}

		public static CBLoteConfigDTO ObterConfigLote(int codLinha)
		{

			return daoPostoQueima.ObterConfigLote(codLinha);

		}

		public static List<CBQueimaRegistroDTO> ObterRegistros(DateTime datInicio, DateTime datFim)
		{

			return daoPostoQueima.ObterRegistros(datInicio, datFim);
		
		}

		public static List<CBQueimaRegistroDTO> ObterRegistrosDefeitos(DateTime datInicio)
		{

			return daoPostoQueima.ObterRegistrosStatus(datInicio, "X");

		}

		public static CBQueimaRegistroDTO ObterRegistro(string codFab, string numRPA, string codPallet)
		{

			return daoPostoQueima.ObterRegistro(codFab, numRPA, codPallet);

		}

		public static CBQueimaRegistroDTO ObterRegistro(string codFab, string numRPA)
		{

			return daoPostoQueima.ObterRegistro(codFab, numRPA);

		}

		public static List<QueimaRelatorioInspecao> ObterInspecoes(DateTime datInicio, DateTime datFim)
		{
			var dados = daoPostoQueima.ObterInspecoes(datInicio, datFim);
			return dados;

		}

		public static List<CBQueimaInspecaoDTO> ObterInspecoes(string codFab, string numRPA, string codPallet)
		{

			if (String.IsNullOrEmpty(codPallet))
			{
				return daoPostoQueima.ObterInspecoes(codFab, numRPA);
			}
			else
			{
				return daoPostoQueima.ObterInspecoes(codFab, numRPA, codPallet);
			}

		}

		public static CBQueimaInspecaoDTO ObterInspecaoSerie(string numECB, string codFab, string numRPA)
		{

			return daoPostoQueima.ObterInspecaoSerie(numECB, codFab, numRPA);

		}

		public static List<CBQueimaTestesDTO> ObterTestes(string codFab, string numRPA, string numECB)
		{
			if (String.IsNullOrEmpty(numECB))
			{
				return daoPostoQueima.ObterTestes(codFab, numRPA);
			}
			else
			{
				return daoPostoQueima.ObterTestes(codFab, numRPA, numECB);
			}
			

		}

		public static List<CBQueimaTestesDTO> ObterTestes(string codFab, string numRPA)
		{
			return daoPostoQueima.ObterTestes(codFab, numRPA);
		
		}

		public static CBQueimaTestesDTO ObterTesteSerie(string numECB, string codFab, string numRPA, string codDefeito, int codSubDefeito)
		{

			return daoPostoQueima.ObterTesteSerie(numECB, codFab, numRPA, codDefeito, codSubDefeito);

		}

		public static string ObterECBSerie(string codModelo, string numSerie)
		{
			return daoPostoQueima.ObterECBSerie(codModelo, numSerie);

		}

		public static bool VerificaPalletTotal(string codFab, string numRPA)
		{
			return daoPostoQueima.VerificaPalletTotal(codFab, numRPA);
		}

		public static int ObterQtdAprovPallet(string codFab, string numRPA, string flgAprov = "S")
		{
			return daoPostoQueima.ObterQtdAprovPallet(codFab, numRPA, flgAprov);
		}
		#endregion

		#region Manutenção Queima

		public static bool GravarInspecao(CBQueimaInspecaoDTO dadosInspec, List<CBQueimaTestesDTO> testes) {

			try
			{
				//identifica se a inspeção foi realizada com o aparelho aprovado
				var flgStatus = "S";
				var qtdReprov = testes.Count(x => x.FlgStatus == "X");
				if (qtdReprov > 0) {
					flgStatus = "X";
				}
				dadosInspec.FlgStatus = flgStatus;
				dadosInspec.DatLeitura = DateTime.Now;

				//gravar a inspeção da série
				GravarInspecao(dadosInspec);

				//gravar os dados dos testes
				testes.ForEach(x => {
					if (x.FlgStatus == "X")
					{
						var defeito = new CBDefeitoDTO()
						{
							NumECB = x.NumECB,
							CodLinha = x.CodLinha,
							NumPosto = x.NumPosto,
							CodDefeito = x.CodDefeito,
							CodSubDefeito = x.CodSubDefeito,
							DatEvento = SEMP.BO.Util.GetDate(),
							ObsDefeito = x.Observacao,
							FlgRevisado = Constantes.DEF_ABERTO
						};
						daoDefeitos.InsereDefeito(defeito); //insere o defeito para revisão no posto técnico
					}
					// grava os dados do teste em questão
					GravarTeste(x); 
				});

				return true;
			}
			catch
			{
				return false;
			}


		}

		public static bool FecharInspecao(string codFab, string numRPA, string codPallet) {

			try
			{
				var dado = ObterRegistro(codFab, numRPA, codPallet);
				var series = ObterInspecoes(codFab, numRPA, codPallet);

				dado.DatFechamento = SEMP.BO.Util.GetDate();
				if (series.Count(x => x.FlgStatus == "X") > 0)
				{
					dado.FlgStatus = "X";
				}
				else
				{
					dado.FlgStatus = "F";
				}

				if (numRPA.StartsWith("P")) {
					dado.NumRPE = numRPA;
					dado.DatLiberado = dado.DatFechamento;
				}

				GravarRegistro(dado);

				return true;
			}
			catch
			{
				return false;
			}

		}

		public static string GravarRegistro(CBQueimaRegistroDTO dados, bool flgExcluir = false, bool flgMovimenta = false) {

			return daoPostoQueima.GravarRegistro(dados, flgExcluir, flgMovimenta);

		}

		public static bool GravarInspecao(CBQueimaInspecaoDTO dados, bool flgExcluir = false)
		{

            var testes = daoPostoQueima.ObterTestes(dados.CodFab, dados.NumRPA, dados.NumECB);

            if (testes.Any(x => x.FlgStatus == "X"))
            {
                dados.FlgStatus = "X";
            }

			return daoPostoQueima.GravarInspecao(dados, flgExcluir);
		}

		public static bool GravarTeste(CBQueimaTestesDTO dados, bool flgExcluir = false)
		{
			
			return daoPostoQueima.GravarTeste(dados, flgExcluir);

		}

		#endregion

		#region Consultas Status

		public List<CBQueimaTipoStatusDTO> ObterListaStatus() {

			return daoPostoQueima.ObterListaStatus();

		}

		public CBQueimaTipoStatusDTO ObterTipoStatus(string codStatus)
		{

			return daoPostoQueima.ObterTipoStatus(codStatus);

		}
		#endregion

		#region Consultas GPA

		public static List<ResumoGPA> ObterPalletsGPA(DateTime datInicio, DateTime datFim)
		{
			return daoPostoQueima.ObterPalletsGPA(datInicio, datFim);

		}

		public static ResumoGPA ObterPalletGPA(string numPallet, string codModelo, string numSerie)
		{
			return daoPostoQueima.ObterPalletGPA(numPallet, codModelo, numSerie);

		}

		public static List<string> ObterSeriesPallet(string numPallet)
		{
			return daoPostoQueima.ObterSeriesPallet(numPallet);

		}

		public static ResumoGPA ObterPalletSerie(string codModelo, string numSerie)
		{

			return daoPostoQueima.ObterPalletSerie(codModelo, numSerie);

		}

		#endregion
	}
}
