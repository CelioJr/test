﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MATERIAIS.BO.Estoque;
using RAST.BO.Relatorios;
using SEMP.Model.DTO;
using SEMP.Model.VO.Materiais;

namespace SEMP.Areas.Outros.Controllers
{
	public class SaldoPlacaController : Controller
	{
		//
		// GET: /Coleta/SaldoPlaca/

		public ActionResult Index(String agrupa, String modelo, String placa)
		{
			var saldoplacas = new List<EstoquePCIDTO>();
			var resumo = new List<SaldoPlacasResumo>();

			try
			{
				//saldoplacas = SaldoPlacas.ObterSaldoPlacas(d, familia, modelo, placa, estudo);
				saldoplacas = Estoque.ObterEstoquePCI();
				resumo = Estoque.ObterResumoSaldoPlacas(agrupa, modelo, placa);


			}
			catch (Exception ex)
			{
				ViewBag.erro = "Erro ao consultar: " + ex.Message;
			}

			ViewBag.saldoPlacas = saldoplacas;
			return View(resumo);
		}

		public ActionResult APsMes(String codModelo, String datReferencia)
		{

			try
			{
				DateTime dIni = DateTime.Parse(datReferencia);

				var aps = RAST.BO.Relatorios.SaldoPlacas.ObterAPsMes(codModelo, dIni);

				var ats = RAST.BO.Relatorios.SaldoPlacas.ObterATsMes(codModelo, dIni);

				ViewBag.ats = ats;
				return View(aps);
			}
			catch (Exception ex)
			{

				ViewBag.erro = "Erro ao consultar: " + ex.Message;

				return View(new LSAAPDTO());
			}

		}

		public void ImportarSaldoSAP() {

			var dados = SAPConnector.SAPFunctions.ObterEstoquePCI();

			Estoque.GravarEstoquePCI(dados);

		}

	}
}
