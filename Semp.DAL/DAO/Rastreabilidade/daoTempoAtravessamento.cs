﻿using SEMP.DAL.Models;
using SEMP.Model.DTO;
using SEMP.Model.VO.Rastreabilidade;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEMP.DAL.DAO.Rastreabilidade
{
	public class daoTempoAtravessamento
	{

		public static List<TempoAtravessamento> ObterAtravessamento(DateTime datInicio, DateTime datFim, string codModelo)
		{

			using (var banco = new DTB_CBContext())
			{

				try
				{
					var dInicio = new SqlParameter("@DatInicio", datInicio);
					var dFim = new SqlParameter("@DatFim", datFim);
					var cModelo = new SqlParameter("@CodModelo", codModelo);
					
					var dados = banco.Database.SqlQuery<TempoAtravessamento>("spc_CBTempoAtravessamento @DatInicio, @DatFim, @CodModelo", dInicio, dFim, cModelo);

					return dados.ToList();

				}
				catch (Exception ex)
				{
					throw new Exception(ex.Message);
				}


			}

		}

		public static List<TempoAtravessamentoResumo> ObterAtravessamentoResumo(DateTime datInicio, DateTime datFim, string codModelo)
		{

			using (var banco = new DTB_CBContext())
			{

				try
				{
					var dInicio = new SqlParameter("@DatInicio", datInicio);
					var dFim = new SqlParameter("@DatFim", datFim);
					var cModelo = new SqlParameter("@CodModelo", codModelo);
					var fDados = new SqlParameter("@FlgDados", "R");

					var dados = banco.Database.SqlQuery<TempoAtravessamentoResumo>("spc_CBTempoAtravessamentoResumo @DatInicio, @DatFim, @CodModelo, @FlgDados", dInicio, dFim, cModelo, fDados);

					return dados.ToList();

				}
				catch (Exception ex)
				{
					throw new Exception(ex.Message);
				}


			}

		}
		
		public static List<TempoAtravessamentoModelo> ObterAtravessamentoModelo(DateTime datInicio, DateTime datFim, string codModelo)
		{

			using (var banco = new DTB_CBContext())
			{

				try
				{
					var dInicio = new SqlParameter("@DatInicio", datInicio);
					var dFim = new SqlParameter("@DatFim", datFim);
					var cModelo = new SqlParameter("@CodModelo", codModelo);
					var fDados = new SqlParameter("@FlgDados", "D");

					var dados = banco.Database.SqlQuery<TempoAtravessamentoModelo>("spc_CBTempoAtravessamentoResumo @DatInicio, @DatFim, @CodModelo, @FlgDados", dInicio, dFim, cModelo, fDados);

					return dados.ToList();

				}
				catch (Exception ex)
				{
					throw new Exception(ex.Message);
				}


			}

		}

		public static List<TempoAtravessamentoResumo> ObterAtravessamentoSerie(string numSerie)
		{

			using (var banco = new DTB_CBContext())
			{

				try
				{
					var dInicio = new SqlParameter("@DatInicio", DateTime.Now);
					var dFim = new SqlParameter("@DatFim", DateTime.Now);
					var cModelo = new SqlParameter("@CodModelo", "");
					var fDados = new SqlParameter("@FlgDados", "R");
					var nSerie = new SqlParameter("@NumSerie", numSerie);

					var dados = banco.Database.SqlQuery<TempoAtravessamentoResumo>("spc_CBTempoAtravessamentoResumo @DatInicio, @DatFim, @CodModelo, @FlgDados, @NumSerie", dInicio, dFim, cModelo, fDados, nSerie);

					return dados.ToList();

				}
				catch (Exception ex)
				{
					throw new Exception(ex.Message);
				}


			}

		}

		public static List<TempoAtravessamentoModelo> ObterAtravessamentoSerieDados(string numSerie)
		{

			using (var banco = new DTB_CBContext())
			{

				try
				{
					var dInicio = new SqlParameter("@DatInicio", DateTime.Now);
					var dFim = new SqlParameter("@DatFim", DateTime.Now);
					var cModelo = new SqlParameter("@CodModelo", "");
					var fDados = new SqlParameter("@FlgDados", "D");
					var nSerie = new SqlParameter("@NumSerie", numSerie);

					var dados = banco.Database.SqlQuery<TempoAtravessamentoModelo>("spc_CBTempoAtravessamentoResumo @DatInicio, @DatFim, @CodModelo, @FlgDados, @NumSerie", dInicio, dFim, cModelo, fDados, nSerie);

					return dados.ToList();

				}
				catch (Exception ex)
				{
					throw new Exception(ex.Message);
				}


			}

		}

		public static List<TempoAtravessamentoResumoAP> ObterAtravessamentoResumoAP(string flgDia, string flgDefeitos, string flgFaltaLeitura, string codFab, string numAP)
		{

			using (var banco = new DTB_CBContext())
			{

				try
				{
					var fDia = new SqlParameter("@FlgDiaUnico", flgDia ?? "");
					var fDefeito = new SqlParameter("@FlgDefeito", flgDefeitos ?? "");
					var fFaltaLeitura = new SqlParameter("@FlgFaltaLeit", flgFaltaLeitura ?? "");
					var fDados = new SqlParameter("@FlgDados", "R");
					var cFab = new SqlParameter("@CodFab", codFab);
					var nAP = new SqlParameter("@NumAP", numAP);

					var dados = banco.Database.SqlQuery<TempoAtravessamentoResumoAP>("spc_CBTempoAtravessamentoResumoAP @CodFab, @NumAP, @FlgDiaUnico, @FlgDefeito, @FlgFaltaLeit, @FlgDados", cFab, nAP, fDia, fDefeito, fFaltaLeitura, fDados);

					return dados.ToList();

				}
				catch (Exception ex)
				{
					throw new Exception(ex.Message);
				}


			}

		}

		public static decimal ObterDifHora(DateTime datInicio, DateTime datFim, int codLinha)
		{

			using (var banco = new DTB_CBContext())
			{

				try
				{
					var dInicio = new SqlParameter("@DatInicio", datInicio);
					var dFim = new SqlParameter("@DatFim", datFim);
					var cLinha = new SqlParameter("@CodLinha", codLinha);
					var hEntrada = new SqlParameter("@HoraEntrada", "06:45");
					var hSaida = new SqlParameter("@HoraSaida", "16:53");

					var dados = banco.Database.SqlQuery<decimal>("Select dbo.fun_CalculaDifHora(@DatInicio, @DatFim, @HoraEntrada, @HoraSaida, @CodLinha)", dInicio, dFim, hEntrada, hSaida, cLinha);

					return dados.FirstOrDefault();

				}
				catch (Exception ex)
				{
					throw new Exception(ex.Message);
				}


			}

		}

		public static decimal ObterDifHoraSemLinha(DateTime datInicio, DateTime datFim)
		{

			using (var banco = new DTB_CBContext())
			{

				try
				{
					var dInicio = new SqlParameter("@DatInicio", datInicio);
					var dFim = new SqlParameter("@DatFim", datFim);
					var hEntrada = new SqlParameter("@HoraEntrada", "06:45");
					var hSaida = new SqlParameter("@HoraSaida", "16:53");

					var dados = banco.Database.SqlQuery<decimal>("Select dbo.fun_CalculaDifHoraSemLinha(@DatInicio, @DatFim, @HoraEntrada, @HoraSaida)", dInicio, dFim, hEntrada, hSaida);

					return dados.FirstOrDefault();

				}
				catch (Exception ex)
				{
					throw new Exception(ex.Message);
				}


			}

		}

		public static List<string> ObterAPsModeloData (string codModelo, DateTime datReferencia) {

			using (var contexto = new DTB_CBContext())
			{
				var dados = contexto.tbl_CBEmbalada.Where(x => x.CodModelo == codModelo && x.DatReferencia == datReferencia).GroupBy(y => new { y.CodFab, y.NumAP }).Select(z => String.Format("{0} / {1}", z.Key.CodFab, z.Key.NumAP)).ToList();

				return dados;

			}


		}

		public static List<FeriadosDTO> ObterFeriados()
		{

			using (var banco = new DTB_CBContext())
			{

				return banco.tbl_Feriados.ToList().Select(x => new FeriadosDTO(){
					DatFeriado = x.DatFeriado,
					DesFeriado = x.DesFeriado,
					DiaUtil = x.DiaUtil,
					Setor = x.Setor

				}).ToList();

			}

		}

		public static List<FeriadosDTO> ObterFeriados(DateTime datInicio, DateTime datFim)
		{

			using (var banco = new DTB_CBContext())
			{

				return banco.tbl_Feriados.Where(x => x.DatFeriado >= datInicio && x.DatFeriado <= datFim).ToList().Select(x => new FeriadosDTO()
				{
					DatFeriado = x.DatFeriado,
					DesFeriado = x.DesFeriado,
					DiaUtil = x.DiaUtil,
					Setor = x.Setor

				}).ToList();

			}

		}


		#region Nova Versão

		public static List<CBTAModeloDTO> ObterResumo(DateTime datInicio, DateTime datFim){

			using (var contexto = new DTB_CBContext())
			{
				var dados = contexto.tbl_CBTAModelo.Where(x => x.Data >= datInicio && x.Data <= datFim)
							.Select(x => new CBTAModeloDTO() { 
								CodModelo = x.CodModelo,
								Data = x.Data,
								QtdAparelhos = x.QtdAparelhos,
								QtdDefeitos = x.QtdDefeitos,
								TempoIAC = x.TempoIAC,
								TempoIMC = x.TempoIMC,
								TempoMF = x.TempoMF,
								WIP1 = x.WIP1,
								WIP2 = x.WIP2,
								TempoAtravessa = x.TempoAtravessa,
								TempoCorrido = x.TempoCorrido,
								TempoTecnico = x.TempoTecnico,
								Observacao = x.Observacao
							});

				return dados.ToList();
			}

		}

		public static List<CBTAModeloDTO> ObterResumo(DateTime datInicio, DateTime datFim, string codModelo)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = contexto.tbl_CBTAModelo.Where(x => x.Data >= datInicio && x.Data <= datFim && x.CodModelo == codModelo)
							.Select(x => new CBTAModeloDTO()
							{
								CodModelo = x.CodModelo,
								Data = x.Data,
								QtdAparelhos = x.QtdAparelhos,
								QtdDefeitos = x.QtdDefeitos,
								TempoIAC = x.TempoIAC,
								TempoIMC = x.TempoIMC,
								TempoMF = x.TempoMF,
								WIP1 = x.WIP1,
								WIP2 = x.WIP2,
								TempoAtravessa = x.TempoAtravessa,
								TempoCorrido = x.TempoCorrido,
								TempoTecnico = x.TempoTecnico,
								Observacao = x.Observacao,
								TempoProducao = x.TempoProducao,
								TempoWIP = x.TempoWIP
							});

				return dados.ToList();
			}

		}

		public static List<CBTAModeloDTO> ObterResumoModelos(DateTime datInicio, DateTime datFim)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = contexto.tbl_CBTAModelo
							.Where(x => x.Data >= datInicio && x.Data <= datFim)
							.GroupBy(x => x.CodModelo)
							.Select(x => new CBTAModeloDTO()
							{
								CodModelo = x.Key,
								Data = datInicio,
								QtdAparelhos = x.Sum(y => y.QtdAparelhos),
								QtdDefeitos = x.Sum(y => y.QtdDefeitos),
								TempoIAC = (int)x.Average(y => y.TempoIAC),
								TempoIMC = (int)x.Average(y => y.TempoIMC),
								TempoMF = (int)x.Average(y => y.TempoMF),
								WIP1 = (int)x.Average(y => y.WIP1),
								WIP2 = (int)x.Average(y => y.WIP2),
								TempoAtravessa = (int)x.Average(y => y.TempoAtravessa),
								TempoCorrido = (int)x.Average(y => y.TempoCorrido),
								TempoTecnico = (int)x.Average(y => y.TempoTecnico),
								Observacao = ""
							});

				return dados.ToList();
			}

		}

		public static List<CBTAFaseDTO> ObterResumoFase(DateTime datInicio, DateTime datFim)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = contexto.tbl_CBTAFase.Where(x => x.DatReferencia >= datInicio && x.DatReferencia <= datFim)
							.Select(x => new CBTAFaseDTO()
							{
								CodModelo = x.CodModelo,
								DatReferencia = x.DatReferencia,
								NumSerieModelo = x.NumSerieModelo,
								NumSeriePCI = x.NumSeriePCI,
								Fase = x.Fase,
								Ordem = x.Ordem,
								TempoAtravessa = x.TempoAtravessa,
								TempoCorrido = x.TempoCorrido,
								QtdDefeitos = x.QtdDefeitos,
								TempoTecnico = x.TempoTecnico
							});

				return dados.ToList();
			}

		}

		public static List<CBTAItemDTO> ObterResumoItem(DateTime datInicio, DateTime datFim, string codModelo)
		{

			using (var contexto = new DTB_CBContext())
			{

				/*var dados = (from ta in contexto.tbl_CBTAItem
							let series = contexto.tbl_CBTAItem.Where(x => x.DatSaida >= datInicio && x.DatSaida <= datFim && x.FlgTempoMedio == true && (codModelo != "" && x.CodModelo == codModelo)).Select(x => x.NumSerieModelo).Distinct()
							 where series.Contains(ta.NumSerieModelo)
							 select new CBTAItemDTO()
							 {
								CodModelo = ta.CodModelo,
								NumSerieModelo = ta.NumSerieModelo,
								CodModeloPCI = ta.CodModeloPCI,
								NumSeriePCI = ta.NumSeriePCI,
								Fase = ta.Fase,
								SubFase = ta.SubFase,
								CodProcesso = ta.CodProcesso,
								Ordem = ta.Ordem,
								DatEntrada = ta.DatEntrada,
								DatSaida = ta.DatSaida,
								TempoCorrido = ta.TempoCorrido,
								TempoAtravessa = ta.TempoAtravessa,
								QtdDefeito = ta.QtdDefeito,
								TempoTecnico = ta.TempoTecnico,
								CodLinha = ta.CodLinha
							 });*/
				var dados = contexto.spc_CBTAObterResumoItem(datInicio, datFim, codModelo);

				return dados.ToList();
			}

		}

		public static List<CBTAItemDTO> ObterResumoItem(string numECB)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = contexto.tbl_CBTAItem.Where(x => x.NumSerieModelo == numECB)
							.Select(x => new CBTAItemDTO()
							{
								CodModelo = x.CodModelo,
								NumSerieModelo = x.NumSerieModelo,
								CodModeloPCI = x.CodModeloPCI,
								NumSeriePCI = x.NumSeriePCI,
								Fase = x.Fase,
								SubFase = x.SubFase,
								CodProcesso = x.CodProcesso,
								Ordem = x.Ordem,
								DatEntrada = x.DatEntrada,
								DatSaida = x.DatSaida,
								TempoCorrido = x.TempoCorrido,
								TempoAtravessa = x.TempoAtravessa,
								QtdDefeito = x.QtdDefeito,
								TempoTecnico = x.TempoTecnico,
								CodLinha = x.CodLinha
							});

				return dados.ToList();
			}

		}

		public static List<CBTATempoItemDTO> ObterTempoItem(string numECB)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = contexto.tbl_CBTATempoItem.Where(x => x.NumECB == numECB)
							.Select(x => new CBTATempoItemDTO()
							{
								CodModelo = x.CodModelo,
								NumECB = x.NumECB,
								Fase = x.Fase,
								SubFase = x.SubFase,
								CodLinha = x.CodLinha,
								DatEntrada = x.DatEntrada,
								DatSaida = x.DatSaida,
								TempoAtravessa = x.TempoAtravessa,
								Observacao = x.Observacao,
								QtdDefeito = x.QtdDefeito,
								TempoCorrido = x.TempoCorrido,
								TempoTecnico = x.TempoTecnico
							});

				return dados.ToList();
			}

		}

		public static List<CBTAMetasDTO> ObterMetas(string codModelo)
		{

			using (var contexto = new DTB_CBContext())
			{
				var dados = contexto.tbl_CBTAMetas.Select(x => new CBTAMetasDTO()
							{
								CodFase = x.CodFase,
								SubFase = x.SubFase,
								Meta = x.Meta,
								CodModelo = x.CodModelo,
								Ordem = x.Ordem
							});

				if(!String.IsNullOrEmpty(codModelo)){
					dados = dados.Where(x => x.CodModelo == codModelo);

				}

				var retorno = dados.ToList();

				if (retorno.Count() == 0){

					dados = contexto.tbl_CBTAMetas.Where(x => x.CodModelo == "").Select(x => new CBTAMetasDTO()
					{
						CodFase = x.CodFase,
						SubFase = x.SubFase,
						Meta = x.Meta,
						CodModelo = x.CodModelo,
						Ordem = x.Ordem
					});
					
					retorno = dados.ToList();
				}

				return retorno;
			}

		}

		#endregion

	}
}
