using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_CBCadOrigem
    {
        public string CodOrigem { get; set; }
        public string DesOrigem { get; set; }
        public Nullable<bool> FlgAtivo { get; set; }
    }
}
