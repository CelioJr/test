namespace SEMP.DAL.Integrator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_ProcessaApontFinal
    {
        public int id { get; set; }

        public DateTime? datprocesso { get; set; }

        public bool? status { get; set; }
    }
}
