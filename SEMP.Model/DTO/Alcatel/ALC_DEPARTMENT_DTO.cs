using System;
using System.Collections.Generic;

namespace SEMP.Model.DTO
{
    public partial class ALC_DEPARTMENT_DTO
    {
        public ALC_DEPARTMENT_DTO()
        {
            this.ALC_DOWNTIME = new List<ALC_DOWNTIME_DTO>();
        }

        public int ID_DEPARTMENT { get; set; }
        public string DESCRIPTION { get; set; }
        public virtual ICollection<ALC_DOWNTIME_DTO> ALC_DOWNTIME { get; set; }
    }
}
