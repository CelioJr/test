using System;
using System.Collections.Generic;

namespace SEMP.DAL.Models
{
    public partial class tbl_RelacaoIMC_IAC
    {
        public string CodModelo { get; set; }
        public string CodItem { get; set; }
    }
}
