namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_SESSAO
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SES_CONEXAO { get; set; }

        public DateTime? SES_DATAHORA_LOGIN { get; set; }

        [StringLength(20)]
        public string SES_USUARIO { get; set; }

        [StringLength(40)]
        public string SES_NOME_USUARIO { get; set; }

        [StringLength(40)]
        public string SES_NOME_MAQUINA { get; set; }

        [StringLength(20)]
        public string SES_END_IP { get; set; }

        [StringLength(3)]
        public string SIS_CODIGO { get; set; }
    }
}
