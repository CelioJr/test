﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEMP.DAL.DAO.Rastreabilidade;
using SEMP.DAL.Models;
using SEMP.DAL.DAO.Embalagem;
using SEMP.Model.VO;
using SEMP.Model.VO.Rastreabilidade;
using SEMP.Model.DTO;
using SEMP.Model;
using SEMP.BO;

namespace RAST.BO.Rastreabilidade
{
	[Serializable]
	public class Passagem : Posto
	{

		public Passagem(string numIP) : base(numIP) { }
		public Passagem(int codLinha, int numPosto) : base(codLinha, numPosto) { }
		public Passagem(DadosPosto p) : base(p) { }

		/// <summary>
		/// Rotina para fazer a validação de erros de passagem
		/// Situações verificadas:	1 - Leitura repetida no mesmo posto
		///							2 - Leitura fora da sequência correta
		///							3 - Item não liberado pelo posto técnico
		///							
		/// </summary>
		/// <param name="numECB">Etiqueta lida</param>
		/// <param name="codDRT">Usuário que leu</param>
		/// <returns>Estado com o código relacionado ao resultado das validações</returns>
		public Mensagem ValidaPassagem(string numECB, string codDRT)
		{

			Mensagem retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);

			// verifica se há defeitos em aberto
			Boolean erroDefeitoAberto = daoPassagem.DefeitoAberto(numECB);

			if (erroDefeitoAberto)
			{
				retorno = Defeito.AddDefeitoRastreamento(numECB, Constantes.RAST_NAOLIBERADO_DEFEITO, codDRT, posto.CodLinha, posto.NumPosto);
			}
			else
			{
				// valida erro de passagem
				Boolean erroPassagem = daoPassagem.PassouNoPosto(numECB, posto.CodLinha, posto.NumPosto);//, posto.NumSeq);				

				if (erroPassagem)
				{
					retorno = Defeito.AddDefeitoRastreamento(numECB, Constantes.RAST_LEITURA_REPETIDA, codDRT, posto.CodLinha, posto.NumPosto);
				}
				else
				{
					//Verifica se a última leitura foi realizada em um posto de mesmo número de sequência
					CBPassagemDTO ultimaLeitura = daoPassagem.ObterUltimaLeitura(numECB);

					int ultimaSequencia = daoPassagem.ObterNumSeq(ultimaLeitura.CodLinha, ultimaLeitura.NumPosto);

					if (ultimaSequencia == posto.NumSeq)
					{
						retorno = Defeito.AddDefeitoRastreamento(numECB, Constantes.RAST_LEITURA_REPETIDA, codDRT, posto.CodLinha, posto.NumPosto);
						return retorno;
					}
					//verifica se deixou de ler em algum posto

					// pegar a quantidade de postos a ler considerando o posto atual
					int qtdPostos = daoPassagem.ObtertQtdPostosSequencia(posto.CodLinha, posto.NumSeq);

					// pegar a quantidade de leituras até o posto corrente
					int qtdLeituras = daoPassagem.ObterQtdLeituras(posto.CodLinha, numECB, posto.NumSeq);

					// se a quantidade de postos for maior que a quantidade de leituras, então deixou de ler em algum posto
					if (qtdPostos != qtdLeituras)
					{
						retorno = Defeito.AddDefeitoRastreamento(numECB, Constantes.RAST_FORA_SEQUENCIA, codDRT, posto.CodLinha, posto.NumPosto);
					}
				}
			}

			return retorno;
		}

		public Mensagem ValidaPassagem(string numECB, string codDRT, int codLinha, int numPosto, string numAP)
		{
			Mensagem retorno = daoPassagem.ValidarErros(numECB, codDRT, codLinha, numPosto, numAP);
			return retorno;
		}


		public Mensagem ValidaPassagemTablet(string numECB, string codDRT, int codLinha, int numPosto)
		{

			Mensagem retorno = daoPassagem.ValidarErrosTablet(numECB, codDRT, codLinha, numPosto);

			return retorno;


		}

		/// <summary>
		/// Rotina que grava a leitura do posto no banco de dados
		/// </summary>
		/// <param name="numECB"></param>
		/// <param name="codDRT"></param>
		/// <returns></returns>
		public Mensagem GravarPassagem(string numECB, string codDRT)
		{

			Mensagem retorno = new Mensagem();

			try
			{

				tbl_CBPassagem passagem = new tbl_CBPassagem();

				passagem.CodLinha = posto.CodLinha;
				passagem.NumPosto = posto.NumPosto;
				passagem.NumECB = numECB;
				passagem.CodDRT = codDRT;
				passagem.DatEvento = SEMP.BO.Util.GetDate();

				if (daoPassagem.VerificaIdentifica(passagem.NumECB) == false)
				{
					tbl_CBIdentifica identifica = new tbl_CBIdentifica();

					identifica.NumECB = numECB;
					identifica.CodModelo = numECB.Substring(0, 6);
					string fabrica = daoGeral.ObterFabricaPorLinha(posto.CodLinha).ToUpper().Trim();

					switch (fabrica)
					{
						case (Constantes.IAC):
							identifica.DatIdentifica = SEMP.BO.Util.GetDate();
							identifica.CodLinhaIAC = posto.CodLinha.ToString();
							identifica.CodPlacaIAC = daoGeral.ObterCodModeloIAC(identifica.CodModelo);
							break;

						case (Constantes.IMC):
							identifica.DatIMC = SEMP.BO.Util.GetDate();
							identifica.CodLinhaIMC = posto.CodLinha.ToString();
							identifica.CodPlacaIAC = daoGeral.ObterCodModeloIAC(identifica.CodModelo);
							identifica.CodPlacaIMC = identifica.CodModelo;
							break;

						case (Constantes.FEC):
							identifica.DatModelo = SEMP.BO.Util.GetDate();
							identifica.CodLinhaFEC = posto.CodLinha.ToString();
							identifica.CodPlacaIMC = daoGeral.ObterCodModeloIMC(identifica.CodModelo);
							identifica.CodModelo = identifica.CodModelo;
							identifica.CodPlacaIAC = daoGeral.ObterCodModeloIAC(identifica.CodModelo);
							break;
					}

					Boolean gravar = daoPassagem.InsereIdentifica(identifica);

					if (!gravar)
					{
						retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
						return retorno;
					}

					gravar = daoPassagem.InserePassagem(passagem, codDRT, posto);
					if (gravar)
					{
						retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);
					}
					else
					{
						retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
					}

				}
				else
				{
					Boolean gravar = daoPassagem.InserePassagem(passagem, codDRT, posto);
					if (gravar)
					{
						retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);
					}
					else
					{
						retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
					}
				}
			}
			catch (Exception ex)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				retorno.StackErro = ex.InnerException.Message;
			}

			return retorno;
		}

		public Mensagem GravaPassagemProcedure(string numECB, string codDRT, int codLinha, int numPosto, string codFab, string numAP)
		{
			Mensagem retorno = new Mensagem();

			try
			{
				tbl_CBPassagem passagem = new tbl_CBPassagem();

				passagem.CodLinha = posto.CodLinha;
				passagem.NumPosto = posto.NumPosto;
				passagem.NumECB = numECB;
				passagem.CodDRT = codDRT;

				if (codFab != null && numAP != null)
				{
					passagem.CodFab = codFab;
					passagem.NumAP = numAP;
				}

				var gravar = daoPassagem.InserePassagemProcedure(passagem, codDRT, posto);

				if (gravar)
				{
					retorno.CodMensagem = "R0001";
					retorno.DesCor = "lime";
					retorno.DesMensagem = "LEITURA OK. LEIA PRÓXIMO.";
					//retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);
				}
				else
				{
					retorno.CodMensagem = "E0002";
					retorno.DesCor = "red";
					retorno.DesMensagem = "OCORREU UM ERRO NÃO IDENTIFICADO. ENTRE EM CONTATO COM O ADMINISTRADOR DO SISTEMA.";
					//retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				}
			}
			catch (Exception ex)
			{
				//retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				retorno.CodMensagem = "E0002";
				retorno.DesCor = "red";
				retorno.DesMensagem = "OCORREU UM ERRO NÃO IDENTIFICADO. ENTRE EM CONTATO COM O ADMINISTRADOR DO SISTEMA.";
				retorno.StackErro = ex.InnerException.Message;
			}

			return retorno;
		}


		/// <summary>
		/// Rotina que grava a leitura do posto no banco de dados com CodFab e NumAP
		/// </summary>
		/// <param name="numECB"></param>
		/// <param name="codDRT"></param>
		/// <param name="CodFab"></param>
		/// <param name="NumAP"></param>
		/// <returns></returns>
		public Mensagem GravarPassagem(string numECB, string codDRT, string CodFab, string NumAP)
		{

			Mensagem retorno = new Mensagem();

			try
			{

				tbl_CBPassagem passagem = new tbl_CBPassagem();

				passagem.CodLinha = posto.CodLinha;
				passagem.NumPosto = posto.NumPosto;
				passagem.NumECB = numECB;
				passagem.CodDRT = codDRT;
				passagem.CodFab = CodFab;
				passagem.NumAP = NumAP;
				passagem.DatEvento = SEMP.BO.Util.GetDate();

				#region Identificação
				if (daoPassagem.VerificaIdentifica(passagem.NumECB) == false)
				{
					tbl_CBIdentifica identifica = new tbl_CBIdentifica();

					identifica.NumECB = numECB;
					identifica.CodModelo = numECB.Substring(0, 6);
					string fabrica = daoGeral.ObterFabricaPorLinha(posto.CodLinha).ToUpper().Trim();

					switch (fabrica)
					{
						case (Constantes.IAC):
							identifica.DatIdentifica = SEMP.BO.Util.GetDate();
							identifica.CodLinhaIAC = posto.CodLinha.ToString();
							identifica.CodPlacaIAC = daoGeral.ObterCodModeloIAC(identifica.CodModelo);
							break;

						case (Constantes.IMC):
							identifica.DatIMC = SEMP.BO.Util.GetDate();
							identifica.CodLinhaIMC = posto.CodLinha.ToString();
							identifica.CodPlacaIAC = daoGeral.ObterCodModeloIAC(identifica.CodModelo);
							identifica.CodPlacaIMC = identifica.CodModelo;
							break;

						case (Constantes.FEC):
							identifica.DatModelo = SEMP.BO.Util.GetDate();
							identifica.CodLinhaFEC = posto.CodLinha.ToString();
							identifica.CodPlacaIMC = daoGeral.ObterCodModeloIMC(identifica.CodModelo);
							identifica.CodModelo = identifica.CodModelo;
							identifica.CodPlacaIAC = daoGeral.ObterCodModeloIAC(identifica.CodModelo);
							break;
					}

					Boolean gravar = daoPassagem.InsereIdentifica(identifica);

					if (!gravar)
					{
						retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
						return retorno;
					}

					gravar = daoPassagem.InserePassagem(passagem, codDRT, posto);
					if (gravar)
					{
						retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);
					}
					else
					{
						retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
					}

				}
				#endregion
				else
				{
					Boolean gravar = daoPassagem.InserePassagem(passagem, codDRT, posto);
					if (gravar)
					{
						retorno = daoMensagem.ObterMensagem(Constantes.RAST_OK);
					}
					else
					{
						retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
					}
				}
			}
			catch (Exception ex)
			{
				retorno = daoMensagem.ObterMensagem(Constantes.DB_ERRO);
				retorno.StackErro = ex.InnerException.Message;
			}

			return retorno;


		}


		/// <summary>
		/// Recupera o último posto lido pela item
		/// </summary>
		/// <param name="numECB">Etiqueta lida</param>
		/// <returns>Posto</returns>
		public CBPostoDTO UltimoPosto(string numECB)
		{

			CBPassagemDTO ultimoPosto = daoPassagem.ObterUltimaLeitura(numECB);
			CBPostoDTO posto = null;

			if (ultimoPosto != null)
			{
				posto = daoGeral.ObterPosto(ultimoPosto.CodLinha, ultimoPosto.NumPosto);
			}

			return posto;
		}

		public List<CBPassagemDTO> ObterPassagemPosto()
		{
			return daoPassagem.ObterPassagemPosto(posto.CodLinha, posto.NumPosto);
		}

		public List<CBPassagemDTO> ObterPassagemPosto(DateTime datInicio, DateTime datFim)
		{
			return daoPassagem.ObterPassagemPosto(posto.CodLinha, posto.NumPosto, datInicio, datFim);
		}

		public static RealPrevisto ObterQuantidadeRealEPrevisto(int codLinha)
		{
			return daoPassagem.ObterQuantidadeRealEPrevisto(codLinha);
		}

		public static Boolean PassouNoPosto(string numECB, int codLinha, int numPosto) 
		{
			return daoPassagem.PassouNoPosto(numECB, codLinha, numPosto);
		}

	}
}
