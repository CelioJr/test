using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SEMP.DAL.Models.Mapping
{
    public class tbl_ProdParadaDepMap : EntityTypeConfiguration<tbl_ProdParadaDep>
    {
        public tbl_ProdParadaDepMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodFab, t.NumParada, t.CodDepto, t.CodFabDepto});

            // Properties
            this.Property(t => t.CodFab)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.NumParada)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(9)
                .IsUnicode(false);

            this.Property(t => t.CodDepto)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            this.Property(t => t.CodFabDepto)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2)
                .IsUnicode(false);

            this.Property(t => t.TipItem)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1)
                .IsUnicode(false);

            this.Property(t => t.CodGrupo)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(5)
                .IsUnicode(false);

            // Table & Column Mappings
            this.ToTable("tbl_ProdParadaDep");
            this.Property(t => t.CodFab).HasColumnName("CodFab");
            this.Property(t => t.NumParada).HasColumnName("NumParada");
            this.Property(t => t.CodDepto).HasColumnName("CodDepto");
            this.Property(t => t.CodFabDepto).HasColumnName("CodFabDepto");
            this.Property(t => t.TipItem).HasColumnName("TipItem");
            this.Property(t => t.CodGrupo).HasColumnName("CodGrupo");
            this.Property(t => t.DatEnvio).HasColumnName("DatEnvio");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
        }
    }
}
