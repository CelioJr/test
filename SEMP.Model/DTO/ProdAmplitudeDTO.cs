using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SEMP.Model.DTO
{
	public partial class ProdAmplitudeDTO
	{
		[Display(Name = "F�b.")]
		public string CodFab { get; set; }
		[Display(Name = "Depto")]
		public string CodDepto { get; set; }
		[Display(Name = "Usu�rio")]
		public string NomUsuario { get; set; }
		[Display(Name = "E-mail")]
		public string NomEmail { get; set; }
		[Display(Name = "Tipo Usu�rio")]
		public string TipUsuario { get; set; }
		public System.Guid rowguid { get; set; }

		[Display(Name = "Descri��o")]
		public string DesFabrica { get; set; }
		[Display(Name = "Nome")]
		public string NomeUsuario { get; set; }
		[Display(Name = "Descri��o")]
		public string DesDepto { get; set; }
		
	}
}
