﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEMP.Model.DTO
{
	public class CBEntradaIACDTO
	{
		[Key]
		[Column(Order = 0)]
		[StringLength(10)]
		public string NumOP { get; set; }

		[Key]
		[Column(Order = 1)]
		[StringLength(6)]
		public string CodItem { get; set; }

		[Key]
		[Column(Order = 2)]
		[StringLength(10)]
		public string NumUD { get; set; }

		[StringLength(40)]
		public string BarrasUD { get; set; }

		[StringLength(50)]
		public string BarrasFornecedor { get; set; }

		public decimal? QtdItem { get; set; }

		[Key]
		[Column(Order = 3)]
		public DateTime DatLeitura { get; set; }

		[StringLength(20)]
		public string NomUsuario { get; set; }
	}
}
