namespace SEMP.Model.DTO
{
	using System;
	using System.ComponentModel.DataAnnotations;

	public partial class tbl_ZApontamento_DTO
	{
        public int ID { get; set; }

        [Required]
        [StringLength(12)]
        public string NumOrdem { get; set; }

        public int QtdProduzido { get; set; }

        [StringLength(32)]
        public string CodHash { get; set; }

        public DateTime DatCriacao { get; set; }

        public int CodStatus { get; set; }

        public DateTime? DatProcessamento { get; set; }
    }
}
