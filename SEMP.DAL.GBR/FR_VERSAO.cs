namespace SEMP.DAL.GBR
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FR_VERSAO
    {
        [Key]
        public int VER_CODIGO { get; set; }

        [Required]
        [StringLength(1)]
        public string VER_TIPO { get; set; }

        [StringLength(3)]
        public string SIS_CODIGO { get; set; }

        public int? OBJ_CODIGO { get; set; }

        [Required]
        [StringLength(100)]
        public string VER_DESCRICAO { get; set; }

        [Required]
        public byte[] VER_CONTEUDO { get; set; }

        public DateTime VER_DATA_HORA { get; set; }

        [Required]
        [StringLength(10)]
        public string VER_VERSAO { get; set; }

        public int USR_CODIGO { get; set; }
    }
}
